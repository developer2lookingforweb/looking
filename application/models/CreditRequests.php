<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 23-Nov-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="cretid_requests")
 */
class CreditRequests
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Products")
     */
    private $product;
    
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $quotes;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $initial_fee;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $amount;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $fee_value;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $last_name;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $email;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $mobile;
    
    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $date_request;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set product
     *
     * @param models\Products $product
     */
    public function setProduct(\models\Products $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return models\Products
     */
    public function getProduct()
    {
        return $this->product;
    }
    

    /**
     * Set quotes
     *
     * @param string $quotes
     * @return CretitRequests
     */
    public function setQuotes($quotes)
    {
        $this->quotes = $quotes;
    
        return $this;
    }

    /**
     * Get quotes
     *
     * @return string 
     */
    public function getQuotes()
    {
        return $this->quotes;
    }
    
    
    /**
     * Set fee_value
     *
     * @param string $fee_value
     * @return ProductsOrders
     */
    public function setFeeValue($fee_value)
    {
        $this->fee_value = $fee_value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getFeeValue()
    {
        return $this->fee_value;
    }
    
    
    
    /**
     * Set name
     *
     * @param string $name
     * @return CreditRequests
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    /**
     * Set last_name
     *
     * @param string $last_name
     * @return CreditRequests
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }
    
    
    /**
     * Set email
     *
     * @param string $email
     * @return CreditRequests
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    
    /**
     * Set mobile
     *
     * @param string $mobile
     * @return CreditRequests
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }
    
    /**
     * Set initial_fee
     *
     * @param string $initial
     * @return CreditRequests
     */
    public function setInitialFee($initial)
    {
        $this->initial_fee= $initial;
    
        return $this;
    }

    /**
     * Get initialFee
     *
     * @return string 
     */
    public function getInitialFee()
    {
        return $this->initial_fee;
    }

    
    /**
     * Set amount
     *
     * @param string $amount
     * @return CreditRequests
     */
    public function setAmount($amount)
    {
        $this->amount= $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    
    /**
     * Set date_request
     *
     * @param string $request
     */
    public function setDateRequest($request)
    {
        $this->date_request = $request;
    }

    /**
     * Get date_request
     *
     * @return string 
     */
    public function getDateRequest()
    {
        return $this->date_request;
    }
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['quotes']     = $this->getQuotes();
        $return['amount']     = $this->getAmount();
        $return['fee_value']     = $this->getFeeValue();
        $return['initial_fee']     = $this->getInitialFee();
        $return['name']     = $this->getName();
        $return['last_name']     = $this->getLastName();
        $return['email']     = $this->getEmail();
        $return['mobile']     = $this->getMobile();
        $return['date_request']     = $this->getDateRequest();
        $return['product']     = $this->getProduct()->toArray();
        return $return;
    }
}