<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 11-Dec-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="products_tags")
 */
class ProductsTags
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Products")
     */
    private $products;
    
    /**
     * @ManyToOne(targetEntity="Tags")
     */
    private $tags;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set product
     *
     * @param models\Products $product
     */
    public function setProduct(\models\Products $product)
    {
        $this->products = $product;
    }

    /**
     * Get product
     *
     * @return models\Products
     */
    public function getProduct()
    {
        return $this->products;
    }
    

    /**
     * Set tag
     *
     * @param models\Tags $tag
     */
    public function setTag(\models\Tags $tag)
    {
        $this->tags = $tag;
    }

    /**
     * Get tag
     *
     * @return models\Tags
     */
    public function getTag()
    {
        return $this->tags;
    }
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
         if ($relations){
            $return['product'] = $this->getProduct()->toArray();
            $return['tag'] = $this->getTag()->toArray();
        }
        return $return;
    }
}