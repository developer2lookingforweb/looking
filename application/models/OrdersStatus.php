<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="orders_status")
 */
class OrdersStatus
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $name;
    
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set status
     *
     * @param text $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['name']    = $this->getName();
        $return['status']  = $this->getStatus();

        return $return;
    }
}