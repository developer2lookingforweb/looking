<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="users_data")
 */
class UsersData
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;
    
    /**
     * @OneToOne(targetEntity="Users", inversedBy="user_data", cascade={"all"})
     */
    private $user;

    /**
     * @Column(type="string", nullable=false)
     */
    private $dni;

    /**
     * @Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @Column(type="string", nullable=true)
     */
    private $contact;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set user
     *
     * @param models\Users $user
     */
    public function setUser(\models\Users $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return models\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set dni
     *
     * @param string $identification
     * @return UsersData
     */
    public function setDni($identification)
    {
        $this->dni = $identification;
    
        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }
    
    /**
     * Set phone
     *
     * @param string $phone
     * @return UsersData
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * Set address
     *
     * @param string $address
     * @return UsersData
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set contact
     *
     * @param string $contact
     * @return UsersData
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    public function toArray($user = true)
    {
        $return = array();
        
        $return['id']               = $this->getId();
        $return['user']             = $this->getUser()->toArray();
        $return['dni']   = $this->getDni();
        $return['phone']   = $this->getPhone();
        $return['address']   = $this->getAddress();
        $return['contact']   = $this->getContact();
        
        return $return;
    }
}