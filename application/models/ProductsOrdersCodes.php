<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="products_orders_codes")
 */
class ProductsOrdersCodes
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="ProductsOrders")
     */
    private $product_order;
    
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $code;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $redeemed;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $value;
    
    /**
     * @Column(type="datetime", nullable=true) 
     */
    private $redeemed_date;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set productOrder
     *
     * @param models\ProductsOrders $productOrder
     */
    public function setProductOrder(\models\ProductsOrders $productOrder)
    {
        $this->product_order = $productOrder;
    }
    
    /**
     * Get productOrder
     *
     * @return models\ProductsOrders
     */
    public function getProductOrder()
    {
        return $this->product_order;
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return ProductsOrders
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
    
    
    /**
     * Set value
     *
     * @param string $value
     * @return ProductsOrders
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set redeemed
     *
     * @param string $redeemed
     * @return ProductsOrders
     */
    public function setRedeemed($redeemed)
    {
        $this->redeemed = $redeemed;
    
        return $this;
    }

    /**
     * Get redeemed
     *
     * @return string 
     */
    public function getRedeemed()
    {
        return $this->redeemed;
    }
    
    
    /**
     * Set redeemed_date
     *
     * @param string $redeemedDate
     * @return Orders
     */
    public function setRedeemedDate($redeemedDate)
    {
        $this->redeemed_date = $redeemedDate;
    
        return $this;
    }

    /**
     * Get redeemed_date
     *
     * @return string 
     */
    public function getRedeemedDate()
    {
        return $this->redeemed_date;
    }

    
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['code']     = $this->getCode();
        $return['redeemed']     = $this->getRedeemed();
        $return['redeemed_date']     = $this->getRedeemedDate();
        $return['value']     = $this->getValue();
        
         if ($relations){
            $return['brand'] = $this->getProductOrder()->toArray();
        }
        return $return;
    }
}