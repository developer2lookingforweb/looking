<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 22-Mar-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="questions")
 */
class Questions
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $question;

    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $type;

    
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $status;
    
    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $multiple;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $outer;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $step;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $icon;
    
    /**
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $label;
    
    /**
     * @Column(type="integer", length=3, nullable=true) 
     */
    private $order;
    
     /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Qestions
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get questions
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }
 
    
    /**
     * Set type
     *
     * @param string $type
     * @return Qestions
     */
    public function setType($type)
    {
        $this->question = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
 
    
    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    
    
    /**
     * Set status
     *
     * @param string $status
     * @return Qestions
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    /**
     * Get multiple
     *
     * @return integer 
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    
    
    /**
     * Set multiple
     *
     * @param string $multiple
     * @return Qestions
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    
        return $this;
    }
    
    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    
    
    /**
     * Set order
     *
     * @param string $order
     * @return Qestions
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get outer
     *
     * @return string 
     */
    public function getOuter()
    {
        return $this->outer;
    }

    
    
    /**
     * Set outer
     *
     * @param string $outer
     * @return Qestions
     */
    public function setOuter($outer)
    {
        $this->outer = $outer;
    
        return $this;
    }

    /**
     * Get step
     *
     * @return string 
     */
    public function getStep()
    {
        return $this->step;
    }

    
    
    /**
     * Set step
     *
     * @param string $step
     * @return Qestions
     */
    public function setStep($step)
    {
        $this->step = $step;
    
        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    
    
    /**
     * Set icon
     *
     * @param string $icon
     * @return Qestions
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    
    
    /**
     * Set label
     *
     * @param string $label
     * @return Qestions
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Set category
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return models\Categories 
     */
    public function getCategory()
    {
        return $this->category;
    }
 
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['question']     = $this->getQuestion();
        $return['type']     = $this->getType();
        $return['status']     = $this->getStatus();
        $return['multiple']     = $this->getMultiple();
        $return['cateogry']     = $this->getCategory()->toArray();
        $return['order']     = $this->getOrder();
        $return['outer']     = $this->getOuter();
        $return['step']     = $this->getStep();
        $return['icon']     = $this->getIcon();
        $return['label']     = $this->getLabel();
        
        return $return;
    }
}