<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 23-Nov-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="multipropose_registers")
 */
class MultiproposeRegisters
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $last_name;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $email;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $mobile;
    
    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $date_request;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set name
     *
     * @param string $name
     * @return CreditRequests
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    /**
     * Set last_name
     *
     * @param string $last_name
     * @return CreditRequests
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }
    
    
    /**
     * Set email
     *
     * @param string $email
     * @return CreditRequests
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    
    /**
     * Set mobile
     *
     * @param string $mobile
     * @return CreditRequests
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }
    
    

    
    /**
     * Set date_request
     *
     * @param string $request
     */
    public function setDateRequest($request)
    {
        $this->date_request = $request;
    }

    /**
     * Get date_request
     *
     * @return string 
     */
    public function getDateRequest()
    {
        return $this->date_request;
    }
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        
        $return['name']     = $this->getName();
        $return['last_name']     = $this->getLastName();
        $return['email']     = $this->getEmail();
        $return['mobile']     = $this->getMobile();
        $return['date_request']     = $this->getDateRequest();
        
        return $return;
    }
}