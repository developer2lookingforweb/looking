<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 20-Oct-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="cupons")
 */
class Cupons
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $code;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $discount;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $filter;
    
     /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $filter_id;

    
    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $end;
     
     /**
     * @ManyToOne(targetEntity="Customers")
     */
     private $customer;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set filter
     *
     * @param string $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Get filter
     *
     * @return string 
     */
    public function getFilter()
    {
        return $this->filter;
    }
    
    /**
     * Set filterId
     *
     * @param string $filterId
     */
    public function setFilterId($filterId)
    {
        $this->filter_id = $filterId;
    }

    /**
     * Get filterId
     *
     * @return string 
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }
    
    /**
     * Set discount
     *
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    

    /**
     * Set end
     *
     * @param string $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->end;
    }
    
    
    /**
     * Set customer
     *
     * @param models\Customers $customer
     */
    public function setCustomer(\models\Customers $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Get customer
     *
     * @return models\Customers
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    
    public function toArray()
    {
        $return = array();
        $return['id']           = $this->getId();
        $return['code']         = $this->getCode();
        $return['discount']     = $this->getDiscount();
        $return['filter']       = $this->getFilter();
        $return['filterId']     = $this->getFilterId();
        $return['end']          = $this->getEnd();
        $return['customer']     = $this->getCustomer()->toArray();
        
        return $return;
    }
}