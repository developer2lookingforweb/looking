<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 20-Oct-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="campaigns")
 */
class Campaigns
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $discount;

    /**
     * @Column(type="date", nullable=true) 
     */
    private $start;

    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $end;
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set discount
     *
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set start
     *
     * @param integer $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * Get start
     *
     * @return integer 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param string $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->end;
    }
    
    public function toArray()
    {
        $return = array();
        $return['id']           = $this->getId();
        $return['name']         = $this->getName();
        $return['discount']     = $this->getDiscount();
        $return['start']        = $this->getStart();
        $return['end']          = $this->getEnd();
        
        return $return;
    }
}