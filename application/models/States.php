<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="states")
 */
class States
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Countries")
     */
    private $country;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $name;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $code;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $lon;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $lat;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

     /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set lon
     *
     * @param string $lon
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set lat
     *
     * @param string $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set country
     *
     * @param models\Countries $country
     */
    public function setCountry(\models\Countries $country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return models\Countries 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['name']     = $this->getName();
        $return['code']     = $this->getCode();
        $return['lon']     = $this->getLon();
        $return['lat']     = $this->getLat();
        $return['country']  = $this->getCountry()->toArray();
        
        return $return;
    }
}