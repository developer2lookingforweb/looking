<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="mobile_refills")
 */
class MobileRefills
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="integer", nullable=false) 
     */
    private $filled;
    
    /**
     * @ManyToOne(targetEntity="Brands")
     */
    private $brand;
    
    /**
     * @OneToOne(targetEntity="Orders")
     * @JoinColumn(name="order_id", referencedColumnName="id", nullable=true)
     */
    private $order;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $value;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $user;
    
    /**
     * @Column(type="text", length=1000, nullable=true) 
     */
    private $location;
    
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $number;
    
    /**
     * @Column(type="date", nullable=true) 
     */
    private $request_date;
    
    
    public function __construct()
    {
        $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filled
     *
     * @param string $filled
     */
    public function setFilled($filled)
    {
        $this->filled = $filled;
    }

    /**
     * Get filled
     *
     * @return string 
     */
    public function getFilled()
    {
        return $this->filled;
    }

    /**
     * Set number
     *
     * @param string $numbed
     */
    public function setNumber($numbed)
    {
        $this->number = $numbed;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set user
     *
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
    

    /**
     * Set request_date
     *
     * @param string $requestDate
     */
    public function setRequestDate($requestDate)
    {
        $this->request_date = $requestDate;
    }

    /**
     * Get request_date
     *
     * @return string
     */
    public function getRequestDate()
    {
        return $this->request_date;
    }
    
    
    /**
     * Set brand
     *
     * @param models\Brands $brand
     */
    public function setBrand(\models\Brands $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Get brand
     *
     * @return models\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
    
    
    /**
     * Set order
     *
     * @param models\Orders $order
     */
    public function setOrder(\models\Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Get order
     *
     * @return models\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }
    
        
    public function toArray()
    {
        $return = array();
        
        
        $return['id']           = $this->getId();
        $return['filled']       = $this->getFilled();
        $return['number']       = $this->getNumber();
        $return['value']        = $this->getValue();
        $return['request_date'] = $this->getRequestDate();
        $return['brand']        = $this->getBrand()->toArray();
        $return['order']        = $this->getOrder()->toArray();
        $return['user']         = $this->getUser();
        $return['location']     = $this->getLocation();
        
        return $return;
    }
}