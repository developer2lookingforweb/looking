<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="brands")
 */
class Brands
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $brand;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $image;

    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brand
     *
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set image
     *
     * @param text $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set status
     *
     * @param text $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['brand']    = $this->getBrand();
        $return['image']      = $this->getId();
        $return['status']  = $this->getStatus();

        return $return;
    }
}