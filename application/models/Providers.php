<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="providers")
 */
class Providers
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;


    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $provider;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $url;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $address;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $contact;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set provider
     *
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * Get provider
     *
     * @return string 
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set url
     *
     * @param text $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set address
     *
     * @param text $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
   
    /**
     * Set contact
     *
     * @param integer $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get contact
     *
     * @return sting 
     */
    public function getContact()
    {
        return $this->contact;
    }

    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['provider']    = $this->getProvider();
        $return['url']      = $this->getUrl();
        $return['address']  = $this->getAddress();
        $return['contact'] = $this->getContact();
        
        return $return;
    }
}