<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="banners")
 */
class Banners
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $title;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $image;

    
    /**
     * @Column(type="string", length=1000, nullable=true) 
     */
    private $description;
    
    
    /**
     * @Column(type="string", length=20000, nullable=true) 
     */
    private $has_button;
    
    /**
     * @Column(type="integer", length=100, nullable=true) 
     */
    private $published;

    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $is_video;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $align;

    /**
     * @Column(type="datetime",  nullable=true) 
     */
    private $creation_date;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $link;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
    

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    

    /**
     * Set has_button
     *
     * @param string $hasButton
     */
    public function setHasButton($hasButton)
    {
        $this->has_button = $hasButton;
    }

    /**
     * Get has_button
     *
     * @return string 
     */
    public function getHasButton()
    {
        return $this->has_button;
    }
    

    /**
     * Set published
     *
     * @param string $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return string 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set is_video
     *
     * @param string $isVideo
     */
    public function setIsVideo($isVideo)
    {
        $this->is_video = $isVideo;
    }

    /**
     * Get is_video
     *
     * @return string 
     */
    public function getIsVideo()
    {
        return $this->is_video;
    }
    
    /**
     * Set align
     *
     * @param string $align
     */
    public function setAlign($align)
    {
        $this->align = $align;
    }

    /**
     * Get align
     *
     * @return string 
     */
    public function getAlign()
    {
        return $this->align;
    }
    

    
    /**
     * Set link
     *
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }
    
    
    /**
     * Set creation_date
     *
     * @param string $date
     */
    public function setCreationDate($date)
    {
        $this->creation_date = $date;
    }

    /**
     * Get creation_date
     *
     * @return string 
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }
    
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['title']    = $this->getTitle();
        $return['published']    = $this->getPublished();
        $return['is_video']    = $this->getIsVideo();
        $return['image']    = $this->getImage();
        $return['description']    = $this->getDescription();
        $return['has_button']    = $this->getHasButton();
        $return['creation_date']    = $this->getCreationDate();
        $return['link']    = $this->getLink();
        $return['align']    = $this->getAlign();

        return $return;
    }
}