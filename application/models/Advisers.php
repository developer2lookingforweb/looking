<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 22-Mar-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="advisers")
 */
class Advisers
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $text;

    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $isrange;

    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $minval;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $maxval;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $value;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $step;
    
    
     /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;

    
     /**
     * @ManyToOne(targetEntity="CategoriesConfigs")
     */
    private $categoryConfig;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Advisers
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }
 
    
    /**
     * Set range
     *
     * @param string $range
     * @return Advisers
     */
    public function setRange($range)
    {
        $this->isrange = $range;
    
        return $this;
    }

    /**
     * Get Range
     *
     * @return string 
     */
    public function getRange()
    {
        return $this->isrange;
    }
 
    
    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    
    
    /**
     * Set status
     *
     * @param string $status
     * @return Qestions
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    /**
     * Get min
     *
     * @return integer 
     */
    public function getMin()
    {
        return $this->minval;
    }

    
    
    /**
     * Set min
     *
     * @param string $min
     * @return Advisers
     */
    public function setMin($min)
    {
        $this->minval = $min;
    
        return $this;
    }
    
    
    /**
     * Get max
     *
     * @return string 
     */
    public function getMax()
    {
        return $this->maxval;
    }

    
    
    /**
     * Set max
     *
     * @param string $max
     * @return Advisers
     */
    public function setMax($max)
    {
        $this->maxval = $max;
    
        return $this;
    }
    
    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    
    
    /**
     * Set value
     *
     * @param string $value
     * @return Advisers
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get step
     *
     * @return string 
     */
    public function getStep()
    {
        return $this->step;
    }

    
    
    /**
     * Set step
     *
     * @param string $step
     * @return Qestions
     */
    public function setStep($step)
    {
        $this->step = $step;
    
        return $this;
    }

    /**
     * Set category
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return models\Categories 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set categoryConfig
     *
     * @param models\CategoriesConfigs $categoryConfig
     */
    public function setCategoryConfig(\models\Categories $categoryConfig)
    {
        $this->categoryConfig = $categoryConfig;
    
        return $this;
    }

    /**
     * Get categoryConfig
     *
     * @return models\CategoriesConfigs 
     */
    public function getCategoryConfig()
    {
        return $this->categoryConfig;
    }
 
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['text']     = $this->getText();
        $return['status']     = $this->getStatus();
        $return['range']     = $this->getRange();
        $return['cateogry']     = $this->getCategory()->toArray();
        $return['cateogryConfig']     = $this->getCategoryConfig()->toArray();
        $return['min']     = $this->getMin();
        $return['max']     = $this->getMax();
        $return['value']     = $this->getValue();
        $return['step']     = $this->getStep();
        
        return $return;
    }
}