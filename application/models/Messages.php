<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="messages")
 */
class Messages
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $subject;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;

    
    /**
     * @Column(type="string", length=1000, nullable=true) 
     */
    private $message;
    
    
    /**
     * @Column(type="string", length=20000, nullable=true) 
     */
    private $email;
    
    /**
     * @Column(type="datetime",  nullable=true) 
     */
    private $creation_date;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }
    

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    

    /**
     * Set message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
    

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
        
    
    /**
     * Set creation_date
     *
     * @param string $date
     */
    public function setCreationDate($date)
    {
        $this->creation_date = $date;
    }

    /**
     * Get creation_date
     *
     * @return string 
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }
    
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['subject']    = $this->getSubject();
        $return['name']    = $this->getName();
        $return['email']    = $this->getEmail();
        $return['message']    = $this->getMessage();
        $return['creation_date']    = $this->getCreationDate();

        return $return;
    }
}