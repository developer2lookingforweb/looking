<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="customers_data")
 */
class CustomersData
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;
    
    /**
     * @OneToOne(targetEntity="Customers",  cascade={"all"})
     */
    private $customer;

    /**
     * @Column(type="string", nullable=true)
     */
    private $grade;

    /**
     * @Column(type="string", nullable=true)
     */
    private $school;


    /**
     * @Column(type="text", length=10000, nullable=true)
     */
    private $job;


    /**
     * @Column(type="text", length=10000, nullable=true)
     */
    private $personal_references;


    /**
     * @Column(type="text", length=10000, nullable=true)
     */
    private $assets;

    /**
     * @Column(type="string", nullable=true)
     */
    private $neighborhood;

    /**
     * @Column(type="string", nullable=true)
     */
    private $age;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set customer
     *
     * @param models\Customers $customer
     */
    public function setCustomer(\models\Customers $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Get customer
     *
     * @return models\Customers
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    /**
     * Set grade
     *
     * @param string $grade
     * @return CustomersData
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    
        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }
    
    /**
     * Set school
     *
     * @param string $school
     * @return CustomersData
     */
    public function setSchool($school)
    {
        $this->school = $school;
    
        return $this;
    }

    /**
     * Get school
     *
     * @return string 
     */
    public function getSchool()
    {
        return $this->school;
    }
    
    
    /**
     * Set job
     *
     * @param string $job
     * @return CustomersData
     */
    public function setJob($job)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return string 
     */
    public function getJob()
    {
        return $this->job;
    }
    
    /**
     * Set age
     *
     * @param string $age
     * @return CustomersData
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return string 
     */
    public function getAge()
    {
        return $this->age;
    }
    
    /**
     * Set Neighborhood
     *
     * @param string $neighborhood
     * @return CustomersData
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
    
        return $this;
    }

    /**
     * Get Neighborhood
     *
     * @return string 
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }
    
    
    /**
     * Set references
     *
     * @param string $references
     * @return CustomersData
     */
    public function setPersonalReferences($references)
    {
        $this->personal_references = $references;
    
        return $this;
    }

    /**
     * Get references
     *
     * @return string 
     */
    public function getPersonalReferences()
    {
        return $this->personal_references;
    }
    
    
    /**
     * Set assets
     *
     * @param string $assets
     * @return CustomersData
     */
    public function setAssets($assets)
    {
        $this->assets = $assets;
    
        return $this;
    }

    /**
     * Get assets
     *
     * @return string 
     */
    public function getAssets()
    {
        return $this->assets;
    }
    

    public function toArray($user = true)
    {
        $return = array();
        
        $return['id']               = $this->getId();
        $return['customer']             = $this->getCustomer()->toArray();
        $return['grade']   = $this->getGrade();
        $return['school']   = $this->getSchool();
        $return['age']   = $this->getAge();
        $return['age']   = $this->getNeighborhood();
        $return['job']   = $this->getJob();
        $return['personal_references']   = $this->getPersonalReferences();
        $return['assets']   = $this->getAssets();
        
        return $return;
    }
}