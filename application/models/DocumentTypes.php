<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="document_types")
 */
class DocumentTypes
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $doc_type;
    
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentType
     *
     * @param string $documentType
     */
    public function setDcumentType($documentType)
    {
        $this->doc_type = $documentType;
    }

    /**
     * Get documentType
     *
     * @return string 
     */
    public function getDocumentType()
    {
        return $this->doc_type;
    }


    /**
     * Set status
     *
     * @param text $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['documentType']    = $this->getDocumentType();
        $return['status']  = $this->getStatus();

        return $return;
    }
}