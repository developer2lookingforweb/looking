<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 20-Oct-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="offers")
 */
class Offers
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;
    
    /**
     * @Column(type="string", length=1000, nullable=true) 
     */
    private $description;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $discount;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $filter;

    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $filter_id;

    
    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $status;

    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $end;
     
     /**
     * @ManyToOne(targetEntity="Campaigns")
     */
    private $campaign;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set filter
     *
     * @param string $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Get filter
     *
     * @return string 
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set filterId
     *
     * @param string $filterId
     */
    public function setFilterId($filterId)
    {
        $this->filter_id = $filterId;
    }

    /**
     * Get filterId
     *
     * @return string 
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }
    
    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getstatus()
    {
        return $this->status;
    }

    /**
     * Set discount
     *
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    

    /**
     * Set end
     *
     * @param string $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->end;
    }
    
    /**
     * Set campaign
     *
     * @param models\Campaigns $campaign
     */
    public function setCampaign(\models\Campaigns $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get campaign
     *
     * @return models\Campaigns
     */
    public function getCampaign()
    {
        return $this->campaign;
    }
    
    public function toArray()
    {
        $return = array();
        $return['id']           = $this->getId();
        $return['name']         = $this->getName();
        $return['description']     = $this->getDescription();
        $return['discount']     = $this->getDiscount();
        $return['filter']       = $this->getFilter();
        $return['filterId']       = $this->getFilterId();
        $return['status']       = $this->getstatus();
        $return['end']          = $this->getEnd();
        $return['campaign']     = $this->getCampaign();
        
        return $return;
    }
}