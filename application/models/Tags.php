<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 11-Dec-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="tags")
 */
class Tags
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $tag;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Tags
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['tag']     = $this->getTag();
        
        return $return;
    }
}