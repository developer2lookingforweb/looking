<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 22-Mar-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="options_tags")
 */
class OptionsTags
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    
    /**
     * @ManyToOne(targetEntity="Options")
     */
    private $option;

    
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $status;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $tag;
    
    /**
     * @Column(type="float", length=100, nullable=false) 
     */
    private $min;
    
    /**
     * @Column(type="float", length=100, nullable=false) 
     */
    private $max;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set option
     *
     * @param models\Optopns $option
     */
    public function setOption(\models\Option $option)
    {
        $this->option = $option;
    
        return $this;
    }

    /**
     * Get option
     *
     * @return models\Option 
     */
    public function getOption()
    {
        return $this->option;
    }
 

    
 
    
    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    
    /**
     * Set status
     *
     * @param string $status
     * @return Qestions
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    public function getTag()
    {
        return $this->tag;
    }

    
    /**
     * Set tag
     *
     * @param string $tag
     * @return Options
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    

    public function getMax()
    {
        return $this->max;
    }

    
    /**
     * Set max
     *
     * @param string $max
     * @return Options
     */
    public function setMax($max)
    {
        $this->max = $max;
    
        return $this;
    }

    
    public function getMin()
    {
        return $this->min;
    }

    
    /**
     * Set min
     *
     * @param string $min
     * @return Options
     */
    public function setMin($min)
    {
        $this->min = $min;
    
        return $this;
    }

    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['option']     = $this->getOption()->toArray();
        $return['status']     = $this->getStatus();
        $return['tag']     = $this->getTag();
        $return['max']     = $this->getMax();
        $return['min']     = $this->getMin();
        
        return $return;
    }
}