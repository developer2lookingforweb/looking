<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 29-Ago-2017 10:00:38 a.m.
 * 
 * @Entity
 * @Table(name="suggestions")
 */
class Suggestions
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $image;
    
    /**
     * @ManyToOne(targetEntity="Questions")
     */
    private $question;

    
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $status;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $tag;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $value;
    
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $order;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param models\Questions $question
     */
    public function setQuestion(\models\Questions $question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return models\Questions 
     */
    public function getQuestion()
    {
        return $this->question;
    }
 

    /**
     * Set image
     *
     * @param string $image
     * @return Suggestions
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
 
    
    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    
    /**
     * Set status
     *
     * @param string $status
     * @return Qestions
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    public function getTag()
    {
        return $this->tag;
    }

    
    /**
     * Set tag
     *
     * @param string $tag
     * @return Suggestions
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    

    public function getOrder()
    {
        return $this->order;
    }

    
    /**
     * Set value
     *
     * @param string $order
     * @return Suggestions
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    
    public function getValue()
    {
        return $this->value;
    }

    
    /**
     * Set value
     *
     * @param string $value
     * @return Suggestions
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['question']     = $this->getQuestion()->toArray();
        $return['option']     = $this->getOption();
        $return['status']     = $this->getStatus();
        $return['tag']     = $this->getTag();
        $return['max']     = $this->getMax();
        $return['min']     = $this->getMin();
        
        return $return;
    }
}