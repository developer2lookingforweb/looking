<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="cities")
 */
class Cities
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="States")
     */
    private $state;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $name;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $code;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $lon;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $lat;
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $shipping;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set lon
     *
     * @param string $lon
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set lat
     *
     * @param string $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set state
     *
     * @param models\States $states
     */
    public function setStates(\models\States $states)
    {
        $this->state = $states;
    }

    /**
     * Get state
     *
     * @return models\States
     */
    public function getState()
    {
        return $this->state;
    }
    
    
    /**
     * Set shipping
     *
     * @param string $shipping
     * @return Cities
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    
        return $this;
    }

    /**
     * Get shipping
     *
     * @return string 
     */
    public function getShipping()
    {
        return $this->shipping;
    }
    
    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['name']     = $this->getName();
        $return['code']     = $this->getCode();
        $return['lon']     = $this->getLon();
        $return['lat']     = $this->getLat();
        $return['state']  = $this->getState()->toArray();
        $return['shipping']  = $this->getShipping();
        
        return $return;
    }
}