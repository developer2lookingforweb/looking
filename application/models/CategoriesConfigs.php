<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="categories_configs")
 */
class CategoriesConfigs
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $name;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $is_property;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set categories
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return models\Categories
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set is_property
     *
     * @param string $isProperty
     */
    public function setProperty($isProperty)
    {
        $this->is_property = $isProperty;
    }

    /**
     * Get is_property
     *
     * @return integer
     */
    public function getIsProperty()
    {
        return $this->is_property;
    }

    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['name'] = $this->getName();
        $return['is_property'] = $this->getIsProperty();
         if ($relations){
            $return['category'] = $this->getCateogry()->toArray();
        }
        return $return;
    }
}