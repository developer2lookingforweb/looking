<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="prospects")
 */
class Prospects
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $last_name;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $name;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $email;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $address;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $cell_phone;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set last_name
     *
     * @param string $last_name
     * @return Prospects
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    
        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Prospects
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set email
     *
     * @param string $email
     * @return Prospects
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set address
     *
     * @param string $address
     * @return Prospects
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set cell_phone
     *
     * @param string $cell_phone
     * @return Prospects
     */
    public function setCellPhone($cell_phone)
    {
        $this->cell_phone = $cell_phone;
    
        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return string 
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['name']     = $this->getName();
        $return['last_name']     = $this->getLastName();
        $return['email']     = $this->getEmail();
        $return['address']     = $this->getAddress();
        $return['cell_phone']     = $this->getCellPhone();
        
        return $return;
    }
}