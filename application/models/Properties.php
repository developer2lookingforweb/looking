<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="properties")
 */
class Properties
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $property;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $value;

    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $show_property;

    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $is_evaluable;

    /**
     * @Column(type="float", length=1, nullable=true) 
     */
    private $rank;

    
    /**
     * @ManyToOne(targetEntity="Products")
     */
    private $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set property
     *
     * @param string $property
     * @return Properties
     */
    public function setProperty($property)
    {
        $this->property = $property;
    
        return $this;
    }

    /**
     * Get property
     *
     * @return string 
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Properties
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set is_evlauable
     *
     * @param string $evaluable
     * @return Properties
     */
    public function setIsEvaluable($evaluable)
    {
        $this->is_evaluable = $evaluable;
    
        return $this;
    }

    /**
     * Get is_evaluable
     *
     * @return string 
     */
    public function getIsEvaluable()
    {
        return $this->is_evaluable;
    }

    /**
     * Set rank
     *
     * @param string $rank
     * @return Properties
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    
        return $this;
    }

    /**
     * Get rank
     *
     * @return string 
     */
    public function getRank()
    {
        return $this->rank;
    }

     
    /**
     * Set show_property
     *
     * @param string $showProperty
     * @return Properties
     */
    public function setShowProperty($showProperty)
    {
        $this->show_property = $showProperty;
    
        return $this;
    }

    /**
     * Get show_property
     *
     * @return string 
     */
    public function getShowProperty()
    {
        return $this->show_property;
    }

     
    /**
     * Set product
     *
     * @param models\Products $product
     */
    public function setProduct(\models\Products $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return models\Products
     */
    public function getProduct()
    {
        return $this->product;
    }
   

    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['product']     = $this->getProduct();
        $return['property']     = $this->getProperty();
        $return['value']     = $this->getValue();
        $return['show_property']     = $this->getShowProperty();
        $return['is_evaluable']     = $this->getIsEvaluable();
        $return['rank']     = $this->getRank();
        
        return $return;
    }
}