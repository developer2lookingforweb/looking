<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="blogs_comments")
 */
class BlogsComments
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

     /**
     * @ManyToOne(targetEntity="Blogs")
     */
    private $blog;

    /**
     * @Column(type="string", length=10000, nullable=true) 
     */
    private $content;
    
    /**
     * @Column(type="datetime",  nullable=true) 
     */
    private $creation_date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set blog
     *
     * @param models\Blogs $blog
     */
    public function setBlog(\models\Blogs $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Get blog
     *
     * @return models\Blogs
     */
    public function getBlog()
    {
        return $this->blog;
    }
    

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    

    /**
     * Set creation_date
     *
     * @param string $date
     */
    public function setCreationDate($date)
    {
        $this->creation_date = $date;
    }

    /**
     * Get creation_date
     *
     * @return string 
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }
    
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['blog']    = $this->getBlog();
        $return['content']    = $this->getContent();
        $return['creation_date']    = $this->getCreationDate();

        return $return;
    }
}