<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="products")
 */
class Products
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $product;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $reference;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $outstanding;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;

    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $stock;

    /**
     * @Column(type="text", length=10000, nullable=false) 
     */
    private $description;

    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $cost;
        

    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $alternative_cost;
        
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $price;
    
        
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $market_price;
    
    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $tax;
    
    /**
     * @ManyToOne(targetEntity="Brands")
     */
    private $brand;
    
    
    /**
     * @ManyToOne(targetEntity="PaymentMethods")
     */
    private $payment_method;
    
    /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;
    /**
     * @Column(type="string", length=1000, nullable=false) 
     */
    private $image;

            
    /**
     * @Column(type="date",  nullable=false) 
     */
    private $last_update;

            
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param string $product
     * @return Products
     */
    public function setProduct($product)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return string 
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * Set image
     *
     * @param string $image
     * @return Products
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set stock
     *
     * @param string $stock
     * @return Products
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    
        return $this;
    }

    /**
     * Get stock
     *
     * @return string 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set outstanding
     *
     * @param string $outstanding
     * @return Products
     */
    public function setOutstanding($outstanding)
    {
        $this->outstanding = $outstanding;
    
        return $this;
    }

    /**
     * Get outstanding
     *
     * @return string 
     */
    public function getOutstanding()
    {
        return $this->outstanding;
    }

    /**
     * Set tax
     *
     * @param string $tax
     * @return Products
     */
    public function setTax($tax)
    {
        $this->tax= $tax;
    
        return $this;
    }

    /**
     * Get tax
     *
     * @return string 
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Products
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Products
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set cost
     *
     * @param string $cost
     * @return Products
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return string 
     */
    public function getCost()
    {
        return $this->cost;
    }
 
    
    /**
     * Set price
     *
     * @param string $price
     * @return Products
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
 
    
    /**
     * Set reference
     *
     * @param string $reference
     * @return Products
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }
 
    
    /**
     * Set alternative_cost
     *
     * @param string $alternativeCost
     * @return Products
     */
    public function setAlternativeCost($alternativeCost)
    {
        $this->alternative_cost = $alternativeCost;
    
        return $this;
    }

    /**
     * Get alternative_cost
     *
     * @return string 
     */
    public function getAlternativeCost()
    {
        return $this->alternative_cost;
    }
 
    
    /**
     * Set market_price
     *
     * @param string $market_price
     * @return Products
     */
    public function setMarketPrice($market_price)
    {
        $this->market_price = $market_price;
    
        return $this;
    }

    /**
     * Get market_price
     *
     * @return string 
     */
    public function getMarketPrice()
    {
        return $this->market_price;
    }
 
    
    /**
     * Set brand
     *
     * @param models\Brands $brand
     */
    public function setBrand(\models\Brands $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Get brand
     *
     * @return models\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }
    
    
    /**
     * Set payment_method
     *
     * @param models\PaymentMethods $method
     */
    public function setPaymentMethod(\models\PaymentMethods $method)
    {
        $this->payment_method = $method;
    }

    /**
     * Get payment_method
     *
     * @return models\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }
    
    
    /**
     * Set last_update
     *
     * @param string $date
     */
    public function setLastUpdate($date)
    {
        $this->last_update = $date;
    }

    /**
     * Get last_update
     *
     * @return string
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }
    

    /**
     * Set category
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return models\Categories
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['product']     = $this->getProduct();
        $return['description']     = $this->getDescription();
        $return['cost']     = $this->getCost();
        $return['price']     = $this->getPrice();
        $return['reference']     = $this->getReference();
        $return['outstanding']     = $this->getOutstanding();
        $return['alterantive_cost']     = $this->getAlternativeCost();
        $return['image']     = $this->getImage();
        $return['status']     = $this->getStatus();
        $return['tax']     = $this->getTax();
        $return['stock']     = $this->getStock();
        $return['market_price']     = $this->getMarketPrice();
        $return['last_update']     = $this->getLastUpdate();
        
         if ($relations){
            $return['brand'] = $this->getBrand()->toArray();
            $return['category'] = $this->getCategory()->toArray();
            $return['payment_method'] = $this->getPaymentMethod()->toArray();
        }
        return $return;
    }
}