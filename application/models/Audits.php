<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="audits")
 */
class Audits
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="integer", length=10, nullable=true) 
     */
    private $search_user;
    
    /**
     * @Column(type="integer", length=10, nullable=true) 
     */
    private $logged_user;

    
    /**
     * @Column(type="integer", length=10, nullable=true) 
     */
    private $is_logged;
    
    
    /**
     * @Column(type="integer", length=10, nullable=true) 
     */
    private $buyer_user;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $search_options;

    /**
     * @Column(type="datetime",  nullable=true) 
     */
    private $date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set search_user
     *
     * @param string $searchUser
     */
    public function setSearchUser($searchUser)
    {
        $this->search_user = $searchUser;
    }

    /**
     * Get search_user
     *
     * @return string 
     */
    public function getSearchUser()
    {
        return $this->search_user;
    }
    
    /**
     * Set logged_user
     *
     * @param string $loggedUser
     */
    public function setLoggedUser($loggedUser)
    {
        $this->logged_user = $loggedUser;
    }

    /**
     * Get logged_user
     *
     * @return string 
     */
    public function getLoggedUser()
    {
        return $this->logged_user;
    }
    
    /**
     * Set buyer_user
     *
     * @param string $buyerUser
     */
    public function setBuyerUser($buyerUser)
    {
        $this->buyer_user = $buyerUser;
    }

    /**
     * Get buyer_user
     *
     * @return string 
     */
    public function getBuyerUser()
    {
        return $this->buyer_user;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set is_logged
     *
     * @param text $isLogged
     */
    public function setIsLogged($isLogged)
    {
        $this->is_logged = $isLogged;
    }

    /**
     * Get is_logged
     *
     * @return string 
     */
    public function getIsLogged()
    {
        return $this->is_logged;
    }
   

    /**
     * Set search_options
     *
     * @param text $searchOptions
     */
    public function setSearchOptions($searchOptions)
    {
        $this->search_options = $searchOptions;
    }

    /**
     * Get search_options
     *
     * @return string 
     */
    public function getSearchOptions()
    {
        return $this->search_options;
    }
   
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['search_user']    = $this->getSearchUser();
        $return['logged_user']    = $this->getLoggedUser();
        $return['buyer_user']    = $this->getBuyerUser();
        $return['is_logged']    = $this->getIsLogged();
        $return['search_options']    = $this->getSearchOptions();
        $return['date']    = $this->getDate();

        return $return;
    }
}