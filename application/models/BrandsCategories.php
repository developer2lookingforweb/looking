<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2017 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="brands_categories")
 */
class BrandsCategories
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Brands")
     */
    private $brand;
    
    /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set brand
     *
     * @param models\Brands $brand
     */
    public function setBrand(\models\Brands $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Get brand
     *
     * @return models\Brands
     */
    public function getBrand()
    {
        return $this->brand;
    }
    

    /**
     * Set categories
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return models\Categories
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
         if ($relations){
            $return['brand'] = $this->getBrand()->toArray();
            $return['category'] = $this->getCateogry()->toArray();
        }
        return $return;
    }
}