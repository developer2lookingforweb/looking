<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity(repositoryClass="Repositories\UsersRepository")
 * @Table(name="customers")
 */
class Customers
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Cities")
     */
    private $city;
    
    /**
     * @ManyToOne(targetEntity="DocumentTypes")
     */
    private $doc_type;

    /**
     * @ManyToOne(targetEntity="Campaigns")
     */
    private $campaign;

    /**
    * @Column(type="string", length=50, nullable=true) 
    */
    private $dni;
     
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $name;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $last_name;

    /**
     * @Column(type="string", unique=true, length=100, nullable=false) 
     */
    private $email;

    /**
     * @Column(type="datetime", nullable=true) 
     */
    private $last_access;

    
     /**
     * @Column(type="string", length=50, nullable=true) 
     */
     private $mobile;
     
     /**
     * @Column(type="string", length=200, nullable=true) 
     */
     private $phone;
     
     /**
     * @Column(type="string", length=50, nullable=true) 
     */
     private $address;
     
    /**
     * @Column(type="string",  length=100, nullable=true) 
     */
     private $password;
     
    /**
     * @Column(type="integer",  length=1, nullable=true) 
     */
     private $mail_marketing;
    /**
     * @Column(type="string",  length=100, nullable=true) 
     */
     private $facebook_id;
    
     /**
     * @Column(type="date", nullable=true) 
     */
     private $creationDate;
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set last_name
     *
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set last_access
     *
     * @param integer $last_access
     */
    public function setLastAccess($last_access)
    {
        $this->last_access = $last_access;
    }

    /**
     * Get last_access
     *
     * @return integer 
     */
    public function getLastAccess()
    {
        return $this->last_access;
    }

   

    /**
     * Set city
     *
     * @param models\Cities $city
     */
    public function setCity(\models\Cities $city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return models\Cities 
     */
    public function getCity()
    {
        return $this->city;
    }

   
    /**
     * Set docType
     *
     * @param models\DocumentTypes $documentType
     */
    public function setDocType(\models\DocumentTypes $documentType)
    {
        $this->doc_type = $documentType;
    }

    /**
     * Get docType
     *
     * @return models\DocumentTypes
     */
    public function getDocType()
    {
        return $this->doc_type;
    }
   
    /**
     * Set campaign
     *
     * @param models\Campaigns $campaign
     */
    public function setCampaign(\models\Campaigns $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get campaign
     *
     * @return models\Campaigns
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set dni
     *
     * @param string $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }


    
    /**
     * Set mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }
    
    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)     {
        $this->phone = $phone;
    }
    
    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()     {
        return $this->phone;
    }
    
    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)     {
        $this->address = $address;
    }
    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()     {
        return $this->address;
    }
    
    
    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Set maiil_marketing
     *
     * @param string $marketing
     */
    public function setMailMarketing($marketing)
    {
        $this->mail_marketing = $marketing;
    }

    /**
     * Get mail_marketing
     *
     * @return string 
     */
    public function getMailMarketing()
    {
        return $this->mail_marketing;
    }

    
    
    /**
     * Set facebook_id
     *
     * @param string $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;
    }

    /**
     * Get facebook-id
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    
    /**
     * Set creationDate
     *
     * @param string $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * Get creationDate
     *
     * @return string 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    public function toArray()
    {
        $return = array();
        $return['id']           = $this->getId();
        $return['name']         = $this->getName();
        $return['last_name']    = $this->getLastName();
        $return['email']        = $this->getEmail();
        $return['phone']     = $this->getPhone();
        $return['mobile'] = $this->getMobile();
        $return['address']        = $this->getAddress();
        $return['city']         = $this->getCity()->toArray();
        $return['docType']         = $this->getDocType()->toArray();
        $return['dni']         = $this->getDni();
        $return['password']         = $this->getPassword();
        $return['mail_marketing']         = $this->getMailMarketing();
        $return['facebook_id']         = $this->getFacebookId();
        $return['campaign']         = (is_object($this->getCampaign()))?$this->getCampaign()->toArray():"";
        $return['creationDate'] = "";
        
        if (is_null($this->getCreationDate()) == false){
            $return['creationDate']    = $this->getCreationDate()->format("Y-m-d");
        }
        
        return $return;
    }
}