<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="favorites")
 */
class Favorites
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Products")
     */
    private $product;
    
    /**
     * @ManyToOne(targetEntity="Customers")
     */
    private $customer;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set product
     *
     * @param models\Products $product
     */
    public function setProduct(\models\Products $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return models\Products
     */
    public function getProduct()
    {
        return $this->product;
    }
    

    /**
     * Set customer
     *
     * @param models\Customers $customers
     */
    public function setCustomer(\models\Customers $customers)
    {
        $this->customer = $customers;
    }

    /**
     * Get customer
     *
     * @return models\Customers
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['product']     = $this->getProduct();
        $return['customer']     = $this->getCustomer();
        return $return;
    }
}