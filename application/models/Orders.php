<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="orders")
 */
class Orders
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="datetime", nullable=true) 
     */
    private $order_date;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $reference;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $response_pol;

    /**
     * @Column(type="integer", length=100, nullable=true) 
     */
    private $invoice;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $order_reference;

    /**
     * @ManyToOne(targetEntity="OrdersStatus")
     */
    private $status;


    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $value;
    
    /**
     * @Column(type="integer", length=100, nullable=true) 
     */
    private $promissory_note;
        
    
    /**
     * @Column(type="integer", length=1, nullable=true) 
     */
    private $shipping;
        
    /**
     * @ManyToOne(targetEntity="PaymentMethods")
     */
     private $payment_method;

     /**
     * @ManyToOne(targetEntity="Customers")
     */
     private $customer;
    
     /**
     * @ManyToOne(targetEntity="Offers")
     */
    private $offer;
    
     /**
     * @ManyToOne(targetEntity="Cupons")
     */
    private $cupon;
    
   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoie
     *
     * @param string $invoice
     * @return Orders
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    
        return $this;
    }

    /**
     * Get invoice
     *
     * @return string 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set order
     *
     * @param string $order
     * @return Orders
     */
    public function setOrder($order)
    {
        $this->order_reference = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return string 
     */
    public function getOrder()
    {
        return $this->order_reference;
    }
    
    /**
     * Set order_date
     *
     * @param string $orderDate
     * @return Orders
     */
    public function setOrderDate($orderDate)
    {
        $this->order_date = $orderDate;
    
        return $this;
    }

    /**
     * Get order_date
     *
     * @return string 
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Products
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }
    

    /**
     * Set response_pol
     *
     * @param string $response_pol
     * @return Products
     */
    public function setResponsePol($response_pol)
    {
        $this->response_pol = $response_pol;
    
        return $this;
    }

    /**
     * Get response_pol
     *
     * @return string 
     */
    public function getResponsePol()
    {
        return $this->response_pol;
    }
    
    /**
     * Set value
     *
     * @param string $value
     * @return Products
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
 
    
    /**
     * Set promissory_note
     *
     * @param string $promissoryNote
     * @return Products
     */
    public function setPromissoryNote($promissoryNote)
    {
        $this->promissory_note = $promissoryNote;
    
        return $this;
    }

    /**
     * Get promissory_note
     *
     * @return string 
     */
    public function getPromissoryNote()
    {
        return $this->promissory_note;
    }
 
    
 
    
    /**
     * Set shipping
     *
     * @param string $shipping
     * @return Products
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    
        return $this;
    }

    /**
     * Get shipping
     *
     * @return string 
     */
    public function getShipping()
    {
        return $this->shipping;
    }
 
    
    /**
     * Set status
     *
     * @param \models\OrdersStatus $status
     * @return Orders
     */
    public function setStatus(\models\OrdersStatus $status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return \models\OrdersStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    
    /**
     * Set payment_method
     *
     * @param \models\PaymentMethods $paymentMethod
     * @return Orders
     */
    public function setPaymentMethod(\models\PaymentMethods $paymentMethod = null) {
            $this->payment_method = $paymentMethod;

            return $this;
    }

    /**
     * Get payment_method
     *
     * @return \models\PaymentMethods
     */
    public function getPaymentMethod() {
            return $this->payment_method;
    }

    /**
     * Set customer
     *
     * @param models\Customers $customer
     */
    public function setCustomer(\models\Customers $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Get customer
     *
     * @return models\Customers
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
     /**
     * Set offer
     *
     * @param models\Offers $offer
     */
    public function setOffer(\models\Offers $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get offer
     *
     * @return models\Offers
     */
    public function getOffer()
    {
        return $this->offer;
    }
     /**
     * Set cupon
     *
     * @param models\Offers $cupon
     */
    public function setCupon(\models\Cupons $cupon)
    {
        $this->cupon = $cupon;
    }

    /**
     * Get cupon
     *
     * @return models\Cupons
     */
    public function getCupon()
    {
        return $this->cupon;
    }
    
    public function toArray($relations = true)
    {
        

        
        $return = array();
        $return['id']       = $this->getId();
        $return['invoice']     = $this->getInvoice();
        $return['order_reference']     = $this->getOrder();
        $return['order_date']     = $this->getOrderDate();
        $return['reference']     = $this->getReference();
        $return['promissory_note']     = $this->getPromissoryNote();
        $return['shipping']     = $this->getShipping();
        $return['response_pol']     = $this->getResponsePol();
        $return['value']     = $this->getValue();
        
         if ($relations){
            $return['status']     = $this->getStatus()->toArray();
            $return['customer'] = $this->getCustomer()->toArray();
            $return['payment_method'] = $this->getPaymentMethod()->toArray();
            $return['offer'] = (is_object($this->getOffer()))?$this->getOffer()->toArray():"";
            $return['cupon'] = (is_object($this->getCupon()))?$this->getCupon()->toArray():"";
        }
        return $return;
    }
}