<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 05-June-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="blogs")
 */
class Blogs
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $title;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $image;

    
    /**
     * @Column(type="string", length=1000, nullable=true) 
     */
    private $description;
    
    
    /**
     * @Column(type="string", length=20000, nullable=true) 
     */
    private $content;
    
    /**
     * @Column(type="integer", length=100, nullable=true) 
     */
    private $published;

    /**
     * @Column(type="datetime",  nullable=true) 
     */
    private $creation_date;

    /**
     * @Column(type="integer",  nullable=true) 
     */
    private $created_by;
    
    /**
     * @Column(type="string",  nullable=true) 
     */
    private $alias;

    /**
     * @Column(type="integer",  nullable=true) 
     */
    private $likes;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
    

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    

    /**
     * Set published
     *
     * @param string $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return string 
     */
    public function getPublished()
    {
        return $this->published;
    }
    

    /**
     * Set alias
     *
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }
    

    /**
     * Set likes
     *
     * @param string $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * Get likes
     *
     * @return string 
     */
    public function getLikes()
    {
        return $this->likes;
    }
    
    /**
     * Set created_by
     *
     * @param string $user
     */
    public function setCreatedBy($user)
    {
        $this->created_by = $user;
    }

    /**
     * Get createrd_by
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }
    
    
    /**
     * Set creation_date
     *
     * @param string $date
     */
    public function setCreationDate($date)
    {
        $this->creation_date = $date;
    }

    /**
     * Get creation_date
     *
     * @return string 
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }
    
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['title']    = $this->getTitle();
        $return['alias']    = $this->getAlias();
        $return['published']    = $this->getPublished();
        $return['image']    = $this->getImage();
        $return['description']    = $this->getDescription();
        $return['content']    = $this->getContent();
        $return['likes']    = $this->getLikes();
        $return['creation_date']    = $this->getCreationDate();
        $return['created_by']    = $this->getCreatedBy();

        return $return;
    }
}