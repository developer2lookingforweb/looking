<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="payment_methods")
 */
class PaymentMethods
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $method;
    
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method
     *
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * Get method
     *
     * @return string 
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * Set status
     *
     * @param text $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
   
    
    public function toArray($country = true)
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['method']    = $this->getMethod();
        $return['status']  = $this->getStatus();

        return $return;
    }
}