<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="orders_fees")
 */
class OrdersFees
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Orders")
     */
    private $order;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $value;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $paid;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $status;
    

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $response;
    

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $reference;
    

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $history;
    
    /**
     * @Column(type="date",  nullable=true) 
     */
    private $payment_date;
    
    
    /**
     * @Column(type="date",  nullable=true) 
     */
    private $transaction_date;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set order
     *
     * @param models\Order $order
     */
    public function setOrder(\models\Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Get order
     *
     * @return models\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }
    
 
    /**
     * Set value
     *
     * @param string $value
     * @return ProductsOrders
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    
 
    /**
     * Set paid
     *
     * @param string $paid
     * @return ProductsOrders
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    
        return $this;
    }

    /**
     * Get paid
     *
     * @return string 
     */
    public function getPaid()
    {
        return $this->paid;
    }
    
    
    /**
     * Set status
     *
     * @param string $status
     * @return ProductsOrders
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    /**
     * Set response
     *
     * @param string $response
     * @return ProductsOrders
     */
    public function setResponse($response)
    {
        $this->response = $response;
    
        return $this;
    }

    /**
     * Get response
     *
     * @return string 
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * Set history
     *
     * @param string $history
     * @return ProductsOrders
     */
    public function setHistory($history)
    {
        $this->history = $history;
    
        return $this;
    }

    /**
     * Get history
     *
     * @return string 
     */
    public function getHistory()
    {
        return $this->history;
    }
    
    
 
    /**
     * Set reference
     *
     * @param string $reference
     * @return ProductsOrders
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }
    
    
    
    /**
     * Set payment_date
     *
     * @param string $date
     * @return ProductsFees
     */
    public function setPaymentDate($date)
    {
        $this->payment_date = $date;
    
        return $this;
    }

    /**
     * Get payment_date
     *
     * @return string 
     */
    public function getPaymentDate()
    {
        return $this->payment_date;
    }
    
    
    /**
     * Set transaction_date
     *
     * @param string $date
     * @return ProductsFees
     */
    public function setTransactionDate($date)
    {
        $this->transaction_date = $date;
    
        return $this;
    }

    /**
     * Get transaction_date
     *
     * @return string 
     */
    public function getTransactionDate()
    {
        return $this->transaction_date;
    }
    
    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['value']     = $this->getValue();
        $return['paid']     = $this->getPaid();
        $return['payment_date']     = $this->getPaymentDate();
        $return['transaction_date']     = $this->getTransactionDate();
        $return['status']     = $this->getStatus();
        $return['response']     = $this->getResponse();
        $return['history']     = $this->getHistory();
        $return['reference']     = $this->getReference();
        
         if ($relations){
            $return['order'] = $this->getOrder()->toArray();
        }
        return $return;
    }
}