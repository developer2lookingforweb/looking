<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="categories")
 */
class Categories
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $category;

    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $image;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $color;
    
    /**
     * @Column(type="string", length=2000, nullable=true) 
     */
    private $meta;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $status;

    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $home;

    /**
     * @Column(type="integer", length=100, nullable=false) 
     */
    private $position;

    /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $parent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Categories
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Categories
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Categories
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set meta$meta
     *
     * @param string $meta
     * @return Categories
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    
        return $this;
    }

    /**
     * Get meta
     *
     * @return string 
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Categories
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    

    /**
     * Set home
     *
     * @param string $home
     * @return Categories
     */
    public function setHome($home)
    {
        $this->home = $home;
    
        return $this;
    }

    /**
     * Get home
     *
     * @return string 
     */
    public function getHome()
    {
        return $this->home;
    }
    

    /**
     * Set order
     *
     * @param string $order
     * @return Categories
     */
    public function setOrder($order)
    {
        $this->position = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return string 
     */
    public function getOrder()
    {
        return $this->position;
    }
    
    
    /**
     * Set parent
     *
     * @param models\Categories $parent
     */
    public function setParent(\models\Categories $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return models\Categories
     */
    public function getParent()
    {
        return $this->parent;
    }
    

    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['category']     = $this->getCategory();
        $return['image']     = $this->getImage();
        $return['color']     = $this->getColor();
        $return['meta']     = $this->getMeta();
        $return['status']     = $this->getStatus();
        $return['home']     = $this->getHome();
        $return['order']     = $this->getOrder();
        $return['parent']     = $this->getParent();
        
        return $return;
    }
}