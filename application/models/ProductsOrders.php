<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author John Jimenez
 * @version 1.0
 * @created 06-Jun-2016 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="products_orders")
 */
class ProductsOrders
{
    
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Products")
     */
    private $product;
    
    /**
     * @ManyToOne(targetEntity="Orders")
     */
    private $order;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $quantity;
    
    /**
     * @Column(type="integer", length=1, nullable=false) 
     */
    private $tax;
    
    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $value;
    
    /**
     * @Column(type="string", length=100, nullable=true) 
     */
    private $detail;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set product
     *
     * @param models\Products $product
     */
    public function setProduct(\models\Products $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return models\Products
     */
    public function getProduct()
    {
        return $this->product;
    }
    

    /**
     * Set order
     *
     * @param models\Orders $order
     */
    public function setOrder(\models\Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Get order
     *
     * @return models\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Set quantity
     *
     * @param string $quantity
     * @return ProductsOrders
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    
    /**
     * Set value
     *
     * @param string $value
     * @return ProductsOrders
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    
    
    
    /**
     * Set detail
     *
     * @param string $detail
     * @return ProductsOrders
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    
        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }
    
    /**
     * Set tax
     *
     * @param string $tax
     * @return Products
     */
    public function setTax($tax)
    {
        $this->tax= $tax;
    
        return $this;
    }

    /**
     * Get tax
     *
     * @return string 
     */
    public function getTax()
    {
        return $this->tax;
    }

    
    public function toArray($relations = true)
    {
        $return = array();
        $return['id']     = $this->getId();
        $return['quantity']     = $this->getQuantity();
        $return['value']     = $this->getValue();
        $return['detail']     = $this->getDetail();
        $return['tax']     = $this->getTax();
        
         if ($relations){
            $return['brand'] = $this->getProduct()->toArray();
            $return['category'] = $this->getOrder()->toArray();
        }
        return $return;
    }
}