<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Heyward Jimenez
 * @version 1.0
 * @created 23-Ene-2012 02:39:38 p.m.
 * 
 * @Entity
 * @Table(name="subcategories")
 */
class Subcategories
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO") 
     */
    private $id;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $subcategory;

    /**
     * @Column(type="string", length=100, nullable=false) 
     */
    private $status;

    /**
     * @ManyToOne(targetEntity="Categories")
     */
    private $category;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subcategory
     *
     * @param string $subcategory
     * @return Subcategories
     */
    public function setSubcategory($subcategory)
    {
        $this->subcategory = $subcategory;
    
        return $this;
    }

    /**
     * Get subcategory
     *
     * @return string 
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Subcategories
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set category
     *
     * @param models\Categories $category
     */
    public function setCategory(\models\Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return models\Categories 
     */
    public function getCategory()
    {
        return $this->category;
    }
    

    public function toArray()
    {
        $return = array();
        $return['id']       = $this->getId();
        $return['subcategory']     = $this->getSubcategory();
        $return['status']     = $this->getStatus();
        $return['category']     = $this->getCategory()->toArray();
        
        return $return;
    }
}