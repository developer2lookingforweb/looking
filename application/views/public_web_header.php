<?php if ($this->session->userdata(AuthConstants::NAMES)) { ?>
    <header id="header" style="border-bottom: solid 1px rgba(0, 0, 0, 0.25); " >
        <nav class="left">
            <a id="imenu" href="#menu" class="hidden-md hidden-lg"> <img class='img-responsive lin_mhb' src="<?php echo base_url(); ?>images/imenu.png" alt="" style="width: 80%;display: block;"/> </a>
            <a href="#" class="logop"> <img class='img-responsive' src="<?php echo base_url(); ?>images/logo1.png" alt="" /> </a>
        </nav>
        <nav class="right"  >
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right "   >
                    <li class="icono_barrab opc_destacados" ><a  id='opc_destacados' href="#" >  <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/destacados.svg" alt="" /><p class='tit_14_2 '  >Destacados</p> </a> </li>
                    <li class="icono_barrab" ><a  class="opc_favoritos" id='opc_favoritos' href="#" >  <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/favoritos.svg" alt="" /><p class='tit_14_2'  >Favoritos</p>  </a></li>
                    <li class=""  >
                        <div class="right-inner-addon">
                            <i id="opc_buscar_ico" class="fa fa-search" aria-hidden="true"></i> 
                            <input type="search"
                                   class="form-control input_lin2" 
                                   placeholder="Buscar lo que quieras..." 
                                   id='opc_buscar'
                                   />
                        </div>
                    </li>
                    <li class="icono_barrab" >
                        <a id='opc_carrito' href="#" >  <img class='img-responsive' src="<?php echo base_url(); ?>images/carrito.png" alt="" /> </a>
                        <span class="badge"><?php echo count($this->session->userdata(AuthConstants::CART)) ?></span>
                    </li>
                    <li class="icono_barra" ><img class='img-responsive' src="<?php echo base_url(); ?>images/avatar.png" alt="" /></li>
                    <li class="dropdown"   >
                        <a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img class='img-responsive' src="<?php echo base_url(); ?>images/arrowdown.svg" alt="" />
                        </a>
                        <ul class="dropdown-menu"  >
                            <li>
                                <a id='opc_perfil' href="#"> 
                                    <h1 class='tit_14' >
                                        <?php echo $this->session->userdata(AuthConstants::NAMES) . " " . $this->session->userdata(AuthConstants::LAST_NAMES) ?>    
                                        <br /> <p class='tit_14_1' ><?php echo $this->session->userdata(AuthConstants::EMAIL) ?></p> </h1>	
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a id='opc_salir' href="<?php echo base_url("login/webLogout") ?>"><h1 class='tit_14' > CERRAR SESION </h1></a></li>
                        </ul>
                    </li>
                </ul>
            </div>	
        </nav>
    </header>

<?php } else { ?>
    <header id="header">
        <nav class="left">
            <a href="#menu" class="hidden-md hidden-lg"> <img class='img-responsive lin_mhb' src="<?php echo base_url(); ?>images/imenu.png" alt="" /> </a>
            <div class="right-inner-addon hidden-xs">
                <i id="opc_buscar_ico" class="fa fa-search" aria-hidden="true"></i> 
                <input type="search"
                       class="form-control input_lin2" 
                       placeholder="Buscar lo que quieras..." 
                       id='opc_buscar_off'
                       />
            </div>
        </nav>
        <a href="<?php echo base_url(); ?>" class="logo"> <img class='img-responsive' src="<?php echo base_url(); ?>images/logo1.png" alt="" /> </a>
        <nav class="right">
            <ul>
            <li class="icono_barrab" >
            <a href="<?php echo base_url("looking/start") ?>" class="button btn_tipo1 hvr-float-shadow">EMPIEZA AHORA</a>
            </li>
            <li class="icono_barrab" >
            <a href="<?php echo base_url("looking/login") ?>" class="button btn_blancop hvr-pulse-shrink">INGRESAR</a>
            </li>
            <li class="icono_barrab" >
            <!--<a href="<?php echo base_url("navidad") ?>" class="button btn_tipo1 hvr-float-shadow">REGISTRATE Y GANA</a>-->
                <a id='opc_carrito' href="#" >  <img class='img-responsive' src="<?php echo base_url(); ?>images/carrito.png" alt="" /> </a>
                <span class="badge"><?php echo count($this->session->userdata(AuthConstants::CART)) ?></span>
            </li>
            </ul>
        </nav>
<!--        <ul class="nav navbar-nav right">
            <li class="icono_barrab opc_destacados" ><a  id='opc_destacados' href="#" >  <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/destacados.svg" alt="" /><p class='tit_14_2 '  >Destacados</p> </a> </li>
            <li class="right"  >
                <div class="right-inner-addon">
                    <i id="opc_buscar_ico" class="fa fa-search" aria-hidden="true"></i> 
                    <input type="search"
                           class="form-control input_lin2" 
                           placeholder="Buscar lo que quieras..." 
                           id='opc_buscar'
                           />
                </div>
            </li>
        </ul>-->
    </header>

<?php
}?>