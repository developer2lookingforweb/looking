<div id="body" class="row">
    <div id="container-menu" class="hide-for-small medium-3 large-2 columns">
         <?=$menu?>
    </div>
    <div id="content" class="small-12 medium-9 large-10 columns">
        <div class="wrapper">
            <div class="row">
                <div class="large-12 columns">
                    <span class="title"><?=$title?></span>
                    <?php if(isset($subtitle)): ?>
                        <span class="sub-title"><?=$subtitle?></span>
                    <?php endif; ?>
                     <?php foreach ($actions as $label => $url):?>
                        <a href="<?=$url?>" class="actions">
                            <?php $exp = explode("_", $label);?>
                            <i class="icon-2x icon-<?=$exp[0]?>"></i> <?=lang($label)?>
                        </a>
                    <?php endforeach;?>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <?php if(isset($breadcrumb)): ?>
                        <?=$breadcrumb?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <?=$sections?>
            </div>
        </div>
    </div>
</div>

<!-- DIALOGOS -->
<div id="confirmation_delete" title="<?=lang("confirmation_delete")?>">
    <div id="message_delete"></div>
</div>