    <?php
$categories = $this->session->userdata(AuthConstants::CATEGORIES);
?>
<?php if(!$this->session->userdata(AuthConstants::CART)){
    $cartCount =0;
}else{
    $cartCount =count($this->session->userdata(AuthConstants::CART));;
    
}
?>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars fa-2x"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/alofijosmall.png" alt="Logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <form class="navbar-form search-form" role="search" action="<?php echo base_url("browse"); ?>">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-xs hidden-sm">
                        
                        <div class="input-group stylish-input-group">
                            <input id="opc_buscar_off" type="text" class="form-control search-box"  placeholder="Busca..." >
                            <span class="input-group-addon">
                                <button type="submit" id="opc_buscar_off_b" class="opc_buscar_off_b">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </li>
                    <li class="hidden-md hidden-lg">
                        <a id='opc_perfil' href="#"> 
                                <?php echo $this->session->userdata(AuthConstants::NAMES) . " " . $this->session->userdata(AuthConstants::LAST_NAMES) ?>    
                                <br /><?php echo $this->session->userdata(AuthConstants::EMAIL) ?>
                        </a>
                    </li >
<?php if ($this->session->userdata(AuthConstants::EMAIL)) { ?>
                    <li class="">
                       <a href="<?php echo base_url("favorites")?>" id="favorites"><i class="fa fa-star"></i> Favoritos</a> 
                    </li>
<?php } ?>
                    <li class="active">
                       <a href="<?php echo base_url("outstanding")?>" id="outstanding"><i class="fa fa-heart"></i> Destacados </a> 
                    </li>
                    <li>
                       <a href="<?php echo base_url("checkout")?>" id="cart"><i class="fa fa-shopping-cart"></i> Carrito <span class="badge"><?php echo $cartCount ?></span></a> 
                    </li>
<?php if ($this->session->userdata(AuthConstants::EMAIL)) { ?>
                    <li class="hidden-sm hidden-xs">
                       <a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                           <?php 
                           $name = explode(" ", $this->session->userdata(AuthConstants::NAMES));
                           echo $name[0]?>
                            <!--<img class='img-responsive' src="<?php echo base_url(); ?>images/arrowdown.svg" alt="" />-->
                        </a>
                        <ul class="dropdown-menu"  >
                            <li>
                                <a id='opc_perfil' href="#"> 
                                    <h6 class='' >
                                        <?php echo $this->session->userdata(AuthConstants::NAMES) . " " . $this->session->userdata(AuthConstants::LAST_NAMES) ?>    
                                        <br /> <p class='tit_14_1' ><?php echo $this->session->userdata(AuthConstants::EMAIL) ?></p> </h6>	
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a id='opc_salir' href="<?php echo base_url("login/webLogout") ?>"><h6 class='' > CERRAR SESION </h6></a></li>
                        </ul>
                    </li>
                    <li class="hidden-md hidden-lg"><a id='opc_salir' href="<?php echo base_url("login/webLogout") ?>">CERRAR SESION </a></li>
<?php }else{ ?>
                    <li class="signup">
                        <a class="ga-cta-signup" href="#">Regístrate</a>
                    </li>
                    <li class="login active">
                        <a class="" href="#">Ingresar</a>
                    </li>
                    <li class="hidden-lg hidden-md login active">
                        <h4 class="panel-title text-center">
                                &nbsp;&nbsp;&nbsp;CATEGORIAS&nbsp;&nbsp;&nbsp;
                        </h4>
                    </li>
<?php } ?>
                    <?php foreach ($categories as $cat) { ?>
                    <li class="hidden-lg hidden-md col-lg-4 col-sm-6">
                        <a id="<?php echo $cat["id"]?>" href="<?php echo base_url("category/".$cat["id"]."/".  str_replace(" ", "-", $cat['category'])); ?>" class="portfolio-box">
                            <div class="cat-name text-left">
                                <?php echo $cat['category']?>
                            </div>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="input-group stylish-input-group hidden-lg hidden-md">
                <input id="opc_buscar_off_m" type="text" class="form-control search-box"  placeholder="Busca..." >
                <span class="input-group-addon">
                    <button type="submit" id="opc_buscar_off_b" class="opc_buscar_off_b">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>  
                </span>
            </div>
            </form>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    
<div class="row categories-row hidden-xs hidden-sm">
<?php foreach ($categories as $cat) {
    $class = (strlen($cat['category'])>18)?"2":"1";
    ?>

    <div class="col-lg-<?php echo $class;?> col-md-1 col-sm-6">
        <a id="<?php echo $cat["id"]?>" href="<?php echo base_url("category/".$cat["id"]."/".  str_replace(" ", "-", $cat['category'])); ?>" class="portfolio-box">
            <div class="cat-name text-center">
                <?php echo $cat['category']?>
            </div>
        </a>
    </div>
<?php }?>
</div>
<!--<div class="top-nav-categories hidden-lg hidden-md">
<div class="row">
    <div class="panel-group" id="accordion">
            <div class="">
                <div class="panel-heading ">
                    <h4 class="panel-title text-center">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            &nbsp;&nbsp;&nbsp;CATEGORIAS&nbsp;&nbsp;&nbsp;
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <?php foreach ($categories as $cat) { ?>
                    <div class="col-lg-4 col-sm-6">
                        <a id="<?php echo $cat["id"]?>" href="<?php echo base_url("category/".$cat["id"]."/".  str_replace(" ", "-", $cat['category'])); ?>" class="portfolio-box">
                            <div class="cat-name text-center">
                                <?php echo $cat['category']?>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
</div>    
</div>-->
</nav>