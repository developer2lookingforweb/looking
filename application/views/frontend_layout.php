<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="LookingForWeb">
    <script type="application/ld+json">{"@context":"http://schema.org","@type":"Blog","url":"https://alofijo.com/blogs/entries"}</script><script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization","name":"Alofijo","url":"https://alofijo.com","sameAs":["https://www.facebook.com/Alofijo_oficial-580096049062714/","https://www.instagram.com/alofijo_oficial/","https://twitter.com/AloFijoWeb","https://www.youtube.com/channel/UCzXgOYPzw66kxMyRaMQUcNw","https://business.google.com/dashboard/l/17242175969494819579?hl=es-CO&utm_source=google&utm_medium=et&utm_campaign=ww-ww-et-ip-z-gmb-s-z-l~skp-manage-u&sa=X&ved=2ahUKEwiCnYnsiOfhAhXIslkKHR0gCFcQ_8ACMAB6BAgLEAM"]}</script>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no" />
        <meta name="keywords" CONTENT="Alofijo, Looking For Web, Recomedación, Al mejor precio, Celulares, Portatiles, Educación, campara y compra.">
        <meta name="theme-color" content="#e81313">
        <!-- Open Graph data -->
        <?php if(isset($metas)){?>
        <meta name="description" CONTENT="<?php echo strip_tags(htmlspecialchars_decode($metas["description"]))?>">
        <meta property="og:title" content="<?php echo $metas["title"]?>" />
        <meta property="og:url" content="<?php echo $metas["url"]?>" />
        <meta property="og:image" content="<?php echo $metas["image"]?>" />
        <meta property="og:description" content="<?php echo strip_tags(htmlspecialchars_decode($metas["description"]))?>" />
        <meta property="og:site_name" content="<?php echo $metas["site_name"]?>" />
        <meta name="twitter:site" content="@alofijoweb"/>
        <meta property="article:publisher" content="https://www.facebook.com/AloFijoWeb"/>
        <?php  if(array_key_exists("type",$metas)){?><meta property="og:type" content="<?php echo $metas["type"]?>" /><?php } ?>
        <?php  if(array_key_exists("amount",$metas)){?><meta property="og:price:amount" content="<?php echo $metas["amount"]?>" /><?php } ?>
        <?php  if(array_key_exists("currency",$metas)){?><meta property="og:price:currency" content="<?php echo $metas["currency"]?>" /><?php } ?>
        <?php }?>
        <title><?=$title?></title>
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
        

        <!-- Bootstrap Core CSS -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


        <?php if($this->config->item("test_enviroment")==false){ ?>
<!--         Facebook Pixel Code Carolina
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '407098566151302');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=407098566151302&ev=PageView&noscript=1"
        /></noscript>
         End Facebook Pixel Code -->
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '201273390278772'); // Insert your pixel ID here.
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=201273390278772&ev=PageView&noscript=1"
        /></noscript>
        <!-- DO NOT MODIFY -->
        <!-- End Facebook Pixel Code -->
        <!-- Google Analytics Code -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80994196-1', 'auto');
        ga('send', 'pageview');

        </script>
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '407098566151302');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=407098566151302&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        
        <!-- End Google Analytics Code -->
        <!-- Smartlook Code -->
<!--        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', 'fcd2711c5599d2edafda0581c954a927989c4ce1');
        </script>-->
        <!-- End Smartlook Code -->
        <!--Start of Zendesk Chat Script-->
<!--            <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="https://v2.zopim.com/?4Ptgv0ggGzlqDM65wp7XqVU0omIFI2wH";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>-->
        <!--End of Zendesk Chat
        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = 'd49920288c682808199f107ec236d6870d371251';
        window.smartsupp||(function(d) {
                var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
                s=d.getElementsByTagName('script')[0];c=d.createElement('script');
                c.type='text/javascript';c.charset='utf-8';c.async=true;
                c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
        
        </script>
        <!-- END Smartsupp Live Chat script -->
        <!--<script src="//load.sumome.com/" data-sumo-site-id="74b45bc3fe3bf15fdc603fa0661118935537947e1fec158b274835ace95e17e7" async="async"></script>-->
        <?php }else{ ?>
        <!-- Erase chache in test enviroments -->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <?php } ?>
        <link href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap-slider.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>vendor/bootstrap/css/smartselect.min.css" rel="stylesheet">

        <!-- Custom Fonts -->

        <link href="<?php echo base_url(); ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="https://use.typekit.net/dht8lmf.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
        <!-- Plugin CSS -->
        <link href="<?php echo base_url(); ?>vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>/css/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.css">
        <link href="<?php echo base_url(); ?>css/creative.min.css" rel="stylesheet">
        <?=$css?>
    </head>
   <body id="page-top">

       <!-- magnific popui trigger and base modal-->
       <a id="modal-trigger" class="popup-with-zoom-anim hide" href="#small-dialog" ></a>
        <div id="small-dialog" class="zoom-anim-dialog white-popup mfp-with-anim  mfp-hide">
            <h2 class="text-center"></h2>
                <p></p>
        </div>
        <?=$header?>
        <?=$body?>
        <?=$footer?>
<!--        <div class="col-sm-3 frame">
            <div class="chat-header">Podemos ayudarte</div>
            <ul></ul>
            <div>
                <div class="msj-rta macro" style="margin:auto">                        
                    <div class="text text-r" style="background:whitesmoke !important">
                        <input class="mytext" placeholder="Type a message"/>
                    </div> 
                </div>
            </div>
        </div>        -->
<!--        <div class="chat_window">
            <div class="top_menu">
                <div class="buttons">
                    <div class="button close"></div>
                    <div class="button minimize"></div>
                    <div class="button maximize"></div>
                        
                </div>
                <div class="title">Chat</div>
                    
            </div>
            <ul class="messages fade"></ul>
            <div class="bottom_wrapper fade clearfix">
                <div class="message_input_wrapper">
                    <input class="message_input" placeholder="Type your message here..." />
                </div>
                <div class="send_message">
                    <div class="icon"></div>
                    <div class="text">Send</div>
                        
                </div>
                    
            </div>
                
        </div><div class="message_template"><li class="message"><div class="avatar"></div><div class="text_wrapper"><div class="text"></div></div></li></div>-->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=425960334187885";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        
        
        
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>vendor/jquery/jquery-easing.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/jquery/responsive-tabs.js"></script>
    <script src="<?php echo base_url(); ?>vendor/jquery/bootstrap-slider.js"></script>
    <script src="<?php echo base_url(); ?>vendor/jquery/jquery.smartselect.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/jquery/jquery-block-ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    
    <!-- Theme JavaScript -->
    <script src="<?php echo base_url(); ?>js/creative.min.js"></script>
        <?=$JS?>
        
    
       
        <div class="hide" id="loader">
            <div class="progress progress-info progress-striped active" style="">
                <div class="bar" style="width: 100%; color:#5d5d5d;"><img width="64" src="<?php echo base_url()?>images/loading.png" alt="Loading"></div>
            </div>
        </div>


    </body>
</html>