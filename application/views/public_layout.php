<!DOCTYPE html>
 <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->   
    
    <head>
        <!--http://www.keenthemes.com/preview/metronic/index.html-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no" />
        <meta name="Description" CONTENT="Looking For Web, búsqueda y comparación de los productos que tu necesitas, cuéntanos que estás buscando, juntos lo encontraremos, te mostramos las mejores opciones para ti por que te conocemos.">
        <meta name="keywords" CONTENT="Looking For Web, encuentra lo que necesitas, Lookingforweb, Planes Celulares,Claro, Movistar, ETB, Avantel, Servicios móviles,Recargas, Compra Celular, Móviles, Planes, Internet hogares, telefonía fija,">
        <meta name="theme-color" content="#fa823b">
        <title><?=$title?></title>
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
        <!-- JQuery UI -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.10.2/themes/base/jquery.ui.all.css" />
        <!--Foundation-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/foundation.css" />
        <!--Custom-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/<?=($this->session->userdata(AuthConstants::THEME)!="")?$this->session->userdata(AuthConstants::THEME):"default";?>.css" id="css-swithcer" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/fonts.css" />
        <!--Icons-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/general/css/general_foundicons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/accessibility/css/accessibility_foundicons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/FortAwesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/sizes.css" />
        <!--[if lt IE 8]>
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/general/css/general_foundicons_ie7.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/accessibility/css/accessibility_foundicons_ie7.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/FortAwesome/css/font-awesome-ie7.min.css" />
        <![endif]-->        
        <!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ie8.css">
        <![endif]-->        
        <!-- Datatables -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>js/DataTables/media/css/demo_table_jui.css" />
        
        
<!--        <script src="<?php echo base_url(); ?>js/vendor/custom.modernizr.js"></script>-->
        <script src="<?php echo base_url(); ?>js/vendor/modernizr.js"></script>
        <script src="<?php echo base_url(); ?>js/jsapi.js"></script>
        <?php if($this->config->item("test_enviroment")==false){ ?>
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '407098566151302');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=407098566151302&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Google Analitycs Code -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80994196-1', 'auto');
        ga('send', 'pageview');

        </script>
        <!-- End Google Analitycs Code -->
        <!-- Smartlook Code -->
        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', 'fcd2711c5599d2edafda0581c954a927989c4ce1');
        </script>
        <!-- End Smartlook Code -->
        <!--Start of Zendesk Chat Script-->
            <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="https://v2.zopim.com/?4Ptgv0ggGzlqDM65wp7XqVU0omIFI2wH";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>
        <!--End of Zendesk Chat Script-->
        <?php } ?>
    </head>
    <body> 
    <div id="overlay">
        <img src="<?php echo base_url("images/nuevoLogo.png")?>" width="256" alt="Loading" />
        <label><?php echo lang('loading')?>...</label>
        <img src="<?php echo base_url("images/overlay.gif")?>" alt="Loading" />
    </div>
        <?=$header?>
        <?=$body?>
        <?=$footer?>
        
        <!-- BBEGIN MODAL MESSAGES -->
        <div id="modal-message" class="modal-obl reveal-modal tiny" data-reveal style="z-index: 1000">
            <div class="row header-reveal-modal">
                <div class="small-11 large-11 columns large-centered">
                    <h3 id="modal-title">Alerta</h3>
                </div>
            </div>
            <div class="row content-modal">
                <div class="large-12 columns">
                    <div id="message"></div>
                </div>
                <div class="small-4 small-pull-4  small-centered medium-4 medium-pull-4 medium-centered large-4 large-pull-1 large-centered columns">
                    <div id="modal-close" class="button">Volver</div>
                </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <div id="modal-error" class="modal-obl reveal-modal tiny" data-reveal>
            <div class="row header-reveal-modal">
                <div class="large-11 columns large-centered">
                    <h3><?=lang("error")?></h3>
                </div>
            </div>
            <div class="row content-modal">
                <div class="large-12 columns">
                    <div id="error"></div>
                </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
        </div>
        
        <script src="<?php echo base_url(); ?>js/vendor/jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/app.js?base_url=<?php echo str_replace("/","47slash",str_replace(".","250dot",str_replace(":","58twopoints",base_url()))); ?>" id="identificador_js"></script>
        <script src="<?php echo base_url(); ?>js/media-queries.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.accordion.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.alert.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.clearing.js"></script>
<!--        <script src="<?php echo base_url(); ?>js/foundation/foundation.cookie.js"></script>-->
        <script src="<?php echo base_url(); ?>js/foundation/jquery.cookie.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.dropdown.js"></script>
<!--        <script src="<?php echo base_url(); ?>js/foundation/foundation.forms.js"></script>-->
        <script src="<?php echo base_url(); ?>js/foundation/foundation.equalizer.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.interchange.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.joyride.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.magellan.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.offcanvas.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.orbit.js"></script>
<!--        <script src="<?php echo base_url(); ?>js/foundation/foundation.placeholder.js"></script>-->
        <script src="<?php echo base_url(); ?>js/foundation/foundation.reveal.js"></script>
<!--        <script src="<?php echo base_url(); ?>js/foundation/foundation.section.js"></script>-->
        <script src="<?php echo base_url(); ?>js/foundation/foundation.tab.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.tooltip.js"></script>
        <script src="<?php echo base_url(); ?>js/foundation/foundation.topbar.js"></script>

        <script src="<?php echo base_url(); ?>js/custom/jquery.metadata.js"></script>
        <script src="<?php echo base_url(); ?>js/custom/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>js/custom/additional-methods.min.js"></script>
        <script src="<?php echo base_url(); ?>js/custom/jquery.form.js"></script>
        <script src="<?php echo base_url(); ?>js/DataTables/media/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url(); ?>js/custom/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>js/tiny_mce/tiny_mce.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery-ui-1.10.2/ui/jquery-ui.js"></script>
        
        <?=$JS?>
        <?=$css?>
        
        <script type="text/javascript">
            $(document).foundation();
            $(window).load(function () {
                // Page is fully loaded .. time to fade out your div with a timer ..
                $('#overlay').fadeOut(1000);
            });
            $.metadata.setType("attr", "validate");
            
            $(document).ready(function(){
                <?php if (isset($notifications)): ?>
                    $(".notification").click(function(){
                        if (<?=count($notifications)?> > 0){
                            $.post("<?=site_url("notifications/read")?>");
                        }
                    });
                <?php endif; ?>
                
                $(document).ajaxStart(function(){
                    $('#alert').removeClass("success secondary alert");
                    $.blockUI({ 
                        message: $("#loader").html(),
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: 'none', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            color: '#fff' 
                        } 
                    });
                })
                .ajaxStop(function(){
                    $.unblockUI();
                })
                .ajaxError(function(event, request, settings){
                    $.unblockUI();
                    $('#alert').addClass("alert");
                    $("#message").html("Error, porfavor verifica la conexión a Internet!");
                    $("#alert").show();
                });
            });
            
            $.extend($.fn.dataTable.defaults, {
                "bStateSave": true,
                "fnStateSave": function(oSettings,oData){
                    var url = location.pathname.replace(/[0-9\/]/g, "");
                    suff=$(this).attr('id')+url;
                    localStorage.setItem('DataTables_'+ suff, JSON.stringify(oData));
                },
                "fnStateLoad":function(oSettings){
                    var url = location.pathname.replace(/[0-9\/]/g, "");
                    suff=$(this).attr('id')+url;
                    return JSON.parse(localStorage.getItem('DataTables_'+ suff));
                },
                "bJQueryUI": true,
                "bProcessing": true,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "bServerSide": true,
                "oLanguage": $.extend($.fn.dataTable.defaults.oLanguage, {
                    "sUrl": "<?php echo base_url() ?>js/i18n/data_table_<?php echo $this->session->userdata(AuthConstants::LANG); ?>.txt"
                })
            });
            
            jQuery.validator.setDefaults({
                ignore: [],
                errorElement: "small",
                errorPlacement: function(error, element) {
                    error.appendTo( element.parent() );
                    element.parent().parent().find(".prefix").addClass("error");
                    element.addClass("error");
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).parent().parent().find(".prefix").addClass("error");
                    $(element).addClass("error");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parent().parent().find(".prefix").removeClass("error");
                    $(element).removeClass("error");
                    $(element).parent().parent().find("span.prefix").removeClass("error");
                },
            });
            
            $(document).on("click", "a", function(event){
                if ($(this).attr("href") == "#"){
                    event.preventDefault();
                }
            });

        </script>
        
        <div class="hide" id="loader">
            <div class="progress progress-info progress-striped active" style="">
                <div class="bar" style="width: 100%; color:#5d5d5d;"><img width="64" src="<?php echo base_url()?>images/loading.png"></div>
            </div>
        </div>
        
        <ul id="drop-user-menu" class="f-dropdown content-user-menu">
            <li><a href="<?=site_url(array(lang("users"),lang("mydata")))?>"><i class="icon-user"></i> <?=lang("profile")?></a></li>
            <li class="last"><a href="<?=site_url(array(lang("login"),  strtolower(lang("logout"))))?>"><i class="icon-signout"></i> <?=lang("logout")?></a></li>
            <li class="last">
                <a>
                    <i class="foundicon-monitor"></i> <?=lang("theme")?><br />
                    <div class="color gray" color="gray"></div>
                    <div class="color blue" color="blue"></div>                
                    <div class="color green" color="green"></div>
                    <div class="color dark-red" color="dark-red"></div>                
                    <div class="color orange" color="orange"></div>
                    <div class="color purple" color="purple"></div>                
                    <div class="color light-gray" color="light-gray"></div>
                    
                </a>
            </li>
        </ul>
        
        <ul id="drop-notifications" class="f-dropdown content-notification">
            <?php if (isset($notifications)): ?>
                <li class="first"><?=sprintf(lang("title_notifications"), count($notifications))?></li>
                <?php foreach($notifications as $aNotification): ?>
                    <li><a href="<?=$aNotification->getUrl()?>"><i class="foundicon-people"></i><?=$aNotification->getMessage()?></a></li>
                <?php endforeach;?>
            <?php endif; ?>
        </ul>
        
    </body>
</html>