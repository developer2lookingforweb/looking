
	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					SIEMPRE CONECTADOS
				</h4>

				<div>
					<p class="s-text7 w-size27">
						¿Preguntas? Déjanos atenderte, estamos muy cerca a Unicentro en la Cra 15a # 124 - 75 Of 3 en Bogotá o llámanos al 317 804 5157
					</p>

					<div class="flex-m p-t-30">
						<a href="https://www.facebook.com/Alofijo_oficial-580096049062714/" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
						<a href="https://www.instagram.com/alofijo_oficial/" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
						<a href="https://www.youtube.com/channel/UCzXgOYPzw66kxMyRaMQUcNw" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
					</div>
				</div>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Categorías
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="<?php echo base_url()?>category/5/Celulares" class="s-text7">
							Celulares
						</a>
					</li>

					<li class="p-b-9">
						<a href="category/13/Tecnologia" class="s-text7">
							Tecnología
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>category/9/Educacion" class="s-text7">
							Educación
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>category/19/Salud-y-belleza" class="s-text7">
							Salud y belleza
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					NOSOTROS
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="<?php echo base_url()?>" class="s-text7">
							Inicio
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>articles/display/2/quienes-somos" class="s-text7">
							Quienes Somos
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>articles/display/1/terminos-y-condiciones" class="s-text7">
							Términos y condiciones
						</a>
					</li>

				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					MAS
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="<?php echo base_url()?>blogs/entries" class="s-text7">
							Blog
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>articles/display/4/politicas/envio" class="s-text7">
							Políticas de envío
						</a>
					</li>

					<li class="p-b-9">
						<a href="<?php echo base_url()?>messages/userMessage" class="s-text7">
							Contacto
						</a>
					</li>

					
				</ul>
			</div>

			<div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					Newsletter
				</h4>

				<form action="https://alofijo.com/sendy/subscribe" method="post" class="sign-up">
					<div class="effect1 w-size9">
						<input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="email@example.com">
						<input type="hidden" name="list" value="qimA763J24863swVos2Pu3Lg">
						<span class="effect1-line"></span>
					</div>

					<div class="w-size2 p-t-20">
						<!-- Button -->
						<button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
							Subscríbete
						</button>
					</div>

				</form>
				
			</div>
		</div>

		<div class="t-center p-l-15 p-r-15">
            <a target="_blank"  href="https://www.epayco.co/"><img src="<?php echo base_url("images/epayco.png")?>" alt="Epayco"></a>
			<div class="t-center s-text8 p-t-20">
            <p>Looking For Web All Rights Reserved<br />Copyright &copy; <?php echo date('Y');?></p>
			</div>
		</div>
	</footer>