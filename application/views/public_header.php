<?php if(isset($theme)){
    $headerTheme = $theme;
}else{
    $headerTheme = "";
}?>
<div id="header" class="row navbar-fixed-top <?php echo $headerTheme?>">
    <div id="logo" class="small-8 medium-3 large-2 columns">
        <a href="<?=base_url()?>">
            <img src="<?=base_url()?>images/nuevoLogo.png" alt="Logo"  width="205"/>
        </a>
    </div>
    <?php if($this->session->userdata(AuthConstants::NAMES)){
        $logged = "";
        $register = "hide";
    }else{
        $logged = "hide";
        $register = "";
    }?>
    <div class="small-12 medium-9 large-9  large-offset-1 login columns center">
        <div class="columns medium-6 large-5 left">
            <form id="searchForm" name="searchForm" method="POST" action="<?php echo base_url("browser/directResults") ?>" >
                <input type="text" id="globalSearch" name="globalSearch" value="" placeholder="¿Qué estás buscando?"/>
            </form>
            <span class="search-icon"><img id="search-img" width="26" src="<?php echo base_url("images/search-icon2.png")?>"/></span>
        </div>
        <div class="columns medium-1 large-1 left">
        <span class="cart" class="columns large-3 left ">
            <span id="cartCount"><?php echo count($this->session->userdata(AuthConstants::CART))?></span>
            
            <a id="drop1-trigger" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">
                <img width="32" src="<?php echo base_url() ?>images/cart.png" alt="checkout"/>
            </a>
        </div>
            <div id="drop1" class="f-dropdown small" data-dropdown-content aria-hidden="true" tabindex="-1">
                <label class="resume-cart-label"><?php echo lang("resume")?></label>
              <div class="resume-cart"></div>
              <div class="columns large-12"><a class="button cart-btn columns large-12 large-centered" href="<?php echo base_url()?>browser/checkout"><?php echo lang("go_checkout")?>
                  </a></div>
            </div>
        </span>
        <div id="drop4-trigger" data-dropdown="drop4" aria-controls="drop4" aria-expanded="false" class="columns large-3 right  <?php echo $logged?>">
            <span id="username"><?php echo $this->session->userdata(AuthConstants::NAMES)." ". $this->session->userdata(AuthConstants::LAST_NAMES)?></span>
            <img id="user-img" src="<?=base_url()?>images/users/frontuser.png" alt="Logo"  width="43"/>
        </div>
        <div id="drop4" class="f-dropdown small login-popup" data-dropdown-content aria-hidden="true" tabindex="-1">
            <form id="register-form" name="register-form" method="post">
            <div class="columns large-12"><a id="logout-button" class="button cart-btn columns large-10 large-centered" href="#"><?php echo lang("logout")?>
            </a></div>
            </form>
        </div>
        <div class="columns medium-4 large-5 right hide-for-small">
            <button id="drop3-trigger" data-dropdown="drop3" class="columns large-5 medium-4 small-5  right <?php echo $register?>" aria-controls="drop3" aria-expanded="false">
                <div class="hide-for-medium">Iniciar Sesión</div>
                <div class="show-for-medium-only"><img id="" src="<?=base_url()?>images/account-icon.png" alt="Logo"  width="43"/></div>
            </button>
            <div id="drop3" class="f-dropdown small login-popup" data-dropdown-content aria-hidden="true" tabindex="-1">
                <form id="login-form" name="login-form" method="post">
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("email")?></label></div>
                <div class="columns large-10 large-centered"><input type="text" name="email-login"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("password")?></label></div>
                <div class="columns large-10 large-centered"><input type="password" name="password-login"/></div>
                <div class="columns large-10 large-centered">
                    <input id="11" type="checkbox" name="remember" value=""/>
                    <label for="11">
                        <span></span> 
                    </label>
                    <label class="login-popup-label left">Recordar Usuario</label>
                </div>
                <div class="columns large-12"><a id="login-button" class="button cart-btn columns large-10 large-centered" href="#"><?php echo ucfirst(lang("login"))?>
                </form>

                </a></div>
            </div>
            <button id="drop2-trigger" data-dropdown="drop2" class="columns large-5 medium-4 small-5 right <?php echo $register?>" aria-controls="drop2" aria-expanded="false">
                <div class="hide-for-medium">Registrate</div>
                <div class="show-for-medium-only"><img id="" src="<?=base_url()?>images/signup_icon.png" alt="Logo"  width="43"/></div>
            </button>
            <div id="drop2" class="f-dropdown small login-popup" data-dropdown-content aria-hidden="true" tabindex="-1">
                <form id="register-form" name="register-form" method="post">
                    <?php $CI =& get_instance();?>
                    <input id="csrftag" type="hidden" name="<?php echo $CI->security->get_csrf_token_name()?>" value="<?php echo $CI->security->get_csrf_hash()?>">
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("name")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-name" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("last_name")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-last_name" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("email")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-email" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("address")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-address" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("mobile")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-mobile" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("phone")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="text" name="reg-phone" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("password")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="password" name="reg-password" class="required"/></div>
                <div class="columns large-10 large-centered"><label class="login-popup-label left"><?php echo lang("repeat_password")?>*</label></div>
                <div class="columns large-10 large-centered"><input type="password" name="reg-repassword" class="required"/></div>
                <div class="columns large-12"><a id="register-button" class="button cart-btn columns large-10 large-centered" href="#"><?php echo lang("register")?>
                </a></div>
                </form>
            </div>
        </div>
<!--        <div class="columns large-1 left show-for-large-only">
            <span class="country" class="columns large-3 left ">
                <a id="drop5-trigger" data-dropdown="drop5" aria-controls="drop5" aria-expanded="false">
                    <img width="24" src="<?php echo base_url() ?>images/colombia.png" alt="country"/>
                </a>
            </span>
        </div>
            <ul id="drop5" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li class="country-item">
                  <img width="24" src="<?php echo base_url() ?>images/colombia.png" alt="country"/>
                  <label>Colombia</label>
              </li>
              <li class="country-item disabled">
                  <img width="24" src="<?php echo base_url() ?>images/chile.png" alt="country"/>
                  <label>Chile<span class="soon">Próximamente</span></label>
              </li>
              <li class="country-item disabled">
                  <img width="24" src="<?php echo base_url() ?>images/mexico.png" alt="country"/>
                  <label>Mexico<span class="soon">Próximamente</span></label>
              </li>
              <li class="country-item disabled">
                  <img width="24" src="<?php echo base_url() ?>images/peru.png" alt="country"/>
                  <label>Perú<span class="soon">Próximamente</span></label>
              </li>
            </ul>-->
    </div>
</div>