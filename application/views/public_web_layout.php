<!DOCTYPE html>
 <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <!--http://www.keenthemes.com/preview/metronic/index.html-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no" />
        <meta name="Description" CONTENT="Looking For Web, búsqueda y comparación de los productos que tu necesitas, cuéntanos que estás buscando, juntos lo encontraremos, te mostramos las mejores opciones para ti por que te conocemos.">
        <meta name="keywords" CONTENT="Looking For Web, encuentra lo que necesitas, Lookingforweb, Planes Celulares,Claro, Movistar, ETB, Avantel, Servicios móviles,Recargas, Compra Celular, Móviles, Planes, Internet hogares, telefonía fija,">
        <meta name="theme-color" content="#fa823b">
        <title><?=$title?></title>
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>favicon.png" />
        <!-- JQuery UI -->
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-1.10.2/themes/base/jquery.ui.all.css" />-->
        <!--Foundation-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.min.css" />
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/foundation.css" />-->
        <!--Custom-->
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" />-->
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/<?=($this->session->userdata(AuthConstants::THEME)!="")?$this->session->userdata(AuthConstants::THEME):"default";?>.css" id="css-swithcer" />-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/fonts.css" />
        <!--Icons-->
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/general/css/general_foundicons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/accessibility/css/accessibility_foundicons.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/FortAwesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/sizes.css" />-->

        <script src="https://use.typekit.net/dht8lmf.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/hover/css/hover-min.css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/css/styles.css">


        <!--[if lt IE 8]>
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/general/css/general_foundicons_ie7.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/accessibility/css/accessibility_foundicons_ie7.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>css/icons/FortAwesome/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ie8.css">
        <![endif]-->
        <!-- Datatables -->
<!--            <link rel="stylesheet" href="<?php echo base_url(); ?>js/DataTables/media/css/demo_table_jui.css" />


        <script src="<?php echo base_url(); ?>js/vendor/custom.modernizr.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/modernizr.js"></script>
        <script src="<?php echo base_url(); ?>js/jsapi.js"></script>-->
        <?php if($this->config->item("test_enviroment")==false){ ?>
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '407098566151302');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=407098566151302&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Google Analytics Code -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80994196-1', 'auto');
        ga('send', 'pageview');

        </script>
        <!-- End Google Analytics Code -->
        <!-- Smartlook Code -->
<!--        <script type="text/javascript">
            window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', 'fcd2711c5599d2edafda0581c954a927989c4ce1');
        </script>-->
        <!-- End Smartlook Code -->
        <!--Start of Zendesk Chat Script-->
<!--            <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="https://v2.zopim.com/?4Ptgv0ggGzlqDM65wp7XqVU0omIFI2wH";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>-->
        <!--End of Zendesk Chat
        <!-- Smartsupp Live Chat script -->
        <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = 'd49920288c682808199f107ec236d6870d371251';
        window.smartsupp||(function(d) {
                var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
                s=d.getElementsByTagName('script')[0];c=d.createElement('script');
                c.type='text/javascript';c.charset='utf-8';c.async=true;
                c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
        </script>
        <!-- END Smartsupp Live Chat script -->
        <?php } ?>
    </head>
    <body>



<!--    <div id="overlay">
        <img src="<?php echo base_url("images/nuevoLogo.png")?>" width="256" alt="Loading" />
        <label><?php echo lang('loading')?>...</label>
        <img src="<?php echo base_url("images/overlay.gif")?>" alt="Loading" />
    </div>-->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=425960334187885";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/skel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/zipped.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>

    <script>
	  $(document).ready(function() {
	    
                   
                    /* agregar animaciones */
                 
            

			 $( ".lin_mhb" ).click(function() {
			    $('#menu').removeClass('animated ');
				//$('#menu').addClass('animated rollIn');
			 });
			 $( ".close" ).click(function() {
			    $('#menu').removeClass('animated ');
				//$('#menu').addClass('animated rollOut');
			 });

	         $( ".lin_mhb" ).mouseover(function() {
				$(this).addClass('animated jello');
			 }).mouseout(function() {
				$(this).removeClass('animated jello');
			 });
			  $( ".close" ).mouseover(function() {
				$(this).addClass('animated tada');
			 }).mouseout(function() {
				$(this).removeClass('animated tada');
			 });
			 $( ".lin_social" ).mouseover(function() {
				$(this).addClass('animated pulse');
			 }).mouseout(function() {
				$(this).removeClass('animated pulse');
			 });

			/* fin animaciones */

			$("#owl-demo").owlCarousel({
				navigation: true,
				slideSpeed: 300,
				paginationSpeed: 400,
				singleItem: true,
				itemsTablet: true,
				itemsMobile : true,
				itemsDesktop : true,
				itemsDesktopSmall :true,
			});

		});
	</script>
        <?=$header?>
        <?=$body?>
        <?=$footer?>
        <?=$JS?>
        <?=$css?>
         <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/slick/slick-theme.css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/slick/slick.min.js"></script>
        <script type="text/javascript">
//            $(document).foundation();
//            $(window).load(function () {
//                // Page is fully loaded .. time to fade out your div with a timer ..
//                $('#overlay').fadeOut(1000);
//            });
//            $.metadata.setType("attr", "validate");

            $(document).ready(function(){
                <?php if (isset($notifications)): ?>
                    $(".notification").click(function(){
                        if (<?=count($notifications)?> > 0){
                            $.post("<?=site_url("notifications/read")?>");
                        }
                    });
                <?php endif; ?>

                $(document).ajaxStart(function(){
                    $('#alert').removeClass("success secondary alert");
//                    $.blockUI({
//                        message: $("#loader").html(),
//                        css: {
//                            border: 'none',
//                            padding: '15px',
//                            backgroundColor: 'none',
//                            '-webkit-border-radius': '10px',
//                            '-moz-border-radius': '10px',
//                            color: '#fff'
//                        }
//                    });
                })
                .ajaxStop(function(){
//                    $.unblockUI();
                })
                .ajaxError(function(event, request, settings){
//                    $.unblockUI();
                    $('#alert').addClass("alert");
                    $("#message").html("Error! Sorry!");
                    $("#alert").show();
                });
            });

            $(document).on("click", "a", function(event){
                if ($(this).attr("href") == "#"){
                    event.preventDefault();
                }
            });

        </script>

        <div class="hide" id="loader">
            <div class="progress progress-info progress-striped active" style="">
                <div class="bar" style="width: 100%; color:#5d5d5d;"><img width="64" src="<?php echo base_url()?>images/loading.png"></div>
            </div>
        </div>


    </body>
</html>