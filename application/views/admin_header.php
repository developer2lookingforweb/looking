<div id="header" class="row navbar-fixed-top">
    <div id="logo" class="small-8 medium-3 large-2 columns">
        <a href="#">
            <img src="<?=base_url()?>images/nuevoLogo.png" alt="Logo" />
        </a>
    </div>
    <div class="small-3 medium-3 large-2 small-offset-4 medium-offset-6 large-offset-8 columns">
        <?php if(isset($notifications)): ?>
            <a data-dropdown="drop-notifications" href="#" class="notification">
                <i class="foundicon-mail"></i>
                <span class="badge"><?=count($notifications)?></span>
            </a>
        <?php endif; ?>
        <a data-dropdown="drop-user-menu"  href="#" id="user-menu">
            <?php if(isset($full_name)): ?>
            <span class="small-2 medium-2 large-2 avatar right"><img alt='<?=$full_name?>'src='<?=site_url(AuthConstants::USERS_PATH."thumb_".$this->session->userdata(AuthConstants::USER_IMAGE))?>'></span>
            <span class="small-8 medium-8 large-8 right"><?=$full_name?></span>
            <?php endif; ?>
            <i class="arrow icon-angle-down"></i>
        </a>
        <ul id="drop-user-menu" class="f-dropdown content-user-menu">
            <li><a href="<?=site_url(array(lang("users"),lang("mydata")))?>"><i class="icon-user"></i> <?=lang("profile")?></a></li>
            <li class="last"><a href="<?=site_url(array(lang("login"),  strtolower(lang("logout"))))?>"><i class="icon-signout"></i> <?=lang("logout")?></a></li>
<!--            <li class="last">
                <a>
                    <i class="foundicon-monitor"></i> <?=lang("theme")?><br />
                    <div class="color gray" color="gray"></div>
                    <div class="color blue" color="blue"></div>                
                    <div class="color green" color="green"></div>
                    <div class="color dark-red" color="dark-red"></div>                
                    <div class="color orange" color="orange"></div>
                    <div class="color purple" color="purple"></div>                
                    <div class="color light-gray" color="light-gray"></div>
                    
                </a>
            </li>-->
        </ul>
    </div>
</div>