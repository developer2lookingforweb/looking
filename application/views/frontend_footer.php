<footer>
    <div class="navbar navbar-inverse ">
        <div class="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapseF" data-target="#footer-body">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapseF" id="footer-body">
                <ul class="nav navbar-default col-lg-2">
                    <li><a href="<?php echo base_url("")?>">Inicio</a></li>
                    <li><a href="<?php echo base_url("articles/display/2/quienes-somos")?>">Acerca de nosotros</a></li>
                    <li><a href="<?php echo base_url("articles/display/1/terminos-condiciones")?>">Términos y condiciones</a></li>
                </ul>
                <ul class="nav navbar-default col-lg-2">
                    <li><a href="<?php echo base_url("blogs/entries")?>">Blogs</a></li>
                    <li><a href="<?php echo base_url("articles/display/4/politicas-envio")?>">Políticas de Envio</a></li>
					<li><a href="<?php echo base_url("messages/userMessage")?>">Contacto</a></li>
                    <li><a href="#">&nbsp;</a></li>
                </ul>
                <ul class="nav navbar-default col-lg-4">
                    <li>
                        <div class="text-center">
                            <a target="_blank" class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/Alofijo_oficial-580096049062714/"><span class="fa fa-facebook fa-2x"></span></a>
                            <!-- <a target="_blank" class="btn btn-social-icon btn-google" href="https://plus.google.com/u/4/b/112950144734980604096/112950144734980604096"><span class="fa fa-google fa-2x"></span></a> -->
                            <!-- <a target="_blank" class="btn btn-social-icon btn-twitter" href="https://twitter.com/alofijoweb"><span class="fa fa-twitter fa-2x"></span></a> -->
                            <a target="_blank" class="btn btn-social-icon btn-youtube" href="https://www.youtube.com/channel/UCzXgOYPzw66kxMyRaMQUcNw"><span class="fa fa-youtube fa-2x"></span></a>
                            <a target="_blank" class="btn btn-social-icon btn-instagram" href="https://www.instagram.com/alofijo_oficial/"><span class="fa fa-instagram fa-2x"></span></a>
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-default col-lg-3">
                    <li><a target="_blank"  href="https://www.epayco.co/"><img src="<?php echo base_url("images/epayco.png")?>" alt="Epayco"></a></li>
                </ul>
            </div>
            <div class="text-center copyright">
                <p>Looking For Web All Rights Reserved<br />Copyright &copy; <?php echo date('Y');?></p>
            </div>
        </div>
    </div>
</footer>
