<div class="large-12 columns">
    <dl class="tabs" data-tab>    
        <?php 
        foreach ($groupTabs as $key => $title) {
            $active = ($key===0)?"active":"";
            ?><dd class="text-center-for-small <?=$active?>"><a href="#panel2-<?=($key+1)?>"><?=$title?></a></dd><?php
        }
        ?>   
    </dl>
    <div class="tabs-content">
        <?php   foreach ($groupSections as $key => $section) :
                    $active = ($key===0)?"active":"";
            ?>            
        <div class="content <?=$active?>" id="panel2-<?=($key+1)?>">
        <div class="columns large-<?=(isset($span)) ? $span : 12 ?>">
            <div class="columns">        
                <div class="box-content">
                    <?=$section["content"] ?>

                    <div class="large-12 columns">
                        <div data-alert id="alert" class="alert-box" style="display: none;">
                            <div id="message"></div>
                            <a href="#" class="close">&times;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php endforeach;?>
    </div>
</div>
