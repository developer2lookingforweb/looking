<?php
/*CONTROLLERS*/
$lang['builder']='constructor';
$lang['articles']='articulos';
$lang['blogs']='blogs';
$lang['cities']='ciudades';
$lang['countries']='paises';
$lang['cupons']='cupones';
$lang['home']='dashboard';
$lang['login']='inicio-sesion';
$lang['permissions']='permisos';
$lang['message']='contactenos';
$lang['profiles']='perfiles';
$lang['sections']='secciones';
$lang['users']='usuarios';
$lang['usersdata']='informacion-adicional';
$lang['wsdl']='wsdl';

/*RELEVANT METHODS*/
$lang['index']='inicio';
$lang['logout']='salir';
$lang['edit']='editar';
$lang['form']='crear';
$lang['mydata']='informacion-personal';
$lang['assignPermissions']='asignar-permisos';

/*METHODS*/
$lang['signin_company']='registro';

