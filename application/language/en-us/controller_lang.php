<?php
/*CONTROLLERS*/
$lang['builder']='model-builder';
$lang['articles']='Articles';
$lang['blogs']='blogs';
$lang['cities']='cities';
$lang['countries']='countries';
$lang['cupons']='cupons';
$lang['home']='dashboard';
$lang['login']='login';
$lang['permissions']='permission';
$lang['message']='contactus';
$lang['profiles']='profiles';
$lang['sections']='section';
$lang['users']='users';
$lang['usersdata']='usersdata';
$lang['wsdl']='wsdl';

/*RELEVANT METHODS*/
$lang['index']='index';
$lang['logout']='logout';
$lang['edit']='edit';
$lang['form']='create';
$lang['mydata']='my-data';
$lang['assignPermissions']='assign-permissions';

/*METHODS*/
$lang['signin_company']='singin';