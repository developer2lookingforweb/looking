<?php defined('BASEPATH') OR exit('No direct script access allowed');
//ini_set("display_errors", "on");
//date_default_timezone_set('America/Bogota');
include("nusoap.php"); 

class Consumer {
    private $_wsdlObj;
    private $_namespace;
    private $_function;
    private $_comercio;
    private $_puntoVenta;
    private $_terminal;
    private $_clave;
    private $_params = array();
    
    public function __construct() {
        $this->_wsdlObj = new nusoap_client(AuthConstants::WS_URL);
        $this->_function = AuthConstants::WS_FUNC;
        $this->_namespace    = AuthConstants::WS_NAMESPACE;
        $this->_wsdlObj->setCredentials(AuthConstants::WS_USER, AuthConstants::WS_USERPASSWD);
        $this->initParams();
    }
    
    public function init() {
        $this->_wsdlObj = new nusoap_client(AuthConstants::WS_URL,true);
        $this->_function = AuthConstants::WS_FUNC;
        $this->_namespace    = AuthConstants::WS_NAMESPACE;
        $this->_wsdlObj->setCredentials(AuthConstants::WS_USER, AuthConstants::WS_USERPASSWD);
//        $this->initParams();
    }
    
    private function initParams(){
        
        $this->setParam('comercioId', AuthConstants::WS_COMERCE);
        $this->setParam('puntodeventaId', AuthConstants::WS_SALESPOINT);
        $this->setParam('terminalId', AuthConstants::WS_TERMINAL);
        $this->setParam('clave', AuthConstants::WS_PASSWORD);
    }
    private function initPaquetigoParams($idPaquetigo, $celular){
        
        $this->_params = array();
        $this->setParam('clave', AuthConstants::WS_PASSWORD);
        $this->setParam('codigoPaquetigo', $idPaquetigo);
        $this->setParam('comercioId', AuthConstants::WS_COMERCE);
        $this->setParam('numeroCelular', $celular);
        $this->setParam('puntodeventaId', AuthConstants::WS_SALESPOINT);
        $this->setParam('terminalId', AuthConstants::WS_TERMINAL);
    }
    private function initPaquetigoQueryParams($idPaquetigo){
        
        $this->_params = array();
        $this->setParam('codigoPaquetigo', $idPaquetigo);
        $this->setParam('comercioId', AuthConstants::WS_COMERCE);
    }
    private function setParam($key,$value){
        $this->_params[$key] = $value;
    }
    public function setNumber($number){
        $this->setParam("numero", $number);
    }
    public function setValue($value){
        $this->setParam("valor", $value);
    }
    public function setTrace($id){
        $this->setParam("trace", $id);
    }
    
    public function setOwner($name){
        switch ($name){
            case 'movistar':
                $this->setParam("codigoOperador", '668');                
                break;
            case 'claro':
                $this->setParam("codigoOperador", '266');                
                break;
            case 'tigo':
                $this->setParam("codigoOperador", '652');
                break;
            case 'exito':
                $this->setParam("codigoOperador", '660');
                break;
            case 'avantel':
                $this->setParam("codigoOperador", '350');
                break;
            case 'cellvoz':
                $this->setParam("codigoOperador", '300');
                break;
            case 'uffmovil':
                $this->setParam("codigoOperador", '304');
                break;
            case 'une':
                $this->setParam("codigoOperador", '500');
                break;
            case 'directv':
                $this->setParam("codigoOperador", '400');
                break;
            case 'etb':
                $this->setParam("codigoOperador", '903');
                break;
            case 'virgin':
                $this->setParam("codigoOperador", '906');
                break;
        }
    }
    
    public function sendPaquetigo($idPaquetigo,$celular){
        try{
                $this->initPaquetigoParams($idPaquetigo, $celular);
                echo '<pre>';
              
//                $datos = array ("clave"=>120403,"codigoPaquetigo"=>16,"comercioId"=>293488,"numeroCelular"=>3013202020,"puntodeventaId"=>144223,"terminalId"=>198453);
                var_dump($this->_params);
                $result = $this->_wsdlObj->callRequest(AuthConstants::WS_FUNC_3, $this->_params , $this->_namespace);
                var_dump($result);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
        }
    }

    public function findPaquetigo($idPaquetigo){
        try{
//                $this->init();
                $this->initPaquetigoQueryParams($idPaquetigo);
                echo '<pre>';
                var_dump($this->_params);
//                var_dump($this->_wsdl);
                $result = $this->_wsdlObj->call(AuthConstants::WS_FUNC_2, $this->_params , $this->_namespace);
                var_dump($result);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
        }
    }


    public function send(){
        if(count($this->_params)== 8){
            try{
                echo '<pre>';
                $result = $this->_wsdlObj->call($this->_function,  $this->_params, $this->_namespace);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
            }
        }  else {
            echo "Datos incompletos";
        }
    }
    
}