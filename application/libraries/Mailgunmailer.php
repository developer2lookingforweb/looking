<?php
require_once 'vendor/autoload.php';
use Mailgun\Mailgun;

class Mailgunmailer{
    private $_key;
    private $_domain;
    public $from = '';
    public $to = '';
    public $bc = '';
    public $bcc = '';
    public $subject ='';
    public $text ='';
    public $html ='';
    public $attach = array();
    public $mailObj;
    
    public function __construct() {
        $this->ci = &get_instance();
        if($this->ci->config->item("test_enviroment")==true){
            /**
             * Sandbox data
             */
//            $this->_key = "key-12d3b9e748bb20bc6c3042923bd42e7d";
//            $this->_domain = "sandboxbe512428efe1438b89ce73d39cfddde8.mailgun.org";
            $this->_key = "key-12d3b9e748bb20bc6c3042923bd42e7d";
            $this->_domain = "lookingforweb.com";
        }  else {
            $this->_key = "key-12d3b9e748bb20bc6c3042923bd42e7d";
            $this->_domain = "lookingforweb.com";
        }
        $this->initMailer();
    }
    private function initMailer(){
        $this->mailObj = new Mailgun($this->_key);
    }
    public function setSubject($subject){
        $this->subject = $subject;
    }
    public function setFrom($from, $emailFrom){
        $this->from = $from. " <".$emailFrom.">";
    }
    public function setText($text){
        $this->text = $text;
    }
    public function setHtml($html){
        $this->html = $html;
    }
    public function setTo($to){
        $this->setRecipient($to, "to");
    }
    public function setbc($to){
        $this->setRecipient($to, "bc");
    }
    public function setbcc($to){
        $this->setRecipient($to, "bcc");
    }
    public function setAttachment($atach){
        array_push($this->attach, $atach);
    }
    
    public function setRecipient($to,$type){
        $this->$type = "";
        foreach($to as $index => $recipient){
            if($index>0){ 
                $this->$type .= ", ";                
            }            
            $this->$type .= $recipient['name']." <". $recipient['mail'].">";
        }
    }
    public function send(){
        if($this->to != '' && $this->from != '' && $this->subject != ''){
            try{
                $params['from'] = $this->from;
                $params['subject'] = $this->subject;
                $params['to'] = $this->to;
                if($this->bc != ''){
                    $params['bc'] = $this->bc;
                }
                if($this->bcc != ''){
                    $params['bcc'] = $this->bcc;
                }
                if($this->html != ''){
                    $params['html'] = $this->html;
                }else{
                    $params['text'] = $this->text;
                }
                
                $result = $this->mailObj->sendMessage($this->_domain, $params, array(
                    'attachment' => $this->attach
                ));
            } catch (Exception $ex) {
                var_dump($ex);
            }
        }
    }
    public function notify($to,$subject,$template,$params,$bc = false,$bcc =false, $attach = null){
        if($this->ci->config->item("test_enviroment")==true){
            $this->setTo(array(array('name'=>AuthConstants::ML_SUPPORTNAME,'mail'=>  AuthConstants::ML_SUPPORTEMAIL)));
        }  else {
            $this->setTo($to);
            if($bc!== false)$this->setbc($bo);
            if($bcc!== false)$this->setbcc($boc);
        }
        $this->setFrom(AuthConstants::ML_INFONAME, AuthConstants::ML_INFOEMAIL);
        $this->setSubject($subject);
        $html = file_get_contents("templates/$template.html");
        foreach ($params as $key => $field) {
            $html = str_replace($key, $field, $html);

        }
        if($attach !== null){
            if(is_array($attach)){
                foreach ($attach as $key => $file) {
                    if(is_file("templates/".$file)){
                        $this->setAttachment(realpath("templates/".$file));
                    }
                }
            }else{
                if(is_file("templates/".$attach)){
                    $this->setAttachment(realpath("templates/".$attach));
                }
            }
        }
        $this->setHtml($html);
        $this->send();   
    }
    public function notifyInvoice($to,$subject,$template,$params,$bc = false,$bcc =false, $attach = null){
        if($this->ci->config->item("test_enviroment")==true){
            $this->setTo(array(array('name'=>AuthConstants::ML_SUPPORTNAME,'mail'=>  AuthConstants::ML_SUPPORTEMAIL)));
        }  else {
            $this->setTo($to);
            if($bc!== false)$this->setbc($bo);
            if($bcc!== false)$this->setbcc($boc);
        }
        $this->setFrom(AuthConstants::ML_INFONAME, AuthConstants::ML_INFOEMAIL);
        $this->setSubject($subject);
        $html = file_get_contents("templates/$template.html");
        foreach ($params as $key => $field) {
            $html = str_replace($key, $field, $html);

        }
        if($attach !== null){
            if(is_array($attach)){
                foreach ($attach as $key => $file) {
                    if(is_file("invoices/".$file)){
                        $this->setAttachment(realpath("invoices/".$file));
                    }
                }
            }else{
                if(is_file("invoices/".$attach)){
                    $this->setAttachment(realpath("invoices/".$attach));
                }
            }
        }
        $this->setHtml($html);
        $this->send();   
    }
    
}