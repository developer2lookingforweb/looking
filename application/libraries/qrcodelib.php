<?php defined('BASEPATH') OR exit('No direct script access allowed');


require_once(dirname(__FILE__) . '/qrcode/qrlib.php');
class QrcodeLib 
{
	/**
	 * Get an instance of CodeIgniter
	 *
	 * @access	protected
	 * @return	void
	 */
	public function standardQr($data,$filename){
            QRcode::png($data, $filename, "M", 3, 2);
//            QRtools::timeBenchmark();
            return $filename;
        }
}