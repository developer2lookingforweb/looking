<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Orders extends REST_Controller
{
	function order_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }
            
            $order = $this->em->find('models\Orders', $this->get('id'));

            if ($order){
                $this->response(array("status"=>true, "data"=>$order->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function order_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $order = $this->em->find('models\Orders', $this->post("id"));
            $order->setInvoice($this->post('invoice'));
			$order->setOrder($this->post('order'));
			$order->setOrderDate($this->post('orderDate'));
			$order->setReference($this->post('reference'));
			$order->setResponsePol($this->post('responsePol'));
			$order->setValue($this->post('value'));
			$order->setStatus($this->post('status'));
			$order->setPaymentMethod($this->post('paymentMethod'));
			$order->setCustomer($this->post('customer'));
			$order->setOffer($this->post('offer'));
			$order->setCupon($this->post('cupon'));
            $this->em->persist($order);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function order_put()
    {
        try {
            $order = new models\Orders();
            $order->setInvoice($this->put('invoice'));
			$order->setOrder($this->put('order'));
			$order->setOrderDate($this->put('orderDate'));
			$order->setReference($this->put('reference'));
			$order->setResponsePol($this->put('responsePol'));
			$order->setValue($this->put('value'));
			$order->setStatus($this->put('status'));
			$order->setPaymentMethod($this->put('paymentMethod'));
			$order->setCustomer($this->put('customer'));
			$order->setOffer($this->put('offer'));
			$order->setCupon($this->put('cupon'));
            $this->em->persist($order);
            $this->em->flush();
            $id = $order->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function order_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }
            
            $id           = $this->delete("id");
            $order = $this->em->find('models\Orders', $id);
        
            $this->em->remove($order);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}