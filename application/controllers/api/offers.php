<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Offers extends REST_Controller
{
	function offer_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }
            
            $offer = $this->em->find('models\Offers', $this->get('id'));
            
            if ($offer){
                $this->response(array("status"=>true, "data"=>$offer->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function offer_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $offer = $this->em->find('models\Offers', $this->post("id"));
            $offer->setName($this->post('name'));
            $offer->setDescription($this->post('description'));
            $offer->setFilter($this->post('filter'));
            if($this->post('filterId'))$offer->setFilterId($this->post('filterId'));
            $offer->setStatus($this->post('status'));
            $offer->setDiscount($this->post('discount'));
            $end = new DateTime($this->post('end'));
            $offer->setEnd($end);
            if($this->post('campaign')){
                $this->loadRepository("Campaigns");
                $campaign = $this->Campaigns->find($this->post('campaign'));
                $offer->setCampaign($campaign);
            }
            $this->em->persist($offer);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function offer_put()
    {
        try {
            $offer = new models\Offers();
            $offer->setName($this->put('name'));
            $offer->setDescription($this->put('description'));
            $offer->setFilter($this->put('filter'));
            if($this->put('filterId'))$offer->setFilterId($this->put('filterId'));
            $offer->setStatus($this->put('status'));
            $offer->setDiscount($this->put('discount'));
            $end = new DateTime($this->put('end'));
            $offer->setEnd($end);
            if($this->put('campaign')){
                $this->loadRepository("Campaigns");
                $campaign = $this->Campaigns->find($this->put('campaign'));
                $offer->setCampaign($campaign);
            }
            $this->em->persist($offer);
            $this->em->flush();
            $id = $offer->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function offer_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }
            
            $id           = $this->delete("id");
            $offer = $this->em->find('models\Offers', $id);
        
            $this->em->remove($offer);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}