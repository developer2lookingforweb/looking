<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Products extends REST_Controller
{
	function product_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }
            
            $product = $this->em->find('models\Products', $this->get('id'));
            
            if ($product){
                $this->response(array("status"=>true, "data"=>$product->toArray(true)), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function product_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $product = $this->em->find('models\Products', $this->post("id"));
            $product->setProduct($this->post('product'));
            if ($this->post("image")){$product->setImage($this->post('image'));}
            $product->setDescription($this->post('description'));
            $product->setCost($this->post('cost'));
            $product->setMarketPrice($this->post('market_price'));
            $product->setTax($this->post('tax'));
            $product->setStatus($this->post('status'));
            $product->setStock($this->post('stock'));
            $product->setOutstanding($this->post('outstanding'));
            $this->loadRepository("Brands");
            $brand = $this->Brands->find($this->post('brand'));
            $product->setBrand($brand);
            $this->loadRepository("Categories");
            $category = $this->Categories->find($this->post('category'));
            $product->setCategory($category);
            $this->loadRepository("PaymentMethods");
            $paymentMethod = $this->PaymentMethods->find($this->post('paymentMethod'));
            $product->setPaymentMethod($paymentMethod);
            $product->setLastUpdate(new DateTime());
            $this->em->persist($product);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function status_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $product = $this->em->find('models\Products', $this->post("id"));
            $product->setStatus($this->post('status'));
            $this->em->persist($product);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function outstanding_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $product = $this->em->find('models\Products', $this->post("id"));
            $product->setOutstanding($this->post('outstanding'));
            $this->em->persist($product);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function product_put()
    {
        try {
            $product = new models\Products();
            $product->setProduct($this->put('product'));
            if ($this->put("image"))$product->setImage($this->put('image'));
            if ($this->put("reference"))$product->setReference($this->put('reference'));
            $product->setLastUpdate(new DateTime($this->put('last_update')));
            $product->setPrice($this->put('price'));
            $product->setMarketPrice($this->put('market_price'));
            $product->setAlternativeCost($this->put('alternative_cost'));
            $product->setStock($this->put('stock'));
            $product->setOutstanding($this->put('outstanding'));
            $product->setDescription($this->put('description'));
            $product->setCost($this->put('cost'));
            $product->setMarketPrice($this->put('market_price'));
            $product->setTax($this->put('tax'));
            $product->setStatus($this->put('status'));
            $product->setOutstanding($this->put('outstanding'));
            $this->loadRepository("Brands");
            $brand = $this->Brands->find($this->put('brand'));
            $this->loadRepository("Categories");
            $product->setBrand($brand);
            $category = $this->Categories->find($this->put('category'));
            $product->setCategory($category);
            $this->loadRepository("PaymentMethods");
            $paymentMethod = $this->PaymentMethods->find($this->put('paymentMethod'));
            $product->setPaymentMethod($paymentMethod);
            $this->em->persist($product);
            $this->em->flush();
            $id = $product->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function product_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }
            
            $id           = $this->delete("id");
            $product = $this->em->find('models\Products', $id);
        
            $this->em->remove($product);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}