<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Cupons extends REST_Controller
{
	function cupon_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }
            
            $cupon = $this->em->find('models\Cupons', $this->get('id'));
            
            if ($cupon){
                $this->response(array("status"=>true, "data"=>$cupon->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function cupon_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $cupon = $this->em->find('models\Cupons', $this->post("id"));
            $code = substr(md5(rand(1,999999)),0,7);
            $cupon->setCode($code);
            $cupon->setFilter($this->post('filter'));
            if($this->post('filterId'))$cupon->setFilterId($this->post('filterId'));
            if($this->post('customer')){
                $this->loadRepository("Customers");
                $customer = $this->Customer->find($this->post('customer'));
                $cupon->setCustomer($customer);
            
            }
            $cupon->setDiscount($this->post('discount'));
            $end = new DateTime($this->post('end'));
            $cupon->setEnd($end);
            $this->em->persist($cupon);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function cupon_put()
    {
        try {
            $cupon = new models\Cupons();
            $code = substr(md5(rand(1,999999)),0,7);
            $cupon->setCode($code);
            $cupon->setFilter($this->put('filter'));
            if($this->put('filterId'))$cupon->setFilterId($this->put('filterId'));
            if($this->put('customer')){
                $this->loadRepository("Customers");
                $customer = $this->Customer->find($this->put('customer'));
                $cupon->setCustomer($customer);
            
            }
            $cupon->setDiscount($this->put('discount'));
            $end = new DateTime($this->put('end'));
            $cupon->setEnd($end);
            $this->em->persist($cupon);
            $this->em->flush();
            $id = $cupon->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function cupon_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }
            
            $id           = $this->delete("id");
            $cupon = $this->em->find('models\Cupons', $id);
        
            $this->em->remove($cupon);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}