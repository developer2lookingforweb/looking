<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Blogs extends REST_Controller
{
	function blog_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }

            $blog = $this->em->find('models\Blogs', $this->get('id'));

            if ($blog){
                $this->response(array("status"=>true, "data"=>$blog->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function blog_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }

            $blog = $this->em->find('models\Blogs', $this->post("id"));
            $blog->setTitle($this->post('title'));
            if($this->post('image'))$blog->setImage($this->post('image'));
            $blog->setDescription($this->post('description'));
            $blog->setContent($this->post('content'));
            $blog->setPublished($this->post('published'));
            $blog->setAlias($this->post('alias'));
            $blog->setLikes($this->post('likes'));
//            $blog->setCreatedBy($this->post('createdBy'));
//            $blog->setCreationDate($this->post('creationDate'));
            $this->em->persist($blog);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function blog_put()
    {
        try {
            $blog = new models\Blogs();
            $blog->setTitle($this->put('title'));
            if($this->put('image'))$blog->setImage($this->put('image'));
            $blog->setDescription($this->put('description'));
            $blog->setContent($this->put('content'));
            $blog->setPublished($this->put('published'));
            $blog->setAlias($this->put('alias'));
            $blog->setLikes($this->put('likes'));
            $blog->setCreatedBy($this->put('createdBy'));
            $blog->setCreationDate(new DateTime($this->put('creationDate')));
            $this->em->persist($blog);
            $this->em->flush();
            $id = $blog->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function blog_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }

            $id           = $this->delete("id");
            $blog = $this->em->find('models\Blogs', $id);

            $this->em->remove($blog);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}