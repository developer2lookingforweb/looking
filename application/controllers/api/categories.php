<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Categories extends REST_Controller
{
	function category_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }
            
            $category = $this->em->find('models\Categories', $this->get('id'));
            
            if ($category){
                $this->response(array("status"=>true, "data"=>$category->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function category_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }
        
            $category = $this->em->find('models\Categories', $this->post("id"));
            if(false !==($this->post('category')))$category->setCategory($this->post('category'));
            if(false !==($this->post('image')))$category->setImage($this->post('image'));
            if(false !==($this->post('status')))$category->setStatus($this->post('status'));
            if(false !==($this->post('home')))$category->setHome($this->post('home'));
            if(false !==($this->post('order')))$category->setOrder($this->post('order'));
            if(false !==($this->post('parent')))$category->setParent($this->post('parent'));
            $this->em->persist($category);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function category_put()
    {
        try {
            $category = new models\Categories();
            $category->setCategory($this->put('category'));
            if(false !==($this->put('image')))$category->setImage($this->put('image'));
            if(false !==($this->put('status')))$category->setStatus($this->put('status'));
            if(false !==($this->put('home')))$category->setHome($this->put('home'));
            if(false !==($this->put('order')))$category->setOrder($this->put('order'));
            $this->loadRepository("Categories");
            $obj = $this->Categories->find($this->put('parent'));
            if(false !==($this->put('parent')))$category->setParent($obj);
            $this->em->persist($category);
            $this->em->flush();
            $id = $category->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function category_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }
            
            $id           = $this->delete("id");
            $category = $this->em->find('models\Categories', $id);
        
            $this->em->remove($category);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}