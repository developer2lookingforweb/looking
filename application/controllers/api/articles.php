<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Articles extends REST_Controller
{
	function article_get()
    {
        try{
            if(!$this->get('id')){
            	$this->response(NULL, 400);
            }

            $article = $this->em->find('models\Articles', $this->get('id'));

            if ($article){
                $this->response(array("status"=>true, "data"=>$article->toArray()), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function article_post()
    {
        try {
            if (!$this->post("id")){
            	$this->response(NULL, 400);
            }

            $article = $this->em->find('models\Articles', $this->post("id"));
            $article->setTitle($this->post('title'));
			$article->setBanner($this->post('banner'));
			$article->setShowBanner($this->post('showBanner'));
			$article->setContent($this->post('content'));
			$article->setPublished($this->post('published'));
			$article->setAlias($this->post('alias'));
//			$article->setCreatedBy($this->post('createdBy'));
//			$article->setCreationDate($this->post('creationDate'));
            $this->em->persist($article);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function article_put()
    {
        try {
            $article = new models\Articles();
            $article->setTitle($this->put('title'));
            $article->setBanner($this->put('banner'));
            $article->setShowBanner($this->put('showBanner'));
            $article->setContent($this->put('content'));
            $article->setPublished($this->put('published'));
            $article->setAlias($this->put('alias'));
            $article->setCreatedBy($this->put('createdBy'));
            $article->setCreationDate(new DateTime($this->put('creationDate')));
            $this->em->persist($article);
            $this->em->flush();
            $id = $article->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function article_delete()
    {
        try {
            if (!$this->delete("id")){
            	$this->response(NULL, 400);
            }

            $id           = $this->delete("id");
            $article = $this->em->find('models\Articles', $id);

            $this->em->remove($article);
            $this->em->flush();
            $this->response(array('status' => true), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}