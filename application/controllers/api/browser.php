<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Browser extends REST_Controller
{
    function search_get()
    {
           try {
               $cat = $this->get("cat");
               $data = $this->get("data");
               $price = objectToArray(json_decode($this->get("price")));
               $brand = $this->get("brand");
//               var_dump(objectToArray($price));
   //            if(count($data)==0 || $data === false){
   //            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
   //            }
   //            var_dump(($data));
               $var = array();
   //            if(count($data)==0 || $data === false){
               foreach (json_decode($data) as $index => $value) {
                   if ($value != 'fWDnxEXM _csrf_token_name') {
   //                        if($value !== reset(json_decode($data)))$var .= ",";
                       if (is_array($value)) {
                           foreach ($value as $option) {
                               if ($option != "")
                                   $var[] = $option;
                           }
                       } else {
                           if (!empty($value))
                               $var[] = $value;
                       }
                   }
               }
   //            }
                if( $cat === false){
                     $sqlCat = "select distinct(c.id) from models\\OptionsTags op join op.option opt join opt.question q join q.category c
                   where opt.id in (" . implode(",", $var) . ")
                   ";
                   $querySearch = $this->em->createQuery($sqlCat);
                   $cat = $querySearch->getResult();
                   $cat = $cat[0]['id'];

                }
               //Query para identificar la categoria
               if (count($var) > 0) {

                   $sqlCat = "select distinct(c.id) from models\\OptionsTags op join op.option opt join opt.question q join q.category c
                   where opt.id in (" . implode(",", $var) . ")
                   ";
                   $querySearch = $this->em->createQuery($sqlCat);
                   $category = $querySearch->getResult();
                   $category = $category[0]['id'];

                   //Querypara obtener los los valores a evaluar segun las respuestas obtenidas
                   $sql = "select op.tag, min(op.min) ,max(op.max)  from models\\OptionsTags op join op.option opt
                    where opt.id in (" . implode(",", $var) . ") and op.status = 1 
                    group by op.tag";
                   $querySearch = $this->em->createQuery($sql);
                   $results = $querySearch->getResult();
//               var_dump($results);//exit;
                   $arrayIds = array();
                   $extraarrayIds = array();
                   if (count($results) > 0 || $results === false) {
                       foreach ($results as $index => $value) {
   //                    $dql .= "AND prod.id in (";
   //                    if($value !== reset(json_decode($data)))$dql .= "OR";
                           $unlimited = '';
                           switch ($value['tag']) {
                               case 'MINUTOS':
                                   $unlimited = " or pr_$index.value = 9999";
   //                            $unlimited .= " or pr_$index.value = 0";
                                   break;
                               case 'DATOS':
                                   $unlimited = " or pr_$index.value = 999";
   //                            $unlimited .= " or pr_$index.value = 0";
                                   break;
                           }

                           //Query para obtener los ids por los rangos seleccionados
                           $dql = "select distinct (pd_$index.id) from models\\Properties pr_$index join pr_$index.product pd_$index join pd_$index.category ct_$index
                               where  pd_$index.status = 1 and pr_$index.property = '" . $value["tag"] . "' and ((pr_$index.value >= " . floor($value["1"]) . " and pr_$index.value <= " . ceil($value["2"]) . ") " . $unlimited . ") and ct_$index.id =" . $category;
                           //Query para obtener los rangos  30% mas amplios 
                           $extradql = "select distinct (pd_$index.id) from models\\Properties pr_$index join pr_$index.product pd_$index join pd_$index.category ct_$index
                               where  pd_$index.status = 1 and pr_$index.property = '" . $value["tag"] . "' and ((pr_$index.value >= " . ($value["1"] * 0.7) . " and pr_$index.value <= " . ($value["2"] * 1.3) . ") " . $unlimited . ") and ct_$index.id =" . $category;
   //                    $dql .= ")";
                           $querySearch = $this->em->createQuery($dql);
                           $results = $querySearch->getResult();
//                       var_dump($results);
   //                    $arrayIds[$index] = array();
                           foreach ($results as $data) {
                               $arrayIds[$index][] = $data["id"];
                           }
                           $querySearch = $this->em->createQuery($extradql);
                           $extraresults = $querySearch->getResult();
   //                    var_dump($results);exit;
                           $extraarrayIds[$index] = array();
                           foreach ($extraresults as $data) {
                               $extraarrayIds[$index][] = $data["id"];
                           }
                       }
                   }
               }
   //            var_dump($arrayIds);exit;
               $dql = "SELECT prod.id, "
                       . "prod.product, "
                       . "prod.description, "
                       . "prod.cost, "
                       . "prod.price, "
                       . "prod.stock, "
                       . "prod.alternative_cost, "
                       . "prod.market_price, "
                       . "prod.image, "
                       . "prod.last_update, "
                       . "prov.id provid,  "
                       . "prov.brand brand,  "
                       . "payment.id method, "
                       . "cat.id category "
                       . "FROM models\\Products prod ";
   //            $dql .= "JOIN models\\Properties prop with (prop.product = prod) ";            
   //            $dql .= "JOIN models\\Providers prov with (prod.provider = prov)";            
               $dql .= "JOIN prod.brand prov ";
               $dql .= "JOIN prod.category cat ";
               $dql .= "JOIN prod.payment_method payment ";
               $dql .= "WHERE  prod.status =1 AND prod.cost > 0 AND prod.category =" . $cat . " ";
               if($brand !="" && $brand > 0){
                    $dql .= "AND prod.brand =" . $brand . " ";
               }
               if(is_array($price) && count($price)==2){
                    if($cat==9){
                         $dql .= "AND (prod.alternative_cost >" . $price[0] . " AND prod.alternative_cost <" . $price[1] . " )";
                    }else{
                         $dql .= "AND (prod.cost >" . $price[0] . " AND prod.cost <" . $price[1] . " )";
                    }
               }
               $extradqlFinal = $dql;
   //            var_dump($arrayIds);

               if (isset($arrayIds)) {
                   if (count($arrayIds) > 0) {
                       $dql.= " AND ( ";
                       foreach ($arrayIds as $index => $aTag) {
                           if (count($aTag) > 0) {
                               if ($index > 0)
                                   $dql .= "AND ";

                               $dql .= " prod.id in (" . implode(",", $aTag) . ") ";
                           }
                       }
                       $dql .= ") ";
                   }
                   if (count($extraarrayIds) > 0) {
                       $extradqlFinal .= "AND ( ";
                       foreach ($extraarrayIds as $index => $aTag) {
                           if (count($aTag) > 0) {
                               if ($index > 0)
                                   $extradqlFinal .= "AND ";
                               $extradqlFinal .= " prod.id in (" . implode(",", $aTag) . ") ";
                           }
                       }
                       $extradqlFinal .= ") ";
                   }
               }

               if ($cat == 2 || $cat == 6)
                   $dql .= " GROUP by prov.id ";
               $dql .= " ORDER by prod.cost desc ";
//               echo $dql;
               $querySearch = $this->em->createQuery($dql)->setMaxResults(90);
               $results = $querySearch->getResult();

//               var_dump($results);
               if ($cat == 2) {
                   $exclude = array();
                   foreach ($results as $row) {
                       foreach ($row as $idx => $val) {
                           if ($idx == "provid") {
                               $exclude[] = $val;
                           }
                       }
                   }
                   if (count($exclude) > 0)
                       $extradqlFinal .= " AND prov.id not in (" . implode(",", $exclude) . ") ";
                   $extradqlFinal .= " GROUP by prov.id ";
                   $extradqlFinal .= " ORDER by prod.cost desc ";

   //                var_dump($extradqlFinal);

                   $querySearch = $this->em->createQuery($extradqlFinal)->setMaxResults(90);
                   $results2 = $querySearch->getResult();
                   $resultUnion = array_merge($results, $results2);
               }else {
                   $resultUnion = $results;
               }
   //            var_dump($extradqlFinal);
               if (count($resultUnion) > 0) {
                   $this->response(array("status" => true, "data" => $resultUnion), 200);
               } else {
                   $this->response(array("status" => false, 'error' => 'no-data'), 404);
               }
           } catch (PDOException $e) {
               $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
           } catch (Doctrine\DBAL\DBALException $e) {
               $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
           } catch (Doctrine\ORM\ORMException $e) {
               $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
           } catch (Exception $e) {
               $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
           }
       }

    function advisers_get()
    {
        try{
            $range = json_decode($this->get("range"));
            $property = json_decode($this->get("property"));
            $counter=$current = 0;
            $betterDql = "";
            $dql = "select b.id from models\\Products b
            where b.status = 1 ";
            foreach ($range as $key => $value) {
                $dql .= "AND b.id IN( select prd$key.id from models\Properties p$key 
                join p$key.product prd$key 
                where prd$key.status = 1
                AND (p$key.property = '".$property[$key]."' AND (p$key.value >= ".$value[0]." AND p$key.value<= ".$value[1].")))";
//                echo $dql;
                $querySearch = $this->em->createQuery($dql);
                $results = $querySearch->getResult();
                $current = count($results);
                if($key == 0){
                    $counter = $current;
                }
                if($current <= $counter && $current>0){
                    $betterDql = $results;
                }
            }
            $dqlIn = "(";
            foreach ($betterDql as $key => $data) {
                if($key !==0 ) $dqlIn .= ",";
                $dqlIn .= $data["id"];
            }
            $dqlIn .= ")";
            $dql = "SELECT prod.id, "
                       . "prod.product, "
                       . "prod.description, "
                       . "prod.cost, "
                       . "prod.price, "
                       . "prod.stock, "
                       . "prod.alternative_cost, "
                       . "prod.market_price, "
                       . "prod.image, "
                       . "prod.last_update, "
                       . "prov.id provid,  "
                       . "prov.brand brand,  "
                       . "payment.id method, "
                       . "cat.id category "
                       . "FROM models\\Products prod ";
               $dql .= "JOIN prod.brand prov ";
               $dql .= "JOIN prod.category cat ";
               $dql .= "JOIN prod.payment_method payment ";
               $dql .= "WHERE  prod.id IN $dqlIn ";
               $querySearch = $this->em->createQuery($dql);
                $results = $querySearch->getResult();
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

       function directSearch_get()
    {
        try{
            $data = $this->get("data");
            if(count($data)==0 || $data === false){
            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
            }
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category   FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
            if(is_array(objectToArray(json_decode($data)))){
                $dql .= "AND ( ";
                foreach (objectToArray(json_decode($data))  as $index => $value) {
                    if($index > 0 ) $dql .= " OR ";
                    $dql .= " (prod.product like '%".trim($value)."%' OR prov.brand like '%".trim($value)."%' )";

                }
                $dql .= ") ";
            }else{
                $dql .= "AND ( ";
                $dql .= " prod.product like '%".trim($data)."%' OR prov.brand like '%".trim($data)."%'";
                $dql .= ") ";
            }
            if($this->get('cat')){
                $dql .= "AND cat.id = ".$this->get('cat');
            }
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.cost desc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
       function brand_get()
    {
        try{
            $bid = $this->get("bid");
            if( $bid === false){
            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
            }
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock,  prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category   FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
            $dql .= "AND ( prov.id = '$bid' )";
            if($this->get('cat')){
                $dql .= "AND cat.id = ".$this->get('cat');
            }
                
            
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.cost desc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function outstanding_get()
    {
        try{
//            $data = $this->get("data");
//            if(count($data)==0 || $data === false){
//            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
//            }
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
            $dql .= "AND prod.outstanding = 1 ";
            if($this->get('cat')){
                $dql .= "AND cat.id = ".$this->get('cat');
            }
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by cat.id asc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function last_updated_get()
    {
        try{
//            $data = $this->get("data");
//            if(count($data)==0 || $data === false){
//            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
//            }
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
//            $dql .= "AND prod.outstanding = 1 ";
            if($this->get('cat')){
                $dql .= "AND cat.id = ".$this->get('cat');
            }
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.last_update desc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql)->setMaxResults(20);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function top_sales_get()
    {
        try{
//            $data = $this->get("data");
//            if(count($data)==0 || $data === false){
//            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
            $dql = " Select p.id, p.product, p.description, p.cost, p.price, p.stock, p.alternative_cost, p.last_update, p.market_price, p.image, b.id provid, b.brand brand,  pm.id method, c.id category, sum(po.quantity) conteo "
                    . "from models\\ProductsOrders po "
                    . "join po.product p "
                    . "join po.order o "
                    . "join p.brand b "
                    . "join p.category c "
                    . "join p.payment_method pm "
                    . "where p.status =1 and o.status = 4 "
                    . "Group by p.id "
                    . "order by conteo DESC";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql)->setMaxResults(20);
            $results = $querySearch->getResult();
////            var_dump($results);
//            $ids = array();
//            foreach ($results as $key => $row) {
//                if($row["conteo"]>3){
//                    $ids[] = $row["id"];
//                    
//                }
//            }
////            var_dump($ids);
////            }
//            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category FROM models\\Products prod ";
//            $dql .= "JOIN prod.brand prov ";            
//            $dql .= "JOIN prod.category cat ";            
//            $dql .= "JOIN prod.payment_method payment ";            
//            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
//            $dql .= "AND prod.id in (".implode(",",$ids).")";
//            if($this->get('cat')){
//                $dql .= "AND cat.id = ".$this->get('cat');
//            }
////            $dql .= " GROUP by prov.id ";
//            $dql .= " ORDER by prod.last_update asc ";
////            echo $dql;
//            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
//            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function related_get()
    {
        try{
            $pid = $this->get("pid");
            if( $bid === false){
            	$this->response(array("status"=>false, 'error' => 'Please add more filers'), 404);
            }
            $this->loadRepository("ProductsTags");
            $propertyArray = $this->ProductsTags->findBy(array("products"=>$pid  ));
            $tags = array();
            foreach ($propertyArray as $related){
                $tags[] = $related->getTag()->getId();
            }
            $query = $this->ProductsTags->createQueryBuilder('o')
                ->select( "distinct(p.id)" )
                    ->join("o.products"," p")
                ->where("o.tags in (".implode(",", $tags).")")
                ->andWhere("p.id < > ".$pid)
                ->getQuery();
            $ids = $query->getResult();
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
            $dql .= "AND prod.id in (".implode(",",$ids).")";
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.last_update asc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function favorites_get()
    {
        try{
            $data = $this->get("id");
            if(count($data)==0 || $data === false){
            	$this->response(array("status"=>false, 'error' => 'No such user'), 404);
            }
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.stock, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category ";
            $dql .= "FROM models\\Favorites fav ";            
            $dql .= "JOIN fav.product prod ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 ";
            $dql .= "AND fav.customer = ".$data;
            if($this->get('cat')){
                $dql .= "AND cat.id = ".$this->get('cat');
            }
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.cost desc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function total_sales_get()
    {
        try{
            
            $dql = "SELECT MAX(o.invoice) total ";
            $dql .= "FROM models\\Orders o ";            
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function total_customers_get()
    {
        try{
            
            $dql = "SELECT COUNT(c.id) total ";
            $dql .= "FROM models\\Customers c ";            
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    function total_products_get()
    {
        try{
            
            $dql = "SELECT COUNT(p.id) total ";
            $dql .= "FROM models\\Products p ";            
            $dql .= "JOIN p.category c ";            
            $dql .= "WHERE p.status = 1 AND p.stock > 0 AND c.status = 1 AND c.id <> 9 ";            
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            
//            var_dump($results);
            if(count($results)>0){
                $this->response(array("status"=>true, "data"=>$results), 200);
            } else {
                $this->response(array("status"=>false, 'error' => 'no-data'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

	function list_get()
    {
        try{
            $this->loadRepository("Cities");
            
            $cities = $this->Cities->findAll();
            
            if(is_numeric($this->get('country'))){
                $cities = $this->Cities->findBy(array("country"=>$this->get("country")));
            }
            
            if($cities){
                $return = array();
                
                foreach($cities as $aCity){
                    $return[$aCity->getId()] = $aCity->toArray(); 
                }
                
                $this->response(array("status"=>true, "data"=>$return, 200));
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    

	function list_by_user_get()
    {
        try{
            $this->loadRepository("Cities");
            
            $cities = $this->Cities->findAll();
            
            $country = "";
            if(is_numeric($this->get('user'))){
                $user = $this->em->find('models\Users', $this->get('user'));
                $cities = $this->Cities->findBy(array("country"=>$user->getCity()->getCountry()->getId()));
                $country = $user->getCity()->getCountry()->toArray();
            }
            
            if($cities){
                $return = array();
                
                foreach($cities as $aCity){
                    $return[$aCity->getId()] = $aCity->toArray(); 
                }
                
                $this->response(array("status"=>true, "data"=>$return, "country"=>$country, 200));
            } else {
                $this->response(array("status"=>false, 'error' => 'Register could not be found'), 404);
            }
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
    
    function favorite_post()
    {
        try {
            if (!$this->post("user") || !$this->post("product")){
            	$this->response(NULL, 400);
            }
        
            $customer = $this->em->find('models\Customers', $this->post("user"));
            $product = $this->em->find('models\Products', $this->post("product"));
            
            $favorite = new models\Favorites();
            $favorite->setCustomer($customer);
            $favorite->setProduct($product);
            
            $this->em->persist($favorite);
            $this->em->flush();
            
            $id = $favorite->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function city_put()
    {
        try {
            $country = $this->em->find('models\Countries', $this->put("idCountry"));
            
            $city = new models\Cities();
            $city->setName($this->put("name"));
            $city->setCountry($country);
            
            $this->em->persist($city);
            $this->em->flush();
            
            $id = $city->getId();
            $this->response(array('status' => true, 'id' => $id), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }

    function favorite_delete()
    {
        try {
            if (!$this->delete("user") || !$this->delete("product")){
            	$this->response(array('status' => "data required"), 400);
            }
        
            $this->loadRepository("Favorites");
            $favorites       = $this->Favorites->findBy(array('customer'=>$this->delete("user"),'product'=>$this->delete("product")));
            
            $status = false;
            if (count($favorites)>0) {
                foreach ($favorites as $aFavorite) {
                    $this->em->remove($aFavorite);
                    $this->em->flush();
                    $status = true;
                }
            }
            $this->response(array('status' => $status), 200);
        } catch (PDOException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\DBAL\DBALException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Doctrine\ORM\ORMException $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        } catch (Exception $e) {
            $this->response(array('status' => false, 'error' => $e->getMessage()), 400);
        }
    }
}