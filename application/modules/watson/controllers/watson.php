<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Watson extends MY_Controller 
{
        public $public = true;
	public function index()
	{
		
            if(! $this->session->userdata(AuthConstants::WSON_CONVID)){
                $this->session->set_userdata(AuthConstants::WSON_CONVID,json_encode(array("conversation_id"=>"")));
            }
            $this->load->library('WatsonConversation');
            
//            $_SESSION['context'] = json_encode(array("conversation_id"=>"866c5224-87d2-40b6-86bb-d418d6f45b96"));
            $watson = new WatsonConversation();
            $watson->set_credentials(AuthConstants::WSON_USER, AuthConstants::WSON_PASSWD);
            
            if($this->input->post("msg") && $this->input->post("msg")!=""){
                $data_arr = $watson->send_watson_conv_request($this->input->post("msg"), AuthConstants::WSON_WORKSPACE);
                $watson->set_context(json_encode($data_arr['context']));
                $this->session->set_userdata(AuthConstants::WSON_CONVID,json_encode($data_arr['context']));
                
            }else{
                $data_arr = $watson->send_watson_conv_request("iniciaNuevoChat", AuthConstants::WSON_WORKSPACE);
                $watson->set_context(json_encode($data_arr['context']));
                $this->session->set_userdata(AuthConstants::WSON_CONVID,json_encode($data_arr['context']));
                
                
            }
            echo json_encode(array('status'=>true,'message'=>$data_arr['output']['text'][0]));
	}
	
	public function setListParameters()
	{
        
        }
    
}