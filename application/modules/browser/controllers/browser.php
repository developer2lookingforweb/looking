<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Browser extends MY_Controller 
{
    public $public = true;
    public function setListParameters(){}
    

    public function index()
	{
		$data = array();
		$data["title"] = AuthConstants::ML_INFONAME;
                if(! $this->session->userdata(AuthConstants::CART)){
                    $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
                }
                if($this->input->get('msg') !== false){
                    $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
                    $message = explode("--", $this->input->get('msg'));
                    $data["message"] = lang($message[0]).$message[1];                    
                }
                $data["mainCategory"] = 1;
                $data["categories"] = $this->getSubcategories(1);
                $data["categories3"] = $this->getSubcategories(8);
		$this->viewPublic('home', $data);
	}
	public function people()
	{
		$data = array();
		$data["title"] = "Browser";
		
		$this->viewPublic('browser', $data);
	}
	public function bussiness()
	{
		$data = array();
		$data["title"] = "Browser";
		
		$this->viewPublic('bussiness', $data);
	}
	public function mobile()
	{
		$data = array();
		$data["title"] = "Browser";
		
		$this->viewPublic('mobile', $data);
	}
        
        
        public function browse() {
            $data = array();
            $data = $this->input->post('questions');
            $output = $this->rest->get('browser/search/', array("data"=>json_encode($data)));
            echo json_encode($output);
        
        }
        public function results() {
            $postData = $this->input->post('questions');
            $option = $this->em->find('models\Options', $postData[0]);
//            $data[] = $this->input->post('navegacion');
//            $data[] = $this->input->post('app');
//            var_dump($data);exit;
            
            
            $customer = $this->getCustomer($this->input->post('email'));
            $output = $this->rest->get('browser/search/', array("data"=>json_encode($postData)));
            $this->load->helper('audits');
            $obj = new Audits();
            $obj->addSearchAudit(json_encode($postData),$customer);
//            var_dump($output);exit;
            $questions = $this->getQuestions($option->getQuestion()->getCategory()->getId());
            $data = array();
            $data["title"] = $option->getQuestion()->getCategory()->getCategory();
            $data["minutos"] = $this->input->post('minutos');
            $data["navegacion"] = $this->input->post('navegacion');
            $data["answers"] = array();
            if (count($postData) >0) {
                foreach ($postData as $return) {
                    if($return>0){
                        $selected = $this->em->find('models\Options', $return);
                        $data["answers"][$selected->getQuestion()->getId()] = $return;
                    }
                }
            }
            $data["app"] = $this->input->post('app');
            $data["category"] = $option->getQuestion()->getCategory()->getCategory();
            $data["mainCategory"] = $option->getQuestion()->getCategory()->getParent()->getCategory();
            $this->session->set_userdata(AuthConstants::CART_OWNER, $this->input->post('email'));
            $data["theme"] = "cat".$option->getQuestion()->getCategory()->getParent()->getId();
            $data["questions"] = $questions;
            $data["results"] = array();
            if(isset($output->status) && $output->status){
                
                $data["results"] = $output->data;
            }
//            var_dump($data);exit;
            $this->viewPublic('results', $data);
        }
        public function directResults() {
            $postData = $this->input->post('globalSearch');
//            var_dump($postData);exit;
            $option = $this->em->find('models\Options', $postData[0]);
//            $data[] = $this->input->post('navegacion');
//            $data[] = $this->input->post('app');
//            var_dump($data);exit;
            
            
            $customer = $this->getCustomer($this->input->post('email'));
            $output = $this->rest->get('browser/directSearch/', array("data"=>json_encode(explode(" ", $postData))));
//            $questions = $this->getQuestions($option->getQuestion()->getCategory()->getId());
//            var_dump($questions);exit;
            $data = array();
//            $data["title"] = $option->getQuestion()->getCategory()->getCategory();
            $data["title"] = $postData;
            $data["minutos"] = $this->input->post('minutos');
            $data["navegacion"] = $this->input->post('navegacion');
            $data["answers"] = array();
//            if (count($postData) >0) {
//                foreach ($postData as $return) {
//                    if($return>0){
//                        $selected = $this->em->find('models\Options', $return);
//                        $data["answers"][$selected->getQuestion()->getId()] = $return;
//                    }
//                }
//            }
            $data["app"] = $this->input->post('app');
            $data["category"] = "Búsqueda Directa";
            $data["mainCategory"] = "";
            $this->session->set_userdata(AuthConstants::CART_OWNER, $this->input->post('email'));
//            $data["theme"] = "cat".$option->getQuestion()->getCategory()->getParent()->getId();
            $data["theme"] = "cat2";
            $data["questions"] = array();
            $data["results"] = array();
            if(isset($output->status) && $output->status){
                
                $data["results"] = $output->data;
            }
            $this->viewPublic('results', $data);
        }
        
        public function compare() {
            $comparison = $this->input->post('comparison');
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
            foreach ($comparison as $key => $value) {
                $prod = array();
                $product = $this->em->find('models\Products', $value);
                if($product)$response['status']=true;
                $prod['id']= $product->getId();
                $prod['name']= $product->getProduct();
                $prod['description'] = $product->getDescription();
                $prod['image'] = $product->getImage();
                $prod['cost'] = $product->getCost();
                $propertyArray = $this->Properties->findBy(array("product"=>$product->getid()));
                $props = array();
                foreach ($propertyArray as $property) {
                    $propsDetails = array();
                    $propsDetails['property'] = $property->getProperty();
                    $propsDetails['value'] = $property->getValue();
                    $props[] = $propsDetails;
                }
                $prod['details']= $props;
                $response['data'][] = $prod;
            }
            echo json_encode($response);
        }
        
        public function properties() {
            $productId = $this->input->post('id');
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
                $prod = array();
                $propertyArray = $this->Properties->findBy(array("product"=>$productId,'show_property'=>1));
                $props = array();
                $response['status']=true;
                foreach ($propertyArray as $property) {
                    $propsDetails = array();
                    $propsDetails['property'] = $property->getProperty();
                    $propsDetails['label'] = lang(strtolower($property->getProperty()));
                    if($property->getProperty()== 'COSTO' || $property->getValue() == 999 || $property->getValue() == 9999 || lang(strtolower($property->getProperty()."_unit")) == "$"){
                        if(($property->getValue() == 9999 || $property->getValue() == 999) && $property->getProperty()!= 'COSTO' && lang(strtolower($property->getProperty()."_unit"))!=="$"){
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".  lang("unlimited");                        
                        }else{
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".$property->getValue();                        
                        }
                    }else{
                        $propsDetails['value'] = $property->getValue()." ".lang(strtolower($property->getProperty()."_unit"));                                                
                    }
                    $props[] = $propsDetails;
                }
                $prod['details']= $props;
                $response['data']['details'] = $props;
            echo json_encode($response);
        }
        public function cartInfo() {
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $description_gifts = "";
            $subtotal = 0;
            $status = true;
            if(is_array($cart) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $cart[$index]['name'] = $product->getProduct();
                        $cart[$index]['cost'] = $product->getCost();
                        $cart[$index]['totalCost'] = $item['cost'];
                        $subtotal += $item['cost'];
                }
            }
            $data = array('cart'=>$cart,'total'=>$subtotal);
            $response = array('status'=>$status,'data'=>$data);
            echo json_encode($response);
        }
	public function addItem(){
            $data = $this->input->post();
////            var_dump($data);
//            $val = $this->session->userdata($data['type']);
//            $val[] = $data["id"];
//            $this->session->set_userdata(array($data['type'] => $val));
//            $val = $this->session->userdata($data['type']);
////            var_dump($val);
//            $data = array();
//            $data["title"] = "Results";
//            $data["category"] = "Móvil";
//            
            $id = $this->input->post('id');
            
            $quotes = $this->input->post('quotes');
            $cost = $this->input->post('cost');
            $available = $this->input->post('available');
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!$cart){
                $cart = array();
            }
            $return = "error";
            if ($id && $cost) {
                    if (count($cart) > 0) {
                            $insert = true;
                            foreach ($cart as $index => $item) {
                                    if ($id == $item['id']) {
//                                        if($cart[$index]['quotes']>=$available){
//                                            $insert = false;
//                                            $return = "unavailable";
//                                        }else{
                                            $cart[$index]['quotes'] = $cart[$index]['quotes'] + $quotes;
                                            $cart[$index]['cost'] = $cart[$index]['cost'] + $cost;
                                            $return = "added";
                                            $insert = false;
//                                        }
                                    }
                            }
                            if ($insert) {
                                    array_push($cart, array('id' => $id, 'cost' => $cost, "quotes" => $quotes));
                                    $return = "added";
                            }
                    } else {
                            array_push($cart, array('id' => $id,  'cost' => $cost, "quotes" => $quotes));
                            $return = "added";
                    }
            }
            $this->session->set_userdata(AuthConstants::CART, $cart);
            $json = array();
            if ($return == "added") {
                $json['status'] = true;
                $json['data'] = count($this->session->userdata(AuthConstants::CART));
            }else if($return == "unavailable"){
                $json['status'] = false;
                $json['data'] = "error";
            }
            
            echo json_encode($json);
        }
        public function editCart() {
		$gift = $this->input->post('id');
		$quotes = $this->input->post('quotes');
		$amount = $this->input->post('cost');

		$cart = $this->session->userdata(AuthConstants::CART);
		$return = "error";
		$giftValue = "";
		$subtotal = 0;
		$process = "";
		$total = 0;
		if ($gift && $quotes && $amount) {
			if (count($cart) > 0) {
				foreach ($cart as $index => $item) {
					if ($gift == $item['id']) {
						$limit = false;
//						$giftObj = $this->em->find('models\Products', $gift);
//						$query = $this->em->createQuery("SELECT sum(c.fees) as fees, sum(c.value) as total FROM models\Contributions c WHERE c.gift = " . $gift);
//						$currentFees = $query->getResult();
//
//						$currentFees = $currentFees[0]['fees'];
//						$totalFees = $giftObj->getNumFees();
//						$available = $totalFees - $currentFees;
//						if ($quotes > $available) {
//							$quotes = $available;
//							$limit = true;
//						}

						$cart[$index]['quotes'] = $quotes;
						$giftValue = $quotes * $amount;
						$cart[$index]['cost'] = $giftValue;
						$return = ($limit) ? $quotes : "added";
					}
					$subtotal = $subtotal + $cart[$index]['cost'];
				}
			}
		}
		$this->session->set_userdata(AuthConstants::CART, $cart);
		$process = number_format($this->getProccessCost($subtotal),0);
		$total = number_format(($subtotal + $this->getProccessCost($subtotal)), 0);
		$giftValue = number_format($giftValue, 0);
		$subtotal = number_format($subtotal, 0);
		echo json_encode(array("response" => $return, "gift" => $giftValue, "subtotal" => $subtotal, "process" => $process, "total" => $total));
	}

	public function deleteCart() {
		$gift = $this->input->post('id');
		$cart = $this->session->userdata(AuthConstants::CART);
		$return = "error";
		$giftValue = "";
		$subtotal = 0;
		$process = 0;
		$total = 0;
		if ($gift) {
                    if (count($cart) > 0) {
                        foreach ($cart as $index => $item) {
                            if ($gift == $item['id']) {

                                    unset($cart[$index]);
                                    $return = "added";
                            }
                            if (isset($cart[$index]))
                                    $subtotal = $subtotal + $cart[$index]['cost'];
                        }
                    }
		}
		$this->session->set_userdata(AuthConstants::CART, $cart);
		$process = number_format($this->getProccessCost($subtotal),0);
		$total = number_format(($subtotal + $this->getProccessCost($subtotal)), 0);
		$subtotal = number_format($subtotal, 0);
		echo json_encode(array("response" => $return, "subtotal" => $subtotal, "process" => $process, "total" => $total, "cartCount" => count($this->session->userdata(AuthConstants::CART))));
	}

	public function linkPayment($reference)
	{
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');
            $orderId = ltrim(ltrim($reference, "O"),"0");
            
            $order = $this->em->find('models\Orders', $orderId);
            if($order->getStatus()!==4 && $order->getResponsePol()!==1){
                $customer = $this->getCustomer($order->getCustomer()->getEmail());
    //            if($customer){
    //                $order      = $this->generateOrder($customer);
    //                if($order){
    //                    $fill       = $this->createFill($order);
    //                }
    //            }
                $newreference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
                $newreference = $newreference.time();
                $order->setOrder($newreference);
                $this->em->persist($order);
                $this->em->flush();
                $description_gifts = "";
                $subtotal = 0;
                $cart = array();
                $cart[0]=array();
                $cart[0]['id'] =  1;
                $cart[0]['name'] = AuthConstants::PAYU_RESEND.$reference;
                $cart[0]['image'] = "payu.png";
                $cart[0]['cost'] = $order->getValue();
                $cart[0]['brand'] = "";
                $cart[0]['number'] = "";
                $cart[0]['quotes'] = 1;
                $subtotal += $cart[0]['cost'];
    //            $reference = "O".str_pad($order->getId(), 5, "-=", STR_PAD_LEFT);
                $data['fill'] = true;
                $data['directPayment'] = true;
                $data['cid'] = $customer->getId();
                $data['name'] = $customer->getName();
                $data['last_name'] = $customer->getLastName();
                $data['dni'] = $customer->getDni();
                $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
                $data['docType'] = $docT;
                $data['email'] = $customer->getEmail();
                $data['mobile'] = $customer->getMobile();
                $data['phone'] = $customer->getPhone();
                $data['address'] = $customer->getAddress();
                $data['cart'] = $cart;
                $data['subtotal'] = $subtotal;
                $data['process'] = $this->getProccessCost($subtotal);
                $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
                $data['merchant'] = $payU->merchant;
                $data['account'] = $payU->account;
                $data['urlPayU'] = $payU->url;
                $data['description'] = "Valida pagos";
                $data['reference'] = $newreference;
                $data['amount'] = $subtotal;
                $data['currency'] = "COP";
                $data['signature'] = md5($payU->key."~".$payU->merchant."~".$newreference."~".$subtotal."~COP");
                $data['test'] = AuthConstants::PAYU_MODE;
                $data['buyerEmail'] = $customer->getEmail();
                $data['response'] = base_url().$payU->response.$newreference;
                $data['confirmation'] = base_url().$payU->confirmation.$newreference;
                $csrf = $this->config->config["csrf_token_name"];
                $data['csrf'] = $csrf;
    //            echo "<pre>"; var_dump($data);exit;
                $this->viewPublic('checkout', $data, $actions);
            }else{
                redirect("browser/?msg=tx_complete--".$reference);
            }
	}
	public function checkout()
	{
//		$data = array();
//		$data["title"] = "Checkout";
//		
//		$this->viewPublic('checkout', $data);
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');

            $this->loadRepository("UsersData");
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $description_gifts = "";
            $subtotal = 0;
            if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $cart[$index]['name'] = $product->getProduct();
                        $cart[$index]['image'] = $product->getImage();
                        $description_gifts .=  $product->getProduct() . ", ";
                        $subtotal += $item['cost'];
                }
            }
            $data['cart'] = $cart;
            $data['description_gifts'] = substr($description_gifts, 0, (strlen($description_gifts) -2));
            $data['subtotal'] = $subtotal;
            $data['process'] = $this->getProccessCost($subtotal);
            $data['publishable_key'] = '123123123';
            
            $customer = $this->getCustomer($this->session->userdata(AuthConstants::CART_OWNER));

            $data['cid'] = $customer->getId();
            $data['name'] = $customer->getName();
            $data['last_name'] = $customer->getLastName();
            $data['dni'] = $customer->getDni();
            $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
            $data['docType'] = $docT;
            $data['email'] = $customer->getEmail();
            $data['mobile'] = $customer->getMobile();
            $data['phone'] = $customer->getPhone();
            $data['address'] = $customer->getAddress();
            $data['country'] = 0;
            $data['state'] = 0;
            $data['city'] = 0;
            if($customer->getCity() !== NULL){
                $data['city'] = $customer->getCity()->getId();
                $data['state'] = $customer->getCity()->getState()->getId();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getId();
                
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            
            
            $data['client_id'] = '654654654';
            $data['env'] = '987987987';
            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $data['merchant'] = $payU->merchant;
            $data['account'] = $payU->account;
            $data['urlPayU'] = $payU->url;
            $data['description'] = "LookingForWeb (".count($cart).") Producto(s)";
            $data['reference'] = "";
            $data['amount'] = $subtotal;
            $data['currency'] = "COP";
            $data['signature'] = "";
            $data['test'] = AuthConstants::PAYU_MODE;
            $data['buyerEmail'] = $customer->getEmail();
            $data['response'] = base_url().$payU->response;
            $data['confirmation'] = base_url().$payU->confirmation;
            $csrf = $this->config->config["csrf_token_name"];
            $data['csrf'] = $csrf;
            $this->viewPublic('checkout', $data, $actions);

	}
	public function chargeCheckout(){

            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');
            $questions = $this->input->post('questions');
//            $this->loadRepository("Options");
            foreach ($questions as $option) {
                $aOption = $this->em->find('models\Options', $option);
                if($aOption !== null){
                    if($aOption->getQuestion()->getId() == 10){
                        $_POST['operador']= $aOption->getOption();
                    }else{
                        $_POST['valor']= $aOption->getOption();
                    }
                }else{
                    $_POST['numero']= $option;
                }
            }
//            var_dump($_POST);exit;
//            var_dump($questions);exit;
            $customer = $this->getCustomer($this->input->post('email'));
//            if($customer){
//                $order      = $this->generateOrder($customer);
//                if($order){
//                    $fill       = $this->createFill($order);
//                }
//            }
            $description_gifts = "";
            $subtotal = 0;
            $cart = array();
            $cart[0]=array();
            $cart[0]['id'] =  1;
            $cart[0]['name'] =  $this->input->post('operador')." Recargas";
            $cart[0]['image'] = $this->input->post('operador').".png";
            $cart[0]['brand'] = $this->input->post('operador');
            $cart[0]['number'] = $this->input->post('numero');
            $cart[0]['cost'] = $this->input->post('valor');
            $cart[0]['quotes'] = 1;
            $subtotal += $cart[0]['cost'];
//            $reference = "O".str_pad($order->getId(), 5, "-=", STR_PAD_LEFT);
            $data['fill'] = true;
            $data['cid'] = $customer->getId();
            $data['name'] = $customer->getName();
            $data['last_name'] = $customer->getLastName();
            $data['dni'] = $customer->getDni();
            $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
            $data['docType'] = $docT;
            $data['email'] = $customer->getEmail();
            $data['mobile'] = $customer->getMobile();
            $data['phone'] = $customer->getPhone();
            $data['address'] = $customer->getAddress();
            $data['country'] = 0;
            $data['state'] = 0;
            $data['city'] = 0;
            if($customer->getCity() !== NULL){
                $data['city'] = $customer->getCity()->getId();
                $data['state'] = $customer->getCity()->getState()->getId();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getId();
                
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            $data['cart'] = $cart;
            $data['subtotal'] = $subtotal;
            $data['process'] = $this->getProccessCost($subtotal);
            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $data['merchant'] = $payU->merchant;
            $data['account'] = $payU->account;
            $data['urlPayU'] = $payU->url;
            $data['description'] = $this->input->post('operador')." Recargas";;
            $data['reference'] = "";
            $data['amount'] = $subtotal;
            $data['currency'] = "COP";
            $data['signature'] = "";
            $data['test'] = AuthConstants::PAYU_MODE;
            $data['buyerEmail'] = $customer->getEmail();
            $data['response'] = base_url().$payU->response;
            $data['confirmation'] = base_url().$payU->confirmation;
            $csrf = $this->config->config["csrf_token_name"];
            $data['csrf'] = $csrf;
//            echo "<pre>"; var_dump($data);exit;
            $this->viewPublic('checkout', $data, $actions);
        }
        
        private function getProccessCost($amount){
            if($amount<=AuthConstants::PAYU_PROCCESSLIMIT){
                $process = AuthConstants::PAYU_PROCCESSMINCOST;
            }else{
                $process = $amount*AuthConstants::PAYU_PROCCESSPERCENTAGE;
            }
            $iva = $process * AuthConstants::PAYU_PROCCESSIVA;
            $fte = $amount * AuthConstants::PAYU_PROCCESSRETEFTE;
            $ica = $amount * AuthConstants::PAYU_PROCCESSRETEICA;
            return 0;
//            return ceil($process + $iva + $fte + $ica);
        }
        
	public function orderConfirmation(){
            $response = array("status"=>false,"data"=>"");
            $customer = $this->updateCustomer();
            if($customer){
                $cart = $this->session->userdata(AuthConstants::CART);
                if(!isset($cart)|| $cart == FALSE){
                    $cart = array();
                }
                $payable = $unpayable = 0;
                $amount = array();
                $payuamount = 0;
                if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                    foreach ($cart as $index => $item) {
                            $product = $this->em->find('models\Products', $item);
                            $pm = $product->getPaymentMethod()->getId();
                            if(! isset($amount[$pm]))$amount[$pm] = 0;
                            $amount[$pm] += $product->getCost();
                            if($pm == AuthConstants::PM_USE_PAYU){
                                $payuamount += $product->getCost();
                            }
                    }
                }
                foreach ($amount as $method => $value) {
                    $order = $this->generateOrder($customer, $value, $method);
                    if(isset($order)){
                        $reference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
                        $reference = $reference.time();
                        $order->setOrder($reference);
                        $this->em->persist($order);
                        $this->em->flush();
                    }
                    if(! $this->input->post('brand')){
                        $addItems = $this->assocItems($order,$method);
                        
                    }else{
                        $fill       = $this->createFill($orderP);
                    }
                }
                $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
//                    var_dump($payU->key."~".$payU->merchant."~".$reference."~".$this->input->post('total')."~COP");
                $data['amount'] = $amount;
//                $data['amount_manual'] = $unpayable;
                $data['reference'] = $reference;
                $data['address'] = $customer->getAddress();
                $data['city'] = $customer->getCity()->getName();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getCode();
                $data['signature'] = md5($payU->key."~".$payU->merchant."~".$reference."~".$payuamount."~COP");
                $data['response'] = base_url().$payU->response.$reference;
                $data['confirmation'] = base_url().$payU->confirmation.$reference;
                $response['status']=true;
                $response['data']=$data;
            }
            echo json_encode($response);
        }
	public function assocItems($order,$method){
            $cart = $this->session->userdata(AuthConstants::CART);
            if(is_array($cart)){
                foreach ($cart as $item){
                    $product = $this->em->find('models\Products',$item['id']);
                    $paymentMethod = $product->getPaymentMethod()->getId();
                    if($method == $paymentMethod){
                        $productItem = new models\ProductsOrders();
                        $productItem->setOrder($order);
                        $productItem->setProduct($product);
                        $productItem->setQuantity($item['quotes']);
                        $productItem->setValue($product->getCost());
                        $this->em->persist($productItem);
                        $this->em->flush();
                        if($method==AuthConstants::PM_GENERATE_CODE){
                            $code = new models\ProductsOrdersCodes();
                            $code->setProductOrder($productItem);
                            $code->setCode($productItem->getId().substr(md5(rand(3,5)),0,6));
                            $code->setRedeemed(0);
                            $code->setRedeemedDate(new DateTime(date("Y-m-d H:i:s")));
                            $code->setValue($product->getCost());
                            $this->em->persist($code);
                            $this->em->flush();
                        }
                    }
                }
            }
        }
	public function createFill($order){
            $fill = new models\MobileRefills();
            $fill->setOrder($order);
            $payment = $this->em->find('models\PaymentMethods', 1);
            $fill->setFilled(0);
            $this->loadRepository("Brands");
            $brand = $this->Brands->findOneBy(array('brand' => $this->input->post('brand')));
            $fill->setBrand($brand);
            $fill->setValue($this->input->post('total'));
            $fill->setNumber($this->input->post('number'));
            $this->em->persist($fill);
            $this->em->flush();
            return $fill;
        }
	public function generateOrder($customer,$value, $method =1){
            $order = new models\Orders();
            $order->setCustomer($customer);
            $payment = $this->em->find('models\PaymentMethods', $method);
            $order->setPaymentMethod($payment);
            $order->setOrderDate(new DateTime(date("Y-m-d H:i:s")));
            $order->setReference("");
//            $order->setOrder("");
            $order->setInvoice("");
            $order->setStatus(1);
            $order->setValue($value);
            $this->em->persist($order);
            $this->em->flush();
            return $order;
        }
	public function getCustomer($email){
            $this->loadRepository("Customers");
            $customer = $this->Customers->findBy(array('email' => $email));
            if(count($customer)===0){
                $customer = new models\Customers();
                $customer->setEmail($email);
//                $city = $this->em->find('models\Cities', 1);
//                $customer->setCity($city);
                $customer->setCreationDate(new DateTime(date("Y-m-d")));
                $this->em->persist($customer);
                $this->em->flush();
//                $id = $user->getId();
            }else{
                $customer = $customer[0];
            }
            return $customer;
        }
	public function updateCustomer(){
//            $this->loadRepository("Customers");
//            $customer = $this->Customers->find(array('email' => $this->input->post('email')));
            $customer = $this->em->find('models\Customers', $this->input->post('cid'));
            $customer->setName($this->input->post('name'));
            $customer->setLastName($this->input->post('last_name'));
            $customer->setAddress($this->input->post('address'));
            $city = $this->em->find('models\Cities', $this->input->post('city'));
            if($city !== null)$customer->setCity($city);
            $type = $this->em->find('models\DocumentTypes', $this->input->post('docType'));
            if($type !== null)$customer->setDocType($type);
            $customer->setDni($this->input->post('dni'));
            $customer->setMobile($this->input->post('mobile'));
            $customer->setPhone($this->input->post('phone'));
            $customer->setLastAccess(new DateTime(date("Y-m-d H:i:s")));
            $this->em->persist($customer);
            $this->em->flush();
//                $id = $user->getId();
            return $customer;
        }
	public function getPayUData($test = 1){
            $payU = new stdClass();
            if($test){
                $payU->url = AuthConstants::PAYU_URL_S;
                $payU->merchant = AuthConstants::PAYU_MERCHANT_S;
                $payU->account = AuthConstants::PAYU_ACOUNT_S;
                $payU->key = AuthConstants::PAYU_KEY_S;
            }else{
                $payU->url = AuthConstants::PAYU_URL;
                $payU->merchant = AuthConstants::PAYU_MERCHANT;
                $payU->account = AuthConstants::PAYU_ACOUNT;
                $payU->key = AuthConstants::PAYU_KEY;
                
            }
            $payU->response = AuthConstants::PAYU_RESPONSE;
            $payU->confirmation = AuthConstants::PAYU_CONFIRMATION;
            return $payU;
        }
	
        public function chargeConfirmation($reference){
            $this->chargeResponse($reference,FALSE);
        }
        public function chargeResponse($reference,$redirect = true){
//            echo "<pre>";
//            var_dump($_REQUEST);
            $response = $_REQUEST;
            if($redirect==true){
                $buyer = $response['buyerEmail'];
                $transactionValue = $response['TX_VALUE'];
                $referenceCode = $response['referenceCode'];
                $referencePol = $response['reference_pol'];
                $transactionState = $response['transactionState'];
                $polResponseCode = $response['polResponseCode'];
            }  else {
                $buyer = $response['email_buyer'];
                $transactionValue = $response['value'];
                $referenceCode = $response['reference_sale'];
                $referencePol = $response['reference_pol'];
                $transactionState = $response['state_pol'];
                $polResponseCode = $response['response_code_pol'];
            }
            $this->loadRepository("Orders");
            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $value = explode(".", $transactionValue);
            $email = $buyer;
            $orderId = ltrim(ltrim($referenceCode, "O"),"0");
            $order = $this->Orders->findOneBy(array("order_reference"=>$reference));
            $signature = md5($payU->key."~".$payU->merchant."~".$reference."~".$value[0]."~COP");
            $data = array();
            $reference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
//            $this->load->library('mailgunmailer');
            if($email== $order->getCustomer()->getEmail() && $value[0]== $order->getValue()){
                $order->setReference($referencePol);
                $order->setStatus($transactionState);
                $order->setResponsePol($polResponseCode);
                $this->em->persist($order);
                $this->em->flush();
                //recargar puntored
                $this->loadRepository("MobileRefills");
                $refill = $this->MobileRefills->findBy(array('order'=>$order->getId()));
//                var_dump(count($refill));
                if(count($refill)>0){
                    $data["messagge"] = "tx_complete_fill--".$reference;
//                    $this->load->library('consumer');
//                    $response  = $this->consumer->findPaquetigo('16');
//    //                var_dump($this->consumer);
//                    var_dump(strtolower($refill[0]->getBrand()->getBrand()));
//                    var_dump("movistar");
//                    var_dump($refill[0]->getNumber());
//                    var_dump($refill[0]->getValue());
//                    var_dump($refill[0]->getId());
                    include("consumerWS.php"); 
                    //
                    ////nueva recarga
                    $wsdl = new ConsumerWS();
//                    $wsdl->setOwner("movistar");
                    $wsdl->setOwner(strtolower($refill[0]->getBrand()->getBrand()));
                    $wsdl->setNumber($refill[0]->getNumber());
                    $wsdl->setValue($refill[0]->getValue());
                    $wsdl->setTrace($refill[0]->getId());
                    $rta = $wsdl->send();
//                    $wsdl->findPaquetigo('16');
//                    $wsdl->sendPaquetigo('16','3013213232');
//                    var_dump($response);
                    if($rta['codigoRespuesta']=='00'){
                        $reference[0]->setFilled(1);
                        $this->em->persist($reference[0]);
                        $this->em->flush();
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);
                        
                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"recargas",$params);

                    }else{
                        $to[]= array('name'=>  AuthConstants::ML_SUPPORTNAME,'mail'=>  AuthConstants::ML_SUPPORTEMAIL);


                        $params = array();
                        $params['*|FILLID|*'] = $refill[0]->getId();
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_ERRORSUBJECT,"errorPuntored",$params);

                        
                    }
//                    exit;
                }else{
                    if($transactionState == 4 && $polResponseCode == 1){
                        
                        $dqlInvoice = "select max(o.invoice) invoice from models\\Orders o ";
                        $querySearch = $this->em->createQuery($dqlInvoice);
                        $invoiceNum = $querySearch->getResult();
//                        var_dump($invoiceNum);
                        $invoiceNum = $invoiceNum[0]['invoice'];
//                        exit();
                        $order->setInvoice($invoiceNum + 1);
                        $this->em->persist($order);
                        $this->em->flush();
                        
                        $data["messagge"] = "tx_complete--".$reference;
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);
                        $this->loadRepository("ProductsOrders");
                        $products = $this->ProductsOrders->findBy(array('order'=>$order->getId()));
                        $listProducts = '<ul>';
                        foreach ($products as $item) {
                            $listProducts .= "<li>".$item->getProduct()->getBrand()->getBrand()." ".$item->getProduct()->getProduct()."</li>";
                            $item->getProduct()->setStock($item->getProduct()->getStock()- $item->getQuantity());
                            if($item->getProduct()->getStock() <= 0){
                                $item->getProduct()->setStatus(0);
                                //notify
                                $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
                                $params['*|PRODUCTS|*'] = $item->getProduct()->getProduct();
                                $params['*|CURRENT_YEAR|*'] = date("Y");
                                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                                $toL[]= array('name'=>  AuthConstants::ML_INFONAME,'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
                                $this->notify($toL,AuthConstants::ML_NEWTRANSACTION,"stockout",$params);
                            }
                            $this->em->persist($item);
                            $this->em->flush();
                        }
                        $listProducts .= '</ul>';
                        $listCustomer = '<ul>';
                        $listCustomer .= "<li>".lang("name").": ".$order->getCustomer()->getName()." ".$order->getCustomer()->getLastName()."</li>";
                        $listCustomer .= "<li>".lang("dni").": ".$order->getCustomer()->getDni()."</li>";
                        $listCustomer .= "<li>".lang("phone").": ".$order->getCustomer()->getPhone()."</li>";
                        $listCustomer .= "<li>".lang("mobile").": ".$order->getCustomer()->getMobile()."</li>";
                        $listCustomer .= "<li>".lang("address").": ".$order->getCustomer()->getAddress()."</li>";
                        $listCustomer .= "<li>".lang("city").": ".$order->getCustomer()->getCity()->getName()."</li>";
                        $listCustomer .= '</ul>';
                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|PRODUCTS|*'] = $listProducts;
                        $params['*|ADDRESS|*'] = $order->getCustomer()->getAddress();
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"transaccion",$params);
                        $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
                        $params['*|PRODUCTS|*'] = $listProducts;
                        $params['*|ADDRESS|*'] = $listCustomer;
                        $toL[]= array('name'=>  AuthConstants::ML_INFONAME,'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
                        $this->notify($toL,AuthConstants::ML_NEWTRANSACTION,"insidetransaccion",$params);
                        
                        $params['*|NAME|*'] = AuthConstants::ML_CEONAME;
                        
                        $toO[]= array('name'=>  AuthConstants::ML_INFONAME,'mail'=>  AuthConstants::ML_OWNEREMAIL);
                        $this->notify($toO,AuthConstants::ML_NEWTRANSACTION,"insidemoneytransaccion",$params);

                    }else if($transactionState == 6 && $polResponseCode != 19){
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);


                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|URL|*'] = base_url()."browser/linkPayment/".$reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_REJECTEDSUBJECT,"pagoRechazado",$params);

                        $data["messagge"] = "tx_incomplete--";
                        
                    }else if($transactionState == 7){
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);


                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_REJECTEDSUBJECT,"pendiente",$params);

                        $data["messagge"] = "tx_pending--";
                        
                    }
                }
            }else{
                echo "no corresponde";
                $data["messagge"] = "tx_incomplete--";
                //notificar error
            }
//            if(! $this->session->userdata(AuthConstants::CART)){
                $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
//            }
            if($redirect) redirect("browser/?msg=".$data['messagge']);
            else echo $data['messagge'];
        }
        
        public function notify($to,$subject,$template,$params){
            $this->load->library('mailgunmailer');
            $this->mailgunmailer->notify($to,$subject,$template,$params);
        }

        public function getCountries(){
            $this->loadRepository("Countries");
            $countries = $this->Countries->findAll();
            $return = array();
            foreach ($countries as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName());
            }
            return $return;
        }
        public function getDocTypes(){
            $this->loadRepository("DocumentTypes");
            $types = $this->DocumentTypes->findAll();
            $return = array();
            foreach ($types as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getDocumentType());
            }
            return $return;
        }
        public function getStates(){
            $this->loadRepository("States");
            $states = $this->States->findBy(array('country'=>  $this->input->post('countryId')));
            $return = array();
            foreach ($states as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName());
            }
            echo json_encode(array('status'=>true,'data'=>$return));
        }
        public function getCities(){
            $this->loadRepository("Cities");
            $cities = $this->Cities->findBy(array('state'=>  $this->input->post('stateId')));
            $return = array();
            foreach ($cities as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName());
            }
            echo json_encode(array('status'=>true,'data'=>$return));
        }
        public function getSubcategories($idCat){
            $this->loadRepository("Categories");
            $categories = $this->Categories->findBy(array("parent"=>$idCat,'status'=>1));
            $return = array();
            foreach ($categories as $value) {
                $action = ($value->getId()== 3)?  base_url("/browser/chargeCheckout"):base_url("/browser/results");
                $return[] = array("id"=>$value->getId(),"category"=>$value->getCategory(),'questions'=>  $this->getQuestions($value->getId()),"action"=>$action);
            }
            return $return;
        }
        public function getQuestions($idCat){
            $this->loadRepository("Questions");
            $questions = $this->Questions->findBy(array("category"=>$idCat,'status'=>1));
            $return = array();
            foreach ($questions as $value) {
                $return[] = array("id"=>$value->getId(),"question"=>$value->getQuestion(),"multiple"=>$value->getMultiple(),'options'=>  $this->getAnswers($value->getId()));
            }
            return $return;
        }
        public function getAnswers($idQuestion){
            $this->loadRepository("Options");
            $options = $this->Options->findBy(array("question"=>$idQuestion,'status'=>1));
            $return = array();
            foreach ($options as $value) {
                $return[] = array("id"=>$value->getId(),"option"=>$value->getOption());
            }
            return $return;
        }
            
        public function generatePdf(){
//            require_once APPPATH.'libraries/dompdf/autoload.inc.php';
//            // reference the Dompdf namespace
//
//            // instantiate and use the dompdf class
//            $dompdf = new Dompdf();
//            $dompdf->loadHtml('hello world');
//
//            // (Optional) Setup the paper size and orientation
//            $dompdf->setPaper('A4', 'landscape');
//
//            // Render the HTML as PDF
//            $dompdf->render();
//
//            // Output the generated PDF to Browser
//            $dompdf->stream();
//            $this->pdf->test();
            $this->load->library('pdf');
            $this->pdf->loadHtml('hello world');
            $this->pdf->setPaper('A4', 'landscape');
            $this->pdf->render();
            $this->pdf->stream("welcome.pdf");
        }
        public function invoice($reference){
            $orderId = ltrim(ltrim($reference, "O"),"0");
            
            $order = $this->em->find('models\Orders', $orderId);
            $customer = $order->getCustomer();
            $qr = $qrFile = "images/invoiceqr/$reference.png";
//            if(!file_exists($qrFile)){
                $this->load->library('qrcodelib');
                $qr = $this->qrcodelib->standardQr(base_url("browser/invoice/".$reference),  $qrFile);
//            }
            $this->loadRepository("ProductsOrders");
            $products = $this->ProductsOrders->findBy(array('order'=>$order->getId()));
            $items = array();
            if(count($products)>0){
                foreach ($products as $product) {
                    $items[]= array(
                        "cod"=>$product->getProduct()->getId(),
                        "product"=>$product->getProduct()->getProduct(),
                        "quantity"=>$product->getQuantity(),
                        "cost"=>$product->getValue(),
                        "total"=>($product->getValue() * $product->getQuantity()),
                    );
                }
            }else{
                $this->loadRepository("MobileRefills");
                $refill = $this->MobileRefills->findBy(array('order'=>$order->getId()));
                if(count($refill)==1){
                    $items[]= array(
                        "cod"=>$refill[0]->getId(),
                        "product"=>"Recargas productos móviles",
                        "quantity"=>1,
                        "cost"=>$refill[0]->getValue(),
                        "total"=>$refill[0]->getValue(),
                    );
                }
            }
//            var_dump($products);exit;
            $data = array();
            $data['qr'] = "images/invoiceqr/".$reference.".png";
            $data['items'] = $items;
            $data['subtotal'] = $order->getValue()/1.16;
            $data['iva'] = $order->getValue() - $data['subtotal'];
            $data['total'] = $order->getValue();
            $data['reference'] = str_pad($order->getInvoice(), 8, "0", STR_PAD_LEFT);
            $data['referenceDate'] = $order->getOrderDate()->format('Y/m/d');
            $data['customerName'] = $customer->getName()." ".$customer->getLastName();
            $data['customerDni'] = $customer->getDni();
            $data['customerAddress'] = $customer->getAddress();
            $data['customerCity'] = $customer->getCity()->getName();
            $data['customerState'] = $customer->getCity()->getState()->getName();
            $data['customerEmail'] = $customer->getEmail();
            $data['customerMobile'] = $customer->getMobile();
            $data['sellerName'] = AuthConstants::IN_SELLER_NAME;
            $data['sellerAddress'] = AuthConstants::IN_SELLER_ADDRESS;
            $data['sellerPhone'] = AuthConstants::IN_SELLER_PHONE;
            $data['sellerNit'] = AuthConstants::IN_SELLER_NIT;
            $data['sellerWeb'] = AuthConstants::IN_SELLER_WEB;
            $data['resolution'] = AuthConstants::IN_SELLER_RES;
            $data['resolution2'] = AuthConstants::IN_SELLER_RES2;
            $data['disclamer'] = AuthConstants::IN_SELLER_DISC;
            $data['title']= "Factura No ".$reference;
//            var_dump($data);exit;
//            $this->viewPublic('invoice', $data);
            $this->load->library('pdf');
            $this->pdf->load_view('invoice',$data);
            $this->pdf->render();
//            $this->pdf->set_base_path('css/normalize.css');
//            $this->pdf->set_base_path('css/foundation.css');
//            $this->pdf->set_base_path('css/app.css');
//            $this->pdf->set_base_path('css/fonts.css');
            $this->pdf->stream("$reference");
        }
        public function reservedOrder($order){
            $this->loadRepository("Orders");
//            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $orderId = ltrim(ltrim($order, "O"),"0");
            $orderOb = $this->Orders->findOneBy(array('order_reference'=>$order));
//            var_dump($orderOb);
            $fullName   = AuthConstants::ML_SUPPORTNAME;
            $email      = AuthConstants::ML_LOGISTICSEMAIL;
            $to[]= array('name'=>$fullName,'mail'=>$email);
            $fullName   = AuthConstants::ML_COONAME;
            $email      = AuthConstants::ML_OPERATIONSEMAIL;
            $to[]= array('name'=>$fullName,'mail'=>$email);
//            $to[]= array('name'=>$fullName,'mail'=>'john.jimenez@lookingforweb.com');
            
            $method = $orderOb->getPaymentMethod()->getId();
            
            $this->loadRepository("ProductsOrders");
            $products = $this->ProductsOrders->findBy(array('order'=>$orderOb->getId()));
            $listProducts = '<ul>';
            $listCodes = '<ul>';
            foreach ($products as $item) {
                $listProducts .= "<li>".$item->getProduct()->getBrand()->getBrand()." ".$item->getProduct()->getProduct()."</li>";
                $this->loadRepository("ProductsOrdersCodes");
                $code = $this->ProductsOrdersCodes->findOneBy(array('product_order'=>$item->getId()));
                if($code !== null)$listCodes .= "<li>".$code->getCode()."</li>";
            }
            $listProducts .= '</ul>';
            $listCodes .= '</ul>';
            
            $listCustomer = '<ul>';
            $listCustomer .= "<li>".lang("name").": ".$orderOb->getCustomer()->getName()." ".$orderOb->getCustomer()->getLastName()."</li>";
            $listCustomer .= "<li>".lang("dni").": ".$orderOb->getCustomer()->getDni()."</li>";
            $listCustomer .= "<li>".lang("phone").": ".$orderOb->getCustomer()->getPhone()."</li>";
            $listCustomer .= "<li>".lang("mobile").": ".$orderOb->getCustomer()->getMobile()."</li>";
            $listCustomer .= "<li>".lang("address").": ".$orderOb->getCustomer()->getAddress()."</li>";
            $listCustomer .= "<li>".lang("city").": ".$orderOb->getCustomer()->getCity()->getName()."</li>";
            $listCustomer .= '</ul>';

            $params = array();
            $params['*|NAME|*'] = 'Mary';
            $params['*|ORDER|*'] = $order;
            $params['*|PRODUCTS|*'] = $listProducts;
            $params['*|ADDRESS|*'] = $listCustomer;
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            var_dump($params);
            var_dump($to);
            $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
            $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"planes",$params);
            
            
            $toUser[]= array('name'=>$orderOb->getCustomer()->getName(),'mail'=>$orderOb->getCustomer()->getEmail());
            $params = array();
            $params['*|NAME|*'] = $orderOb->getCustomer()->getName();
            $params['*|ORDER|*'] = $order;
            $params['*|PRODUCTS|*'] = $listProducts;
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            var_dump($params);
            var_dump($toUser);
            if($method == AuthConstants::PM_GENERATE_CODE){
                $tmpl = "codigo";
                $subject = AuthConstants::ML_CODESUBJECT;
                $params['*|CODE|*'] = $listCodes;
            }else{
                $subject = AuthConstants::ML_RESERVEDSUBJECT;
                $tmpl = "reserva";
            }
                
            $this->notify($toUser,$subject,$tmpl,$params);
        }
        public function pendingOrder($order){
            $this->loadRepository("Orders");
//            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $orderId = ltrim(ltrim($order, "O"),"0");
            $orderOb = $this->Orders->find($orderId);
//            var_dump($orderOb);
            $fullName   = $orderOb->getCustomer()->getName()." ".$orderOb->getCustomer()->getLastName();
            $email      = $orderOb->getCustomer()->getEmail();
            $to[]= array('name'=>$fullName,'mail'=>$email);
//            $to[]= array('name'=>$fullName,'mail'=>'john.jimenez@lookingforweb.com');


            $params = array();
            $params['*|NAME|*'] = $orderOb->getCustomer()->getName();
            $params['*|ORDER|*'] = $order;
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
//            var_dump($params);
//            var_dump($to);

            $this->notify($to,AuthConstants::ML_PENDINGSUBJECT,"pendiente",$params);
        }
        
        public function demo(){
            $data = array();
		$data["title"] = "Demo";
		$this->viewPublic('demo', $data);
        }

        public function forbbiden()
	{
		$data = array();
		$data["title"] = "Forbbiden";
		$data["menu"]   = "";
		
		$this->view('forbbiden', $data);
	}
}
