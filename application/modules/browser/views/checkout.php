<!--<div class="large-6 columns">
    <div class="row ">
        formularico
    </div>
    
</div>-->
<div class="tabs-content">
    <div class="content active sumary" id="panel2-1">
        <div class="large-10 large-centered columns sumary ">
            <div class="fill-data"><?php echo lang('owner_info');?></div>
        </div>
        <div class="large-10 large-centered columns">
            <input name="cid" id="cid"   type="hidden"  value=<?php echo $cid?>   >
            <div class="guests" >
            <div class="row">
                <div class="large-2 columns "><?php echo lang('basic_info')?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="name" name="name" class="required" placeholder="<?php echo lang('name')?>" value="<?php echo $name?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="last_name" name="last_name" class="required" placeholder="<?php echo lang('last_name')?>" value="<?php echo $last_name?>"/>
                </div>
                <div class="large-4 columns ">
                    &nbsp;<!--<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email')?>" value="<?php echo $email?>"/>-->
                </div>                           
            </div>                           
            <div class="row">
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <select  id="docType" name="docType" class="required" placeholder="<?php echo lang('document_type')?>" >
                        <option><?php echo lang('select_value')?></option>
                        <?php foreach ($docTypes as $key => $value) {
                            $selected = "";
                            if(isset($docType)){
                                if($docType== $value['id']){
                                    $selected = 'selected="selected"';
                                }
                            }
                            echo "<option value='".$value['id']."' $selected>".$value['name']."</option>";
                         }
                         ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="dni" name="dni" class="required" placeholder="<?php echo lang('dni')?>" value="<?php echo $dni?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="email" name="email" readonly="readonly" class="required" placeholder="<?php echo lang('email')?>" value="<?php echo $email?>"/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-2 columns "><?php echo lang('location')?></div>
                <div class="large-3 columns ">
                    <select id="country" name="country" placeholder="<?php echo lang('country')?>">
                        <option><?php echo lang('select_value')?></option>
                        <?php foreach ($countries as $key => $value) {
//                            var_dump($value);
                            echo "<option value='".$value['id']."'>".$value['name']."</option>";
                         }
                         ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <select id="state" name="state" placeholder="<?php echo lang('state')?>">
                        <option><?php echo lang('select_value')?></option>
                    </select>
                </div>
                <div class="large-4 columns ">
                    <select id="city" name="city" placeholder="<?php echo lang('city')?>" class="required">
                        <option><?php echo lang('select_value')?></option>
                    </select>
                </div>                
            </div>
            <div class="row">
                <div class="large-2 columns "><?php echo lang('contact')?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="phone" name="phone" class="required" placeholder="<?php echo lang('phone')?>" value="<?php echo $phone?>"/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="mobile" name="mobile" class="required" placeholder="<?php echo lang('mobile')?>" value="<?php echo $mobile?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="address" name="address" class="required" placeholder="<?php echo lang('address')?>" value="<?php echo $address?>"/>
                </div>
                   
            </div>
        </div>
    </div>
    <div class="content active sumary" id="panel2-2">
        
        <div class="large-10 large-centered columns">
            <div class="row items">
            <?php foreach($cart as $item):?>
                <div class="row" id="<?php echo $item['id']?>">
                    <?php if(!isset($fill)){?>
                    <div class="hide-for-small medium-2 large-1 columns" id="<?php echo $item['id']?>">
                        <a id="delete_<?php echo $item['id']?>" class="action_list action_delete" title="Delete" href="#"></a>
                        <span><a id="edit_<?php echo $item['id']?>" class="action_list action_edit" title="Edit" href="#"></a></span>
                        <span class="hide"><a id="save_<?php echo $item['id']?>" class="action_list action_save" title="Save" href="#"></a></span>
                    </div>
                    <div class="hide-for-small medium-1 large-1 columns">
                        <span id="quotes_<?php echo $item['id']?>" ><?php echo $item['quotes']?></span>
                        <input id="edit_quotes_<?php echo $item['id']?>" class="hide small-12 large-12" name="edit_quotes_<?php echo $item['id']?>" value="<?php echo $item['quotes']?>"/>
                    </div>
                    <?php }else{?>
                        <input name="brand" id="brand" type="hidden"  value=<?php echo $item['brand']?>   >
                        <input name="number" id="number" type="hidden"  value=<?php echo $item['number']?>   >
                    <?php }?>
                    <div class="small-12 medium-6 large-6 columns item-name">
                        <div class="small-3 medium-6 large-3 columns">
                            <img src="<?php echo base_url()?>images/products/<?php echo $item['image']?>" width="90"/>
                        </div>
                        <div class="small-9 medium-6 large-9 columns">
                            <?php echo $item['name']?>
                        </div>
                    </div>
                    <div class="show-for-small-only small-12 quantity columns">
                        <div class="show-for-small-only small-4 columns">
                            Cantidad
                        </div>
                        <div class="show-for-small-only small-4 columns">
                            <span id="quotes_<?php echo $item['id']?>b" ><?php echo $item['quotes']?></span>
                            <input id="edit_quotes_<?php echo $item['id']?>b" class="hide small-12 large-12" name="edit_quotes_<?php echo $item['id']?>" value="<?php echo $item['quotes']?>"/>
                        </div>
                        <div class="show-for-small-only small-4 columns" id="<?php echo $item['id']?>b">
                            <a id="delete_<?php echo $item['id']?>" class="action_list action_delete" title="Delete" href="#"></a>
                            <span><a id="edit_<?php echo $item['id']?>b" class="action_list action_edit" title="Edit" href="#"></a></span>
                            <span class="hide"><a id="save_<?php echo $item['id']?>b" class="action_list action_save" title="Save" href="#"></a></span>
                        </div>
                    </div>
                    <div class="hide-for-small small-6 medium-2 large-2 text-right columns amount"><?php echo lang('currency');?><?php echo number_format(($item['cost']/$item['quotes']),0)?></div>
                    <div class="small-12 medium-2 large-2 text-right columns amount"><?php echo lang('currency');?><span id="amount_<?php echo $item['id']?>" base="<?php echo ($item['cost']/$item['quotes'])?>"><?php echo number_format($item['cost'],0)?></span></div>
                </div>
                <!-- //$subtotal = $subtotal + $item['amount'];-->
            <?php endforeach;?>
            <!--// $process = ($subtotal/100)*AuthConstants::PROCESS;-->
            </div>
            <div class="row subtotal right">
                <div class="row">
                    <div class="small-6 large-8 columns text-right"><?php echo lang('subtotal')?></div>
                    <div class="small-6 large-4 columns text-right"><?php echo lang('currency');?><span id="subtotal"><?php echo number_format($subtotal,0)?></span></div>
                    <div class="small-6 large-8 columns text-right"><?php echo lang('processing')?></div>
                    <div class="small-6 large-4 columns text-right"><?php echo lang('currency');?><span id="process"><?php echo number_format($process,0)?></span></div>
                </div>
            </div>
            <div class="row total">                
                <div class="row">                
                    <div class="large-12 columns text-right"><?php echo lang('currency');?><span id="total"><?php echo number_format($subtotal + $process,0)?></span></div>                
                </div> 
            </div> 
            <div class="row right">                
                <form id="payu" method="post" action="<?php echo $urlPayU?>">
                 <!--<form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/">-->
                  <!--<input name="merchantId"    type="hidden"  value="563485"   >-->
                  <!--<input name="accountId"     type="hidden"  value="566035" >-->
                  <input name="merchantId"    type="hidden"  value=<?php echo $merchant?>   >
                  <input name="accountId"     type="hidden"  value="<?php echo $account?>" >
                  <input name="description"   type="hidden"  value="<?php echo $description?>"  >
                  <input id="address" name="shippingAddress" type="hidden"  value="" >
                  <input id="city" name="shippingCity" type="hidden"  value="" >
                  <input id="country" name="shippingCountry" type="hidden"  value="" >
                  <input id="reference" name="referenceCode" type="hidden"  value="<?php echo $reference?>" >
                  <input id="pamount" name="amount"        type="hidden"  value="<?php echo $amount?>"   >
                  <input name="tax"           type="hidden"  value="0"  >
                  <input name="taxReturnBase" type="hidden"  value="0" >
                  <input name="currency"      type="hidden"  value="<?php echo $currency?>" >
                  <!--<input name="signature"     type="hidden"  value="<?php // echo md5("213FV389v0d31mACBAo0CuqQjZ~563485~test0002~7000~COP") ?>"  >-->
                  <input id="signature" name="signature"     type="hidden"  value=""  >
                  <input name="test"          type="hidden"  value="<?php echo $test?>" >
                  <input name="buyerEmail"    type="hidden"  value="<?php echo $buyerEmail?>" >
                  <input id="response" name="responseUrl"    type="hidden"  value="<?php echo $response?>" >
                  <input id="confirmation" name="confirmationUrl"    type="hidden"  value="<?php echo $confirmation?>" >
                  <!--<input name="Submit"   class="button"     type="submit"  value="Realizar pago" >-->
                </form>
                <?php $classBtn = (isset($directPayment))?"directPayment":"";?>
                <button id="payment" class="<?php echo $classBtn ?>"><?php echo lang("do_payment")?></button>
            </div> 
        </div> 
    </div>

