<!--<div class="columns large-12 medium-12 small-12 center">
    <div class="columns large-2 medium-12 small-12">
        &nbsp;
    </div>
    <div class="columns large-4 medium-12 small-12 justify disclamer">
        <strong>Vamos a ayudarte a decidir, haz clic</strong> en la categor&iacute;a que buscas y encuentra la mejor alternativa a tu medida.
    </div>
    <div class="columns large-6 medium-12 small-12">
        &nbsp;
    </div>
</div>-->
<div class="columns large-2 medium-2 small-12">
    &nbsp;
    <ul class="categories">
        <li><img src="<?php echo base_url()?>images/categories/telco2.png" width="48"><a href="#block1">Telecomunicaciones</a></li>
        <li><img src="<?php echo base_url()?>images/categories/educ2.png" width="48"><a href="#block3">Educación</a></li>
<!--        <li><img src="<?php echo base_url()?>images/categories/techno2.png" width="48"><a href="#block2">Tecnología</a></li>
        <li><img src="<?php echo base_url()?>images/categories/bussiness2.png" width="48"><a href="#block4">Finanzas</a></li>
        <li><img src="<?php echo base_url()?>images/categories/travel2.png" width="48"><a href="#block5">Viajes</a></li>
        <li><img src="<?php echo base_url()?>images/categories/restaurant2.png" width="48"><a href="#block6">Restaurantes</a></li>
        <li><img src="<?php echo base_url()?>images/categories/home2.png" width="48"><a href="#block7">Hogar</a></li>
        <li><img src="<?php echo base_url()?>images/categories/health2.png" width="48"><a href="#block8">Salud</a></li>-->
    </ul>
</div>
<div class="columns large-10 medium-10 small-12 block-content">
    <div class="columns large-4 medium-12 small-12 justify disclamer">
        <strong>Queremos ayudarte a decidir</strong>
    </div>
    <div class="columns large-4 medium-12 small-12 justify disclamer">
        1. Haz clic en la categor&iacute;a que buscas.</br>2. Responde las preguntas.
    </div>
    <div class="columns large-4 medium-12 small-12 justify disclamer">
        3. Escribe tu correo.</br>4. Encuentra <strong>alternativas</strong> a tu medida.
    </div>
    
</div>
<div class="columns large-6 medium-10 small-12 block-content">
    <div id="block1" class="content active question-container">
        <ul class="tabs subcategories" data-tab>
            <?php
            $large = round(12 / count($categories));
            foreach ($categories as $cat) {
                $class ="";
                $left ="";
                $right ="";
                if ($cat === reset($categories)){
                    $class = "active";
                    $left = "left-corner";
                }
                if ($cat === end($categories)){
                    $right = "right-corner";
                }
                echo '<li class="columns large-'.$large.' small-'.$large.' '.$left.$right.' tab-title '.$class.'"><a href="#panel1-'.$cat['id'].'">'.$cat['category'].'</a></li>';
            }
            ?>
<!--            <li class="columns large-4 left-corner tab-title active"><a href="#panel1-1">Móvil</a></li>
            <li class="columns large-4 tab tab-title"><a href="#panel1-2">Recargas</a></li>
            <li class="columns large-4 right-corner tab-title"><a href="#panel1-3">Equipos</a></li>-->
            <!--<li class="columns large-2 tab tab-title"><a href="#panel1-4">Fijo Hogar</a></li>-->
            <!--<li class="columns large-2 tab-title"><a href="#panel1-5">Móvil Empresa</a></li>-->
            <!--<li class="columns large-2 right-corner tab-title"><a href="#panel1-5">Fijo Empresa</a></li>-->
        </ul>
        <div class="tabs-content">
        <?php
        foreach ($categories as $catIdx => $cat) {
        $active ="";
        if ($cat === reset($categories)){
            $active = "active";
        }
        ?>
            <div class="content <?php echo $active?>" id="panel1-<?php echo $cat['id']?>">
                <div id="<?php echo $cat['category']?>" class="question-zone">
                    <form method="post" action="<?php echo $cat['action'] ?>" id="form<?php echo $catIdx ?>" class="form">
                    <?php
                    $fillClass = "";
                    if($cat['id']==3){
                      $fillClass = "-refill";  
                    }
                    foreach ($cat['questions'] as $idx => $question) {
                        $multiple = ($question['multiple']==1)?'multiple="multiple"':"";
                        $multiCls = ($question['multiple']==1)?'multiple':"";
                        $multiLbl = ($question['multiple']==1)?  lang("multiple_disc"):"";
                    ?>
                    <div class="columns large-6">
                        <label><?php echo $question['question']?><div class="hide-for-small instructions"><?php echo $multiLbl?></div></label>
                        <?php 
                        if(count($question['options'])>0){?>
                        <select id="q<?php echo $idx?>" name="questions[]" class="<?php echo$multiCls?> required" <?php echo $multiple?>>
                            <?php
                            if($question['multiple']!==1){
                                ?><option value=""><?php echo lang("default_select")?></option><?php
                            }
                            foreach ($question['options'] as $option) {
                            ?>
                            <option value="<?php echo $option['id']?>"><?php echo $option['option']?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <?php }else{ ?>
                        <input type="text" id="" name="questions[]" placeholder="" class="columns required" />
                        <?php } ?>
                    </div>
                    <?php
                    }
                    ?>
<!--                    <div class="columns large-6">
                        <label>Cuantas aplicaciones utilizas?</label>
                        <select name="app" class="columns multioption" multiple="multiple">
                            <option value="6">Spotify</option>
                            <option value="7">Waze</option>
                            <option value="8">Netflix</option>
                            <option value="9">Facebook</option>
                            <option value="10">Twitter</option>
                            <option value="11">Instagram</option>
                            <option value="12">Mas de 6</option>
                        </select>
                    </div>-->
                    <div class="columns large-12">
                        <label>Cu&aacute;l es tu email?
                            <div class="instructions">Este email es sólo para estar en contacto contigo!</div>
                        </label>
                        <input type="text" id="email<?php echo $cat['id'] ?>" name="email" placeholder="usuario@dominio.com" class="columns multioption email" />
                    </div>
                    <div class="columns large-12">
                        <input id="termns<?php echo $cat['id'] ?>" type="checkbox" value="1" name="terms" class="required" />
                        <label class="columns small-1  large-1" for="termns<?php echo $cat['id'] ?>"><span></span></label><label class="columns small-9 large-7 terms">Acepto los <a class="launch-terms" href="#">Términos y condiciones</a></label>
                    <button class="validation-trigger send<?php echo $fillClass;?> columns large-3 large-pull-1 right" cat="<?php echo $cat['id'] ?>">Alternativas</button>
                    </div>
                    </form>
                </div>
            </div>
            
        <?php
        }
        ?>
        </div>
    </div>
    <div id="block2" class="content question-container-2">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel2-1">Tecnología</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel2-2">Portatiles</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel2-3">Impresoras</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel2-4">Accesorios</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel2-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">Sabemos que te gusta la tecnología, por eso próximamente tendrás lo que quieres.</label>
                </div>
            </div>
            <div class="content" id="panel2-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel2-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel2-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block3" class="content question-container-3">
        <ul class="tabs subcategories" data-tab>
            <!--<li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel3-1">Educación</a></li>-->
            <?php
            $large = round(12 / count($categories3));
            foreach ($categories3 as $cat) {
                $class ="";
                $left ="";
                $right ="";
                if ($cat === reset($categories3)){
                    $class = "active";
                    $left = "left-corner";
                }
                if ($cat === end($categories3)){
                    $right = "right-corner";
                }
                echo '<li class="columns large-'.$large.' small-'.$large.' '.$left.$right.' tab-title '.$class.'"><a href="#panel1-'.$cat['id'].'">'.$cat['category'].'</a></li>';
            }
            ?>
<!--            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel3-1">Educación</a></li>-->
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel3-2">Postgrados</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel3-3">Diplomados</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel3-4">Continuada</a></li>-->
        </ul>
        <div class="tabs-content">
<!--            <div class="content active" id="panel3-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">En busca de aprendizaje, próximamente seremos tu asesor vocacional.</label>
                </div>
            </div>-->
            <?php
            foreach ($categories3 as $catIdx => $cat) {
            $active ="";
            if ($cat === reset($categories3)){
                $active = "active";
            }
            ?>
              <div class="content <?php echo $active?>" id="panel1-<?php echo $cat['id']?>">
                <div id="<?php echo $cat['category']?>" class="question-zone">
                    <form method="post" action="<?php echo $cat['action'] ?>" id="form<?php echo $cat['id'] ?>" class="form">
                    <?php
                    $fillClass = "";
                    if($cat['id']==3){
                      $fillClass = "-refill";  
                    }
                    foreach ($cat['questions'] as $idx => $question) {
                        $multiple = ($question['multiple']==1)?'multiple="multiple"':"";
                        $multiCls = ($question['multiple']==1)?'multiple':"";
                        $multiLbl = ($question['multiple']==1)?  lang("multiple_disc"):"";
                    ?>
                    <div class="columns large-6">
                        <label><?php echo $question['question']?><div class="hide-for-small instructions"><?php echo $multiLbl?></div></label>
                        <?php 
                        if(count($question['options'])>0){?>
                        <select id="q<?php echo $idx?>" name="questions[]" class="<?php echo$multiCls?> required" <?php echo $multiple?>>
                            <?php
                            if($question['multiple']!==1){
                                ?><option value=""><?php echo lang("default_select")?></option><?php
                            }
                            foreach ($question['options'] as $option) {
                            ?>
                            <option value="<?php echo $option['id']?>"><?php echo $option['option']?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <?php }else{ ?>
                        <input type="text" id="" name="questions[]" placeholder="" class="columns required" />
                        <?php } ?>
                    </div>
                    <?php
                    }
                    ?>
<!--                    <div class="columns large-6">
                        <label>Cuantas aplicaciones utilizas?</label>
                        <select name="app" class="columns multioption" multiple="multiple">
                            <option value="6">Spotify</option>
                            <option value="7">Waze</option>
                            <option value="8">Netflix</option>
                            <option value="9">Facebook</option>
                            <option value="10">Twitter</option>
                            <option value="11">Instagram</option>
                            <option value="12">Mas de 6</option>
                        </select>
                    </div>-->
                    <div class="columns large-12">
                        <label>Cu&aacute;l es tu email?</label>
                        <input type="text" id="email<?php echo $cat['id'] ?>" name="email" placeholder="usuario@dominio.com" class="columns multioption email" />
                    </div>
                    <div class="columns large-12">
                        <input id="termns<?php echo $cat['id'] ?>" type="checkbox" value="1" name="terms" class="required" />
                        <label class="columns small-1  large-1" for="termns<?php echo $cat['id'] ?>"><span></span></label><label class="columns small-9 large-7 terms">Acepto los <a class="launch-terms" href="#">Términos y condiciones</a></label>
                    
                        <button class="validation-trigger send<?php echo $fillClass;?> columns large-3 large-pull-1 right" cat="<?php echo $cat['id'] ?>">Alternativas</button>
                    </div>
                    </form>
                </div>
            </div>
            
        <?php
        }
        ?>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block4" class="content question-container-4">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel4-1">Finanzas</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel4-2">Vehículos</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel4-3">Inversiones</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel4-4">Pymes</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel4-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">Próximamente encontrarás tu nuevo asesor financiero.</label>
                </div>
            </div>
            <div class="content" id="panel4-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel4-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel4-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block5" class="content question-container-5">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel5-1">Viajes</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel5-2">Cruceros</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel5-3">Colombia</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel5-4">Expedición</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel5-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">No sabes a dónde ir éstas vacaciones, próximamente seremos tu guía turístico.</label>
                </div>
            </div>
            <div class="content" id="panel5-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel5-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel5-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block6" class="content question-container-6">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel6-1">Restaurantes</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel6-2">Italiana</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel6-3">De Mar</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel6-4">Mexicana</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel6-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">Planeas una cena especial, próximamente te mostraremos la carta que buscas.</label>
                </div>
            </div>
            <div class="content" id="panel6-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel6-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel6-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block7" class="content question-container-7">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel7-1">Hogar</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel7-2">Italiana</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel7-3">De Mar</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel7-4">Mexicana</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel7-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">Quieres adecuar tu hogar, próximamente encontrarás lo que te hace falta.</label>
                </div>
            </div>
            <div class="content" id="panel7-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel7-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel7-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
    <div id="block8" class="content question-container-8">
        <ul class="tabs subcategories" data-tab>
            <li class="columns left-corner large-3 small-3 tab-title active"><a href="#panel8-1">Salud</a></li>
<!--            <li class="columns tab-title large-3 small-3 "><a href="#panel8-2">Italiana</a></li>
            <li class="columns tab tab-title large-3 small-3 "><a href="#panel8-3">De Mar</a></li>
            <li class="columns right-corner tab-title large-3 small-3 "><a href="#panel8-4">Mexicana</a></li>-->
        </ul>
        <div class="tabs-content">
            <div class="content active" id="panel8-1">
                <div id="mobile" class="question-zone">
                    <label class="tmp-disc">La salud lo es todo, próximamente encontrarás lo que tú y tu familia necesitan.</label>
                </div>
            </div>
            <div class="content" id="panel8-2">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel8-3">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <div class="content " id="panel8-4">
                <div id="" class="question-zone">
                    <label>Próximamente</label>
                </div>
            </div>
            <!--<button class="columns large-2 large-pull-1 right">Alternativas</button>-->
        </div>
    </div>
</div>
<div class="columns large-3 medium-12 small-12">
</div>
</div>
</div>
<div class="row bottom-cats show-for-large-up">
    <ul class="categories comming-soon">
        <li><span class="comming-soon-title">Próximamente</span></li>
        <li><img src="<?php echo base_url()?>images/categories/techno2.png" width="48"><a href="#block2">Tecnología</a></li>
        <!--<li><img src="<?php echo base_url()?>images/categories/educ2.png" width="48"><a href="#block3">Educación</a></li>-->
        <li><img src="<?php echo base_url()?>images/categories/bussiness2.png" width="48"><a href="#block4">Finanzas</a></li>
        <li><img src="<?php echo base_url()?>images/categories/travel2.png" width="48"><a href="#block5">Viajes</a></li>
        <li><img src="<?php echo base_url()?>images/categories/restaurant2.png" width="48"><a href="#block6">Restaurantes</a></li>
        <li><img src="<?php echo base_url()?>images/categories/home2.png" width="48"><a href="#block7">Hogar</a></li>
        <li><img src="<?php echo base_url()?>images/categories/health2.png" width="48"><a href="#block8">Salud</a></li>
    </ul>
</div>