<script type="text/javascript">
//    $(document).foundation({
//        orbit: {
//          animation: 'slide',
//          timer_speed: 5000,
////          pause_on_hover: true,
//          animation_speed: 500,
//          navigation_arrows: false,
//          bullets: true,
//          slide_number: false,
//          timer: false
//        }
//      });
    $(document).ready(function() {
       $("#browse").click(function(){
            if($("#name").val()!=""){
                var minutes = ($("select[name=minutos]").val()!="")?$("select[name=minutos]").val():"";
                var navigation = ($("select[name=navegacion]").val()!="")?$("select[name=navegacion]").val():"";
                var app = ($("select[name=app]").val()!="")?$("select[name=app]").val():"";
                var submitdata = {
                        "MINUTOS": minutes,
                        "NAVEGACION": navigation,
                        "APP": app,
                        "c4e429d1506c7d66d56ad1843cfe4f2c": $('input[name=c4e429d1506c7d66d56ad1843cfe4f2c').val()
                };
                $.ajax({
                    url: "<?php echo base_url();?>browser/browse/",
                    type: "post",            
                    dataType: "json",
                    data: submitdata,
                    success: function(data){ 
                        if(data.status == true){
                            $.each(data.data,function(index,value){
                                var html = '';
                                html += '<div class="large-4 small-6 columns left grid-item">';
                                html += '   <img src="../images/products/'+value.image+'.jpg">';
                                html += '   <details>'+value.description+'</details>';
                                html += '   <div class="panel">';
                                html += '       <h5>'+ value.product+'</h5>';
                                html += '       <h6 class="subheader">$'+ value.cost+'</h6>';
                                html += '   </div>';
                                html += '</div>';
                                $(".result-grid-row").append(html);
                                
                            })
                    
                            $("#question-slider").fadeOut()
                            $("#result-grid").fadeIn()
                        }else{
                            if(data.error = "no-data"){
                                $("#modal-message #message").html("No se han encontrado resultados<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                            }else{
                                $("#modal-message #message").html("Error inesperado<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                            }
                        }
                        
                    }
                });
            }else{
                $("#modal-message #message").html("Dinos tu nombre!");
                $('#modal-message').foundation('reveal', 'open');
            }
       });
       
       $(".result-grid-row").on('click','.grid-item',function(){
            $("#modal-message #modal-title").html("Detalles");
            $("#modal-message #message").html($(this).find('details').html()+"<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');
        })
       $("#modal-close").click(function(){
           $('#modal-message').foundation('reveal', 'close');
       })
       
    });
</script>