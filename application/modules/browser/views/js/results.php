<script type="text/javascript">
//    $(document).foundation({
//        orbit: {
//          animation: 'slide',
//          timer_speed: 5000,
////          pause_on_hover: true,
//          animation_speed: 500,
//          navigation_arrows: false,
//          bullets: true,
//          slide_number: false,
//          timer: false
//        }
//      });
    var comparison = [];
    $(document).ready(function() {
        
//       $("body").on("mouseover",".grid-item",function(){
//           $(this).find(".botones").removeClass("transparency")
//       });
//       $("body").on("mouseleave",".grid-item",function(){
//           $(this).find(".botones").addClass("transparency")
//       }); 
       $(".grid-item .compare").click(function(){
           addItem($(this).attr("id"),$(this).attr("cost"),"1","comparison")
       }); 
       $("body").on("click",".grid-item .add",function(){
           addItem($(this).attr("id"),$(this).attr("cost"),"1","cart")
       }); 
       $("body").on("click",".add-item",function(){
           addItem($(this).attr("id"),$(this).attr("cost"),"1","cart")
            $('#modal-message').foundation('reveal', 'close');
       }); 
       
       function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
       
       //Refresh products grid with new search
       $(".research").change(function(){
            var questions = [];
            $(".research").each(function(idx){
                questions.push($(this).val())
            });
            var submitdata = {
                    "questions": questions,
                    <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                url: "<?php echo base_url();?>browser/browse/",
                type: "post",            
                dataType: "json",
                data: submitdata,
                success: function(data){ 
                    $(".result-grid-row").html("");
                    if(data.status == true){
                        $.each(data.data,function(index,value){
                            var checked = "";
                            $.each(comparison,function(idx,val){
                                if(val == value.id){
                                    checked = "checked";
                                    return false
                                }
                            });
                            var html = '';
                            html += '<div id="'+ value.id+'" class="large-6 medium-6 small-12 columns left grid-item">';
//                            html += '   <span class="left '+checked+' checkbox "></span>';
                            html += '   <details>'+value.description+'</details>';
                            html += '   <div class="panel">';
                            html += '       <div class="row">';
                            html += '           <div class="columns large-6 large-centered">';
                            html += '               <img class="image-selector" src="<?php echo base_url() ?>images/products/'+value.image+'">';
                            html += '           </div>';
                            html += '           <div class="columns large-12">';
                            html += '               <h5><span class="product-selector left text-left">'+ value.product+'</span></h5>';
                            html += '           </div>';
                            html += '           <div class="columns large-7 price left">';
                            html += '                   <h6 class="subheader">$ <span class="amount-selector">'+ numberThousand(value.cost)+'</span></h6>';
                            html += '           </div>';
                            html += '       </div>';
                            html += '       <div class="row">';
                            html += '           <div class="botones ">';
                            var labelBtn = '';
                            if(value.method == <?php echo AuthConstants::PM_GENERATE_CODE?>){
                                labelBtn = '<?php echo lang("add_code") ?>';
                            }else{
                                labelBtn = '<?php echo lang("add") ?>';
                            }
                            html += '               <button id="'+value.id+'" cost="'+value.cost+'" class="columns small-6 large-6 right add">'+labelBtn+'</button>';
                            html += '              <button id="'+value.id+'" cost="'+value.cost+'" class="columns small-5 large-4 right view"><?php echo lang("view") ?></button>';
                            html += '           </div>';
                            html += '       </div>';
                            html += '   </div>';
                            html += '</div>';
                            $(".result-grid-row").append(html);

                        });

//                            $("#question-slider").fadeOut()
//                            $("#result-grid").fadeIn()
                    }else{
                        if(data.error = "no-data"){
                            $("#modal-message #message").html("No se han encontrado resultados<br/><br/>");
                            $('#modal-message').foundation('reveal', 'open');
                        }else{
                            $("#modal-message #message").html("Error inesperado<br/><br/>");
                            $('#modal-message').foundation('reveal', 'open');
                        }
                    }

                }
            });
       });
       //open poput with product details
       $("body").on("click",".result-grid-row .view",function(){
            var id = $(this).parent().parent().parent().parent().attr('id');
            var desc = $(this).parent().parent().parent().parent().find('details').html();
            var title = $(this).parent().parent().parent().find('.product-selector').html();
            var amount = $(this).parent().parent().parent().find('.amount-selector').html().replace(",","").replace(",","");
            var label = $(this).parent().find('.add').html();
            var details = '';
            var image = $(this).parent().parent().parent().find('.image-selector').attr("src");
            $.ajax({
                url: "<?php echo base_url();?>browser/properties/",
                type: "post",            
                dataType: "json",
                data: {id:id},
                success: function(data){ 
                    details = data.data.details;

                    var html ="";                
                    html += "       <div class='row box item'>";
                    html += "           <div class='large-6 columns'>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-10 large-centered columns text-center'>";
                    html += "                   <img src='"+image+"' style='width:100%;'>";        
                    html += "                   </div>";
                    html += "               </div>";
                    html += "           </div>";
                    html += "           <div class='large-6 columns'>";
                    html += "               <div class='row'>";
                    html += "                   <div class='small-12 large-8 columns title text-center-for-small-only'>";
                    html += "                   "+title;
                    html += "                   </div>";
                    html += "                   <div class='small-12 large-4 columns text-right price'>";
                    html += "                   $ "+numberThousand(amount);
                    html += "                   </div>";
                    html += "               </div>";
                    html += "               <div class='checkout-hide'>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-12 columns description'>";
                    html += "                       "+desc;
                    html += "                   </div>";
                    html += "               </div>";
                    var tempdata =details;
                    html += "               <div class='row properties'>";
                    $.each(tempdata,function(i,detail){
                    html += "               <div class='row'>";
                    html += "                   <div class='small-7 large-6 columns details'>";
                    html += "                       <img src='<?php echo base_url('images')?>/"+detail.property+".png' width='16'/>";
                    html += "                       "+detail.label;
                    html += "                   </div>";
                    html += "                   <div class='small-5 large-6 columns details text-right'>";
                    html += "                       "+detail.value;
                    html += "                   </div>";
                    html += "               </div>";
                    });            
                    html += "               </div>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-12 columns group' id='base-amount' data='"+amount+"'>";
                    html += "                       <span class='columns large-6'><?=lang('quantity')?></span>";
                    html += "                       <select class='columns large-2' id='quantity'>";
                    for(var i=1; i<=3; i++){
                        html += "                       <option value='"+i+"'>"+i+"</option>";            
                    }
                    html += "                       </select>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "               </div>";
                    html += "               <div class='checkout-show hide'>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-8 columns large-centered text-center gift-added'>";
                    html += "                        <img src='<?php echo base_url()."images/items-added.png"?>' width='50%'>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-8 columns large-centered text-center gift-added-text'>";
                    html += "                        <span id='final-quotes'></span> <?=lang('added_items')?>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "               </div>";
                    html += "               <div class='row'>";
                    html += "                   <div class='large-12 columns total'>";
                    html += "                       Total $<span id='total-amount' product='"+id+"' quotes='1' available='1' amount='"+amount+"'>"+numberThousand(amount)+"</span>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "               <div class='row btn-row'>";
                    html += "                   <div id='btn-add' class='large-12 columns'>";
                    html += "                           <button class='button btn-form cart-btn btn-blue add-cart right' >";
                    html += "                               <a href='#' ></a>";
                    html += "                                   "+label;
                    html += "                           </button>";
                    html += "                   </div>";
                    html += "                   <div id='btn-add-more' class='large-6 small-6 columns hide'>";
                    html += "                           <button class='button cart-btn btn-form small-12 btn-blue-edit add-more' >";
                    html += "                               <a href='#' ></a>";
                    html += "                                   <?=lang('continue_buying')?>";
                    html += "                           </button>";
                    html += "                   </div>";
                    html += "                   <div id='btn-checkout' class='large-6 small-6 columns hide'>";
                    html += "                           <button class='button cart-btn btn-form small-12 btn-blue add-checkout' >";
                    html += "                               <a href='#' ></a>";
                    html += "                                   <?=lang('go_checkout')?>";
                    html += "                           </button>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "           </div>";
                    html += "       </div>";
                    $(".header-reveal-modal div").addClass("btn-purchase").parent().css('background','#FFFFFF');
                    $("#modal-message #modal-title").html("<?=lang('view')?>");
                    $("#modal-message #message").html(html);
                    $("#modal-message #modal-close").hide();
                    $('#modal-message').removeClass('tiny').addClass('large').css('top', '25px');
                    $('#modal-message').foundation('reveal', 'open');
                }
            })
        });
         $("body").on('click','.add-more',function(){
            $('#modal-message').foundation('reveal', 'close');
        });

        $("body").on('click','.add-checkout',function(){
            window.location='<?php echo base_url()."browser/checkout" ?>';
        });
        
        //add item to comparison on click
//       $(".result-grid-row").on('click','.grid-item',function(){
//            var id = $(this).attr('id');
//            if($(this).find(".checkbox").hasClass("checked")){
//                var index = comparison.indexOf(id);
//                comparison.splice(index,1)
//                $(this).find(".checkbox").toggleClass("checked");
//            }else{
//                if(comparison.length<3){
//                    comparison.push(id)
//                    $(this).find(".checkbox").toggleClass("checked");
//                }else{
//                    $('#modal-message').foundation('reveal', 'close');
//                    $("#modal-message #modal-title").html("<?=lang('compare_products')?>");
//                    $('#modal-message').removeClass('extra-large').addClass('tiny').css('top', '85px');
//                    $("#modal-message #message").html('<?php echo lang('exceded_products')?></br></br>');
//                    $('#modal-message').foundation('reveal', 'open');
//                }
//            }
//            var compare = (comparison.length>0)?comparison.length:"";
//                $("#compare_num").html(compare);
//        })
        
        //Comparision action at button click
//       $("button.compare").click(function(){
//            if(comparison.length==0){
//                $('#modal-message').foundation('reveal', 'close');
//                $("#modal-message #message").html('<?php echo lang('no_comparison')?>');
//                $('#modal-message').foundation('reveal', 'open');
//            }else{
//               $.ajax({
//                    url: "<?php echo base_url();?>browser/compare/",
//                    type: "post",            
//                    dataType: "json",
//                    data: {comparison:comparison},
//                    success: function(data){ 
//                        var columns = 12/parseInt(comparison.length)
//                        if(data.status == true){
//                            var html ="";                
//                            html += "       <div class='row box compare'>";
//                            $.each(data.data,function(idx,product){
//                            html += "           <div class='large-"+columns+" columns'>";
//                            html += "               <div class='row'>";
//                            html += "                   <div class='small-12 large-12 columns title'>";
//                            html += "                   "+ product.name;
//                            html += "                   </div>";
//                            html += "               </div>";
//                            html += "               <div class='row'>";
//                            html += "                   <div class='large-12 columns text-center'>";
//                            html += "                   <img src='<?php echo site_url('images/products')?>/"+product.image+"' style='width:20%;'>";        
//                            html += "                   </div>";
//                            html += "               </div>";
//                            html += "               <div class='row'>";
//                            html += "                   <div class='large-12 columns description'>";
//                            html += "                       "+product.description;
//                            html += "                   </div>";
//                            html += "               </div>";
//                                var tempdata =product.details;
//                                $.each(tempdata,function(i,detail){
//                                html += "               <div class='row'>";
//                                html += "                   <div class='large-6 columns comparison'>";
//                                html += "                       "+detail.property;
//                                html += "                   </div>";
//                                html += "                   <div class='large-6 columns comparison text-right'>";
//                                html += "                       "+detail.value;
//                                html += "                   </div>";
//                                html += "               </div>";
//                                });
//                            html += "               <div class='row'>";
//                            html += "                   <div id='btn-add' class='large-6 right columns'>";
//                            html += "                       <button id='"+product.id+"' cost='"+product.cost+"' class='button  btn-form cart-btn add-item right' >";
//                            html += "                           <?=lang('add')?>";
//                            html += "                       </button>";
//                            html += "                   </div>";
//                            html += "               </div>";
//                            html += "           </div>";
//                            });
//                            html += "       </div>";
//                            $(".header-reveal-modal div").addClass("btn-purchase").parent().css('background','#FFFFFF');
//                            $("#modal-message #modal-title").html("<?=lang('compare_products')?>");
//                            $("#modal-message #message").html(html);
//                            $("#modal-message #modal-close").hide();
//                            $('#modal-message').removeClass('tiny').addClass('extra-large').css('top', '85px');
//                            $('#modal-message').foundation('reveal', 'open');
//                        }else{
//                            $('#modal-message').foundation('reveal', 'close');
//                            $("#modal-message #message").html('<?php echo lang('comparison_unavailable')?>');
//                            $('#modal-message').foundation('reveal', 'open');
//                        }
//                    }
//                }); 
//            }
//       });
        
       $("#modal-close").click(function(){
           $('#modal-message').foundation('reveal', 'close');
       });
       
       $("body").on('click',"button.add-cart",function(){            
            var quotes = $("#total-amount").attr('quotes');
            var amount = $("#total-amount").attr('amount');
            var product = $("#total-amount").attr('product');
            var available = $("#total-amount").attr('available');
            addItem(product,amount,quotes,"cart")
        });
        $("body").on('change',"#quantity",function(){            
            $("#total-amount").attr('quotes',$(this).val());
            $("#total-amount").attr('amount',($("#base-amount").attr('data')*$(this).val()));
            $("#total-amount").html(numberThousand($("#base-amount").attr('data')*$(this).val()));
        })
       function addItem(id,cost,quotes,type){
            var submitdata = {
                "id": id,
                "cost": cost,
                "quotes": quotes,
                "available": 1,
                "type": type
            };
            $.ajax({
                url: "<?php echo base_url();?>browser/addItem/",
                type: "post",            
                dataType: "json",
                data: submitdata,
                success: function(data){ 
                    if(data.status == true){
                        $("#cartCount").html(data.data)
                        
                        $("#btn-add").addClass('hide');
                        $("#btn-add-more").removeClass('hide');
                        $(".checkout-hide").addClass('hide');
                        $(".checkout-show").removeClass('hide');
                        $("#btn-checkout").removeClass('hide');
                        $("#final-quotes").html(submitdata.quotes);
                        $(".total").addClass('text-center');
                    }else{
                        $('#modal-message').foundation('reveal', 'close');
                        $("#modal-message #message").html('<?php echo lang('product_unavailable')?>');
                        $('#modal-message').foundation('reveal', 'open');
                    }
                }
            });
        }
    });
    
</script>