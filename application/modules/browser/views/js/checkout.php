<script type="text/javascript">
//    $(document).foundation({
//        orbit: {
//          animation: 'slide',
//          timer_speed: 5000,
////          pause_on_hover: true,
//          animation_speed: 500,
//          navigation_arrows: false,
//          bullets: true,
//          slide_number: false,
//          timer: false
//        }
//      });
    $(document).ready(function() {
       $("#payment").click(function(){
           if(validate()){
            if($(this).hasClass("directPayment")){
                setTimeout($("#payu").submit(),500);
            }else{
                 var submitdata = {
                         "cid": $("#cid").val(),
                         "name": $("#name").val(),
                         "last_name": $("#last_name").val(),
                         "address": $("#address").val(),
                         "phone": $("#phone").val(),
                         "mobile": $("#mobile").val(),
                         "city": $("#city").val(),
                         "dni": $("#dni").val(),
                         "docType": $("#docType").val(),
     //                    "email": $("#email").val(),
                         "total": $("#total").html().replace(",","").replace(",",""),
                         <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                 };
                 if($("#brand").val() && $("#number").val()){
                     submitdata.brand = $("#brand").val();
                     submitdata.number = $("#number").val();
                 }
                 $.ajax({
                     url: "<?php echo base_url();?>browser/orderConfirmation/",
                     type: "post",            
                     data: submitdata,
                     dataType: "json",
                     success: function(data){   
                         if( data.status != false){
                             var pamount = 0;
                             var usepayu = 0;
                             var reserve = 0;
                             var code    = 0;
                             $.each(data.data.amount,function(index,amount){
                                 if(index == <?php echo AuthConstants::PM_USE_PAYU?>){
                                     pamount = amount;
                                     usepayu = 1;
                                 }
                                 if(index == <?php echo AuthConstants::PM_RESERVE?>){
                                     reserve = 1;
                                 }
                                 if(index == <?php echo AuthConstants::PM_GENERATE_CODE?>){
                                     code    = 1;
                                 }
                             })
                             
                            $("#reference").val(data.data.reference);
                            $("#pamount").val(pamount);
                            $("#signature").val(data.data.signature);
                            $("#response").val(data.data.response);
                            $("#confirmation").val(data.data.confirmation);
//                            $("#address").val(data.data.address);
//                            $("#city").val(data.data.city);
//                            $("#country").val(data.data.country);
                             if(usepayu >0 && reserve == 0 && code ==0){
                                setTimeout($("#payu").submit(),1000);
                             }
                             if(pamount > 0 && (reserve > 0 || code > 0)){
                                $("#modal-message #modal-title").html("Solicitud de compra");
                                $("#modal-message #message").html("Se efectuará un pago por $"+pamount+", un agente se pondrá en contacto por el valor restante<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                                $.ajax({
                                    url :"<?php echo base_url();?>browser/reservedOrder/"+data.data.reference,
                                    global: false,
                                    success:function(data){   
                                        setTimeout(function(){$("#payu").submit()},1000);
                                    }
                                });
                             }
                             if(usepayu ==0 && (reserve > 0 || code > 0)){
                                $("#modal-message #modal-title").html("Solicitud de compra");
                                $("#modal-message #message").html("Se ha reservado el producto, un agente se pondrá en contacto<br/><br/>");
                                if(code>0){
                                    $("#modal-message #modal-title").html("Solicitud de descuento");
                                    $("#modal-message #message").html("Se ha generado un código de descuento, verifica tu correo electrónico<br/><br/>");
                                }
                                $('#modal-message').foundation('reveal', 'open');
                                $.ajax({
                                    url :"<?php echo base_url();?>browser/reservedOrder/"+data.data.reference,
                                    global: false,
                                    success:function(data){
                                        setTimeout(function(){window.location = "<?php echo base_url();?>"},1000);
                                    }
                                });
                             }
                             if(usepayu ==0 && reserve == 0 && code == 0 ){
                                $("#modal-message #modal-title").html("Solicitud de compra");
                                $("#modal-message #message").html("No hay productos en el carrito, verifica la información<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                             }
                         }else{
                            $("#modal-message #modal-title").html("Solicitud de compra");
                            $("#modal-message #message").html("No hay productos en el carrito, verifica la información<br/><br/>");
                            $('#modal-message').foundation('reveal', 'open');
                         }
                     }
                 });
             }
            }else{
                $("#modal-message #modal-title").html("Alerta");
                $("#modal-message #message").html("Error de validacion<br/><br/>");
                $('#modal-message').foundation('reveal', 'open');
            
            }
            
       });
       
       $(".result-grid-row").on('click','.grid-item',function(){
            $("#modal-message #modal-title").html("Detalles");
            $("#modal-message #message").html($(this).find('details').html()+"<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');
        })
       $("#modal-close").click(function(){
           $('#modal-message').foundation('reveal', 'close');
       });
       
       $(".action_edit").click(function(){
            $("#quotes_"+$(this).parent().parent().attr("id")).addClass("hide");
            $("#edit_quotes_"+$(this).parent().parent().attr("id")).removeClass("hide");
            $("#edit_"+$(this).parent().parent().attr("id")).parent().addClass("hide");
            $("#save_"+$(this).parent().parent().attr("id")).parent().removeClass("hide");
            if($("#edit_quotes_"+$(this).parent().parent().attr("id")).attr('value')>0){
               $("#edit_quotes_"+$(this).parent().parent().attr("id")).val($("#edit_quotes_"+$(this).parent().parent().attr("id")).attr('value')); 
            }
            
        });
        $(".action_save").click(function(){
            
            var gift = $(this).parent().parent().attr('id');
            var id = $(this).parent().parent().attr('id').replace("b","");
            var quotes = $("#edit_quotes_"+$(this).parent().parent().attr('id')).val();
            var amount = $("#amount_"+id).attr("base");
            $.ajax({
                url: "<?php echo base_url();?>browser/editCart/",
                type: "post",            
                data: {id:id,quotes:quotes,cost:amount,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.response != "error"){
                        $("#edit_quotes_"+id).val(quotes);
                        $("#edit_quotes_"+gift).val(quotes);
                        $("#quotes_"+id).html(quotes);
                        $("#quotes_"+gift).html(quotes);
                        $("#amount_"+id).html(data.gift);
                        $("#subtotal").html(data.subtotal);
                        $("#process").html(data.process);
                        $("#total").html(data.total);
                        $("#quotes_"+gift).removeClass("hide");
                        $("#edit_quotes_"+id).addClass("hide");
                        $("#edit_quotes_"+gift).addClass("hide");
                        $("#edit_"+gift).parent().removeClass("hide");
                        $("#save_"+gift).parent().addClass("hide");                        
                        if(data.response != "added"){
                            $("#quotes_"+gift).html(data.response)
                            $("#quotes_"+id).html(data.response)
                            $("#modal-message #message").html("<?php echo lang('max_available_fees')?>" + data.response);
                            $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                }
            });
        });
        $(".action_delete").click(function(){
            
            var gift = $(this).parent().attr('id').replace("b","");
            var quotes = $("#edit_quotes_"+$(this).parent().parent().attr('id')).val();
            var amount = $("#amount_"+gift).attr("base");
            $.ajax({
                url: "<?php echo base_url();?>browser/deleteCart/",
                type: "post",            
                data: {id:gift,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.response != "error"){
                        $("#"+gift).remove();
                        $("#subtotal").html(data.subtotal);
                        $("#process").html(data.process);
                        $("#total").html(data.total);                        
                        $("#cartCount").html(data.cartCount);  
                        if(parseInt(data.cartCount)==0){
                            window.location = '<?php echo base_url()?>browser';
                        }
                    }
                }
            });
        });
        $("#country").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>browser/getStates/",
                type: "post",            
                data: {countryId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#state").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .text(option.name);
                            $("#state").append(option);
                        });
                    }
                }
            });
        });
        var docType = '<?php echo $docType?>';
        if(parseInt(docType)>0){
            $("#docType").val(parseInt(docType));
        }
        var country = '<?php echo $country?>';
        if(parseInt(country)>0){
            $("#country").val(parseInt(country));
            $("#country").trigger('change');
            var state = '<?php echo $state?>';
            if(parseInt(state)>0){
                    setTimeout(function(){
                        $("#state").val(parseInt(state));
                        $("#state").trigger('change');
                    },500);
                var city = '<?php echo $city?>';
                if(parseInt(city)>0){
                    setTimeout(function(){
                        $("#city").val(parseInt(city));
                        $("#city").trigger('change');
                    },1000);
                }
            }
        }
        $("#state").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>browser/getCities/",
                type: "post",            
                data: {stateId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#city").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .text(option.name);
                            $("#city").append(option);
                        });
                    }
                }
            });
        });
        
        $("input").focusout(function(){
            required($(this))
        });
        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate(){
            $(".guest .required").each(function(){
                required($(this));
            });
            if ($(".validation-error")[0]){
                return false;
            } else {
                return true;
            }
        }
        function required(obj){
            $(obj).removeClass("validation-error")
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
    });
</script>