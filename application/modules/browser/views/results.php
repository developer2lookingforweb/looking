<div class="row">
 
    <div id="result-grid" class="large-3 medium-12 small-12 columns center">
        <div class="row">
            <div class="columns large-12 text-left category-title"> <?php echo ($category)?></div>
        </div>
        <div class="row">
            <div class="columns large-4 main-category"> <?php echo ($mainCategory)?></div>
        </div>
        <div class="filters row">
            <?php
            if(count($questions)>0){
            foreach($questions as $item){
                $multiple = ($item['multiple']==1)?'multiple="multiple" "':"";
                $multiCls = ($item['multiple']==1)?'multiple':"";
                ?>
                <div class="columns large-12">
                    <label><?php echo $item['question']?></label>
                    <select name="questions[]" class="research <?php echo$multiCls?>" <?php echo$multiple?>>
                        <?php
                        
                        if($item['multiple']!==1){
                            ?><option value=""><?php echo lang("default_select")?></option><?php
                        }
                        foreach($item['options'] as $option){
                            $selected = "";
                            if(isset($answers[$item["id"]])){
                                if($answers[$item["id"]]== $option["id"]){
                                    $selected = 'selected="selected"';
                                }
                            }
                        ?>
                            <option value="<?php echo $option['id']?>" <?php echo $selected?>><?php echo $option['option']?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            <?php
            }
            }  else {
                ?><label>Selecciona la opción de tu preferencia!</label><?php
            }
            ?>
        </div>
            <div class="row">
                <button  class="columns large-10 large-centered compare cart-btn hide">Comparar <span id="compare_num"></span> productos</button>
            <div class="columns large-12">
        </div>
        </div>
    </div>
    <div id="result-grid" class="large-8 medium-12 small-12 columns center">
        <div class="large-11 columns">
            <div class="row result-grid-row">
                <?php 
                foreach ($results as $idx => $value) {
//                    var_dump($value);
                    ?><div id="<?php echo $value->id ?>" class="large-6 medium-6 small-12 columns left grid-item">
                        <!--<span class="left checkbox "></span>-->
                        <details><?php echo $value->description ?></details>
                        <div class="panel">
                            <div class="row">
                                <div class="columns large-6 large-centered">
                                    <img class="image-selector" src="<?php echo base_url() ?>images/products/<?php echo $value->image ?>">
                                </div>
                                <div class="columns large-12">
                                    <h5><span class="product-selector left text-left"><?php echo $value->product ?></span></h5>
                                </div>
                                <div class="columns large-7 price left">
                                    <h6 class="subheader">$ <span class="amount-selector"><?php echo number_format($value->cost) ?></span></h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class='botones '>
                                    <button id="<?php echo $value->id ?>" cost="<?php echo $value->cost ?>" class="columns small-6 large-6 right add"><?php echo ($value->method == AuthConstants::PM_GENERATE_CODE)?lang("add_code"):lang("add") ?></button>
                                    <button id="<?php echo $value->id ?>" cost="<?php echo $value->cost ?>" class="columns small-5 large-4 right view"><?php echo lang("view") ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
 
            </div> 
        </div> 

        
    </div>
</div>