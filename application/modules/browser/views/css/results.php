<style>
    #header {
        background: #FFF;
        border-bottom: 1px solid #383f5c;
    }
     #footer{
        color: #fff;
        position: fixed;
        bottom: 0;
        padding: 10px;
        font-size: small;
        text-align: center;
    }
    #header.cat6{
       background: #999 none repeat scroll 0 0;
    }
    .body-content {
        background: #E9EBEE;
    }
    #content {
        background: none;
        margin-bottom: 0px;
    }
    
   .content-modal table tr th,.content-modal table tr td {
    color: #222222;
    font-size: 0.5875rem;
    padding: 0.35625rem 0.4625rem;
  }
    
    .body-content #body {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    }
    button, .button {
        background-color: rgba(255, 255, 255, 0);
        border: 1px solid;
        border-radius: 50px;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0.5) inset !important;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-family: inherit;
        font-weight: normal !important;
        line-height: 1;
        margin: 0 0.2em 0.25em;
        padding: 0.75em 1em;
        position: relative;
        text-align: center;
        text-decoration: none;
         /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f8ac3e+0,ff3535+100 */
        background: #f8ac3e; /* Old browsers */
        background: -moz-linear-gradient(left,  #f8ac3e 0%, #ff3535 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(left,  #f8ac3e 0%,#ff3535 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to right,  #f8ac3e 0%,#ff3535 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8ac3e', endColorstr='#ff3535',GradientType=1 ); /* IE6-9 */
    }
    .login {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        font-size: 1rem;
        padding: 1em;
    }
    .category-title {
        color: #384060;
        font-size: 2.98rem;
        font-weight: 600;
        padding: 5% 10% 1%;
      }
    .main-category {
        color: #FA823B;
        font-size: 0.7rem;
        font-weight: 600;
        padding: 0 12% 5%;
      }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        height: 1.84em;
        border-radius: 0;
        padding: 0.15em;
        /*color: #2FB3E8;*/
        font-size: 0.69rem;
      }
    .filters {
        background: rgba(255,255,255,1) none repeat scroll 0 0;
        margin-left: 8% !important;
        min-height: 8em;
        padding: 9% 14% 9% 3%;
      }
    .reveal-modal{
        padding: 0.3rem;
        /*font-style: italic;*/
    }
    .reveal-modal h3{
        color: #353C5A;
        /*font-style: italic;*/    
        font-weight: 600;
    }
/*    .result-grid-row{
        height: 500px;
        overflow: auto;
    }*/
    .result-grid-row .panel {
        background: none;
        border-width: 0px;
        margin-bottom: 0.25rem;
        padding: 0rem 2rem 0;
        border-bottom: 1px solid #384060;
        background: #FFFFFF;
        border-radius: 25px;
      }
    .result-grid-row .panel h5 {
        color: #384060;
        font-family: 'Roboto Regular';
        font-size: 1rem;
        min-height: 50px;
    }
    .item .title {
        font-size: 1.14rem;
        margin: 6px 0 15px;
    }
    .box {
        border: 0px solid #33ccff;
      }
    .botones {
        padding: 1px 0px 30px;
      }
    .item .description {
        padding: 4px 10px;
    }
    .item .price, .item .total{
        color: #353C5A;
        font-size: 1.2rem;
        font-weight: 300;
    }
    .item .total{
        text-align: right;
    }
    .total {
        padding: 2% 4%;
      }
    .item .details {
        font-size: 0.9rem;
    }
    .item .description {
        font-size: 0.75rem;
        text-align: justify;
        padding: 4% 5% 1%;
        line-height: 1.3;
      }
      .compare .title{
         min-height: 50px;
         font-size: 0.9rem;
         text-align: center;
         margin: 6px 0 15px;
      }
      .compare .add-item{
         margin: 7% 0;
      }
      .compare .comparison{
        font-style: italic;
        font-size: 0.8rem;
      }
      .compare .description{
         min-height: 150px;
         font-size: 0.8rem;
         text-align: justify;
         padding: 4% 5% 1%;
      }
      .checkbox{
          background-image:url(<?php echo base_url()?>images/checkbox_unchecked.png);
          height: 32px;
          width: 32px;
      }
      .checked{
          background-image:url(<?php echo base_url()?>images/checkbox_checked.png);
          height: 32px;
          width: 32px;
      }
    .group {
        margin: 2% 0;
      }
      .properties {
        margin: 2% 0!important;
      }
      .details{
        text-transform: capitalize;
      }
    .botones button {
        background-color: #33ccff;
        font-size: 0.8rem;
        padding: 1.5%;
        margin: 0 0.5%;
        border: none;
    }   
    .transparency button{
        display:  none;
    }
    .price {
        margin-top: 1%;
      }
      .btn-row{
          padding: 5% 0;
      }
    .price h6{
        font-size: 1rem;
        color: #384060;
        font-family: inherit;
        font-weight: inherit;
        text-align: left;
    }
    .grid-item{
        transition: background 0.5s;
        padding-top: 1%;
        /*border-radius: 25px;*/
    }
    .grid-item details{
        display: none;
    }
    .panel:hover{
        background: #e6e6e6;
    }
    .grid-item img{
        border-radius: 5%;
    }
    select.multiple{
        height: 20%;
    }
    .question {
        margin-left: 7%;
        margin-top: 18%;
        position: absolute;
    }
    .question label {
        font-size: 2.5em;
        font-style: italic;
        color: #1D5AA7;
    }
    #browse {
        float: right;
        margin-right: 10%;
        margin-top: -13%;
    }
    #footer{
        color: #fff;
        font-size: 10pt;
        text-align: center;
    }
    #footer a{
        color: #fa823b;
    }
    #result-grid{
        padding: 2%;
    }
    .multioption{
        height: 100px;
    }
    .f-dropdown {
        border: 1px solid #cccccc;
        border-radius: 3%;
        /*margin-left: -100px;*/
        max-width: 300px;
    }
    
    
    
    
    
    
    
    /* Styles for screens small*/
@media only screen and (max-width: 40.063em) {
    #browse{
        margin-right: 0;
        margin-top: 2%;
    }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        font-size: 0.9rem;
        height: 2.2em;
    }
    .question label {
        color: #2c4090;
        font-size: 1em;
        font-style: italic;
    }
    select.multiple{
        height: 25px;
    }
    .botones button {
        margin: 0 0.5% 5%;
      }
    button, .button {
        background: #33ccff none repeat scroll 0 0;
        border: 1px solid;
        padding: 5% !important;
        min-height: 43px;
    }
    .text-center-for-small-only{
        text-align: center;
    }
    .cart{
        margin-top: -116px;
        margin-left: 80%;
    }
     .f-dropdown::after {
        left: 88%;
      }
    .f-dropdown::before {
        left: 88%;
      }
    #footer{
          position :relative;
          bottom: -4px;
      }
}
/* Styles for screens medium*/
@media only screen and (min-width: 40.063em) {
}
/* Styles form screens large*/
@media only screen and (min-width: 64.063em) {
   }

    
</style>