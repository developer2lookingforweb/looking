<style>
    #header {
        background: #FFF;
        border-bottom: 1px solid #383f5c;
    }
/*     #footer{
        display: none;
    }*/
    .body-content {
        background: #E9EBEE;
    }
    #content {
        background: none;
        margin-bottom: 0px;
    }
    
    .body-content #body {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    }
    .cart{
        display: none;
    }
    .items{
        border-bottom: 1px solid #a0a0a0;
    }
    .items .row {
        margin-bottom: 3%;
        margin-top: 3%;
    }
    .total {
        font-weight: 400;
        padding: 5% 0;
    }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        height: 2.3em;
        border-radius: 4px;
        padding: 0.15em;
        /*color: #2FB3E8;*/
        font-size: 0.69rem;
      }
      .validation-error{
       /*background: #FBEFF1!important;*/   
          border-color: #BE4747!important;
      }
      .fill-data {
        color: #373E5D;
        font-size: 2rem;
        font-style: italic;
        font-weight: 400;
        padding: 3% 0% 2%;
      }
/*    button, .button {
        background-color: rgba(255, 255, 255, 0);
        border: 1px solid;
        border-radius: 6px;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0.5) inset !important;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-family: inherit;
        font-weight: normal !important;
        line-height: 1;
        margin: 0 0 0.25em;
        padding: 0.25em 1em;
        position: relative;
        text-align: center;
        text-decoration: none;
    }*/
    .login {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        font-size: 1rem;
        padding: 1em;
    }
    .reveal-modal{
        padding: 0.3rem;
        font-style: italic;
    }
    .reveal-modal h3{
        color: #1D5AA7;
        font-style: italic;        
    }
    .result-grid-row .panel h5{
        font-size: 0.8rem;
        min-height: 60px;
    }
    .grid-item{
        transition: background 0.5s;
        padding-top: 1%;
    }
    .grid-item details{
        display: none;
    }
    .grid-item:hover{
        background: #eee;
    }
    .question {
        margin-left: 7%;
        margin-top: 18%;
        position: absolute;
    }
    .question label {
        font-size: 2.5em;
        font-style: italic;
        color: #1D5AA7;
    }
    #browse {
        float: right;
        margin-right: 10%;
        margin-top: -13%;
    }
    #footer{
        color: #fff;
        font-size: 10pt;
        text-align: center;
    }
    #footer a{
        color: #1D5AA7;
    }
    #result-grid{
        padding: 2%;
    }
    .multioption{
        height: 100px;
    }
    button, .button,button:hover, .button:hover,button:focus, .button:focus{
        background: #33CCFF;
        border-radius: 50px;
        margin: 0 0.2em 0.25em;
         /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f8ac3e+0,ff3535+100 */
        background: #f8ac3e; /* Old browsers */
        background: -moz-linear-gradient(left,  #f8ac3e 0%, #ff3535 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(left,  #f8ac3e 0%,#ff3535 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to right,  #f8ac3e 0%,#ff3535 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8ac3e', endColorstr='#ff3535',GradientType=1 ); /* IE6-9 */
    }
    
    
    
    
    
    
    
    /* Styles for screens small*/
@media only screen and (max-width: 40.063em) {
    #browse{
        margin-right: 0;
        margin-top: 2%;
    }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        font-size: 0.9rem;
        height: 2.2em;
    }
    .question label {
        color: #1d5aa7;
        font-size: 1em;
        font-style: italic;
    }
    .subtotal, #total{
        font-size:1.3rem;
    }
    .items{
        font-size: 1.3rem;
        padding-bottom: 20%;
    }
    .items > div {
        border-bottom: 1px dotted #c0c0c0;
        padding: 2% 0;
    }
    .item-name{
        padding: 5% 0;
        font-style: italic;
    }
    .quantity{
        padding: 5% 0;
    }
}
/* Styles for screens medium*/
@media only screen and (min-width: 40.063em) {
}
/* Styles form screens large*/
@media only screen and (min-width: 64.063em) {
   }

    
</style>