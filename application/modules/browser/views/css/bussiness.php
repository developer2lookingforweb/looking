<style>
    button.disabled, button[disabled], .button.disabled, .button[disabled] {
        background-color: #a0a0a0;
        border-color: #a0a0a0;
        box-shadow: none;
        color: white;
        cursor: default;
        opacity: 0.7;
    }
    .reveal-modal{
        padding: 0.3rem;
        font-style: italic;
    }
    .reveal-modal h3{
        color: #1D5AA7;
        font-style: italic;        
    }
    .result-grid-row .panel h5{
        font-size: 0.8rem;
        min-height: 60px;
    }
    .grid-item{
        transition: background 0.5s;
        padding-top: 1%;
    }
    .grid-item details{
        display: none;
    }
    .grid-item:hover{
        background: #eee;
    }
    .question {
        margin-left: 7%;
        margin-top: 18%;
        position: absolute;
    }
    .question label {
        font-size: 2.5em;
        font-style: italic;
        color: #1D5AA7;
    }
    #browse {
        float: right;
        margin-right: 10%;
        margin-top: -13%;
    }
    #footer{
        color: #fff;
        font-size: 10pt;
        text-align: center;
    }
    #footer a{
        color: #1D5AA7;
    }
    #result-grid{
        padding: 2%;
    }
    .multioption{
        height: 100px;
    }
    
    
    
    
    
    
    
    
    /* Styles for screens small*/
@media only screen and (max-width: 40.063em) {
    #browse{
        margin-right: 0;
        margin-top: 2%;
    }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        font-size: 0.9rem;
        height: 2.2em;
    }
    .question label {
        color: #1d5aa7;
        font-size: 1em;
        font-style: italic;
    }
}
/* Styles for screens medium*/
@media only screen and (min-width: 40.063em) {
}
/* Styles form screens large*/
@media only screen and (min-width: 64.063em) {
   }

    
</style>