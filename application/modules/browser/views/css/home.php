<style>
/*    #footer{
        display: none;
    }*/
    
    #header{
        background: rgba(255,255,255,1);
    }
    .body-content {
        background: rgba(0, 0, 0, 0) url(<?php echo base_url()?>images/homecolor.png) no-repeat fixed center 80px / cover ;
    }
    
    #content {
        background: none;
        margin-bottom: 0px;
    }
    
    .body-content #body {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    }
    
    #footer {
        background: rgba(50, 50, 50, 0.5) none repeat scroll 0 0;
        color: #fff;
        position: fixed;
        bottom: 0;
        padding: 10px;
        font-size: small;
        text-align: center;
   } 
   
   #modal-message .header-reveal-modal h3{
       color: #363D5A;
       font-style: italic;
       font-size: 1.489rem;
   }
   input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
    height: 1.84em;
    border-radius: 0;
    padding: 0.15em;
    margin: 0 0 0.5em;
    /*color: #2FB3E8;*/
    font-size: 0.69rem;
  }
 
  .question-zone label{
      color: white;
      font-size: 0.66em;
  }
   button, .button {
        background-color: rgba(255, 255, 255, 0);
        border: 0px solid;
        border-radius: 50px;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0.5) inset !important;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-family: inherit;
        font-weight: normal !important;
        line-height: 1;
        margin: 0 0.2em 0.25em;
        padding: 0.75em 1em;
        position: relative;
        text-align: center;
        text-decoration: none;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f8ac3e+0,ff3535+100 */
        background: #f8ac3e; /* Old browsers */
        background: -moz-linear-gradient(left,  #f8ac3e 0%, #ff3535 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(left,  #f8ac3e 0%,#ff3535 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to right,  #f8ac3e 0%,#ff3535 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8ac3e', endColorstr='#ff3535',GradientType=1 ); /* IE6-9 */

    }
    .login {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        font-size: 1rem;
        padding: 1em;
    }
    button:hover,
    .button:hover,
    button:focus,
    .button:focus {
        background-color: rgba(255, 255, 255, 0);
    }
    .disclamer {
        font-family: 'Roboto Regular',sans-serif;
        padding: 2% 0% 2% 0;
        color: #353b5a;
        font-size: 0.8rem;
      }
      .terms{
        margin-left: -21px;
        margin-top: 4px;
        marging-bottom: 5px;
      }
      .terms a{
          font-size: inherit;
      }
      .termns_and_cond ol li, .termns_and_cond p{
          font-size: 0.7rem;
          text-align: justify;
      }
      .termns_and_cond{
        height: 300px;
        overflow: auto;
        padding-right: 5%;
      }
      .categories{
          margin-left: -50%;
      }
      .categories li{
          text-align: center;
      }
      .categories li a {
        color: #363D5A;
        cursor: pointer;
        display: block;
        font-size: 0.575rem;
        font-weight: normal;
        line-height: 1.5;
        margin-bottom: 0;
        text-align: center;
      }
      .block-content > .content {
        display: none;
        float: left;
        /*padding: 0.9375em 0;*/
        width: 100%;
      }
      .block-content > .content.active {
        display: block;
      }
      .question-container {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-2 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-3 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-4 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-5 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-6 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-7 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .question-container-8 {
        background-color: rgba(53, 59, 90, 0.5);
        border-radius: 9px;
        /*padding: 2%;*/
      }
      .subcategories li{
          display: inline;
          padding: 2px;
    }
    .subcategories li.item {
        /*border-left: 1px solid white;*/
    }
    li.active {
        border-left: 1px solid white;
        border-right: 1px solid white;
        border-top: 0 solid #702626!important;
        margin-right: -1px;
    }
    #block1 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    
    #block2 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    
    #block3 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block3  input, #block3 select {
        color: #353b5a;
    }
    #block4 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block4 input, #block4 select {
        color: #353b5a;
    }
    #block5 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block5 input, #block5 select {
        color: #353b5a;
    }
    #block6 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block6 input, #block6 select {
        color: #353b5a;
    }
    #block7 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block7 input, #block7 select {
        color: #353b5a;
    }
    #block8 li.active {
        background: #353b5a none repeat scroll 0 0;
    }
    #block8 input, #block8 select {
        color: #353b5a;
    }
    .subcategories li a{
        font-size:0.6rem;
    }
    .subcategories li.left-corner {
        border-left: 0px solid white!important;
        border-radius: 9px 0 0 0;
    }
    .subcategories li.right-corner {
        border-right: 0px solid white;
        border-radius: 0 9px  0 0;
    }
    .question-zone{
        border-top: 0px solid white;
        padding: 1%;

    }
    .multioption{
        height: 20%;
    }
    select.multiple{
        height: 20%;
    }
    .tmp-disc{
        font-size: 1.1rem!important;
        font-style: italic;
    }
    .tab-title{
        background: #ff633a;
        border-left: 1px solid white;
    }
    .categories img{
        cursor: pointer;
        -webkit-transition: width 0.5s; /* Safari */
        transition: width 0.5s;
        width: 48px;
    }
    .categories img:hover{
        -webkit-transition: width 0.5s; /* Safari */
        transition: width 0.5s;
        width: 58px;
    }
      .subcategories li a {
        color: white;
        font-weight: 400;
        outline: medium none !important;
        padding: 0 4%;
        transform: rotate(0deg);      }
      
      .bottom-cats{
          position :fixed;
          bottom: 35px;
          width: 100%!important;
      }
      
      .comming-soon {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: rgba(255,255,255,0.3);
        height: 80px;
    }
     .comming-soon .categories img:hover{
        -webkit-transition: width 0.5s; /* Safari */
        transition: width 0.5s;
        width: 48px;
    }
    .comming-soon li {
        float: left;
        width: 12%;
    }
    
    .comming-soon-title {
        color: #363D5A;
        font-family: 'Roboto Regular';
        font-size: 1.2rem;
        font-weight: 600;
        margin: -5% 10%;
      }
    .comming-soon li img {
        /*filter: grayscale(100%);*/
    }
    .comming-soon li a {
        display: block;
        color: #363D5A;
        text-align: center;
        padding: 0 56px;
        text-decoration: none;
    }
    .tabs-content {
        margin-bottom: 0;
      }
      
      .instructions{
          font-size: 0.48rem;
      }
      
    /* Styles for screens small*/
@media only screen and (max-width: 40.063em) {
    #header{
        position:relative;
    }
    .body-content {
        background: #E9EBEE none repeat scroll 0 0;
    }
    #browse{
        margin-right: 0;
        margin-top: 2%;
    }
    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, select, .fileWrapper {
        font-size: 0.9rem;
        height: 2.2em;
    }
    .question label {
        color: #1d5aa7;
        font-size: 1em;
        font-style: italic;
    }
    .categories{
        margin-left: 0; 
    }
    .categories li{
        float: left;
        width: 33%; 
    }
    .subcategories li {
        min-height: 64px;
    }
    .subcategories li a {
         transform: rotate(-90deg)!important;
    }
    select.multiple{
        height: 25px;
    }
    .subcategories li{
        padding: 3%;
        text-align: center;
    }
    #footer{
          position :relative;
          bottom: -4px;
      }
    button, .button {
/*        background: #2ba6cb none repeat scroll 0 0;
        border: 1px solid;*/
        padding: 5% !important;
    }
     .cart{
        margin-top: -116px;
        margin-left: 80%;
    }
     .f-dropdown::after {
        left: 88%;
      }
    .f-dropdown::before {
        left: 88%;
      }
      .categories img:hover{
        -webkit-transition: width 0.5s; /* Safari */
        transition: width 0.5s;
        width: 48px;
    }
    .terms {
        margin-left: -16px;
        margin-bottom: 10px;
      }
}
/* Styles for screens medium*/
@media only screen and (min-width: 40.063em) {
}
/* Styles form screens large*/
@media only screen and (min-width: 64.063em) {
   }

    
</style>