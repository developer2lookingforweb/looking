<style>
/*    #header{
        display: none;
    }
    #body{
        margin-top: 0!important;
    }*/
    #header{
        background: rgba(50,50,50,0.5);
    }
    .body-content {
        background: rgba(0, 0, 0, 0) url(<?php echo base_url()?>images/homecolor.jpg) no-repeat fixed center center / cover ;
    }
    
    #content {
        background: none;
        margin-bottom: 0px;
    }
    .login {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        font-size: 1rem;
        padding: 1em;
        display: none;
    }
    .body-content #body {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    }
    button, .button {
        background-color: rgba(255, 255, 255, 0);
        border: 1px solid;
        border-radius: 6px;
        box-shadow: 0 0 0 rgba(255, 255, 255, 0.5) inset !important;
        color: white;
        cursor: pointer;
        display: inline-block;
        font-family: inherit;
        font-weight: normal !important;
        line-height: 1;
        margin: 0 0.2em 0.25em;
        padding: 0.75em 1em;
        position: relative;
        text-align: center;
        text-decoration: none;
    }
    button:hover,
    .button:hover,
    button:focus,
    .button:focus {
        background-color: rgba(255, 255, 255, 0);
    }
    #footer{
        display: none !important;
    }
    .disclamer {
        font-family: Roboto Thin Italic,sans-serif;
        font-style: italic;
        padding: 2% 0% 2% 0;
        color: #233487;
      }
    video {
        float: right;
        left: 0;
        position: absolute;
        width: 100%;
  }
</style>