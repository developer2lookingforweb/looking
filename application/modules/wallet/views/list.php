<?php
?>
<table id="list" class="display">
    <thead>
        <tr>
            <th><?php echo lang('id') ?></th>
            <th><?=lang('invoice')?></th>
            <!--<th><?=lang('image')?></th>-->
            <th><?=lang('name')?></th>
            <th><?=lang('last_name')?></th>
            <th><?=lang('value')?></th>
            <th><?=lang('fees')?></th>
            <th><?php echo lang('actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
        </tr>
    </tbody>
</table>
<?php echo form_open("") . form_close(); ?>

<div id="detailModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h5 id="modalTitle">Detalles de la cuenta</h5>
    <p class="lead">
    <div class="large-10 large-centered columns">
            <input name="cid" id="cid"   type="hidden"  value=""  >
            <div class="row" >
                <label  class="columns large-3 left"><h5>Nombres</h5></label>
                <label id="name" class="columns large-3 left"></label>
                <label id="last_name" class="columns large-3 left"></label>
            </div>                           
                <label  class="columns large-3 left"><h5>Factura</h5></label>
                <label id="invoice" class="columns large-2 left"></label>
                <label id="products" class="columns large-5 left"></label>
                <label id="pvalue" class="columns large-2 left"></label>
            <div class="row">
                <label  class="columns large-12 left"><h5>Histórico</h5></label>
                <div id="table">
                </div>                
            </div>                           
            <div class="row">
            </div>
            <div class="row">
            </div>
        </div>
    </div>
    </p>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>