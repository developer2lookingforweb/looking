<script type="text/javascript">
    $(document).ready(function() {
        $("select[name='idCategory']").val(<?=$category?>);
        $("select[name='idBrand']").val(<?=$brand?>);
        $("select[name='status']").val(<?=$status?>);
        $("select[name='paymentMethod']").val(<?=$paymentMethod?>);
        
        
        $("#form_properties").validate({
            rules: {
                properties: 'required'
            },
            messages: {
                properties:'<?=lang('required')?>'
            },
            submitHandler: function(form) {
                $('#form_properties').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert_form_properties').addClass("success");
                            $("#message_form_properties").html(data.message);
                            $("#alert_form_properties").show();
                        }
                        
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
    });
</script>