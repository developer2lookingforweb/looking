<script type="text/javascript">
    $(document).ready(function() {
        var list = $('#list').dataTable( {
            "aoColumns":[
                {"bVisible": true},
                null ,
                null ,
                null ,
                null ,
                {"bVisible": true,"bSearchable":false},
                null],
            "sAjaxSource": "<?php echo site_url('/wallet/getList'); ?>",
        });

        var id = 0;
        $( "#confirmation_delete" ).dialog({
            autoOpen: false,
            resizable: false,
            height:"auto",
            modal: true,
            buttons: {
                "<?php echo lang("button_delete_ok"); ?>": function() {
                    $.post("<?php echo site_url('/products/delete'); ?>",
                    {'id':id, '<?=$csrf?>': $('input[name=<?=$csrf?>]').val()},
                    function(data){
                        $("#confirmation_delete").dialog( "close" );
                        if(data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }
                        
                        if (data.warning != "") {
                            $('#alert').addClass("alert-info");
                            $("#message").html(data.warning);
                            $("#alert").show();
                        }
                        
                        if(data.message != ""){
                            list.fnDraw();
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                    },'json'
                );
                },
                "<?php echo lang("button_delete_ko"); ?>": function() {
                    $( this ).dialog( "close" );
                }
            }
        });

        $(document).on("click", ".action_delete", function(){
     	   id = this.id;
           $( "#message_delete" ).html("<?php echo lang("message_delete_product"); ?>");
           $( "#confirmation_delete" ).dialog("open");
        });
        
        $(document).on("click", ".action_detail", function(){
     	   id = this.id;
           $.post("<?php echo site_url('/wallet/detail'); ?>",
                {'id':id, '<?= $csrf ?>': $('input[name=<?= $csrf ?>]').val()},
                function(data){
                    if (data.error != "") {
                        $('#detailModal').foundation('reveal', 'open');
                        $("#name").html(data.data.name)
                        $("#last_name").html(data.data.last_name)
                        $("#invoice").html(data.data.invoice)
                        $("#pvalue").html("$ "+data.data.orderValue)
                        $("#products").html(data.data.products)
                        var html = '';
                        html += '<div class="columns large-3"><h5>Pago Min</h5></div>'
                        html += '<div class="columns large-3"><h5>Fecha</h5></div>'
                        html += '<div class="columns large-3"><h5>Pago</h5></div>'
                        html += '<div class="columns large-3"><h5>Fecha</h5></div>'
                         $.each(data.data.fees,function(idx,val){
                             var paid = (val.paid == null || val.paid == 0)?"":"$ "+val.paid;
                            html += '<div class="columns large-3"><label>$ '+val.value+'</label></div>'
                            html += '<div class="columns large-3"><label>'+val.payment_date+'</label></div>'
                            html += '<div class="columns large-3"><label>&nbsp;'+paid+'</label></div>'
                            html += '<div class="columns large-3"><label>&nbsp;'+val.transaction_date+'</label></div>'
                        })
                        $("#table").html(html)
                    }

                    if (data.warning != "") {
//                        $('#alert').addClass("alert-info");
//                        $("#message").html(data.warning);
//                        $("#alert").show();
                    }

                    if (data.message != "") {
                        list.fnDraw();
//                        $('#alert').addClass("success");
//                        $("#message").html(data.message);
//                        $("#alert").show();
                    }
                }, 'json'
            );
        });
        
        $(".filters").change(function(){
            var filterCol = $(this).attr('data-col');
            var filterValue = $(this).val();
            list.fnFilter(filterValue, filterCol);
//            list.fnDraw();
        })
    });
</script>
