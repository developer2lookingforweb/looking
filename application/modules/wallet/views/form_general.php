<div id="managerModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h5 id="modalTitle">File Manager</h5>
    <p class="lead">
        <iframe width="100%" height="550" frameborder="0" src="<?php echo base_url("imagemanager/dialog.php?type=1&fldr=products")?>"> </iframe>
    </p>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<?php 
    $imageArr= explode("|", $image);
    $opCat = array();
    $opCat[""] = lang("default_select");
    foreach ($categories as $aCat){
        $opCat[$aCat->getId()] = $aCat->getCategory();
    }
    $opBrand = array();
    $opBrand[""] = lang("default_select");
    foreach ($brands as $aBrand){
        $opBrand[$aBrand->getId()] = $aBrand->getBrand();
    }
    $opPayment = array();
    $opPayment[""] = lang("default_select");
    foreach ($paymentMethods as $aPaymentM){
        $opPayment[$aPaymentM->getId()] = $aPaymentM->getMethod();
    }
    $opStat = array();
    $opStat["0"] = lang("unpublished");
    $opStat["1"] = lang("published");
    $ouStat = array();
    $ouStat["1"] = lang("yes");
    $ouStat["0"] = lang("no");
    $fields = array();
    $fields["col-1"]["config"]       = array("large"=>"4");
    $fields["col-2"]["config"]       = array("large"=>"8");
    $fields["col-3"]["config"]       = array("large"=>"12");
    $fields["col-2"][lang('product')] = form_input(array('name'=>'product', 'id'=>'product', 'class'=>'span3 focused', 'value'=>$product));
    $fields["col-2"][lang('cost')] = form_input(array('name'=>'cost', 'id'=>'cost', 'class'=>'span3 focused', 'value'=>$cost));
    $fields["col-2"][lang('stock')] = form_input(array('name'=>'stock', 'id'=>'stock', 'class'=>'span3 focused', 'value'=>$stock));
    $fields["col-2"][lang('market_price')] = form_input(array('name'=>'market_price', 'id'=>'market_price', 'class'=>'span3 focused', 'value'=>$market_price));
    $fields["col-2"][lang('tax')] = form_input(array('name'=>'tax', 'id'=>'tax', 'class'=>'span3 focused', 'value'=>$tax));
    $fields["col-2"][lang('payment_method')] = form_dropdown("paymentMethod", $opPayment, $paymentMethod, "id='paymentMethod' class='filters span4'  data-col='7'");
    $fields["col-1"]["no-label"] = Soporte::creaTag("div", 
                                Soporte::creaTag("img", 
                                "", 
                                "id='profileThumb' src='".site_url("images/products/".$imageArr[0])."'"),
                            "class='center'");
//    $fields["col-2"][lang('image')] = my_form_upload(array('name'=>'image','id'=>'image', 'class'=>'span3 focused', 'value'=>$image));
    $fields["col-1"][lang('published')] = form_dropdown("status", $opStat, $status, "id='status' class='filters span4'  data-col='7'");
    $fields["col-1"][lang('outstanding')] = form_dropdown("outstanding", $ouStat, $outstanding, "id='outstanding' class='filters span4'  data-col='7'");
    $fields["col-1"][lang('category')] = form_dropdown("category", $opCat, $category, "class='span4'");
    $fields["col-2"][lang('brand')] = form_dropdown("brand", $opBrand, $brand, "class='span4'");
    $fields["col-3"]["no-label"] = form_textarea(array('name'=>'description', 'id'=>'tinymce', 'class'=>'span3 focused tinymce','rows'=>10),$description);
//    $fields["col-3"]["no-label"] = '<a href="#" class="button radius right"  data-reveal-id="managerModal"><i class=" icon-image"></i>&nbsp;Agregar Imagen </a>';
//    if($id>0)$fields["col-1"][lang('likes')] = form_input(array('name'=>'likes', 'class'=>'large-6 focused', 'value'=>$likes));
//    if($id>0)$fields["col-1"][lang('createdBy')] = form_input(array('name'=>'createdBy', 'class'=>'large-6 focused', 'value'=>$createdBy));
//    if($id>0)$fields["col-2"][lang('creationDate')] = form_input(array('name'=>'creationDate', 'class'=>'large-6 focused', 'value'=>$creationDate));
    $hidden = array('id' => $id);
    echo print_form('/products/persist/', $fields, $hidden,"formGeneral",false,12);