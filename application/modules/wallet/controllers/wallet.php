<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wallet extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$data = array();
//		$actions["create_product"] = site_url("products/form");

                $this->loadRepository("Brands");
                $this->loadRepository("Categories");

//                $data['categories'] = $this->Categories->findBy(array("status"=>"1"));
//                $data['brands'] = $this->Brands->findBy(array("status"=>"1"));
		$data["title"] = lang("Wallet");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("OrdersFees", "off", array("__SUM(off.value) - SUM(off.paid)"=>array("amount","e"),"__COUNT(off.value)"=>array("fees","f")) );
        $date = new DateTime();
        $model->setNumerics(array("o.id"));
        $model->setGroupBy(array("o.invoice"));
        $model->setConditions(array("off.payment_date < '".$date->format('Y-m-d')."'",
            "(off.status <> ".AuthConstants::IN_APPROVED." OR off.status IS NULL)"));
        
        $cat = new Model("Orders", "o", array("id" => array("id","a"), 'invoice' => array('invoice',"b")));
        $cat->setRelation("order");
        $brand = new Model("Customers", "c", array("name"=>array("name","c"),"last_name"=>array("last_name","d") ));
        $brand->setRelation("customer");
        $brand->setModelJoin("o");
        $brand->setNumerics(array("c.id"));
        
        
        $actions = array();
//        array_push($actions, new Action("products", "form", "edit"));        
        array_push($actions, new Action("wallet", "detail", "detail", false));        
//        array_push($actions, new Action("wallet", "walletDetail", "change_outstanding", false));        
//        array_push($actions, new Action("products", "outstanding", "change_outstanding", false));        
//        var_dump($_REQUEST);exit;
        $relations = array();
        array_push($relations, $cat);
        array_push($relations, $brand);
        
        $this->model   = $model;
        $this->actions = $actions;
	$this->relations = $relations;
        
        }
    
    public function detail ($identifier = 0){
        $identifier = $this->input->post("id");
        $this->loadRepository("OrdersFees");
        $this->loadRepository("Orders");
        $this->loadRepository("ProductsOrders");
        $order = $this->Orders->find($identifier);
        $fees = $this->OrdersFees->findBy(array("order"=>$identifier));
        $products = $this->ProductsOrders->findBy(array("order"=>$identifier));
        $data = array();
        $data['name'] = $order->getCustomer()->getName();
        $data['last_name'] = $order->getCustomer()->getLastName();
        $data['invoice'] = $order->getInvoice();
        $data['orderValue'] = $order->getValue();
        $productsList = array();
        foreach($products as $aProduct){
            if($aProduct->getProduct()->getId()!== AuthConstants::FN_FINANCIAL_ID){
                $productsList[] = $aProduct->getProduct()->getProduct();
            }
        }
        $feesList = array();
        foreach($fees as $aFee){
            if($aFee->getTransactionDate() !== null){
                $feesList[] = array("value"=>$aFee->getValue(),"paid"=>$aFee->getPaid(),"payment_date"=>$aFee->getPaymentDate()->format("Y-m-d"),"transaction_date"=>$aFee->getTransactionDate()->format("Y-m-d"));
            }else{
                $feesList[] = array("value"=>$aFee->getValue(),"paid"=>$aFee->getPaid(),"payment_date"=>$aFee->getPaymentDate()->format("Y-m-d"),"transaction_date"=>"");
            }
        }
        
        $data['products'] = implode("-", $productsList);
        $data['fees'] = $feesList;
        echo json_encode(array("status"=>true,"data"=>$data));
    }
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $prod = $this->input->post('product');
        $product = $this->input->post('product');
        $status = $this->input->post('status');
        $outstanding = $this->input->post('outstanding');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $cost = $this->input->post('cost');
        $market_price = $this->input->post('market_price');
        $tax = $this->input->post('tax');
        $paymentMethod = $this->input->post('paymentMethod');
        $brand = $this->input->post('brand');
        $category = $this->input->post('category');
        $stock = $this->input->post('stock');
        $properties = array();
        
        if ($identifier > 0){
            $output = $this->rest->get('products/product/', array("id"=>$identifier));
            
            if ($output->status){
                $prod    = $output->data;
                $product = $prod->product;
                $image = $prod->image;
                $description = $prod->description;
                $cost = $prod->cost;
                $market_price = $prod->market_price;
                $tax = $prod->tax;
                $paymentMethod = $prod->payment_method->id;
//                echo "<pre>";
//                var_dump($prod); exit;
                $status = $prod->status;
                $stock = $prod->stock;
                $outstanding = $prod->outstanding;
                $brand = $prod->brand->id;
                $category = $prod->category->id;
                $id         = $prod->id;
                $extradql = "select prop.id, prop.property, prop.value, prop.rank from models\\Properties prop join prop.product pr
                        where  pr.id = ". $id;
                $querySearch = $this->em->createQuery($extradql);
                $properties = $querySearch->getResult();
            }
        }
        
        $actions = array();
        $actions["return_product"] = site_url("products/index");
        
        $this->loadRepository("Brands");
        $this->loadRepository("Categories");
        $this->loadRepository("PaymentMethods");
        $this->loadRepository("CategoriesConfigs");
        
        $data = array();
        $data["title"]  = lang("Products");
        $data['product'] = $product;
        $data['image'] = $image;
        $data['description'] = $description;
        $data['cost'] = $cost;
        $data['market_price'] = $market_price;
        $data['tax'] = $tax;
        $data['paymentMethod'] = $paymentMethod;
        $data['brand'] = $brand;
        $data['outstanding'] = $outstanding;
        $data['status'] = $status;
        $data['stock'] = $stock;
        $data['category'] = $category;
        $data['categories'] = $this->Categories->findBy(array("status"=>"1"));
        $data['brands'] = $this->Brands->findBy(array("status"=>"1"));
        $data['paymentMethods'] = $this->PaymentMethods->findBy(array("status"=>"1"));
        $data["id"] = $id;
        if($id > 0){
            $dataProperties = array();
            $dataProperties['title'] = lang('properties');
            $dataProperties['categories_configs'] = $this->CategoriesConfigs->findBy(array("category"=>$category));
            $dataProperties['properties'] = $properties;
            $images = array();
            $images['title'] = lang('images');
            $images['properties'] = "";
            $views = array(); 
            $views['form_general'] = $data;
            $views['form_properties'] = $dataProperties;
            $views['images'] = $images;

            $sectionActions = array();
            $tabs=true;
            $this->viewSections($views,$actions,$tabs);
        }else{
            $data['image'] = "noimage.jpg";
            $this->view('form_general', $data, $actions);
            
        }
    }
    
    public function saveImage (){
            $image64 = $this->input->post('image');
            $data = explode(";",$image64);
            $splitted = explode(",",$image64);
            $ext = explode("/",$data[0]);
            
            $id = $this->input->post('id');
            $name = $id."-".substr(md5(rand(1,999999)),0,8).".".$ext[1];
            
            $path = "images/products/".$name;

            $result = file_put_contents($path,base64_decode($splitted[1]));
            
            $status = false;
            if($result !== false){
                $this->loadRepository("Products");
                $product = $this->Products->find($id);
                $dbImage = $product->getImage();
                if($dbImage == "noimage.jpg"){
                    $dbImage = $name;
                }else{
                    $dbImage .= "|".$name;
                }
                $product->setImage($dbImage);
                $this->em->persist($product);
                $this->em->flush();
                $status = true;
            }
            echo json_encode(array('status'=>$status,'name'=>$name));
    }
    
    public function deleteImage (){
            $name = $this->input->post('image');
            
            $id = $this->input->post('id');
            $path = "images/products/".$name;

            $result = unlink($path);
            
            $status = false;
            if($result !== false){
                $this->loadRepository("Products");
                $product = $this->Products->find($id);
                $dbImage = explode("|",$product->getImage());
                
                $deleteIdx = array_search($name, $dbImage);
                unset($dbImage[$deleteIdx]);
                if(count($dbImage)==0)$dbImage = ["noimage.jpg"];
                $product->setImage(implode("|",$dbImage));
                $this->em->persist($product);
                $this->em->flush();
                $status = true;
            }
            echo json_encode(array('status'=>$status,'images'=>implode("|",$dbImage)));
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('product', 'lang:product', 'required');
		$this->form_validation->set_rules('description', 'lang:description', 'required');
		$this->form_validation->set_rules('cost', 'lang:cost', 'required');
//		$this->form_validation->set_rules('image', 'lang:image', 'required');
//		$this->form_validation->set_rules('brand', 'lang:brand', 'required');
//		$this->form_validation->set_rules('category', 'lang:category', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('products/product', $this->input->post()); 
            }else{
                $_POST["outstanding"]=0;
                $_POST["stock"]=1;
                $_POST["alternative_cost"]=0;
                $_POST["price"]=0;
                $_POST["image"]="noimage.jpg";
                $_POST["reference"]="reference";
                $_POST["last_update"]=date("Y-m-d");
//                var_dump($this->input->post());exit;
                $output = $this->rest->put('products/product', $this->input->post()); 
            }
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('product_edition') : lang('product_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            $data["id"]   = $this->input->post("id");
            if(!$this->input->post("id") > 0){
                $data["id"]   = $output->id;
                
            }
            echo json_encode($data);
        }
    }

    public function persistProperties ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        $this->form_validation->set_rules('properties[]', 'lang:properties[]', 'required');
        $this->loadRepository("Properties");
        $this->loadRepository("Products");
        if ($this->form_validation->run($this)){
            
            $id = $this->input->post("id");
            $product = $this->Products->find($id);
            $ranks = $this->input->post("rank");
            foreach ($this->input->post("properties") as $idx => $prop) {
                $pName = str_replace("idx_", "", $idx);
                $property = $this->Properties->findOneBy(array("product"=>$id,"property"=>$pName));
                if($property != null){
                    $prp = $property->getProperty();
                    if($prp == $pName){
                        $property->setValue($prop);
                        $property->setRank($ranks[$idx]);
                        $this->em->persist($property);
                        $this->em->flush();
                        $message = lang("properties_saved");
                    }
                        
                    
                }else{
                    $property = new models\Properties();
                    $property->setProduct($product);
                    $property->setProperty($pName);
                    $property->setValue($prop);
                    $property->setIsEvaluable(0);
                    $property->setShowProperty(1);
                    $property->setRank($ranks[$idx]);
                    $this->em->persist($property);
                    $this->em->flush();
                    $message = lang("properties_updated");
                }
//                var_dump($property);exit;
//                      var_dump($message);exit;
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            $data["id"]   = $this->input->post("id");
            if(!$this->input->post("id") > 0){
                $data["id"]   = $output->id;
                
            }
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('products/product', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("product_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }

    public function publish()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        $product = $this->em->find('models\Products', $this->input->post("id"));
        $status = ($product->getStatus()==1)?1:0;
        $output = $this->rest->post('products/status', array("id"=>$this->input->post("id"),"status"=>(!$status)));

        if ($output->status){
            $message = lang("product_status_changed");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
    public function outstanding()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        $product = $this->em->find('models\Products', $this->input->post("id"));
        $outstanding = ($product->getOutstanding()==1)?1:0;
        $output = $this->rest->post('products/outstanding', array("id"=>$this->input->post("id"),"outstanding"=>(!$outstanding)));

        if ($output->status){
            $message = lang("product_status_changed");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}