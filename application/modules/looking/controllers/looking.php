<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!session_id()) {
    session_start();
}
class Looking extends MY_Controller 
{
    public $public = true;
    public function setListParameters(){
        $this->session->set_userdata(AuthConstants::CATEGORIES,        $this->getCategories(1));
        
    }
    

    public function usersByDay()
    {
        $data = array();
        $data["title"] = AuthConstants::ML_INFONAME;
        $data["users"] = $this->getUsersByDate();
        $data["date"] = date("Y/m/d");
        $this->loadRepository("Campaigns");
        $campaigns = $this->Campaigns->findAll();
        $data["campaigns"] = $campaigns;
//        var_dump($campaigns[0]);exit;
        $this->viewFrontend('usersByDay', $data);
    }
    
    public function getUsersByDate($returnArray = false){

        $filterDate1 = ($this->input->post("date1"))?$this->input->post("date1"):date("Y-m-d");
        $filterDate2 = ($this->input->post("date2"))?$this->input->post("date2"):date("Y-m-d");
        $campaign = ($this->input->post("campaign"))?$this->input->post("campaign"):0;
        $from = new \DateTime($filterDate1." 00:00:00");
        $to   = new \DateTime($filterDate2." 23:59:59");
//        $date = new DateTime($filterDate);
        $this->loadRepository("Customers");
        if($campaign > 0){
//            $this->loadRepository("Campaigns");
//            $campaign = $this->Campaings->find($campaign);
            $query = $this->Customers->createQueryBuilder('p')
            ->where("p.creationDate BETWEEN :date1 AND :date2")
            ->andWhere("p.campaign = :campaign")
            ->setParameter('date1', $from)
            ->setParameter('date2', $to)
            ->setParameter('campaign', $campaign)
            ->getQuery();
        }else{
            $query = $this->Customers->createQueryBuilder('p')
            ->where("p.creationDate BETWEEN :date1 AND :date2")
            ->setParameter('date1', $from)
            ->setParameter('date2', $to)
            ->getQuery();
        }
        $users = $query->getResult();
//        $users = $this->Customers->findBy(array("creationDate"=>$date));
        $arrayUsers = array();
        foreach ($users as $value) {
            $campaign = ($value->getCampaign()== null)?"N/A":$value->getCampaign()->getName();
            $data["id"] = $value->getId();
            $data["name"] = $value->getName();
            $data["last_name"] = $value->getLastName();
            $data["email"] = $value->getEmail();
            $data["creationDate"] = $value->getCreationDate()->format("Y/m/d");
            $data["facebookId"] = $value->getFacebookId();
            $data["campaign"] = $campaign;
            $arrayUsers[] = $data;
        }
        if (!$this->input->is_ajax_request() || $returnArray) {
            return $arrayUsers;
        }  else {
            echo json_encode(array('status'=>true,'users'=>$arrayUsers));
        }
    }
    public function downloadExcel() {
        $this->load->library('Spreadsheet_Excel_Writer');
        $sheet = & $this->spreadsheet_excel_writer->addWorksheet('content');
        $customers = $this->getUsersByDate(true);
        $data = array();

         $data[] = lang('Id');
         $data[] = lang('name');
         $data[] = lang('last_name');
         $data[] = lang('email');
         $data[] = lang('creation_date');
         $data[] = lang('facebook_id');
         $data[] = lang('campaign');

        $rows[] = $data;

        foreach ($customers as $customer) {
            $aRow = array();
            $aRow[] = $customer["id"];
            $aRow[] = $customer["name"];
            $aRow[] = $customer["last_name"];
            $aRow[] = $customer["email"];
            $aRow[] = $customer["creationDate"];
            $aRow[] = $customer["facebookId"];
            $aRow[] = $customer["campaign"];
            $rows[] = $aRow;
        }

        foreach ($rows as $i => $row) {
                foreach ($row as $j => $col) {
                        $sheet->write($i, $j, utf8_decode($col));
                }
        }

        $this->spreadsheet_excel_writer->send('Reporte_Usuarios_' . date("Y-m-d") . '.xls');
        @$this->spreadsheet_excel_writer->close();
        exit();
    }
    
    public function puntoredReport()
    {
        $data = array();
        $data["title"] = AuthConstants::ML_INFONAME;
        $data["fills"] = $this->getFillsByDate();
        $data["date1"] = strtotime("-1 week");
        $data["date2"] = new DateTime();
        $this->loadRepository("Campaigns");
        $users = explode("||", AuthConstants::WS_WEBUSERPASSWD);
        $data["users"] = explode("|",$users[0]);
//        var_dump($campaigns[0]);exit;
        $this->viewFrontend('puntoredReport', $data);
    }
    
    public function getFillsByDate($returnArray = false){

        $filterDate1 = ($this->input->post("date1"))?$this->input->post("date1"):date("Y-m-d");
        $filterDate2 = ($this->input->post("date2"))?$this->input->post("date2"):date("Y-m-d");
        $place = ($this->input->post("place"))?$this->input->post("place"):0;
        $from = new \DateTime($filterDate1." 00:00:00");
        $to   = new \DateTime($filterDate2." 23:59:59");
//        $date = new DateTime($filterDate);
        $this->loadRepository("MobileRefills");
        if($place !== 0){
//            $this->loadRepository("Campaigns");
//            $campaign = $this->Campaings->find($campaign);
            $query = $this->MobileRefills->createQueryBuilder('p')
            ->where("p.request_date BETWEEN :date1 AND :date2")
            ->andWhere("p.user like :place")
            ->setParameter('date1', $from)
            ->setParameter('date2', $to)
            ->setParameter('place', $place)
            ->getQuery();
        }else{
            $query = $this->MobileRefills->createQueryBuilder('p')
            ->where("p.request_date BETWEEN :date1 AND :date2")
            ->setParameter('date1', $from)
            ->setParameter('date2', $to)
            ->getQuery();
        }
        $users = $query->getResult();
//        $users = $this->Customers->findBy(array("creationDate"=>$date));
        $arrayUsers = array();
        foreach ($users as $value) {
//            $place = ($value->getUser()== null)?"N/A":$value->getUser();
//            var_dump($value->getUser());
            $data["id"] = $value->getId();
            $data["filled"] = $value->getFilled();
            $data["value"] = $value->getValue();
            $data["brand"] = $value->getBrand()->getBrand();
            $data["number"] = $value->getNumber();
            $data["requestDate"] = $value->getRequestDate()->format("Y/m/d");
            $data["place"] = $value->getUser();
            $arrayUsers[] = $data;
        }
        if (!$this->input->is_ajax_request() || $returnArray) {
            return $arrayUsers;
        }  else {
            echo json_encode(array('status'=>true,'users'=>$arrayUsers));
        }
    }
    public function downloadFillsExcel() {
        $this->load->library('Spreadsheet_Excel_Writer');
        $sheet = & $this->spreadsheet_excel_writer->addWorksheet('content');
        $customers = $this->getFillsByDate(true);
        $data = array();

         $data[] = lang('Id');
         $data[] = lang('filled');
         $data[] = lang('value');
         $data[] = lang('brand');
         $data[] = lang('number');
         $data[] = lang('request_date');
         $data[] = lang('user');

        $rows[] = $data;

        foreach ($customers as $customer) {
            $aRow = array();
            $aRow[] = $customer["id"];
            $aRow[] = $customer["filled"];
            $aRow[] = $customer["value"];
            $aRow[] = $customer["brand"];
            $aRow[] = $customer["number"];
            $aRow[] = $customer["requestDate"];
            $aRow[] = $customer["place"];
            $rows[] = $aRow;
        }

        foreach ($rows as $i => $row) {
                foreach ($row as $j => $col) {
                        $sheet->write($i, $j, utf8_decode($col));
                }
        }

        $this->spreadsheet_excel_writer->send('Reporte_Usuarios_' . date("Y-m-d") . '.xls');
        @$this->spreadsheet_excel_writer->close();
        exit();
    }
    
    public function getMetas($name,$desc,$price=0,$image=""){
        $metas = array();
        $metas["site_name"] = AuthConstants::ML_INFONAME;
        $metas["title"] = $name;
        $metas["url"] = base_url($this->uri->uri_string());
        $metas["type"] = "website";
        if($price>0){
            $metas["type"] = "article";
            $metas["amount"] = $price;
            $metas["currency"] = "COP";
        }
        if($image == ""){
            $metas["image"] = base_url("images/alofijologoGrande.png");
        }  else {
            $metas["image"] = base_url("images/products/".$image);
        }
        $metas["description"] = $desc;
        return $metas;
    }
    public function index()
	{
		$data = array();
		$data["title"] = AuthConstants::GN_SITENAME;
                $data["categories"] = $this->getCategories(1,true);
                // $this->loadRepository("Blogs");
                // $blogs = $this->Blogs->findBy(array("published"=>1));
                // $lastBlogs = array_slice($blogs, -3, 3, true);
                // $data["blogs"] = array_reverse($lastBlogs);
                // $csrf = $this->config->config["csrf_token_name"];
                // $hash = $this->security->get_csrf_hash();;
                // $data['csrf'] = $csrf;
                // $data['hash'] = $hash;
                $data['metas'] = $this->getMetas(AuthConstants::GN_SITENAME, AuthConstants::GN_DESCRIPTION);
                $data['loginUrl'] = $this->getFbUrl();
                if( $this->session->userdata(AuthConstants::USER_ID)){
                    redirect("outstanding");
                }  else {
                    if(! $this->session->userdata(AuthConstants::CART)){
                        $this->session->set_userdata(array(AuthConstants::CART=>array()));
                    }
                    if($this->input->get('msg') !== false){
                        $this->session->set_userdata(array(AuthConstants::CART=>array()));
                        $message = explode("--", $this->input->get('msg'));
                        $data["message"] = lang($message[0]).$message[1];                    
                    }
                    if($this->input->get('userExist') !== false){
                        $this->session->set_userdata(array(AuthConstants::CART=>array()));
                        $message = "Usuario registrado previamente";
                        $data["exist"] = $message;                    
                    }
                    if($this->input->get('loginError') !== false){
                        $this->session->set_userdata(array(AuthConstants::CART=>array()));
                        $message = "Error en el ingreso";
                        $data["error"] = $message;                    
                    }
                    if($this->input->get('signinError') !== false){
                        $this->session->set_userdata(array(AuthConstants::CART=>array()));
                        $message = "Error en el ingreso";
                        $data["signinError"] = $message;                    
                    }
                    $this->loadRepository("Banners");
                    
                    // $data["banners"] = $this->Banners->findBy(array("published"=>1));
                     $output = $this->rest->get('browser/outstanding/', array());
                     $data["outstanding"] = $output->data;
                     
                    // $output = $this->rest->get('browser/top_sales/', array());
                    // $data["topSales"] = $output->data;
                    // $output = $this->rest->get('browser/last_updated/', array());
                    // $data["lastUpdated"] = $output->data;
                    // $output = $this->rest->get('browser/total_sales/', array());
                    // $data["sales"] = $output->data;
                    // $output = $this->rest->get('browser/total_customers/', array());
                    // $data["customers"] = $output->data;
                    // $output = $this->rest->get('browser/total_products/', array());
                    // $data["products"] = $output->data;
                    // $data["sales"] = $data["sales"][0]->total;
                    // $data["customers"] = $data["customers"][0]->total;
                    // $data["products"] = $data["products"][0]->total;
                    $data["viewblogs"] = false;
                    $data["viewbenefits"] = false;
                    $data["viewtestimonials"] = false;
                    $data["viewnewsletter"] = false;
                    $data["viewcontactus"] = false;
                    

//                    $data["mainCategory"] = 1;
//                    $data["categories3"] = $this->getSubcategories(8);``      
                    $this->viewFrontend2('home', $data);
                }
	}
        public function bienvenido(){
            $this->outstanding(true);
            
        }
        public function newsletter(){
            $this->load->helper('sendy');
            $sendy = new Sendy(AuthConstants::SDY_NEWSLETTERLIST);
            $sendy->subscribe("", $this->input->post("email"));
            
        }
        public function principal(){
            $this->outstanding();
	}
        public function related($pid){
            $this->loadRepository("ProductsTags");
            $propertyArray = $this->ProductsTags->findBy(array("products"=>$pid  ));
            $tags = array();
            foreach ($propertyArray as $related){
                $tags[] = $related->getTag()->getId();
            }
            $ids = array();
            if(count($tags)>0){
                $query = $this->ProductsTags->createQueryBuilder('o')
                    ->select( "distinct(p.id)" )
                        ->join("o.products"," p")
                    ->where("o.tags in (".implode(",", $tags).")")
                    ->andWhere("p.id < > ".$pid)
                    ->getQuery();
                $ids = $query->getResult();
            }
//            var_dump($ids);exit;
            $imploded = "";
            foreach ($ids as $idx => $item){
                if($idx > 0)$imploded .= ",";
                $imploded .= $item["id"];
            }
            $results = array();
            if(count($ids)>0){
            $dql = "SELECT prod.id, prod.product, prod.description, prod.cost, prod.price, prod.alternative_cost, prod.last_update, prod.market_price, prod.image, prov.id provid, prov.brand brand,  payment.id method, cat.id category FROM models\\Products prod ";
            $dql .= "JOIN prod.brand prov ";            
            $dql .= "JOIN prod.category cat ";            
            $dql .= "JOIN prod.payment_method payment ";            
            $dql .= "WHERE  prod.status =1  AND prod.cost > 0 AND prod.stock > 0 ";
            $dql .= "AND prod.id in (".$imploded.")";
//            $dql .= " GROUP by prov.id ";
            $dql .= " ORDER by prod.last_update asc ";
//            echo $dql;
            $querySearch = $this->em->createQuery($dql); //->setMaxResults(8);
            $results = $querySearch->getResult();
            }
            return $results;
	}
        public function detail($pid){
            $productId = $pid;
            $this->loadRepository("Products");
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
            $prod = array();
            $product = $this->Products->find($productId);
            $propertyArray = $this->Properties->findBy(array("product"=>$productId,'show_property'=>1),array("property"=>"ASC"));
            $props = array();
            $info = array();
            $data = array();
            $data["title"] = $product->getProduct();
            $data['related'] = $this->related($pid);
            $info['id'] = $product->getId();
            $info['product'] = $product->getProduct();
            $info['cost'] = $product->getCost();
            $info['stock'] = $product->getStock();
            $info['category'] = $product->getCategory()->getId();
            $info['brand'] = $product->getBrand()->getBrand();
            $info['market_price'] = $product->getMarketPrice();
            $info['description'] = $product->getDescription();
            $info['image'] = $product->getImage();
            $info['method'] = $product->getPaymentMethod()->getId();
            $data['show_files'] = false;
            if($product->getCategory()->getId()==9){
                $file["jpg"] = is_file(FCPATH."files".DIRECTORY_SEPARATOR.$pid.".jpg");
                $file["png"] = is_file(FCPATH."files".DIRECTORY_SEPARATOR.$pid.".png");
                $file["gif"] = is_file(FCPATH."files".DIRECTORY_SEPARATOR.$pid.".gif");
                $file["pdf"] = is_file(FCPATH."files".DIRECTORY_SEPARATOR.$pid.".pdf");
                foreach($file as $ext => $exist){
                    if($exist){
                        $data['show_files'] = true;
                        $data['file_type'] = $ext;
                    }
                }
            }
            $data['metas'] = $this->getMetas($product->getProduct(), $product->getDescription(),$product->getCost(),$product->getImage());
            $this->load->helper('favorites');
            $obj = new Favorites();
            $info['favorite'] = $obj->isFavorite($productId);

            $this->load->helper('offers');
            $obj = new Offers();
            $info['alternative_cost'] = $product->getAlternativeCost();
            $info['offer'] = $obj->findOffer($product,1);
            $info['offerId'] = $obj->findOfferId($product,1);
            $response['status']=true;
            $generalRank = $rankCount = 0;
            foreach ($propertyArray as $property) {
                $propsDetails = array();
                $propsDetails['property'] = $property->getProperty();
                $propsDetails['rank'] = round($property->getRank(), 2);
                $generalRank += round($property->getRank(), 2);
                $rankCount ++;
                $propsDetails['label'] = lang(strtolower($property->getProperty()));
                if($property->getProperty()== 'COSTO' || $property->getValue() == 999 || $property->getValue() == 9999 || lang(strtolower($property->getProperty()."_unit")) == "$"){
                    if(($property->getValue() == 9999 || $property->getValue() == 999) && $property->getProperty()!= 'COSTO' && lang(strtolower($property->getProperty()."_unit"))!=="$"){
                        $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".  lang("unlimited");                        
                    }else{
                        $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".$property->getValue();                        
                    }
                }else{
                    $propsDetails['value'] = $property->getValue()." ".lang(strtolower($property->getProperty()."_unit"));                                                
                }
                $props[] = $propsDetails;
            }
            $data['details']= $props;
            $data['generalRank']= ($rankCount>0)?$generalRank/$rankCount:0;
            $data['info'] = $info;
            $data["categories"] = $this->getCategories(1);
            $response['data']['details'] = $props;
            $this->viewFrontend2('detail2', $data);
            
	}
        public function credit($pid){
            $productId = $pid;
            $this->loadRepository("Products");
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
            $prod = array();
            $product = $this->Products->find($productId);
            $propertyArray = $this->Properties->findBy(array("product"=>$productId,'show_property'=>1),array("property"=>"ASC"));
            $props = array();
            $info = array();
            $data = array();
            $data["title"] = $product->getProduct();
            $info['id'] = $product->getId();
            $info['product'] = $product->getProduct();
            $info['cost'] = $product->getCost();
            $info['category'] = $product->getCategory()->getId();
            $info['brand'] = $product->getBrand()->getBrand();
            $info['market_price'] = $product->getMarketPrice();
            $info['description'] = $product->getDescription();
            $info['image'] = $product->getImage();
            $info['method'] = $product->getPaymentMethod()->getId();
            $data['show_files'] = false;
            $data['metas'] = $this->getMetas($product->getProduct(), $product->getDescription(),$product->getCost(),$product->getImage());

            $this->load->helper('offers');
            $obj = new Offers();
            $info['alternative_cost'] = $product->getAlternativeCost();
            $info['offer'] = $obj->findOffer($product,1);
            $info['offerId'] = $obj->findOfferId($product,1);
            $response['status']=true;
            $generalRank = $rankCount = 0;
            
            $data['info'] = $info;
            $data["categories"] = $this->getCategories(1);
            $response['data']['details'] = $props;
            $this->viewFrontend2('credit', $data);
            
	}
        public function simulator(){
            $response = array('status'=>false,'data'=>array());
            $prod = array();
            $props = array();
            $info = array();
            $data = array();
            $data["title"] = "Simulador";
            $data['info'] = $info;
            $data["categories"] = $this->getCategories(1);
            $response['data']['details'] = $props;
            $this->viewFrontend2('simulator', $data);
            
	}
        public function puntored(){
            
            $data['title'] = "Recargas PuntoRed";
            $data['brands'] = ["claro","movistar","tigo","etb","avantel","virgin"];
            $data['amount'] = ["2000","3000","4000","5000","6000","10000","15000","20000","30000"];
            $users = explode("||",AuthConstants::WS_WEBUSERPASSWD);
            $passwords = explode("|",$users[1]);
            $users = explode("|",$users[0]);
            $data['users'] = $users;
            $data['passwords'] = $passwords;
            $this->viewFrontend('puntored', $data);
            
	}
        public function singleFill(){
            
            $data['title'] = "Recargas PuntoRed";
            
            
            include("consumerWS.php"); 
            ////nueva recarga
            $fill       = $this->createFill();
            $wsdl = new ConsumerWS();
            $wsdl->setOwner(strtolower($fill->getBrand()->getBrand()));
            $wsdl->setNumber($fill->getNumber());
            $wsdl->setValue($fill->getValue());
            $wsdl->setTrace($fill->getId());
            $rta = $wsdl->send();
            if($rta['codigoRespuesta']=='00'){
                $fill->setFilled(1);
                $this->em->persist($fill);
                $this->em->flush();
                $status = true;
            }else{
                $status = false;
            }
            echo json_encode(array('status'=>$status));
	}
        public function principal2($type = "category" , $cid = 0, $conversion = false){
		$data = array();
		$data["title"] = AuthConstants::ML_INFONAME;
//                if( $this->session->userdata(AuthConstants::USER_ID)){
//                    if(! $this->session->userdata(AuthConstants::CART)){
//                        $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
//                    }
                    if($this->input->get('msg') !== false){
                        $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
                        $message = explode("--", $this->input->get('msg'));
                        $data["message"] = lang($message[0]).$message[1];                    
                    }
                    if($type == 'product'){
                        $this->loadRepository("Products");
                        $product = $this->Products->find($cid);
                        $productCat = $product->getCategory()->getId();
                        $data["productCat"] = $productCat;
                    }
                    $data["mainCategory"] = 1;
                    $data["trigger".$type] = $cid;
                    $data["conversion"] = $conversion;
                    $data["categories"] = $this->getCategories(1);
                    $data["inactiveCategories"] = $this->getCategories();
//                    echo "<pre>"; var_dump($data);exit;
                    $this->viewWebPublic('principal', $data);
//                }  else {
//                    redirect("looking/");
//                }
	}
        public function searchAdvisers(){
            $postData = $this->input->post('advisers');
            foreach ($postData as $key => $value) {
                $property[] = $value['id'];
                $range[] = $value['val'];
            }
            $this->loadRepository("CategoriesConfigs");
            foreach ($property as $key => $value) {
                $property[$key] = $this->CategoriesConfigs->find($value)->getName();
            }
//            echo json_encode($range);
//            echo json_encode($property);
            $output = $this->rest->get('browser/advisers/', array("range"=>  json_encode($range),
                                                                    "property"=>json_encode($property)
                                                                )
                                        );
                
                $data = array();
                $data["status"] = true;
                $data["results"] = array();

                $this->resultsProcess($output);
                $data["results"] = $output->data;
                
                echo json_encode($data);
            
            
        }
        public function resultsProcess(&$output, &$categories = null){
            $comparison = $this->session->userdata(AuthConstants::COMPARISON);
            if(!$comparison)$comparison=array();
            foreach ($output->data as $key => $value) {
                if($categories !== null)$categories[]= $value->category;
                $this->load->helper('favorites');
                $this->load->helper('offers');
                $obj = new Favorites();
                $off = new Offers();
                $value->favorite = $obj->isFavorite($value->id);
                $value->offer = $off->findOffer($value);
                $value->offerId = $off->findOfferId($value);
                $inComparison = false;
                foreach ($comparison as $index => $item) {
                    if ($value->id == $item['id']) {
                            $inComparison = true; 
                    }
                }
                $value->comparison = $inComparison;

            }
        }
        public function checkCart(){
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = count($cart);
            }else{
                $cart = 0;
            }
            echo  $cart;
	}
        public function outstanding($register = false){
            $output = $this->rest->get('browser/outstanding/', array("cat"=>  $this->input->post("cat")));
            $tmpl = file_get_contents("templates/ajax/results.php");
            $params = array();
            $categories = array();
            $params['*|comparison-icon-src|*'] = base_url("images/comparar-lateral.svg");
            foreach ($params as $key => $field) {
                $tmpl = str_replace($key, $field, $tmpl);
            }
            if(isset($output->status) && $output->status){
                $data = array();
                $this->resultsProcess($output, $categories);
                
                $this->loadRepository("Categories");
                $cats = $this->Categories->findBy(array('id'=>$categories));
                $listCategories = array();
                foreach ($cats as $value) {
                    $listCategories[] = array('id'=>$value->getId(),'option'=>$value->getCategory());
                }
                $output->categories = $listCategories;
                $output->tmpl = $tmpl;
//                echo json_encode($output);
            }  else {
//                echo json_encode(false);
            }
            $data["title"] = AuthConstants::ML_INFONAME;
            $data["leftboxTitle"] = "DESTACADOS";
            $data["leftboxDesc"] = "No te pierdas de nuestos destacados, seguro hay algo para ti.";
            $data["filters"] = array(array("id"=>"1","question"=>"Categorías encontradas","icon"=>"fa-filter","label"=>"Filtrar Resultados","options"=>$listCategories));
            $data["products"] = $output->data;
            $data["pixel"] = $register;
            $data["categories"] = $this->getCategories(1);
            $data["categoryName"] = lang("outstanding");
            $data["type"] = "outstanding";
            $data["categoryImage"] = "bannerphones.png";
            $data["color"] = "#ffffff";
            $data["cid"] = 99;
            $data["extra"] = "";
            $data['metas'] = $this->getMetas(AuthConstants::GN_OUTSTANDING, AuthConstants::GN_DESCRIPTION_OUTSTANDING);
//            echo "<pre>"; var_dump($data);exit;
            $this->viewFrontend2('results', $data);
        }
        public function category($cid,$name){
            $cat = $this->em->find('models\Categories', $cid);
            $output = $this->rest->get('browser/search/', array("cat"=>  $cid,"data"=>  json_encode(array())));
            $params = array();  
            
            if(isset($output->status) && $output->status){
                $data = array();
                $this->resultsProcess($output);
//                echo json_encode($output);
            }  else {
//                echo json_encode(false);
            }
            
            $data["help"] = 0;
            if(strpos( $name,"--help")){
                $data["help"] = 1;
            }
            $data["title"] = AuthConstants::GN_SITENAMESHORT . $cat->getCategory();
            $data["leftboxTitle"] = "¡RESPONDE Y ENCUENTRA!";
            $data["leftboxDesc"] = "Con estas simples preguntas podemos darte las mejores opciones.";
            $data["filters"] = $questions = $this->getQuestions($cid);
            $this->loadRepository("CategoriesConfigs");
            $this->loadRepository("Advisers");
            $data["configs"] = $this->CategoriesConfigs->findBy(array("category"=>$cid));
            $data["advisers"] = $this->Advisers->findBy(array("status"=>1,"category"=>$cid));
            $data["products"] = $output->data;
            $data["categories"] = $this->getCategories(1);
            $data["color"] = $cat->getColor();
            $data["cid"] = $cat->getId();
            $data["categoryName"] = $cat->getCategory();
            $data["categoryImage"] = $cat->getImage();
            $data["type"] = "category";
            $data["kvalue"] = AuthConstants::FN_AVAILABLE_FINANCIAL;
            $data["extra"] = "";
            $data["category"] = $cat->getId();
            $data['metas'] = $this->getMetas($cat->getCategory(), $cat->getMeta());
            
//            echo "<pre>"; var_dump($data);exit;
            $this->viewFrontend2('results', $data);
        }
        public function cart(){
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');

            $this->loadRepository("UsersData");
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $description_gifts = "";
            $subtotal = 0;
            if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $cart[$index]['name'] = $product->getProduct();
                        $cart[$index]['image'] = $product->getImage();
                        $description_gifts .=  $product->getProduct() . ", ";
                        $subtotal += $item['cost'];
                }
            }
            $data['cart'] = $cart;
            $data['description_gifts'] = substr($description_gifts, 0, (strlen($description_gifts) -2));
            $data['subtotal'] = $subtotal;
            $data['process'] = $this->getProccessCost($subtotal);
            $data['publishable_key'] = '123123123';
            
            $customer = $this->getCustomer($this->session->userdata(AuthConstants::EMAIL));

            $data['cid'] = $customer->getId();
            $data['name'] = $customer->getName();
            $data['last_name'] = $customer->getLastName();
            $data['dni'] = $customer->getDni();
            $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
            $data['docType'] = $docT;
            $data['email'] = $customer->getEmail();
            $data['mobile'] = $customer->getMobile();
            $data['phone'] = $customer->getPhone();
            $data['address'] = $customer->getAddress();
            $data['country'] = 1;
            $data['state'] = 0;
            $data['city'] = 0;
            $data['tmpl'] = file_get_contents("templates/ajax/cart.php");
            if($customer->getCity() !== NULL){
                $data['city'] = $customer->getCity()->getId();
                $data['state'] = $customer->getCity()->getState()->getId();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getId();
                
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            $data['metas'] = $this->getMetas(AuthConstants::GN_CART, AuthConstants::GN_DESCRIPTION_CART);
            
            $data['client_id'] = '654654654';
            $data['env'] = '987987987';
            $payU = $this->getPayUData();
            $params['*|merchant|*'] = $payU->merchant;
            $params['*|account|*'] = $payU->account;
            $params['*|urlPayU|*'] = $payU->url;
            $params['*|description|*'] = "LookingForWeb (".count($cart).") Producto(s)";
            $params['*|reference|*'] = "";
            $params['*|amount|*'] = $subtotal;
            $params['*|currency|*'] = "COP";
            $params['*|signature|*'] = "";
            $params['*|test|*'] = AuthConstants::PAYU_MODE;
            $params['*|buyerEmail|*'] = $customer->getEmail();
            $params['*|response|*'] = base_url().$payU->response;
            $params['*|confirmation|*'] = base_url().$payU->confirmation;
            $csrf = $this->config->config["csrf_token_name"];
            
            foreach ($params as $key => $field) {
                $data['tmpl'] = str_replace($key, $field, $data['tmpl']);

            }
            $data['csrf'] = $csrf;
            echo json_encode(array('status'=>true,'data'=>$data));
        }
        public function profile(){
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');

            $customer = $this->getCustomer($this->session->userdata(AuthConstants::EMAIL));

            $data['cid'] = $customer->getId();
            $data['name'] = $customer->getName();
            $data['last_name'] = $customer->getLastName();
            $data['dni'] = $customer->getDni();
            $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
            $data['docType'] = $docT;
            $data['email'] = $customer->getEmail();
            $data['mobile'] = $customer->getMobile();
            $data['phone'] = $customer->getPhone();
            $data['address'] = $customer->getAddress();
            $data['country'] = 0;
            $data['state'] = 0;
            $data['city'] = 0;
            $data['tmpl'] = file_get_contents("templates/ajax/profile.php");
            if($customer->getCity() !== NULL){
                $data['city'] = $customer->getCity()->getId();
                $data['state'] = $customer->getCity()->getState()->getId();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getId();
                
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            $data['metas'] = $this->getMetas("", "");
            $csrf = $this->config->config["csrf_token_name"];
            $data['csrf'] = $csrf;
            echo json_encode(array('status'=>true,'data'=>$data));
        }
        public function favorites(){
            $output = $this->rest->get('browser/favorites/', array("id"=>$this->session->userdata(AuthConstants::USER_ID),"cat"=>  $this->input->post("cat")));
            $params = array();
            $categories = array();
            $tmpl = file_get_contents("templates/ajax/results.php");
            $params['*|comparison-icon-src|*'] = base_url("images/comparar-lateral.svg");
            foreach ($params as $key => $field) {
                $tmpl = str_replace($key, $field, $tmpl);
            }
            $rows = array();
            $listCategories = array();
            if(isset($output->status) && $output->status){
                $data = array();
                $this->resultsProcess($output, $categories);
                $this->loadRepository("Categories");
                $cats = $this->Categories->findBy(array('id'=>$categories));
                foreach ($cats as $value) {
                    $listCategories[] = array('id'=>$value->getId(),'option'=>$value->getCategory());
                }
                $output->categories = $listCategories;
//                echo json_encode($output);
//            }  else {
//                echo json_encode(array('status'=>false,'tmpl'=>$tmpl));
            
                if(isset($output->data) ){
                    $rows = $output->data;
                }
            }
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            $data["leftboxTitle"] = "FAVORITOS";
            $data["leftboxDesc"] = "Estos son los equipos que has marcado como favoritos.";
            $data["filters"] = array(array("id"=>"1","question"=>"Categorías encontradas","icon"=>"fa-filter","label"=>"Filtrar Resultados","options"=>$listCategories));
            $data["products"] = $rows;
            $data["categories"] = $this->getCategories(1);
            $data["type"] = "favorites";
            $data["extra"] = "";
            $data['metas'] = $this->getMetas(AuthConstants::GN_FAVORITES, AuthConstants::GN_DESCRIPTION_FAVORITES);
//            echo "<pre>"; var_dump($data);exit;
            $this->viewFrontend('principal', $data);
        }
        public function addFavorites(){
            $this->loadRepository("Favorites");
            $exist = $this->Favorites->findBy(array('customer'=>$this->session->userdata(AuthConstants::USER_ID),'product'=>$this->input->post('id')));
            $output= new stdClass();
            $output->status = false;
            $sessFavorites = $this->session->userdata(AuthConstants::FAVORITES);
            if(count($exist)==0){
                $output = $this->rest->post('browser/favorite/', array("product"=>  $this->input->post('id'),'user'=>$this->session->userdata(AuthConstants::USER_ID)));
                array_push($sessFavorites, $this->input->post('id'));
                $this->session->set_userdata(AuthConstants::FAVORITES,      $sessFavorites);
            }
            if(isset($output->status) && $output->status){
                echo json_encode($output);
            }  else {
                echo json_encode(false);
            }
        }
        public function removeFavorites(){
            $this->loadRepository("Favorites");
            $exist = $this->Favorites->findBy(array('customer'=>$this->session->userdata(AuthConstants::USER_ID),'product'=>$this->input->post('id')));
            $output= new stdClass();
            $output->status = false;
            $sessFavorites = $this->session->userdata(AuthConstants::FAVORITES);
            if(count($exist)>0){
                $output = $this->rest->delete('browser/favorite', array("product"=>  $this->input->post('id'),'user'=>$this->session->userdata(AuthConstants::USER_ID)));
                $index = array_search($this->input->post('id'),$sessFavorites);
                unset($sessFavorites[$index]);
                $this->session->set_userdata(AuthConstants::FAVORITES,      $sessFavorites);
            }
            if(isset($output->status) && $output->status){
                echo json_encode($output);
            }  else {
                echo json_encode(false);
            }
        }
	public function start($loginError = false,$existUser = false){
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            if($loginError){
                $data["error"] = true;
            }
            if($existUser){
                $data["exist"] = true;
            }
            
            $data['loginUrl'] = $this->getFbUrl();
            echo $this->load->view('start', $data);
        }
	public function register($loginError = false,$existUser = false){
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            if($loginError){
                $data["error"] = true;
            }
            if($existUser){
                $data["exist"] = true;
            }
            
            $data['loginUrl'] = $this->getFbUrl();
            echo $this->load->view('register', $data);
        }
	public function persistRegister($loginError = false,$existUser = false){
            
            $intent = new models\CreditRequests();
            $this->loadRepository("Products");
            $product = $this->Products->find($this->input->post("product"));
            $intent->setProduct($product);
            $intent->setQuotes($this->input->post('quotes'));
            $intent->setInitialFee($this->input->post('first'));
            $intent->setAmount($this->input->post('amount'));
            $intent->setFeeValue($this->input->post('total'));
            $intent->setName($this->input->post('name'));
            $intent->setLastName($this->input->post('last_name'));
            $intent->setEmail($this->input->post('email'));
            $intent->setMobile($this->input->post('mobile'));
            $date = new DateTime();
            $intent->setDateRequest($date);
            $this->em->persist($intent);
            $this->em->flush();
            $this->load->helper('sendy');
            $sendy = new Sendy(AuthConstants::SDY_CREDITREQUEST);
            $sendy->subscribe($this->input->post('name')." ".$this->input->post('last_name'), $this->input->post('email'));
            
            $to[]= array('name'=>  AuthConstants::ML_EPMNAME,'mail'=>  AuthConstants::ML_INFOEMAIL);
            
            $params = array();
            $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
            $params['*|CLIENT|*'] = $this->input->post('name')." ". $this->input->post('last_name');
            $params['*|EMAIL|*'] = $this->input->post('email');
            $params['*|MOBILE|*'] = $this->input->post('mobile');
            $params['*|PRODUCT|*'] = $product->getProduct();
            $params['*|INITIAL_FEE|*'] = "$".number_format($this->input->post('first'));
            $params['*|QUOTES|*'] = $this->input->post('quotes');
            $params['*|TOTAL|*'] = "$".number_format($this->input->post('total'));
            $params['*|MC_PREVIEW_TEXT|*'] = "Hay una nueva solicitud de crédito, iniciemos el proceso";
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            $params2 = array();
            $params2['*|MC_PREVIEW_TEXT|*'] = "Has solicitado un nuevo crédito, por favor sigue las instrucciones";

            $to2[]= array('name'=>  $this->input->post('name'),'mail'=> $this->input->post('email'));

            $this->notify($to,AuthConstants::ML_CREDITREQUEST,"creditRequest",$params);
            $this->notify($to2,AuthConstants::ML_CREDITREQUEST,"creditRequestDocuments",$params2,array("facilidadesdepago.pdf","AutorizacionConsultaCentralesdeRiesgo.docx"));
            
            echo json_encode("ok");
        }
        public function getFbUrl(){
            require_once(  'vendor/facebook/graph-sdk/src/Facebook/autoload.php' );
            $fb = new \Facebook\Facebook([
                'app_id' => '103262576813038',
                'app_secret' => '47d238728ddb56b3d2192099d6912698',
                'default_graph_version' => 'v2.8',
//                'default_access_token' => 'EAABd6rE1oZB4BAKIzydSvOCelUDYiOAVdgcLJejjmeZBpA8tlVcZBlCpF7iqV5YQYppIfQx4ppJu2dHMQEoRuNJnC8m9JBfCB8ZBjQA8V6BW06hzVRgEC67ZBOqePS2jZAsksg9yVqpHP3YyFxb35xyT6LJZA9zllz66tR2BB0lARuPL8kqF5KYcxQUOukIVHwZD', // optional
              ]);

          // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
             $helper = $fb->getRedirectLoginHelper();
          //   $helper = $fb->getJavaScriptHelper();
          //   $helper = $fb->getCanvasHelper();
          //   $helper = $fb->getPageTabHelper();

          try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
//            $accessToken = $helper->getAccessToken();
//            $response = $fb->get('/me', $accessToken);
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }

//            $me = $response->getGraphUser();
//            $URL = $fb->get('loginUrl');
            $permissions = ['email']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(base_url("looking/people"), $permissions);
            return $loginUrl;
        }
	public function campaign($reference){
            $this->session->set_userdata(AuthConstants::REF_CAMPAIGN, $reference);
            $this->index();
        }
	public function login($loginError = false){
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            if($loginError){
                $data["error"] = true;
            }
            $data['loginUrl'] = $this->getFbUrl();
            echo $this->load->view('login', $data);
        }
	public function cupon(){
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            $data["cupon"] = AuthConstants::CUPON_REGISTER;
            echo $this->load->view('cupon', $data);
        }
	public function recoverPasswd(){
            $data = array();
            $data["title"] = AuthConstants::ML_INFONAME;
            echo $this->load->view('recover', $data);
        }
	public function people()
	{
//            $this->load->library('facebook/facebook');
//            $login_url = $this->facebook->login_url();
//            var_dump($login_url);
            require_once(  'vendor/facebook/graph-sdk/src/Facebook/autoload.php' );
            $fb = new \Facebook\Facebook([
                'app_id' => '103262576813038',
                'app_secret' => '47d238728ddb56b3d2192099d6912698',
                'default_graph_version' => 'v2.8',
                'default_access_token' => 'EAABd6rE1oZB4BAKIzydSvOCelUDYiOAVdgcLJejjmeZBpA8tlVcZBlCpF7iqV5YQYppIfQx4ppJu2dHMQEoRuNJnC8m9JBfCB8ZBjQA8V6BW06hzVRgEC67ZBOqePS2jZAsksg9yVqpHP3YyFxb35xyT6LJZA9zllz66tR2BB0lARuPL8kqF5KYcxQUOukIVHwZD', // optional
              ]);

            $helper = $fb->getRedirectLoginHelper();

            try {
              $accessToken = $helper->getAccessToken();
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
              // When Graph returns an error
              echo 'Graph returned an error: ' . $e->getMessage();
              exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
              // When validation fails or other local issues
              echo 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
            }

            if (! isset($accessToken)) {
              if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
              } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
              }
              exit;
            }

            // Logged in
//            echo '<h3>Access Token</h3>';
//            var_dump($accessToken->getValue());

            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $fb->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
//            echo '<h3>Metadata</h3>';
//            var_dump($tokenMetadata);

            // Validation (these will throw FacebookSDKException's when they fail)
//            $tokenMetadata->validateAppId(103262576813038); // Replace {app-id} with your app id
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();

            if (! $accessToken->isLongLived()) {
              // Exchanges a short-lived access token for a long-lived one
              try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
              } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
              }

//              echo '<h3>Long-lived</h3>';
//              var_dump($accessToken->getValue());
            }

            $_SESSION['fb_access_token'] = (string) $accessToken;
            $response = $fb->get('/me?locale=en_US&fields=name,email',$accessToken);
            $userNode = $response->getGraphUser();
            $userEmail = $userNode->getField('email');
            $userName = $userNode->getField('name');
            $userFbId = $userNode->getField('id');
            $this->loadRepository("Customers");
            $newRegister = false;
            $customer = $this->Customers->findOneBy(array('email' => $userEmail));
//            $user = $this->getCustomer($userEmail);
//            if($user->getName()=="")$user->setName ($userName);
//            $user->setFacebookId ($userFbId);
//            if($user->getPassword()=="")$user->setPassword (md5("facebookregister"));
//            $this->em->persist($user);
//            $this->em->flush();
            $this->load->library('../modules/login/controllers/login');
            if(count($customer)===0){
                $_POST["email"]=$userEmail;
                $_POST["name"]=$userName;
                $_POST["password"]= "facebookregister";
                $_POST["password_in_md5"]=0;
                $_POST["facebookId"]=$userFbId;
                $this->login->signin();
            }else{
                $_POST["email"]=$customer->getEmail();
                $_POST["name"]=$customer->getName();
                $_POST["password"]= $customer->getPassword();
                $_POST["password_in_md5"]=1;
                $customer->setFacebookId ($userFbId);
                $this->em->persist($customer);
                $this->em->flush();
                $this->login->authCustomer();
            }

	}
	public function bussiness()
	{
		$data = array();
		$data["title"] = "Browser";
		
		$this->viewPublic('bussiness', $data);
	}
	public function mobile()
	{
		$data = array();
		$data["title"] = "Browser";
		
		$this->viewPublic('mobile', $data);
	}
        
        
        public function browse() {
            
            $keyword = $_REQUEST['search-product'];
            
            $data = array();
            $categories = array();
            $results = array();
//            var_dump((json_encode(explode(" ", $keyword))));exit;
            $output = $this->rest->get('browser/directSearch/', 
                                        array("data"=>json_encode(explode(" ", $keyword))));
            if(isset($output->data)){
                $this->resultsProcess($output, $categories);
            }
            $results = array();
            $listCategories = array();
            if($output->status){
                $this->loadRepository("Categories");
                $cats = $this->Categories->findBy(array('id'=>$categories));
                foreach ($cats as $value) {
                    $listCategories[] = array('id'=>$value->getId(),'option'=>$value->getCategory());
                }
                $results = $output->data;
                
            }
            $keyword = str_replace("%20", " ", $keyword);
            $data["title"] = AuthConstants::ML_INFONAME;
            $data["leftboxTitle"] = "BUSCASTE PRODUCTOS RELACIONADOS CON '$keyword'";
            $data["leftboxDesc"] = "Estos son los resultados que mas se ajustan.";
            $data["filters"] = array(array("id"=>"1","question"=>"Categorías encontradas","icon"=>"fa-filter","label"=>"Filtrar Resultados","options"=>$listCategories));
            $data["products"] = $results;
            $data["categories"] = $this->getCategories(1);
            $data["categoryName"] = $keyword;
            $data["categoryImage"] = "bannerphones.png";
            $data["color"] = "#ffffff";
            $data["cid"] = 99;
            $data["type"] = "browse";
            $data["kvalue"] = AuthConstants::FN_AVAILABLE_FINANCIAL;
            $data['metas'] = $this->getMetas($keyword, "");
            $data["extra"] = $keyword;
//            echo "<pre>"; var_dump($data);exit;
            $this->viewFrontend2('results', $data);
        
        }
        public function brand($bid,$name) {
            $data = array();
            $categories = array();
            $output = $this->rest->get('browser/brand/', array("bid"=>$bid));
            if(isset($output->data)){
                $this->resultsProcess($output, $categories);
            }
            $this->loadRepository("Categories");
            $cats = $this->Categories->findBy(array('id'=>$categories));
            $listCategories = array();
            foreach ($cats as $value) {
                $listCategories[] = array('id'=>$value->getId(),'option'=>$value->getCategory());
            }
            $data["title"] = AuthConstants::ML_INFONAME;
            $data["leftboxTitle"] = $name;
            $data["leftboxDesc"] = "Estos son los resultados para la marca que buscas.";
            $data["filters"] = array(array("id"=>"1","question"=>"Categorías encontradas","icon"=>"fa-filter","label"=>"Filtrar Resultados","options"=>$listCategories));
            $data["products"] = $output->data;
            $data["categories"] = $this->getCategories(1);
            $data["categoryName"] = $name;
            $data["categoryImage"] = "bannerphones.png";
            $data["color"] = "#ffffff";
            $data["cid"] = 99;
            $data["type"] = "brand";
            $data["extra"] = $bid;
            $data["kvalue"] = AuthConstants::FN_AVAILABLE_FINANCIAL;
            $data['metas'] = $this->getMetas($name, "");
//            echo "<pre>"; var_dump($data);exit;
            $this->viewFrontend2('results', $data);
        
        }
        public function results() {
            $postData = $this->input->post('questions');
            $type = $this->input->post('type');
            $extra = $this->input->post('extra');
            $answerOpt = "";
            $price = "";
            $brand = "";
            $cat = "";
            if(!$postData){
                $postData = array();
            }
            if(count($postData) == 1 && $postData[0]["id"]=="filter"){
                $answerOpt = reset(($postData))["value"];
            }else{
                foreach ($postData as $idx => $question) {
                    $prefix = ($idx==0)?"":",";
                    $this->loadRepository("Questions");
                    $questionOuter = $this->Questions->find($question["id"])->getOuter();
                    $cat = $this->Questions->find($question["id"])->getCategory()->getId();
                    $this->loadRepository("Options");
                    if($questionOuter=="brand"){
                        $brand = $question["value"];
                    }else if($questionOuter=="price"){
                        $price = explode(",", $question["value"]);
                        $realPrice = array();
                        foreach ($price as $priceVal) {
                            $realPrice[] = $this->Options->find($priceVal)->getOption();
                        }
                        $price = $realPrice;
                    }else{
                        $typeQ = $this->Questions->find($question["id"])->getType();
                        if($typeQ == "slider"){
                            $questionVals = explode(",", $question["value"]);
                            $query = $this->Options->createQueryBuilder('p')
                                ->select( "p.id id" )
                                ->where("p.question = '".$question["id"]."'")
                                ->andWhere("p.id >= '".$questionVals[0]."'")
                                ->andWhere("p.id <= '".$questionVals[1]."'")
                                ->getQuery();
                             $options = $query->getResult();
                             $answerOpt2 = "";
                             foreach ($options as $idx => $optionId) {
                                $prefix2 = ($idx==0)?"":",";
                                $answerOpt2 .= $prefix2.$optionId["id"];
                             }
                             $answerOpt .= $prefix.$answerOpt2;
                        }else{
                            $answerOpt .= $prefix.$question["value"];
                        }
                    }
                }
            }
//            var_dump($answerOpt);
//            var_dump($cat);
//            var_dump($brand);
//            var_dump($price);
//            var_dump($type);
//            exit;
            
//            $customer = $this->getCustomer($this->input->post('email'));
            if($type == "category"){
                $finalData = ($answerOpt == "")?$answerOpt = array():explode(",",$answerOpt);
                $output = $this->rest->get('browser/search/', array("cat"=>$cat,
                                                                    "data"=>json_encode($finalData),
                                                                    "price"=>json_encode($price),
                                                                    "brand"=>$brand));
            }
            if($type == "outstanding"){
                $output = $this->rest->get('browser/outstanding/', array("cat"=>  $answerOpt));
            }
            if($type == "favorites"){
                $output = $this->rest->get('browser/favorites/', array("cat"=>  $answerOpt));
            }
            if($type == "brand"){
                $output = $this->rest->get('browser/brand/', array("cat"=>  $answerOpt,"bid" => $extra));
            }
            if($type == "browse"){
                var_dump(explode(" ", $extra));exit;
                $output = $this->rest->get('browser/directSearch/', array("data"=>json_encode(explode(" ", $extra)),"cat"=>  $answerOpt));
            }
//            $this->load->helper('audits');
//            $obj = new Audits();
//            $obj->addSearchAudit(json_encode($postData),$customer);
//            var_dump($questions);exit;
//            var_dump($output);
//            exit;
            
            $data = array();
            $data["status"] = true;
            $data["results"] = array();
            if(isset($output->status) && $output->status){
                $this->resultsProcess($output);
                $data["results"] = $output->data;
            }
            echo json_encode($data);
        }
        public function directResults() {
            $postData = $this->input->post('globalSearch');
//            var_dump($postData);exit;
            $option = $this->em->find('models\Options', $postData[0]);
//            $data[] = $this->input->post('navegacion');
//            $data[] = $this->input->post('app');
//            var_dump($data);exit;
            
            $tmpl = file_get_contents("templates/ajax/results.php");
            $params['*|comparison-icon-src|*'] = base_url("images/comparar-lateral.svg");
            foreach ($params as $key => $field) {
                $tmpl = str_replace($key, $field, $tmpl);
            }
            $customer = $this->getCustomer($this->input->post('email'));
            $output = $this->rest->get('browser/directSearch/', array("data"=>json_encode(explode(" ", $postData))));
//            $questions = $this->getQuestions($option->getQuestion()->getCategory()->getId());
//            var_dump($questions);exit;
            $data = array();
//            $data["title"] = $option->getQuestion()->getCategory()->getCategory();
            $data["title"] = $postData;
            $data["tmpl"] = $tmpl;
            $data["minutos"] = $this->input->post('minutos');
            $data["navegacion"] = $this->input->post('navegacion');
            $data["answers"] = array();
//            if (count($postData) >0) {
//                foreach ($postData as $return) {
//                    if($return>0){
//                        $selected = $this->em->find('models\Options', $return);
//                        $data["answers"][$selected->getQuestion()->getId()] = $return;
//                    }
//                }
//            }
            $data["status"] = true;
            $data["app"] = $this->input->post('app');
            $data["category"] = "Búsqueda Directa";
            $data["mainCategory"] = "";
            $this->session->set_userdata(AuthConstants::CART_OWNER, $this->input->post('email'));
//            $data["theme"] = "cat".$option->getQuestion()->getCategory()->getParent()->getId();
            $data["theme"] = "cat2";
            $data["questions"] = array();
            $data["results"] = array();
            if(isset($output->status) && $output->status){
                
                $data["results"] = $output->data;
            }
            echo json_encode($data);
        }
        
        public function compare($ids) {
            $comparison = explode("-",$ids);
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
            $detail = array();
            foreach ($comparison as $key => $value) {
                $prod = array();
                $product = $this->em->find('models\Products', $value);
                if($product)$response['status']=true;
                $prod['id']= $product->getId();
                $prod['name']= $product->getProduct();
                $prod['description'] = $product->getDescription();
                $prod['brand'] = $product->getBrand()->getBrand();
                $prod['image'] = $product->getImage();
                $prod['cost'] = $product->getCost();
                $this->load->helper('offers');
                $obj = new Offers();
                $offer = $obj->findOffer($product,1);
                $offerId = $obj->findOfferId($product,1);
                $prod['offer'] = $offer;
                $prod['offerId'] = $offerId;
                $prod['market_price'] = $product->getMarketPrice();
                $propertyArray = $this->Properties->findBy(array("product"=>$product->getid(),'show_property'=>1),array("property"=>"ASC"));
                $props = array();
                foreach ($propertyArray as $property) {
                    $propsDetails = array();
                    $propsDetails['property'] = $property->getProperty();
                    $propsDetails['label'] = lang(strtolower($property->getProperty()));
                    if($property->getProperty()== 'COSTO' || $property->getValue() == 999 || $property->getValue() == 9999 || lang(strtolower($property->getProperty()."_unit")) == "$"){
                        if(($property->getValue() == 9999 || $property->getValue() == 999) && $property->getProperty()!= 'COSTO' && lang(strtolower($property->getProperty()."_unit"))!=="$"){
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".  lang("unlimited");                        
                        }else{
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".$property->getValue();                        
                        }
                    }else{
                        $propsDetails['value'] = $property->getValue()." ".lang(strtolower($property->getProperty()."_unit"));                                                
                    }
                    $props[] = $propsDetails;
                }
                $detail[$product->getId()]= $props;
                $response['data'][$product->getId()] = $prod;
            }
            $propertyIdx = array();
            foreach ($detail as $id=> $single) {
                $productMtx = array();
                foreach ($single as $row) {
                    $propertyIdx[$row["property"]]= $row["label"];
                }
            }
            foreach ($propertyIdx as $name=> $label) {
                $features = array();
                foreach ($detail as $id=> $single) {
                    $val ="";
                    foreach ($single as $row) {
                        if( $name == $row["property"]){
                            $val =$row["value"];
                        }
                    }
                    $features[$id]=$val;
                }
                $propertyIdx[$name]=array("label"=>$label,"details"=>$features);
            }
            $data = array();
            $data['title']= "Comparación";
            $data['details']= $propertyIdx;
            $data['products'] = $response['data'];
            $data['metas'] = $this->getMetas("Comparación", "");
//            echo "<pre>";var_dump($propertyIdx);exit;
            $this->viewFrontend('comparison', $data);
        }
        
        public function properties() {
            $productId = $this->input->post('id');
            $this->loadRepository("Products");
            $this->loadRepository("Properties");
            $response = array('status'=>false,'data'=>array());
                $prod = array();
                $product = $this->Products->find($productId);
                $propertyArray = $this->Properties->findBy(array("product"=>$productId,'show_property'=>1),array("property"=>"ASC"));
                $props = array();
                $info = array();
                $info['id'] = $product->getId();
                $info['product'] = $product->getProduct();
                $info['cost'] = $product->getCost();
                $info['brand'] = $product->getBrand()->getBrand();
                $info['market_price'] = $product->getMarketPrice();
                $info['description'] = $product->getDescription();
                $info['image'] = $product->getImage();
                $info['method'] = $product->getPaymentMethod()->getId();
                $this->load->helper('favorites');
                $obj = new Favorites();

                $info['favorite'] = $obj->isFavorite($productId);

                $response['status']=true;
                foreach ($propertyArray as $property) {
                    $propsDetails = array();
                    $propsDetails['property'] = $property->getProperty();
                    $propsDetails['label'] = lang(strtolower($property->getProperty()));
                    if($property->getProperty()== 'COSTO' || $property->getValue() == 999 || $property->getValue() == 9999 || lang(strtolower($property->getProperty()."_unit")) == "$"){
                        if(($property->getValue() == 9999 || $property->getValue() == 999) && $property->getProperty()!= 'COSTO' && lang(strtolower($property->getProperty()."_unit"))!=="$"){
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".  lang("unlimited");                        
                        }else{
                            $propsDetails['value'] = lang(strtolower($property->getProperty()."_unit"))." ".$property->getValue();                        
                        }
                    }else{
                        $propsDetails['value'] = $property->getValue()." ".lang(strtolower($property->getProperty()."_unit"));                                                
                    }
                    $props[] = $propsDetails;
                }
                $prod['details']= $props;
                $response['data']['info'] = $info;
                $response['data']['details'] = $props;
            echo json_encode($response);
        }
        public function cartInfo() {
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $description_gifts = "";
            $subtotal = 0;
            $status = true;
            if(is_array($cart) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $cart[$index]['name'] = $product->getProduct();
                        $cart[$index]['cost'] = $product->getCost();
                        $cart[$index]['totalCost'] = $item['cost'];
                        $subtotal += $item['cost'];
                }
            }
            $data = array('cart'=>$cart,'total'=>$subtotal);
            $response = array('status'=>$status,'data'=>$data);
            echo json_encode($response);
        }
	public function addComparison(){
            $data = $this->input->post();
            $id = $this->input->post('id');
            $name = $this->input->post('name');
            $src = $this->input->post('src');
            $stars = $this->input->post('stars');
            
            $quotes = $this->input->post('quotes');
            $cost = $this->input->post('cost');
            $offer = $this->input->post('offer');
            $available = $this->input->post('available');
            $comparison = $this->session->userdata(AuthConstants::COMPARISON);
            if(!$comparison){
                $comparison = array();
            }
            $return = "error";
            $insert = true;
            if (count($comparison) < 4) {
                foreach ($comparison as $index => $item) {
                    if ($id == $item['id']) {
                        $insert = false;
                    }
                }
                if($insert){
                    array_push($comparison, array('id' => $id,'name' => $name,'src' => $src,'stars' => $stars));
                    $return = "added";
                } 
            } 
            $this->session->set_userdata(AuthConstants::COMPARISON, $comparison);
            $json = array();
            if ($return == "added") {
                $json['status'] = true;
                $json['comparison'] = $comparison;
            }else {
                $json['status'] = false;
                $json['comparison'] = "error";
            }
            echo json_encode($json);
        }
        public function removeComparison() {
            $id = $this->input->post('id');
            $comparison = $this->session->userdata(AuthConstants::COMPARISON);
            $return = "error";
            if ($id) {
                if (count($comparison) > 0) {
                    foreach ($comparison as $index => $item) {
                        if ($id == $item['id']) {

                                unset($comparison[$index]);
                                $return = "added";
                        }
                    }
                }
            }
            $this->session->set_userdata(AuthConstants::COMPARISON, array_values($comparison));
            if ($return == "added") {
                $json['status'] = true;
                $json['comparison'] = array_values($comparison);
            }else {
                $json['status'] = false;
                $json['comparison'] = "error";
            }
            echo json_encode($json);
	}
        public function getComparison() {
            $comparison = $this->session->userdata(AuthConstants::COMPARISON);
            if(!$comparison)$comparison = array();
            $json['status'] = true;
            $json['comparison'] = array_values($comparison);
            echo json_encode($json);
	}
	public function addItem($noresponse = false){
            $data = $this->input->post();
////            var_dump($data);
//            $val = $this->session->userdata($data['type']);
//            $val[] = $data["id"];
//            $this->session->set_userdata(array($data['type'] => $val));
//            $val = $this->session->userdata($data['type']);
////            var_dump($val);
//            $data = array();
//            $data["title"] = "Results";
//            $data["category"] = "Móvil";
//            
            $id = $this->input->post('id');
            
            $quotes = $this->input->post('quotes');
            $cost = $this->input->post('cost');
            $offer = $this->input->post('offer');
            $available = $this->input->post('available');
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!$cart){
                $cart = array();
            }
            $return = "error";
            if ($id && $cost) {
                    if (count($cart) > 0) {
                            $insert = true;
                            foreach ($cart as $index => $item) {
                                    if ($id == $item['id']) {
//                                        if($cart[$index]['quotes']>=$available){
//                                            $insert = false;
//                                            $return = "unavailable";
//                                        }else{
                                            $cart[$index]['quotes'] = $cart[$index]['quotes'] + $quotes;
                                            $cart[$index]['cost'] = $cart[$index]['cost'] + $cost;
                                            $return = "added";
                                            $insert = false;
//                                        }
                                    }
                            }
                            if ($insert) {
                                    array_push($cart, array('id' => $id, 'cost' => $cost, 'quotes' => $quotes, 'offer' => $offer, 'available' => $available));
                                    $return = "added";
                            }
                    } else {
                            array_push($cart, array('id' => $id,  'cost' => $cost, 'quotes' => $quotes, 'offer' => $offer, 'available' => $available));
                            $return = "added";
                    }
            }
            $this->session->set_userdata(AuthConstants::CART, $cart);
            $json = array();
            if ($return == "added") {
                $json['status'] = true;
                $json['data'] = count($this->session->userdata(AuthConstants::CART));
            }else if($return == "unavailable"){
                $json['status'] = false;
                $json['data'] = "error";
            }
            if($noresponse== false){
                echo json_encode($json);
            }
        }
        public function editCart() {
		$gift = $this->input->post('id');
		$quotes = $this->input->post('quotes');
		$amount = $this->input->post('cost');

		$cart = $this->session->userdata(AuthConstants::CART);
		$return = "error";
		$giftValue = "";
		$subtotal = 0;
		$process = "";
		$total = 0;
		if ($gift && $quotes && $amount) {
			if (count($cart) > 0) {
				foreach ($cart as $index => $item) {
					if ($gift == $item['id']) {
						$limit = false;
//						$giftObj = $this->em->find('models\Products', $gift);
//						$query = $this->em->createQuery("SELECT sum(c.fees) as fees, sum(c.value) as total FROM models\Contributions c WHERE c.gift = " . $gift);
//						$currentFees = $query->getResult();
//
//						$currentFees = $currentFees[0]['fees'];
//						$totalFees = $giftObj->getNumFees();
//						$available = $totalFees - $currentFees;
//						if ($quotes > $available) {
//							$quotes = $available;
//							$limit = true;
//						}

						$cart[$index]['quotes'] = $quotes;
						$giftValue = $quotes * $amount;
						$cart[$index]['cost'] = $giftValue;
						$return = ($limit) ? $quotes : "added";
					}
					$subtotal = $subtotal + $cart[$index]['cost'];
				}
			}
		}
		$this->session->set_userdata(AuthConstants::CART, $cart);
		$process = number_format($this->getProccessCost($subtotal),0);
		$total = number_format(($subtotal + $this->getProccessCost($subtotal)), 0);
		$giftValue = number_format($giftValue, 0);
		$subtotal = number_format($subtotal, 0);
		echo json_encode(array("response" => $return, "gift" => $giftValue, "subtotal" => $subtotal, "process" => $process, "total" => $total));
	}

	public function deleteCart() {
		$gift = $this->input->post('id');
		$cart = $this->session->userdata(AuthConstants::CART);
		$return = "error";
		$giftValue = "";
		$subtotal = 0;
		$process = 0;
		$total = 0;
		if ($gift) {
                    if (count($cart) > 0) {
                        foreach ($cart as $index => $item) {
                            if ($gift == $item['id']) {

                                    unset($cart[$index]);
                                    $return = "added";
                            }
                            if (isset($cart[$index]))
                                    $subtotal = $subtotal + $cart[$index]['cost'];
                        }
                    }
		}
		$this->session->set_userdata(AuthConstants::CART, $cart);
		$process = number_format($this->getProccessCost($subtotal),0);
		$total = number_format(($subtotal + $this->getProccessCost($subtotal)), 0);
		$subtotal = number_format($subtotal, 0);
		echo json_encode(array("response" => $return, "subtotal" => $subtotal, "process" => $process, "total" => $total, "cartCount" => count($this->session->userdata(AuthConstants::CART))));
	}

	public function linkPayment($reference)
	{
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');
            $orderId = ltrim(ltrim($reference, "O"),"0");
            
            $order = $this->em->find('models\Orders', $orderId);
            if($order->getStatus()!==4 && $order->getResponsePol()!==1){
                $customer = $this->getCustomer($order->getCustomer()->getEmail());
    //            if($customer){
    //                $order      = $this->generateOrder($customer);
    //                if($order){
    //                    $fill       = $this->createFill($order);
    //                }
    //            }
                $newreference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
                $newreference = $newreference.time();
                $order->setOrder($newreference);
                $this->em->persist($order);
                $this->em->flush();
                $description_gifts = "";
                $subtotal = 0;
                $cart = array();
                $cart[0]=array();
                $cart[0]['id'] =  1;
                $cart[0]['name'] = AuthConstants::PAYU_RESEND.$reference;
                $cart[0]['image'] = "payu.jpg";
                $cart[0]['cost'] = $order->getValue();
                $cart[0]['brand'] = "";
                $cart[0]['number'] = "";
                $cart[0]['quotes'] = 1;
                $subtotal += $cart[0]['cost'];
    //            $reference = "O".str_pad($order->getId(), 5, "-=", STR_PAD_LEFT);
                $data['fill'] = true;
                $data['directPayment'] = true;
                $data['cid'] = $customer->getId();
                $data['name'] = $customer->getName();
                $data['last_name'] = $customer->getLastName();
                $data['dni'] = $customer->getDni();
                $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
                $data['docType'] = $docT;
                $data['email'] = $customer->getEmail();
                $data['mobile'] = $customer->getMobile();
                $data['phone'] = $customer->getPhone();
                $data['address'] = $customer->getAddress();
                $data['country'] = 1;
                $data['state'] = 0;
                $data['city'] = 0;
                if($customer->getCity() !== NULL){
                    $data['city'] = $customer->getCity()->getId();
                    $data['state'] = $customer->getCity()->getState()->getId();
                    $data['country'] = $customer->getCity()->getState()->getCountry()->getId();

                }
                $data['docTypes'] = $this->getDocTypes();
                $data['cart'] = $cart;
                $data['subtotal'] = $subtotal;
                $data['process'] = $this->getProccessCost($subtotal);
                $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
                $data['merchant'] = $payU->merchant;
                $data['account'] = $payU->account;
                $data['urlPayU'] = $payU->url;
                $data['description'] = "Valida pagos";
                $data['reference'] = $newreference;
                $data['amount'] = $subtotal;
                $data['currency'] = "COP";
                $data['signature'] = md5($payU->key."~".$payU->merchant."~".$newreference."~".$subtotal."~COP");
                $data['test'] = AuthConstants::PAYU_MODE;
                $data['buyerEmail'] = $customer->getEmail();
                $data['response'] = base_url().$payU->response.$newreference;
                $data['confirmation'] = base_url().$payU->confirmation.$newreference;
                $csrf = $this->config->config["csrf_token_name"];
                $data['csrf'] = $csrf;
    //            echo "<pre>"; var_dump($data);exit;
                $this->viewFrontend('checkout', $data, $actions);
            }else{
                redirect("looking/?msg=tx_complete--".$reference);
            }
	}
	public function checkout()
	{
//		$data = array();
//		$data["title"] = "Checkout";
//		
//		$this->viewPublic('checkout', $data);
            $code = $this->input->post("code");
            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');
            $this->session->set_userdata("VALID_CODE",false);
            $this->session->set_userdata("REDEMED_CODE",false);
            $this->loadRepository("UsersData");
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $description_gifts = "";
            $subtotal = 0;
            $codeRedemed = 0;
            if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $cart[$index]['name'] = $product->getProduct();
                        $cart[$index]['image'] = $product->getImage();
                        $description_gifts .=  $product->getProduct() . ", ";
                        if($code !== FALSE && $item['offer']==0){
                            $this->load->helper('offers');
                            $obj = new Offers();
                            $newPrice = $obj->validateCode($code,$product);                            
                            if($newPrice>0){
                                $cart[$index]['cost'] = $newPrice;
                                $subtotal += $newPrice;
                                $codeRedemed = 1;
                                $codeId = $obj->getCodeId($code);
                                $this->session->set_userdata("VALID_CODE",$codeId);
                                $this->session->set_userdata("REDEMED_CODE",$code);
                            }else if($newPrice < 0){
                                $codeRedemed = -1;
                            }
                        }else{
                            $subtotal += $item['cost'];
                        }
                }
            }
            $data['cart'] = $cart;
            $data['isCode'] = ($code !== FALSE);
            $data['codeRedemed'] = $codeRedemed;
            $data['description_gifts'] = substr($description_gifts, 0, (strlen($description_gifts) -2));
            $data['subtotal'] = $subtotal;
            $data['process'] = $this->getProccessCost($subtotal);
            $data['publishable_key'] = '123123123';
            
            $data['cid'] = "";
            $data['name'] = "";
            $data['last_name'] = "";
            $data['dni'] = "";
            $data['docType'] = "";
            $data['email'] = "";
            $data['mobile'] = "";
            $data['phone'] = "";
            $data['address'] = "";
            $data['country'] = 1;
            $data['state'] = 0;
            $data['city'] = 0;
            if($this->session->userdata(AuthConstants::USER_ID)){
                $customer = $this->getCustomer($this->session->userdata(AuthConstants::EMAIL));

                $data['cid'] = $customer->getId();
                $data['name'] = $customer->getName();
                $data['last_name'] = $customer->getLastName();
                $data['dni'] = $customer->getDni();
                $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
                $data['docType'] = $docT;
                $data['email'] = $customer->getEmail();
                $data['mobile'] = $customer->getMobile();
                $data['phone'] = $customer->getPhone();
                $data['address'] = $customer->getAddress();
                if($customer->getCity() !== NULL){
                    $data['city'] = $customer->getCity()->getId();
                    $data['state'] = $customer->getCity()->getState()->getId();
                    $data['country'] = $customer->getCity()->getState()->getCountry()->getId();

                }
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            
            
            $data['client_id'] = '654654654';
            $data['env'] = '987987987';
            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $data['merchant'] = $payU->merchant;
            $data['account'] = $payU->account;
            $data['urlPayU'] = $payU->url;
            $data['epaykey'] = AuthConstants::EPAY_KEY;
            if($this->config->item("test_enviroment")==true){
                $data['epaytest'] = 'true';
            }else{
                $data['epaytest'] = 'false';
            }
//            $data['description'] = "LookingForWeb (".count($cart).") Producto(s)";
            $data['description'] = substr($description_gifts, 0, (strlen($description_gifts) -2));
            $data['reference'] = "";
            $data['amount'] = $subtotal;
            $data['currency'] = "COP";
            $data['signature'] = "";
            $data['test'] = AuthConstants::PAYU_MODE;
            $data['buyerEmail'] = "";
            $data['response'] = base_url().$payU->response;
            $data['confirmation'] = base_url().$payU->confirmation;
            $csrf = $this->config->config["csrf_token_name"];
            $data['csrf'] = $csrf;
            $this->viewFrontend2('checkout', $data, $actions);

	}
	public function fillCheckout(){

            $data = array();
            $actions = array();
            $data['title'] = lang('checkout');
            $questions = $this->input->post('questions');
//            $this->loadRepository("Options");
            foreach ($questions as $option) {
                $aOption = $this->em->find('models\Options', $option);
                if($aOption !== null){
                    if($aOption->getQuestion()->getId() == 10){
                        $_POST['operador']= $aOption->getOption();
                    }else{
                        $_POST['valor']= $aOption->getOption();
                    }
                }else{
                    $_POST['numero']= $option;
                }
            }
//            var_dump($_POST);exit;
//            var_dump($questions);exit;
            $customer = $this->getCustomer($this->input->post('email'));
//            if($customer){
//                $order      = $this->generateOrder($customer);
//                if($order){
//                    $fill       = $this->createFill($order);
//                }
//            }
            $description_gifts = "";
            $subtotal = 0;
            $cart = array();
            $cart[0]=array();
            $cart[0]['id'] =  1;
            $cart[0]['name'] =  $this->input->post('operador')." Recargas";
            $cart[0]['image'] = $this->input->post('operador').".png";
            $cart[0]['brand'] = $this->input->post('operador');
            $cart[0]['number'] = $this->input->post('numero');
            $cart[0]['cost'] = $this->input->post('valor');
            $cart[0]['quotes'] = 1;
            $subtotal += $cart[0]['cost'];
//            $reference = "O".str_pad($order->getId(), 5, "-=", STR_PAD_LEFT);
            $data['fill'] = true;
            $data['cid'] = $customer->getId();
            $data['name'] = $customer->getName();
            $data['last_name'] = $customer->getLastName();
            $data['dni'] = $customer->getDni();
            $docT = ($customer->getDocType() !== null )? $customer->getDocType()->getId():"";
            $data['docType'] = $docT;
            $data['email'] = $customer->getEmail();
            $data['mobile'] = $customer->getMobile();
            $data['phone'] = $customer->getPhone();
            $data['address'] = $customer->getAddress();
            $data['country'] = 0;
            $data['state'] = 0;
            $data['city'] = 0;
            if($customer->getCity() !== NULL){
                $data['city'] = $customer->getCity()->getId();
                $data['state'] = $customer->getCity()->getState()->getId();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getId();
                
            }
            $data['countries'] = $this->getCountries();
            $data['docTypes'] = $this->getDocTypes();
            $data['cart'] = $cart;
            $data['subtotal'] = $subtotal;
            $data['process'] = $this->getProccessCost($subtotal);
            $payU = $this->getPayUData();
            $data['merchant'] = $payU->merchant;
            $data['account'] = $payU->account;
            $data['urlPayU'] = $payU->url;
            $data['description'] = $this->input->post('operador')." Recargas";;
            $data['reference'] = "";
            $data['amount'] = $subtotal;
            $data['currency'] = "COP";
            $data['signature'] = "";
            $data['test'] = AuthConstants::PAYU_MODE;
            $data['buyerEmail'] = $customer->getEmail();
            $data['response'] = base_url().$payU->response;
            $data['confirmation'] = base_url().$payU->confirmation;
            $csrf = $this->config->config["csrf_token_name"];
            $data['csrf'] = $csrf;
//            echo "<pre>"; var_dump($data);exit;
            $this->viewPublic('checkout', $data, $actions);
        }
        
        private function getProccessCost($amount){
            if($amount<=AuthConstants::PAYU_PROCCESSLIMIT){
                $process = AuthConstants::PAYU_PROCCESSMINCOST;
            }else{
                $process = $amount*AuthConstants::PAYU_PROCCESSPERCENTAGE;
            }
            $iva = $process * AuthConstants::PAYU_PROCCESSIVA;
            $fte = $amount * AuthConstants::PAYU_PROCCESSRETEFTE;
            $ica = $amount * AuthConstants::PAYU_PROCCESSRETEICA;
            return 0;
//            return ceil($process + $iva + $fte + $ica);
        }
        
	public function orderConfirmation(){
            $response = array("status"=>false,"data"=>"");
            $customer = $this->updateCustomer();
            $shipping = $this->input->post("shipping");
            if($customer){
                $cart = $this->session->userdata(AuthConstants::CART);
                if(!isset($cart)|| $cart == FALSE){
                    $cart = array();
                }
                $payable = $unpayable = 0;
                $amount = array();
                $payuamount = 0;
                if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                    foreach ($cart as $index => $item) {
                            $product = $this->em->find('models\Products', $item);
                            $pm = $product->getPaymentMethod()->getId();
                            if(! isset($amount[$pm]))$amount[$pm] = 0;
                            $this->load->helper('offers');
                            $obj = new Offers();
                            $offer = $obj->findOffer($product,1);
                            $offerId = $obj->findOfferId($product,1);
                            $realCost = ($offer>0)?$offer:$product->getCost();
                            if($this->session->userdata("VALID_CODE")  && $offer ==0){
                                $cuponPrice = $obj->validateCode($this->session->userdata("REDEMED_CODE"), $product);
                                $realCost = ($cuponPrice>0)?$cuponPrice:$realCost;
                            }
                            $amount[$pm] += ($realCost * $item["quotes"]);
                            if($pm == AuthConstants::PM_USE_PAYU){
                                $payuamount += ($realCost * $item["quotes"]);
                            }
                    }
                }
                foreach ($amount as $method => $value) {
                    $order = $this->generateOrder($customer, $value, $method);
                    if(isset($order)){
                        $reference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
                        $reference = $reference.time();
                        if($offerId>0 && $method==1){
                            $offerObj = $this->em->find('models\Offers',$offerId);
                            $order->setOffer($offerObj);
                        }
                        if($this->session->userdata("VALID_CODE") && $method==1){
                            $cupon = $this->em->find('models\Cupons',$this->session->userdata("VALID_CODE"));
                            $order->setCupon($cupon);
                        }
                        $order->setOrder($reference);
                        $this->em->persist($order);
                        $this->em->flush();
                    }
                    if(! $this->input->post('brand')){
                        $addItems = $this->assocItems($order,$method);
                        
                    }else{
                        $fill       = $this->createFill($order);
                    }
                }
                
                $payuamount = $payuamount + $shipping;
                $payU = $this->getPayUData();
//                    var_dump($payU->key."~".$payU->merchant."~".$reference."~".$this->input->post('total')."~COP");
                $data['amount'] = $amount;
//                $data['amount_manual'] = $unpayable;
                $data['reference'] = $reference;
                $data['address'] = $customer->getAddress();
                $data['city'] = $customer->getCity()->getName();
                $data['email'] = $customer->getEmail();
                $data['country'] = $customer->getCity()->getState()->getCountry()->getCode();
                $data['signature'] = md5($payU->key."~".$payU->merchant."~".$reference."~".$payuamount."~COP");
                $data['response'] = base_url().$payU->response.$reference;
                $data['confirmation'] = base_url().$payU->confirmation.$reference;
                $response['status']=true;
                $response['data']=$data;
            }
            echo json_encode($response);
        }
	public function apiRegister(){
            $response = $this->addItem(true);
            $customer = $this->orderConfirmation();
            return $customer;
        }
	public function assocItems($order,$method){
            $cart = $this->session->userdata(AuthConstants::CART);
            if(is_array($cart)){
                foreach ($cart as $item){
                    $product = $this->em->find('models\Products',$item['id']);
                    $paymentMethod = $product->getPaymentMethod()->getId();
                    if($method == $paymentMethod){
                        $productItem = new models\ProductsOrders();
                        $productItem->setOrder($order);
                        $productItem->setProduct($product);
                        $productItem->setQuantity($item['quotes']);
                        if($item['available']!== 1){
                            $productItem->setDetail($item['available']);
                            
                        }
                        $this->load->helper('offers');
                        $obj = new Offers();
                        $offer = $obj->findOffer($product,1);
                        
                        $realCost = ($offer>0)?$offer:$product->getCost();
                        if($this->session->userdata("VALID_CODE") && $offer ==0){
                            $cuponPrice = $obj->validateCode($this->session->userdata("REDEMED_CODE"), $product);
                            $realCost = ($cuponPrice>0)?$cuponPrice:$realCost;
                        }
                        $productItem->setValue($realCost);
                        $productItem->setTax($product->getTax());
                        $this->em->persist($productItem);
                        $this->em->flush();
                        if($method==AuthConstants::PM_GENERATE_CODE){
                            $code = new models\ProductsOrdersCodes();
                            $code->setProductOrder($productItem);
                            $code->setCode($productItem->getId().substr(md5(rand(1,999999)),0,6));
                            $code->setRedeemed(0);
                            $code->setRedeemedDate(new DateTime(date("Y-m-d H:i:s")));
                            $code->setValue($product->getCost());
                            $this->em->persist($code);
                            $this->em->flush();
                        }
                    }
                }
            }
        }
	public function createFill($order = null){
            $fill = new models\MobileRefills();
            if($order!==null){
                $fill->setOrder($order);
            }
            $payment = $this->em->find('models\PaymentMethods', 1);
            $fill->setFilled(0);
            $this->loadRepository("Brands");
            $brand = $this->Brands->findOneBy(array('brand' => $this->input->post('brand')));
            $fill->setBrand($brand);
            $fill->setValue($this->input->post('total'));
            $fill->setUser($this->input->post('user'));
//            $navegador = get_browser(null, true);
//            $fill->setLocation(json_encode($navegador));
            $fill->setNumber($this->input->post('number'));
            $fill->setRequestDate(new DateTime());
            $this->em->persist($fill);
            $this->em->flush();
            return $fill;
        }
	public function generateOrder($customer,$value,$method){
            $order = new models\Orders();
            $order->setCustomer($customer);
            $payment = $this->em->find('models\PaymentMethods', $method);
            $status = $this->em->find('models\OrdersStatus', 1);
            $order->setPaymentMethod($payment);
            $order->setOrderDate(new DateTime(date("Y-m-d H:i:s")));
            $order->setReference("");
//            $order->setOrder("");
            $order->setInvoice(0);
            $order->setStatus($status);
            $order->setShipping($this->input->post("shipping_type"));
            $order->setValue($value);
            $this->em->persist($order);
            $this->em->flush();
            return $order;
        }
	public function getCustomer($email){
            $this->loadRepository("Customers");
            $customer = $this->Customers->findBy(array('email' => $email));
            if(count($customer)===0){
                $customer = new models\Customers();
                $customer->setEmail($email);
//                $city = $this->em->find('models\Cities', 1);
//                $customer->setCity($city);
                $customer->setCreationDate(new DateTime(date("Y-m-d")));
                $this->em->persist($customer);
                $this->em->flush();
//                $id = $user->getId();
            }else{
                $customer = $customer[0];
            }
            return $customer;
        }
	public function persistProfile(){
            $customer = $this->updateCustomer();
            if($customer->getId()>0){
                $this->session->set_userdata(AuthConstants::NAMES,          $this->input->post('name'));
                $this->session->set_userdata(AuthConstants::LAST_NAMES,     $this->input->post('last_name'));
                echo json_encode(array('status'=>true));
            }else{
                echo json_encode(array('status'=>false));                
            }
        }
	public function updateCustomer(){
//            $this->loadRepository("Customers");
//            $customer = $this->Customers->find(array('email' => $this->input->post('email')));
            if($this->input->post('cid')>0 || $this->input->post('cid') !=""){
                $customer = $this->em->find('models\Customers', $this->input->post('cid'));
            }else{
                $customer = $this->getCustomer($this->input->post('email'));
            }
            $customer->setName($this->input->post('name'));
            $customer->setLastName($this->input->post('last_name'));
            $customer->setEmail($this->input->post('email'));
            $customer->setAddress($this->input->post('address'));
            $city = $this->em->find('models\Cities', $this->input->post('city'));
            if($city !== null)$customer->setCity($city);
            $type = $this->em->find('models\DocumentTypes', $this->input->post('docType'));
            if($type !== null)$customer->setDocType($type);
            $campaign = $this->em->find('models\Campaigns', $this->input->post('campaign'));
            if($campaign !== null)$customer->setCampaign($campaign);
            $customer->setDni($this->input->post('dni'));
            $customer->setMobile($this->input->post('mobile'));
            $customer->setPhone($this->input->post('phone'));
            $customer->setLastAccess(new DateTime(date("Y-m-d H:i:s")));
            $this->em->persist($customer);
            $this->em->flush();
//                $id = $user->getId();
            return $customer;
        }
	public function getPayUData(){
            $payU = new stdClass();
            if($this->config->item("test_enviroment")==true){
                $payU->url = AuthConstants::PAYU_URL_S;
                $payU->merchant = AuthConstants::PAYU_MERCHANT_S;
                $payU->account = AuthConstants::PAYU_ACOUNT_S;
                $payU->key = AuthConstants::PAYU_KEY_S;
            }else{
                $payU->url = AuthConstants::PAYU_URL;
                $payU->merchant = AuthConstants::PAYU_MERCHANT;
                $payU->account = AuthConstants::PAYU_ACOUNT;
                $payU->key = AuthConstants::PAYU_KEY;
                
            }
            $payU->response = AuthConstants::PAYU_RESPONSE;
            $payU->confirmation = AuthConstants::PAYU_CONFIRMATION;
            return $payU;
        }
        
        public function epayco($reference){
            // $data = json_decode(file_get_contents('https://secure.epayco.co/validation/v1/reference/'.$_REQUEST['ref_payco']));
            $data = json_decode(file_get_contents('https://secure.payco.co/restpagos/transaction/response.json?ref_payco='.$_REQUEST['ref_payco']."&public_key=". AuthConstants::EPAY_KEY));
            if($data->success){

                // 1	Aceptada
                // 2	Rechazada
                // 3	Pendiente
                // 4	Fallida
                // 6	Reversada
                // 7	Retenida
                // 8	Iniciada
                // 9	Exprirada
                // 10	Abandonada
                // 11	Cancelada
                // 12	Antifraude
                
                $trstatus = array();
                $trstatus["tx_1"] = 4; //Approved
                $trstatus["tx_2"] = 6; //Declined
                $trstatus["tx_9"] = 5; //Expired
                $trstatus["tx_3"] = 7; //Pending
                $trstatus["tx_4"] = 6; //
                $trstatus["tx_6"] = 6; //
                $trstatus["tx_7"] = 6; //
                $trstatus["tx_8"] = 7; //
                $trstatus["tx_10"] = 6; //
                $trstatus["tx_11"] = 6; //
                $trstatus["tx_12"] = 6; //
                
                $pol = ($data->data->x_cod_response == 1)?1:4;
                
                $_REQUEST['buyerEmail'] = $data->data->x_customer_email;
                $_REQUEST['TX_VALUE'] = $data->data->x_amount;
                $_REQUEST['referenceCode'] = $data->data->x_id_invoice;
                $_REQUEST['reference_pol'] = $data->data->x_transaction_id;
                $_REQUEST['transactionState'] = $trstatus["tx_".$data->data->x_cod_response];
                $_REQUEST['polResponseCode'] = $pol;
                
                $_REQUEST['x_ref_payco'] = $data->data->x_ref_payco;
                $_REQUEST['x_description'] = $data->data->x_description;
                $_REQUEST['x_transaction_date'] = $data->data->x_transaction_date;
                $_REQUEST['x_transaction_id'] = $data->data->x_transaction_id;
                $_REQUEST['x_response'] = $data->data->x_response;
                $_REQUEST['x_response_reason_text'] = $data->data->x_response_reason_text;
                $_REQUEST['x_cod_response'] = $data->data->x_cod_response;
                $_REQUEST['x_bank_name '] = $data->data->x_bank_name ;
                $_REQUEST['x_id_invoice'] = $data->data->x_id_invoice;
                $_REQUEST['x_franchise'] = $data->data->x_franchise;
                $_REQUEST['x_amount_ok'] = $data->data->x_amount_ok;
                $_REQUEST['x_customer_name'] = $data->data->x_customer_name;
                $_REQUEST['x_customer_lastname'] = $data->data->x_customer_lastname;
                $_REQUEST['x_customer_country'] = $data->data->x_customer_country;
                $_REQUEST['x_customer_email'] = $data->data->x_customer_email;
                $_REQUEST['x_customer_phone'] = $data->data->x_customer_phone;
                $_REQUEST['x_customer_address'] = $data->data->x_customer_address;

                $this->chargeResponse($reference);
            }else{
                header("Location: ".base_url());
            }
        }
        public function epaycoc($reference){
            // $data = json_decode(file_get_contents('https://secure.epayco.co/validation/v1/reference/'.$_REQUEST['ref_payco']));
            $p_cust_id_cliente = AuthConstants::EPAY_ID;
            $p_key             = AuthConstants::EPAY_PKEY;
            $x_ref_payco      = $_REQUEST['x_ref_payco'];
            $x_transaction_id = $_REQUEST['x_transaction_id'];
            $x_amount         = $_REQUEST['x_amount'];
            $x_currency_code  = $_REQUEST['x_currency_code'];
            $x_signature      = $_REQUEST['x_signature'];
            $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
            $x_response     = $_REQUEST['x_response'];
            $x_motivo       = $_REQUEST['x_response_reason_text'];
            $x_id_invoice   = $_REQUEST['x_id_invoice'];
            $x_autorizacion = $_REQUEST['x_approval_code'];
            //Validamos la firma
            
            if ($x_signature == $signature) {


                // 1	Aceptada
                // 2	Rechazada
                // 3	Pendiente
                // 4	Fallida
                // 6	Reversada
                // 7	Retenida
                // 8	Iniciada
                // 9	Exprirada
                // 10	Abandonada
                // 11	Cancelada
                // 12	Antifraude
                
                $trstatus = array();
                $trstatus["tx_1"] = 4; //Approved
                $trstatus["tx_2"] = 6; //Declined
                $trstatus["tx_9"] = 5; //Expired
                $trstatus["tx_3"] = 7; //Pending
                $trstatus["tx_4"] = 6; //
                $trstatus["tx_6"] = 6; //
                $trstatus["tx_7"] = 6; //
                $trstatus["tx_8"] = 7; //
                $trstatus["tx_10"] = 6; //
                $trstatus["tx_11"] = 6; //
                $trstatus["tx_12"] = 6; //
                
                $pol = ($_REQUEST['x_cod_response'] == 1)?1:4;
                
                $_REQUEST['buyerEmail'] = $_REQUEST['x_customer_email'];
                $_REQUEST['TX_VALUE'] = $_REQUEST['x_amount'];
                $_REQUEST['referenceCode'] = $_REQUEST['x_id_invoice'];
                $_REQUEST['reference_pol'] = $_REQUEST['x_transaction_id'];
                $_REQUEST['transactionState'] = $trstatus["tx_".$_REQUEST['x_cod_response']];
                $_REQUEST['polResponseCode'] = $pol;
                $this->chargeResponse($reference);
            } else {
                die("Firma no valida");
            }
        }
        public function chargeConfirmation($reference){
            $this->chargeResponse($reference,FALSE);
        }
        public function chargeResponse($reference,$redirect = true){
//            echo "<pre>";
//            var_dump($_REQUEST);
            $response = $_REQUEST;
            if($redirect==true){
                $buyer = $response['buyerEmail'];
                $transactionValue = $response['TX_VALUE'];
                $referenceCode = $response['referenceCode'];
                $referencePol = $response['reference_pol'];
                $transactionState = $response['transactionState'];
                $polResponseCode = $response['polResponseCode'];
            }  else {
                $buyer = $response['email_buyer'];
                $transactionValue = $response['value'];
                $referenceCode = $response['reference_sale'];
                $referencePol = $response['reference_pol'];
                $transactionState = $response['state_pol'];
                $polResponseCode = $response['response_code_pol'];
            }
            $this->loadRepository("Orders");
            $this->loadRepository("OrdersStatus");
            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $value = explode(".", $transactionValue);
            $email = $buyer;
            $orderId = ltrim(ltrim($referenceCode, "O"),"0");
            $order = $this->Orders->findOneBy(array("order_reference"=>$reference));
            $signature = md5($payU->key."~".$payU->merchant."~".$reference."~".$value[0]."~COP");
            $data = array();
            $reference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
            $transactionStateObj = $this->OrdersStatus->find($transactionState);
//            $this->load->library('mailgunmailer');
            $shipping = 0;
            if($order->getShipping() == AuthConstants::ID_STANDARD){
                $shipping = $order->getCustomer()->getCity()->toArray()['shipping'];
            }
            if($order->getShipping() == AuthConstants::ID_PREMIUM){
                $shipping = AuthConstants::SHIPPING_PREMIUM_COST;
            }
            $totalVal = $order->getValue() + $shipping;
            if($email== $order->getCustomer()->getEmail() && $value[0]== $totalVal){
                $order->setReference($referencePol);
                $order->setStatus($transactionStateObj);
                $order->setResponsePol($polResponseCode);
                $this->em->persist($order);
                $this->em->flush();
                //recargar puntored
                $this->loadRepository("MobileRefills");
                $refill = $this->MobileRefills->findBy(array('order'=>$order->getId()));
//                var_dump(count($refill));
                if(count($refill)>0){
                    $data["messagge"] = "tx_complete_fill--".$reference;
//                    $this->load->library('consumer');
//                    $response  = $this->consumer->findPaquetigo('16');
//    //                var_dump($this->consumer);
//                    var_dump(strtolower($refill[0]->getBrand()->getBrand()));
//                    var_dump("movistar");
//                    var_dump($refill[0]->getNumber());
//                    var_dump($refill[0]->getValue());
//                    var_dump($refill[0]->getId());
                    include("consumerWS.php"); 
                    //
                    ////nueva recarga
                    $wsdl = new ConsumerWS();
//                    $wsdl->setOwner("movistar");
                    $wsdl->setOwner(strtolower($refill[0]->getBrand()->getBrand()));
                    $wsdl->setNumber($refill[0]->getNumber());
                    $wsdl->setValue($refill[0]->getValue());
                    $wsdl->setTrace($refill[0]->getId());
                    $rta = $wsdl->send();
//                    $wsdl->findPaquetigo('16');
//                    $wsdl->sendPaquetigo('16','3013213232');
//                    var_dump($response);
                    if($rta['codigoRespuesta']=='00'){
                        $reference[0]->setFilled(1);
                        $this->em->persist($reference[0]);
                        $this->em->flush();
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);
                        
                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"recargas",$params);

                    }else{
                        $to[]= array('name'=>  AuthConstants::ML_SUPPORTNAME,'mail'=>  AuthConstants::ML_SUPPORTEMAIL);


                        $params = array();
                        $params['*|FILLID|*'] = $refill[0]->getId();
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_ERRORSUBJECT,"errorPuntored",$params);

                        
                    }
//                    exit;
                }else{
                    if($transactionState == 4 && $polResponseCode == 1){
                        if($order->getInvoice()==0){

                            $dqlInvoice = "select max(o.invoice) invoice from models\\Orders o ";
                            $querySearch = $this->em->createQuery($dqlInvoice);
                            $invoiceNum = $querySearch->getResult();
//                            var_dump($invoiceNum);
                            $invoiceNum = $invoiceNum[0]['invoice'];
//                            exit();
                            $order->setInvoice($invoiceNum + 1);
                            $this->em->persist($order);
                            $this->em->flush();

                            $data["messagge"] = "tx_complete--".$reference;
                            $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                            $email      = $order->getCustomer()->getEmail();
                            $to[]= array('name'=>$fullName,'mail'=>$email);
                            $this->loadRepository("ProductsOrders");
                            $products = $this->ProductsOrders->findBy(array('order'=>$order->getId()));
                            $listProducts = '<ul>';
                            $toL = array();
                            $toL[]= array('name'=>  AuthConstants::ML_INFONAME,'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
                            $params = array();
                            foreach ($products as $item) {
                                if($item->getProduct()->getId() == AuthConstants::ENGLISH_ID){
                                    $this->makeIntelligingCurl();
                                }
                                $listProducts .= "<li>".$item->getProduct()->getBrand()->getBrand()." ".$item->getProduct()->getProduct()." (".$item->getQuantity().")</li>";
                                $item->getProduct()->setStock($item->getProduct()->getStock()- $item->getQuantity());
                                if($item->getProduct()->getStock() <= 0){
    //                                $item->getProduct()->setStatus(0);
                                    //notify
                                    $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
                                    $params['*|PRODUCTS|*'] = $item->getProduct()->getProduct();
                                    $params['*|CURRENT_YEAR|*'] = date("Y");
                                    $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                                    $this->notify($toL,AuthConstants::ML_NEWTRANSACTION,"stockout",$params);
                                }
                                $this->em->persist($item);
                                $this->em->flush();
                            }
                            $listProducts .= '</ul>';
                            if($shipping == 0){
                                $listProducts .= '<br>';
                                $listProducts .= lang("shipping")." : ".  lang("on_site");
                            }else if($shipping == AuthConstants::SHIPPING_PREMIUM_COST){    
                                $listProducts .= lang("shipping")." : ".  lang("premium");
                            }else{
                                $listProducts .= lang("shipping")." : ".  lang("standard");
                            }
                            $listCustomer = '<ul>';
                            $listCustomer .= "<li>".lang("name").": ".$order->getCustomer()->getName()." ".$order->getCustomer()->getLastName()."</li>";
                            $listCustomer .= "<li>".lang("dni").": ".$order->getCustomer()->getDni()."</li>";
                            $listCustomer .= "<li>".lang("phone").": ".$order->getCustomer()->getPhone()."</li>";
                            $listCustomer .= "<li>".lang("mobile").": ".$order->getCustomer()->getMobile()."</li>";
                            $listCustomer .= "<li>".lang("address").": ".$order->getCustomer()->getAddress()."</li>";
                            $listCustomer .= "<li>".lang("city").": ".$order->getCustomer()->getCity()->getName()."</li>";
                            $listCustomer .= '</ul>';
                            $params = array();
                            $params['*|NAME|*'] = $order->getCustomer()->getName();
                            $params['*|ORDER|*'] = $reference;
                            $params['*|PRODUCTS|*'] = $listProducts;
                            $params['*|ADDRESS|*'] = $order->getCustomer()->getAddress();
                            $params['*|CURRENT_YEAR|*'] = date("Y");
                            $params['*|MC_PREVIEW_TEXT|*'] = AuthConstants::ML_SUCCESSSUBJECT;
                            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
    //                        $campaing = $order->getCustomer()->getCampaign()->getId();
    //                        if($campaing == 5){
    //                            $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"hooponopono",$params);
    //                        }else{
    //                        }
                            $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"transaccion",$params);
                            $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
                            $params['*|PRODUCTS|*'] = $listProducts;
                            $params['*|ADDRESS|*'] = $listCustomer;
                            $this->notify($toL,AuthConstants::ML_NEWTRANSACTION,"insidetransaccion",$params);

                            $params['*|NAME|*'] = AuthConstants::ML_CEONAME;

                            $toO[]= array('name'=>  AuthConstants::ML_INFONAME,'mail'=>  AuthConstants::ML_OWNEREMAIL);
                            $this->notify($toO,AuthConstants::ML_NEWTRANSACTION,"insidemoneytransaccion",$params);
                        }

                    }else if($transactionState == 6 && $polResponseCode != 19){
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);


                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|URL|*'] = base_url()."looking/linkPayment/".$reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_REJECTEDSUBJECT,"pagoRechazado",$params);

                        $data["messagge"] = "tx_incomplete--";
                        
                    }else if($transactionState == 7){
                        $fullName   = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
                        $email      = $order->getCustomer()->getEmail();
                        $to[]= array('name'=>$fullName,'mail'=>$email);


                        $params = array();
                        $params['*|NAME|*'] = $order->getCustomer()->getName();
                        $params['*|ORDER|*'] = $reference;
                        $params['*|CURRENT_YEAR|*'] = date("Y");
                        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                        
                        
                        $this->notify($to,AuthConstants::ML_PENDINGSUBJECT,"pendiente",$params);

                        $data["messagge"] = "tx_pending--";
                        
                    }
                }
            }else{
                echo "no corresponde";
                $data["messagge"] = "tx_incomplete--";
                //notificar error
            }
//            if(! $this->session->userdata(AuthConstants::CART)){
                $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
//            }
            if($redirect) redirect("looking/?msg=".$data['messagge']);
            else echo $data['messagge'];
        }
        
        public function makeIntelligingCurl(){
            if(isset($_REQUEST['x_ref_payco'])){
                $fields = array(
                    'x_ref_payco' => $_REQUEST['x_ref_payco'],
                    'x_description' => $_REQUEST['x_description'],
                    'x_transaction_date' => $_REQUEST['x_transaction_date'],
                    'x_transaction_id' => $_REQUEST['x_transaction_id'],
                    'x_response' => $_REQUEST['x_response'],
                    'x_response_reason_text' => $_REQUEST['x_response_reason_text'],
                    'x_cod_response' => $_REQUEST['x_cod_response'],
                    'x_bank_name' => $_REQUEST['x_bank_name'],
                    'x_id_invoice' => $_REQUEST['x_id_invoice'],
                    'x_franchise' => $_REQUEST['x_franchise'],
                    'x_amount_ok' => $_REQUEST['x_amount_ok'],
                    'x_customer_name' => $_REQUEST['x_customer_name'],
                    'x_customer_lastname' => $_REQUEST['x_customer_lastname'],
                    'x_customer_country' => $_REQUEST['x_customer_country'],
                    'x_customer_email' => $_REQUEST['x_customer_email'],
                    'x_customer_phone' => $_REQUEST['x_customer_phone'],
                    'x_customer_address' => $_REQUEST['x_customer_address']
                    
                );

                // echo "<pre>";
                // var_dump($fields);exit;
                $ch = curl_init();
    
                //    curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt_array($ch, array(
                    CURLOPT_RETURNTRANSFER => true,
                    // CURLOPT_URL => 'http://localhost/looking/looking/apiRegister',
                    // CURLOPT_URL => 'https://i-biling.com/PagosRestWEB/rest/state/response',
                    CURLOPT_URL => 'https://i-biling.com/PagosRestWEB/rest/state/test',
                    CURLOPT_USERAGENT => 'Codular Sample cURL Request',
                    CURLOPT_POST => 1,
                    CURLOPT_POSTFIELDS => $fields
                ));
            // Send the request & save 
                $userId = curl_exec($ch);
                curl_close($ch);
               $userId = json_decode($userId);
                var_dump($userId);exit;
            }
        }

        public function notify($to,$subject,$template,$params, $attach = null){
            $this->load->library('mailgunmailer');
            $this->mailgunmailer->notify($to,$subject,$template,$params,false,false, $attach);
        }

        public function getCountries(){
            $this->loadRepository("Countries");
            $countries = $this->Countries->findAll();
            $return = array();
            foreach ($countries as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName());
            }
            return $return;
        }
        public function getDocTypes(){
            $this->loadRepository("DocumentTypes");
            $types = $this->DocumentTypes->findAll();
            $return = array();
            foreach ($types as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getDocumentType());
            }
            return $return;
        }
        public function getStates(){
            $this->loadRepository("States");
            $states = $this->States->findBy(array('country'=>  $this->input->post('countryId')));
            $return = array();
            foreach ($states as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName());
            }
            echo json_encode(array('status'=>true,'data'=>$return));
        }
        public function getCities(){
            $this->loadRepository("Cities");
            $cities = $this->Cities->findBy(array('state'=>  $this->input->post('stateId')));
            $return = array();
            foreach ($cities as $key => $item) {
                $return[] = array('id'=>$item->getId(),'name'=>$item->getName(),'shipping'=>$item->getShipping());
            }
            echo json_encode(array('status'=>true,'data'=>$return));
        }
        public function getSubcategories(){
            $this->loadRepository("Categories");
            $categories = $this->Categories->findBy(array("parent"=>  $this->input->post('id'),'status'=>1));
            $return = array();
            foreach ($categories as $value) {
                $action = ($value->getId()== 3)?  base_url("/looking/chargeCheckout"):base_url("/looking/results");
                $return[] = array("id"=>$value->getId(),"category"=>$value->getCategory(),'questions'=>  $this->getQuestions($value->getId()),"action"=>$action);
            }
            $tmpl = file_get_contents("templates/ajax/subcategories.php");
            echo json_encode(array('status'=>true,'data'=>$return,"tmpl"=>$tmpl));
        }
        public function getCatData(){
            $this->loadRepository("Categories");
            $categories = $this->Categories->findBy(array("parent"=>  $this->input->post('id'),'status'=>1));
            $return = array();
            if(count($categories)>0){
                foreach ($categories as $value) {
                    $action = ($value->getId()== 3)?  base_url("/looking/chargeCheckout"):base_url("/looking/results");
                    $return[] = array("id"=>$value->getId(),"category"=>$value->getCategory(),'questions'=>  $this->getQuestions($value->getId()),"action"=>$action);
                }
            }  else {
                $cat = $this->Categories->find($this->input->post('id'));
                $return[] = array("id"=>$cat->getId(),"category"=>$cat->getCategory(),'questions'=>  $this->getQuestions($cat->getId()));
                
            }
            $tmpl = file_get_contents("templates/ajax/subcategories.php");
            $tmpl2 = file_get_contents("templates/ajax/resultsquestions.php");
            $tmpl3 = file_get_contents("templates/ajax/thirdcat.php");
            echo json_encode(array('status'=>true,'data'=>$return,"tmpl"=>$tmpl,"tmpl2"=>$tmpl2));
        }
        public function getCategories($active = 0,$home = false){
            $this->loadRepository("Categories");
            $categories = $this->Categories->findBy(array('home'=>$active),array("position"=>"ASC"));
            $return = array();
            foreach ($categories as $value) {
                $action = ($value->getId()== 3)?  base_url("/looking/chargeCheckout"):base_url("/looking/results");
                if($value->getParent()!== null){
                    if($home){
                        if($value->getHome() == 1){
                            $return[] = array("id"=>$value->getId(),"category"=>$value->getCategory(),"image"=>$value->getImage());
                        }
                    }else{
                        $return[] = array("id"=>$value->getId(),"category"=>$value->getCategory(),"image"=>$value->getImage());
                    }
                }
            }
            return $return;
        }
        public function getQuestions($idCat){
            $this->loadRepository("Questions");
            $questions = $this->Questions->findBy(array("category"=>$idCat,'status'=>1),array("order"=>"ASC"));
            $return = array();
            foreach ($questions as $value) {
                if($value->getOuter()=="brand"){
                     if(count($this->getAnswers($value->getId()))==0){
                        $this->loadRepository("BrandsCategories");
                        $brandCats = $this->BrandsCategories->findBy(array("category"=>$idCat));
                        $bCats =array();
                        foreach ($brandCats as $catValue) {
                            $bCats[] = array("id"=>$catValue->getBrand()->getId(),"option"=>$catValue->getBrand()->getBrand());
                        }
                        $return[] = array("id"=>$value->getId(),"question"=>$value->getQuestion(),"outer"=>$value->getOuter(),"category"=>$idCat,"label"=>$value->getLabel(),"multiple"=>$value->getMultiple(),"type"=>$value->getType(),"icon"=>$value->getIcon(),"options"=>  $bCats , "suggestions" => $this->getSuggestions($value->getId()));
                     }
                }else{
                    $return[] = array("id"=>$value->getId(),"question"=>$value->getQuestion(),"outer"=>$value->getOuter(),"category"=>$idCat,"label"=>$value->getLabel(),"multiple"=>$value->getMultiple(),"type"=>$value->getType(),"icon"=>$value->getIcon(),"step"=>$value->getStep(),"options" =>  $this->getAnswers($value->getId()),"suggestions" => $this->getSuggestions($value->getId()));
                }
            }
            return $return;
        }
        public function getSuggestions($idQuestion){
            
            $this->loadRepository("Suggestions");
            $suggestions = $this->Suggestions->findBy(array("question"=>$idQuestion,'status'=>1),array("order"=>"ASC"));
            $sugg = array();
            if(count($suggestions)>0){
                foreach ($suggestions as $singleS){
                    $sugg[] = array(
                                "id"=>$singleS->getId(),
                                "tag"=>$singleS->getTag(),
                                "value"=>$singleS->getValue(),
                                "question"=>$singleS->getQuestion()->getId(),
                                "option"=>$singleS->getImage()
                            );
                }
           }
           return $sugg;
        }
        public function getAnswers($idQuestion){
            $this->loadRepository("Options");
            $options = $this->Options->findBy(array("question"=>$idQuestion,'status'=>1));
            $return = array();
            foreach ($options as $value) {
                $return[] = array("id"=>$value->getId(),"option"=>$value->getOption());
            }
            return $return;
        }
            
        public function generatePdf(){
//            require_once APPPATH.'libraries/dompdf/autoload.inc.php';
//            // reference the Dompdf namespace
//
//            // instantiate and use the dompdf class
//            $dompdf = new Dompdf();
//            $dompdf->loadHtml('hello world');
//
//            // (Optional) Setup the paper size and orientation
//            $dompdf->setPaper('A4', 'landscape');
//
//            // Render the HTML as PDF
//            $dompdf->render();
//
//            // Output the generated PDF to Browser
//            $dompdf->stream();
//            $this->pdf->test();
            $data = array();
            $this->load->library('pdf');
//            $this->pdf->loadHtml('hello world');
//            $this->pdf->setPaper('A4', 'landscape');
//            $this->pdf->render();
            $this->pdf->load_view('invoicetest',$data);
            $this->pdf->render();
            $this->pdf->stream("testing");
        }
        
        public function validateOrder($order){
            $order = explode("TNT", $order);
            if($order[0] == AuthConstants::IN_HASH){
                $this->loadRepository("Orders");
                $orderOb = $this->Orders->find($order[1]);
                $this->loadRepository("OrdersStatus");
                $status = $this->OrdersStatus->find(AuthConstants::IN_APPROVED);
                $orderOb->setStatus($status);
                $this->em->persist($orderOb);
                $this->em->flush(); 
                header('Location: '.  base_url());
            }else{
                header('Location: '.  base_url());
            }
        }
        public function reservedOrder($order){
            if($order != ""){
                
                $this->loadRepository("Orders");
    //            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
                $orderId = ltrim(ltrim($order, "O"),"0");
                $orderOb = $this->Orders->findOneBy(array('order_reference'=>$order));
    //            var_dump($orderOb);
                $fullName   = AuthConstants::ML_SUPPORTNAME;
                $email      = AuthConstants::ML_LOGISTICSEMAIL;
                $to[]= array('name'=>$fullName,'mail'=>$email);
//                $fullName   = AuthConstants::ML_COONAME;
//                $email      = AuthConstants::ML_OPERATIONSEMAIL;
//                $to[]= array('name'=>$fullName,'mail'=>$email);
    //            $to[]= array('name'=>$fullName,'mail'=>'john.jimenez@lookingforweb.com');

                $method = $orderOb->getPaymentMethod()->getId();

                $this->loadRepository("ProductsOrders");
                $products = $this->ProductsOrders->findBy(array('order'=>$orderOb->getId()));
                $listProducts = '<ul>';
                $listCodes = '<ul>';
                foreach ($products as $item) {
                    $listProducts .= "<li>".$item->getProduct()->getBrand()->getBrand()." ".$item->getProduct()->getProduct()."</li>";
                    $this->loadRepository("ProductsOrdersCodes");
                    $code = $this->ProductsOrdersCodes->findOneBy(array('product_order'=>$item->getId()));
                    if($code !== null)$listCodes .= "<li>".$code->getCode()."</li>";
                }
                $listProducts .= '</ul>';
                $listCodes .= '</ul>';

                $listCustomer = '<ul>';
                $listCustomer .= "<li>".lang("name").": ".$orderOb->getCustomer()->getName()." ".$orderOb->getCustomer()->getLastName()."</li>";
                $listCustomer .= "<li>".lang("dni").": ".$orderOb->getCustomer()->getDni()."</li>";
                $listCustomer .= "<li>".lang("phone").": ".$orderOb->getCustomer()->getPhone()."</li>";
                $listCustomer .= "<li>".lang("mobile").": ".$orderOb->getCustomer()->getMobile()."</li>";
                $listCustomer .= "<li>".lang("address").": ".$orderOb->getCustomer()->getAddress()."</li>";
                $listCustomer .= "<li>".lang("city").": ".$orderOb->getCustomer()->getCity()->getName()."</li>";
                $listCustomer .= '</ul>';

                $params = array();
                $params['*|NAME|*'] = AuthConstants::ML_EPMNAME;
                $params['*|ORDER|*'] = $order;
                $params['*|PRODUCTS|*'] = $listProducts;
                $params['*|ADDRESS|*'] = $listCustomer;
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
//                var_dump($params);
//                var_dump($to);
                $this->session->set_userdata(array(AuthConstants::CART=>array(),  AuthConstants::COMPARISON=>array()));
                $this->notify($to,AuthConstants::ML_SUCCESSSUBJECT,"planes",$params);


                $toUser[]= array('name'=>$orderOb->getCustomer()->getName(),'mail'=>$orderOb->getCustomer()->getEmail());
                $params = array();
                $params['*|NAME|*'] = $orderOb->getCustomer()->getName();
                $params['*|ORDER|*'] = $order;
                $params['*|PRODUCTS|*'] = $listProducts;
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
    //            var_dump($params);
    //            var_dump($toUser);
                if($method == AuthConstants::PM_GENERATE_CODE){
                    $tmpl = "codigo";
                    $subject = AuthConstants::ML_CODESUBJECT;
                    $params['*|CODE|*'] = $listCodes;
                }else{
                    $subject = AuthConstants::ML_RESERVEDSUBJECT;
                    $tmpl = "reserva";
                }

                $this->notify($toUser,$subject,$tmpl,$params);
            }
        }
        public function secondCheckout(){
        
            $dql = "select distinct c.name, c.email, c.id, o.id orderid from  models\\Orders o "
                    . "JOIN o.customer c "
//                    . "where (o.invoice > 52 and o.invoice < 100) "
                    . "  ";
            $querySearch = $this->doctrine->em->createQuery($dql);
            $users = $querySearch->getResult();
            echo count($users);
            foreach ($users as $key => $buyer) {
                echo "<li>".$buyer["id"];
                echo "<li>".$buyer["orderid"];
                echo "<li>".$buyer["email"];
                $this->loadRepository("Cupons");
                $cupon = $this->Cupons->findOneBy(array("customer"=>$buyer["id"]));
//                $cupon = new models\Cupons();
//                $code = substr(md5(rand(1,999999)),0,7);
//                $cupon->setCode($code);
//                $cupon->setFilter(AuthConstants::CUPON_FILTER_CATS);
//                $cupon->setFilterId(5);
//                $this->loadRepository("Customers");
//                $user = $this->Customers->find($buyer["id"]);
//                $cupon->setCustomer($user);
//
//                $cupon->setDiscount(AuthConstants::CUPON_SECOND_CHECKOUT);
//                $end = new DateTime(date("Y-m-t"));
//                $cupon->setEnd($end);
//                $this->em->persist($cupon);
//                $this->em->flush();
//                $id = $cupon->getId();
//
//
                if($cupon !== null){
                    
//                var_dump($cupon->getCode());
                    $to[]= array();
                    $to[]= array('name'=>$buyer["name"],'mail'=>$buyer["email"]);
                    $params = array();
                    $params['*|CUPON|*'] = $cupon->getCode();
                    $params['*|MC_PREVIEW_TEXT|*'] = "Ingresa este código al momento de realizar tu pago";
                    $params['*|CURRENT_YEAR|*'] = date("Y");
                    $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
        //            var_dump($params);
        //            var_dump($to);

                    $this->notify($to,AuthConstants::ML_SECONDCHECKOUT,"cuponcompradores",$params);
                }
            }
        }
        public function pendingOrder($order){
            $this->loadRepository("Orders");
//            $payU = $this->getPayUData(AuthConstants::PAYU_MODE);
            $orderId = ltrim(ltrim($order, "O"),"0");
            $orderOb = $this->Orders->find($orderId);
//            var_dump($orderOb);
            $fullName   = $orderOb->getCustomer()->getName()." ".$orderOb->getCustomer()->getLastName();
            $email      = $orderOb->getCustomer()->getEmail();
            $to[]= array('name'=>$fullName,'mail'=>$email);
//            $to[]= array('name'=>$fullName,'mail'=>'john.jimenez@lookingforweb.com');


            $params = array();
            $params['*|NAME|*'] = $orderOb->getCustomer()->getName();
            $params['*|ORDER|*'] = $order;
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
//            var_dump($params);
//            var_dump($to);

            $this->notify($to,AuthConstants::ML_PENDINGSUBJECT,"pendiente",$params);
        }
        
        public function demo(){
            $data = array();
		$data["title"] = "Demo";
		$this->viewPublic('demo', $data);
        }

        public function forbbiden()
	{
		$data = array();
		$data["title"] = "Forbbiden";
		$data["menu"]   = "";
		
		$this->view('forbbiden', $data);
	}
}
