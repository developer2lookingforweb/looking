<div class="container comparison">
    <div class="row">
        <div class="col-xs-12 col-sm-offset-3 col-sm-9">
            <div class="row">
                <?php 
                $columns = 12/count($products);
                $i=0;
                foreach($products as $idx => $product){
                    $i++;
                ?>
                    <div class="col-xs-<?php echo $columns;?> my_planHeader my_plan<?php echo $i ?>">
                        <div class="my_planTitle"><?php echo $product["name"] ?></div>
                        <!--<div class="my_planTitle"><?php echo lang("costo_unit").number_format($product["cost"]) ?></div>-->
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-offset-3 col-sm-9">
            <div class="row">
                <?php 
                $columns = 12/count($products);
                $i=0;
                $reference=array();
                foreach($products as $idx => $product){
                    $reference[] = array("id"=>$product["id"],"name"=>  str_replace(" ", "-", $product["name"]));
                    $i++;
                    $image= explode("|", $product["image"])
                ?>
                    <div class="col-xs-<?php echo $columns;?>  ">
                        <div class="text-center" ><img style="width: 50%;" src="<?php echo base_url("images/products/".$image[0]) ?>"></div>
                    </div>
                <?php }?>
                
            </div>
        </div>
    </div>
    
        <?php 
        $columns = 12/count($products);
        foreach($details as $idx => $singleDetail){
        $i=0;
        ?>
        <div class="row my_featureRow">
            <div class="col-xs-12 col-sm-3 my_feature">
                <?php echo $singleDetail["label"];?>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="row">
            <?php foreach($singleDetail["details"] as $id => $singleDetail){
                $i++;?>
                <div class="col-xs-<?php echo $columns;?> col-sm-<?php echo $columns;?> my_planFeature my_plan<?php echo $i;?>">
                    &nbsp;<?php echo $singleDetail;?>
                </div>
            <?php }?>
                </div>
            </div>
        </div>
        <?php }?>
        <div class="row my_featureRow">
            <div class="col-xs-12 col-sm-3 my_feature">
                <?php echo lang("costo")?>
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="row">
                <?php 
                if(count($products)>0){
                $i=0;    
                foreach($products as  $singleDetail){
                    $itemCost = ($singleDetail["offer"]>0)?$singleDetail["offer"]:$singleDetail["cost"];
                    $i++;?>
                    <div class="col-xs-<?php echo $columns;?> col-sm-<?php echo $columns;?> my_planFeature my_plan<?php echo $i;?>">
                        <?php echo lang("costo_unit")." ".number_format($itemCost);?>
                    </div>

                <?php }}?>
                </div>
            </div>
        </div>
        <div class="row my_featureRow">
            <div class="col-xs-12 col-sm-3 my_feature">
                &nbsp;
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="row">
            <?php 
            if(count($products)>0){
            $i=0;    
            foreach($products as  $singleDetail){
                $i++;?>
                    <div class="col-xs-<?php echo $columns;?> col-sm-<?php echo $columns;?> my_planFeature my_plan<?php echo $i;?>">
                        <?php $itemCost = ($singleDetail["offer"]>0)?$singleDetail["offer"]:$singleDetail["cost"];?>
                        <button type="button" class="add-product btn bg-primary" item-offer="<?php echo $singleDetail["offerId"]?>" item-id="<?php echo $singleDetail["id"]?>" item-cost="<?php echo $itemCost?>">
                            COMPRAR
                        </button>
                    </div>
<!--                <div class="col-xs-<?php echo $columns;?> col-sm-<?php echo $columns;?> my_planFeature my_plan<?php echo $i;?>">
                    <a class="btn btn-sm btn-success bg-primary" href="<?php echo base_url("detail/".$singleDetail["id"]."/".$singleDetail["name"]);?>">Detalles</a>
                </div>-->
            
            <?php }}?>
                </div>
            </div>
        </div>

        
    
</div>
<br>
<br>
<br>
