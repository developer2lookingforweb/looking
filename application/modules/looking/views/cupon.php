<style>
    
    .btn-cupon{
        border-radius: 0 5px 5px 0;
        height: 40px;
    }
    .input-cupon{
        border-radius: 5px 0 0 5px;
        border: 1px solid #ee4b28;
        height: 40px;
    }
    .cupon-amount {
        font-size: 5rem;
        font-weight: 600;
        margin-bottom: 0px;
    }
    .cupon-complement {
        margin-top: 0px;
        margin-bottom: 10%;
    }
    .cupon-amount i{
        color: #ee4b28;
        font-style: normal;
    }
    @media only screen and (max-width: 768px) {
        .cupon-amount{
            font-size: 3rem;
        }
        .cupon-complement {
            font-size: 1.5rem;
        }
    }
</style>
    
    <div class="row">
    <div class="col-md-12">
        <div class="loginw-box">
            <form id="registerForm" class="form-horizontal" role="form" action="<?php echo base_url("login/signin10"); ?>" method="post" >
                <div class="modal-body">
                    <div class="">
                        <div class="col-md-12 col-xs-8 col-xs-push-2">
                            <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/logo1.png" alt="" />
                        </div>
                    </div> 
                    <div class="">
                        <div class="col-md-12 col-xs-12 text-center">
                            <h5 class='tit_loginh1' >Juntos encontraremos lo que tu necesitas</h5>
                        </div>  
                    </div>
                    
                    <div class="">
                        <div class="col-md-12 text-center">
                            <h1 class='cupon-amount' >Recibe <i class='discount' ><?php echo $cupon; ?> % </i> </h2>
                        </div>  
                    </div>
                    
                    <div class="">
                        <div class="col-md-12 text-center">
                            <h3 class="cupon-complement"> En tu primera compra</h3>
                        </div>  
                    </div> 
                    
                    <div class="">
                        <div class="col-md-12 text-center">
                            <span class="tit_loginredes"> A dónde enviamos tu cupón?</span>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" id="email" name="email" class="input-cupon required email col-lg-8 col-xs-12"  placeholder="Correo" error-msg="No es un correo válido">
                            <input type="submit" id="submit" name="submit" class="btn-cupon bg-primary btn-group-lg btn col-lg-4 col-xs-12"  value="Recibir <?php echo $cupon; ?>%">
                        </div> 
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            &nbsp;
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12 text-center">
                            <span class="tit_loginredes"><a href="#" id="close-cupon"> No gracias, prefiero pagar el precio completo</a></span>
                        </div>  
                    </div> 
                    <div class="">
                        <div class="col-md-12 text-center">
                            <span class="tit_loginredes">Válido únicamente para compras online</span>
                        </div>  
                    </div> 

                </div> 	
            </form>  	
        </div>
    </div> 
    <div class="col-md-3">  
    </div> 
</div>