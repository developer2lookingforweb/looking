<div class="loginw-box">
    <form class="form-horizontal" role="form" action="<?php echo base_url("login/authCustomer"); ?>" method="post" >
        <div class="modal-body">
            <div class="form-group">
                <div class="col-md-12">
                    <img class='img-responsive center-block' src="<?php echo base_url(""); ?>images/logo0.png" alt="" />
                </div>
            </div> 
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <h3 class='tit_loginh1' >Ingresa a tu cuenta</h3>
                </div>  
            </div>
            <div class="">
                <div class="col-md-12 text-center">
                    <ul class="list-inline">
                        <li><a href="<?php echo $loginUrl ?>" class='btn btn-facebook btn-lg ga-facebook-signup' ><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;Entrar con Facebook</a></li>
                        <!--<li><a href="principal.html" class="btn btn-danger"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;&nbsp;Google</a></li>-->
                    </ul>
                </div>  
            </div> 
            <hr>
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <p class="tit_loginredes"> También puedes ingresar con tu correo y contraseña </p>
                </div>  
            </div> 
            <div class="form-group">
                <div class="col-md-1"></div>  
                <div class="col-md-10">
                    <input type="email" id="email" name="email" class="form-control profile email "  placeholder="Correo">
                </div> 
                <div class="col-md-1"></div>  								
            </div>
            <div class="form-group">
                <div class="col-md-1"></div>  
                <div class="col-md-10">
                    <input type="password"  id="password"  name="password" class="form-control profile input_lin required" placeholder="Contraseña" >
                </div>
                <div class="col-md-1"></div>  
            </div>
            <div class="form-group">
                <div class="col-md-1"></div>  
                <div class="col-md-12 text-center">
                    <button type="submit" id="submit" class="btn btn-primary">INGRESAR</button>
                </div>  
                <div class="col-md-1"></div>  
            </div> 
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <p class='' >¿Olvidaste tu contraseña? <a class="remember-password" href="#"> Recupérala aquí</a></p>
                </div>  
            </div> 

        </div> 	
    </form>  	
</div>