<header class="header-spacing"></header>
<div class="container-fluid bg-primary col-sm-12 categories-container">
    <div class="btn-group">
        <a class="btn  text-faded go-back" data-toggle="dropdown" href="#">
            &nbsp;
        </a>
    </div>
</div>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-lg-12" >
            <div class="col-lg-1" >
                <label>Fecha</label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <input id="date1" type="text" class="datepicker form-control" value="<?php echo $date2->format("Y/m/d")?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-1" >
                <label>Hasta</label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <input id="date2" type="text" class="datepicker form-control" value="<?php echo $date2->format("Y/m/d")?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-1" >
                <label><?php echo lang("user")?></label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <select id="place" class=" form-control" >
                        <option value="0"><?php echo lang("default_select")?></option>
                        <?php foreach ($users as $aUser){?>
                        <option value="<?php echo $aUser ?>"><?php echo $aUser ?></option>
                        <?php }?>
                    </select>
                </div>
                
            </div>
            <div class="col-lg-2" >
                <label>Registros Encontrados</label>
            </div>
            <div class="col-lg-1" >
                <div class="records-found" >
                    <?php echo count($fills) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<div class="container">
    <div class="col-lg-12" >
        <a id="download" href="#">Descargar Archivo</a>
    </div>
</div>
<br/>
<br/>
<div class="row">
    <div class="col-lg-12" >

        <div class="container product-detail">
            <div class="row">

                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo lang("filled")?></th>
                                <th><?php echo lang("value")?></th>
                                <th><?php echo lang("brand")?></th>
                                <th><?php echo lang("number")?></th>
                                <th><?php echo lang("requestDate")?></th>
                                <th><?php echo lang("user")?></th>
                            </tr>
                        </thead>
                        <tbody id="results">
                            <?php foreach ($fills as $row) { ?>
                                <tr>
                                    <td><?php echo $row["id"] ?></td>
                                    <td><?php echo $row["filled"] ?></td>
                                    <td><?php echo $row["brand"] ?></td>
                                    <td><?php echo $row["value"] ?></td>
                                    <td><?php echo $row["number"] ?></td>
                                    <td><?php echo $row["requestDate"] ?></td>
                                    <td><?php echo $row["place"] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>