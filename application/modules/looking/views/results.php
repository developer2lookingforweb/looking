<?php
function uri_parse($string){
	return $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
}
?>
	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-color: #<?php echo $color?> ;background-image: url(<?php echo base_url()?>images/categories/<?php echo $categoryImage?>);">
		<h2 class="l-text2 t-center">
			<?php echo $categoryName?>
		</h2>
		<p class="m-text13 t-center">
			<!-- New Arrivals Women Collection 2018 -->
		</p>
	</section>


	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
					<div class="leftbar p-r-20 p-r-0-sm">
						<!--  -->
						<h4 class="m-text14 p-b-7">
							Categorías
						</h4>

						<ul class="p-b-54">
							<?php foreach ($categories as $cat) {
								$classActive = ($cat["id"]== $cid)?"active1":"";	
							?>
									
								<li class="p-t-4">
									<a href="<?php echo base_url("category/".$cat["id"]."/".  str_replace(" ", "-", $cat['category'])); ?>" class="s-text13 <?php echo $classActive?>">
										<?php echo $cat['category']?>
									</a>
								</li>
								<?php } ?>
						
						</ul>

						<!--  -->
						
					</div>
				</div>

				<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
					<!--  -->
					<!--<div class="flex-sb-m flex-w p-b-35">
						<div class="flex-w">
							<div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
								<select class="selection-2" name="sorting">
									<option>Default Sorting</option>
									<option>Popularity</option>
									<option>Price: low to high</option>
									<option>Price: high to low</option>
								</select>
							</div>

							<div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
								<select class="selection-2" name="sorting">
									<option>Price</option>
									<option>$0.00 - $50.00</option>
									<option>$50.00 - $100.00</option>
									<option>$100.00 - $150.00</option>
									<option>$150.00 - $200.00</option>
									<option>$200.00+</option>

								</select>
							</div>
						</div>

						<span class="s-text8 p-t-5 p-b-5">
							Showing 1–12 of 16 results
						</span>
					</div>-->

					<!-- Product -->
					<div id="products" class="row">
                        <?php foreach ($products as $idx => $item) {
                        $noImages = count(explode("|", $item->image));
						$images = explode("|", $item->image);
						$images[0] = file_exists(("./images/products/".$images[0]))?$images[0]:"noimage.jpg";
                        ?>
						<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
							<!-- Block2 -->
							<div class="block2">
								<?php $sale = ($item->offer > 0 || $item->market_price > 0)?"block2-labelsale":""; ?>

								<div class="block2-img wrap-pic-w of-hidden pos-relative <?php echo $sale?>">
									<img src="<?php echo base_url(); ?>images/products/<?php echo $images[0]?>" alt="<?php echo $item->product ?>">

									<div class="block2-overlay trans-0-4">
										<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
											<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
											<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
										</a>
										
										<div class="block2-btn-addcart w-size1 trans-0-4">
											<!-- Button -->
											<a href="<?php echo base_url("detail/" . $item->id . "/" . uri_parse($item->product)) ?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4 m-b-10">
												Ver Más
											</a>
											<!-- Button -->
											<!-- <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
												Agregar
											</button> -->
										</div>
									</div>
								</div>
								
								<div class="block2-txt p-t-20">
									<a href="<?php echo base_url("brand/" . $item->provid . "/" . uri_parse($item->brand)) ?>" class="block2-name dis-block s-text3 p-b-5">
										<?php echo $item->brand ?>
									</a>
									<a href="<?php echo base_url("detail/" . $item->id . "/" . uri_parse($item->product)) ?>" class="block2-name dis-block s-text3 p-b-5">
										<?php echo $item->product ?>
									</a>
									<?php if($item->stock ==0){?>
										<span class="block2-newprice m-text8 p-r-5 text-danger"> PRODUCTO AGOTADO!</span>
									<?php }else{?>
										<?php if ($item->offer > 0 || $item->market_price > 0) { ?>
											
										<span class="block2-oldprice m-text7 p-r-5">
											$<?php echo number_format($item->market_price) ?>
										</span>
										
										<span class="block2-newprice m-text8 p-r-5">
											$<?php echo number_format($item->cost) ?>
										</span>
										<?php }else{ ?>
											
											<span class="block2-price m-text6 p-r-5">
												$<?php echo number_format($item->cost) ?>
											</span>
										<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
							
					</div>

					<!-- Pagination -->
					<!--<div class="pagination flex-m flex-w p-t-26">
						<a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
						<a href="#" class="item-pagination flex-c-m trans-0-4">2</a>
					</div>-->
				</div>
			</div>
		</div>
	</section>
