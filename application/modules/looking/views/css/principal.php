<link href="<?php echo base_url()?>assets/css/wizard-demo.css" rel="stylesheet"></link>
<style>

<?php if($color){?>
/* #products{
	border-top: 5px solid #<?php echo $color?>;
} */
#mainNav {
	border-bottom: 8px solid #<?php echo $color?> !important;
}
.navbar-default .nav > li.active > a, .navbar-default .nav > li.active > a:focus {
	color: #<?php echo $color?> !important;
}
a {
	color:  #<?php echo $color?>;
} 
.filter-popup{
	color:  #<?php echo $color?>!important;
	border: 1px solid #<?php echo $color?>!important;
}
.bg-primary {
	background-color: #<?php echo $color?>!important;
}
<?php }?>

.main-grid {
  margin-top: 90px;
}
@media only screen and (max-width: 768px) {
    .main-grid {
      margin-top: 44%;
    }
    .text-center-small{
        text-align: center;
    }
    .btn{
        font-size: 1rem;
    }
    h3{
        font-size: 2rem;
    }
    .tab-pane {
        min-height: 350px!important;
      }
}
.list-group-item {
  border: 0px solid #ddd;
}
.card.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    /*margin-bottom: 10px;*/
}
.list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover {
  background-color: #fff;
  border-color: #fff;
  color: #fff;
  z-index: 2;
}
.carousel-control{
    z-index: 3;
}
/*.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    background: #428bca;
}*/

.card.list-group-item .list-group-image
{
    /*margin-right: 10px;*/
}
.card.list-group-item .thumbnail
{
    /*margin-bottom: 0px;*/
}
.card.list-group-item .caption
{
    /*padding: 9px 9px 0px 9px;*/
}
.card.list-group-item:nth-of-type(odd)
{
    /*background: #eeeeee;*/
}

.card.list-group-item:before, .card.list-group-item:after
{
    display: table;
    content: " ";
}

.card.list-group-item img
{
    /*float: left;*/
    padding-top: 10px;
}
.card.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    /*margin: 0 0 11px;*/
}
.carousel-inner img {
  margin: auto;
}
.animated {
    -webkit-transition: height 0.2s;
    -moz-transition: height 0.2s;
    transition: height 0.2s;
}
.price-lead {
  font-size: 17px;
  font-weight: 300;
  line-height: 1.4;
  margin-top: 0px;
  margin-bottom: 0px;
}
.offer {
  float: right;
  left: 180px;
  top: 127px;
  z-index: 10;
  width: 100px!important;
  position: absolute;
}
.media-price{
    font-size: 1.1rem;
    color: #4a890f;
    margin-bottom: 5px;
}
.media-price-container {
  height: 71px;
}
.over-price{
    margin-bottom: 0px;
}
#comparison .container{
    margin-left: 0;
}
.go-comparison{
    color: white!important;
    margin: 7% 0;
}
.go-comparison:hover, .go-comparison:focus{
    background-color: #e13612!important;
}
.action-buttons a{
    margin: 2px 0;
}
#site-wrapper {
position: relative;
overflow: hidden;
width: 100%;
margin-top: 52px;
height: 100%; /* Temp: Simulates a tall page. */
}
@media (max-width: 768px) {
    #site-wrapper {
    margin-top: 0px;
    }
}
#site-canvas {
  width: 100%;
  height: 100%;
  position: relative;
  -webkit-transform: translateX(0);
  transform: translateX(0);
  -webkit-transition: .3s ease all;
  transition: .3s ease all;

}
#site-menu {
  width: 300px;
  height: 100%;
  position: absolute;
  top: 0;
  left: -300px;
  padding: 15px;
}
#site-wrapper.show-nav #site-canvas {
  -webkit-transform: translateX(300px);
  transform: translateX(300px);
}
.offcanvas-toggle{
    float: left;
}
.filter-popup{
    float: left;
}
.nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
        background-color: #333;
        color: #fff;
        border-bottom: 0px solid #ddd;
    }
    .nav-pills > li > a
    {
        border: 1px solid #ddd;
    }
    .nav-pills > li
    {
        margin: 10px 0;
    }
    .nav-pills {
        border-bottom: 0px solid #ddd;
    }
    .nav-pills > li > a > span {
        padding-left: 10px;
    }
    .suggestion-col{
        padding-top: 30px;
        padding-left: 2px;
        padding-right: 2px;
    }
    .result-header{
        margin-top: 25px;
        position: fixed;
        z-index: 100;
        width: 100%;
    }
    input[type="radio"],
    input[type="checkbox"]{
        display: none;
    }
    .container {
        margin-bottom: -17px;
        margin-left: 15%;
    }
    #filter-dialog .container {
        margin-left: 15%;
    }
    .container label {
        position: relative;
    }
    .tab-pane {
        min-height: 375px;
    }

    /* Base styles for spans */
    .container span::before,
    .container span::after {
        content: '';
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto;
    }

    #filter-div .question-label{
        padding-top: 3%;
    }
    /* Radio buttons */
    .container span.radio:hover {
        cursor: pointer;
    }
    .container span.radio::before {
        left: -52px;
        width: 45px;
        height: 20px;
        background-color: #C4C4C4;
        border-radius: 50px;
    }
    .container span.radio::after {
        left: -49px;
        width: 17px;
        height: 17px;
        border-radius: 10px;
        background-color: #6C788A;
        transition: left .25s, background-color .25s;
    }
    input[type="radio"]:checked + label span.radio::after {
        left: -27px;
        background-color: #EE4B28;
    }

    /* Check-boxes */
    .container span.checkbox::before {
        width: 27px;
        height: 27px;
        background-color: #c4c4c4;
        left: -35px;
        box-sizing: border-box;
        border: 3px solid transparent;
        transition: border-color .2s;
    }
    .container span.checkbox:hover::before {
        border: 3px solid #EE4B28;
    }
    .container span.checkbox::after {
        content: '\f00c';
        font-family: 'FontAwesome';
        left: -28px;
        top: 1px;
        color: transparent;
        transition: color .2s;
    }
    input[type="checkbox"]:checked + label span.checkbox::after {
        color: #EE4B28;
    }
    .slider {
        margin: 0;
        width: 100%!important;
    }
    .slider-handle {
        background-color: #ee4b28;
        background-image: linear-gradient(to bottom, #ee4b28 0%, #ee4b28 100%);
        background-repeat: repeat-x;
        border: 0 solid transparent;
        box-shadow: none;
        filter: none;
        height: 20px;
        position: absolute;
        top: 0;
        width: 20px;
    }
    .smartselect .btn{
        white-space: normal;
    }
    .smartselect .dropdown-menu {
        left: 16px;
    }
    .smartselect > button {
        min-width: 100%;
    }
    .slider-selection {
        background: #ee4b28;
    }
    .float-right{
        float: right;
    }
    #filter-dialog{
        padding-left: 3px;
        padding-right: 10px;
    }
    .filter-popup,.filter-popup:hover,.filter-popup:focus{
        border: 1px solid #ee4b28;
        border-radius: 5px;
        margin: 8px 1%;
        padding: 1%;
        color: #ee4b28;
    }
    .filter-title{
        margin-bottom: 10px;
        margin-left: 20px;
        margin-top: 5px;
    }
    #filter-dialog .mfp-close{
        display: none;
    }
    .order-properties {
        background-color: #c0c0c0;
        border: 1px solid #c0c0c0;
        border-radius: 10px;
        color: #3d3d3d;
        font-weight: 800;
        height: 45px;
        list-style: outside none none;
        margin: 1% 0;
        padding: 3% 0;
        text-align: center;
      }
    .order-properties img {
        float: right;
      }
      .btn-detail{
          background-color: #286090;
          color: #fff;
      }
      .recomended{
          box-shadow: 7px 7px 23px #EE4B28;
      }
      .recomended:hover{
          box-shadow: 7px 7px 23px #EE4B28;
      }
      #products .item .product-box img {
            max-height: 256px;
        }
</style>