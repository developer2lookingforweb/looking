<style>

.carousel-inner img {
  margin: auto;
}
.product-detail{
    margin-top: 20px!important;
}
/*********************************************
    			Call Bootstrap
*********************************************/


/*********************************************
        		Theme Elements
*********************************************/

.gold{
	color: #FFBF00;
}

/*********************************************
					PRODUCTS
*********************************************/

.product{
	border: 1px solid #dddddd;
	height: 321px;
}

.product>img{
	max-width: 230px;
}

.product-rating{
	font-size: 20px;
	margin-bottom: 25px;
}

.product-title{
	font-size: 20px;
}

.product-desc{
	font-size: 14px;
}

.product-price{
	font-size: 22px;
        padding: 0 0;
}
.product-price2{
	font-size: 20px;
        padding: 0 0;
}
.product-old-price {
  margin: 0px 0 20px 0;
  color: gray;
  font-size: 1.3em;
}
.product-stock{
	color: #74DF00;
	font-size: 20px;
	margin-top: 10px;
}

.product-info{
		margin-top: 50px;
}

/*********************************************
					VIEW
*********************************************/

.content-wrapper {
	max-width: 1140px;
	background: #fff;
	margin: 0 auto;
	margin-top: 0px;
	margin-bottom: 10px;
	border: 0px;
	border-radius: 0px;
}



.view-wrapper {
	float: right;
	max-width: 70%;
	margin-top: 25px;
}


/*********************************************
				ITEM 
*********************************************/

.service1-items {
	padding: 0px 0 0px 0;
	float: left;
	position: relative;
	overflow: hidden;
	max-width: 100%;
	height: 321px;
	width: 130px;
}

.service1-item {
	height: 107px;
	width: 120px;
	display: block;
	float: left;
	position: relative;
	padding-right: 20px;
	border-right: 1px solid #DDD;
	border-top: 1px solid #DDD;
	border-bottom: 1px solid #DDD;
}

.service1-item > img {
	max-height: 110px;
	max-width: 110px;
	opacity: 0.6;
	transition: all .2s ease-in;
	-o-transition: all .2s ease-in;
	-moz-transition: all .2s ease-in;
	-webkit-transition: all .2s ease-in;
}

.service1-item > img:hover {
	cursor: pointer;
	opacity: 1;
}

.service-image-left {
	padding-right: 50px;
}

.service-image-right {
	padding-left: 50px;
}

.service-image-left > center > img,.service-image-right > center > img{
	max-height: 155px;
}
#radar-chart {
    width: 100%;
    height: 280px;
    /*background-color: #30303d; color: #fff;*/
}
.detail-box {
  background-color: #e3e3e3;
  height: 50px;
  padding: 5% 2%;
  margin: 1% 0;
  border-radius: 5px;
}
@media (max-width:768px) {
    .detail-box {
      font-size: 1.2rem;
    }
}
.btn-group > .btn:first-child {
  margin-bottom: 2%;
  margin-left: 0;
  margin-top: 2%;
}
/*Detail table*/
@media (min-width: 768px) and (max-width:992px) {
    .container {
		width: initial;
        padding-left: 2em;
        padding-right: 2em;        
	}
}

/* --- Plans ---------------------------- */

.my_planHeader {
    text-align: center;
    color: white;
    padding-top:0.2em;
    padding-bottom:0.2em;
}
.my_planTitle {
    font-size:2em;
    font-weight: bold;
}
.my_planPrice {
    font-size:1.4em;
    font-weight: bold;    
}
.my_planDuration {
    margin-top: -0.6em;
}

@media (max-width: 768px) {
    .my_planTitle {
        font-size:small;
    }    
}

/* --- Features ------------------------- */

.my_feature {
    line-height:2.8em;   
}

@media (max-width: 768px) {
    .my_feature {
        text-align: center
    }
 }

.my_featureRow {
    margin-top: 0;
    margin-bottom: 0;
    border: 0em solid rgb(163, 163, 163);
}    

/* --- Plan 1 --------------------------- */
.my_plan1 {
    background: rgb(224,234,242);
}

.my_planHeader.my_plan1 a {
    background: rgb(72, 109, 139);
    color:white;
}

.my_planHeader.my_plan1 {
    background: rgb(105, 153, 193);
    border-bottom: thick solid rgb(72, 109, 139);
}

/* --- Plan 2 --------------------------- */
.my_plan2 {
    background: rgb(230,235,218);
}

.my_planHeader.my_plan2 a {
    background: rgb(108, 131, 62);
    color:white;
}

.my_planHeader.my_plan2 {
    background: rgb(134, 162, 77);
    border-bottom: thick solid rgb(108, 131, 62);
}

/* --- Plan 3 --------------------------- */
.my_plan3 {
    background: rgb(254,235,212);
}

.my_planHeader.my_plan3 a {
    background: rgb(199, 127, 40);
    color:white;
}

.my_planHeader.my_plan3 {
    background: rgb(253, 161, 49);
    border-bottom: thick solid rgb(199, 127, 40);
}




.my_planFeature {
    text-align: center;
    font-size: 1.4em;
    padding: 5px 0;
}

.my_planFeature i.my_check {
    color: green;
}
.rank {
	color: #fff;
	background-color: #3d3d3d;
	text-align: center;
	border-radius: 5px;
}
.detail-box-title {
	margin: -6px 0 0;
}

.input-price {
	background: #eee;
	padding: 0 20px;
	border-radius: 50px;
}
.first-quote li {
	float: left;
	margin: 3px;
	background: #eee;
	padding: 20px 17px;
	border-radius: 50px;
	width: 65px;
	height: 65px;
	text-align: center;
}
.first-quote li.selected {
	background: #e2362e;
	color: #ffffff;	
}
</style>