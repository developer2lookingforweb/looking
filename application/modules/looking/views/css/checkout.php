<!-- <link href="<?php echo base_url(); ?>css/creative.min.css" rel="stylesheet"> -->
<style>
  #formt{
    margin-top: 20px;
  }
  body, html {
	font-family: Montserrat-Regular, sans-serif!important;
	
}
.findCustomer {
	padding: 15px 0;
}
.profile{
  font-family: Montserrat-Regular, sans-serif!important;
  width: 100%!important;
  border-bottom: 1px solid #aaaaaa;
  padding: 10px 0;
}
.checkout{
    margin-top: 160px!important;
}
.row-padding{
  padding: 20px;
}
.profile {
	border-style: none none solid !important;
	border-width: medium medium 1px !important;
	border-color: #cccccc !important;
}
.validation-error {
	border-bottom: 1px solid #e2362e !important;
}

.personal-data{
  padding: 0 50px;
}
.number-spinner-input {
	border: medium none;
	padding-top: 3px;
	width: 20px;
	height: 30px;
	padding-left: 6px;
	padding-bottom: 5px;
}
.btn-group-sm > .btn, .btn-sm, 
.btn {
	font-size: 0.70rem!important;
  font-weight: 200;

}
.panel-info {
  border: 1px solid #ddd;
}
.panel-info > .panel-heading {
  background-color: #f5f5f5;
  border-bottom: 1px solid #ddd;
  color: #333;
}
.btn.focus, .btn:focus, .btn:hover{
    color: #ffffff;
    text-decoration: none;
}
.promocode {
  border: 1px solid #ccc;
  border-radius: 300px 0 0 300px;
  margin-right: -4px;
  padding: 1px 0 7px 5px;
  text-align: center;
}
.validate-promocode {
  border: 1px solid #f05f40;
  border-radius: 0 300px 300px 0;
  margin-left: -3px;
}
header{
  height: 0!important;
  min-height: 0% !important;
}
</style>