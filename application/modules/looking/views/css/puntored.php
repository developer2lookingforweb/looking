<style>
.account-panel
{
    margin-top: 20%;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.unlock-form .input-group-addon:first-child {
    border-bottom-left-radius: 0;
}
.unlock-form .input-group-btn:last-child>.btn,
.input-group .form-control:last-child {
    border-bottom-right-radius: 0;
}
.unlock-form .btn-block {
    margin-top: 5px;
}
.page-splash {
    z-index: 99999 !important;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #999;
    opacity: .9;
    pointer-events: auto;
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    -o-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition: opacity 0.3s linear;
    -moz-transition: opacity 0.3s linear;
    -o-transition: opacity 0.3s linear;
    transition: opacity 0.3s linear;
}
</style>