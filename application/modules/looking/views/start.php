<div class="row">
    <div class="col-md-12">
        <!--<a href="#" class="pasos"><img class='img-responsive' src="<?php echo base_url(); ?>images/pasos.png" alt="" /></a>--> 
        <div class="loginw-box">
            <form id="registerForm" class="form-horizontal" role="form" action="<?php echo base_url("login/signin"); ?>" method="post" >
                <div class="modal-body">
                    <div class="">
                        <div class="col-md-12">
                            <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/logo0.png" alt="" />
                        </div>
                    </div> 
                    <div class="">
                        <div class="col-md-12 text-center">
                            <h3 class='tit_loginh1' >Regístrate, es rápido<br/> <p class='tit_loginolvido1' >Crea una cuenta para mejorar tu experiencia<br/>de uso. </p> </h3>
                        </div>  
                    </div>
                    <div class="">
                        <div class="col-md-12 text-center">
                            <ul class="list-inline">
                                <li><a href="<?php echo $loginUrl ?>" class='btn btn-facebook btn-lg ga-facebook-signup' ><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;Entrar con Facebook</a></li>
                                <!--<li><a href="principal.html" class="btn btn-danger"><i class="fa fa-google-plus"></i>&nbsp;&nbsp;&nbsp;Google</a></li>-->
                            </ul>
                        </div>  
                    </div>
                    <hr>
                    <div class="">
                        <div class="col-md-12 text-center">
                            <p class="tit_loginredes"> También puedes registrarte creando un usuario y contraseña </p>
                        </div>  
                    </div> 
                    <div class="">
                        <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control profile required alpha"  placeholder="Nombre" error-msg="No es un nombre válido">
                        </div> 
                        <div class="col-md-6">
                            <input type="text" id="last_name" name="last_name" class="form-control profile required alpha"  placeholder="Apellidos" error-msg="No es un apellido válido">
                        </div> 
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <input type="email" id="email" name="email" class="form-control profile required"  placeholder="Correo" error-msg="No es un correo válido">
                        </div> 
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <input type="password"  id="password"  name="password" class="form-control profile required" placeholder="Contraseña" >
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            &nbsp;
                        </div>
                    </div>
<!--                    <div class="">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="tit_loginolvido"><input type="checkbox" id="termns" /><label for="termns" ><span ></span></label> Acepto los <a id="termns-modal" class="tit_loginolvido2" href="#"> Términos y Condiciones</a></p>
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-10 col-md-offset-1">
                            <p class='tit_loginolvido' ><input type="checkbox" id="mailMarketing" name="mailMarketing" value="1" /><label for="mailMarketing" ><span ></span></label> Deseo recibir ofertas y promociones</p>
                        </div>
                    </div>-->

                    <div class="">
                        <div class="col-md-1"></div>  
                        <div class="col-md-12 text-center">
                            <button type="submit" id="submit" class="btn btn-primary">CREAR MI CUENTA</button>
                        </div>  
                        <div class="col-md-1"></div>  
                    </div>

                    <hr class="visible-xs">

                    <div class="row visible-xs">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="text-center">
                                ¿Ya tienes una cuenta?<br />
                                <a href="<?php echo base_url("looking/login"); ?>" class="button btn_tipo4">INGRESAR AHORA</a>
                            </p>
                        </div>
                    </div>

                </div> 	
            </form>  	
        </div>
    </div> 
    <div class="col-md-3">  
    </div> 
</div> 