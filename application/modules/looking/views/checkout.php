
<div class="checkout container">
    <div class="row">
        <div class="col-xs-12 col-md-4 col-lg-4 personal-data">
        <form class="form-horizontal">
            <fieldset>
                <!-- Address form -->
                <h5>¿Ya eres cliente?</h5>
                <div class="controls row">
                    <input id="dnic" name="dnic" type="text" class="col-md-7 profile" placeholder="No de identificación" />
                    <a href="#" class="btn btn-primary btn-sm mb30 findCustomer col-md-5">consultar</a>
                </div>    
                <h5 id="formt"><?php echo lang('owner_info'); ?></h5>
                <input id="cid" name="cid" value="<?php echo $cid?>" type="hidden">
                <!-- first-name input-->
                <div class="control-group">
                    <label class="control-label"><?php echo lang('name')?></label>
                    <div class="controls row ">
                        <input  type="text" id="name" name="name" class="profile required col-xs-6 col-md-6" placeholder="<?php echo lang('name')?>" value="<?php echo $name?>"/>
                        <input  type="text" id="last_name" name="last_name" class="profile required col-xs-6 col-md-6" placeholder="<?php echo lang('last_name')?>" value="<?php echo $last_name?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('email')?></label>
                    <div class="controls row">
                        <input  type="text" id="email" name="email" class="profile required col-xs-12" placeholder="<?php echo lang('email')?>" value="<?php echo $email?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('document_type')?></label>
                    <div class="controls row">
                        <select  id="docType" name="docType" class="profile required col-xs-12" placeholder="<?php echo lang('document_type')?>" >
                            <option><?php echo lang('select_value')?></option>
                            <?php foreach ($docTypes as $key => $value) {
                                $selected = "";
                                if(isset($docType)){
                                    if($docType== $value['id']){
                                        $selected = 'selected="selected"';
                                    }
                                }
                                echo "<option value='".$value['id']."' $selected>".$value['name']."</option>";
                             }
                             ?>
                        </select>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('dni')?></label>
                    <div class="controls row">
                        <input  type="text" id="dni" name="dni" class="profile required col-xs-12" placeholder="<?php echo lang('dni')?>" value="<?php echo $dni?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('state')?></label>
                    <div class="controls row">
                        <input type="hidden" id="country" name="country" value="<?php echo $country?>"/>
                        <select id="state" name="state" placeholder="<?php echo lang('state')?>" class="profile required col-xs-12">
                            <option><?php echo lang('select_value')?></option>
                        </select>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('city')?></label>
                    <div class="controls row">
                        <select id="city" name="city" placeholder="<?php echo lang('city')?>" class="profile required col-xs-12">
                            <option><?php echo lang('select_value')?></option>
                        </select>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('shipping')?></label>
                    <div class="controls row">
                        <select id="shipping" name="shipping" placeholder="<?php echo lang('shipping')?>" class="profile required col-xs-12">
                            <option><?php echo lang('select_value')?></option>
                            <option value="1"><?php echo lang('standard')?></option>
                            <option id="premium-shipping" value="2"><?php echo lang('premium')?></option>
                            <option value="3"><?php echo lang('on_site')?></option>
                        </select>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('mobile')?></label>
                    <div class="controls row">
                        <input  type="text" id="mobile" name="mobile" class="profile required col-xs-12" placeholder="<?php echo lang('mobile')?>" value="<?php echo $mobile?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('phone')?></label>
                    <div class="controls row">
                        <input  type="text" id="phone" name="phone" class="profile required col-xs-12" placeholder="<?php echo lang('phone')?>" value="<?php echo $phone?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?php echo lang('address')?></label>
                    <div class="controls row">
                        <input  type="text" id="address" name="address" class="profile required col-xs-12" placeholder="<?php echo lang('address')?>" value="<?php echo $address?>"/>
                        <p class="help-block"></p>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
            </fieldset>
        </form>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row row-padding">
                            <div class="col-xs-6 col-md-6 col-lg-6 hidden-xs">
                                <h6><span class="glyphicon glyphicon-shopping-cart fa fa-shopping-cart "></span> Carrito de compras</h6>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <a type="button" class="btn bg-dark btn-sm btn-block" href="<?php echo base_url("outstanding") ?>">
                                    <span class="glyphicon glyphicon-share-alt fa fa-share"></span> Seguir comprando
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    <div class="panel-title row-padding">
                        <div >
                            <form action="#" method="POST" class="row">
                            <div class="col-xs-6 col-md-6 col-lg-6 ">
                                    <h6><span class="fa fa-ticket "></span> Redimir código</h6>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-6 ">
                                <div class="input-group">
                                    <input class="promocode col-xs-6" name="code" id="code" type="input" placeholder="Ingresa el código" required>
                                    <button id="validate-code" class="btn btn-primary validate-promocode col-xs-6" type="submit">Validar Código</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel-body row-padding">
                    
                    <?php foreach($cart as $item):?>
                    <div id="item-<?php echo $item['id']?>">
                        <div  class="row">
                            <?php $images = explode("|",$item["image"]);?>
                            <div class="col-xs-6 col-lg-2"><img class="img-responsive" src="<?php echo base_url()?>images/products/<?php echo $images[0]?>" width="70">
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <h6 class="product-name"><?php echo $item['name']?></h6>
                            </div>
                            <div class="col-xs-12 col-lg-7 row">
                                <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4 text-center">
                                    <?php echo lang('currency');?><?php echo number_format(($item['cost']/$item['quotes']),0)?>&nbsp;<span class="text-muted">x</span>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-3">
                                    <div class="input-group number-spinner">
                                        <span class="input-group-btn">
                                                <button class="btn bg-dark" data-dir="dwn"><span class="glyphicon glyphicon-minus fa fa-minus"></span></button>
                                        </span>
                                        <input id="quotes-<?php echo $item['id']?>"  item-id="<?php echo $item['id']?>" base-cost="<?php echo $item['cost']/$item['quotes']?>" type="text" class="number-spinner-input bg-dark" value="<?php echo $item['quotes']?>">
                                        <span class="input-group-btn">
                                                <button class="btn  bg-dark" data-dir="up"><span class="glyphicon glyphicon-plus  fa fa-plus"></span></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-5 col-md-5 col-lg-4 text-center">
                                    <?php echo lang('currency');?><span id="total-<?php echo $item['id']?>"><?php echo number_format($item['cost'],0)?></span>
                                </div>
                                <div class="col-xs-6 col-sm-2 col-md-1 col-lg-1 text-center ">
                                    <button type="button" class="btn btn-xs action-delete" item-id="<?php echo $item['id']?>">
                                        <span class="glyphicon glyphicon-trash"> </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                <?php endforeach;?>
                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h6 class="text-right"><?php echo lang('subtotal');?> 
                                <?php echo lang('currency');?><span id="total"><?php echo number_format($subtotal + $process,0)?></span>
                            </h6>
                            <h6 class="text-right"><?php echo lang('shipping');?> 
                                <?php echo lang('currency');?><span id="shipping-cost">0</span>
                            </h6>
                            <h6 class="text-right"><?php echo lang('total');?> 
                                <?php echo lang('currency');?><span id="total-w-shipping"><?php echo number_format($subtotal + $process,0)?></span>
                            </h6>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 col-lg-push-6">
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 col-lg-push-6 row-padding">
                            <form id="payu" method="post" action="<?php echo $urlPayU?>">
                            <!--<form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/">-->
                             <!--<input name="merchantId"    type="hidden"  value="563485"   >-->
                             <!--<input name="accountId"     type="hidden"  value="566035" >-->
                             <input name="merchantId"    type="hidden"  value=<?php echo $merchant?>   >
                             <input name="accountId"     type="hidden"  value="<?php echo $account?>" >
                             <input name="description"   type="hidden"  value="<?php echo $description?>"  >
                             <input id="address" name="shippingAddress" type="hidden"  value="" >
                             <input id="city" name="shippingCity" type="hidden"  value="" >
                             <input id="country" name="shippingCountry" type="hidden"  value="" >
                             <input id="reference" name="referenceCode" type="hidden"  value="<?php echo $reference?>" >
                             <input id="pamount" name="amount"        type="hidden"  value="<?php echo $amount?>"   >
                             <input name="tax"           type="hidden"  value="0"  >
                             <input name="taxReturnBase" type="hidden"  value="0" >
                             <input name="currency"      type="hidden"  value="<?php echo $currency?>" >
                             <!--<input name="signature"     type="hidden"  value="<?php // echo md5("213FV389v0d31mACBAo0CuqQjZ~563485~test0002~7000~COP") ?>"  >-->
                             <input id="signature" name="signature"     type="hidden"  value="<?php echo $signature?>"  >
                             <input name="test"          type="hidden"  value="<?php echo $test?>" >
                             <input id="buyerEmail" name="buyerEmail"    type="hidden"  value="<?php echo $buyerEmail?>" >
                             <input id="response" name="responseUrl"    type="hidden"  value="<?php echo $response?>" >
                             <input id="confirmation" name="confirmationUrl"    type="hidden"  value="<?php echo $confirmation?>" >
                             <!--<input name="Submit"   class="button"     type="submit"  value="Realizar pago" >-->
                           </form>
                           <?php $classBtn = (isset($directPayment))?"directPayment":"";?>
                            <button id="payment" type="button" class="btn btn-primary btn-block <?php echo $classBtn ?>"><?php echo lang("do_payment")?></button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
</div>

