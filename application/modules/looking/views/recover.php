<div class="loginw-box">
    <form class="form-horizontal" role="form" action="<?php echo base_url("login/authCustomer"); ?>" method="post" >
        <div class="modal-body">
            <div class="form-group">
                <div class="col-md-12">
                    <img class='img-responsive center-block' src="<?php echo base_url(""); ?>images/logo0.png" alt="" />
                </div>
            </div> 
            <hr>
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <p class="tit_loginredes"> Ingresa tu correo para recuperar tu contraseña</p>
                </div>  
            </div> 
            <div class="form-group">
                <div class="col-md-1"></div>  
                <div class="col-md-10">
                    <input type="email" id="email" name="email" class="form-control profile email "  placeholder="Correo">
                </div> 
                <div class="col-md-1"></div>  								
            </div>
            <div class="form-group">
                <div class="col-md-1"></div>  
                <div class="col-md-12 text-center">
                    <button type="submit" id="submit-recover" class="btn btn-primary">ENVIAR</button>
                </div>  
                <div class="col-md-1"></div>  
            </div> 
        </div> 	
    </form>  	
</div>