<div id="wizard-dialog" class="zoom-anim-dialog white-popup medium-popup mfp-with-anim  mfp-hide">
	<div class="fluid-container">
        <div class="wizard threesteps">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav wizard-nav-tabs nav-tabs " role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-list-alt"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-phone"></i>
                            </span>
                        </a>
                    </li>
<!--                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-usd"></i>
                            </span>
                        </a>
                    </li>-->

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <h3>¿Qué es lo más importante para ti en un celular?</h3>
                        <p>Organiza las características según tus preferencias, arrastrándolas.</p>
                        <div role="tabpanel" class="active" id="filter_0">
                            <div class="col-xs-0 col-lg-3" > &nbsp;</div>
                            <div class="col-xs-10 col-lg-6" >
                                <ul id="sortable">
                        <?php if(count($configs)>0){
                            foreach ($configs as $key => $cnfg) {?>
                                    <li id="<?php echo$cnfg->getId()?>" class="order-properties">
                                        <?php echo lang(strtolower($cnfg->getName()))?>
                                        <img src="<?php echo base_url("images/".$cnfg->getName().".png")?>" alt="<?php echo lang(strtolower($cnfg->getName()))?>"/>
                                    </li>
                        <?php
                            
                            }
                            
                            
                            }?>
                                </ul>
                            </div>
                            <div  class="col-xs-0 col-lg-3"> &nbsp;</div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary next-step unhide-sliders">Continuar</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <h3>Puntua las propiedades</h3>
                        <?php if(count($advisers)>0){
                            foreach ($advisers as $key => $advQuestion) {
                                ?><div id="prop_<?php echo $advQuestion->getCategoryConfig()->getId()?>" class="hidden propAdviser"><?php
                                ?><label><?php echo $advQuestion->getText()?></label>
                                <br/>
                                <span class="text-left"><?php echo $advQuestion->getMin() ?></span>
                                <span class="float-right"><?php echo $advQuestion->getMax() ?></span><br/>
                                <input id="adv<?php echo $advQuestion->getCategoryConfig()->getId() ?>" type="text" class="span2 dataslider fslider adviser<?php echo $advQuestion->getId() ?>" value="" 
                                       data-slider-min="<?php echo $advQuestion->getMin() ?>" 
                                       data-slider-max="<?php echo $advQuestion->getMax() ?>" 
                                       data-slider-step="<?php echo $advQuestion->getStep() ?>"
                                       <?php if($advQuestion->getRange()==1){
                                          $values = explode(",", $advQuestion->getValue()); 
                                        ?>                                       
                                       data-slider-value="[<?php echo $values[0] ?>,<?php echo $values[1] ?>]"
                                       <?php }else{?>
                                       data-slider-value="[<?php echo $advQuestion->getValue() ?>]"
                                       <?php }?>

                                       />
                                <br/><?php
                            
                                ?></div ><?php
                            }
                        }
                        ?>
                        <br/>        
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Anterior</button></li>
                            <li><button type="button" class="btn btn-primary next-step process-advisers">Guardar y Continuar</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3>Step 3</h3>
                        <p>This is step 3</p>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-default next-step">Skip</button></li>
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane text-center" role="tabpanel" id="complete">
                        <h3>Estamos preparando tu recomendación</h3
                        <br/>
                        <br/>
                        <img class="hide-loadgif" src="<?php echo base_url("images/overlay.png")?>" alt="cargando" />
                        <div class="hidden show-results-adv">
                            <p>Recomendación finalizada, revisa los reultados.</p>
                            <i class="fa fa-binoculars fa-4x"></i>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <button type="button" class="btn bg-primary show-results-advisers">Ver Resultados</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
</div>
</div>
<div id="filter-dialog" class="zoom-anim-dialog white-popup medium-popup mfp-with-anim  mfp-hide">
    <form class="filter-form" filter-type="<?php echo $type ?>" filter-extra="<?php echo $extra ?>">
        <div class="">
            <div class="row">
                <div class="fluid-container">
                    <div class="col-xs-6 col-sm-6 text-left">
                        <h3 class="filter-title">Ajusta tu búsqueda</h3>
                    </div>
                    <div class="col-xs-6 col-sm-6 text-right">
                        <!--<button name="modal-cancel" class="btn btn-info" data-dismiss="modal">Cancelar</button>--> 
                        <a name="modal-search" class="hidden-lg hidden-md btn btn-primary btn-close-modal" data-dismiss="modal">Ver Resultados</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-4">
                    <!-- Nav tabs -->
                    <!--<div class="card">-->
                    <ul class="nav nav-tabs  nav-pills nav-stacked" role="tablist">
                        <?php
                        foreach ($filters as $idx => $value) {
                            $active = ($idx == 0) ? "active" : "";
                            ?>


                            <li role="presentation" class="<?php echo $active ?> text-center-small">
                                <a href="#filter_<?php echo $value["id"] ?>" aria-controls="filter_<?php echo $value["id"] ?>" role="tab" data-toggle="tab">
                                    <i class="fa <?php echo $value["icon"] ?>">
                                    </i>
                                    <span class=" hidden-xs">
                                        <?php echo $value["label"] ?>
                                    </span>
                                </a>
                            </li>
                            <?php }
                        ?>
                    </ul>

                </div>
                <div class=" [ col-xs-9 ] [ col-sm-7 col-sm-pull-1 ] [ col-md-19 col-md-offset-1 ] ">
                    <!-- Tab panes -->
                    <div class="tab-content">
<!--                            <div role="tabpanel" class="tab-pane active" id="filter_0">
                                <label class="col-sm-12 col-md-12 col-xs-12">Organiza las propiedades según la importancia que tienen para ti</label>
                                <ul id="sortable">
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 1</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 2</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 3</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 4</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 5</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 6</li>
                                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 7</li>
                                </ul>

                                
                            </div>-->
<?php
foreach ($filters as $idx => $value) {
    $active = ($idx == 0) ? "active" : "";
    ?>
                            <div role="tabpanel" class="tab-pane <?php echo $active ?>" id="filter_<?php echo $value["id"] ?>">
                                <div class="container-fluid">
                                    <?php drawQuestions($idx, $value); ?>
                                </div>
                                <?php if (array_key_exists("suggestions", $value) && count($value["suggestions"]) > 0) { ?>
                                    </br>
                                    <label class="col-sm-12 col-md-12 col-xs-12">Sugerencias de selección</label>
        <?php
        foreach ($value["suggestions"] as $key => $singleSuggestion) {
            ?>
                                        <div class="col-xs-4 col-sm-4 col-md-4 suggestion-col">
                                            <a href="#" class="select-suggestion" data-value="<?php echo $singleSuggestion["value"] ?>" data-selector="<?php echo $singleSuggestion["question"] ?>">
                                                <h4 class="text-center hidden-sm hidden-xs"><?php echo $singleSuggestion["tag"] ?></h4>
                                                <img src="<?php echo base_url("images/" . $singleSuggestion["option"]) ?>" class="img-responsive"/>
                                            </a>
                                        </div>

            <?php
        }
    }
    ?>

                            </div>


<?php } ?>
                    </div>
                </div>
                <!--</div>-->
            </div>
            <div class="row">
                <div class="fluid-container">
                    <div class="col-xs-6 col-sm-6 text-left">
                    </div>
                    <div class="col-xs-6 col-sm-6 text-right">
                        <!--<button name="modal-cancel" class="btn btn-info" data-dismiss="modal">Cancelar</button>--> 
                        <a name="modal-search" class="btn btn-primary btn-close-modal" data-dismiss="modal">Ver Resultados</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<!--button and show options (grid/list)-->
<div class="well well-sm text-right hidden-xs hidden-sm result-header">
<?php foreach ($filters as $question) { ?>
        <button data-tab="tab-brand" class="btn btn-default filter-popup toggle-nav btn popup-with-zoom-anim" type="button" data-target="filter_<?php echo $question["id"] ?>" href="#filter-dialog">
            <span class="display-block " data-brands=""><i class="fa <?php echo $question["icon"] ?>"></i></span>
            <span class="display-block " data-brands-title=""><?php echo $question["label"] ?></span>
        </button>
        <!--<a  class="filter-popup toggle-nav btn btn-primary  popup-with-zoom-anim" href="#filter-dialog" id="big-sexy"><i class="fa fa-filter"></i> Necesitas ayuda <i class="fa fa-question"></i></a>-->
<?php } ?>
<?php if(isset ($category) && $category == 5) { ?>
        <button data-tab="tab-brand" class="btn btn-default filter-popup toggle-nav btn popup-with-zoom-anim btn-advisers" type="button" href="#wizard-dialog">
            <span class="display-block " data-brands=""><i class="fa fa-trophy"></i></span>
            <span class="display-block " data-brands-title="">Recomendación</span>
        </button>
<?php } ?>
    <!--<a  class="filter-popup toggle-nav btn btn-primary  popup-with-zoom-anim" href="#filter-dialog" id="big-sexy"><i class="fa fa-filter"></i> Necesitas ayuda <i class="fa fa-question"></i></a>-->
    <strong>Ver por</strong>
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>Lista</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Grilla</a>
    </div>
</div>

<!--    <div class=" ">
        <div id="filter-div" class="bg-dark col-lg-12 initiallyHidden" >
            <form class="filter-form" filter-type="<?php echo $type ?>" filter-extra="<?php echo $extra ?>">
                <h4 ><?php echo $leftboxTitle ?> </h4>
                <p><?php echo $leftboxDesc ?></p>
                <div class="bg-dark col-lg-4 col-lg-push-1">

                    <h4 ><?php echo lang("need_help") ?> </h4>
<?php
foreach ($filters as $idx => $value) {

    if (array_key_exists("outer", $value)) {
        if ($value["outer"] == "") {
            drawQuestions($idx, $value);
        }
    } else {

        drawQuestions($idx, $value);
    }
}
?>
                  <br>
                  <br>
              <input id="subscribe-newsletter" class="btn btn-default btn-sm sr-button" value="SUBSCRIBE" data-sr-id="6" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s; transition: transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s; " type="button">
                </div>
                <div class="bg-dark col-lg-4 col-lg-push-3">

                    <h4 ><?php echo lang("know_what_you_want") ?> </h4>
<?php
foreach ($filters as $idx => $value) {

    if (array_key_exists("outer", $value)) {
        if ($value["outer"] != "") {
            drawQuestions($idx, $value);
        }
    }
}
?>
                  <br>
                  <br>
              <input id="subscribe-newsletter" class="btn btn-default btn-sm sr-button" value="SUBSCRIBE" data-sr-id="6" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s; transition: transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.2s; " type="button">
                </div>
            </form>
        </div>
        <div class="bg-primary hide-filters hidden"><i class="fa fa-chevron-up fa-3x"></i></div>
    </div>-->


<div class="container-fluid no-padding main-grid">
    <!--        <a class="filter-popup toggle-nav text-center btn-default hidden-lg hidden-md popup-with-zoom-anim col-xs-10 col-xs-push-1" href="#filter-dialog" id="big-sexy">
                Ajusta tus resultados 
            </a>-->
    <button data-tab="tab-brand" class="btn btn-default filter-popup toggle-nav btn popup-with-zoom-anim hidden-lg hidden-md col-xs-10 col-xs-push-1" type="button" href="#filter-dialog">
        <span class="display-block " data-brands=""><i class="fa fa-external-link"></i></span>
        <span class="display-block " data-brands-title="">Ajusta tus resultados</span>
    </button>
    </br>
    <button data-tab="tab-brand" class="btn btn-default filter-popup toggle-nav btn popup-with-zoom-anim btn-advisers hidden-lg hidden-md  col-xs-10 col-xs-push-1" type="button" href="#wizard-dialog">
        <span class="display-block " data-brands=""><i class="fa fa-trophy"></i></span>
        <span class="display-block " data-brands-title="">Recomendación</span>
    </button>
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
        <div id="products" class="row list-group">
            <?php
            if (count($products) == 0) {
                ?><div class="col-lg-push-1 col-lg-10 text-center well"><h1>Ajusta los parámetros de tu búsqueda para obtener mejores resultados</h1></div><?php
            } else {
                foreach ($products as $idx => $item) {
                    $noImages = count(explode("|", $item->image));
                    $images = explode("|", $item->image);
                    ?>
                    <div class="card col-xs-12 col-sm-4 col-md-3 col-lg-3 ">
                        <div class="item thumbnail">
                            <div id="product-<?php echo $item->id ?>" class=" product-box">
                                <?php if ($item->offer > 0 || $item->market_price > 0) { ?>
                                <img class="offer" src="<?php echo base_url("images/offer.png")?>">
                                <?php } ?>
                                <a href="<?php echo base_url("detail/" . $item->id . "/" . str_replace(array(" ","'", ",", "?", "+", "(", ")"), "-", $item->product)) ?>"> 
                                <div id="myCarousel<?php echo $idx ?>" class="carousel slide list-group-item-heading" data-interval="false">
                                    <!-- Indicators -->
                                        <?php if ($noImages > 1) { ?>
                                        <ol class="carousel-indicators">
                                            <?php
                                            foreach ($images as $imgIdx => $image) {
                                                $active = ($image == reset($images)) ? "active" : "";
                                                ?>
                                                <li data-target="#myCarousel<?php echo $idx ?>" data-slide-to="<?php echo $imgIdx ?>" class="<?php echo $idx ?>"></li>
                                            <?php } ?>
                                        </ol>
                                        <?php } ?>
                                    <div class="carousel-inner">
                                        <?php
                                        foreach ($images as $imgIdx => $image) {
                                            $active = ($image == reset($images)) ? "active" : "";
                                            ?>
                                            <div class="item <?php echo $active ?>">
                                                <img id="img-<?php echo $item->id ?>-<?php echo $imgIdx ?>" src="<?php echo base_url(); ?>images/products/<?php echo $image ?>" class="img-responsive">
                                            </div>
                                        <?php } ?>
                                    </div>
                                        <?php if ($noImages > 1) { ?>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#myCarousel<?php echo $idx ?>" data-slide="prev">
                                            <span class="icon-prev"></span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel<?php echo $idx ?>" data-slide="next">
                                            <span class="icon-next"></span>
                                        </a>  
                                        <?php } ?>
                                </div>
                                        </a>
                                <!-- /.carousel -->

                                <div class="caption listable">
                                    <div class="row text-center">
                                        <?php if ($item->category == 9) { ?>
                                            <?php echo "Inscripción  $" . number_format($item->cost) ?>
                                        <?php } else { ?>
                                            <!--<div id="stars-existing" class="starrr" data-rating='4'></div>-->
                                        <?php } ?>
                                    </div>
                                    <h6 class="brand-name group inner list-group-item-heading">
                                        <a href="<?php echo base_url("brand/" . $item->provid . "/" . str_replace(array(" ","'", ",", "?", "+", "(", ")"), "-", $item->brand)) ?>"> 
                                            <?php echo $item->brand ?>
                                        </a>
                                    </h6>
                                    <h5 class="product-name group inner list-group-item-heading">
                                        <a href="<?php echo base_url("detail/" . $item->id . "/" . str_replace(array(" ","'", ",", "?", "+", "(", ")"), "-", $item->product)) ?>"> 
                                            <?php echo $item->product ?>
                                        </a>
                                    </h5>
                                    <div class="row">
                                        <div class="price-area">
                                            <?php if ($item->stock > 0) { 
                                                $buttonDisabled = "";
                                            ?>
                                                <?php if ($item->offer > 0) { ?>
                                                    <p class="col-xs-7 text-right price-lead">Precio</p>
                                                    <p class="col-xs-5 text-right price-lead text-danger">$&nbsp;<?php echo number_format($item->offer) ?></p>
                                                    <p class="col-xs-12 text-right">&nbsp;
                                                    <strike>$&nbsp;<?php echo number_format($item->cost) ?></strike>
                                                    </p>
                                                <?php } else { ?>
                                                        <?php if ($item->category == 9) { ?>
                                                            <p class="col-xs-12 text-right price-lead">$&nbsp;<?php echo number_format($item->alternative_cost) ?></p>
                                                        <?php } else { ?>
                                                        <?php $classOffer = ($item->market_price > 0)?"text-danger":""; ?>
<!--                                                            <p class="col-xs-7 text-right price-lead">x Cuotas Desde</p>
                                                            <p class="col-xs-5 text-right price-lead <?php echo $classOffer?>">$&nbsp;<?php echo number_format($item->cost* AuthConstants::FN_CREDITPERCENT) ?></p>-->
                                                            <p class="col-xs-7 text-right price-lead">Precio Contado</p>
                                                            <p class="col-xs-5 text-right price-lead <?php echo $classOffer?>">$&nbsp;<?php echo number_format($item->cost) ?></p>
                                                            <p class="col-xs-12 text-right over-price">&nbsp;<strike>
                                                            <?php if ($item->market_price > 0) { ?>
                                                            $&nbsp;<?php echo number_format($item->market_price) ?>
                                                            <?php } ?>
                                                            </strike></p>
                                                        <?php } ?>
                                                <?php } ?>
                                            <?php } else { 
                                                    $buttonDisabled = "disabled";
                                            ?>
                                                    <!--<p class="col-xs-12 text-right price-lead text-warning">&nbsp;</p>-->
                                                    <p class="col-xs-12 text-right price-lead text-warning">PRODUCTO AGOTADO!</p>
                                                    <p class="col-xs-12 text-right over-price">&nbsp;</p>
                                            <?php } ?>
                                        </div>
                                        <div class="row media-price-container">&nbsp;
                                            <?php if ($item->price > 0) { ?>
                                                    <p class="col-xs-7 text-right price-lead media-price">Precio promedio</p>
                                                    <p class="col-xs-5 text-right media-price price-lead">$&nbsp;<?php echo number_format($item->price) ?></p>
                                            <?php }else{ ?>
                                                    <p class="col-xs-7 text-right price-lead media-price">&nbsp;</p>
                                                    <p class="col-xs-5 text-right media-price price-lead">&nbsp;</p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-xs-12 col-md-12 text-center action-buttons">
                                            <a class="btn btn-sm bg-dark " href="<?php echo base_url("detail/" . $item->id . "/" . str_replace(array(" ","'", ",", "?", "+", "(", ")"), "-", $item->product)) ?>">Detalles</a>
                                            <?php $itemCost = ($item->offer>0)?$item->offer:$item->cost;?>
                                            <a class="add-product btn btn-sm bg-primary btn-success <?php echo $buttonDisabled?> " item-offer="<?php echo $item->offerId?>" item-id="<?php echo $item->id?>" item-cost="<?php echo $itemCost?>">
                                                <i class="fa fa-cart-plus "></i>&nbsp;Comprar
                                            </a>
                                            <?php if(in_array($item->category, explode(",", AuthConstants::FN_AVAILABLE_FINANCIAL))){?>
                                            <a class="add-product-credit btn btn-sm bg-dark btn-success " href="<?php echo base_url("credit/" . $item->id . "/") ?>">
                                                <i class="fa fa-credit-card "></i>&nbsp;x cuotas
                                            </a>
                                            <?php }?>
                                            
                                            <?php $comparisonClass = ($item->comparison) ? "comparison-active" : ""; ?>
                                            <?php $comparisonAction = ($item->comparison) ? "remove-comparison" : "add-comparison"; ?>
                                            <a class="btn btn-sm btn-warning <?php echo $comparisonClass . " " . $comparisonAction ?>"  item-id="<?php echo $item->id ?>" data-toggle="tooltip" data-placement="right" data-html="true" title="Comprar">Vs</a>
                                            <?php if ($this->session->userdata(AuthConstants::USER_ID)) { ?>
            <?php $favoriteClass = ($item->favorite) ? "favorite-active" : ""; ?>
            <?php $favoriteAction = ($item->favorite) ? "remove-favorites" : "add-favorites"; ?>
                                                <!--<a class="btn btn-sm btn-warning bg-dark <?php echo $favoriteClass . " " . $favoriteAction ?>" item-id="<?php echo $item->id ?>" data-toggle="tooltip" data-placement="left" data-html="true" title="Agrega a favoritos"><i class="fa fa-star "></i>&nbsp;favoritos</a>-->
        <?php } ?>
                                            <!--                                        <div class="caption">
                                                                                    </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    <?php } ?>
<?php } ?>
        </div>
    </div>
</div>
<footer>
    <div id="comparison" class="navbar navbar-inverse navbar-fixed-bottom hidden">
        <div class="container">
            <div class="navbar-header text-center">
                <label>Comparando (<span id="productCounter">0</span>) productos</label>
                <button type="" class="navbar-toggle" data-toggle="collapse" data-target="#footer-body"> 
                    <i class="fa fa-tasks fa-2x"></i>
                </button>
            </div>
            <div class="navbar-collapse collapse" id="footer-body">
                <ul class="nav navbar-nav text-right">
                    <li id="li1" class="photo-li"><i class="fa fa-photo fa-3x"></i>
                    </li>
                    <li id="li2" class="photo-li"><i class="fa fa-photo fa-3x"></i>
                    </li>
                    <li id="li3" class="photo-li"><i class="fa fa-photo fa-3x"></i>
                    </li>
                    <li id="li4"><a class="go-comparison btn btn-primary btn-default" href="<?php echo base_url("compare") ?>">Comparar</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>



<?php

function drawQuestions($idx, $value) {
    ?><label class="question-label" for="f<?php echo $idx ?>"><?php echo $value["question"] ?></label><?php
    if (!isset($value["type"])) {
        $value["type"] = "default";
    }
    switch ($value["type"]) {
        case "slider":
            ?>
            <br/>
            <br/>
            <span class="text-left"><?php echo reset($value["options"])["option"] ?></span>
            <span class="float-right"><?php echo end($value["options"])["option"] ?></span><br/>
            <input id="qs<?php echo $value["id"] ?>" type="text" class="span2 dataslider ffilter fslider questions<?php echo $value["id"] ?>" value="" 
                   data-slider-min="<?php echo reset($value["options"])["option"] ?>" 
                   data-slider-max="<?php echo end($value["options"])["option"] ?>" 
                   data-slider-step="<?php echo $value["step"] ?>" 
                   data-slider-value="[<?php echo reset($value["options"])["option"] ?>,<?php echo end($value["options"])["option"] ?>]"

                   />
            <br/>
            <?php foreach ($value["options"] as $opidx => $option) { ?> 
                <input id="qs<?php echo $value["id"] . "-" . $option["option"] ?>" name="questions<?php echo $value["id"] ?>" type="hidden" value="<?php echo $option["id"] ?>" />
            <?php } ?>
            <?php
            break;
        case "checkbox":
            ?>
            <?php foreach ($value["options"] as $opidx => $option) { ?> 
                <div class="container">
                    <input type="checkbox" class="ffilter fcheckbox questions<?php echo $value["id"] ?>"  name="questions<?php echo $value["id"] ?>" id="qs<?php echo $value["id"] . "-" . $option["id"] ?>" value="<?php echo $option["id"] ?>">
                    <label for="qs<?php echo $value["id"] . "-" . $option["id"] ?>"><span class="checkbox"><?php echo $option["option"] ?></span></label>
                </div>
            <?php } ?>
            <?php
            break;
        case "radio":
            ?>
            <?php foreach ($value["options"] as $opidx => $option) { ?> 
                <div class="container">
                    <input type="radio" class="ffilter fradio questions<?php echo $value["id"] ?>" name="questions<?php echo $value["id"] ?>" id="qs<?php echo $value["id"] . "-" . $option["id"] ?>" value="<?php echo $option["id"] ?>">
                    <label for="qs<?php echo $value["id"] . "-" . $option["id"] ?>"><span class="radio"><?php echo $option["option"] ?></span></label>
                </div>
            <?php } ?>
                <?php
                break;
            case "smartselect":
                ?>
            <select class="form-control research ffilter fselect questions<?php echo $value["id"] ?>" id="qs<?php echo $value["id"] ?>" name="questions<?php echo $value["id"] ?>">
                <option value="0">-Todo-</option>
            <?php foreach ($value["options"] as $opidx => $option) { ?> 
                    <option value="<?php echo $option["id"] ?>"><?php echo $option["option"] ?></option>
            <?php } ?>
            </select><?php
                break;
            default :
                ?>
            <select class="form-control filtersearch ffilter fselect questions<?php echo $value["id"] ?>" id="qs<?php echo $idx ?>" name="questions<?php echo $value["id"] ?>">
                <option value="">-Todo-</option>
            <?php foreach ($value["options"] as $opidx => $option) { ?> 
                    <option value="<?php echo $option["id"] ?>"><?php echo $option["option"] ?></option>
            <?php } ?>
            </select><?php
            break;
    }
}
?>
