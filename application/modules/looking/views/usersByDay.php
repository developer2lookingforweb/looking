<header class="header-spacing"></header>
<div class="container-fluid bg-primary col-sm-12 categories-container">
    <div class="btn-group">
        <a class="btn  text-faded go-back" data-toggle="dropdown" href="#">
            &nbsp;
        </a>
    </div>
</div>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <div class="col-lg-12" >
            <div class="col-lg-1" >
                <label>Fecha</label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <input id="date1" type="text" class="datepicker form-control" value="<?php echo $date?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-1" >
                <label>Hasta</label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <input id="date2" type="text" class="datepicker form-control" value="<?php echo $date?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-1" >
                <label>Campaña</label>
            </div>
            <div class="col-lg-2" >
                <div class="input-group date">
                    <select id="campaigns" class=" form-control" >
                        <option value="0"><?php echo lang("default_select")?></option>
                        <?php foreach ($campaigns as $aCampaign){?>
                        <option value="<?php echo $aCampaign->getId()?>"><?php echo $aCampaign->getName()?></option>
                        <?php }?>
                    </select>
                </div>
                
            </div>
            <div class="col-lg-2" >
                <label>Registros Encontrados</label>
            </div>
            <div class="col-lg-1" >
                <div class="records-found" >
                    <?php echo count($users) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<div class="container">
    <div class="col-lg-12" >
        <a id="download" href="#">Descargar Archivo</a>
    </div>
</div>
<br/>
<br/>
<div class="row">
    <div class="col-lg-12" >

        <div class="container product-detail">
            <div class="row">

                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Correo</th>
                                <th>Fecha Creación</th>
                                <th>Facebook Id</th>
                                <th>Campaña</th>
                            </tr>
                        </thead>
                        <tbody id="results">
                            <?php foreach ($users as $row) { ?>
                                <tr>
                                    <td><?php echo $row["id"] ?></td>
                                    <td><?php echo $row["name"] ?></td>
                                    <td><?php echo $row["last_name"] ?></td>
                                    <td><?php echo $row["email"] ?></td>
                                    <td><?php echo $row["creationDate"] ?></td>
                                    <td><?php echo $row["facebookId"] ?></td>
                                    <td><?php echo $row["campaign"] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>