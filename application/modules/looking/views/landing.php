<div id="dg_termns" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;padding-top: 5%;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div id="" class="col-md-12" style="margin-top:-4%; background: #FFFFFF; border-radius: 5px;padding: 0 31px;">
                        <div class="team-member modern">
                            <div  class="member-info-det">
                                <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">
                                    
                                </div>
                            </div> 
                            <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                            </div>
                            
                        </div> 
                    </div> 

                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- Menu -->
<nav id="menu">
         <ul class="sidebar-nav">
    <!--<a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>-->
    <li class="sidebar-brand">
        <a href="#top"  onclick = $("#menu-close").click(); > <img class='img-responsive' src="<?php echo base_url()?>images/logo3.png" alt="" /> </a>
    </li>
</ul>
        <br />
        <br />
        <br />
        <ul class="links ">
                <li  >
                    <div class="right-inner-addon">
                        <i id="opc_buscar_ico" class="fa fa-search" aria-hidden="true"></i> 
                        <input type="search"
                               class="form-control input_lin2" 
                               placeholder="Buscar lo que quieras..." 
                               id='opc_buscar_off_m'
                               />
                    </div>
                    
                </li>
                <li  ><a class='hvr-wobble-vertical' href="<?php echo base_url("looking/")?>">INICIO </a></li>
                <li  ><a class='hvr-wobble-vertical' href="<?php echo base_url("looking/start")?>">EMPIEZA AHORA</a></li>
                <li  ><a class='hvr-wobble-vertical' href="<?php echo base_url("looking/login")?>">INGRESAR</a></li>
                <li  ><a class='hvr-wobble-vertical' href="<?php echo base_url("blogs/entries"); ?>"> BLOG</a></li>
                <li  ><a class='hvr-wobble-vertical' href="<?php echo base_url("contact-us"); ?>">CONTACTENOS</a></li>
        </ul>
        <br />
        <br />
        <br />
<ul class="menu_social2">
    <li><a href="https://www.youtube.com/channel/UCIbBPueeBbenvN6_LGCRCNQ"><img class='lin_social' style='width: 32px;height:32px; ' src="<?php echo base_url()?>images/youtube.png" alt="" /> </a></li>
    <li><a href="https://twitter.com/lookingforweb"><img class='lin_social' style='width: 32px;height:32px; ' src="<?php echo base_url()?>images/twitter.png" alt="" /></a></li>
    <li><a href="https://www.facebook.com/LookingForWeb"><img class='lin_social' style='width: 32px;height:32px; ' src="<?php echo base_url()?>images/facebook.png" alt="" /></a></li>
    <li><a href="https://plus.google.com/u/4/b/112950144734980604096/112950144734980604096"><img class='lin_social' style='width: 32px;height:32px; ' src="<?php echo base_url()?>images/googleplus.png" alt="" /></a></li>
</ul>
        <ul class="menu_social2">
           <div class="footer-content text-center">
     <p>Looking For Web All Rights Reserved<br />Copyright &copy; 2016</p>
   </div>
        </ul>
</nav>

<!-- Banner -->
<!--<section id="banner">
        <div class="content">
                <h1 class='tit_1' >BUSCA + COMPARA + COMPRA</h1>
                <p class='tit_2' >Filtra tu categoría, compara las mejores opciones del mercado actual y compra.<br />Es el momento de comprar de forma inteligente. ¡Empieza Ahora! </p>
        </div>
</section>-->

<style>
    carousel,
.item,
.active {
    height: 100%;
}

.carousel-inner {
    height: 100%;
}

/* Background images are set within the HTML using inline CSS, not here */

.fill {
    width: 100%;
    height: 100%;
    background-position: center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
}
.carousel-fade {
    .carousel-inner {
        .item {
            transition-property: opacity;
        }
        
        .item,
        .active.left,
        .active.right {
            opacity: 0;
        }

        .active,
        .next.left,
        .prev.right {
            opacity: 1;
        }

        .next,
        .prev,
        .active.left,
        .active.right {
            left: 0;
            transform: translate3d(0, 0, 0);
        }
    }

    .carousel-control {
        z-index: 2;
    }
}

</style>
<!-- Full Page Image Background Carousel Header -->
<!--<section id="myCarousel" class="carousel slide carousel-fade" style="height: 580px;">
         Indicators 
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

         Wrapper for Slides 
        <div class="carousel-inner">
            <div class="item active">
                 Set the first background image using inline CSS below. 
                <div class="fill" style="background-image:url('https://k33.kn3.net/taringa/3/2/E/6/4/8/KonahsFvZ/B18.jpg');">
                </div>
                <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                 Set the second background image using inline CSS below. 
                <div class="fill" style="background-image:url('http://definicionde.hugex.net/wp-content/uploads/2015/07/e1001e86903d5fccba2a7e83a0547bd4.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                 Set the third background image using inline CSS below. 
                <div class="fill" style="background-image:url('http://3.bp.blogspot.com/-eY4XOSGwfzQ/Tz1ZkSv67VI/AAAAAAAAwpA/boT_yBzM8Tk/s1600/paisajes-naturales---106.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>
        
         Controls 
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </section>-->
<div id="myCarousel" class="carousel slide" data-ride="carousel" > 
  <!-- Indicators -->
  
  <ol class="carousel-indicators">
  <?php 
    if(count($banners)> 1){
    foreach ($banners as $idx => $value) {
        $active = ($idx == 0 )?"active":"";
  ?>
    <li data-target="#myCarousel" data-slide-to="0" class="<?=$active?>"></li>
  <?php 
    }
    }
  ?>
  </ol>
  <div class="carousel-inner">
  <?php 
    foreach ($banners as $idx => $value) {
        $active = ($idx == 0 )?"active":"";
  ?>
    <div class="item <?=$active?>"> 
        <?php if($value->getLink()!= "" && $value->getLink()!= "#") {?>
        <a href="<?= $value->getLink() ?>">
            <?php 
            if($value->getIsVideo() == 0) {?>
                <img src="<?= $value->getImage()?>" style="width:100%" alt="slide">
            <?php }else{?>
                <video width="100%" controls="false" autoplay="autoplay" loop>
                    <source src="<?= $value->getImage()?>" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            <?php }?>
        </a>
        <?php }else{?>
            <?php if($value->getIsVideo() == 0) {?>
                <img src="<?= $value->getImage()?>" style="width:100%" alt="slide">
            <?php }else{?>
                <video width="100%" controls="false" autoplay="autoplay" loop>
                    <source src="<?= $value->getImage()?>" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            <?php }?>
        <?php }?>
            <div class="container">
              <div class="carousel-caption carousel-caption-<?=$value->getAlign()?> tit_1 left">
                <?php if($value->getTitle()!= "")?><h1><?= $value->getTitle()?></h1>
                <?php if($value->getDescription()!= ""){?><p class="hidden-xs"><?= $value->getDescription()?></p><?php }?>
              </div>
            </div>
    </div>
        
  <?php 
    }
  ?>
    
  </div>
  <?php if(count($banners)> 1){?>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> 
  <?php 
    }
  ?>
  
 </div>
 <script>
    $('.carousel').carousel({
        interval: 7000 //changes the speed
    })
    </script>

<section id="opc1" class="wrapper1 bg_opc1">
    <div class="container">
                <div class="row">
                        <div class="menu_principal_1 text-center ">
                            <!--<span class='tit_8 ' ><br />Conocemos los mejores servicios y planes en diversos mercados </span>-->
                                <ul class="menu_principal_b">
                                    <?php foreach ($categories as $cat) {?>

                                    <li class="">
                                        <a id="<?php echo $cat["id"]?>" class="tit_17b category-selector hvr-float" href="<?php echo base_url("looking/principal/category/".$cat["id"]); ?>">
                                            <div class="text-center"><?php echo $cat['category']?></div>
                                            <img class='hvr-float' src="<?php echo base_url(); ?>images/categories/<?php echo $cat['image']?>" alt="<?php echo $cat['category']?>" />
                                        </a>
                                    </li>
                                    <?php }?>
<!--                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/telefonia.png" alt="" /> </a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/tecnologia.png" alt="" /></a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/educacion.png" alt="" /></a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/finanzas.png" alt="" /></a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/viajes.png" alt="" /> </a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/restaurantes.png" alt="" /></a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/hogar.png" alt="" /></a></li>
                                        <li><a href="<?php echo base_url("looking/start")?>"><img class='hvr-float' src="<?php echo base_url()?>images/menu/salud.png" alt="" /></a></li>-->
                                </ul>
                        </div>
                </div>
    </div>	
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12"><br/><br/><br/><br/><br/>   </div> 
            </div> 
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 text-center">
                          <h1 class='tit_3' >COMPARA Y ESCOGE EL MEJOR</h1>
                        </div> 
                        <div class="col-md-2"></div>
            </div> 
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 text-center">
                          <p class='tit_4' >Empieza ahora, muchas personas están comprando de manera inteligente, <br /> ahorra dinero evitando comprar algo más costoso. </p>
                        </div> 
                        <div class="col-md-2"></div>
            </div> 
                <div class="row">
                    <div class="col-md-12"><br/><br/><br/><br/> </div> 
            </div> 
            <div class="row">
                   <div class="col-md-2"></div>
                   <div class="col-md-8 text-center">
                          <a href="<?php echo base_url("looking/start")?>" class="button btn_tipo2 hvr-float-shadow">EMPIEZA AHORA</a> 
                   </div> 
                   <div class="col-md-2"></div>
            </div> 
                <div class="row">
                    <div class="col-md-12"><br/><br/><br/><br/> </div> 
            </div> 
                <div class="row">
                   <div class="col-md-2"></div>
                   <div class="col-md-8 text-center">
                          <p class='tit_9' >Regístrate completamente GRATIS para siempre.<br /> Sólo pagarás cuando compres un producto o servicio </p>
                   </div> 
                   <div class="col-md-2"></div>
            </div> 
        </div> 		
</section>

<section id="opc2" class="wrapper2 bg_opc2">
  <div class="container-fluid">
     <div class="row">
                    <div class="col-md-12"><br/><br/><br/><br/> </div> 
         </div> 
     <div class="row">
            <div class="col-md-2"></div>
                <div class="col-md-7"> 
                         <h1 class='tit_5' >EN TRES SIMPLES PASOS,<br /> <p class='tit_6' >JUNTOS ENCONTRAREMOS LO QUE NECESITAS </p> </h1>	
                </div>
                <div class="col-md-2"></div>
     </div>
    <div class="row">
           <div class="col-md-2"></div>
           <div class="col-md-7">
                <div class="media">
                           <div class="media-left">
                             <a href="<?php echo base_url("looking/start")?>"><img class="media-object" style='width: 64px;height:64px; ' src="<?php echo base_url()?>images/btn_1.png" alt="Paso1"></a>
                           </div>
                           <div class="media-body">
                                  <h4 class="media-heading tit_7">Busca</h4>
                                  <p class='tit_8' >Haz click en el producto que prefieras </p>
                           </div>
                        </div>
                        <div class="media">
                           <div class="media-left">
                             <a href="#"><img class="media-object" style='width: 64px;height:64px; ' src="<?php echo base_url()?>images/btn_2.png" alt="Paso1"></a>
                           </div>
                           <div class="media-body">
                                  <h4 class="media-heading tit_7">Compara</h4>
                                  <p class='tit_8' >Responde algunas preguntas y  haz click en calcular opciones. </p>
                           </div>
                        </div>
                        <div class="media">
                           <div class="media-left">
                             <a href="#"><img class="media-object" style='width: 64px;height:64px; ' src="<?php echo base_url()?>images/btn_3.png" alt="Paso1"></a>
                           </div>
                           <div class="media-body">
                                  <h4 class="media-heading tit_7">Compra</h4>
                                  <p class='tit_8' >Recibe descuentos y otros beneficios? </p>
                           </div>
                        </div>
<!--                        <div class="media">
                           <div class="media-left">
                             <a href="#"><img class="media-object" style='width: 64px;height:64px; ' src="<?php echo base_url()?>images/btn_4.png" alt="Paso1"></a>
                           </div>
                           <div class="media-body">
                                  <h4 class="media-heading tit_7">Compra en dos clicks</h4>
                                  <p class='tit_8' >Puedes comprar el producto/servicio que más te guste y<br />pagas con tu tarjeta débito o crédito</p>
                           </div>
                        </div>-->
                  </div>
                  <div class="col-md-3"></div>
         </div>
         <div class="row">
             <div class="col-md-12"><div class="hidden-xs"><br/><br/><br/> <br/><br/><br/></div></div> 
         </div> 

  </div>	
</section>

<section id="opc3" class="section text-center bg_opc3  hidden-xs hidden-sm ">
 <div class="container col-md-12">
    <div class="container col-md-6">
        <br/>
        <br/>
        <iframe width="350" height="200" src="https://www.youtube.com/embed/cwlptworNpM" frameborder="0" allowfullscreen></iframe>
        <br/>
        <br/>
    </div>
    <div class="container col-md-6">
        <p class="wow fadeInDown tit_8" data-wow-duration="1s">
            <br/>
            <br/>
            <br/>
            Únete a las <strong>#7308</strong> persona que han realizado búsquedas y a los <strong>#2845</strong> que han comparado diferentes opciones, elige bien! 
        </p>
    </div>
 </div>
<br/>
 <div class="container col-md-12">
    <div class="container">
       <div id="owl-demo" class="owl-carousel owl-theme">
           <div class="item">
               <div class="single-testimonial">
               <div class="col-md-4">
                   <img  class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/testimonials/1.jpg" alt="Testimoinial">
                                           <h1 class='tit_10' >MAKRO CONSTRUCCIONES </h1>
               </div>
               <div class="col-md-8">
                   <p class="wow fadeInDown tit_8" data-wow-duration="1s">
                       Looking For Web nos ayudó a buscar y comparar diferentes opciones de equipos celulares de acuerdo a nuestros gustos y preferencias!!!
                   </p>
               </div>
               </div>
           </div>
           <div class="item">
               <div class="single-testimonial">
               <div class="col-md-4">
                   <img class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/client3.png" alt="Testimoinial">
                                           <h1 class='tit_10' >DIANA PEREZ </h1>
               </div>
               <div class="col-md-8">
                   <p class="wow fadeInDown tit_8" data-wow-duration="1s">
                       Looking For Web me pareció una herramienta muy útil para decidir que equipo celular debía comprar, me asesoraron en todo lo que necesitaba, estoy muy agradecida.
                   </p>
               </div>
               </div>
           </div>
           <div class="item">
               <div class="single-testimonial">
               <div class="col-md-4">
                   <img class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/testimonials/1.jpg" alt="Testimoinial">
                                           <h1 class='tit_10' >MAKRO INMOBILIARIA </h1>
               </div>
               <div class="col-md-8">
                   <p class="wow fadeInDown tit_8" data-wow-duration="1s">
                       Looking For Web nos ayudó a buscar y comparar diferentes opciones de Equipos de Celular de acuerdo a nuestros gustos y preferencias!!!
                   </p>
               </div>
               </div>
           </div>
       </div>
    </div>
</div>  
</section>
<section id="opc4" class="bg_opc4 hide">
        <div class="bg_opc4_b">
          <div class="container-fluid">
             <div class="row">
                    <div class="col-md-12"><br /><br /><br /><br /> <br />   </div> 
             </div> 
                 <div class="row hidden-md hidden-lg visible-sm visible-xs">
                    <div class="col-md-12"><br /><br /><br /><br /> <br />   </div> 
             </div> 
            <div class="row">
                   <div class="col-md-7"></div>
                   <div class="col-md-5"> 
                     <h1 class='tit_11' >Sabes cuál es la mejor opción?<br /><p class='tit_12'> Deja que Looking For Web te ayude. </p> </h1>
                   </div>
            </div> 
                 <div class="row">
                    <div class="col-md-8"></div> 
                        <div class="col-md-4">
                           <a href="<?php echo base_url("looking/start")?>" class="button btn_tipo3 hvr-float-shadow hidden-xs hidden-sm ">EMPIEZA AHORA</a>  
                        </div>
             </div> 
                 <div class="row">
                    <div class="col-md-12"><br /><br /></div> 
             </div> 
                 <div class="row">
                    <div class="col-md-7"></div>
                    <div class="col-md-5"> 
                       <p class='tit_13   hidden-sm hidden-xs visible-md visible-lg' >Regístrate completamente GRATIS para siempre.<br /> Sólo pagarás cuando compres un producto o servicio </p>
                           <p class='tit_13_1 hidden-md hidden-lg visible-sm visible-xs' >Regístrate completamente GRATIS para siempre.<br /> Sólo pagarás cuando compres un producto o servicio </p>
                    </div>
             </div> 
 </div>
        </div>

</section>