<div id="dg_termns" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;padding-top: 5%;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div id="" class="col-md-12" style="margin-top:-4%; background: #FFFFFF; border-radius: 5px;padding: 0 31px;">
                        <div class="team-member modern">
                            <div  class="member-info-det">
                                <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">
                                    
                                </div>
                            </div> 
                            <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                            </div>
                            
                        </div> 
                    </div> 

                </div>
            </div>
            
        </div>
    </div>
</div>

<!-- Banner -->
<!--<section id="banner">
        <div class="content">
                <h1 class='tit_1' >BUSCA + COMPARA + COMPRA</h1>
                <p class='tit_2' >Filtra tu categoría, compara las mejores opciones del mercado actual y compra.<br />Es el momento de comprar de forma inteligente. ¡Empieza Ahora! </p>
        </div>
</section>-->
<style>
.navbar-default .nav > li > a, .navbar-default .nav > li > a:focus {
	text-transform: none;
}
.counter-Txt {
	text-transform: none;
}
</style>
<!--<style>
/* button */
.animated-button {
  background-color: #EE4B28;
  border-radius: 0 100px 100px 0;
  display: inline-block;
  font-size: 19px;
  /*color: white;*/
  left: 48vw;
  margin-left: -678px;
  margin-top: -228px;
  padding: 30px;
  position: fixed;
  text-align: center;
  top: 49vh;
  transition: all 1s ease 0s;
  z-index: 10000;
  font-weight: bold;
  height: 110px;
  width: 210px;
}

.animated-button:hover{
  background-color:#FFF;
  cursor:pointer;
  height: 400px;
  width: 400px;
  border-radius: 0 10px 10px 0;
  -moz-transition: all 1s ease;
    -webkit-transition: all 1s ease;
    -o-transition: all 1s ease;
    transition: all 1s ease;
}

</style>
<span class="animated-button">
    <span class="test-it">Pide una <br>  Prueba! </span>

    <div class="row delivery-form fade">
    <div class="col-md-12">
        <a href="#" class="pasos"><img class='img-responsive' src="<?php echo base_url(); ?>images/pasos.png" alt="" /></a> 
        <div class="loginw-box">
            <form class="form-horizontal" role="form" action="<?php echo base_url("login/signin"); ?>" method="post" >
                <div class="modal-body">
                    <div class="">
                        <div class="col-md-12 text-center">
                            <h3 class='tit_loginh1' >Regístrate, es rápido<br/> <p class='tit_loginolvido1' ></p> </h3>
                        </div>  
                    </div>
                    
                    <hr>
                    <div class="">
                        <div class="col-md-12 text-center">
                            <p class="tit_loginredes"> También puedes registrarte creando un usuario y contraseña </p>
                        </div>  
                    </div> 
                    <div class="">
                        <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control profile required"  placeholder="Nombre">
                        </div> 
                        <div class="col-md-6">
                            <input type="text" id="last_name" name="last_name" class="form-control profile required"  placeholder="Apellidos">
                        </div> 
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <input type="email" id="email" name="email" class="form-control profile required"  placeholder="Correo">
                        </div> 
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <input type="password"  id="password"  name="password" class="form-control profile required" placeholder="Contraseña" >
                        </div>
                    </div>
                    </br>
                    </br>
                    <div class="">
                        <div class="col-md-1"></div>  
                        <div class="col-md-12 text-center">
                            <button type="submit" id="submit" class="btn btn-primary">SOLICITAR PRUEBA</button>
                        </div>  
                        <div class="col-md-1"></div>  
                    </div>

                    <hr class="visible-xs">

                    <div class="row visible-xs">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="text-center">
                                ¿Ya tienes una cuenta?<br />
                                <a href="<?php echo base_url("looking/login"); ?>" class="button btn_tipo4">INGRESAR AHORA</a>
                            </p>
                        </div>
                    </div>

                </div> 	
            </form>  	
        </div>
    </div> 
    <div class="col-md-3">  
    </div> 
</div> -->
</span>

<section id="benefits" class="no-padding benefits hidden-xs hidden-sm">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="col-xs-12 col-sm-5 fa fa-3x fa-shield text-primary sr-icons"></i>
                <span class="col-xs-12 col-sm-7 text-primary sr-icons">Compra segura pago certificado</span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <a href="<?php echo base_url("orders/feepayment")?>">
                    <i class="col-xs-12 col-sm-5 fa fa-3x fa-money text-primary sr-icons"></i>
                    <span class="col-xs-12 col-sm-7 text-primary sr-icons">Realizar pagos parciales</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <a href="<?php echo base_url("articles/display/3/Facilidades-de-pago-a-cuotas")?>">
                    <i class="col-xs-12 col-sm-5 fa fa-3x fa-credit-card-alt text-primary sr-icons"></i>
                    <span class="col-xs-12 col-sm-7 text-primary sr-icons">Múltiples formas de pago</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="col-xs-12 col-sm-5 fa fa-3x fa-whatsapp text-primary sr-icons"></i>
                <span class="col-xs-12 col-sm-7 text-primary sr-icons">Atención al cliente 317 8045157</span>
            </div>
        </div>
</section>
<div id="" class="full-container"  >        
    
    <div id="myCarousel" class="carousel slide carousel-fade " data-ride="carousel" > 
      <!-- Indicators -->

      <ol class="carousel-indicators">
      <?php 
        if(count($banners)> 1){
        foreach ($banners as $idx => $value) {
            $active = ($idx == 0 )?"active":"";
      ?>
        <li data-target="#myCarousel" data-slide-to="<?=$idx?>" class="<?=$active?>"></li>
      <?php 
        }
        }
      ?>
      </ol>
      <div class="carousel-inner">
      <?php 
        foreach ($banners as $idx => $value) {
            $active = ($idx == 0 )?"active":"";
      ?>
        <div class="item <?=$active?>"> 
            <?php if($value->getLink()!= "" && $value->getLink()!= "#") {?>
            <a href="<?= $value->getLink() ?>">
                <?php 
                if($value->getIsVideo() == 0) {?>
                    <img src="<?= base_url("images/banners/".$value->getImage())?>" style="width:100%" alt="slide">
                <?php }else{?>
                    <video width="100%" controls="false" autoplay="autoplay" loop>
                        <source src="<?= base_url("images/banners/".$value->getImage())?>" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                <?php }?>
            </a>
            <?php }else{?>
                <?php if($value->getIsVideo() == 0) {?>
                    <img src="<?= base_url("images/banners/".$value->getImage())?>" style="width:100%" alt="<?php echo $value->getImage() ?>">
                <?php }else{?>
                    <video width="100%" controls="false" autoplay="autoplay" loop>
                        <source src="<?= base_url("images/banners/".$value->getImage())?>" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                <?php }?>
            <?php }?>
<!--                <div class="container">
                  <div class="carousel-caption carousel-caption-<?=$value->getAlign()?> tit_1 left">
                    <?php if($value->getTitle()!= "")?><h1><?= $value->getTitle()?></h1>
                    <?php if($value->getDescription()!= ""){?><p class="hidden-xs"><?= $value->getDescription()?></p><?php }?>
                    <?php if($value->getHasButton()== "1"){?>
                        <input id="" class="btn btn-start btn-primary btn-lg " value="Regístrate"   type="button">
                        <a id="" class="btn btn-start btn-primary btn-xl hidden-xs" type="button" href="<?= $value->getLink()?>">Conoce Mas</a>
                    <?php }?>
                  </div>
                </div>-->
        </div>

      <?php 
        }
      ?>

      </div>
      <?php if(count($banners)> 1){?>
      <!--<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>--> 
      <?php 
        }
      ?>

     </div>
</div>
<!--<div class="col-lg-4">
    <a href="pnllanding/">
        <p class="text-center banner-suffix">Inscripciones Abiertas</p>
        <video id="hooponopono" loop="loop" preload="true" controls poster="<?php echo base_url() ?>/pnllanding/mp4/screen.jpg" muted="" style="margin: 10px 12px 0; width: 92%; height: 100%;">
            <source src="<?php echo base_url() ?>/pnllanding/mp4/mariajose.mp4" type="video/mp4">
            <source src="<?php echo base_url() ?>/pnllanding/mp4/mariajose.ogv" type="video/ogv">
            <source src="<?php echo base_url() ?>/pnllanding/mp4/mariajose.webm" type="video/webm">
        </video>
        <iframe class="embebed-video" width="100%" height="240" 
            src="https://www.youtube.com/embed/GV-c8ChW6YU?autoplay=1&controls=0&autohide=1&mute=1&version=3&loop=1&rel=0&playlist=GV-c8ChW6YU"  
            allowfullscreen>                
        </iframe>

    </a>
    <script>
        var vid = document.getElementById("hooponopono"); 
        function playVid(){
            vid.play(); 
        }
        setTimeout(playVid(),500);
    </script>
</div>-->
<!-- <section id="m-benefits" class="no-padding benefits hidden-lg hidden-md">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="col-xs-12 col-sm-5 fa fa-3x fa-shield text-primary sr-icons"></i>
                <span class="col-xs-12 col-sm-7 text-primary sr-icons">Compra segura pago certificado</span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="col-xs-12 col-sm-5 fa fa-3x fa-truck text-primary sr-icons"></i>
                <span class="col-xs-12 col-sm-7 text-primary sr-icons">Envío GRATIS en pagos de contado</span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <a href="<?php echo base_url("articles/display/3/Facilidades-de-pago-a-cuotas")?>">
                    <i class="col-xs-12 col-sm-5 fa fa-3x fa-credit-card-alt text-primary sr-icons"></i>
                    <span class="col-xs-12 col-sm-7 text-primary sr-icons">Múltiples formas de pago</span>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="col-xs-12 col-sm-5 fa fa-3x fa-whatsapp text-primary sr-icons"></i>
                <span class="col-xs-12 col-sm-7 text-primary sr-icons">Atención al cliente 305 7442722</span>
            </div>
        </div>
</section> -->
<!--<div id="" class="col-lg-12 outstanding"  >
    <h2 class="text-center">Nuestras ofertas</h2>
        <div id="myCarousel2" class="slide list-group-item-heading products-carrousel" data-interval="false">
         Indicators 
        
        <div class="carousel-inner">
        <?php foreach ($outstanding as $idx => $prd) {?>
        <?php 
            $active = ($idx == 0)?"active":"";
            $image = explode("|", $prd->image);
        ?>
                <div class="item <?php echo $active?>">
                    <div class="caption listable text-center">
                        <a href="<?php echo base_url("detail/".$prd->id."/".rawurlencode($prd->product))?>"> 
                            <img id="img-<?php echo $prd->id?>" src="<?php echo base_url(); ?>images/products/<?php echo $image[0]?>" class="img-responsive" alt="<?php echo rawurlencode($prd->product)?>">
                            <h4 class="product-name group inner list-group-item-heading">
                                    <?php echo $prd->product?>
                            </h4>
                            <p class="price-lead text-center">$&nbsp;<?php echo number_format($prd->cost)?></p>
                        </a>
                    </div>
                </div>
        <?php }?>
        </div>
        <?php if(count($outstanding) >1){?>
         Controls 
        <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
          <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel2" data-slide="next">
          <span class="icon-next"></span>
        </a>  
        <?php }?>
      </div>
</div>-->
<style>
.cat-img{
    padding: 1%;
}
</style>
<div class=""> 
    <div class="col-lg-12">
        <div class="col-lg-12 cat-img">
            <a href="https://www.i-biling.com/intelliging/#/login_alofijo">
                <img class="hidden-md hidden-lg" src="<?php echo base_url("images/cat-english-sm.jpg")?>" style="width:100%" alt="Ingles-Alofijo">
                <img class="hidden-xs hidden-sm" src="<?php echo base_url("images/cat-english-lg.jpg")?>" style="width:100%" alt="Ingles-Alofijo">
            </a>
        </div>
    </div>
  <div class="col-lg-4">
    <div class="col-lg-12 cat-img">
        <a href="<?php echo base_url("category/5/Celulares-Accesorios")?>">
        <img src="<?php echo base_url("images/cat-phone.jpg")?>" style="width:100%" alt="Celulares">
        </a>
    </div>
    <!-- <div class="col-lg-6 cat-img">
        <a href="<?php echo base_url("category/14/Audio")?>">
      <img src="<?php echo base_url("images/cat-audio.jpg")?>" style="width:100%">
        </a>
    </div>
    <div class="col-lg-6 cat-img">
        <a href="<?php echo base_url("category/12/Video")?>">
        <img src="<?php echo base_url("images/cat-video.jpg")?>" style="width:100%">
        </a>
    </div> -->
  </div>
  <div class="col-lg-4">
    <div class="col-lg-12 cat-img">
        <a href="<?php echo base_url("category/13/Tecnología-Audio-Video")?>">
        <img src="<?php echo base_url("images/cat-techno.jpg")?>" style="width:100%" alt="Tecnologia-Audio-Video">
        </a>
    </div>
    
    <div class="col-lg-12 cat-img">
        <a href="<?php echo base_url("category/19/Salud-y-belleza")?>">
        <img src="<?php echo base_url("images/cat-hb.jpg")?>" style="width:100%" alt="Salud-Belleza">
        </a>
    </div>
  </div>
  <div class="col-lg-4">
  <div class="col-lg-12 cat-img">
        <a href="<?php echo base_url("category/8/Educacion")?>">
        <img src="<?php echo base_url("images/cat-education.jpg")?>" style="width:100%" alt="Educacion">
        </a>
    </div>
  </div>  
  
</div>
<!-- <div id="" class="col-lg-12 "  >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">OFERTAS</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div id="owl1" class="owl-carousel owl-theme">
        <?php foreach ($outstanding as $idx => $prd) { ?>
            <?php
            $active = ($idx == 0) ? "active" : "";
            ?>
            <div class="item <?php echo $active ?>">

                <?php echo drawBasicCard($prd) ?>
            </div>
        <?php } ?>
    </div>
</div>
<div id="" class="col-lg-12 "  >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">PRODUCTOS MAS COMPRADOS</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div id="owl2" class="owl-carousel owl-theme">
        <?php foreach ($topSales as $idx => $prd) { ?>
            <?php
            $active = ($idx == 0) ? "active" : "";
            ?>
            <div class="item <?php echo $active ?>">
                <?php echo drawBasicCard($prd) ?>
            </div>
        <?php } ?>
    </div>
</div>
<div id="" class="col-lg-12 "  >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">ULTIMOS PRODUCTOS</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div id="owl3" class="owl-carousel owl-theme">
        <?php foreach ($lastUpdated as $idx => $prd) { ?>
            <?php
            $active = ($idx == 0) ? "active" : "";
            ?>
            <div class="item <?php echo $active ?>">
                <?php echo drawBasicCard($prd) ?>
            </div>
        <?php } ?>
    </div>
</div> -->

	
<div id="facebook" class="hidden-md hidden-lg text-center facebook-section">
    <a href="<?php echo $loginUrl ?>" class='btn btn-facebook btn-lg' ><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;Entrar con Facebook</a>
</div>
<!--<section class="no-padding" id="portfolio">
    <div class="container-fluid">
        <div class="row no-gutter ">
            <?php foreach ($categories as $cat) {?>

            <div class="col-lg-4 col-md-4 col-sm-12">
                <a id="<?php echo $cat["id"]?>" href="<?php echo base_url("category/".$cat["id"]."/".  str_replace(" ", "-", $cat['category'])); ?>--help" class="portfolio-box">
                    <img src="<?php echo base_url(); ?>images/portfolio/thumbnails/<?php echo $cat['image']?>" class="img-responsive" alt="<?php echo $cat['category']?>">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">

                            <div class="project-name">
                                <?php echo $cat['category']?>
                            </div>
                            <div class="project-name-search hidden">
                                <br/>
                                <?php if($cat['id']== "9") $cat['category'] = "Carrera" ?>
                                <input id="" class="btn  btn-primary  " value="Elige <?php echo $cat['category']?>" type="button">
                                <br/>
                                Nosotros te ayudamos
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php }?>

        </div>
    </div>
</section>-->
<?php if($viewbenefits){?>
<section id="benefits" class="no-padding">
<!--    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">ESTOS SON ALGUNOS DE NUESTROS BENEFICIOS</h2>
                <hr class="primary">
            </div>
        </div>
    </div>-->
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-clock-o text-primary sr-icons"><h4>Ahorra tiempo</h4></i>
                    
                    <p class="text-muted">Encuentra lo que necesitas en un solo lugar.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-lock text-primary sr-icons"><h4>Seguridad</h4></i>
                    
                    <p class="text-muted">Transacciones seguras y con respaldo. Paga incluso en Baloto y Efecty</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-mobile-phone text-primary sr-icons"><h4>Flexibilidad</h4></i>
                    
                    <p class="text-muted">Ingresa desde cualquier sitio y desde cualquie dispositivo.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-tasks text-primary sr-icons"><h4>Guía</h4></i>
                    
                    <p class="text-muted">Nosotros te recomendamos, si tienes dudas compara todo en el mismo sitio.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-thumbs-up text-primary sr-icons"><h4>Gestión</h4></i>
                    
                    <p class="text-muted">No te preocupas por papeleo, nosotros lo hacemos por ti.</p>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 text-center">
                <div class="service-box">
                    <i class="fa fa-2x fa-send text-primary sr-icons"><h4>Envío</h4></i>
                    
                    <p class="text-muted">Lo pides en la web y llega a tu casa o donde prefieras.</p>
                </div>
            </div>

        </div>
    </div>
</section>

<?php }?>
<section id="services" class="hidden-xs hidden-sm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">EN TRES SIMPLES PASOS,<br>JUNTOS ENCONTRAREMOS LO QUE NECESITAS </h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-search text-primary sr-icons"></i>
                        <h3>Busca</h3>
                        <p class="text-muted">Haz click en el producto que prefieras.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-tasks text-primary sr-icons"></i>
                        <h3>Compara</h3>
                        <p class="text-muted">Responde algunas preguntas y haz click en calcular opciones.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-credit-card text-primary sr-icons"></i>
                        <h3>Compra</h3>
                        <p class="text-muted">Recibe descuentos y otros beneficios.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <section class="bg-dark" id="counters">
        <div class="container">
            <h2 class="section-heading text-center">NUESTRAS CIFRAS NOS RESPALDAN</h2>
            <hr class="light">
            <div class="row" id="counter">
        	<div class="col-sm-4 counter-Txt"> + de <span class="counter-value" data-count="<?php echo $customers?>">0</span> Usuarios </div>
                <div class="col-sm-4 counter-Txt"> + de <span class="counter-value" data-count="<?php echo $products?>">0</span> Productos</div>
                <div class="col-sm-4 counter-Txt margin-bot-35"> &nbsp; <span class="counter-value" data-count="<?php echo $sales?>">0</span> Compras</div>
            </div>
        </div>
    </section>
    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">COMPARA Y ESCOGE EL MEJOR</h2>
                    <hr class="light">
                    <p class="text-faded">Empieza ahora, muchas personas están comprando de manera inteligente,
ahorra dinero evitando comprar algo más costoso.</p>
                    <!--<a href="" class="page-scroll btn btn-default btn-xl sr-button btn-start"></a>-->
                    <input id="" class="btn btn-start btn-default btn-xl sr-button" value="Empieza ahora"   type="button">
                </div>
            </div>
        </div>
    </section>
    <?php if($viewblogs){?>
    <section class="no-padding bg-dark" id="blogs">
        <div class="container-fluid">
            <div class="row no-gutter">
                <?php 
                    foreach ($blogs as $idx => $value) {
                ?>
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <a href="<?php echo base_url(); ?>blogs/display/<?php echo $value->getId()?>/<?php echo $value->getAlias()?>" class="portfolio-box">
                        <img src="<?php echo base_url(); ?>images/gallery/<?php echo $value->getImage()?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                
                                <div class="project-name">
                                    <?php echo $value->getTitle()?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php 
                    }
                ?>
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <a href="<?php echo base_url(); ?>blogs/entries" class="portfolio-box">
                        <img src="<?php echo base_url(); ?>images/gallery/blog-blogs.png" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                
                                <div class="project-name">
                                    Ver todas las entradas
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    

    <?php }?>
    <?php if($viewnewsletter){?>
	
    <aside class="bg-dark">
        <div class="container text-center">
			<div class="newsletter">
				<h3>Newsletter</h3>
				<p>SUSCRÍBETE AL BOLETÍN DE NOTICIAS Y DESCUENTOS.</p>
                                <form class="col-lg-4 col-lg-offset-4">
                                    <input id="csrf" type="hidden" value="<?php echo $hash?>" name="<?php echo $csrf ?>">
                                    <input id="email-newsletter" type="text" value="" class="form-control input_lin2" size="50" placeholder="nombre@dominio.com">
				  <br>
				  <br>
                                  <input id="subscribe-newsletter" type="button" class="btn btn-default btn-xl sr-button" value="SUBSCRIBE">
				</form>
			</div>
        </div>
    </aside>
    
    <?php }?>
    <?php if($viewtestimonials){?>
    <section id="testimonials" class="bg-primary no-padding">    				
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-md-8">
                    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item">
                                <div class="profile-circle" >
                                    <img  class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/testimonials/1.jpg" alt="Testimoinial">
                                </div>
                                <blockquote>
                                    <h3 >MAKRO CONSTRUCCIONES </h3>
                                    <p>Looking For Web nos ayudó a buscar y comparar diferentes opciones de equipos celulares de acuerdo a nuestros gustos y preferencias!!!</p>
                                </blockquote>	
                            </div>
                            <div class="item">
                                <div class="profile-circle">
                                    <img  class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/testimonials/client2.png" alt="Testimoinial">
                                </div>
                                <blockquote>
                                    <h3 >DIANA PEREZ </h3>
                                    <p>Looking For Web me pareció una herramienta muy útil para decidir que equipo celular debía comprar, me asesoraron en todo lo que necesitaba, estoy muy agradecida.</p>
                                </blockquote>
                            </div>
                            <div class="active item">
                                <div class="profile-circle" >
                                    <img  class="center-block wow fadeInDown" data-wow-duration="1s" src="<?php echo base_url()?>images/testimonials/rondon.png" alt="Testimoinial">
                                </div>
                                <blockquote>
                                    <h3 >COLEGIO JUAN JOSE RONDON </h3>
                                    <p>Looking For Web nos ayudó a buscar y comparar diferentes opciones de computadores de escritorio, realizamos la compra el línea y el producto llegó en menos de un día.</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>							
                <div class="col-md-4 testimonial-video hidden-xs hidden-sm hidden-md ">
                    <iframe width="100%" height="310" src="https://www.youtube.com/embed/cwlptworNpM" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <?php if($viewcontactus){?>
    <section id="contact" class="bg-dark no-padding">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 hidden-xs hidden-sm hidden-md col-sm-12 col-xs-12 text-center">
                    <div id="map"></div>
                    <style>
                     #map {
                       width: 100%;
                       height: 622px;
                       background-color: grey;
                     }
                   </style>
                   <script>
                     function initMap() {
                       var uluru = {lat: 4.703202, lng: -74.043651};
                       var map = new google.maps.Map(document.getElementById('map'), {
                         zoom: 16,
                         center: uluru,
                         scrollwheel: false
                       });
                       var marker = new google.maps.Marker({
                         position: uluru,
                         map: map
                       });
                     }
                   </script>
                   <script async defer
                   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcIid-c1snnPHBFS2zBwMN3JrUkm8FVHw&callback=initMap&sensor=false">
                   </script>
                </div>
                <div class="col-lg-8 col-sm-12 col-xs-12 text-center">
                    <div class="col-lg-8 col-lg-offset-2 text-center top">
                        <h2 class="section-heading">Comunícate con nosotros</h2>
                        <hr class="primary">
                        <!--<p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>-->
                    </div>
                    <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                        <i class="fa fa-map-marker fa-3x sr-contact"></i>
                        <p>Cra 15a # 124 - 75</p>
                    </div>
                    <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                        <i class="fa fa-phone fa-3x sr-contact"></i>
                        <p>317 8045157  -  6202066 Ext 113 </p>
                    </div>
                    <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                        <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                        <p><a href="mailto:sales@lookingforweb.com">sales@lookingforweb.com</a></p>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 center "></br>
                        <h2 class="text-center">Escríbenos</h2>    
                        <form class="form-horizontal" role="form" method="post" action="<?=  base_url("messages/persistUser")?>">
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label"><?=  lang("name")?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombres y  apellidos" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label"><?=  lang("email")?></label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="ejemplo@dominio.com" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-3 control-label"><?=  lang("subject")?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-3 control-label"><?=  lang("message")?></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="4" name="message" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2 text-center">
                                <button id="submit" name="submit" type="submit" value="Send" class="btn btn-default btn-xl sr-button">Enviar</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php }?>
    <?php 

                function drawBasicCard($prd) {
                     $image = explode("|", $prd->image); 
                    ?>
                    <div class="deal ">

                        <a href="<?php echo base_url("detail/" . $prd->id . "/" . rawurlencode($prd->product)) ?>"> 
                            <div class="deal-img ">
                                <img id="img-<?php echo $prd->id ?>" src="<?php echo base_url(); ?>images/products/<?php echo $image[0] ?>" class="img-responsive" alt="<?php echo rawurlencode($prd->product) ?>">
                            </div>
                            <div class="deal-caption ">
                            <h5 class="product-name group inner list-group-item-heading">
                                <?php echo $prd->product ?>
                            </h5>
                            <h6 class="brand-name ">
                                <?php echo $prd->brand ?>
                            </h6>
                            <?php if($prd->price > 0){ ?>
                            <p class="price-lead-strike text-right"><strike>$&nbsp;<?php echo number_format($prd->price) ?></strike></p>
                            <?php } ?>
                            <p class="price-lead text-right">$&nbsp;<?php echo number_format($prd->cost) ?></p>
                            </div>
                        </a>
                    </div>
                        <?php
                }
    ?>