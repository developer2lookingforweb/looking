<?php if($conversion){?>
<?php if($this->config->item("test_enviroment")==false){ ?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '201273390278772'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=201273390278772&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<?php }?>
<?php }?>
<div id="dg_detalle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div id="" class="col-md-12" style="margin-top:-17%;">
                        <div class="team-member modern">
                            <a href="#"> 
                                <div class="member-photo imgevent row">
                                    <img id="detail_image" class="img-responsive center-block img_item" src="" alt="">
                                    <div class="regular-price">
                                        <div class="member-name color01">
                                            <span id="detail_price">$ 0</span> 
                                        </div>
                                    </div>
                                    <div class="promo-price">
                                        <div class="member-name color04">
                                            <span id="detail_price">$ 0</span> 
                                        </div>
                                        <div class="team-member-cross member-name color05">
                                            <span id="detail_old_price">$ 0</span> 
                                        </div>
                                    </div>
                                </div>    
                            </a>
                            <div class="member-info">
                                <div>
                                    <h3  class="tit_tarjeta_b"><a id="detail_brand" href="#"></a></h2>
                                    <h2 id="detail_name" class="tit_tarjeta_a"></h2>
                                </div>
                            </div>
                            <div id="detail_properties">
                            </div>

                            <div  class="member-info-det">
                                <div id="detail_description" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">

                                </div>
                            </div> 
                            <div class="member-info-det">
                                <div class="col-md-12">
                                    <a href="#" id="0" class="detail_add add button btn_tipo1 hvr-float-shadow" style="margin-top: 10px; padding-top: 10px;margin-bottom: 10px; padding-bottom: 10px; ">Añadir al Carrito</a>
                                </div> 
                            </div> 
                            <!--                            <div class="member-socail ">
                                                            <br>
                                                            <a href="#" id="addCompare" class="tarjeta_icon cls_add_comparar" data-id="" style="padding: 4px 4px; width:32px;height:32px;margin: 5 5px;display:inline-block; border-right: 2px solid #efefef;" onclick="comparar_agregar(this);">
                                                                <img id="img-compare" class="img-responsive center-block" src="" alt="">  
                                                            </a>
                                                            <a href="#" id="addFavorite" class="tarjeta_icon" style="padding: 4px 4px;width:32px;height:32px;margin: 5 5px; display:inline-block; border-right: 2px solid #efefef;">
                                                                <img id="img-favorite" class="img-responsive center-block" src="" alt="">  
                                                            </a>
                                                        </div>-->
                        </div> 
                    </div> 

                </div>
            </div>

        </div>
    </div>
</div>
<div id="dg_error" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"
     style="z-index: 100010!important;padding-top: 15%;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01"
                            style="float: right;position: relative;z-index: 10;" data-dismiss="modal"
                            aria-label="Close">Cerrar
                    </button>
                    <div id="" class="col-md-12" style="margin-top:-17%;">
                        <div class="team-member modern">
                            <div class="member-info-det">
                                <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">

                                </div>
                            </div>
                            <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- Menu -->
<nav id="menu">
    <ul class="sidebar-nav">
        <!--<a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>-->
        <li class="sidebar-brand">
            <a href="#top" onclick=$("#menu-close").click();> <img class='img-responsive'
                src="<?php echo base_url() ?>images/logo3.png"
                alt=""/> </a>
        </li>
    </ul>


    <br/>
    <br/>
    <br/>
    <ul class="links ">
        <li class="">
            <div class="right-inner-addon">
                <i id="opc_buscar_ico" class="fa fa-search" aria-hidden="true"></i>
                <input type="search"
                       class="form-control input_lin2"
                       placeholder="Buscar lo que quieras..."
                       id='opc_buscar-m'
                />
            </div>
        </li>
        <li class="text-center">
            <div class="menu-icon" id='opc_destacados-m' href="#"><img class='img-responsive center-block'
                                                                       src="<?php echo base_url(); ?>images/destacados.png"
                                                                       alt=""/>
                <p class='tit_14_2 '>Destacados</p></div>
            <div class="menu-icon" id='opc_favoritos-m' href="#"><img class='img-responsive center-block'
                                                                      src="<?php echo base_url(); ?>images/favoritos.png"
                                                                      alt=""/>
                <p class='tit_14_2'>Favoritos</p></div>
        </li>

        <?php if (count($categories) > 0): ?>
            <li>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="tit_15">Categorías activas</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <ul class="links ">
                            <?php foreach ($categories as $cat) { ?>

                                <li><a id="<?php echo $cat["id"] ?>" class="tit_17 category-selector" href="#"><img
                                            class='hvr-float'
                                            src="<?php echo base_url(); ?>images/categories/<?php echo $cat['image'] ?>"
                                            alt="<?php echo $cat['category'] ?>"
                                            width="32"/> <?php echo $cat['category'] ?>  </a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </li>
        <?php endif; ?>
        <li class="">
            <a id='opc_carrito_m' href="#">
                <span class=""><?php echo count($this->session->userdata(AuthConstants::CART)) ?>&nbsp;Productos seleccionados</span>
            </a>
        </li>
        <li>
            <a id='opc_perfil_m' href="#">
                <h1 class='tit_14'>
                    <?php echo $this->session->userdata(AuthConstants::NAMES) . " " . $this->session->userdata(AuthConstants::LAST_NAMES) ?>
                    <br/>
                    <p class='tit_14_1'><?php echo $this->session->userdata(AuthConstants::EMAIL) ?></p></h1>
            </a>
        </li>
        <li><a id='opc_salir' href="<?php echo base_url("login/webLogout") ?>"><h1 class='tit_14'> CERRAR SESION </h1>
            </a></li>
    </ul>

    <div class="clearfix">&nbsp;</div>
    <ul class="menu_social2">
        <li><a href="https://www.youtube.com/channel/UCIbBPueeBbenvN6_LGCRCNQ"><img class='lin_social'
                                                                                    style='width: 32px;height:32px; '
                                                                                    src="<?php echo base_url() ?>images/youtube.png"
                                                                                    alt=""/> </a></li>
        <li><a href="https://twitter.com/lookingforweb"><img class='lin_social' style='width: 32px;height:32px; '
                                                             src="<?php echo base_url() ?>images/twitter.png"
                                                             alt=""/></a></li>
        <li><a href="https://www.facebook.com/LookingForWeb"><img class='lin_social' style='width: 32px;height:32px; '
                                                                  src="<?php echo base_url() ?>images/facebook.png"
                                                                  alt=""/></a></li>
        <li><a href="https://plus.google.com/u/4/b/112950144734980604096/112950144734980604096"><img class='lin_social'
                                                                                                     style='width: 32px;height:32px; '
                                                                                                     src="<?php echo base_url() ?>images/googleplus.png"
                                                                                                     alt=""/></a></li>
    </ul>
    <ul class="menu_social2">
        <div class="footer-content text-center">
            <p>Looking For Web All Rights Reserved<br/>Copyright &copy; 2016</p>
        </div>
    </ul>
</nav>

<div class="fondo_principal">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2" style="position: relative; ">
                <div id="menu_lp" class="col-md-2 container-fixed hidden-xs hidden-sm"
                     style="border-right: solid 1px rgba(0, 0, 0, 0.25); border-bottom: solid 1px rgba(0, 0, 0, 0.25);overflow: auto;">
                    <div class="">
                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="tit_15">Categorías</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="tit_16">Activas</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="links ">
                                    <?php foreach ($categories as $cat) { ?>
                                        <li><a id="<?php echo $cat["id"] ?>" class="tit_17 category-selector"
                                               href="#"><img class='hvr-float'
                                                             src="<?php echo base_url(); ?>images/categories/<?php echo $cat['image'] ?>"
                                                             alt="<?php echo $cat['category'] ?>"
                                                             width="32"/> <?php echo $cat['category'] ?>  </a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr style="width: 150px;">
                                <br/>
                            </div>
                        </div>
                        <!--                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="tit_16" >Próximamente</h1>
                                <ul class="links ">
                                    <?php foreach ($inactiveCategories as $cat) { ?>

                                        <li><a id="<?php echo $cat["id"] ?>" class="tit_18" href="#"><img class='hvr-float' src="<?php echo base_url(); ?>images/categories/<?php echo $cat['image'] ?>" alt="" /> <?php echo $cat['category'] ?>  </a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12">
                                <br/><br/><br/><br/><br/><br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id='contenido_' class="col-md-10"
                 style='background-color: #F6F6F6;min-height: 585px; padding-right: 0'>


            </div>
        </div>
    </div>
</div>