<header class="header-spacing"></header>
<div class="container-fluid bg-primary col-sm-12 categories-container">
    <div class="btn-group">
        <a class="btn  text-faded go-back" data-toggle="dropdown" href="#">
           Recargas a celulares
        </a>
    </div>
</div>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <h1 class="text-center">Formulario de recargas a celulares</h1>
    </div>
    <div class="row">
        <h3 class="text-center location-name"></h3>
    </div>
    <hr>
    <br>
    <br>
    <div class="row">
        <div class="col-lg-6 col-lg-push-3" >
            <div class="col-lg-2" >
                <label>Operador</label>
            </div>
            <div class="col-lg-10" >
                <div class="input-group">
                     <div class="dropdown">
                        <button id="brand-trigger" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Selecciona Operador
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu brand-down">
                            <?php foreach($brands as $oper){?>
                            <li id="<?php echo $oper?>">
                                <a href="#"><img width="32" src="<?php echo base_url("images/products/".ucfirst($oper).".jpg")?>">&nbsp;<?php echo ucfirst($oper)?></a>
                            </li>
                            <?php }?>
                        </ul>
                      </div>
                </div>
                
            </div>
            <hr>
            <div class="col-lg-2" >
                <label>Monto</label>
            </div>
            <div class="col-lg-10" >
                <div class="input-group">
                     <div class="dropdown">
                         <button id="price-trigger" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Selecciona Valor
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu price-down">
                            <?php foreach($amount as $price){?>
                            <li id="<?php echo $price?>">
                                <a href="#">$&nbsp;<?php echo number_format($price)?></a>
                            </li>
                            <?php }?>
                        </ul>
                      </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="col-lg-2" >
                <label>Número</label>
            </div>
            <div class="col-lg-10" >
                <div class="input-group">
                     <input type="text" class="form-control" name="number" id="number">
                </div>
            </div>
            <br/>
            <br/>
            <div class="col-lg-10" >
                <div class="input-group btn-zone">
                     
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<div class="page-splash">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
            
                <div class="panel panel-default account-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-lock"></span> Contraseña de desbloqueo</h3>
                    </div>
                    <div class="panel-body">
                        <div class="center-block text-center">
                            <img class="img-thumbnail img-circle" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                                 alt="">
                            <p>Usuario Autorizado</p>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                    <input type="password" id="password" class="form-control" placeholder="Password" required autofocus>
                                </div>
                                <button type="button" class="btn btn-primary btn-block">Desbloquear</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>