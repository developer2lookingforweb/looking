<?php
    $images = explode("|", $info["image"]);
    $noImages = count($images);
?>
<div class="container-fluid product-detail">
    <div class="content-wrapper">	
        <div class="item-container">	
            <div class="row">	
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                    <div id="myCarousel<?php echo $info["id"]?>" class="carousel slide list-group-item-heading" data-interval="false">
                            <!-- Indicators -->
                            <?php if($noImages >1){?>
                            <ol class="carousel-indicators">
                            <?php foreach($images as $imgIdx => $image){
                                $active = ($image == reset($images))?"active":"";
                            ?>
                              <li data-target="#myCarousel<?php echo $info["id"]?>" data-slide-to="<?php echo $imgIdx?>" class="<?php echo $info["id"]?>"></li>
                            <?php }?>
                            </ol>
                            <?php }?>
                            <div class="carousel-inner">
                                <?php foreach($images as $imgIdx => $image){
                                    $active = ($image == reset($images))?"active":"";
                                ?>
                                    <div class="item <?php echo $active?>">
                                      <img src="<?php echo base_url(); ?>images/products/<?php echo $image?>" class="img-responsive">
                                    </div>
                                <?php }?>
                            </div>
                            <?php if($noImages >1){?>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#myCarousel<?php echo $info["id"]?>" data-slide="prev">
                              <span class="icon-prev"></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel<?php echo $info["id"]?>" data-slide="next">
                              <span class="icon-next"></span>
                            </a>  
                            <?php }?>
                          </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="product-title"><h3><?php echo $info["product"]?></h3></div>
                        <div class="product-desc"><h4><?php echo $info["brand"]?></h4></div>
                        <div id="stars-existing" class="starrr" data-rating='4'></div>
                        <?php if($info["offer"]>0){?>
                        <div class="product-price">$ <?php echo number_format($info["offer"])?></div>
                            <div class="product-old-price">
                                <strike>$<?php echo number_format($info["cost"])?></strike>
                            </div>
                        <?php }else{?>
                            <?php if($info["alternative_cost"]>0){?>
                                <div class="product-price">Inscripción  :  $ <?php echo number_format($info["cost"])?></div>
                                <div class="product-price2">Matrícula :  $ <?php echo number_format($info["alternative_cost"])?></div>
                            <?php }else{?>
                                <div class="product-price">$ <?php echo number_format($info["cost"])?></div>
                            <?php }?>
                            <?php if($info["market_price"]>0){?>
                            <div class="product-old-price">
                                <strike>$<?php echo number_format($info["market_price"])?></strike>
                            </div>
                            <?php }?>
                        <?php }?>
                        <div class="">
                            <p><?php echo lang("first_disc")?><br/>
                        </div>
                        <div class="product-first-cuote">
                            <ul class="list-inline first-quote">
<!--                                <li id="10" class="locked">10%</li>
                                <li id="20" class="locked">20%</li>-->
                                <li id="50">50%</li>
                                <li id="60">60%</li>
                                <li id="70">70%</li>
                            </ul>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-12 col-lg-12">
                            <h4><?php echo lang("first_quote_amount")?></h4>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-12 col-lg-12">
                            <h4 id="first-quote-amount" > $ 0</h4>
                        </div>
                        <div class="">
                            <p><?php echo lang("quotes_disc")?><br/>
                        </div>
                                <br>
                        <div class="product-cuotes">
                            <ul class="list-inline first-quote">
                                <li id="1" >1</li>
                                <li id="2">2</li>
                                <li id="3">3</li>
                                <li id="4">4</li>
                                <li id="5">5</li>
                                <!--<li id="6" class="locked">6</li>-->
                            </ul>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-8 col-lg-8">
                            <?php $itemCost = ($info["offer"]>0)?$info["offer"]:$info["cost"];?>
                            <?php // $itemCost = ($itemCost * AuthConstants::FN_CREDITPERCENT);?>
                            <button type="button" class="calculate btn bg-primary d-none" item-offer="<?php echo $info["offerId"]?>" item-id="<?php echo $info["id"]?>" item-cost="<?php echo $itemCost?>">
                                <?php echo lang("calculate")?>
                            </button>
                        </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="projection-wrap fade">
                        <div class="product-desc"><h3><?php echo lang("quotes_projection")?></h3></div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col"><?php echo lang("date")?></th>
                                <th scope="col"><?php echo lang("value")?></th>
                                </tr>
                            </thead>
                            <tbody id="table-body">                                


                            </tbody>
                            </table>
                        <div class="row">
                            <div class="cart col-xs-12 col-sm-12 col-lg-6">
                                <?php $itemCost = ($info["offer"]>0)?$info["offer"]:$info["cost"];?>
                                <?php // $itemCost = ($itemCost * AuthConstants::FN_CREDITPERCENT);?>
                                <button type="button" class="credit-product flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" item-offer="<?php echo $info["offerId"]?>" item-id="<?php echo $info["id"]?>" item-cost="<?php echo $itemCost?>">
                                <?php echo lang("begin_credit_process")?>
                            </button>
                            </div>
                            <div class="cart col-xs-12 col-sm-12 col-lg-6">
                            <a type="button"  href="<?php echo base_url("detail/" . $info["id"] . "/" . str_replace(array(" ","'", ",", "?", "+", "(", ")"), "-", $info["product"])) ?>" class="credit-product flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" item-offer="<?php echo $info["offerId"]?>" item-id="<?php echo $info["id"]?>" item-cost="<?php echo $itemCost?>">
                                    <?php echo lang("begin_payment_process")?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div> 
        </div>
    </div>
</div>