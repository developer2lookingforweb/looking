<header id="header2">
    <nav class="right">
        <a href="<?php echo base_url("looking/login"); ?>" class="button lb_blanco hvr-pulse-shrink hidden-xs hidden-sm">Tengo una cuenta</a>
        <a href="<?php echo base_url("looking/login"); ?>" class="button btn_tipo4 hvr-float-shadow hidden-xs hidden-sm">INGRESAR AHORA</a>
    </nav>
</header>
<div id="dg_error" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;padding-top: 15%;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div id="" class="col-md-12" style="margin-top:-17%;">
                        <div class="team-member modern">
                            <div  class="member-info-det">
                                <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">
                                    
                                </div>
                            </div> 
                            <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                            </div>
                            
                        </div> 
                    </div> 

                </div>
            </div>
            
        </div>
    </div>
</div>
<div id="dg_termns" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;padding-top: 5%;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div id="" class="col-md-12" style="margin-top:-4%; background: #FFFFFF; border-radius: 5px;padding: 0 31px;">
                        <div class="team-member modern">
                            <div  class="member-info-det">
                                <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">
                                    
                                </div>
                            </div> 
                            <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                            </div>
                            
                        </div> 
                    </div> 

                </div>
            </div>
            
        </div>
    </div>
</div>
<div  class="fondo_registro">
    <div class="container">	
        <div class="row">
            <div class="col-md-12 text-center">
                <br />  <br />  <br /> 
            </div> 
        </div> 
        <div class="row">
            <div class="col-md-3">
            </div> 
            <div class="col-md-12">
                <!--<a href="#" class="pasos"><img class='img-responsive' src="<?php echo base_url(); ?>images/pasos.png" alt="" /></a>--> 
                <div class="loginw-box">
                    <form class="form-horizontal" role="form" action="<?php echo base_url("login/signinCampaign"); ?>" method="post" >
                        <input type="hidden" id="campaign" name="campaign" value="2"/>
                        <div class="col-md-12 text-center">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <h3 class='tit_loginh1' >Inscribite y gana con LookingForWeb<br/> <p class='tit_loginolvido1' >Inscríbete y podrás ganar un premio muy especial. </p> </h3>
                                </div>  
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <img class='img-responsive center-block' src="<?php echo base_url(); ?>images/navidad.png" width="350" alt="" />
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <h3 class='tit_loginh1' >Regístrate, es rápido<br/> <p class='tit_loginolvido1' >Crea una cuenta para mejorar tu experiencia<br/>de uso. </p> </h3>
                                </div>  
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-5">
                                    <input type="text" id="name" name="name" class="form-control input_lin required"  placeholder="Nombre">
                                </div> 
                                <div class="col-md-5">
                                    <input type="text" id="last_name" name="last_name" class="form-control input_lin required"  placeholder="Apellidos">
                                </div> 
                                <div class="col-md-1"></div>  								
                            </div>
                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-10">
                                    <input type="email" id="email" name="email" class="form-control input_lin email"  placeholder="Correo">
                                </div> 
                                <div class="col-md-1"></div>  								
                            </div>
                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-10">
                                    <input type="password"  id="password"  name="password" class="form-control input_lin required" placeholder="Contraseña" >
                                </div>
                                <div class="col-md-1"></div>  
                            </div>
<!--                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-10">
                                    <input type="text"  id="dni"  name="dni" class="form-control input_lin required" placeholder="Cédula" >
                                </div>
                                <div class="col-md-1"></div>  
                            </div>-->
<!--                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-10">
                                    <input type="text"  id="mobile"  name="mobile" class="form-control input_lin required" placeholder="Celular" >
                                </div>
                                <div class="col-md-1"></div>  
                            </div>-->
<!--                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-10">
                                    <input type="text"  id="address"  name="address" class="form-control input_lin required" placeholder="Dirección" >
                                </div>
                                <div class="col-md-1"></div>  
                            </div>
                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-5">
                                    <input type="hidden" id="country" name="country" value="0"/>
                                    <select class="form-control tit_tarjeta_a required" id="state" name="state">
                                    </select> 
                                </div>
                                <div class="col-md-1"></div>  
                                <div class="col-md-5">
                                    <select class="form-control tit_tarjeta_a required" id="city" name="city">
                                    </select> 
                                </div>
                            </div>-->
                            </br>
                            <div class="form-group">
                                <div class="col-md-12">Preferencias</div>  
                                </br>
                                <div class="col-md-5">
                                    <input type="radio" name="category" id="termns1" value="educacion"/><label for="termns1" >Educación<span ></span></label>
                                </div>
                                <div class="col-md-1"></div>  
                                <div class="col-md-5">
                                    <input type="radio" name="category" id="termns1" value="tecnologia"/><label for="termns1" >Tecnología<span ></span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-1">
                                    <input type="checkbox" id="termns" /><label for="termns" ><span ></span></label>
                                </div>
                                <div class="col-md-9"><p class='tit_loginolvido1' >He leido y acepto los <a id="termns-modal-" class="tit_loginolvido2" href="<?=  base_url("images/terminos.pdf")?>" target="_blank"> Términos y Condiciones</a></p> </div> 
                                <div class="col-md-1"></div>  
                            </div>
                             <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <p class='tit_loginolvido' ><input type="checkbox" id="mailMarketing" name="mailMarketing" value="1" /><label for="mailMarketing" ><span ></span></label> Deseo recibir ofertas y promociones</p>
                                </div>
                            </div>
                        </div>

                            <div class="form-group">
                                <div class="col-md-1"></div>  
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="submit" class="button btn_tipo4 hvr-shadow-radial">CREAR MI CUENTA</button>
                                </div>  
                                <div class="col-md-1"></div>  
                            </div> 
<!--                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <p class="tit_loginredes"> También puedes registrarte con tu red social favorita </p>
                                </div>  
                            </div> 
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <ul class="list-inline">
                                        <li><a href="principal.html" class='button btn_facebook hvr-shadow-radial' >Facebook</a></li>
                                        <li><a href="principal.html" class="button btn_google hvr-shadow-radial">Google</a></li>
                                    </ul>
                                </div>  
                            </div> -->

                        </div> 	
                    </form>  	
                </div>
            </div> 
            <div class="col-md-3">  
            </div> 
        </div> 
        <div class="row">
            <div class="col-md-12 text-center">
            </div> 
        </div> 
    </div> 	
</div>