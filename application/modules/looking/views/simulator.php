<?php
?>
<div class="container-fluid product-detail">
    <div class="content-wrapper">	
        <div class="item-container">	
            <div class="row">	
                <div class="col-md-3 col-lg-1 col-sm-12 col-xs-12">
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="row">	
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="product-title"><h3>Simulador Manual</h3></div>
                        <div class="product-desc"><h4>Costo</h4></div>
                                <div class="product-price">$ <?php echo form_input(array('name'=>'cost', 'id'=>'cost', 'class'=>'span3 focused input-price', 'value'=>"0"));?></div>
                        <div class="">
                            <p><?php echo lang("first_disc")?><br/>
                        </div>
                        <div class="product-first-cuote">
                            <ul class="list-inline first-quote">
                                <li id="10">10%</li>
                                <li id="20">20%</li>
                                <li id="30">30%</li>
                                <li id="40">40%</li>
                                <li id="50">50%</li>
                            </ul>
                            <ul class="list-inline first-quote">
                                <li id="60">60%</li>
                                <li id="70">70%</li>
                                <li id="80">80%</li>
                                <li id="90">90%</li>
                                <li id="100">100%</li>
                            </ul>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-12 col-lg-12">
                            <h4><?php echo lang("first_quote_amount")?></h4>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-12 col-lg-12">
                            <h4 id="first-quote-amount" > $ 0</h4>
                        </div>
                        <div class="">
                            <p><?php echo lang("quotes_disc")?><br/>
                        </div>
                                <br>
                        <div class="product-cuotes">
                            <ul class="list-inline first-quote">
                                <li id="1" >1</li>
                                <li id="2">2</li>
                                <li id="3">3</li>
                                <li id="4">4</li>
                                <li id="5">5</li>
                                <li id="6" >6</li>
                                <li id="7" >7</li>
                                <li id="8" >8</li>
                                <li id="9" >9</li>
                                <li id="10" >10</li>
                            </ul>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-8 col-lg-8">
                            <button type="button" class="calculate btn bg-primary">
                                <?php echo lang("calculate")?>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="projection-wrap fade">
                            <div class="product-desc"><h3><?php echo lang("quotes_projection")?></h3></div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col"><?php echo lang("date")?></th>
                                    <th scope="col"><?php echo lang("value")?></th>
                                  </tr>
                                </thead>
                                <tbody id="table-body">                                

                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>

            </div> 
        </div>
    </div>
</div>