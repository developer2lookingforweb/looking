<?php
    $images = explode("|", $info["image"]);
    $noImages = count($images);
    $buttonDisabled = ($info['stock'] >0)?"":"disabled";
?>
<script src="<?php echo base_url()?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/themes/dark.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/animate/animate.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/responsive/responsive.min.js"></script>

<div class="container-fluid product-detail">
    <div class="content-wrapper">	
        <div class="item-container">	
            <div class="row">	
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div id="myCarousel<?php echo $info["id"]?>" class="carousel slide list-group-item-heading" data-interval="false">
                            <!-- Indicators -->
                            <?php if($noImages >1){?>
                            <ol class="carousel-indicators">
                            <?php foreach($images as $imgIdx => $image){
                                $active = ($image == reset($images))?"active":"";
                            ?>
                              <li data-target="#myCarousel<?php echo $info["id"]?>" data-slide-to="<?php echo $imgIdx?>" class="<?php echo $info["id"]?>"></li>
                            <?php }?>
                            </ol>
                            <?php }?>
                            <div class="carousel-inner">
                                <?php foreach($images as $imgIdx => $image){
                                    $active = ($image == reset($images))?"active":"";
                                ?>
                                    <div class="item <?php echo $active?>">
                                      <img src="<?php echo base_url(); ?>images/products/<?php echo $image?>" class="img-responsive">
                                    </div>
                                <?php }?>
                            </div>
                            <?php if($noImages >1){?>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#myCarousel<?php echo $info["id"]?>" data-slide="prev">
                              <span class="icon-prev"></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel<?php echo $info["id"]?>" data-slide="next">
                              <span class="icon-next"></span>
                            </a>  
                            <?php }?>
                          </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="product-title"><h3><?php echo $info["product"]?></h3></div>
                        <div class="product-desc"><h4><?php echo $info["brand"]?></h4></div>
                        <div id="stars-existing" class="starrr" data-rating='4'></div>
                        <?php if($info["stock"]==0){?>
                            <div class="product-old-price text-warning">
                                PRODUCTO AGOTADO!
                            </div>
                        <?php }?>
                        <?php if($info["offer"]>0){?>
                            <div class="product-price">$ <?php echo number_format($info["offer"])?></div>
                            <div class="product-old-price">
                                <strike>$<?php echo number_format($info["cost"])?></strike>
                            </div>
                        <?php }else{?>
                            <?php if($info["alternative_cost"]>0){?>
                                <div class="product-price">Inscripción  :  $ <?php echo number_format($info["cost"])?></div>
                                <div class="product-price2">Matrícula :  $ <?php echo number_format($info["alternative_cost"])?></div>
                            <?php }else{?>
                                <div class="product-price">$ <?php echo number_format($info["cost"])?></div>
                            <?php }?>
                            <?php if($info["market_price"]>0){?>
                            <div class="product-old-price">
                                <strike>$<?php echo number_format($info["market_price"])?></strike>
                            </div>
                            <?php }?>
                        <?php }?>
                        <?php if($info["category"]==9){?>
                        <div class="product-disclamer">
                            <p><i class="fa fa-asterisk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El costo hace referencia a pago de contado<br/>
                                <i class="fa fa-asterisk"></i><i class="fa fa-asterisk"></i>&nbsp;&nbsp;Nosotros te apoyamos con el proceso de inscripción</p>
                        </div>
                        <?php }else{?>
                        <div class="product-disclamer">
                            <p><i class="fa fa-asterisk"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El costo hace referencia a pago de contado<br/>
                                <i class="fa fa-asterisk"></i><i class="fa fa-asterisk"></i>&nbsp;&nbsp;El tiempo de entrega estimado es de 24 Hrs</p>
                        </div>
                        <?php }?>
                        <div class="btn-group cart col-xs-12 col-sm-8 col-lg-8">
                            <?php $itemCost = ($info["offer"]>0)?$info["offer"]:$info["cost"];?>
                            <button type="button" class="add-product btn bg-primary <?php echo $buttonDisabled?>" item-offer="<?php echo $info["offerId"]?>" item-id="<?php echo $info["id"]?>" item-cost="<?php echo $itemCost?>">
                                <?php echo lang("add")?>
                            </button>
                        </div>
                        <div class="btn-group cart col-xs-12 col-sm-4 col-lg-4">
                            <a target="_blank" href="<?php echo "https://www.facebook.com/sharer.php?u=".$metas["url"]?>" type="button" class="add-product btn btn-facebook" >
                                <i class="fa fa-facebook"></i>&nbsp;Compartir
                            </a>
                       </div>
                        <?php if($this->session->userdata(AuthConstants::USER_ID)){?>
                            <?php $favoriteClass = ($info["favorite"])?"favorite-active":"";?>
                            <?php $favoriteAction = ($info["favorite"])?"remove-favorites":"add-favorites";?>
                        <div class="btn-group wishlist col-xs-12 col-sm-12 col-lg-12">
                            <button type="button" class="btn bg-dark <?php echo $favoriteClass." ".$favoriteAction?>"  item-id="<?php echo $info["id"]?>">
                                <i class="fa fa-star "></i>&nbsp;Agregar a favoritos
                            </button>
                        </div>
                        <?php }?>
                        <?php if(in_array($info["category"], explode(",", AuthConstants::FN_AVAILABLE_FINANCIAL))){?>
                        <div class="btn-group wishlist col-xs-12 col-sm-12 col-lg-12">
                             <a class="add-product-credit btn btn-sm bg-dark btn-success " href="<?php echo base_url("credit/" . $info["id"] . "/") ?>">
                                <i class="fa fa-credit-card "></i>&nbsp;x cuotas
                            </a>
                        </div>
                        <?php }?>
                    </div>
                    <?php 
                        if($info["category"]==5){?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <h4 >Calificación del equipo</h4>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <h4 class="rank"><?php echo round($generalRank,1)?></h4>
                        </div>
                        <div id="radar-chart"></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                        <?php foreach($details as $property){?>
                        <div class="col-xs-6 col-sm-3 col-lg-3">
                        <div class=" detail-box">
                            <h6 class="detail-box-title text-right"><small><?php echo lang(strtolower($property["property"]))?></small></h6>
                            <img src="<?php echo base_url("images/".$property["property"].".png")?>" />
                            <?php echo $property["value"]?>
                        </div>
                        </div>
                        <?php }?>
                        
                    </div>
                    <?php }?>
                </div>

            </div> 
        </div>
        <div class="row">		
            <div class="col-md-12 product-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs responsive">

                    <li class="active"><a href="#service-one" data-toggle="tab"><?php echo lang("description")?></a></li>
                    <li><a href="#service-two" data-toggle="tab">Información General</a></li>
                    <?php if($show_files){?>
                        <li><a href="#service-three" data-toggle="tab">Contenido Académico</a></li>
                    <?php }?>
                    <!--<li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>-->

                </ul>
                <div id="myTabContent" class="tab-content responsive">
                    <div class="tab-pane fade in active product-info" id="service-one">

                        <?php echo $info["description"]?>

                    </div>
                    <div class="tab-pane fade product-info" id="service-two">
                        <div class="container">
                        <?php foreach($details as $property){?>
                            <div class="col-lg-7 my_featureRow">
                                <div class="col-xs-12 col-sm-4 my_feature">
                                    <?php echo $property["label"]?>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 my_planFeature my_plan1">
                                            <?php echo $property["value"]?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        </div>
                    </div>
                    <div class="tab-pane fade product-info" id="service-three">
                         <?php if($show_files){?>
                            <div class="container">
                                <div class="col-lg-7 my_featureRow">
                                    <div class="col-xs-12 col-sm-4 my_feature">
                                        Contenido
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 my_planFeature my_plan1">
                                                <a class="file-popup" type="<?=$file_type?>" href="<?php echo base_url("files/".$info["id"].".".$file_type)?>">Ver Información</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<?php if(count($related) >0 ) { ?>
<div class="row">
    <div class="col-lg-12 text-center">
        <h2 class="section-heading">Te Puede Interesar</h2>
        <hr class="primary">
    </div>
    <div id="owl4" class="owl-carousel owl-theme">
            <?php foreach ($related as $idx => $prd) { ?>
            <?php
            $active = ($idx == 0) ? "active" : "";
            ?>
            <div class="item <?php echo $active ?>">
                <?php echo drawBasicCard($prd) ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<script>
      
    </script>
    <style>
        
        .owl-theme .owl-dots,.owl-theme .owl-nav{text-align:center;-webkit-tap-highlight-color:transparent}.owl-theme .owl-nav{margin-top:10px}.owl-theme .owl-nav [class*=owl-]{color:#FFF;font-size:14px;margin:5px;padding:4px 7px;background:#D6D6D6;display:inline-block;cursor:pointer;border-radius:3px}.owl-theme .owl-nav [class*=owl-]:hover{background:#869791;color:#FFF;text-decoration:none}.owl-theme .owl-nav .disabled{opacity:.5;cursor:default}.owl-theme .owl-nav.disabled+.owl-dots{margin-top:10px}.owl-theme .owl-dots .owl-dot{display:inline-block;zoom:1}.owl-theme .owl-dots .owl-dot span{width:10px;height:10px;margin:5px 7px;background:#D6D6D6;display:block;-webkit-backface-visibility:visible;transition:opacity .2s ease;border-radius:30px}.owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span{background:#869791}
    </style>
    
    <?php 

                function drawBasicCard($prd) {
                     $image = explode("|", $prd["image"]); 
                    ?>
                    <div class="deal ">

                        <a href="<?php echo base_url("detail/" . $prd["id"] . "/" . rawurlencode($prd["product"])) ?>"> 
                            <div class="deal-img ">
                                <img id="img-<?php echo $prd["id"] ?>" src="<?php echo base_url(); ?>images/products/<?php echo $image[0] ?>" class="img-responsive" alt="<?php echo rawurlencode($prd["product"]) ?>">
                            </div>
                            <div class="deal-caption ">
                            <h5 class="product-name group inner list-group-item-heading">
                                <?php echo $prd["product"] ?>
                            </h5>
                            <h6 class="brand-name ">
                                <?php echo $prd["brand"] ?>
                            </h6>
                            <?php if($prd["price"] > 0){ ?>
                            <p class="price-lead-strike text-right"><strike>$&nbsp;<?php echo number_format($prd["price"]) ?></strike></p>
                            <?php } ?>
                            <p class="price-lead text-right">$&nbsp;<?php echo number_format($prd["cost"]) ?></p>
                            </div>
                        </a>
                    </div>
                        <?php
                }
    ?>