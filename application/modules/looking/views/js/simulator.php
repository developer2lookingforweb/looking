<script type="text/javascript">
var __slice = [].slice;
var constI = (<?php echo AuthConstants::FN_INTEREST?>/100);
var product = 0;
var first = $("#cost").val();
var amount = 0;
var quotes = 1;
var partial = 0;
var total = 0;
    $(document).ready(function () {
        
        $(".product-first-cuote ul li").click(function(){
            if($("#cost").val() > 0){
                if(!$(this).hasClass("locked")){
                    $(".product-first-cuote ul li").removeClass("selected")
                    $(this).addClass("selected")
                    var total = $("#cost").val();
                    first = Math.ceil((total/100)* $(this).attr("id"));
                    amount = total - first;
                    $("#first-quote-amount").html("$&nbsp;" + numberThousand(first));
                }
            }
            
        })
        $(".product-cuotes ul li").click(function(){
            if(!$(this).hasClass("locked")){
                if(amount == 0){
                 $(this).showMessage("Cuidado","Selecciona primero la cuiota inicial")
                }else{
                    $(".product-cuotes ul li").removeClass("selected")
                    $(this).addClass("selected")
                    quotes = $(this).attr("id")
                    
                }
            }
        })
        $(".calculate").click(function(){
            $(".projection-wrap").removeClass("fade")
            partial = Math.pow((1+constI),quotes);
            total = amount*((constI*partial)/(partial-1));
            if(first == $("#cost").val()){
                quotes = 0;
            }else{
                quotes = 1;
                quotes = $(".product-cuotes ul li.selected").attr("id");
                
            }
            var date = new Date();
            var months = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Jul","Ago","Oct","Nov","Dic"];
            var html = '';
            html += "<tr>";
            html += "   <th scope='row'>Inicial</th>";
            html += "   <td>"+date.getDate()+"/"+months[date.getMonth()]+"/"+date.getFullYear()+"</td>";
            html += "   <td>$&nbsp;"+numberThousand(first)+"</td>";
            html += "</tr>";
            for(var i=1; i<= quotes; i++){
                date.setMonth(date.getMonth() + 1);
                html += "<tr>";
                html += "   <th scope='row'>"+(i)+"</th>";
                html += "   <td>"+date.getDate()+"/"+months[date.getMonth()]+"/"+date.getFullYear()+"</td>";
                html += "   <td>$&nbsp;"+numberThousand(Math.ceil(total))+"</td>";
                html += "</tr>";
            }
            $("#table-body").html(html);
        })
        $(".credit-product").click(function(){
            var obj = this;
            product = $(obj).attr("item-id");
            $.ajax({
                type: 'post',
                data: [],
                url: '<?php echo base_url("looking/register/"); ?>',
                
                success: function (data) {
                    $(obj).showMessage("Solicitud de pago a cuotas",data);
                    var sessid = "<?php echo ($this->session->userdata(AuthConstants::USER_ID)) ?>";
                    if(sessid !== ""){
                        $("#name").val("<?php echo ($this->session->userdata(AuthConstants::NAMES)) ?>")
                        $("#last_name").val("<?php echo ($this->session->userdata(AuthConstants::LAST_NAMES)) ?>")
                        $("#email").val("<?php echo ($this->session->userdata(AuthConstants::EMAIL)) ?>")
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        })
        $("body").on("keypress","#mobile",validateNumber)
        $("body").on("click","#submit-credit",function(){
            if(!$(this).hasClass("disabled")){
                var valName = $("#name").val();
                var valLastName = $("#last_name").val();
                var valEmail = $("#email").val();
                var valMobile = $("#mobile").val();
                $(this).addClass("disabled");
                if(valName !== "" && valLastName !== "" && valEmail !== "" && valMobile !== ""){
                    var submitData = {
                        product : product,
                        quotes  : quotes,
                        first   : first,
                        amount  : amount,
                        total   : Math.ceil(total),
                        name    : valName,
                        last_name    :valLastName,
                        email   : valEmail,
                        mobile  : valMobile
                    }
                    $.ajax({
                        type: 'post',
                        data: submitData,
                        url: '<?php echo base_url("looking/persistRegister/"); ?>',
                        dataType: "json",
                        success: function (data) {
                            $(this).showMessage("Solicitud de pago a cuotas","Uno de nuestros asesores se pondrá en contacto contigo lo mas pronto posible.<br><br><div class='col-lg-12 text-center'><a href='<?php echo base_url()?>' class='btn btn-primary'>Volver</a></div>");
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                        }
                    });
                }else{
                    $(this).showMessage("Verifica tus datos","Debes diligenciar todos los campos");
                }
            }
        })
        function validateNumber(event) {
            if(event.target.value.length<10){
                var key = window.event ? event.keyCode : event.which;
                if (event.keyCode === 8 || event.keyCode === 46) {
                    return true;
                } else if ( key < 48 || key > 57 ) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }

        };
        function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
        $("#contenido_").on('focusout', 'input', function () {
            required($(this))
        });
        $("#contenido_").on('focusout change', 'select', function () {
//        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate() {
            $(".required").each(function () {
                required($(this));
            });
            if ($(".validation-error")[0]) {
                return false;
            } else {
                return true;
            }
        }
        function required(obj) {
            $(obj).removeClass("validation-error")
            if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
        $(".rating").hover(function(){
            $(this).toggleClass("glyphicon-star-empty");
            $(this).toggleClass("glyphicon-star");
        });
        
    });


</script>