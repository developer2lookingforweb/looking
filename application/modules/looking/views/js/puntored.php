<script type="text/javascript">
    /*!
 * Datepicker for Bootstrap v1.6.1 (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
  

    function validateNumber(event) {
        if(event.target.value.length<10){
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode === 8 || event.keyCode === 46) {
                return true;
            } else if ( event.keyCode == 13 ) {
                $(".btn-block").trigger("click");
                return true;
            } else if ( key < 48 || key > 57 ) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
            
    };
    var brand = "";
    var amount = 0;
    var number = 0;
    var passwdok = false;
    var user = '';
    $(document).ready(function(){
        setTimeout(function(){ location.reload() },300000);
        $('#number').keypress(validateNumber);
        $('.btn-block').click(function(){
            <?php
             foreach ($passwords as $key => $pwd) {                 ?>
                         console.log('<?php echo $pwd?>')
                         console.log($("#password").val())
                         console.log($("#password").val()=='<?php echo $pwd?>')
                if($("#password").val()=='<?php echo $pwd?>'){
                    passwdok = true;
                    user = '<?php echo $users[$key] ?>';
                    $(".location-name").html(user);
                }
                 
            <?php }
            ?>
//            passwdok = true;
            if(passwdok){
                $(".page-splash").fadeOut();
                $(".btn-zone").html('<button type="button" class="btn btn-primary btn-refill">Recargar</button>');
            }else{    
                $("#password").css("border-color","red")
            }
        });
        $('.brand-down li').click(function(){
            brand = $(this).attr("id");
            $("#brand-trigger").html($(this).attr("id"));
        });
        $('.price-down li').click(function(){
            amount = $(this).attr("id")
            $("#price-trigger").html("$" + $(this).attr("id"));
        });
        $('body').on("click",".btn-refill",function(){
            number =$("#number").val();
            if(passwdok == true){
                if(brand != "" && amount >0 && number > 0){
                    var dataSubmit = {
                        brand: brand,
                        total: amount,
                        user: user,
                        number: number
                    }
                    $.ajax({
                        type: 'post',
                        data: dataSubmit,
                        url: '<?php echo base_url("looking/singleFill/"); ?>',
                        dataType: "json",
                        success: function (data) {
                            if (data.status == true) {
                                $(this).showMessage("Recargas","Recarga realizada con éxito");
                                $("#number").val("");
                                $("#brand-trigger").html("Selecciona Operador");
                                $("#price-trigger").html("Selecciona Valor");
                                number = 0;
                                brand = "";
                                amount = 0;
                            }else{
                                $(this).showMessage("Error","No se ha realizado la recarga, por favor verifica los datos")
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $(this).showMessage("Alerta","Error inesperado, por favor recarga la páagina")
                        }
                    });
                }else{
                    $(this).showMessage("Error","Verifica los datos");
                }
            }else{
                $(this).showMessage("Alerta de Seguridad","Sistema bloqueado, por favor recarga la página");
            }
        });
               
    });
    function search(){
        $.ajax({
                type: 'post',
                data: {
                    campaign: $("#campaigns").val(),
                    date1: $("#date1").val(),
                    date2: $("#date2").val()
                },
                url: '<?php echo base_url("looking/getUsersByDate/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(".records-found").html(data.users.length);
                        var html ='';
                        $.each(data.users,function(idx,row){
                            html +='    <tr>';
                            html +='        <td>'+row.id+'</td>';
                            html +='        <td>'+row.name+'</td>';
                            html +='        <td>'+row.last_name+'</td>';
                            html +='        <td>'+row.email+'</td>';
                            html +='        <td>'+row.creationDate+'</td>';
                            html +='        <td>'+row.facebookId+'</td>';
                            html +='        <td>'+row.campaign+'</td>';
                            html +='    </tr>';
                        })
                        $("#results").html(html);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
    }
    function downloadXls(){
        $.redirectPost('<?php echo base_url("looking/downloadExcel/"); ?>',
                        {
                            campaign: $("#campaigns").val(),
                            date1: $("#date1").val(),
                            date2: $("#date2").val()
                        });

//        $.ajax({
//                type: 'post',
//                data: {
//                    campaign: $("#campaigns").val(),
//                    date1: $("#date1").val(),
//                    date2: $("#date2").val()
//                },
//                url: '<?php echo base_url("looking/downloadExcel/"); ?>',
//                dataType: "json",
//                success: function (data) {
//                    var blob = new Blob([data], { type: "application/vnd.ms-excel" });
//                    alert('BLOB SIZE: ' + data.length);
//                    var URL = window.URL || window.webkitURL;
//                    var downloadUrl = URL.createObjectURL(blob);
//                    document.location = downloadUrl;
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
//                }
//            });
    }
    $.extend(
    {
        redirectPost: function(location, args)
        {
            var form = '';
            $.each( args, function( key, value ) {
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
        }
    });
</script>