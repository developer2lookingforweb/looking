<script type="text/javascript">

    $(document).ready(function () {
        <?php if(isset($error)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("No hemos podido registrarte, intenta nuevamente<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        <?php if(isset($exist)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("El usuario ya se encuentra registrado<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        
        $('#submit').click(function (e) {
            e.preventDefault();

            if ( required($("#password")) && required($("#name")) && required($("#last_name")) && validateEmail($("#email")) ) {
                if(validateTermns($("#termns"))){
                    $(this).unbind('click').click();
                }
            }else{
//                alert("invalido")
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Verifica tu datos de registro para continuar<br/>");
                $('#dg_error').modal('toggle'); 
            }
        })

         $('#termns-modal,#termns-modal2').click(function () {
                $("#dg_termns #error-title").html("Términos y condiciones");
                $("#dg_termns #error-msg").html("<?php echo lang("termns_and_conditions") ?>");
                $('#dg_termns').modal('toggle'); 
            
        });
         $('#email').focusout(function () {
            validateEmail($("#email"))
        });
        $('.required').focusout(function () {
            required($(this))
        });
        function validateTermns(input) {
            if (!input.is(':checked')) {
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Debes aceptar los términos y condiciones<br/>");
                $('#dg_error').modal('toggle'); 
                
            }
            console.log(input.is(':checked'));
            return input.is(':checked');
        }
        function validateCell(value) {
            if (!isCellphone(value)) {
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("No es un número celular válido.<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
            } else {
                return true;
            }
        }
        ;

        function validateEmail(obj) {
            if (!isEmail($(obj).val())) {
                obj.toggleClass("validation-error")
                return false;
            } else {
                $(obj).removeClass("validation-error")
                return true;
            }
        }
        ;
        
        function required(obj){
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error")
                return false;
            }else{
                $(obj).removeClass("validation-error")
                return true;
            }
        }
        
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        ;

        function isCellphone(number) {
            var regex = /^3[0-9]{9}$/;
            return regex.test(number);
        }
        ;
    });
</script>