<script type="text/javascript">

    $(document).ready(function() {
//     $(document).foundation('tab', 'reflow');
     
     <?php
        if(isset($message)){
            ?>
                $("#modal-message #modal-title").html("Transacción finalizada");
                $("#modal-message #message").html("<?php echo $message?><br/><br/>");
                $('#modal-message').foundation('reveal', 'open');
                    
            <?php
        }
     ?>
        $(".launch-terms").click( function(e){
                $("#modal-message #modal-title").html("Términos y condiciones");
                $("#modal-message #message").html("<br/><?php echo lang("termns_and_conditions") ?><br/>");
                $('#modal-message').removeClass('tiny').addClass('large').css('top', '45px');
                $('#modal-message').foundation('reveal', 'open');
            
        });
        $(".form").submit( function(e){
            var form = $(this);
            var formid = $(this).attr("id");
            var valid = true;
            $.each(form.serializeArray(),function(index,field){
                if($("#"+formid+" select[name='"+field.name+"']").hasClass("required") && $("#"+formid+" select[name='"+field.name+"']").val() == ""){
                    $(field).addClass("validation-error");
                    valid = false;
                }
           });
           if(!valid){
                e.preventDefault();
                $("#modal-message #modal-title").html("Varifica la información");
                $("#modal-message #message").html("Responde algunas preguntas para poder ayudarte<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
           }

        });
     
     $(".validation-trigger").click(function(ase){
         var form = $(this).closest("form");
         var valid = true;
         var idForm = ($(form).attr("id"))
         return false;
         $("#"+idForm).submit( function(e){
            e.preventDefault();
            $.each(form.serializeArray(),function(index,field){
                if($(field).hasClass("required") && $(field).val() == ""){
                    $(field).addClass("validation-error");
                    valid = false;
                }
           });
           if(!valid){
                $("#modal-message #modal-title").html("Varifica la información");
                $("#modal-message #message").html("Responde algunas preguntas para poder ayudarte<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
           }
        });
    });
     $(".subcategories li").click(function(){
        $("li").removeClass("active");
        $(this).addClass("active");
        var panel = $(this).find("a").attr("href");
        $(".tabs-content > .content").removeClass("active");
        $(panel).addClass("active");
     })
     $(".categories li").click(function(){
//        $("li").removeClass("active");
//        $(this).addClass("active");
        var panel = $(this).find("a").attr("href");
        $(".block-content .content").removeClass("active");
        $(panel + " .left-corner").click();
        $(panel).addClass("active");
     })
//     $("button.send").click(function(){
//         alert("ASDF")
//     });

       $('#numero').focusout(function(){
            validateCell(this.value)
        });
        $('.send-refill').click(function(e){
           e.preventDefault();
            
            if( validateEmail($("#email3").val())){
//            if(validateCell($("#numero").val()) && validateEmail($("#email3").val())){
                $(this).unbind('click').click();
            }
        }) 
       $('.email').focusout(function(){
            validateEmail(this.value)
        });
        $('.send').click(function(e){
           e.preventDefault();
            
            if(validateEmail($("#email"+$(this).attr('cat')).val()) && validateTermns($("#termns"+$(this).attr('cat')))){
                $(this).unbind('click').click();
            }
        }) 
       $("#browse").click(function(){
            if($("#name").val()!=""){
                var minutes = ($("select[name=minutos]").val()!="")?$("select[name=minutos]").val():"";
                var navigation = ($("select[name=navegacion]").val()!="")?$("select[name=navegacion]").val():"";
                var app = ($("select[name=app]").val()!="")?$("select[name=app]").val():"";
                var submitdata = {
                        "MINUTOS": minutes,
                        "NAVEGACION": navigation,
                        "APP": app,
                        "c4e429d1506c7d66d56ad1843cfe4f2c": $('input[name=c4e429d1506c7d66d56ad1843cfe4f2c').val()
                };
                $.ajax({
                    url: "<?php echo base_url();?>browser/browse/",
                    type: "post",            
                    dataType: "json",
                    data: submitdata,
                    success: function(data){ 
                        if(data.status == true){
                            $.each(data.data,function(index,value){
                                var html = '';
                                html += '<div class="large-4 small-6 columns left grid-item">';
                                html += '   <img src="../images/products/'+value.image+'.jpg">';
                                html += '   <details>'+value.description+'</details>';
                                html += '   <div class="panel">';
                                html += '       <h5>'+ value.product+'</h5>';
                                html += '       <h6 class="subheader">$'+ value.cost+'</h6>';
                                html += '   </div>';
                                html += '</div>';
                                $(".result-grid-row").append(html);
                                
                            })
                    
                            $("#question-slider").fadeOut()
                            $("#result-grid").fadeIn()
                        }else{
                            if(data.error = "no-data"){
                                $("#modal-message #message").html("No se han encontrado resultados<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                            }else{
                                $("#modal-message #message").html("Error inesperado<br/><br/>");
                                $('#modal-message').foundation('reveal', 'open');
                            }
                        }
                        
                    }
                });
            }else{
                $("#modal-message #message").html("Dinos tu nombre!");
                $('#modal-message').foundation('reveal', 'open');
            }
       });
       
       $(".result-grid-row").on('click','.grid-item',function(){
            $("#modal-message #modal-title").html("Detalles");
            $("#modal-message #message").html($(this).find('details').html()+"<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');
        })
       $("#modal-close").click(function(){
           $('#modal-message').foundation('reveal', 'close');
       })
       
       
       function validateTermns(input){
           console.log(input.is(":checked"))
        if( ! input.is(':checked') ){
            $("#modal-message #modal-title").html("Datos inválidos");
            $("#modal-message #message").html("Debes aceptar los términos y condiciones.<br/><br/>");
            $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
            $('#modal-message').foundation('reveal', 'open');
            
        }
        return input.is(':checked');
       }
       function validateCell(value){
            if(!isCellphone(value)){
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("No es un número celular válido.<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
            }else{
                return true;
            }
        };
       
       function validateEmail(value){
            if(!isEmail(value)){
                $('#modal-message').foundation('reveal', 'close');
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("Verifica tu dirección de correo para continuar<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
            }else{
                return true;
            }
        };
       
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        };
        
        function isCellphone(number) {
            var regex = /^3[0-9]{9}$/;
            return regex.test(number);
        };
    });
</script>