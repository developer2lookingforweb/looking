<script type="text/javascript">
    var a = 0;
    var cuponLoaded = 0;
    $(window).scroll(function() {

      var oTop = $('#counter').offset().top - window.innerHeight;
      if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function() {
          var $this = $(this),
            countTo = $this.attr('data-count');
          $({
            countNum: $this.text()
          }).animate({
              countNum: countTo
            },

            {

              duration: 7000,
              easing: 'swing',
              step: function() {
                $this.text(Math.floor(this.countNum));
              },
              complete: function() {
                $this.text(this.countNum);
                //alert('finished');
              }

            });
        });
        a = 1;
      }

    });
    $(document).ready(function () {
//        var cupon = '';
        <?php if(!isset($message) || !isset($error) || !isset($signinError)){?>
//            jQuery(window).load(function () {
//                setInterval(cuponModal, 3000);
////                setTimeout(cuponModal(),3000);
//                
//            });
        <?php }?>
        <?php if(isset($message)){?>
                $("#small-dialog h2").html('Transacción finalizada')
                $("#small-dialog p").html('<?php echo $message?>');
                setTimeout(function(){$("#modal-trigger").trigger('click')},500);
                    
        <?php }?>
        <?php if(isset($error)){?>
                $("#small-dialog h2").html('Error de ingreso')
                $("#small-dialog p").html('Verifica tus datos e intenta nuevamente');
                setTimeout(function(){$("#modal-trigger").trigger('click')},500);
                
        <?php }?>
        <?php if(isset($signinError)){?>
                $("#small-dialog h2").html('Error de registro')
                $("#small-dialog p").html('No hemos podido registrarte, intenta nuevamente');
                setTimeout(function(){$("#modal-trigger").trigger('click')},500);
                
        <?php }?>
        <?php if(isset($exist)){?>
                $("#small-dialog h2").html('Error de registro')
                $("#small-dialog p").html('El usuario ya se encuentra registrado');
                setTimeout(function(){$("#modal-trigger").trigger('click')},500);
        <?php }?>
        

         $('#termns-modal,#termns-modal2').click(function () {
                $("#dg_termns #error-title").html("Términos y condiciones");
                $("#dg_termns #error-msg").html("<?php echo lang("termns_and_conditions") ?>");
                $('#dg_termns').modal('toggle'); 
            
        });
         // close cupon on ling
        $("#small-dialog").on("click","#close-cupon",function(e){
            $("#small-dialog .mfp-close").trigger("click");
        });
         // Subscribe to newsletter
        $("#subscribe-newsletter").click(function(e){
            var email = $("#email-newsletter").val();
            var csrf = $("#csrf").val();
            var csrf_token = $("#csrf").attr('name');
            var data = {
                email: email
            };
            data[csrf_token] = csrf;
            $.ajax({
                type: 'post',
                data: data,
                url: '<?php echo base_url("looking/newsletter") ?>',
    //            dataType: "json",
                success: function (data) {
                    
                    $("#small-dialog h2").html('Gracias por subscribirte!')
                    $("#small-dialog p").html('Por favor sigue explorando el sitio y descubriendo nuestros beneficios')
                    $("#modal-trigger").trigger('click');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                }
            });
        });
//         // trigger signup form
//        $(".btn-start,.signup").click(function(e){
//
//            $.ajax({
//                type: 'post',
//                url: '<?php echo base_url("looking/start") ?>',
//    //            dataType: "json",
//                success: function (data) {
//                    $("#small-dialog h2").html('')
//                    $("#small-dialog p").html(data)
//                    $("#modal-trigger").trigger('click');
//
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                }
//            });
//        });
//        $(".login").click(function(e){
//
//            $.ajax({
//                type: 'post',
//                url: '<?php echo base_url("looking/login") ?>',
//    //            dataType: "json",
//                success: function (data) {
//                    $("#small-dialog h2").html('')
//                    $("#small-dialog p").html(data)
//                    $("#modal-trigger").trigger('click');
//
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                }
//            });
//        });
//        $('#myCarousel').carousel({
//            interval: 7000 //changes the speed
//        });
        $('#myCarousel2').carousel({
            interval: 7000 //changes the speed
        });
        $('.owl-carousel').owlCarousel({
            autoplay:true,
//            rtl:true,
            loop:true,
            margin:10,
            nav:true,
            pagination:true,
            navText: ["<i class='fa fa-chevron-left'></i>",
   "<i class='fa fa-chevron-right'></i>"],
            navClass: ['owl-prev','owl-next'],
            rewindNav : true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        })
        $('.portfolio-box').hover(function(){
            $(this).find(".project-name-search").removeClass("hidden");
            $(this).find(".transparent-input").focus();
        });
        $('.portfolio-box').mouseleave(function(){
            $(this).find(".project-name-search").addClass("hidden");
        });
        
        $('.animated-button').hover(function(){
            setTimeout(function(){$('.test-it').toggleClass("fade");},1000);
            setTimeout(function(){$('.delivery-form').toggleClass("fade");},1000);
        })
        function cuponModal(){
            if(cuponLoaded !== 1){
                $.ajax({
                    type: 'post',
                    data: '',
                    url: '<?php echo base_url("looking/cupon") ?>',
        //            dataType: "json",
                    success: function (data) {
                        $(this).showMessage("Bienvenido",data);
                        cuponLoaded = 1;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            }
        }
        
    });
</script>