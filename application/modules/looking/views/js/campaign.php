<script type="text/javascript">

    $(document).ready(function () {
        <?php if(isset($error)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("No hemos podido registrarte, intenta nuevamente<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        <?php if(isset($exist)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("El usuario ya se encuentra registrado<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        
        $('#submit').click(function (e) {
            e.preventDefault();

            if ( required($("#password")) 
                    && required($("#name")) 
                    && required($("#last_name")) 
                    && required($("#mobile")) 
                    && required($("#phone")) 
                    && required($("#address")) 
                    && required($("#state")) 
                    && required($("#city")) 
                    && required($("#school")) 
                    && required($("#grade")) 
                    && validateEmail($("#email")) ) {
                if(validateTermns($("#termns"))){
                    $(this).unbind('click').click();
                }
            }else{
//                alert("invalido")
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Verifica tu datos de registro para continuar<br/>");
                $('#dg_error').modal('toggle'); 
            }
        })

         $('#termns-modal').click(function () {
                $("#dg_termns #error-title").html("Términos y condiciones");
                $("#dg_termns #error-msg").html("<?php echo lang("termns_and_conditions") ?>");
                $('#dg_termns').modal('toggle'); 
            
        });
         $('#email').focusout(function () {
            validateEmail($("#email"))
        });
        $('.required').focusout(function () {
            required($(this))
        });
        var country = '<?php echo $country?>';
        if (parseInt(country) > 0) {
            $("#country").val(parseInt(country));
//            $("#country").trigger('change');
            var state = '<?php echo $state?>';
            setTimeout(function () {
                $("#country").trigger('change');
                $("#state").trigger('change');
                $("#city").trigger('change');
            }, 100);
            if (parseInt(state) > 0) {
                setTimeout(function () {
                    $("#state").val(parseInt(state));
                    $("#state").trigger('change');
                }, 400);
                var city = '<?php echo $city?>';
                if (parseInt(city) > 0) {
                    setTimeout(function () {
                        $("#city").val(parseInt(city));
                        $("#city").trigger('change');
                    }, 800);
                }
            }
        }
        $("#country").change(function () {
        console.log("change country")
            $.ajax({
                url: "<?php echo base_url(); ?>looking/getStates/",
                type: "post",
                data: {countryId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function (data) {

                    if (data.status != false) {
                        var option = $("<option value=''><?php echo lang('state') ?></option>");
                        $("#state").html(option);
                        $.each(data.data, function (idx, option) {
                            var option = $("<option></option>")
                                    .attr("value", option.id)
                                    .text(option.name);
                            $("#state").append(option);
                        });
                    }
                }
            });
        });

        $("body").on('change', '#state', function () {
            $.ajax({
                url: "<?php echo base_url(); ?>looking/getCities/",
                type: "post",
                data: {stateId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function (data) {

                    if (data.status != false) {
                        var option = $("<option value=''><?php echo lang('city') ?></option>");
                        $("#city").html(option);
                        $.each(data.data, function (idx, option) {
                            var option = $("<option></option>")
                                    .attr("value", option.id)
                                    .text(option.name);
                            $("#city").append(option);
                        });
                    }
                }
            });
        });
        function validateTermns(input) {
            if (!input.is(':checked')) {
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Debes aceptar los términos y condiciones<br/>");
                $('#dg_error').modal('toggle'); 
                
            }
            console.log(input.is(':checked'));
            return input.is(':checked');
        }
        function validateCell(value) {
            if (!isCellphone(value)) {
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("No es un número celular válido.<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
            } else {
                return true;
            }
        }
        ;

        function validateEmail(obj) {
            if (!isEmail($(obj).val())) {
                obj.toggleClass("validation-error")
                return false;
            } else {
                $(obj).removeClass("validation-error")
                return true;
            }
        }
        ;
        
        function required(obj){
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error")
                return false;
            }else{
                $(obj).removeClass("validation-error")
                return true;
            }
        }
        
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        ;

        function isCellphone(number) {
            var regex = /^3[0-9]{9}$/;
            return regex.test(number);
        }
        ;
    });
</script>