<script type="text/javascript">
$(document).ready(function(){
    var maxHeight =0;
    $(".my_planHeader").each(function(index,val){
        var current = $(this).height();
        if(current > maxHeight){
            maxHeight = current;
        }
    });
    $(".my_planHeader").height(maxHeight)
    
    //action on click button add 
        $(".add-product").click(function () {
            var id = $(this).attr("item-id");
            var cost = $(this).attr("item-cost");
            var offer = $(this).attr("item-offer");
            addItem(id, cost, 1, offer);

        });
        
        
        function addItem(id, cost, quotes,offer) {
//            if(isLogged()){
                var submitdata = {
                    "id": id,
                    "cost": cost,
                    "quotes": quotes,
                    "offer": offer,
                    "available": 1
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/addItem/",
                    type: "post",
                    dataType: "json",
                    data: submitdata,
                    success: function (data) {
                        if (data.status == true) {
                            $(".badge").html(data.data)
                            $(".badge").toggleClass("bounce");
                            setTimeout(function () {
                                $(".badge").toggleClass("bounce")
                            }, 3000);
                            $('#dg_detalle').modal('hide');
    //                        $("#btn-add").addClass('hide');
    //                        $("#btn-add-more").removeClass('hide');
    //                        $(".checkout-hide").addClass('hide');
    //                        $(".checkout-show").removeClass('hide');
    //                        $("#btn-checkout").removeClass('hide');
    //                        $("#final-quotes").html(submitdata.quotes);
    //                        $(".total").addClass('text-center');
                        } else {
    //                        $('#modal-message').foundation('reveal', 'close');
    //                        $("#modal-message #message").html('<?php echo lang('product_unavailable') ?>');
    //                        $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                });
//            }
        }
})
</script>