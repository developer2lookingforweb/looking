<script src="<?php echo base_url()?>js/jquery-ui-1.10.2/ui/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript">
    var sliders = [];
    var advisers = [];
$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
    var __slice = [].slice;

    (function ($, window) {
        var Starrr;

        Starrr = (function () {
            Starrr.prototype.defaults = {
                rating: void 0,
                numStars: 5,
                change: function (e, value) {}
            };

            function Starrr($el, options) {
                var i, _, _ref,
                        _this = this;

                this.options = $.extend({}, this.defaults, options);
                this.$el = $el;
                _ref = this.defaults;
                for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                        this.options[i] = this.$el.data(i);
                    }
                }
                this.createStars();
                this.syncRating();
                this.$el.on('mouseover.starrr', 'span', function (e) {
                    return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
                });
                this.$el.on('mouseout.starrr', function () {
                    return _this.syncRating();
                });
                this.$el.on('click.starrr', 'span', function (e) {
                    return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
                });
                this.$el.on('starrr:change', this.options.change);
            }

            Starrr.prototype.createStars = function () {
                var _i, _ref, _results;

                _results = [];
                for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
                }
                return _results;
            };

            Starrr.prototype.setRating = function (rating) {
                if (this.options.rating === rating) {
                    rating = void 0;
                }
                this.options.rating = rating;
                this.syncRating();
                return this.$el.trigger('starrr:change', rating);
            };

            Starrr.prototype.syncRating = function (rating) {
                var i, _i, _j, _ref;

                rating || (rating = this.options.rating);
                if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                        this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                    }
                }
                if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                        this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                    }
                }
                if (!rating) {
                    return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                }
            };

            return Starrr;

        })();
        return $.fn.extend({
            starrr: function () {
                var args, option;

                option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                return this.each(function () {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                        $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                        return data[option].apply(data, args);
                    }
                });
            }
        });
    })(window.jQuery, window);
    var comparison = [];
    var currentRequest = "";
    var questions = [];
    $(document).ready(function () {
        
        /**
         * 
         * auto height price-area
         */
//        var maxHeight =0;
//        $(".price-area").each(function(index,val){
//            var current = $(this).height();
//            if(current > maxHeight){
//                maxHeight = current;
//            }
//        });
//        $(".price-area").height(70)
        
        var maxHeight =0;
        $(".filter-form").each(function(index,val){
            var current = $(this).height();
            if(current > maxHeight){
                maxHeight = current;
            }
        });
        <?php if(isset($help)){
                if($help==1){
        ?>
            $("#filter-div").slideToggle();
            $(".hide-filters ").toggleClass("hidden");
            $(".filter-popup").trigger("click");
        <?php
                }
            }?>
        $("#products").css("min-height",maxHeight)
        
        $(".filter-popup").click(function(){
            $("#filter-div").slideToggle();
            $(".hide-filters ").toggleClass("hidden");
            var tab = $(this).attr('data-target');
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        });
        $(".hide-filters").click(function(){
            $(".hide-filters ").toggleClass("hidden");
//            $(".hide-filters i").toggleClass("fa-angle-down");
            $("#filter-div").slideToggle();
        });
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
        
        $(".btn-close-modal").click(function(){
            $("#filter-dialog .mfp-close").trigger("click");
        })
    
        /**
         * Suggestion select
         */
        $('.select-suggestion').click(function(){
            var selector = $(this).attr("data-selector");
            var value = $(this).attr("data-value");
            //Check radio or checkox field
//            $(".questions"+selector).each(function(idx,val){
//                if($(this).is(":checked"))$(this).removeAttr("checked");
//            });   
            if($(".questions"+selector).length >1){
                $("#qs"+selector+"-"+value).parent().find("span").click();
//                $("#qs"+selector+"-"+value).attr("checked",true);
//                $("#qs"+selector+"-"+value).trigger("change");
            }else{
                $(".questions"+selector).each(function(idx,val){
                      if($(val).hasClass("fslider")){
//                          $(this).attr("data-slider-value","[4,6]")
                          var values = value.split(",");
                          var intval = [];
                          $.each(values,function(i,v){
                              intval[i] = parseInt(v);
                          })
                          console.log(values)
                          sliders["qs"+selector].setValue(intval,true,true);
                      }
                      if($(val).hasClass("fselect")){
                        $("#qs"+selector).val(value);
                        $("#qs"+selector).trigger("change");
                      }
                });
            }
//            alert($("#qs"+$(this).attr("data-selector")))
//            if($("#qs"+$(this).attr("data-selector")).attr("type")==""){
//            }
        })
        
        
        $('.owl-carousel').owlCarousel({
             loop:true,
             autoplay: true,
             autoplayTimeout: 2000,
             margin:10,
             responsiveClass:true,
             animateIn: true,
             animateOut: true,
             responsive:{
                 0:{
                     items:1,
                    autoplay: true,
                    autoplayTimeout: 2000,
                     nav:true
                 },
                 600:{
                     items:1,
                    autoplay: true,
                    autoplayTimeout: 2000,
                     nav:false
                 },
                 1000:{
                     items:1,
                     nav:true,
                    autoplay: true,
                    autoplayTimeout: 2000,
                     loop:false
                 }
             }
         })
         
        rangeSlider();
        //Cart badge number checker
//        setInterval(checkCart, 6000);
        /**
         * Class chenge to list function
         * 
         */
        $('#list').click(function (event) {
            event.preventDefault();
            $('#products .card').addClass('list-group-item');
            $('#products .card .carousel').toggleClass('col-lg-3');
            $('#products .card .carousel').toggleClass('col-md-3');
            $('#products .card .carousel').toggleClass('col-sm-5');
            $('#products .card .listable').toggleClass('col-lg-9');
            $('#products .card .listable').toggleClass('col-md-9');
            $('#products .card .listable').toggleClass('col-sm-7');
        });
        $('#grid').click(function (event) {
            event.preventDefault();
            $('#products .card').removeClass('list-group-item');
            $('#products .card').addClass('grid-group-item');
            $('#products .card .carousel').toggleClass('col-lg-3');
            $('#products .card .carousel').toggleClass('col-md-3');
            $('#products .card .carousel').toggleClass('col-sm-5');
            $('#products .card .listable').toggleClass('col-lg-9');
            $('#products .card .listable').toggleClass('col-md-9');
            $('#products .card .listable').toggleClass('col-sm-7');
        });
        /**
         * 
         * end
         */
        
        /**
         * 
         * Check comparison items and drawit
         */
        $.ajax({
            type: 'post',
            url: '<?php echo base_url("looking/getComparison/"); ?>',
            dataType: "json",
            success: function (data) {
                if (data.status == true) {
                    comparison = data.comparison;
                    if (comparison.length <= 3) {
                        drawComparisons();
                    } else {
                        $(this).showMessage("Atención","Sólo se pueden comparar hasta 3 productos");
                        return false;
                    }

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#contenido_").html("<small>" + xhr.responseText + "</small>");
            }
        });
         /**
          * 
          * Add and remove favorites product icon
          */
        $("#products").on('click', '.add-favorites', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/addFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).toggleClass('favorite-active');
                        $(obj).toggleClass('remove-favorites');
                        $(obj).toggleClass('add-favorites');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#products").on('click', '.remove-favorites', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/removeFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).toggleClass('favorite-active');
                        $(obj).toggleClass('add-favorites');
                        $(obj).toggleClass('remove-favorites');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        /**
         * end favorites 
         */
         
         /**
          * Add and remove items to comparison form product icon and comparison image
          */
        $("#products").on('click', '.add-comparison', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            var submitdata = {
                id: id, 
                src: $("img#img-" + id + "-0").attr("src"), 
                name: $("#product-"+id).find(".product-name a").text(), 
                stars: $("#product-"+id).find(".starrr").attr("data-rating"),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                type: 'post',
                data: submitdata,
                url: '<?php echo base_url("looking/addComparison/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        comparison = data.comparison;
                        if (comparison.length <= 3) {
                            $(obj).toggleClass("comparison-active");
                            $(obj).toggleClass("add-comparison");
                            $(obj).toggleClass("remove-comparison");

                            drawComparisons();
                        } else {
                            $(this).showMessage("Atención","Sólo se pueden comparar hasta 3 productos");
                            return false;
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#products").on('click', '.remove-comparison', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            var submitdata = {
                id: id, 
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                type: 'post',
                data: submitdata,
                url: '<?php echo base_url("looking/removeComparison/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        comparison = data.comparison;
                        if (comparison.length <= 3) {
                            $(obj).toggleClass("comparison-active");
                            $(obj).toggleClass("add-comparison");
                            $(obj).toggleClass("remove-comparison");

                            drawComparisons();
                        } else {
                            $(this).showMessage("Atención","Sólo se pueden comparar hasta 3 productos");
                            return false;
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        
        $("#comparison").on('click', 'img', function () {

            var obj = this;
            var id = $(this).attr('item-id');
            var submitdata = {
                id: id, 
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                type: 'post',
                data: submitdata,
                url: '<?php echo base_url("looking/removeComparison/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        comparison = data.comparison;
                        if (comparison.length <= 3) {
                            $("#product-"+id).find(".remove-comparison").toggleClass("comparison-active");
                            $("#product-"+id).find(".remove-comparison").toggleClass("add-comparison");
                            $("#product-"+id).find(".remove-comparison").toggleClass("remove-comparison");

                            drawComparisons();
                        } else {
                            $(this).showMessage("Atención","Sólo se pueden comparar hasta 3 productos");
                            return false;
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#comparison").on('click', '.go-comparison', function (event) {
            event.preventDefault();
            var ids = "";
            $.each(comparison, function (idx, val) {
                var slug = (idx>0)?"-":"";
                ids += slug + val.id
            });
            var href = $(this).attr("href");
//            window.location.href = href +"/"+ids);
            window.location.assign(href +"/"+ids);
        });
        /**
         * End comparison 
         **/
        
        

        
        //Search data
        
        $(".filtersearch").change(function () {
            /**
             * 
             * Go to search function
             */
            addSearch($(this).val(),"filter" );
        });
        //Get detailed data of product to open the modal
        $("#products").on('click', '.view-more-trigger', function () {
            $("#hidden-data").fadeIn();
            $("#more-data").fadeOut();
        })
        $("#contenido_").on('click', '.modal_trigger', function () {
            var id = $(this).attr("item-id");
            drawModal(id);

        });
        //action on click button add 
        $("#products").on('click','.add-product',function () {
            var id = $(this).attr("item-id");
            var cost = $(this).attr("item-cost");
            var offer = $(this).attr("item-offer");
            addItem(id, cost, 1, offer);

        });

        /**
        *   Unhide sliders step 2 in advisers
        * */ 
        $(".unhide-sliders").click(function(){
            var count = 0;
            $(".propAdviser").addClass("hidden");
            $("#sortable li").each(function(){
                count ++;
                if(count<5){
                    $("#prop_"+$(this).attr("id")).removeClass("hidden");
                }
            })
        })
        

        /**
        *   Get sliders data an process filter
        * */ 
        $(".process-advisers").click(function(){
            $(".hide-loadgif").removeClass("hidden");
            $(".show-results-adv").addClass("hidden");
            
            $(".propAdviser").each(function(){
                if(! $(this).hasClass("hidden")){
//                    alert($(this).attr("id").replace("prop_",""))
//                    console.log(sliders);
//                    alert(sliders["adv"+$(this).attr("id").replace("prop_","")].getValue())
                    advisers.push({'id':$(this).attr("id").replace("prop_",""),'val':sliders["adv"+$(this).attr("id").replace("prop_","")].getValue()});
                }
            });
            setTimeout(function(){
                $(".hide-loadgif").addClass("hidden");
                $(".show-results-adv").removeClass("hidden");
            },2000);
        })
        $(".show-results-advisers").click(function(){
            $.ajax({
                type: 'post',
                url: '<?php echo base_url("looking/searchAdvisers/"); ?>',
                data: {'advisers':advisers},
                dataType: "json",
                success: function (data) {
                    var html = '';
                    $("#products").html(html);
                    if(data.results.length >0){

                        html += drawProducts(data.results,1);

                        $("#products").html(html);
                    }
                    $(".mfp-close").trigger("click");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
            
        })
        $(".btn-advisers").click(function (e) {
            $(".wizard ul li").removeClass("active")   
            $(".wizard ul li").addClass("disabled")   
            $(".wizard ul li").first().removeClass("disabled")   
            $(".wizard ul li").first().addClass("active")   
            $(".tab-content .tab-pane").removeClass("active")   
            $(".tab-content .tab-pane").first().addClass("active")   
            advisers = [];
        })
        
        function checkCart() {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url("looking/checkCart/"); ?>',
                dataType: "json",
                success: function (data) {
                    $("#cart .badge").html(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        }
        ;
        function drawQuestions(questions, answers) {
            var classResearch = 'research';
            if (typeof answers == 'undefined') {
                answers = [];
                classResearch = 'search';
            }
//            $("#main-title").show();
            var html = '';
            if (questions.length == 0) {
                html += '<div class="col-md-12 text-center">';
                html += "   <img src = '<?php echo base_url("images/corporativo.png") ?>'>"
                html += '   <div>';
                html += '   </div>';
                html += '</div>';
                $("#main-title").html("<br>PONTE EN CONTACTO CON NOSOTROS PARA BRINDARTE <br/>LA MEJOR ASESORÍA.<p class='tit_20'>Comunícate al 317 8045157 o por correo electrónico a <br/><a href='mailto:sales@lookingforweb.com' target='_top'>sales@lookingforweb.com</a></p>");
            }
            var cat = 0;
            $.each(questions, function (index, question) {
                cat = question.category;
                var multiple = (question.multiple == 1) ? 'multiple="multiple" "' : "";
                var multiCls = (question.multiple == 1) ? 'multiple' : "";
                html += '<div class="columns large-12 member-info-det">';
                html += '   <div>';
                html += '    <h2 class="tit_tarjeta_a">' + question.question + '</h2>';
                html += '   </div>';
                html += '   <div class="form-group">';
                html += '       <div class="col-md-12">';
                if (multiple !== 1) {
                    if (question.options.length == 0) {
                        html += '       <input type="text" id="question_' + question.id + '" name="questions" class="form-control ' + classResearch + ' ' + multiCls + '"/>';
                    } else {
                        html += '       <select id="question_' + question.id + '" name="questions[]" class="form-control ' + classResearch + ' ' + multiCls + '" ' + multiple + '>';
                    }
                } else {
                    html += '       <select id="question_' + question.id + '" name="questions" class="form-control ' + classResearch + ' ' + multiCls + '" ' + multiple + '>';
                }
                if (multiple !== 1) {
                    html += '            <option value=""><?php echo lang("default_select") ?></option>';
                }
                $.each(question.options, function (index, option) {
                    html += '            <option value="' + option.id + '">' + option.option + '</option>';
                });
                html += '       </select>';
                html += '       </div>';
                html += '   </div>';
                html += '</div>';
            });
            if (answers.length == 0 && questions.length > 0) {
                html += '<div class="member-info-det">';
                html += '   <div class="form-group">';
                html += '       <div class="col-md-12 text-center">';
                html += '           <a id="btn_calcular" cat-data="' + cat + '" href="#" class="button btn_tipo1 hvr-float-shadow " style="margin-top: 10px; padding-top: 14px;margin-bottom: 10px; padding-bottom: 10px;">Calcular Opciones</a>';
                html += '       </div>';
                html += '	</div> ';
                html += '</div>';
            }
            $("#questions").html(html);
            $.each(answers, function (idx, asw) {
                $("#question_" + idx).val(asw);
            });
        }
        
        
        function drawComparisons() {
            if (comparison.length > 0) {
                $("#comparison").removeClass("hidden");
                $("#productCounter").html(comparison.length);
            }else{
                $("#comparison").addClass("hidden");
            }    
                    
            $("#comparison  .photo-li").each(function (idx, id) {
                $(this).html('<i class="fa fa-photo fa-3x"></i>');
            });
            $.each(comparison, function (idx, item) {
                $("#li" + (idx + 1)).html("<label>" + item.name.trim(" ").substring(0,12) + "</label><label><div id='stars-existing' class='starrr' data-rating='" + item.stars + "'></div></label><img src='" + item.src + "'/>");
                $("#li" + (idx + 1) + " img").attr("item-id", item.id);
            });
            $(".starrr").starrr();
        }
        function drawProducts(items,recomended) {
            var html = "";
            var hidden = false;
            $.each(items, function (index, item) {
                if (index == 8 && items.length > 8) {
                   html += '  <div id="more-data" class="card  col-xs-12 col-sm-12 col-md-12 col-lg-12 ">    ';
                   html += '    <div class="thumbnail">        ';
                   html += '        <div id="product-xxx" class=" product-box">            ';
                   html += '            <h5 class="brand-name group inner list-group-item-heading text-center view-more-trigger">  ';
                   html += '                <a >Haz click</a>';
                   html += '            </h5>';
                   html += '            <h4 class="product-name group inner list-group-item-heading text-center view-more-trigger"> ';
                   html += '                <a>Para ver mas</a>';
                   html += '            </h4>';
                   html += '        </div>';
                   html += '    </div>';
                   html += '  </div>';
                   html += '  <div id="hidden-data" style="display: none;">';                            
                    
                    
                    
                    
                    hiddden = true;
                }
                if(index == 0 && typeof(recomended)!== "undefined"){
                    html += drawCard(item,1);
                }else{
                    html += drawCard(item);
                }

            });
            if (hidden) {
                html += '</div>';
            }
            return html;
        }
        function drawCard(item, recomended) {
            var images = item.image.split("|");
            var noImages =images.length;
            var html = '';
            html += '<div class="card col-xs-12 col-sm-6 col-md-3 col-lg-3 ">';
            if(typeof(recomended) !== "undefined"){
                html += '    <div class="item recomended thumbnail">';
            }else{
                html += '    <div class="item  thumbnail">';
            }
            html += '        <div id="product-' + item.id + '" class=" product-box">';
            if(typeof(recomended) !== "undefined"){
            html += '           <img class="offer" src="<?php echo base_url("images/recomendado.png")?>">';
                
            }
            if (item.offer > 0 || item.market_price > 0) { 
            html += '           <img class="offer" src="<?php echo base_url("images/offer.png")?>">';
            }
            //carousel
            html += '            <a href="<?php echo base_url()?>detail/'+item.id+'/'+ item.product.replace(" ","-").replace("'","-").replace(",","-").replace("?","-").replace("+","-").replace("(","-").replace(")","-")+'">'; 
            html += '            <div id="myCarousel' + item.id + '" class="carousel slide list-group-item-heading" data-interval="false">';
            if(noImages >1){
            html += '                <ol class="carousel-indicators">';
            $.each(images,function(index,image){
                var active= (index == 0 )?"active":"";
            html += '                    <li data-target="#myCarousel' + item.id + '" data-slide-to="' + index + '" class="' + item.id + '"></li>';
            });                
            html += '                </ol>';
            }
            html += '                <div class="carousel-inner">';
            $.each(images,function(index,image){
                var active =(index == 0 )?"active":"";
            html += '                    <div class="item '+ active +'">';
            html += '                        <img id="img-' + item.id + '-' + index + '" src="<?php echo base_url(); ?>images/products/' + image + '" class="img-responsive">';
            html += '                    </div>';
            });                
            html += '                </div>';
            if(noImages >1){
            html += '                <a class="left carousel-control" href="#myCarousel' + item.id + '" data-slide="prev">';
            html += '                    <span class="icon-prev"></span>';
            html += '                </a>';
            html += '                <a class="right carousel-control" href="#myCarousel' + item.id + '" data-slide="next">';
            html += '                    <span class="icon-next"></span>';
            html += '                </a>  ';
            }
            html += '            </div>';
            html += '            </a>';
            //end carousel
            
            html += '            <div class="caption listable">';
            html += '                <div class="row text-center">';
                                            if(item.category ==9){
            html += '                    Inscripción $ '+  numberThousand(item.cost);
                                            }else{
//            html += '                    <div id="stars-existing" class="starrr" data-rating="4"></div>';
                                            }
            html += '                </div>';
            html += '                <h6 class="brand-name group inner list-group-item-heading">';
            html += '                    <a href="<?php echo base_url()?>brand/'+item.provid+'/'+item.brand.replace(" ","-").replace("'","-").replace(",","-").replace("?","-").replace("+","-").replace("(","-").replace(")","-")+'">'; 
            html += '                    '+item.brand+'</a>';
            html += '                    </h6>';
            html += '                <h5 class="product-name group inner list-group-item-heading">';
            html += '                    <a href="<?php echo base_url()?>detail/'+item.id+'/'+ item.product.replace(" ","-").replace("'","-").replace(",","-").replace("?","-").replace("+","-").replace("(","-").replace(")","-")+'">'; 
            html += '                    '+ item.product;
            html += '                    </a>';
            html += '                </h5>';
            html += '                <div class="row">';
            html += '                    <div class="price-area">';
            if(item.stock >0){
                var buttonDisabled ='';
                if(item.offer >0){
                    html += '                        <p class="col-xs-7 text-right price-lead">Precio</p>';
                    html += '                        <p class="col-xs-5 text-right price-lead text-danger">$&nbsp;'+ numberThousand(item.offer)+'</p>';
                    html += '                        <p class="col-xs-12 text-right">&nbsp;';
                    html += '                            <strike>$&nbsp;'+  numberThousand(item.cost)+'</strike>';
                    html += '                        </p>';
                }else{
                    if(item.category ==9){
                        html += '                        <p class="col-xs-12 text-right price-lead">$&nbsp;'+ numberThousand(item.alternative_cost)+'</p>';
                    }else{
                        var classOffer = (item.market_price > 0)?"text-danger":"";
//                        html += '                        <p class="col-xs-7 text-right price-lead">x Cuotas Desde</p>';
//                        html += '                        <p class="col-xs-5 text-right price-lead '+classOffer+'">$&nbsp;'+ numberThousand(item.cost* <?php echo AuthConstants::FN_CREDITPERCENT?>)+'</p>';
                        html += '                        <p class="col-xs-7 text-right price-lead">Precio Contado</p>';
                        html += '                        <p class="col-xs-5 text-right price-lead '+classOffer+'">$&nbsp;'+ numberThousand(item.cost)+'</p>';
                        html += '                        <p class="col-xs-12 text-right over-price">&nbsp;<strike>';
                        if(item.market_price>0){
                        html += '                            $&nbsp;'+  numberThousand(item.market_price)+'';
                        }
                        html += '                        </strike></p>';
                    }
                }
            }else{
                var buttonDisabled ='disabled';
//                html += '                        <p class="col-xs-12 text-right price-lead text-warning">&nbsp;</p>';
                html += '                        <p class="col-xs-12 text-right price-lead text-warning">PRODUCTO AGOTADO!</p>';
                html += '                        <p class="col-xs-12 text-right over-price">&nbsp;</p>';
            }
            html += '                    </div>';
            html += '                    <div class="row media-price-container">&nbsp;';
            if (item.price > 0) {
                html += '                                    <p class="col-xs-7 text-right price-lead media-price">Precio promedio</p>';
                html += '                                    <p class="col-xs-5 text-right media-price price-lead">$&nbsp;'+ numberThousand(item.price) +'</p>';
            }else{
                html += '                                    <p class="col-xs-7 text-right price-lead media-price">&nbsp;</p>';
                html += '                                    <p class="col-xs-5 text-right media-price price-lead">&nbsp;</p>';
            }
            html += '                    </div>';
            html += '                    <div class="col-xs-12 col-md-12 text-center action-buttons">';
            html += '                        <a class="btn btn-sm btn-success bg-dark" href="<?php echo base_url()?>detail/'+item.id+'/'+ item.product.replace(" ","-").replace(",","-").replace("'","-").replace("?","-").replace("+","-").replace("(","-").replace(")","-")+'">Detalles</a>';
            var itemCost = (item.offer>0)?item.offer:item.cost;
            html += '                        <a class="add-product btn btn-sm btn-dark btn-success bg-primary  '+buttonDisabled+'" item-offer="'+item.offerId+'" item-id="'+item.id+'" item-cost="'+itemCost+'">';
            html += '                           <i class="fa fa-cart-plus "></i>&nbsp;Comprar';
            html += '                        </a>';
            var camara = "<?php echo $kvalue?>";
            camara = camara.split(",")
            var cat = camara.indexOf(String(item.category))
            if (cat !== -1) {
            html += '                        <a class="add-product-credit btn btn-sm bg-dark btn-success " href="<?php echo base_url()?>credit/'+item.id+'/">';
            html += '                           <i class="fa fa-credit-card "></i>&nbsp;x cuotas';
            html += '                        </a>';
            }
            var comparisonClass = (item.comparison) ? "comparison-active" : "";
            var comparisonAction = (item.comparison) ? "remove-comparison" : "add-comparison";
            html += '                        <a class="btn btn-sm btn-warning '+comparisonClass+' '+comparisonAction+'"  item-id="'+item.id+'" data-toggle="tooltip" data-placement="right" data-html="true" title="Comprara">Vs</a>';
                                                                <?php if($this->session->userdata(AuthConstants::USER_ID)){?>
                                                                var favoriteClass = (item.favorite)?"favorite-active":"";
                                                                var favoriteAction = (item.favorite)?"remove-favorites":"add-favorites";
//            html += '                        <a class="btn btn-sm btn-warning bg-dark '+ favoriteClass +' '+ favoriteAction+'" item-id="'+item.id+'" data-toggle="tooltip" data-placement="left" data-html="true" title="Agrega a favoritos"><i class="fa fa-star "></i>&nbsp;favoritos</a>';
                                                                <?php }?>
            html += '                    </div>';
            html += '                </div>';
            html += '            </div>';
            html += '        </div>';
            html += '    </div>';
            html += '</div>';
            
            
            
            
            
            
            return html;
        }
        function addItem(id, cost, quotes,offer) {
//            if(isLogged()){
                var submitdata = {
                    "id": id,
                    "cost": cost,
                    "quotes": quotes,
                    "offer": offer,
                    "available": 1
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/addItem/",
                    type: "post",
                    dataType: "json",
                    data: submitdata,
                    success: function (data) {
                        if (data.status == true) {
                            $(".badge").html(data.data)
                            $(".badge").toggleClass("bounce");
                            setTimeout(function () {
                                $(".badge").toggleClass("bounce")
                            }, 3000);
                            $('#dg_detalle').modal('hide');
                            window.location='<?php echo base_url()."looking/checkout" ?>';
    //                        $("#btn-add").addClass('hide');
    //                        $("#btn-add-more").removeClass('hide');
    //                        $(".checkout-hide").addClass('hide');
    //                        $(".checkout-show").removeClass('hide');
    //                        $("#btn-checkout").removeClass('hide');
    //                        $("#final-quotes").html(submitdata.quotes);
    //                        $(".total").addClass('text-center');
                        } else {
    //                        $('#modal-message').foundation('reveal', 'close');
    //                        $("#modal-message #message").html('<?php echo lang('product_unavailable') ?>');
    //                        $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                });
//            }
        }
        function isLogged() {
            var sessid = "<?php echo ($this->session->userdata(AuthConstants::USER_ID)) ?>";
            if (sessid == "") {
                $("#dg_error #error-title").html("Ingresa con tu usuario");
                $("#dg_error #error-msg").html("Para realizar transacciones o ver favoritos, debes estar registrado");
                $('#dg_error').modal('toggle');
                return false;
            } else {
                return true;
            }

        }
        function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
        $("#contenido_").on('focusout', 'input', function () {
            required($(this))
        });
        $("#contenido_").on('focusout change', 'select', function () {
//        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate() {
            $(".required").each(function () {
                required($(this));
            });
            if ($(".validation-error")[0]) {
                return false;
            } else {
                return true;
            }
        }
        function required(obj) {
            $(obj).removeClass("validation-error")
            if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
        
        function search(){
            var submitdata = {
                "type": $(".filter-form").attr("filter-type"),
                "extra": $(".filter-form").attr("filter-extra"),
                "questions": questions,
                <?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
//            if (questions.length == 0) {
//                $("#dg_error #error-title").html("Verifica tus respuestas");
//                $("#dg_error #error-msg").html("Debes responder por lo menos 1 pregunta.");
//                $('#dg_error').modal('toggle');
//                return false;
//            }
            $('#products').block({ 
                message: '<img src="<?php echo base_url("images/overlay.png")?>">', 
                centerY: 0,
                css: { 
                     border: 'none', 
                     top: '45px',
                     backgroundColor: 'rgba(0,0,0,0)', 
                     opacity: 1, 
                     color: '#fff' 
                 } 
            }); 
//                $(this).showMessage("","hola mundo cruel");
            if(currentRequest && currentRequest.readyState != 4){
                currentRequest.abort();
            }
            currentRequest = $.ajax({
                type: 'post',
                data: submitdata,
                url: '<?php echo base_url("looking/results/"); ?>',
                dataType: "json",
                
                success: function (data) {
                    $('#products').unblock(); 
                    if (data.status == true) {
//                            $("#result-row").html(data.tmpl);
//                            $("#filter-title").html("Filtra los favoritos <br /><br />");
                        var html = '';
                        $("#products").html(html);
                        if(data.results.length >0){
                            
                            html += drawProducts(data.results);

                            $("#products").html(html);
                        }else{
                            $("#products").html('<div class="col-lg-push-1 col-lg-10 text-center well"><h1>Ajusta los parámetros de tu búsqueda para obtener mejores resultados</h1></div>');
                        }
                         $(".starrr").starrr();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        }
        /*====================================
        * Function to detect smartselect change
        * ====================================*/


        var smartSelection = function($target, event) {
            var self = this;
            addSearch($target.attr('data-value'),self.$element[0].id );
            return true;
        };
        var smartDeselection = function($target, event) {
            var self = this;
            addSearch($target.attr('data-value'),self.$element[0].id );
            return true;
        };
        var addSearch = function(data,question) {
            var questionId = question.replace("qs","").split("-")[0];
            var insert = true;
            console.log(data)
            console.log(question)
            $.each(questions,function(ix,vl){
                if(questionId == vl.id){
                    insert = false;
                    if(vl.value == data){
                        questions.splice(ix, 1);
                    }else{
                        questions[ix].value = data;
                    }
                }
            });
            if(insert)
            {
                questions.push({id: questionId,value:data})
            }
//            console.log(questions)
            search();
            return true;
        };
        
        $(".dataslider").each(function(idx,value){
            sliders[$(this).attr("id")] = new Slider("#"+$(this).attr("id"), {
                                                    tooltip: 'always'
                                            });
//            sliders[$(this).attr("id")] = $(this).slider({toltip:'always'});
        })
         $("input[type='radio'],input[type='checkbox']").on("change",function(evt){
             addSearch($(this).val(),$(this).attr("id") );
         });
         $(".ffilter").on("change",function(slideEvt){
             var data = slideEvt.target.value;
             data = data.split(",");
             var finalData = "";
             var questionId = $(this).attr("id");
             $.each(data,function(idx,val){
                if(idx>0){finalData +=","} 
                finalData += $("#"+questionId+"-"+val).val()
             });
             addSearch(finalData,$(this).attr("id") );
//             alert(slideEvt.value)
         });
         $(".research").change(function(){
             
             addSearch($(this).val(),$(this).attr("id") );
         })
         $(".smartselect").smartselect({
            multiple: false,
            closeOnSelect: true,
            text: {
                selectLabel: 'Select ...'
            },
            searchBy: 'label',
            searchCaseInsensitive: true,
            style: {
                buttonDropdown: 'dropdown-menu',
                toolbarPos: 'left'
            },
            toolbar: {
                buttonSearch:   true,
                buttonAlias:    false,
                buttonView:     false,
                buttonUnfold:   true,
                buttonCancel:   false,
                buttonCheckAll: false,
                buttonUnCheck:  false
            },
            callback:{
                onOptionSelect: [ smartSelection ],
                onOptionDeselect: [ smartDeselection ]
            }
        });
    });


    $(function () {
        return $(".starrr").starrr();
    });

    $(document).ready(function () {

        $("[data-toggle=tooltip]").tooltip();
//    $('#stars-existing').on('.starrr:change', function(e, value){
////    $('#count').html(value);
//    });
    });
        var rangeSlider = function(){
//            var slider = $('.range-slider'),
//            range = $('.range-slider__range'),
//            value = $('.range-slider__value');
//
//            slider.each(function(){
//
//            value.each(function(){
//              var value = $(this).prev().attr('value');
//              $(this).html(value);
//            });
//
//            range.on('input', function(){
//              $(this).next(value).html(this.value);
//            });
//        });
};
$(function() {
  $('.toggle-nav').click(function() {
    // Calling a function in case you want to expand upon this.
    toggleNav();
  });
});

/*========================================
=            CUSTOM FUNCTIONS            =
========================================*/
function toggleNav() {
if ($('#site-wrapper').hasClass('show-nav')) {
  // Do things on Nav Close
  $('#site-wrapper').removeClass('show-nav');
} else {
  // Do things on Nav Open
  $('#site-wrapper').addClass('show-nav');
}

//$('#site-wrapper').toggleClass('show-nav');
}

$(document).ready(function () {
    //Initialize tooltips
//    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
</script>