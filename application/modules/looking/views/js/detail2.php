<script type="text/javascript">
var __slice = [].slice;


    $(document).ready(function () {

        $("#myTab li:first-child a").trigger("click");
        /**
         * 
         * Radar AmChart
         */
        var chartData =[];
        <?php foreach($details as $property){?>
                chartData.push({"property":"<?php echo $property["label"] ?>","value":0});
        <?php }?>
        var chart = AmCharts.makeChart("radar-chart", {
            type: "radar",
            colors: ["#ee4b28"],
            dataProvider: chartData,
            categoryField: "property",
            startDuration: 2,
            valueAxes: [{
                    axisAlpha: 0.15,
                    minimum: 0,
                    maximum: 10,
                    dashLength: 1,
                    axisTitleOffset: 20,
                    gridCount: 5
                }],
            graphs: [{
                    valueField: "value",
                    bullet: "round",
                    balloonText: "Nuestra calificacion para el equipo en cuanto a [[property]] es de [[value]] "
                }],
            responsive: {
                "enabled": true
              }
        });

        function loop() {
             var chartData =[];
        <?php foreach($details as $property){?>
                chartData.push({"property":"<?php echo $property["label"] ?>","value":<?php echo $property["rank"] ?>});
        <?php }?>
            chart.animateData(chartData, {
                duration: 1000

            });
        }

        chart.addListener("init", function () {
            setTimeout(loop, 3000);
        });
        
         /**
          * 
          * Add and remove favorites product icon
          */
        $(".product-detail").on('click', '.add-favorites', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/addFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).toggleClass('favorite-active');
                        $(obj).toggleClass('remove-favorites');
                        $(obj).toggleClass('add-favorites');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $(".product-detail").on('click', '.remove-favorites', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/removeFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).toggleClass('favorite-active');
                        $(obj).toggleClass('add-favorites');
                        $(obj).toggleClass('remove-favorites');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        /**
         * end favorites 
         */
         

        
        //action on click button add 
        $(".add-product").click(function () {
            var id = $(this).attr("item-id");
            var cost = $(this).attr("item-cost");
            var offer = $(this).attr("item-offer");
            addItem(id, cost, 1, offer);

        });
        $(".file-popup").click(function (e) {
            e.preventDefault();
            if($(this).attr("type") == "pdf"){
                $(this).showMessage("Pensum","<embed src='"+$(this).attr("href")+"' width='1000' height='500' href='"+$(this).attr("href")+"'></embed>");
            }else{
                $(this).showMessage("Pensum","<img width='1000px' src='"+$(this).attr("href")+"'>");
            }
            $("#small-dialog").addClass("large-popup");
        });

        
        function addItem(id, cost, quotes,offer) {
//            if(isLogged()){
                var submitdata = {
                    "id": id,
                    "cost": cost,
                    "quotes": quotes,
                    "offer": offer,
                    "available": 1
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/addItem/",
                    type: "post",
                    dataType: "json",
                    data: submitdata,
                    success: function (data) {
                        if (data.status == true) {
                            $(".badge").html(data.data)
                            $(".badge").toggleClass("bounce");
                            setTimeout(function () {
                                $(".badge").toggleClass("bounce")
                            }, 3000);
                            $('#dg_detalle').modal('hide');
                            window.location='<?php echo base_url()."looking/checkout" ?>';
    //                        $("#btn-add").addClass('hide');
    //                        $("#btn-add-more").removeClass('hide');
    //                        $(".checkout-hide").addClass('hide');
    //                        $(".checkout-show").removeClass('hide');
    //                        $("#btn-checkout").removeClass('hide');
    //                        $("#final-quotes").html(submitdata.quotes);
    //                        $(".total").addClass('text-center');
                        } else {
    //                        $('#modal-message').foundation('reveal', 'close');
    //                        $("#modal-message #message").html('<?php echo lang('product_unavailable') ?>');
    //                        $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                });
//            }
        }
        
        function isLogged() {
            var sessid = "<?php echo ($this->session->userdata(AuthConstants::USER_ID)) ?>";
            if(sessid == ""){
                $("#dg_error #error-title").html("Ingresa con tu usuario");
                $("#dg_error #error-msg").html("Para realizar transacciones o ver favoritos, debes estar registrado");
                $('#dg_error').modal('toggle');
                return false;
            }else{
                return true;
            }
        
        }
        function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
        $("#contenido_").on('focusout', 'input', function () {
            required($(this))
        });
        $("#contenido_").on('focusout change', 'select', function () {
//        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate() {
            $(".required").each(function () {
                required($(this));
            });
            if ($(".validation-error")[0]) {
                return false;
            } else {
                return true;
            }
        }
        function required(obj) {
            $(obj).removeClass("validation-error")
            if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
        $(".rating").hover(function(){
            $(this).toggleClass("glyphicon-star-empty");
            $(this).toggleClass("glyphicon-star");
        });
        
    });


$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
  
  });
  
  
</script>