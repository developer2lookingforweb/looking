<script type="text/javascript" src="https://checkout.epayco.co/checkout.js">   </script>
<script type="text/javascript">
    var shipping = 0;
    var shipping_cost = 0;
    var epaycodata = {};
    $(document).ready(function() {

        
        var handler = ePayco.checkout.configure({
  				key: '<?php echo $epaykey?>',
  				test: <?php echo $epaytest?>
  			})

        epaycodata={
          //Parametros compra (obligatorio)
          name: "Producto",
          description: "Producto",
          invoice: "",
          currency: "cop",
          amount: "",
          tax_base: "0",
          tax: "0",
          country: "co",
          lang: "en",

          //Onpage="false" - Standard="true"
          external: "false",


          //Atributos opcionales
          extra1: "",
          extra2: "",
          extra3: "",
          confirmation: "",
          response: "",

          //Atributos cliente
          name_billing: "",
          address_billing: "",
          type_doc_billing: "cc",
          mobilephone_billing: "",
          number_doc_billing: ""
          }


        <?php if($isCode){?>
        <?php   if($codeRedemed>0){?>
                    $(this).showMessage("Código Validado","Se ha aplicado el descuento a los elementos que aplican");
        <?php   }else if($codeRedemed < 0){?>
                    $(this).showMessage("Código Validado","El código ya ha sido utilizado");
        <?php   }else{?>
                    $(this).showMessage("Código Validado","No se han encontrado elementos para aplicar el descuento");
        <?php   }?>
        <?php }?>
        $("#payment").click(function(){
           if(validate()){
            if($(this).hasClass("directPayment")){
                setTimeout($("#payu").submit(),500);
            }else{
                 var submitdata = {
                         "cid": $("#cid").val(),
                         "name": $("#name").val(),
                         "last_name": $("#last_name").val(),
                         "address": $("#address").val(),
                         "phone": $("#phone").val(),
                         "mobile": $("#mobile").val(),
                         "city": $("#city").val(),
                         "dni": $("#dni").val(),
                         "docType": $("#docType").val(),
                         "email": $("#email").val(),
                         "total": $("#total").html().replace(",","").replace(",",""),
                         "shipping": $("#shipping-cost").html().replace(",","").replace(",",""),
                         "shipping_type": $("#shipping").val(),
                         <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                 };
                 if($("#brand").val() && $("#number").val()){
                     submitdata.brand = $("#brand").val();
                     submitdata.number = $("#number").val();
                 }
                 $.ajax({
                     url: "<?php echo base_url();?>looking/orderConfirmation/",
                     type: "post",            
                     data: submitdata,
                     dataType: "json",
                     success: function(data){   
                         if (data.status != false) {
                                var pamount = 0;
                                var usepayu = 0;
                                var reserve = 0;
                                var code = 0;
                                $.each(data.data.amount, function (index, amount) {
                                    if (index == <?php echo AuthConstants::PM_USE_PAYU ?>) {
                                        pamount = pamount + amount;
                                        usepayu = 1;
                                    }
                                    if (index == <?php echo AuthConstants::PM_RESERVE ?>) {
                                        reserve = 1;
                                    }
                                    if (index == <?php echo AuthConstants::PM_GENERATE_CODE ?>) {
                                        code = 1;
                                    }
                                });
                                $("#buyerEmail").val(data.data.email);
                                $("#reference").val(data.data.reference);
                                $("#pamount").val(parseInt(pamount)+parseInt(shipping_cost));
                                $("#signature").val(data.data.signature);
                                $("#response").val(data.data.response);
                                $("#confirmation").val(data.data.confirmation);
                                //                            $("#address").val(data.data.address);
                                //                            $("#city").val(data.data.city);
                                //                            $("#country").val(data.data.country);
                                epaycodata.response = '<?php echo base_url("looking/epayco/")?>/'+data.data.reference;
                                epaycodata.confirmation = '<?php echo base_url("looking/epaycoc/")?>/'+data.data.reference;     
                                epaycodata.name = $("input[name='description']").val();
                                epaycodata.description = $("input[name='description']").val();
                                epaycodata.amount = parseInt(pamount)+parseInt(shipping_cost);
                                epaycodata.invoice = data.data.reference;
                                epaycodata.name_billing = $("#name").val() + " " + $("#last_name").val();
                                epaycodata.address_billing = $("#address").val();
                                epaycodata.mobilephone_billing = $("#mobile").val();
                                epaycodata.number_doc_billing = $("#dni").val();
                                console.log("here")
                                if (usepayu > 0 && reserve == 0 && code == 0) {
                                    handler.open(epaycodata);
                                    // setTimeout($("#payu").submit(), 1000);
                                }
                                if (pamount > 0 && (reserve > 0 || code > 0)) {
                                    $(this).showMessage('Solicitud de compra',"Se efectuará un pago por $" + pamount + ", un agente se pondrá en contacto por el valor restante");
                                    $.ajax({
                                        url: "<?php echo base_url(); ?>looking/reservedOrder/" + data.data.reference,
                                        global: false,
                                        success: function (data) {
                                            setTimeout(function () {
                                                // $("#payu").submit()
                                                handler.open(epaycodata);
                                            }, 1000);
                                        }
                                    });
                                }
                                if (usepayu == 0 && (reserve > 0 || code > 0)) {
                                    $("#dg_error #error-title").html("Solicitud de compra");
                                    $("#dg_error #error-msg").html("Se ha reservado el producto, un agente se pondrá en contacto<br/><br/>");
//                                 $('#modal-message').foundation('reveal', 'open');
                                    if (code > 0) {
                                        $(this).showMessage('Solicitud de descuento','Se ha generado un código de descuento, verifica tu correo electrónico');
                                    }else{
                                        $(this).showMessage('Solicitud de compra','Se ha reservado el producto, un agente se pondrá en contacto');
                                    }
                                    $.ajax({
                                        url: "<?php echo base_url(); ?>looking/reservedOrder/" + data.data.reference,
                                        global: false,
                                        success: function (data) {
                                            setTimeout(function () {
                                                window.location = "<?php echo base_url(); ?>"
                                            }, 1000);
                                        }
                                    });
                                }
                                if (usepayu == 0 && reserve == 0 && code == 0) {
                                    $(this).showMessage('Solicitud de compra','No hay productos en el carrito, verifica la información');
                                }
                            } else {
                                $(this).showMessage('Solicitud de compra','No hay productos en el carrito, verifica la información');
                            }
                     }
                 });
             }
            }else{
//                $("#modal-message #modal-title").html("Alerta");
//                $("#modal-message #message").html("Error de validacion<br/><br/>");
//                $('#modal-message').foundation('reveal', 'open');
//            
                $(this).showMessage('Error de registro','No hemos podido registrarte, intenta nuevamente');
            }
            
       });
       
       $('.findCustomer').click(function(){
            var dataSubmit = {
                dni: $("#dnic").val(),
            }
            $.ajax({
                type: 'post',
                data: dataSubmit,
                url: '<?php echo base_url("orders/getCustomerByDni/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {                        
    //                    $(this).showMessage("Asignación","Usuario Asignado");
                        $("#name").val(data.data.name);
                        $("#last_name").val(data.data.last_name);
                        $("#email").val(data.data.email);
                        $("#address").val(data.data.address);
                        $("#mobile").val(data.data.mobile);
                        $("#phone").val(data.data.phone);
                        $("#dni").val(data.data.dni);
                        $("#docType").val(data.data.doctype);
                        $("#state").val(data.data.state);
                        $("#state").trigger("change");
                        setTimeout(function(){$("#city").val(data.data.city);},1000)
                    }else{
                        $(this).showMessage("Error","No se ha realizado la asignación, por favor verifica los datos")
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(this).showMessage("Hola!","Parece que eres nuevo por aquí, por favor completa el formulario")
                        $("#dni").val($("#dnic").val());
                        $("#name").val("");
                        $("#last_name").val("");
                        $("#email").val("");
                        $("#address").val("");
                        $("#mobile").val("");
                        $("#phone").val("");
                        $("#docType").val("");
                        $("#state").val("");
                }
            });
       });
       
       $(".action_edit").click(function(){
            $("#quotes_"+$(this).parent().parent().attr("id")).addClass("hide");
            $("#edit_quotes_"+$(this).parent().parent().attr("id")).removeClass("hide");
            $("#edit_"+$(this).parent().parent().attr("id")).parent().addClass("hide");
            $("#save_"+$(this).parent().parent().attr("id")).parent().removeClass("hide");
            if($("#edit_quotes_"+$(this).parent().parent().attr("id")).attr('value')>0){
               $("#edit_quotes_"+$(this).parent().parent().attr("id")).val($("#edit_quotes_"+$(this).parent().parent().attr("id")).attr('value')); 
            }
            
        });
        $(".action_save").click(function(){
            
            var gift = $(this).parent().parent().attr('id');
            var id = $(this).parent().parent().attr('id').replace("b","");
            var quotes = $("#edit_quotes_"+$(this).parent().parent().attr('id')).val();
            var amount = $("#amount_"+id).attr("base");
            $.ajax({
                url: "<?php echo base_url();?>browser/editCart/",
                type: "post",            
                data: {id:id,quotes:quotes,cost:amount,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.response != "error"){
                        $("#edit_quotes_"+id).val(quotes);
                        $("#edit_quotes_"+gift).val(quotes);
                        $("#quotes_"+id).html(quotes);
                        $("#quotes_"+gift).html(quotes);
                        $("#amount_"+id).html(data.gift);
                        $("#subtotal").html(data.subtotal);
                        $("#process").html(data.process);
                        $("#total").html(data.total);
                        $("#quotes_"+gift).removeClass("hide");
                        $("#edit_quotes_"+id).addClass("hide");
                        $("#edit_quotes_"+gift).addClass("hide");
                        $("#edit_"+gift).parent().removeClass("hide");
                        $("#save_"+gift).parent().addClass("hide");  
                        calculateTotal();
                        if(data.response != "added"){
                            $("#quotes_"+gift).html(data.response)
                            $("#quotes_"+id).html(data.response)
                            $("#modal-message #message").html("<?php echo lang('max_available_fees')?>" + data.response);
                            $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                }
            });
        });
        $(".action-delete").click(function(){
            
            var gift = $(this).attr('item-id');
            $.ajax({
                url: "<?php echo base_url();?>browser/deleteCart/",
                type: "post",            
                data: {id:gift,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.response != "error"){
                        $("#item-"+gift).fadeOut();
                        $("#subtotal").html(data.subtotal);
                        $("#process").html(data.process);
                        $("#total").html(data.total);                        
                        $("#pamount").html(data.total);                        
                        $("#cart .badge").html(data.cartCount);  
                        calculateTotal();
                        if(parseInt(data.cartCount)==0){
                            window.location = '<?php echo base_url()?>';
                        }
                    }
                }
            });
        });
        $("#country").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>browser/getStates/",
                type: "post",            
                data: {countryId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#state").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .text(option.name);
                            $("#state").append(option);
                        });
                    }
                }
            });
        });
        var docType = '<?php echo $docType?>';
        if(parseInt(docType)>0){
            $("#docType").val(parseInt(docType));
        }
        var country = '<?php echo $country?>';
        if(parseInt(country)>0){
            $("#country").val(parseInt(country));
            $("#country").trigger('change');
            var state = '<?php echo $state?>';
            if(parseInt(state)>0){
                    setTimeout(function(){
                        $("#state").val(parseInt(state));
                        $("#state").trigger('change');
                    },500);
                var city = '<?php echo $city?>';
                if(parseInt(city)>0){
                    setTimeout(function(){
                        $("#city").val(parseInt(city));
                        $("#city").trigger('change');
                    },1000);
                }
            }
        }
        $("#state").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>looking/getCities/",
                type: "post",            
                data: {stateId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#city").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .attr("data-shipping", option.shipping)
                            .text(option.name);
                            $("#city").append(option);
                        });
                    }
                }
            });
        });
        $("#city").change(function(){
            $("#premium-shipping").removeAttr("disabled");
            if($(this).val() !== '<?php echo AuthConstants::ID_BOGOTA ?>'){
                $("#premium-shipping").attr("disabled","disabled");
            }
            shipping = $('option:selected', this).attr('data-shipping');
        });
        $("#shipping").change(function(){
            if($(this).val() == '<?php echo AuthConstants::ID_STANDARD ?>'){
                shipping_cost = shipping;
            }
            if($(this).val() == '<?php echo AuthConstants::ID_PREMIUM ?>'){
                shipping_cost = <?php echo AuthConstants::SHIPPING_PREMIUM_COST ?>;
            }
            if($(this).val() == '<?php echo AuthConstants::ID_ONSITE ?>'){
                shipping_cost = 0;
            }
            $("#shipping-cost").html(numberThousand(parseInt(shipping_cost)));
            calculateTotal();
        });
        
        
        $(document).on('click', '.number-spinner button', function () {    
            var btn = $(this),
                    oldValue = btn.closest('.number-spinner').find('input').val().trim(),
                    newVal = 0;

            if (btn.attr('data-dir') == 'up') {
                    newVal = parseInt(oldValue) + 1;
            } else {
                    if (oldValue > 1) {
                            newVal = parseInt(oldValue) - 1;
                    } else {
                            newVal = 1;
                    }
            }
            btn.closest('.number-spinner').find('input').val(newVal);
            var quotes  = $(this).parent().parent().find('input').val();
            var amount  = $(this).parent().parent().find('input').attr("base-cost");
            var id      = $(this).parent().parent().find('input').attr("item-id");
            $.ajax({
                url: "<?php echo base_url();?>browser/editCart/",
                type: "post",            
                data: {id:id,quotes:quotes,cost:amount,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.response != "error"){
                        $("#total-"+id).html(data.gift);
                        $("#subtotal").html(data.subtotal);
                        $("#process").html(data.process);
                        $("#total").html(data.total);
                        calculateTotal()
                        if(data.response != "added"){
//                            $("#quotes_"+gift).html(data.response)
//                            $("#quotes_"+id).html(data.response)
//                            $("#modal-message #message").html("<?php echo lang('max_available_fees')?>" + data.response);
//                            $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                }
            });
        });
        
        $("input").focusout(function(){
            required($(this))
        });
        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate(){
            $(".required").each(function(){
                required($(this));
            });
            if ($(".validation-error")[0]){
                return false;
            } else {
                return true;
            }
        }
        function required(obj){
            $(obj).removeClass("validation-error")
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
        function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
        function calculateTotal() {
            var total = $("#total").html().replace(",","").replace(",","");
            total = parseInt(total) + parseInt(shipping_cost);
            $("#total-w-shipping").html(numberThousand(total));
        }
    });
</script>