<script type="text/javascript">

    $(document).ready(function () {
        <?php if(isset($error)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("No hemos podido registrarte, intenta nuevamente<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        <?php if(isset($exist)){?>
                $("#dg_error #error-title").html("Error general");
                $("#dg_error #error-msg").html("El usuario ya se encuentra registrado<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        

         $('#termns-modal,#termns-modal2').click(function () {
                $("#dg_termns #error-title").html("Términos y condiciones");
                $("#dg_termns #error-msg").html("<?php echo lang("termns_and_conditions") ?>");
                $('#dg_termns').modal('toggle'); 
            
        });
    });
</script>