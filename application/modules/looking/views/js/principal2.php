<script type="text/javascript">
    var comparison = [];
    $(document).ready(function () {
        <?php 
        if( isset($triggercategory)){
        ?>
            if(parseInt(<?php echo $triggercategory ?>)>0){
                setTimeout(function(){$(".links #"+parseInt(<?php echo $triggercategory ?>)).click()},500);
            }
        <?php 
        }
        ?>
        <?php 
        if( isset($triggerproduct)){
        ?>
            if(parseInt(<?php echo $triggerproduct ?>)>0){
                setTimeout(function(){$(".links #"+parseInt(<?php echo $productCat ?>)).click()},500);
                setTimeout(drawModal(<?php echo $triggerproduct ?>),1000);
                setTimeout(function(){$("#dg_detalle").modal("show")},1500);
                
            }
        <?php 
        }
        ?>
        <?php 
        if( isset($triggercart)){
        ?>
            setTimeout(drawCart(),1000);
        <?php 
        }
        ?>
        <?php 
        if( isset($triggerbrowse)){
        ?>
                setTimeout(directSearch("<?php echo $triggerbrowse ?>"),1000);
        <?php 
        }else{
        ?>
        /**
         * Trigger outstandind event in page ready
         */
        setTimeout(function () {
            $("#opc_destacados").trigger('click')
        }, 500);
        <?php 
        }
        ?>
        /**
         * Hide categories on header menu clic
         */
        $("#imenu").on('click', function(){
                if ($('#menu_lp').css('display') !== 'none'){
                        $('#menu_lp').toggle();
                        $('#contenido_').removeClass("col-md-10");
                        $('#contenido_').addClass("col-md-12");
                }
                else{
                        $('#menu_lp').toggle();
                        $('#contenido_').removeClass("col-md-12");
                        $('#contenido_').addClass("col-md-10");
                }
        });
        
        /**
         * Function to obtain outstandind products
         */
        $("#opc_destacados,#opc_destacados-m").click(function () {
            if(jQuery(this).attr("id") == "opc_destacados-m"){
                jQuery("#menu").removeClass("visible");
            }
            $("#contenido_").html("<br/><br/><br/><br/><br/><br/><br/><br/><img src='<?php echo base_url() ?>images/AjaxLoader.gif'  class='center-block'  >");
            $.ajax({
                type: 'post',
                data: {
                    id_: '1',
                    des: '--',
                },
                url: '<?php echo base_url("looking/outstanding/"); ?>',
                dataType: "json",
                success: function (data) {
                    var html = '';
                    $("#contenido_").html(data.tmpl);
                    if (data.status == true) {
                        $.each(data.data, function (index, outstanding) {
                            html += drawCard(outstanding);
                        })
                    }
                    $("#main-title").html("PRODUCTOS Y SERVICIOS DESTACADOS<br/><p class='tit_20' >DALE UN VISTAZO A TODO LAS MEJORES OFERTAS  </p>");
                    $("#filter-title").html("Filtra los destacados <br /><br />");
                    $("#grid-results").html(html);
                    
                    var s = $("<select id=\"catId\" name=\"selectName\" class=\"form-control search outstanding\" />");
                    $("<option value='0'><?php echo lang("select_value") ?></option>").appendTo(s);
                    $.each(data.categories, function (idx, cat) {
                        $("<option />", {value: cat.id, text: cat.category}).appendTo(s);
                    });
                    $("#questions").html(s);
                    drawComparisons();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#opc_favoritos,#opc_favoritos-m").click(function () {
            if(isLogged()){
                if(jQuery(this).attr("id") == "opc_favoritos-m"){
                    jQuery("#menu").removeClass("visible");
                }
                $("#contenido_").html("<br/><br/><br/><br/><br/><br/><br/><br/><img src='<?php echo base_url() ?>images/AjaxLoader.gif'  class='center-block'  >");
                $.ajax({
                    type: 'post',
                    data: {
                        id_: '1',
                        des: '--',
                    },
                    url: '<?php echo base_url("looking/favorites/"); ?>',
                    dataType: "json",
                    success: function (data) {
                        $("#contenido_").html(data.tmpl);
                        var html = '';
                        if (data.status == true) {
                            $.each(data.data, function (index, favorite) {
                                html += drawCard(favorite);
                            });
                        }
                        $("#main-title").html("MIS FAVORITOS<br/><p class='tit_20' >DALE UN VISTAZO A TODO LO QUE HAS MARCADO COMO FAVORITO </p>");
                        $("#filter-title").html("Filtra los favoritos <br /><br />");
                        $("#grid-results").html(html);

                        var s = $("<select id=\"catId\" name=\"selectName\" class=\"form-control search favorites\" />");
                        $("<option value='0'><?php echo lang("select_value") ?></option>").appendTo(s);
                        $.each(data.categories, function (idx, cat) {
                            $("<option />", {value: cat.id, text: cat.category}).appendTo(s);
                        });
                        $("#questions").html(s);
                        drawComparisons();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                    }
                });
            }
        });
        $("#contenido_").on('change', '#catId', function () {
            var catId = $(this).val();
            if($(this).hasClass("favorites")){
                var url = '<?php echo base_url("looking/favorites/"); ?>';
            }else{
                var url = '<?php echo base_url("looking/outstanding/"); ?>';
            }
            $.ajax({
                type: 'post',
                data: {
                    id_: '1',
                    cat: catId,
                },
                url: url,
                dataType: "json",
                success: function (data) {
//                    $("#contenido_").html(data.tmpl);
                    var html = '';
                    if (data.status == true) {
                        html += drawProducts(data.data);
                    }
                    $("#grid-results").html(html);
                    
                    drawComparisons();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#opc_carrito,#opc_carrito_m").click(function () {
//            if(isLogged()){
                drawCart();
//            }
        });
        $('#opc_buscar, #opc_buscar-m').keypress(function (e) {
            
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                if(jQuery(this).attr("id") == "opc_buscar-m"){
                    jQuery("#menu").removeClass("visible");
                }
                directSearch($(this).val())
                
            }
        });

        $("#contenido_").on('click', '.action_delete', function () {
            var id = $(this).attr('id').replace("delete_","");
            $.ajax({
                type: 'post',
                data: {
                    id: id,
                    des: '--',
                },
                url: '<?php echo base_url("looking/deleteCart/"); ?>',
                dataType: "json",
                success: function (data) {
                    var html = '';
                    $("#row_"+id).remove();
                    $("#opc_carrito").next().html(data.cartCount);
                    $("#pamount").val(data.total.replace(",",""));
                    $("#total-value").html("$ " + numberThousand(data.total));
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#contenido_").on('click', '#payment', function () {
//            if(isLogged()){
            if (validate()) {
                if ($(this).hasClass("directPayment")) {
                    setTimeout($("#payu").submit(), 500);
                } else {
                    var submitdata = {
                        "cid": $("#cid").val(),
                        "name": $("#name").val(),
                        "last_name": $("#last_name").val(),
                        "address": $("#address").val(),
                        "phone": $("#phone").val(),
                        "mobile": $("#mobile").val(),
                        "city": $("#city").val(),
                        "dni": $("#dni").val(),
                        "docType": $("#docType").val(),
                        "email": $("#email").val(),
                        "total": $("#total-value").html().replace(",", "").replace(",", ""),
                        <?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                    };
                    if ($("#brand").val() && $("#number").val()) {
                        submitdata.brand = $("#brand").val();
                        submitdata.number = $("#number").val();
                    }
                    $.ajax({
                        url: "<?php echo base_url(); ?>looking/orderConfirmation/",
                        type: "post",
                        data: submitdata,
                        dataType: "json",
                        success: function (data) {
                            if (data.status != false) {
                                var pamount = 0;
                                var usepayu = 0;
                                var reserve = 0;
                                var code = 0;
                                $.each(data.data.amount, function (index, amount) {
                                    if (index == <?php echo AuthConstants::PM_USE_PAYU ?>) {
                                        pamount = pamount + amount;
                                        usepayu = 1;
                                    }
                                    if (index == <?php echo AuthConstants::PM_RESERVE ?>) {
                                        reserve = 1;
                                    }
                                    if (index == <?php echo AuthConstants::PM_GENERATE_CODE ?>) {
                                        code = 1;
                                    }
                                });
                                $("#reference").val(data.data.reference);
                                $("#pamount").val(pamount);
                                $("#signature").val(data.data.signature);
                                $("#response").val(data.data.response);
                                $("#confirmation").val(data.data.confirmation);
                                //                            $("#address").val(data.data.address);
                                //                            $("#city").val(data.data.city);
                                //                            $("#country").val(data.data.country);
                                if (usepayu > 0 && reserve == 0 && code == 0) {
                                    setTimeout($("#payu").submit(), 1000);
                                }
                                if (pamount > 0 && (reserve > 0 || code > 0)) {
                                    $("#dg_error #error-msgtitle").html("Solicitud de compra");
//                                 $('#modal-message').foundation('reveal', 'open');
                                    $("#dg_error #error-msg").html("Se efectuará un pago por $" + data.data.amount + ", un agente se pondrá en contacto por el valor restante<br/><br/>");
                                    $('#dg_error').modal('toggle');
                                    $.ajax({
                                        url: "<?php echo base_url(); ?>looking/reservedOrder/" + data.data.reference,
                                        global: false,
                                        success: function (data) {
                                            setTimeout(function () {
                                                $("#payu").submit()
                                            }, 1000);
                                        }
                                    });
                                }
                                if (usepayu == 0 && (reserve > 0 || code > 0)) {
                                    $("#dg_error #error-title").html("Solicitud de compra");
                                    $("#dg_error #error-msg").html("Se ha reservado el producto, un agente se pondrá en contacto<br/><br/>");
//                                 $('#modal-message').foundation('reveal', 'open');
                                    if (code > 0) {
                                        $("#dg_error #error-title").html("Solicitud de descuento");
                                        $("#dg_error #error-msg").html("Se ha generado un código de descuento, verifica tu correo electrónico<br/><br/>");
                                    }
                                    $('#dg_error').modal('toggle');
                                    $.ajax({
                                        url: "<?php echo base_url(); ?>looking/reservedOrder/" + data.data.reference,
                                        global: false,
                                        success: function (data) {
                                            setTimeout(function () {
                                                window.location = "<?php echo base_url(); ?>"
                                            }, 1000);
                                        }
                                    });
                                }
                                if (usepayu == 0 && reserve == 0 && code == 0) {
                                    $("#dg_error #error-msgtitle").html("Solicitud de compra");
                                    $("#dg_error #error-msg").html("No hay productos en el carrito, verifica la información<br/><br/>");
//                                 $('#modal-message').foundation('reveal', 'open');
                                    $('#dg_error').modal('toggle');
                                }
                            } else {
//                             $("#modal-message #modal-title").html("Solicitud de compra");
                                $("#dg_error #error-msg").html("No hay productos en el carrito, verifica la información<br/><br/>");
//                             $('#modal-message').foundation('reveal', 'open');
                                $('#dg_error').modal('toggle');
                            }
                        }
                    });
                }
            } else {
                $("#dg_error #error-title").html("Alerta");
                $("#dg_error #error-msg").html("Error de validacion<br/><br/>Verifica que todos los campos hayan sido diligenciados.");
//                 $('#modal-message').foundation('reveal', 'open');
                $('#dg_error').modal('toggle');

            }
//            }

        });
        $("#opc_perfil, #opc_perfil_m").click(function () {
            if(isLogged()){
                $("#contenido_").html("<br/><br/><br/><br/><br/><br/><br/><br/><img src='<?php echo base_url() ?>images/AjaxLoader.gif'  class='center-block'  >");
                $.ajax({
                    type: 'post',
                    data: {
                        id_: '1',
                        des: '--',
                    },
                    url: '<?php echo base_url("looking/profile/"); ?>',
                    dataType: "json",
                    success: function (data) {
                        var html = '';
                        $("#contenido_").html(data.data.tmpl);
                        $("#cid").val(data.data.cid);
                        $("#name").val(data.data.name);
                        $("#last_name").val(data.data.last_name);
                        $("#phone").val(data.data.phone);
                        $("#mobile").val(data.data.mobile);
                        $("#address").val(data.data.address);
                        $("#email").val(data.data.email);
                        $("#dni").val(data.data.dni);
                        var option = $("<option><?php echo lang("select_value") ?></option>")
                        $("#docType").append(option);
                        $.each(data.data.docTypes, function (idx, docType) {
                            var option = $("<option></option>")
                                    .attr("value", docType.id)
                                    .text(docType.name);
                            $("#docType").append(option);
                        });
                        $("#docType").val(data.data.docType);

                        if (parseInt(data.data.country) > 0) {
                            $("#country").val(parseInt(data.data.country));
                            $("#country").trigger('change');
                            var state = data.data.state;
                            if (parseInt(state) > 0) {
                                setTimeout(function () {
                                    $("#state").val(parseInt(state));
                                    $("#state").trigger('change');
                                }, 500);
                                var city = data.data.city;
                                if (parseInt(city) > 0) {
                                    setTimeout(function () {
                                        $("#city").val(parseInt(city));
                                        $("#city").trigger('change');
                                    }, 1000);
                                }
                            }
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                    }
                });
            }
        });
        $("#contenido_").on('click', '#update-profile', function () {
            if (validate()) {
                var submitdata = {
                    "cid": $("#cid").val(),
                    "name": $("#name").val(),
                    "last_name": $("#last_name").val(),
                    "address": $("#address").val(),
                    "phone": $("#phone").val(),
                    "mobile": $("#mobile").val(),
                    "city": $("#city").val(),
                    "dni": $("#dni").val(),
                    "docType": $("#docType").val(),
                    "email": $("#email").val(),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/persistProfile/",
                    type: "post",
                    data: submitdata,
                    dataType: "json",
                    success: function (data) {
                        if (data.status == true) {
                            var name = $("#name").val() + ' ' + $("#last_name").val() + '</br><p class="tit_14_1">' + $("#email").val() + '</p>';
                            $("#opc_perfil h1").html(name);
                            $("#dg_error #error-title").html("Edición de perfil");
                            $("#dg_error #error-msg").html("El perfil ha sido actualizado correctamente.");
                            //                 $('#modal-message').foundation('reveal', 'open');
                            $('#dg_error').modal('toggle');

                        } else {
                            $("#dg_error #error-title").html("Alerta");
                            $("#dg_error #error-msg").html("Error de validacion<br/><br/>Verifica que todos los campos hayan sido diligenciados.");
                            //                 $('#modal-message').foundation('reveal', 'open');
                            $('#dg_error').modal('toggle');
                        }
                    }
                });
            } else {
                $("#dg_error #error-title").html("Alerta");
                $("#dg_error #error-msg").html("Error de validacion<br/><br/>Verifica que todos los campos hayan sido diligenciados.");
                $('#dg_error').modal('toggle');

            }

        });
        $("#contenido_").on('click', '#addFavorite', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/addFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).find('img').attr('src', '<?php echo base_url() ?>images/favoritos2.png');
                        $(obj).attr('id', 'removeFavorite');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#contenido_").on('click', '#addCompare', function () {

            var obj = this;
            var id = $(this).attr('item-id');
            if (comparison.length < 3) {
                comparison.push({id:id,src:$("img#" + id).attr("src")})
                $(this).find("img").attr("src", "<?php echo base_url('images/comparar2.png') ?>");

                obj.id = "removeCompare";
                drawComparisons();
            } else {
                $("#dg_error #error-title").html("<?= lang('compare_products') ?>");
                $("#dg_error #error-msg").html("<?php echo lang('exceded_products') ?>");
                $('#dg_error').modal('toggle');

                return false;
            }
        });
        $("#contenido_").on('click', '#removeCompare', function () {

            var obj = this;
            var id = $(this).attr('item-id');
            var index = -1;
            $.each(comparison, function(idx,val){
                if(val.id == id){
                    index = idx;
                }
            })
            if(index !== -1){
                comparison.splice(index, 1);
            }
            $(this).find("img").attr("src", "<?php echo base_url('images/comparar.png') ?>");

            obj.id = "addCompare";
            drawComparisons();
        });
        $("#contenido_").on('click', '.comparison-img', function () {

            var obj = this;
            var id = $(this).attr('item-id');
            var index = -1;
            $.each(comparison, function(idx,val){
                if(val.id == id){
                    index = idx;
                }
            })
            if(index !== -1){
                comparison.splice(index, 1);
            }
            $(this).attr("src", "");
            $("#removeCompare[item-id$='" + id + "']").find("img").attr("src", "<?php echo base_url('images/comparar.png') ?>");
            $("#removeCompare[item-id$='" + id + "']").attr("id","addCompare");
            drawComparisons();
        });
        $("#contenido_").on('click', '#btn_comparar', function () {
            if (comparison.length == 0) {
                $("#dg_error #error-title").html("<?= lang('compare_products') ?>");
                $("#dg_error #error-msg").html("<?php echo lang('no_comparison') ?>");
                $('#dg_error').modal('toggle');
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/compare/",
                    type: "post",
                    dataType: "json",
                    data: {comparison: comparison},
                    success: function (data) {
                        var columns = 12 / parseInt(comparison.length)
                        if (data.status == true) {
                            var html = "";
                            html += '<div id="" class="row">';
                            $.each(data.data, function (idx, product) {
                            html += '<div id="" class="col-md-'+columns+'">';
                            html += '   <div class="team - member modern">';
                            html += '       <a href="#"> ';
                            html += '           <div class="member-photo imgevent">';
                            html += '               <img id="detail_image" class="img-responsive center-block img_item" src="<?php echo site_url('images/products') ?>/' + product.image + '" alt="">';
                            if(product.market_price > 0){

                            html += '                           <div class="member-name color04">';
                            html += '                               <span> $ ' + numberThousand(product.cost) + '</span> ';
                            html += '                           </div>';
                            html += '                           <div class="team-member-cross member-name color05">';
                            html += '                               <span> $ ' + numberThousand(product.market_price) + '</span> ';
                            html += '                           </div>';
                            }else{
                            html += '                           <div class="member-name color01">';
                            html += '                               <span> $ ' + numberThousand(product.cost) + '</span> ';
                            html += '                           </div>';
                            }
                            html += '           </div>    ';
                            html += '       </a>';
                            html += '   <div class="member-info">';
                            html += '       <div>';
                            html += '           <h2 id="detail_brand" >';
                            html += '               <a class="tit_tarjeta_a" href="<?php echo base_url('looking/principal/browse')?>/'+ product.brand +'" >' + product.brand + '</a> ';
                            html += '           </h2>';
                            html += '           <h2 id="detail_name" class="tit_tarjeta_a">';
                            html += '                   ' + product.name;
                            html += '           </h2>';
                            html += '       </div>';
                            html += '   </div>';
                            
                            //mobile details
                             
                            html += '<div id="" class="hidden-md hidden-lg" >';
                            html += '   <div id="detail_properties">';
                                var tempdata = product.details;
                                $.each(tempdata, function (i, detail) {
                                    html += "<div class='member-info-det'>";
                                    html += "   <div class='col-md-7 tit_tarjeta_f1'>";
                                    html += "       " + detail.property;
                                    html += "   </div>";
                                    html += "   <div class='col-md-5 tit_tarjeta_f2'>";
                                    html += "       " + detail.value;
                                    html += "   </div>";
                                    html += "</div>";
                                });
                            html += '   </div>';
                            html += '</div>';
                            
                            //mobile description
                            html += '<div id="" class="hidden-md hidden-lg">';
                            html += '   <div  class="member-info-det">';
                            html += '       <div id="detail_description" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">';
                            html += '                   ' + product.description;
                            html += '       </div>';
                            html += '   </div> ';
                            html += '</div>';
                            
                            
                            //mobile button
                             html += '<div id="" class="hidden-md hidden-lg">';
                            html += '   <div class="member-info-det">';
                            html += '       <div class="col-md-12">';
                            html += '               <a href="#" id="0" item-id="'+product.id+'" item-cost="'+product.cost+'" class="detail_add add button btn_tipo1 hvr-float-shadow" style="margin-top: 10px; padding-top: 10px;margin-bottom: 10px; padding-bottom: 10px; ">Añadir al Carrito</a>';
                            html += '       </div> ';
//                            html += '       </div> ';
                            html += '   </div>';
                            html += '</div>';
                            
                            
                            html += '</div>';
                            html += '</div>';
                            });
                            html += '</div>';
                            html += '<div id="" class="row hidden-xs hidden-sm">';
                            $.each(data.data, function (idx, product) {
                            html += '<div id="" class="col-md-'+columns+'" >';
                            html += '   <div id="detail_properties">';
                                var tempdata = product.details;
                                $.each(tempdata, function (i, detail) {
                                    html += "<div class='member-info-det'>";
                                    html += "   <div class='col-md-7 tit_tarjeta_f1'>";
                                    html += "       " + detail.property;
                                    html += "   </div>";
                                    html += "   <div class='col-md-5 tit_tarjeta_f2'>";
                                    html += "       " + detail.value;
                                    html += "   </div>";
                                    html += "</div>";
                                });
                            html += '   </div>';
                            html += '</div>';
                            });
                            html += '</div>';
                            html += '<div id="" class="row hidden-xs hidden-sm">';
                            $.each(data.data, function (idx, product) {
                            html += '<div id="" class="col-md-'+columns+'">';
                            html += '   <div  class="member-info-det">';
                            html += '       <div id="detail_description" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">';
                            html += '                   ' + product.description;
                            html += '       </div>';
                            html += '   </div> ';
                            html += '</div>';
                            });
                            html += '</div>';
                            html += '<div id="" class="row hidden-xs hidden-sm">';
                            $.each(data.data, function (idx, product) {
                            html += '<div id="" class="col-md-'+columns+'">';
                            html += '   <div class="member-info-det">';
                            html += '       <div class="col-md-12">';
                            html += '               <a href="#" id="0" item-id="'+product.id+'" item-cost="'+product.cost+'" class="detail_add add button btn_tipo1 hvr-float-shadow" style="margin-top: 10px; padding-top: 10px;margin-bottom: 10px; padding-bottom: 10px; ">Añadir al Carrito</a>';
                            html += '       </div> ';
//                            html += '       </div> ';
                            html += '   </div>';
                            html += '</div>';
                            });
                            html += '</div>';
                            $("#dg_comparison #error-title").html("<?= lang('compare_products') ?>");
                            $("#dg_comparison #error-msg").html(html);
                            $('#dg_comparison').modal('toggle');
                        } else {
                            $("#dg_error #error-title").html("<?= lang('compare_products') ?>");
                            $("#dg_error #error-msg").html("<?php echo lang('comparison_unavailable') ?>");
                            $('#dg_error').modal('toggle');
                        }
                    }
                });
            }
        });
        $("#contenido_").on('click', '#removeFavorite', function () {
            var obj = this;
            var id = $(this).attr('item-id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/removeFavorites/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $(obj).find('img').attr('src', '<?php echo base_url() ?>images/favoritos.png');
                        $(obj).attr('id', 'addFavorite');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $(".category-selector").click(function () {
            var id = $(this).attr('id');
//            $.ajax({
//                type: 'post',
//                data: {
//                    id: id
//                },
//                url: '<?php echo base_url("looking/getSubcategories/"); ?>',
//                dataType: "json",
//                success: function (data) {
//                    if (data.status == true) {
//                        var html = '';
//                        $("#contenido_").html(data.tmpl);
//                        $.each(data.data, function (index, category) {
//                            html += "<li><a id='" + category.id + "'  href='#' class='selx' >" + category.category + "</a></li>";
//                        });
//                        $("#subcategories-list").html(html);
//                        $("#subcategories-list li").first().find('a').trigger('click');
//                    }
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
//                }
//            });
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/getCatData/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
//                        $("#contenido_").html(data.tmpl)
                        if (data.data.length > 1) {
                            $("#questions").html(data.tmpl2)
//                            $.each(data.data, function (index, cat) {
//                                console.log(cat)
//                                var html = "<div class='member-info-det'>";
//                                html += "       <div  class='col-md-6 tit_tarjeta_f1_a'  > " + cat.category + " </div> "
//                                html += "</div> ";
//                                $("#subcats-children").append(html);
//                            })
                        } else {
                            drawQuestions(data.data[0].questions);
                        }
//                            var html = '';
//                            $("#contenido_").html(data.tmpl);
//                            $.each(data.data,function(index,category){
//                                html += "<li><a id='m2_opc1'  href='#' class='selx' >"+category.category+"</a></li>";
//                            });
//                            $("#subcategories-list").html(html); 
                        var html = '';
                        $("#contenido_").html(data.tmpl);
                        $.each(data.data, function (index, category) {
                            html += "<li><a id='" + category.id + "'  href='#' class='selx' >" + category.category + "</a></li>";
                        });
                        $("#subcategories-list").html(html);
                        
                        $("#subcategories-list-mobile").html(html);
                        
                        if($("#subcategories-list").is(":visible")){
                            $("#subcategories-list li").first().find('a').trigger('click');
                        }else{
                            $("#subcategories-list-mobile li").first().find('a').trigger('click');
                            $('#subcategories-list-mobile').slick({
                                centerMode: true,
                                centerPadding: '',
                                slidesToShow: 1,
                                responsive: [
                                  {
                                    breakpoint: 767,
                                    settings: {
                                      arrows: true,
                                      centerMode: true,
                                      centerPadding: '0',
                                      slidesToShow: 2
                                    }
                                  },
                                  {
                                    breakpoint: 767,
                                    settings: {
                                      arrows: true,
                                      centerMode: false,
                                      centerPadding: '0',
                                      slidesToShow: 1
                                    }
                                  }
                                ]
                              });
                              $('#subcategories-list-mobile').on('swipe', function(event, slick, direction){
                                 $("a.selx").removeClass('barra_sup1_activo');
                                 var _a = jQuery("#subcategories-list-mobile li:eq("+(slick.currentSlide+1)+")").find("a");
                                    
                                    var id = $(_a).attr('id');
                                    $("#subcategories-list-mobile a#"+id).addClass('barra_sup1_activo');
                                    $.ajax({
                                        type: 'post',
                                        data: {
                                            id: id
                                        },
                                        url: '<?php echo base_url("looking/getCatData/"); ?>',
                                        dataType: "json",
                                        success: function (data) {
                                            if (data.status == true) {
//                                                $("#result-row").html(data.tmpl)
                                                if (data.data.length > 1) {
                                                    $("#questions").html(data.tmpl2)
                                                    $.each(data.data, function (index, cat) {
                                                        var html = "<div class='member-info-det'>";
                                                        html += "       <div  class='col-md-6 tit_tarjeta_f1_a'  > " + cat.category + " </div> "
                                                        html += "</div> ";
                                                        $("#subcats-children").append(html);
                                                    })
                                                } else {
                                                    drawQuestions(data.data[0].questions);
                                                }
                        //                            var html = '';
                        //                            $("#contenido_").html(data.tmpl);
                        //                            $.each(data.data,function(index,category){
                        //                                html += "<li><a id='m2_opc1'  href='#' class='selx' >"+category.category+"</a></li>";
                        //                            });
                        //                            $("#subcategories-list").html(html); 
                                            }
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                                        }
                                    });
                                
                              });
                              
                        }
                        
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        //subcat selector
        $("#contenido_").click(function(event){
//            console.log(event.target)
        });
        
        $("#contenido_").on('click', 'a.selx', function () {
            $("a.selx").removeClass('barra_sup1_activo');
            $(this).addClass('barra_sup1_activo');
            var id = $(this).attr('id');
            $.ajax({
                type: 'post',
                data: {
                    id: id
                },
                url: '<?php echo base_url("looking/getCatData/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
//                        $("#result-row").html(data.tmpl)
                        if (data.data.length > 1) {
                            $("#questions").html(data.tmpl2)
                            $.each(data.data, function (index, cat) {
                                var html = "<div class='member-info-det'>";
                                html += "       <div  class='col-md-6 tit_tarjeta_f1_a'  > " + cat.category + " </div> "
                                html += "</div> ";
                                $("#subcats-children").append(html);
                            })
                        } else {
                            drawQuestions(data.data[0].questions);
                        }
//                            var html = '';
//                            $("#contenido_").html(data.tmpl);
//                            $.each(data.data,function(index,category){
//                                html += "<li><a id='m2_opc1'  href='#' class='selx' >"+category.category+"</a></li>";
//                            });
//                            $("#subcategories-list").html(html); 
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        //Search data
        $("#contenido_").on('click', '#btn_calcular', function () {
            var category = $(this).attr('cat-data');
            var questions = [];
            $(".search").each(function (idx, data) {
                if ($(data).val() !== "" && $(data).val() !== null) {
                    questions.push($(data).val());
                }
            });
            var submitdata = {
                "questions": questions,
                <?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            if (questions.length == 0) {
                $("#dg_error #error-title").html("Verifica tus respuestas");
                $("#dg_error #error-msg").html("Debes responder por lo menos 1 pregunta.");
                $('#dg_error').modal('toggle');
                return false;
            }
            $.ajax({
                type: 'post',
                data: submitdata,
                url: '<?php echo base_url("looking/results/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $("#result-row").html(data.tmpl);
                        $("#main-title").html('COMPARA Y COMPRA<br><p class="tit_20">COMPARA LAS MEJORES OPCIONES, SI NO ESTAS CONVENCIDO FILTRA DE NUEVO  </p>');
//                            $("#filter-title").html("Filtra los favoritos <br /><br />");
                        var html = '';
                        $("#grid-results").html(html);
                           html += drawProducts(data.results);

                        $("#grid-results").html(html);

                        drawQuestions(data.questions, data.answers);
                        drawComparisons();
                        
                        if($("#subcategories-list-mobile").is(":visible")){
                            jQuery(".filter-block #questions").hide();
                            jQuery(".filter-block").css("padding-bottom","0");
                            
                            jQuery("#filter-title").click(function(){
                                jQuery(".filter-block #questions").toggle(300,function(){
                                    jQuery("#filter-title i").toggleClass("fa-caret-up");
                                });
                            });
                        };
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        });
        $("#contenido_").on('change', '[name="questions[]"]', function () {
            var questions = [];
            if ($(this).hasClass("research")) {
                $(".research").each(function (idx, data) {
                    if ($(data).val() !== "" && $(data).val() !== null) {
                        questions.push($(data).val());
                    }
                });
                var submitdata = {
                    "questions": questions,
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                };
                if (questions.length == 0) {
                $("#dg_error #error-title").html("Verifica tus respuestas");
                $("#dg_error #error-msg").html("Debes responder por lo menos 1 pregunta.");
                $('#dg_error').modal('toggle');
                return false;
            }
                $.ajax({
                    type: 'post',
                    data: submitdata,
                    url: '<?php echo base_url("looking/browse/"); ?>',
                    dataType: "json",
                    success: function (data) {
                        if (data.status == true) {
//                            $("#result-row").html(data.tmpl);
                            $("#main-title").html('COMPARA Y COMPRA<br><p class="tit_20">COMPARA LAS MEJORES OPCIONES, SI NO ESTAS CONVENCIDO FILTRA DE NUEVO  </p>');
//                            $("#filter-title").html("Filtra los favoritos <br /><br />");
                            var html = '';
                            $("#grid-results").html(html);
                                html += drawProducts(data.data);

                            $("#grid-results").html(html);
                            if($("#subcategories-list-mobile").is(":visible")){
                                jQuery(".filter-block #questions").toggle(300,function(){
                                    jQuery("#filter-title i").toggleClass("fa-caret-up");
                                });
                            };
                            setTimeout(drawComparisons(),500);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                    }
                });
            }
        });
        //Get detailed data of product to open the modal
        $("#contenido_").on('click', '.view-more-trigger', function () {
            $("#hidden-data").removeClass('hide');
            $("#more-data").addClass('hide');
        })
        $("#contenido_").on('click', '.modal_trigger', function () {
            var id = $(this).attr("item-id");
            drawModal(id);

        });
        //action on click button add 
        $("#dg_detalle").on('click', '.detail_add', function () {
            var id = $(this).attr("item-id");
            var cost = $(this).attr("item-cost");
            addItem(id, cost, 1);

        });

        $("#contenido_").on('change', '#country', function () {
            $.ajax({
                url: "<?php echo base_url(); ?>looking/getStates/",
                type: "post",
                data: {countryId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function (data) {

                    if (data.status != false) {
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#state").html(option);
                        $.each(data.data, function (idx, option) {
                            var option = $("<option></option>")
                                    .attr("value", option.id)
                                    .text(option.name);
                            $("#state").append(option);
                        });
                    }
                }
            });
        });

        $("#contenido_").on('change', '#state', function () {
            $.ajax({
                url: "<?php echo base_url(); ?>looking/getCities/",
                type: "post",
                data: {stateId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function (data) {

                    if (data.status != false) {
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#city").html(option);
                        $.each(data.data, function (idx, option) {
                            var option = $("<option></option>")
                                    .attr("value", option.id)
                                    .text(option.name);
                            $("#city").append(option);
                        });
                    }
                }
            });
        });


        function drawQuestions(questions, answers) {
            var classResearch = 'research';
            if (typeof answers == 'undefined') {
                answers = [];
                classResearch = 'search';
            }
//            $("#main-title").show();
            var html = '';
            if(questions.length == 0){
                html += '<div class="col-md-12 text-center">';
                html += "   <img src = '<?php echo base_url("images/corporativo.png")?>'>"
                html += '   <div>';
                html += '   </div>';
                html += '</div>';
                $("#main-title").html("<br>PONTE EN CONTACTO CON NOSOTROS PARA BRINDARTE <br/>LA MEJOR ASESORÍA.<p class='tit_20'>Comunícate al 317 8045157 o por correo electrónico a <br/><a href='mailto:sales@lookingforweb.com' target='_top'>sales@lookingforweb.com</a></p>");
            }
            var cat =0;
            $.each(questions, function (index, question) {
                cat = question.category;
                var multiple = (question.multiple == 1) ? 'multiple="multiple" "' : "";
                var multiCls = (question.multiple == 1) ? 'multiple' : "";
                html += '<div class="columns large-12 member-info-det">';
                html += '   <div>';
                html += '    <h2 class="tit_tarjeta_a">' + question.question + '</h2>';
                html += '   </div>';
                html += '   <div class="form-group">';
                html += '       <div class="col-md-12">';
                if (multiple !== 1) {
                    if(question.options.length ==0){
                    html += '       <input type="text" id="question_' + question.id + '" name="questions" class="form-control ' + classResearch + ' ' + multiCls + '"/>';
                    }else{
                    html += '       <select id="question_' + question.id + '" name="questions[]" class="form-control ' + classResearch + ' ' + multiCls + '" ' + multiple + '>';
                    }
                } else {
                    html += '       <select id="question_' + question.id + '" name="questions" class="form-control ' + classResearch + ' ' + multiCls + '" ' + multiple + '>';
                }
                if (multiple !== 1) {
                    html += '            <option value=""><?php echo lang("default_select") ?></option>';
                }
                $.each(question.options, function (index, option) {
                    html += '            <option value="' + option.id + '">' + option.option + '</option>';
                });
                html += '       </select>';
                html += '       </div>';
                html += '   </div>';
                html += '</div>';
            });
            if (answers.length == 0 && questions.length > 0) {
                html += '<div class="member-info-det">';
                html += '   <div class="form-group">';
                html += '       <div class="col-md-12 text-center">';
                html += '           <a id="btn_calcular" cat-data="'+cat+'" href="#" class="button btn_tipo1 hvr-float-shadow " style="margin-top: 10px; padding-top: 14px;margin-bottom: 10px; padding-bottom: 10px;">Calcular Opciones</a>';
                html += '       </div>';
                html += '	</div> ';
                html += '</div>';
            }
            $("#questions").html(html);
            $.each(answers, function (idx, asw) {
                $("#question_" + idx).val(asw);
            });
        }
        function drawComparisons() {
            $(".caja_pos").each(function (idx, id) {
                $(this).find("img").attr("src", "");
                $(this).find("img").attr("item-id", "");
            });
            $.each(comparison, function (idx, item) {
                $("#div_" + (idx + 1) + " img").attr("src", item.src);
                $("#div_" + (idx + 1) + " img").attr("item-id", item.id);
            });
        }
        function drawProducts(items) {
            var html = "";
            var hidden = false;
            $.each(items, function (index, item) {
                if(index == 5 && items.length > 6){
                    html += '           <div id="more-data" class="col-lg-4 col-md-6 col-sm-12">';
                    html += '               <div class="team-member modern">';
                    html += '                   <a href="#" item-id="view-more" class="view-more-trigger" >';
                    html += '                       <div class="member-photo imgevent" >';
                    html += '                           <img id="more-img" class="img-responsive center-block" src="<?php echo base_url() ?>images/more.png" alt="more" />';
                    html += '                       </div>    ';
                    html += '                   </a>';
                    html += '                   <div class="member-info">';
                    html += '                       <div class="post-date product-name text-center">';
                    html += '                           <h2>Haz click</br> <a href="#" class="view-more-trigger " >Para ver más</a></h2>';
                    html += '                       </div>';
                    html += '                   </div> ';
                    html += '                   <div class="member-socail ">';
                    html += '                  &nbsp;';
                    html += '                  </div>';
                    html += '              </div>';
                    html += '          </div>';
                    html += '          <div id="hidden-data" class="hide">';
                    hiddden = true;
                }
                html += drawCard(item);
                
            });
            if(hidden){
                html += '</div>';
            }
            return html;
        }
        function drawCard(item) {
            var html = '';
            html += '           <div class="col-lg-4 col-md-6 col-sm-12">';
            html += '               <div class="team-member modern">';
            html += '                   <a href="#" item-id="' + item.id + '" class=" modal_trigger" data-toggle="modal" data-target="#dg_detalle">';
            html += '                       <div class="member-photo imgevent" >';
            html += '                           <img id="' + item.id + '" class="img-responsive center-block" src="<?php echo base_url() ?>images/products/' + item.image + '" alt="" />';
            if(item.market_price > 0){
                
            html += '                           <div class="member-name color04">';
            html += '                               <span> $ ' + numberThousand(item.cost) + '</span> ';
            html += '                           </div>';
            html += '                           <div class="team-member-cross member-name color05">';
            html += '                               <span> $ ' + numberThousand(item.market_price) + '</span> ';
            html += '                           </div>';
            }else{
            html += '                           <div class="member-name color01">';
            html += '                               <span> $ ' + numberThousand(item.cost) + '</span> ';
            html += '                           </div>';
            }
            html += '                       </div>    ';
            html += '                   </a>';
            html += '                   <div class="member-info">';
            html += '                       <div class="post-date product-name">';
            var prod = (item.product.length > 30 )? item.product.substr(0,30)+"...":item.product;
            html += '                           <h2><a href="<?php echo base_url('looking/principal/browse')?>/'+ item.brand +'" >' + item.brand + '</a> </br> <a href="#" item-id="' + item.id + '" class="tit_tarjeta_al modal_trigger" data-toggle="modal" data-target="#dg_detalle">' + prod + '</a></h2>';
            html += '                       </div>';
            html += '                       <div class="post-date text-center">';
            var lastDate = new Date(item.last_update.date);
            var year = lastDate.getFullYear();
            var month = lastDate.getMonth();
            var day = lastDate.getDate();
            html += '                       Última actualización: ' + year + '-' + (month+1) + '-' + day;
            html += '                       </div>';
            html += '                   </div> ';
            html += '                   <div class="member-socail ">';
            var checked = false;
            $.each(comparison, function (idx, val) {
                if (val.id == item.id) {
                    checked = true;
                    return false
                }
            });
            if (checked) {
                html += '                       <a id="removeCompare" item-id="' + item.id + '" href="#" class="tarjeta_icon cls_add_comparar" data-id="" style="padding: 4px 4px; width:32px;height:32px;margin: 5 5px;display:inline-block; border-right: 2px solid #efefef;"><img class="img-responsive center-block" src="<?php echo base_url() ?>images/comparar2.png" alt="" />  </a>';
            } else {
                html += '                       <a id="addCompare" item-id="' + item.id + '" href="#" class="tarjeta_icon cls_add_comparar" data-id="" style="padding: 4px 4px; width:32px;height:32px;margin: 5 5px;display:inline-block; border-right: 2px solid #efefef;"><img class="img-responsive center-block" src="<?php echo base_url() ?>images/comparar.png" alt="" />  </a>';
            }
            if (item.favorite == 1) {
                html += '                      <a id="removeFavorite" item-id="' + item.id + '"  href="#" class="tarjeta_icon" style="padding: 4px 4px;width:32px;height:32px;margin: 5 5px; display:inline-block; border-right: 2px solid #efefef;"><img class="img-responsive center-block" src="<?php echo base_url() ?>images/favoritos2.png" alt="" />  </a>';
            } else {
                html += '                      <a id="addFavorite" item-id="' + item.id + '"  href="#" class="tarjeta_icon" style="padding: 4px 4px;width:32px;height:32px;margin: 5 5px; display:inline-block; border-right: 2px solid #efefef;"><img class="img-responsive center-block" src="<?php echo base_url() ?>images/favoritos.png" alt="" />  </a>';
            }
            html += '                      <a href="#" item-id="' + item.id + '" style="padding: 4px 4px;display:inline-block;margin: 5 20px;border-left: 1px #EEE;" class="tit_tarjeta_c modal_trigger" data-toggle="modal" data-target="#dg_detalle" >Detalles</a>  ';
            html += '                  </div>';
            html += '              </div>';
            html += '          </div>';
            return html;
        }
        function drawModal(id) {
            $.ajax({
                type: 'post',
                data: {id: id},
                url: '<?php echo base_url("looking/properties/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        $("#detail_name").html(data.data.info.product);
                        $("#detail_brand").html(data.data.info.brand);
                        var browseUrl = "<?php echo base_url("looking/principal/browse/")?>/";
                        $("#detail_brand").attr("href",  browseUrl +data.data.info.brand);
                        $("#detail_description").html(data.data.info.description);
                        $(".regular-price #detail_price").html(numberThousand(data.data.info.cost));
                        $(".promo-price #detail_price").html(numberThousand(data.data.info.cost));
                        $(".promo-price #detail_old_price").html(numberThousand(data.data.info.market_price));
                        if(data.data.info.market_price > 0 ){
                            $(".regular-price").addClass("hide")
                            $(".promo-price").removeClass("hide")
                        }else{
                            $(".promo-price").addClass("hide")
                            $(".regular-price").removeClass("hide")
                        }
                        $("#detail_image").attr("src", "<?php echo base_url("images/products") ?>/" + data.data.info.image);
                        $(".detail_add").attr("item-id", data.data.info.id);
                        $(".detail_add").attr("item-cost", data.data.info.cost);
                        var html = '';
                        $.each(data.data.details, function (index, detail) {
                            html += '<div class="member-info-det">';
                            html += '   <div class="col-md-7 tit_tarjeta_f1">' + detail.label + '</div>';
                            html += '   <div class="col-md-5 tit_tarjeta_f2 ">' + detail.value + '</div>';
                            html += '</div>';
                        });
                        $("#detail_properties").html(html);
                        if (data.data.info.favorite == 1) {
                            $("#img-favorite").attr('src', '<?php echo base_url("images/favoritos2.png") ?>');
                        } else {
                            $("#img-favorite").attr('src', '<?php echo base_url("images/favoritos.png") ?>');
                        }
                        if (data.data.info.method == <?php echo AuthConstants::PM_GENERATE_CODE ?>) {
                            $(".detail_add").html('<?php echo lang("add_code") ?>');
                        } else {
                            $(".detail_add").html('<?php echo lang("add") ?>');
                        }
                        $("#img-compare").attr('src', '<?php echo base_url("images/comparar.png") ?>');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        }
        function addItem(id, cost, quotes) {
//            if(isLogged()){
                var submitdata = {
                    "id": id,
                    "cost": cost,
                    "quotes": quotes,
                    "available": 1
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>looking/addItem/",
                    type: "post",
                    dataType: "json",
                    data: submitdata,
                    success: function (data) {
                        if (data.status == true) {
                            $(".badge").html(data.data)
                            $(".badge").toggleClass("bounce");
                            setTimeout(function () {
                                $(".badge").toggleClass("bounce")
                            }, 3000);
                            $('#dg_detalle').modal('hide');
    //                        $("#btn-add").addClass('hide');
    //                        $("#btn-add-more").removeClass('hide');
    //                        $(".checkout-hide").addClass('hide');
    //                        $(".checkout-show").removeClass('hide');
    //                        $("#btn-checkout").removeClass('hide');
    //                        $("#final-quotes").html(submitdata.quotes);
    //                        $(".total").addClass('text-center');
                        } else {
    //                        $('#modal-message').foundation('reveal', 'close');
    //                        $("#modal-message #message").html('<?php echo lang('product_unavailable') ?>');
    //                        $('#modal-message').foundation('reveal', 'open');
                        }
                    }
                });
//            }
        }
        function drawCart() {
            $("#contenido_").html("<br/><br/><br/><br/><br/><br/><br/><br/><img src='<?php echo base_url() ?>images/AjaxLoader.gif'  class='center-block'  >");
            $.ajax({
                type: 'post',
                data: {
                    id_: '1',
                    des: '--',
                },
                url: '<?php echo base_url("looking/cart/"); ?>',
                dataType: "json",
                success: function (data) {
                    var html = '';
                    $("#contenido_").html(data.data.tmpl);
                    $("#cid").val(data.data.cid);
                    $("#name").val(data.data.name);
                    $("#last_name").val(data.data.last_name);
                    $("#phone").val(data.data.phone);
                    $("#mobile").val(data.data.mobile);
                    $("#address").val(data.data.address);
                    $("#email").val(data.data.email);
                    $("#dni").val(data.data.dni);
                    var option = $("<option><?php echo lang("select_value") ?></option>")
                    $("#docType").append(option);
                    $.each(data.data.docTypes, function (idx, docType) {
                        var option = $("<option></option>")
                                .attr("value", docType.id)
                                .text(docType.name);
                        $("#docType").append(option);
                    });
                    $("#docType").val(data.data.docType);

                    if (parseInt(data.data.country) > 0) {
                        $("#country").val(parseInt(data.data.country));
                        $("#country").trigger('change');
                        var state = data.data.state;
                        if (parseInt(state) > 0) {
                            setTimeout(function () {
                                $("#state").val(parseInt(state));
                                $("#state").trigger('change');
                            }, 500);
                            var city = data.data.city;
                            if (parseInt(city) > 0) {
                                setTimeout(function () {
                                    $("#city").val(parseInt(city));
                                    $("#city").trigger('change');
                                }, 1000);
                            }
                        }
                    }
                    var html = "";
                    var total = 0;
                    $.each(data.data.cart, function (idx, cartItem) {
                        html += '<div id="row_'+ cartItem.id +'" class="member-info-det">';
                        html += '   <div >';
                        html += '       <div class="col-md-9 tit_tarjeta_f1"  >';
                        html += '           <a id="delete_'+ cartItem.id +'" class="action_list action_delete" title="Delete" href="#"><img src="<?php echo base_url() ?>images/iconclose.png" width="16"></a>';
                        html += '           <label for="c' + cartItem.id + '" >';
                        html += '               <span style="border-top: 0px!important;" ></span>';
                        html += '           </label>&nbsp;&nbsp;' + cartItem.name;
                        html += '       </div> ';
                        html += '       <div class="col-md-3 tit_21"> $ ' + numberThousand(cartItem.cost) + ' </div> ';
                        html += '   </div>';
                        html += '</div>';
                        total = total + parseInt(cartItem.cost);
                    });
                    $("#cart-products").html(html);
                    $("#total-value").html("$ " + numberThousand(total));
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                }
            });
        }
        function directSearch(keyword) {
//        `   var submitdata = {
//                    "globalSearch": keyword,
//                    <?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
//            };
            $.ajax({
                    type: 'post',
                    data: {globalSearch:keyword},
                    url: '<?php echo base_url("looking/directResults/"); ?>',
                    dataType: "json",
                    success: function (data) {
                        if (data.status == true) {
                            $("#contenido_").html(data.tmpl);
                            $("#main-title").html('COMPARA Y COMPRA<br><p class="tit_20">COMPARA LAS MEJORES OPCIONES, SI NO ESTAS CONVENCIDO FILTRA DE NUEVO  </p>');
                            //                            $("#filter-title").html("Filtra los favoritos <br /><br />");
                            var html = '';
                               html += drawProducts(data.results);

                            $("#grid-results").html(html);

                            drawQuestions(data.questions, data.answers);
                            drawComparisons();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#contenido_").html("<small>" + xhr.responseText + "</small>");
                    }
                });
        }
        function isLogged() {
            var sessid = "<?php echo ($this->session->userdata(AuthConstants::USER_ID)) ?>";
            if(sessid == ""){
                $("#dg_error #error-title").html("Ingresa con tu usuario");
                $("#dg_error #error-msg").html("Para realizar transacciones o ver favoritos, debes estar registrado");
                $('#dg_error').modal('toggle');
                return false;
            }else{
                return true;
            }
        
        }
        function numberThousand(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }
        $("#contenido_").on('focusout', 'input', function () {
            required($(this))
        });
        $("#contenido_").on('focusout change', 'select', function () {
//        $("select").bind('focusout change',function(){
            required($(this))
        });
        function validate() {
            $(".required").each(function () {
                required($(this));
            });
            if ($(".validation-error")[0]) {
                return false;
            } else {
                return true;
            }
        }
        function required(obj) {
            $(obj).removeClass("validation-error")
            if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
    });

</script>