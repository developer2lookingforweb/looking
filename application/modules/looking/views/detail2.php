<?php
		function uri_parse($string){
			return $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		}
    $images = explode("|", $info["image"]);
    $noImages = count($images);
    $images[0] = file_exists(("./images/products/".$images[0]))?$images[0]:"noimage.jpg";
	$buttonDisabled = ($info['stock'] >0)?"":"disabled";
	$itemCost = ($info["offer"]>0)?$info["offer"]:$info["cost"];
?>
<script src="<?php echo base_url()?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/themes/dark.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/animate/animate.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/responsive/responsive.min.js"></script>


	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size19 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>
                    <div class="slick3">
                    <?php foreach($images as $image){?>
						<div class="item-slick3" data-thumb="<?php echo base_url()?>images/products/<?php echo $image?>">
							<div class="wrap-pic-w">
								<img src="<?php echo base_url()?>images/products/<?php echo $image?>" alt="IMG-PRODUCT">
							</div>
						</div>
                        
                        
                    <?php }?>

					</div>
				</div>
			</div>

			<div class="w-size20 p-t-30 respon5 p-l-25">
                <h4 class="product-detail-name m-text16 p-b-13">
                    <?php echo $info["product"]?>
				</h4>
                
                <h6 class="product-detail-name m-text10 p-b-13">
                    <?php echo $info["brand"]?>
                </h6>
				<span class="m-text17">
                <?php if($info["stock"]==0){?>
                       <span class="text-warning"> PRODUCTO AGOTADO!</span>
                <?php }else{?>
                    <?php if($info["offer"]>0){?>
                        <span class="block2-newprice m-text7 p-r-5">
                        $ <?php echo number_format($info["offer"])?>
                        </span>
                        
                        <span class="block2-oldprice m-text7 p-r-5">
                            $<?php echo number_format($info["cost"])?>
                        </span>
                        
                    <?php }else{?>
                        <?php if($info["alternative_cost"]>0){?>
                            Inscripción  :  $ <?php echo number_format($info["cost"])?>
                            Matrícula :  $ <?php echo number_format($info["alternative_cost"])?>
                            <?php }else{?>
                                $ <?php echo number_format($info["cost"])?>
                            <?php }?>
                            <?php if($info["market_price"]>0){?>
                                    
                                    <span class="block2-oldprice m-text7 p-r-5">
                                        $<?php echo number_format($info["market_price"])?>
                                    </span>
                                    
                            <?php }?>
                    <?php }?>
                <?php }?>
				</span>

				<p class="s-text8 p-t-10">
                    El costo hace referencia a pago de contado
				</p>
				<p class="s-text8 p-t-10">
                    El tiempo de entrega estimado es de 24 hrs para pagos online.
				</p>

				<!--  -->
				<div class="p-t-33 p-b-60">
					

					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="1">

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<a href="#" class="add-product flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4 <?php echo $buttonDisabled?>"  item-offer="<?php echo $info["offerId"]?>" item-id="<?php echo $info["id"]?>" item-cost="<?php echo $itemCost?>">
									Comprar
								</a>
							</div>
							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10" style="display: none;">
								<?php if(in_array($info["category"], explode(",", AuthConstants::FN_AVAILABLE_FINANCIAL))){?>
									<a class="add-product-credit btn btn-sm flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4 m-l-30" href="<?php echo base_url("credit/" . $info["id"] . "/") ?>">
										<i class="fa fa-credit-card "></i>&nbsp;x cuotas
									</a>
								<?php }?>
							</div>
						</div>
					</div>
				</div>


				
			</div>
			<!-- <div class="row">		 -->
            <div class="col-md-12 product-info">
                <ul id="myTab" class="nav nav-tabs nav_tabs responsive">

                    <li class="active"><a href="#service-one" data-toggle="tab"><?php echo lang("description")?></a></li>
                    <li><a href="#service-two" data-toggle="tab">Información General</a></li>
                    <?php if($show_files){?>
                        <li><a href="#service-three" data-toggle="tab">Contenido Académico</a></li>
                    <?php }?>
                    <!--<li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>-->

                </ul>
                <div id="myTabContent" class="tab-content responsive">
                    <div class="tab-pane fade in active product-info" id="service-one">

                        <?php echo $info["description"]?>

                    </div>
                    <div class="tab-pane fade product-info" id="service-two">
                        <div class="container">
                        <?php foreach($details as $property){?>
                            <div class="col-lg-7 my_featureRow">
								<div class="row" >
									<div class="col-xs-12 col-sm-4 col-lg-5 my_feature">
										<?php echo $property["label"]?>
									</div>
									<div class="col-xs-12 col-sm-8  col-lg-7">
										<div class="row">
											<div class="col-xs-12 col-sm-12 my_planFeature my_plan1">
												<?php echo $property["value"]?>
											</div>
										</div> 
									</div>
								</div>
                            </div>
                        <?php }?>
                        </div>
                    </div>
                    <div class="tab-pane fade product-info" id="service-three">
                         <?php if($show_files){?>
                            <div class="container">
                                <div class="col-lg-7 my_featureRow">
                                    <div class="col-xs-12 col-sm-4 my_feature">
                                        Contenido
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 my_planFeature my_plan1">
                                                <a class="file-popup" type="<?=$file_type?>" href="<?php echo base_url("files/".$info["id"].".".$file_type)?>">Ver Información</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <hr>
            </div>
        <!-- </div> -->
		</div>
	</div>

	
	<?php if(count($related) >0 ) { ?>
	<!-- Relate Product -->
	<section class="relateproduct bgwhite p-t-45 p-b-138">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Productos Relacionados
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
				<?php foreach ($related as $idx => $prd) { ?>
						<?php echo drawBasicCard($prd) ?>
				<?php } ?>

				</div>
			</div>

		</div>
	</section>

	<?php } ?>


    
    <?php 

                function drawBasicCard($prd) {
					 $image = explode("|", $prd["image"]); 
					 $image[0] = file_exists(("./images/products/".$image[0]))?$image[0]:"noimage.jpg";
					?>
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative">
								<img src="<?php echo base_url(); ?>images/products/<?php echo $image[0] ?>" alt="<?php echo $prd["product"] ?>">

								<div class="block2-overlay trans-0-4">
									<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
										<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
										<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
									</a>

									<div class="block2-btn-addcart w-size1 trans-0-4">
										<!-- Button -->
										<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
											Agregar
										</button>
									</div>
								</div>
							</div>

							<div class="block2-txt p-t-20">
								<a href="<?php echo base_url("detail/" . $prd["id"] . "/" . uri_parse($prd["product"])) ?>" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $prd["product"] ?>
								</a>

								<?php if($prd["price"] > 0){ ?>
									<span class="block2-oldprice m-text6 p-r-5">
										$&nbsp;<?php echo number_format($prd["price"]) ?>
									</span>
								<?php } ?>
								<span class="block2-price m-text6 p-r-5">
									$&nbsp;<?php echo number_format($prd["cost"]) ?>
								
								</span>
							</div>
						</div>
					</div>

                    
                        <?php
                }
    ?>