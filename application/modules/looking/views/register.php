<div class="row">
    <div class="col-md-12">
        <!--<a href="#" class="pasos"><img class='img-responsive' src="<?php echo base_url(); ?>images/pasos.png" alt="" /></a>--> 
        <div class="loginw-box">
            <form id="registerForm" class="form-horizontal" role="form"  >
                <div class="modal-body">
                    <div class="">
                        <div class="col-md-12 text-center">
                            <p class="tit_loginredes"> Ingresa tus datos para iniciar el proceso y poder brindarte toda la información.  </p>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" id="name" name="name" class="form-control profile required alpha"  placeholder="Nombre" error-msg="No es un nombre válido">
                        </div> 
                        <div class="col-md-6">
                            <input type="text" id="last_name" name="last_name" class="form-control profile required alpha"  placeholder="Apellidos" error-msg="No es un apellido válido">
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" id="email" name="email" class="form-control profile required"  placeholder="Correo" error-msg="No es un correo válido">
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text"  id="mobile"  name="mobile" class="form-control profile required" placeholder="Celular" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            &nbsp;
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1"></div>  
                        <div class="col-md-12 text-center">
                            <button type="button" id="submit-credit" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">SOLICITAR</button>
                        </div>  
                        <div class="col-md-1"></div>  
                    </div>
                    
                </div> 	
            </form>  	
        </div>
    </div> 
    <div class="col-md-3">  
    </div> 
</div> 