<style>
    
      body {
        color: black;
        font-family: Helvetica;
        font-weight: 300;
      }
    /*#footer, #header{*/
     #footer{
        background: none;
        text-align: center;
        /*display: none;*/
     }
     #header{
        display: none;
    }
    .body-content {
        background: none;
    }
    .pdfrow::before, .pdfrow::after {
        content: " ";
        display: table;
      }
      .pdfrow {
        margin: 0 auto;width: 100%;
      }
      .pdfrow::after {
        clear: both;
      }
      
      label {
    color: #4d4d4d;
    cursor: pointer;
    display: block;
    font-size: 0.875rem;
    font-weight: normal;
    line-height: 1.5;
    margin-bottom: 0;
  }
    
.pdfcolumn.large-centered, .pdfcolumns.large-centered {
  float: none;
  margin-left: auto;
  margin-right: auto;
}/**/
    .invoice{
        font-size: 1.8rem;
        color: #1e65af;
        font-style: italic;
      }
    .invoice-date {
        font-size: 1.2rem;
        font-style: italic;
        padding-bottom: 5%;
      }
    .seller-info, .buyer-info{
        border-radius: 15px;
        background: #ededed;
        min-height: 150px;
    }
    label.customer{
        font-weight: 400;
        font-size: 1.1rem;
    }
    label.name{
        font-style: italic;
        font-size: 1.3rem;
    }
    .invoice-head{
        border-bottom: 3px solid #cfcfcf;
        padding-bottom: 4px;
    }
    .products{
        min-height: 300px;
    }
    .products-head label{
        color: #FFF;
    }
    .products-head div{
        font-size: 1.1rem;
        background: #1e65af;
        padding: 2px 0;
        margin: 3px 0;
        
    }
    .products-row div{
        border-bottom: 1px solid #cfcfcf;
        padding: 10px 0;
    }
    .total {
        font-weight: 400;
        border-top: 1px dotted #cfcfcf;
        border-bottom: 1px solid #cfcfcf;
        padding: 14px 0;
    }
</style>

<div class="" style="margin: 0 auto;width: 100%;">
    <div class="large-centered" style="width: 730px;">
        <div class=" invoice-head" style="margin: 0px auto; width: 100%; height: 370px;">
            <div class=" text-left" style="width:360px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;height: 220px;">
                <img src="<?php echo FCPATH;?>images/logomailblue.png"/>
                <label><?php echo $disclamer;?></label>
                <label><?php echo $resolution;?></label>
                <label><?php echo $resolution2;?></label>
            </div>
            <div class="text-right" style="width:325px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;height: 220px;">
                <img src="<?php echo FCPATH.$qr;?>" style="text-align: right;margin-left: 220px;" />
                <div class="invoice"  style="text-align: right;">
                    Factura No. <?php echo $reference;?>
                </div>
                <div class="invoice-date" style="text-align: right;">
                    <?php echo $referenceDate;?>
                </div>
            </div>
            <div class=" seller-info" style="margin-top: 220px; margin-left: -380px;width: 300px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;">
                <label >&nbsp;</label>
                <label class="name"><?php echo $sellerName ?></label>
                <label><?php echo $sellerNit ?></label>
                <label><?php echo $sellerAddress ?></label>
                <label><?php echo $sellerPhone ?></label>
                <label><?php echo $sellerWeb ?></label>
            </div>
            <div class=" buyer-info" style="margin-left: -290px;margin-top: 220px;width:300px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;">
                <label class="customer"><?php echo lang('customer') ?></label>
                <label class="name"><?php echo $customerName ?></label>
                <label><?php echo $customerDni ?></label>
                <label><?php echo $customerAddress ?></label>
                <label><?php echo $customerMobile ?> </label>
<!--                <label><?php echo $customerCity ?> </label>-->
                <!--<label><?php echo $customerState ?></label>-->
                <label><?php echo $customerEmail ?></label>
            </div>


        </div>
        <div class="products" style="">
            <div class="products-head" style="margin: 0 auto;width: 730px;">
                <div class="" style="width: 61px;float: left;text-align: center"><label>Item</label></div>
                <div class="" style="width:366px;float: left;text-align: center"><label>Producto</label></div>
                <div class="" style="width: 61px;float: left;text-align: center"><label>Cantidad</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>Costo</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>Total</label></div>

            </div>
        </div>
        <div  style="">
            <?php 
            foreach($items as $product){
            ?>
            <br/>
            <div class="products-row" style="margin-top:20px;width: 730px; height:15px ;display: block;">
                <div class="" style="width: 61px;float: left;text-align: center; "><label><?php echo $product['cod']?></label></div>
                <div class="" style="width:366px;float: left;"><label><?php echo $product['product']?></label></div>
                <div class="" style="width: 61px;float: left;text-align: center"><label><?php echo $product['quantity']?></label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>$<?php echo number_format($product['cost'])?></label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>$<?php echo number_format($product['total'])?></label></div>
            </div>
            <?php 
            }
            ?>
        </div>
        <div class="total" style="margin: 0 auto;width: 730px; margin-top: 350px;">
            <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('subtotal') ?></label></div>
                <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($subtotal)?></label></div>
            </div>
            <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('iva') ?></label></div>
                <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($iva)?></label></div>
            </div>
            <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('total') ?></label></div>
                <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($total)?></label></div>
            </div>
        </div>
    </div>
</div>