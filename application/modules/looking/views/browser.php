<div class="row">
    <input type="hidden" name="c4e429d1506c7d66d56ad1843cfe4f2c" value="fWDnxEXM_csrf_token_name"/>
    <div id="question-slider" class="large-12 columns center ">
        <ul class="example-orbit" data-orbit>
            <li >
                <div class="large-6 left question">
                    <label class="large-12">Cuál es tu email?</label>
                    <input type="text" id="name" name="name"/>
                </div>
                <img src="../images/gallery/888.jpg" />
            </li>
<!--            <li>
                <div class="large-9 left question">
                    <label class="large-12">Cómo es tu correo electrónico?</label>
                    <input class="large-8" type="text" name="mail"/>
                </div>
                <img src="images/gallery/111.jpg" />
            </li>-->
<!--            <li>
                <div class="large-6 left question">
                    <label class="large-12">Qué edad tienes?</label>
                    <input type="text" name="edad"/>
                </div>
                <img src="images/gallery/222.jpg" />
            </li>-->
<!--            <li>
                <div class="large-6 left question">
                    <label class="large-12">Cuál es tu ocupación?</label>
                    <input type="text" name="ocupacion"/>
                </div>
                <img src="images/gallery/333.jpg" />
            </li>-->
            <li>
                <div class="large-6 left question">
                    <label class="large-12">Prefieres Chatear o hablar?</label>
                    <!--<input type="select" name="minutos"/>-->
                    <select name="minutos">
                        <option value="1">Hablar</option>
                        <option value="2">Chatear</option>
                        <option value="3">Hablar y chatear</option>
                    </select>
                </div>
                <img src="../images/gallery/444.jpg" />
            </li>
            <li>
                <div class="large-6 left question">
                    <label class="large-12">Usas redes Wifi</label>
                    <!--<input type="text" name="navegacion"/>-->
                    <select name="navegacion">
                        <option value="4">Siempre</option>
                        <option value="5">Nunca</option>
                        <option value="13">Ocasionalmente</option>
                    </select>
                </div>
                <img src="../images/gallery/555.jpg" />
            </li>
            <li>
                <div class="large-6 left question">
                    <label class="large-12">Que aplicaciones usas</label>
                    <!--<input type="text" name="navegacion"/>-->
                    <select class="multioption" name="app" multiple="multiple">
                        <option value="6">Spotify</option>
                        <option value="7">Waze</option>
                        <option value="8">Netflix</option>
                        <option value="9">Facebook</option>
                        <option value="10">Twitter</option>
                        <option value="11">Instagram</option>
                        <option value="12">Mas de 6</option>
                    </select>
                </div>
                <img src="../images/gallery/666.jpg" />
            </li>
<!--            <li>
                <div class="large-6 left question">
                    <label class="large-12">Pregunta 7</label>
                    <input type="text" name="campo7"/>
                </div>
                <img src="images/gallery/666.jpg" />
            </li>
            <li>
                <div class="large-6 left question">
                    <label class="large-12">Pregunta 8</label>
                    <input type="text"/>
                </div>
                <img src="images/gallery/777.jpg" />
            </li>-->
        </ul>
        <div class="large-4 large-offset-8">
            <button id="browse" class="">Ver resultados</button>
        </div>
    </div>
    <div id="result-grid" class="large-12 columns center hide">
        <div class="large-12 columns">
            <div class="row result-grid-row">
                
            </div> 
        </div> 

        
    </div>
</div>