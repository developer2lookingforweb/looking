<?php 
    $opCountry = array();
    $opCountry[""] = lang("default_select");
    foreach ($countries as $aCountry){
        $opCountry[$aCountry->getId()] = $aCountry->getName();
    }

    $opCity = array();
    $opCity[""] = lang("default_select");
    foreach ($cities as $aCity){
        $opCity[$aCity->getId()] = $aCity->getName();
    }

    $opLanguages = array();
    $opLanguages[""] = lang("default_select");
    foreach ($languages as $aLanguage){
        $opLanguages[$aLanguage] = lang($aLanguage);
    }

    $opProfile = array();
    $opProfile[""] = lang("default_select");
    foreach ($profiles as $aProfile){
        if ($aProfile->getId() != AuthConstants::ID_PROFILE_USER){
            $opProfile[$aProfile->getId()] = $aProfile->getName();
        }
    }
    
    $fields = array();
    $fields["c1"][lang('country')] = form_dropdown("idCountry", $opCountry, $idCountry, "class='span10'");
    $fields["c2"][lang('city')] = form_dropdown("idCity", $opCity, $idCity, "class='span10'");
    $fields["c1"][lang('name')] = form_input(array('name'=>'name', 'class'=>'span10', 'value'=>$name));
    $fields["c2"][lang('last_name')] = form_input(array('name'=>'lastName', 'class'=>'span10', 'value'=>$last_name));
    $fields["c1"][lang('email')] = form_input(array('name'=>'email', 'class'=>'span10', 'value'=>$email));
    $fields["c2"][lang('password')] = form_password(array('name'=>'password', 'class'=>'span10'));
    $fields["c1"][lang('identification')] = form_input(array('name'=>'identification', 'class'=>'span10', 'value'=>$identification));
    $fields["c2"][lang('language')] = form_dropdown("language", $opLanguages, $language, "class='span10'");
    $fields["c1"][lang('profile')] = form_dropdown("idProfile", $opProfile, $idProfile, "class='span4'");
    
    $hidden = array('id' => $id);
    echo print_form('/usersdata/persist/', $fields, $hidden,"form",false,12);