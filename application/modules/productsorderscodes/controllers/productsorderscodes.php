<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductsOrdersCodes extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["validate_productordercode"] = site_url("productsorderscodes/formValidation");
		
		$data = array();
		$data["title"] = lang("ProductsOrdersCodes");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("ProductsOrdersCodes", "cp", array("id" => array("id","a") ,'code' => array('code',"c")  ,'redeemed' => array('redeemed',"d") ));
        $model->setNumerics(array("cp.id"));
        
        $porder = new Model("ProductsOrders", "bpo");
        $porder->setRelation("product_order");
        $product = new Model("Products", "apr", array("product"=>array("product","b") ));
        $product->setModelJoin("bpo");
        $product->setRelation("product");
        $order = new Model("Orders", "do", array());
        $order->setModelJoin("bpo");
        $order->setRelation("order");
        $customer = new Model("Customers", "cc", array("email"=>array("email","e")));
        $customer->setModelJoin("do");
        $customer->setRelation("customer");
        
//        if ($this->session->userdata("country") > 0){
//            $order->setConditions(array("co.id = ".$this->session->userdata("country")));
//        }
        
        $relations = array();
        array_push($relations, $porder);
        array_push($relations, $product);
        array_push($relations, $order);
        array_push($relations, $customer);
        
        
        
        $actions = array();
//        array_push($actions, new Action("productsorderscodes", "form", "edit"));        
//        array_push($actions, new Action("productsorderscodes", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
        $this->relations = $relations;
	}
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $productOrder = $this->input->post('productOrder');
		$code = $this->input->post('code');
		$redeemed = $this->input->post('redeemed');
        
        if ($identifier > 0){
            $output = $this->rest->get('productsorderscodes/productordercode/', array("id"=>$identifier));
        
            if ($output->status){
                $productordercode    = $output->data;
                $productOrder = $productordercode->productOrder;
				$code = $productordercode->code;
				$redeemed = $productordercode->redeemed;
                $id         = $productordercode->id;
            }
        }
        
        $actions = array();
        $actions["return_productordercode"] = site_url("productsorderscodes/index");
        
        $data = array();
        $data["title"]  = lang("ProductsOrdersCodes");
        $data['productOrder'] = $productOrder;
		$data['code'] = $code;
		$data['redeemed'] = $redeemed;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    public function formValidation ()
    {
                
        $actions = array();
        $actions["return_productordercode"] = site_url("productsorderscodes/index");
        
        $data = array();
        $data["title"]  = lang("ProductsOrdersCodes");
        $data['productOrder'] = "";
		$data['code'] = "";
		$data['redeemed'] =  "";
        $data["id"] = 0;
        $this->view('formValidation', $data, $actions);
    }
    
    public function redeemeCode (){
        $this->loadRepository("ProductsOrdersCodes");
        $codeRow = $this->ProductsOrdersCodes->findOneBy(array('code'=>  $this->input->post('code')));
        $return = array();
        if($codeRow !== null){
            if($codeRow->getRedeemed() !== 1){
                $codeRow->setRedeemed(1);
                $codeRow->setRedeemedDate(new DateTime(date("Y-m-d H:i:s")));
                $this->em->persist($codeRow);
                $this->em->flush();
                echo json_encode(array('status'=>true,'msg'=>lang("code_redeemed_successfully")));
            }  else {
                echo json_encode(array('status'=>false,'msg'=>lang("code_already_redeemed")));
            }
        }  else {
            echo json_encode(array('status'=>false,'msg'=>  lang("code_notFound")));
        }
    }
    public function seeCodeData (){
        $this->loadRepository("ProductsOrdersCodes");
        $codeRow = $this->ProductsOrdersCodes->findOneBy(array('code'=>  $this->input->post('code')));
        $return = array();
        if($codeRow !== null){
            $return['cost'] = $codeRow->getValue();
            $return['product'] = $codeRow->getProductOrder()->getProduct()->getProduct();
            $return['name']= $codeRow->getProductOrder()->getOrder()->getCustomer()->getName();
            $return['lastName'] = $codeRow->getProductOrder()->getOrder()->getCustomer()->getLastName();
            $return['address'] = $codeRow->getProductOrder()->getOrder()->getCustomer()->getAddress();
            $return['phone'] = $codeRow->getProductOrder()->getOrder()->getCustomer()->getPhone();
            $return['mobile'] = $codeRow->getProductOrder()->getOrder()->getCustomer()->getMobile();
            echo json_encode(array('status'=>true,'data'=>$return));
        }else{
            echo json_encode(array('status'=>false,'msg'=>  lang("code_notFound")));
        }
    }
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('productOrder', 'lang:productOrder', 'required');
		$this->form_validation->set_rules('code', 'lang:code', 'required');
		$this->form_validation->set_rules('redeemed', 'lang:redeemed', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('productsorderscodes/productordercode', $this->input->post()); 
            }else{
                $output = $this->rest->put('productsorderscodes/productordercode', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('productordercode_edition') : lang('productordercode_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('productsorderscodes/productordercode', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("productordercode_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}