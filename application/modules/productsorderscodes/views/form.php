<?php 
    $fields = array();
    $fields[lang('productOrder')] = form_input(array('name'=>'productOrder', 'class'=>'span3 focused', 'value'=>$productOrder));
	$fields[lang('code')] = form_input(array('name'=>'code', 'class'=>'span3 focused', 'value'=>$code));
	$fields[lang('redeemed')] = form_input(array('name'=>'redeemed', 'class'=>'span3 focused', 'value'=>$redeemed));
    $hidden = array('id' => $id);
    echo print_form('/productsorderscodes/persist/', $fields, $hidden);