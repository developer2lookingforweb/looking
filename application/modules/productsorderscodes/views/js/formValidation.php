<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                productOrder: 'required',
				code: 'required',
				redeemed: 'required',
            },
            messages: {
                productOrder:'<?=lang('required')?>',
				code:'<?=lang('required')?>',
				redeemed:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
        
        $("#validate").click(function(){
            if($("input[name='code']").val() !== ""){
                $.ajax({
                    url: "<?php echo base_url("productsorderscodes/seeCodeData");?>",
                    type: "post",            
                    data: {code:$("input[name='code']").val(),
                            <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                        },
                    dataType: "json",
                    success: function(data){   

                        if(data.status != false){
                            var html= '';
                            html += "<div class='columns large-8'>";
                            html += "   <h5><strong><?php echo lang("product")?><strong></h5>";
                            html += "</div>";
                            html += "<div class='columns large-4'>";
                            html += "   <h6><strong><?php echo lang("price")?><strong></h6>";
                            html += "</div>";
                            html += "<div class='columns large-8'>";
                            html += "   <h5>"+data.data.product+"</h5>";
                            html += "</div>";
                            html += "<div class='columns large-4'>";
                            html += "   <h6>$"+data.data.cost+"</h6>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label><strong><?php echo lang("name")?></strong></label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label><strong><?php echo lang("last_name")?></strong></label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label>"+data.data.name+"</label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label>"+data.data.lastName+"</label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label><strong><?php echo lang("phone")?></strong></label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label><strong><?php echo lang("mobile")?></strong></label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label>"+data.data.phone+"</label>";
                            html += "</div>";
                            html += "<div class='columns large-6'>";
                            html += "   <label>"+data.data.mobile+"</label>";
                            html += "</div>";
                            html += "<div class='columns large-12'>";
                            html += "   <label><strong><?php echo lang("address")?></strong></label>";
                            html += "</div>";
                            html += "<div class='columns large-12'>";
                            html += "   <label>"+data.data.address+"</label>";
                            html += "</div><div>&nbsp;</br>";
                            $("#validation-data").html(html);
                        }else{
                            $('#alert').addClass("alert");
                            $("#message").html(data.msg);
                            $("#alert").show();
                        }
                    }
                });
            }else{
                $('#alert').addClass("alert");
                $("#message").html("<?=lang('code')?> <?=lang('required')?>");
                $("#alert").show();
                
            }
        });
        $("#redeeme").click(function(){
            if($("input[name='code']").val() !== ""){
                $.ajax({
                    url: "<?php echo base_url("productsorderscodes/redeemeCode");?>",
                    type: "post",            
                    data: {code:$("input[name='code']").val(),
                            <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                        },
                    dataType: "json",
                    success: function(data){   

                        if(data.status != false){
                            var html= '';
                            $('#alert').addClass("success");
                            $("#message").html(data.msg);
                            $("#alert").show();
                            $("#validation-data").html(html);
                            $("input[name='code']").val("")
                        }else{
                            $('#alert').addClass("alert");
                            $("#message").html(data.msg);
                            $("#alert").show();
                        }
                    }
                });
            }else{
                $('#alert').addClass("alert");
                $("#message").html("<?=lang('code')?> <?=lang('required')?>");
                $("#alert").show();
                
            }
        });
    });
</script>