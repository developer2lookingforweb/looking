<div class="large-6 columns large-centered">
    <form id="form" name="form" accept-charset="utf-8" method="post" action="<?php echo base_url("productsorderscodes/redimeCode/")?>" novalidate="novalidate">
        <div style="display:none">
            <input type="hidden" value="0" name="id">
        </div>
        <div class="row">
            <div class="row collapse">
                <div class="large-4 columns">
                    <span class="label"><?php echo lang("code")?></span>
                </div>
                <div class="large-8 columns">
                    <input type="text" class="span3 focused" value="" name="code">
                </div>
            </div>
            <div class="row collapse">
                <div class="large-10 columns"><br>
                </div>
            </div>
        </div>
        <div id="validation-data">
        </div>
        <div class="row collapse">
            <div class="medium-2 large-2 columns">&nbsp;</div>
            <button id="validate" class="medium-3 large-3 columns " type="button" name=""><?php echo lang("validate")?></button>
            <div class="medium-2 large-2 columns">&nbsp;</div>
            <button id="redeeme" class="medium-3 large-3 columns " type="button" name=""><?php echo lang("redeeme")?></button>
            <div class="medium-2 large-4 columns hide-for-small">&nbsp;
            </div>

        </div>
        <div style="display: none;" class="alert-box" id="alert" data-alert="">
            <div id="message">

            </div>
            <a class="close" href="#">×</a>
        </div>
    </form>
</div>


<?php 
//    $fields = array();
//	$fields[lang('code')] = form_input(array('name'=>'code', 'class'=>'span3 focused', 'value'=>$code));
//    $hidden = array('id' => $id);
//    echo print_form('/productsorderscodes/redimeCode/', $fields, $hidden);