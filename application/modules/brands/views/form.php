<?php 
    $fields = array();
    $fields[lang('brand')] = form_input(array('name'=>'brand', 'class'=>'span3 focused', 'value'=>$brand));
	$fields[lang('image')] = form_input(array('name'=>'image', 'class'=>'span3 focused', 'value'=>$image));
	$fields[lang('status')] = form_input(array('name'=>'status', 'class'=>'span3 focused', 'value'=>$status));
    $hidden = array('id' => $id);
    echo print_form('/brands/persist/', $fields, $hidden);