<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                brand: 'required',
				image: 'required',
				status: 'required',
            },
            messages: {
                brand:'<?=lang('required')?>',
				image:'<?=lang('required')?>',
				status:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
    });
</script>