<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["create_brand"] = site_url("brands/form");
		
		$data = array();
		$data["title"] = lang("Brands");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Brands", "b", array("id" => "id", 'brand' => 'brand' ,'image' => 'image' ,'status' => 'status'));
        $model->setNumerics(array("b.id"));

        $actions = array();
        array_push($actions, new Action("brands", "form", "edit"));        
        array_push($actions, new Action("brands", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $brand = $this->input->post('brand');
		$image = $this->input->post('image');
		$status = $this->input->post('status');
        
        if ($identifier > 0){
            $output = $this->rest->get('brands/brand/', array("id"=>$identifier));
        
            if ($output->status){
                $brand    = $output->data;
                $brand = $brand->brand;
				$image = $brand->image;
				$status = $brand->status;
                $id         = $brand->id;
            }
        }
        
        $actions = array();
        $actions["return_brand"] = site_url("brands/index");
        
        $data = array();
        $data["title"]  = lang("Brands");
        $data['brand'] = $brand;
		$data['image'] = $image;
		$data['status'] = $status;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('brand', 'lang:brand', 'required');
		$this->form_validation->set_rules('image', 'lang:image', 'required');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('brands/brand', $this->input->post()); 
            }else{
                $output = $this->rest->put('brands/brand', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('brand_edition') : lang('brand_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('brands/brand', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("brand_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}