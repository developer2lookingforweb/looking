
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                    <img src="<?php echo base_url("images/logo1.png")?>" />
               </div>
                <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                    <h2>Recibo de Pago #<?php echo $reference?></h2>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Cliente</div>
                        <div class="panel-body">
                            <strong><?php echo $name?></strong><br>
                            <?php echo $address?><br>
                            <?php echo $email?><br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Información de la factura</div>
                        <div class="panel-body">
                            <strong>Factura:</strong> <?php echo $invoice?><br>
                            <strong>Fecha Factura:</strong> <?php echo $invoice_date?><br>
                            <strong>Fecha Pago:</strong> <?php echo $invoice_payment?><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Detalle</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Item</strong></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total =0;
                                foreach ($detail as $key => $value) {
                                    foreach ($value as $key => $det) {
                                    $total += $det['value'];
                                    ?>
                                <tr>
                                    <td><?php echo$det['description']?></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">$<?php echo$det['value']?></td>
                                </tr>

                                <?php }
                                
                                    }?>
                                
<!--                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td class="highrow text-right">$958.00</td>
                                </tr>-->
<!--                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Shipping</strong></td>
                                    <td class="emptyrow text-right">$20</td>
                                </tr>-->
                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Total</strong></td>
                                    <td class="highrow text-right">$<?php echo $total?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <a href="<?php echo base_url() ?>" class="add-product btn bg-primary " > Ir al inicio </a>
        </div>
    </div>
    </br>
    </br>
</div>

<style>
.container {
    margin-top: 120px;
    width: 60%;
}
@media (min-width: 320px) and (max-width: 480px) {
    .container {
        width: 90%;
    }
}
.height {
    min-height: 130px;
}

.icon {
    font-size: 47px;
    color: #EE4B28;
}

.iconbig {
    font-size: 77px;
    color: #EE4B28;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid #EE4B28;
}
</style>