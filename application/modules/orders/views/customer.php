    
<form action="http://local.lookingforweb.com/looking/productsorderscodes/persist" method="post" accept-charset="utf-8" name="form" id="form" novalidate="novalidate">

    <div class="large-10 large-centered columns">
        <input name="cid" id="cid"   type="hidden"  value=""  >
        <div class="guests" >
            <div class="row">
                <input  type="hidden" id="id" name="id" value="<?php echo $id ?>"/>
                <div class="large-2 columns "><?php echo lang('basic_info') ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="name" name="name" class="required" placeholder="<?php echo lang('name') ?>" value="<?php echo $name ?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="last_name" name="last_name" class="required" placeholder="<?php echo lang('last_name') ?>" value="<?php echo $last_name ?>"/>
                </div>
                <div class="large-4 columns ">
<!--                    &nbsp;<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email') ?>" value=""/>-->
                </div>                           
            </div>                           
            <div class="row">
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <select  id="docType" name="docType" class="required" placeholder="<?php echo lang('document_type') ?>" >
                        <option value=""><?php echo lang('select_value') ?></option>
                        <?php
                        foreach ($docTypes as $key => $dtype) {
                            $selected = "";
                            if (isset($docType)) {
                                if ($docType == $dtype['id']) {
                                    $selected = 'selected="selected"';
                                }
                            }
                            echo "<option value='" . $dtype['id'] . "' $selected>" . $dtype['name'] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="dni" name="dni" class="required" placeholder="<?php echo lang('dni') ?>" value="<?php echo $dni ?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="email" name="email"  class="required" placeholder="<?php echo lang('email') ?>" value="<?php echo $email ?>"/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-2 columns "><?php echo lang('location') ?></div>
                <div class="large-3 columns ">
                    <select id="country" name="country" class="required"  placeholder="<?php echo lang('country') ?>">
                        <option value=""><?php echo lang('select_value') ?></option>
                        <?php
                        foreach ($countries as $key => $country) {
//                            var_dump($value);
                            echo "<option value='" . $country['id'] . "'>" . $country['name'] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <select id="state" name="state" class="required"  placeholder="<?php echo lang('state') ?>">
                        <option value=""><?php echo lang('select_value') ?></option>
                    </select>
                </div>
                <div class="large-4 columns ">
                    <select id="city" name="city" class="required"  placeholder="<?php echo lang('city') ?>" class="required">
                        <option value=""><?php echo lang('select_value') ?></option>
                    </select>
                </div>                
            </div>
            <div class="row">
                <div class="large-2 columns "><?php echo lang('contact') ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="phone" name="phone" class="required" placeholder="<?php echo lang('phone') ?>" value="<?php echo $phone ?>"/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="mobile" name="mobile" class="required" placeholder="<?php echo lang('mobile') ?>" value="<?php echo $mobile ?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="address" name="address" class="required" placeholder="<?php echo lang('address') ?>" value="<?php echo $address ?>"/>
                </div>

            </div>
            <div class="row">
                <div class="large-3 columns large-push-9 ">
                    <a id="addCustomer" href="#" class="button radius"  ><i class=" icon-user"></i>&nbsp;Actualizar Usuario </a>
                </div>  
            </div>
        </div>
        <div class="guests" >
            <div class="row">
                <div class="large-2 columns "><?php echo lang('neighborhood') ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="neighborhood" name="neighborhood" class="" placeholder="<?php echo lang('neighborhood') ?>" value="<?php echo $neighborhood ?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="age" name="age" class="" placeholder="<?php echo lang('age') ?>" value="<?php echo $age ?>"/>
                </div>
                <div class="large-4 columns ">
    <!--                    &nbsp;<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email') ?>" value=""/>-->
                </div>                           
            </div>                           
            <div class="row">
                <div class="large-2 columns "><?php echo lang("company") ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="company" name="company" class="" placeholder="<?php echo lang('company') ?>" value="<?php echo $company ?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="comp_address" name="comp_address" class="" placeholder="<?php echo lang('address') ?>" value="<?php echo $comp_address ?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="comp_phone" name="comp_phone"  class="" placeholder="<?php echo lang('phone') ?>" value="<?php echo $comp_phone ?>"/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <input  type="text" id="earnings" name="earnings" class="" placeholder="<?php echo lang('earnings') ?>" value="<?php echo $earnings ?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="comp_site" name="comp_site" class="" placeholder="<?php echo lang('comp_site') ?>" value="<?php echo $comp_site ?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="comp_age" name="comp_age"  class="" placeholder="<?php echo lang('comp_age') ?>" value="<?php echo $comp_age ?>"/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <input  type="text" id="contract_type" name="contract_type" class="" placeholder="<?php echo lang('contract_type') ?>" value="<?php echo $contract_type ?>"/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="extra_earnings" name="extra_earnings" class="" placeholder="<?php echo lang('extra_earnings') ?>" value="<?php echo $extra_earnings ?>"/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="concept" name="concept"  class="" placeholder="<?php echo lang('concept') ?>" value="<?php echo $concept ?>"/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-3 columns large-push-9 ">
                    <a id="addJob" href="#" class="button radius"  ><i class=" icon-user"></i>&nbsp;Actualizar Info trabajo </a>
                </div>  
            </div>
            <div class="row">
                <div class="large-2 columns "><?php echo lang('references') ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="r_name" name="r_name" class="" placeholder="<?php echo lang('name') ?>" value=""/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="r_phone" name="r_phone" class="" placeholder="<?php echo lang('mobile') ?>" value=""/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="r_job" name="r_job" class="" placeholder="<?php echo lang('job') ?>" value=""/>
                </div>
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-10 columns list-references ">
                </div>

            </div>
            <div class="row">
                <div class="large-3 columns large-push-9 ">
                    <a id="addReference" href="#" class="button radius"  ><i class=" icon-user"></i>&nbsp;Agregar Referencia </a>
                </div>  
            </div>
            <div class="row">
                <div class="large-2 columns "><?php echo lang('properties') ?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="p_type" name="p_type" class="" placeholder="<?php echo lang('type') ?>" value=""/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="p_spec" name="p_spec" class="" placeholder="<?php echo lang('spec') ?>" value=""/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="p_value" name="p_value" class="" placeholder="<?php echo lang('value') ?>" value=""/>
                </div>
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <input  type="text" id="p_reference" name="p_reference" class="" placeholder="<?php echo lang('reference') ?>" value=""/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="p_pledge" name="p_pledge" class="" placeholder="<?php echo lang('pledge') ?>" value=""/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="p_balance" name="p_balance" class="" placeholder="<?php echo lang('balance') ?>" value=""/>
                </div>
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-10 columns list-assets ">
                </div>

            </div>
            <div class="row">
                <div class="large-3 columns large-push-9 ">
                    <a id="addAsset" href="#" class="button radius"  ><i class=" icon-user"></i>&nbsp;Agregar Propiedad </a>
                </div>  
            </div>
        </div>
    </div>
</form>

<!--     <form action="http://www.imei.info/api/checkimei/" method="POST">
        Login: <input type="text" name="login" />
        Password: <input type="password" name="password" />
        Imei: <input type="text" name="imei" />
        <button type="submit">GO!</button>
    </form>-->
<?php 
   