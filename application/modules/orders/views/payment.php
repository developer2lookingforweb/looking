<style>
    .local-form-control { border-radius: 4px; font-size: 14px; font-weight: 500; width: 100%; height: 70px; padding: 14px 18px; line-height: 1.42857143; border: 1px solid #dfe2e7; background-color: #dfe2e7; text-transform: capitalize; letter-spacing: 0px; margin-bottom: 16px; -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075); box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075); -webkit-appearance: none; }

input[type=radio].with-font, input[type=checkbox].with-font { border: 0; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px; }
input[type=radio].with-font~label:before, input[type=checkbox].with-font~label:before { font-family: FontAwesome; display: inline-block; content: "\f1db"; letter-spacing: 10px; font-size: 1.2em; color: #dfe2e7; width: 1.4em; }
input[type=radio].with-font:checked~label:before, input[type=checkbox].with-font:checked~label:before { content: "\f00c"; font-size: 1.2em; color: #EE4B28; letter-spacing: 5px; }
input[type=checkbox].with-font~label:before { content: "\f096"; }
input[type=checkbox].with-font:checked~label:before { content: "\f046"; color: #EE4B28; }
input[type=radio].with-font:focus~label:before, input[type=checkbox].with-font:focus~label:before, input[type=radio].with-font:focus~label, input[type=checkbox].with-font:focus~label { }

.box { background-color: #fff; border-radius: 8px; border: 2px solid #e9ebef; padding: 50px; margin-bottom: 40px; }
.box-title { margin-bottom: 30px; text-transform: uppercase; font-size: 16px; font-weight: 700; color: #EE4B28; letter-spacing: 2px; }
.plan-selection { border-bottom: 2px solid #e9ebef; padding-bottom: 25px; margin-bottom: 35px; }
.plan-selection:last-child { border-bottom: 0px; margin-bottom: 0px; padding-bottom: 0px; }
.plan-data { position: relative; }
.plan-data label { font-size: 20px; margin-bottom: 15px; font-weight: 400; }
.plan-text { padding-left: 35px; }
.plan-price { position: absolute; right: 0px; color: #EE4B28; font-size: 20px; font-weight: 700; letter-spacing: -1px; line-height: 1.5; bottom: 43px; }
.term-price { bottom: 18px; }
.secure-price { bottom: 68px; }
.summary-block { border-bottom: 2px solid #d7d9de; }
.summary-block:last-child { border-bottom: 0px; }
.summary-content { padding: 28px 0px; }
.summary-price { color: #EE4B28; font-size: 20px; font-weight: 400; letter-spacing: -1px; margin-bottom: 0px; display: inline-block; float: right; }
.summary-small-text { font-weight: 700; font-size: 12px; color: #8f929a; }
.summary-text { margin-bottom: -10px; }
.summary-title { font-weight: 700; font-size: 14px; color: #1c1e22; }
.summary-head { display: inline-block; width: 120px; }

.widget { margin-bottom: 30px; background-color: #e9ebef; padding: 50px; border-radius: 6px; }
.widget:last-child { border-bottom: 0px; }
.widget-title { color: #EE4B28; font-size: 16px; font-weight: 700; text-transform: uppercase; margin-bottom: 25px; letter-spacing: 1px; display: table; line-height: 1; }

.btn-group-lg > .btn, .btn-lg {
	padding: .5rem 1rem;
	font-size: 0.95rem;
	line-height: 1.5;
}
#dni {
	border: none;
	padding-left: 44px;
	padding-bottom: 10px;
	background-color: #e9ebef;
	padding-top: 10px;
	border-radius: 50px;
	margin: 20px;
}
.nmelabel {
	padding: 2%;
}
.continuebutton {
	background: #e2362e!important;
}
.bgcolor {
	background-color: #e2362e !important;
}
.color {
	color: #e2362e !important;
}
</style>
    

<header class="header-spacing"></header>

<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <!-- <h1 class="text-center">Estamos trabajando para brindarte un mejor servicio</h1>
        <h2 class="text-center">Sitio en mantenimiento</h2> -->
        <h2 class="col-lg-12 text-center">Ahora puedes pagar tus cuotas de manera más cómoda</h2>
    </div>
    <!-- <div class="row">

        <div class="responsive text-center">
            <img src="<?php echo base_url()?>images/maintenance.png" alt="mantenimiento">
        </div>

    </div> -->
    
     <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
            <div class="">
                <div class="">
                    <input id="dni" name="dni" type="text" placeholder="No de identificación" />
                    <a href="#" class="btn btn-primary btn-lg mb30 findCustomer">consultar</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 nmelabel">
            <div class="name">
            </div>
            <div class="email">
            </div>
        </div>
    </div>
	<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                    <div class="box">
                        <h3 class="box-title">Pagos pendientes</h3>
                        <div id="results"></div>
                        
                    </div>
                    
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                  
                    <div class="widget">
                        <h4 class="widget-title">Confirma tu Pago</h4>
                        <div class="summary-block">
                            <div class="summary-content">
                                <div class="summary-head"><h5 class="summary-title">Valor</h5></div>
                                <div class="summary-price">
                                    <p class="summary-text">$ <span id="fees">0</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="summary-block">
                            <div class="summary-content">
                               <div class="summary-head"> <h5 class="summary-title">Costo asociado</h5></div>
                                <div class="summary-price">
                                    <p class="summary-text">$ <span id="process">0</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="summary-block">
                            <div class="summary-content">
                               <div class="summary-head"> <h5 class="summary-title">Total</h5></div>
                                <div class="summary-price">
                                    <p class="summary-text">$ <span id="total">0</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="summary-block">
                            <div class="summary-content text-center">
                                <a id="sendpayu"  class="btn btn-primary btn-lg mb30">Confirmar Pago</a>
                                <form id="payu" method="post" action="<?php echo $payu->url?>">
                                    <input name="merchantId"    type="hidden"  value=<?php echo $payu->merchant?>   >
                                    <input name="accountId"     type="hidden"  value="<?php echo $payu->account?>" >
                                    <input name="description"   type="hidden"  value=""  >
                                    <input id="address" name="shippingAddress" type="hidden"  value="" >
                                    <input id="city" name="shippingCity" type="hidden"  value="" >
                                    <input id="country" name="shippingCountry" type="hidden"  value="" >
                                    <input id="reference" name="referenceCode" type="hidden"  value="" >
                                    <input id="pamount" name="amount"        type="hidden"  value=""   >
                                    <input name="tax"           type="hidden"  value="0"  >
                                    <input name="taxReturnBase" type="hidden"  value="0" >
                                    <input name="currency"      type="hidden"  value="COP" >
                                    <input id="signature" name="signature"     type="hidden"  value=""  >
                                    <input name="test"          type="hidden"  value="<?php echo $payutest?>" >
                                    <input id="buyerEmail" name="buyerEmail"    type="hidden"  value="" >
                                    <input id="response" name="responseUrl"    type="hidden"  value="<?php echo $payu->response?>" >
                                    <input id="confirmation" name="confirmationUrl"    type="hidden"  value="<?php echo $payu->confirmation?>" >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	
</div>
</div>
<br/>
<br/>
<br/>
<br/>