<script>
    $(".number").keypress(function(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
         return true;
    })
    $(".product-val").keyup(function(evt){
         calculateTotal();
    })
    $("#updateProducts").click(function () {
        if (validate()) {
            var values = [];
            $("input[name='ids[]']").each(function(idx,val){
                var id = $(this).val();
                values.push({
                    id: id,
                    quantity: $("#quantity_"+ id).val(),
                    value: $("#value_"+ id).val(),
                    tax: $("#tax_"+ id).val(),
                    description: $("#description_"+ id).val()
                })
            })
            var submitdata = {
                order: $("#order").val(),
                total: $("#invoice-total").val(),
                products: values
            }
            $.ajax({
                url: "<?php echo base_url(); ?>orders/persistProducts/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status === true) {
                        window.location.href = "<?php echo base_url("orders")?>"
                    }
                }
            });
        } else {
            $("#modal-message #modal-title").html("Alerta");
            $("#modal-message #message").html("Error de validacion<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');

        }

    });
    function validate() {
        $(".required").each(function () {
            required($(this));
        });
        if ($(".validation-error")[0]) {
            return false;
        } else {
            return true;
        }
    }
    function required(obj) {
        $(obj).removeClass("validation-error")
        if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
            $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
            return false
        }
    }
    function calculateTotal(){
        var temp = 0;
        $(".product-val").each(function(idx,val){
            if($(this).val()!=="")
                temp = parseInt(temp) + parseInt($(this).val());
        })
        $("#invoice-total").val(temp)
    }
</script>
