<script type="text/javascript">
    var constI = (<?php echo AuthConstants::FN_INTEREST?>/100);
    var product = 0;
    var first = $("#value").val();
    var amount = 0;
    var quotes = 0;
    var perquote = 0;
    var partial = 0;
    var total = 0;
    var extraCost = 0;
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                invoice: 'required',
                order: 'required',
//                orderDate: 'required',
//                reference: 'required',
//                responsePol: 'required',
                value: 'required',
                status: 'required',
                paymentMethod: 'required',
                customer: 'required',
//                offer: 'required',
//                cupon: 'required',
            },
            messages: {
                invoice:'<?=lang('required')?>',
                order:'<?=lang('required')?>',
                orderDate:'<?=lang('required')?>',
                reference:'<?=lang('required')?>',
                responsePol:'<?=lang('required')?>',
                value:'<?=lang('required')?>',
                status:'<?=lang('required')?>',
                paymentMethod:'<?=lang('required')?>',
                customer:'<?=lang('required')?>',
                offer:'<?=lang('required')?>',
                cupon:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                var submitdata = {
                    "customerId": $("input[name='customerId']").val(),
                    "invoice": $("#invoice").val(),
                    "status": $("#status").val(),
                    "paymentMethod": $("#paymentMethod").val(),
                    "extraCost": extraCost,
                    "quotes": quotes,
                    "perquote": perquote
                };
                $.ajax({
                    url: "<?php echo base_url(); ?>orders/persist/",
                    type: "post",
                    dataType: "json",
                    data: submitdata,
                    success: function (data) {
                        if (data.status == true) {
                            $(location).attr('href', '<?php echo base_url("orders") ?>');
                        }
                    }
                });
            }
        });
        $( "#customer" ).autocomplete({
            source: "<?php echo base_url("orders/getCustomerInfo")?>",
            minLength: 2,
            select: function( event, ui ) {
                $("input[name='customerId']").val(ui.item.id);
                $("#labelName").html(ui.item.name);
                $("#labelEmail").html(ui.item.email);
                $("#labelMobile").html(ui.item.mobile);
                $("#labelAddress").html(ui.item.address);
            }
        });
        $( "#products" ).autocomplete({
            source: "<?php echo base_url("orders/getProducts")?>",
            minLength: 2,
            select: function( event, ui ) {
                $("#productId").val(ui.item.id);
                $("#productName").val(ui.item.product);
                $("#productPrice").val(ui.item.cost);
                $("#productTax").val(ui.item.tax);
                $.ajax({
                    url: "<?php echo base_url();?>orders/findOffer/",
                    type: "post",            
                    data: {itemId:ui.item.id,<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                    dataType: "json",
                    success: function(data){   

                        if(data.status != false){
                            if(data.data.offerPrice> 0 && data.data.offerPrice < ui.item.cost){
                                $("#productPrice").val(data.data.offerPrice);
                            }
                            $("#productOffer").val(data.data.offerId);
                            
                        }
                    }
                });
            }
        });
        $("#country").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>browser/getStates/",
                type: "post",            
                data: {countryId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#state").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .text(option.name);
                            $("#state").append(option);
                        });
                    }
                }
            });
        });

        $("#state").change(function(){
            $.ajax({
                url: "<?php echo base_url();?>browser/getCities/",
                type: "post",            
                data: {stateId:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                dataType: "json",
                success: function(data){   
                    
                    if(data.status != false){
                        var option = $("<option><?php echo lang('select_value') ?></option>");
                        $("#city").html(option);
                        $.each(data.data,function(idx, option){
                            var option = $("<option></option>")
                            .attr("value", option.id)
                            .text(option.name);
                            $("#city").append(option);
                        });
                    }
                }
            });
        });
        $(".guest").on("keyup",".required",function(){
                required($(this));
            
        });
        $("#addProduct").click(function(){
            var html = '<tr>';
            html+= '<td>'+$("#productName").val()+'</td>';
            html+= '<td class="right">'+$("#productTax").val()+'</td>';
            html+= '<td class="right">$'+$("#productPrice").val()+'</td>';
            html+= '<td class="right">gasdf</td>';
            html+= '          </tr>';
            $("#products").val("");
            var val= $("input[name='value']").val();
//            $("input[name='value']").val(parseInt(val) + parseInt($("#productPrice").val()));
//            $("#productsList").append(html);
            var submitdata = {
                "id": $("#productId").val(),
                "cost": $("#productPrice").val(),
                "quotes": 1,
                "offer": $("#productOffer").val(),
                "available": $("#details").val()
            };
            $("#details").val("");
            $.ajax({
                url: "<?php echo base_url(); ?>looking/addItem/",
                type: "post",
                dataType: "json",
                data: submitdata,
                success: function (data) {
                    if (data.status == true) {
                        drawCart($("input[name='cupon']").val())
                    }
                }
            });
            
        });
        $("#addCustomer").click(function(){
           if(validate()){
                 var submitdata = {
                         "cid": $("#cid").val(),
                         "email": $("#email").val(),
                         "name": $("#name").val(),
                         "last_name": $("#last_name").val(),
                         "address": $("#address").val(),
                         "phone": $("#phone").val(),
                         "mobile": $("#mobile").val(),
                         "city": $("#city").val(),
                         "dni": $("#dni").val(),
                         "docType": $("#docType").val(),
                         <?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
                 };
                 $.ajax({
                     url: "<?php echo base_url();?>orders/addCustomer/",
                     type: "post",            
                     data: submitdata,
                     dataType: "json",
                     success: function(data){   
                         if(data.status === true){
                            $("input[name='customerId']").val(data.data.id);
                            $("#customer").val(data.data.name);
                            $("#labelName").html(data.data.name);
                            $("#labelEmail").html(data.data.email);
                            $("#labelMobile").html(data.data.mobile);
                            $("#labelAddress").html(data.data.address);
                            $('#customerModal').foundation('reveal', 'close');
                            $('.close-reveal-modal').trigger('click');
                        }
                     }
                 });
            }else{
                $("#modal-message #modal-title").html("Alerta");
                $("#modal-message #message").html("Error de validacion<br/><br/>");
                $('#modal-message').foundation('reveal', 'open');
            
            }
            
       });
       $("#paymentMethod").change(function(){
            if($(this).val()== '<?php echo AuthConstants::PM_FINANCE ?>'){
                $(".financial-zone").removeClass("hide");
                $(".financial-cost").removeClass("hide");
                $("#initial").trigger("change");
                calculate();
            }else if($(this).val()== '<?php echo AuthConstants::PM_CHANELS ?>'){
                $(".channel-zone").removeClass("hide");
                $("#initial").trigger("change");
            }else{
                extraCost = 0;
                channels()
                $(".financial-zone").addClass("hide");
                $(".financial-cost").addClass("hide");
                $(".channel-zone").addClass("hide");
            }
        });
       $("#manualCost").focusout(function(){
            var val = $(this).val();
            if($("#paymentMethod").val()== '<?php echo AuthConstants::PM_CHANELS ?>'){
                extraCost = val;
                channels()
            }
       })
       $("#initialNum").focusout(function(){
           var value = $("#value").val();
            var keyPercent = $(this).val();
            var init = ((keyPercent*100)/value);
            var option = $('<option></option>').attr("value", (init)).text(Math.ceil(init) + "%");
            $("#initial").append(option);
            $("#initial").val((init));
            calculate();
        });
       $("#initialNum").keypress(function(evt){
           var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
               return false;
            return true;
        });
       $("#initialNum").change(function(){
           calculate();
        });
       $("#initial").change(function(){
           var total = ($("#value").val());
            first = Math.ceil((total/100)* $("#initial").val());
            $("#initialNum").val(first);
           calculate();
        });
       $("#quotes").change(function(){
           calculate();
        });
        function channels(){
            /**
             * obtiene el valor de la inicial y suma el varlor agregado manualmente
             * 
             */ 
            var total = ($("#value").val());
            $("#extratotal").html("$ "+ (parseInt(extraCost)+parseInt(total)));
        }
        function calculate(){
            /**
             * obtiene el valor de la inicial y las cuotas
             * si es a una cuota el valor es el porcentaje de la constante FN_CREDITPERCENT
             * si son mas cuotas por cada cuota incremente FN_CREDITPERCENT_INC 
             */ 
            var total = ($("#value").val());
            first = Math.ceil((total/100)* $("#initial").val());
            quotes = $("#quotes").val();
//            var percent = <?php echo AuthConstants::FN_CREDITPERCENT ?>;
//            var newPercent = (quotes * <?php echo AuthConstants::FN_CREDITPERCENT_INC ?>)-<?php echo AuthConstants::FN_CREDITPERCENT_INC ?>;
//            var localpercent = percent + newPercent;
//            var totalP = (parseInt($("#value").val())*localpercent);
            amount = total - first;
            partial = Math.pow((1+constI),quotes);
            perquote = Math.ceil(amount*((constI*partial)/(partial-1)));
            var addFee = Math.ceil(perquote * quotes);
            extraCost = addFee - amount;
            $(".label-perquote").html(quotes + " <?php echo lang("quotes")?>");
            $("#perquote").html("$ "+ (parseInt(perquote)));
            $("#extraval").html("$ "+ extraCost)
            $("#extratotal").html("$ "+ (parseInt(extraCost)+parseInt(total)));
        }
        function validate(){
            $(".required").each(function(){
                console.log($(this).val())
                required($(this));
            });
            if ($(".validation-error")[0]){
                return false;
            } else {
                return true;
            }
        }
        function required(obj){
            $(obj).removeClass("validation-error")
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
                return false
            }
        }
       $("#offer").change(function(){
            if($(this).val()!==""){
                $("input[name='cupon']").attr("disabled","disabled");
            }else{
                $("input[name='cupon']").removeAttr("disabled");
            }
       })
        $("input[name='cupon']").keyup(function(){
            if($(this).val().length > 0){
                $("#offer").attr("disabled","disabled");
                $("#validateCode").removeClass("disabled");
            }else{
                $("#offer").removeAttr("disabled");
                $("#validateCode").addClass("disabled");
            }
        });
        $("input[name='cupon']").autocomplete({
            source: "<?php echo base_url("orders/getCupons")?>",
            minLength: 2,
            select: function( event, ui ) {
            }
        });
        $("#validateCode").click(function(){
            drawCart($("input[name='cupon']").val())
        });
       function drawCart(code){
            var submitdata ={};
            if(typeof(code)!== "undefined" && code !== ""){
                submitdata = {code:code};
            }
           $.ajax({
                url: "<?php echo base_url(); ?>orders/getCart/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        var html ='';
                        $.each(data.cart,function(index,value){
                            html+= '<tr>';
                            html+= '<td>'+value.name+'</td>';
                            html+= '<td>'+value.quotes+'</td>';
                            html+= '<td class="right">'+value.tax+'</td>';
                            html+= '<td class="right">$'+value.cost+'</td>';
                            html+= '</tr>';
                        });
                        $("#productsList").html(html);
                        $("input[name='value']").val(data.total);
                        if($("#paymentMethod").val()==<?php echo AuthConstants::PM_FINANCE ?>){
                            calculate();
                        }
                        if($("#paymentMethod").val()==<?php echo AuthConstants::PM_CHANELS ?>){
                            channels();
                        }
                    }
                }
            });
       }
    });
</script>