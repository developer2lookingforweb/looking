<script type="text/javascript">
    /*!
 * Datepicker for Bootstrap v1.6.1 (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
  

    
    var user = "";
    $(document).ready(function(){
        assign(0);
        $('.user-down li').click(function(){
            assign($(this).attr("id"));
//            $("#user-trigger").html($(this).attr("id"));
        });
        $('body').on("click",".erase",function(){
            assign($(this).attr("id"))
        });
               
    });
    function assign(id){
        number =$("#number").val();
        var dataSubmit = {
            user: id,
        }
        $.ajax({
            type: 'post',
            data: dataSubmit,
            url: '<?php echo base_url("orders/persistAssign/"); ?>',
            dataType: "json",
            success: function (data) {
                if (data.status == true) {
//                    $(this).showMessage("Asignación","Usuario Asignado");
                    $("#user-trigger").html("Selecciona Usuario");
                    drawList(data.data.users);
                }else{
                    $(this).showMessage("Error","No se ha realizado la asignación, por favor verifica los datos")
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(this).showMessage("Alerta","Error inesperado, por favor recarga la páagina")
            }
        });
    }
    function drawList(list){
        var html = "";
        $.each(list,function(idx,val){
            html += '<tr>';
            html += '    <td>'+val.name+'</td>';
            html += '    <td><button type="button" class="btn btn-primary erase" id="'+val.id+'">Borrar</button></td>';
            html += '</tr>';
        });
        $("#results").html(html);
    }
    function downloadXls(){
        $.redirectPost('<?php echo base_url("looking/downloadExcel/"); ?>',
                        {
                            campaign: $("#campaigns").val(),
                            date1: $("#date1").val(),
                            date2: $("#date2").val()
                        });

//        $.ajax({
//                type: 'post',
//                data: {
//                    campaign: $("#campaigns").val(),
//                    date1: $("#date1").val(),
//                    date2: $("#date2").val()
//                },
//                url: '<?php echo base_url("looking/downloadExcel/"); ?>',
//                dataType: "json",
//                success: function (data) {
//                    var blob = new Blob([data], { type: "application/vnd.ms-excel" });
//                    alert('BLOB SIZE: ' + data.length);
//                    var URL = window.URL || window.webkitURL;
//                    var downloadUrl = URL.createObjectURL(blob);
//                    document.location = downloadUrl;
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                    $("#contenido_").html("<small>" + xhr.responseText + "</small>");
//                }
//            });
    }
    $.extend(
    {
        redirectPost: function(location, args)
        {
            var form = '';
            $.each( args, function( key, value ) {
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
        }
    });
</script>