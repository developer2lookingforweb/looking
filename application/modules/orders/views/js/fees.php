<script>
    $(".number").keypress(function(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
         return true;
    })
    $(".product-val").keyup(function(evt){
         calculateValue($(this));
    })
    $("#updateProducts").click(function () {
        if (validate()) {
            var valid = true;
            if($("#payment").val() == 0 || !$.isNumeric($("#payment").val()) || parseInt($("#payment").val()) > parseInt($("#invoice-total").val())){
                valid = false;
            }
            if(valid){
                
                var values = $("#payment").val();
                var submitdata = {
                    order: $("#order").val(),
                    total: $("#invoice-total").val(),
                    payments: values
                }
                $.ajax({
                    url: "<?php echo base_url(); ?>orders/persistFeePayments/",
                    type: "post",
                    data: submitdata,
                    dataType: "json",
                    success: function (data) {
                        if (data.status === true) {
                            window.location.href = "<?php echo base_url("orders")?>"
                        }
                    }
                });
            }
        } else {
            $("#modal-message #modal-title").html("Alerta");
            $("#modal-message #message").html("Error de validacion<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');

        }

    });
    function validate() {
        $(".required").each(function () {
            required($(this));
        });
        if ($(".validation-error")[0]) {
            return false;
        } else {
            return true;
        }
    }
    function required(obj) {
        $(obj).removeClass("validation-error")
        if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
            $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
            return false
        }
    }
    function calculateValue(obj){
        var id = obj.attr("id");
        id = id.split("_");
        id = id[1];
        if(obj.val() > $("#values_"+id).val()){
            obj.addClass("error");
            $('#alert').addClass("alert");
            $("#message").html("El valor pagado no puede superar el monto de la cuota<br/><br/>");
            $("#alert").show();

        }else{
            obj.removeClass("error");
            $("#alert").hide();
        }
            
    }
</script>
