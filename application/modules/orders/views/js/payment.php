<script type="text/javascript" src="https://checkout.epayco.co/checkout.js">   </script>
<script type="text/javascript">
    /*!
 * Datepicker for Bootstrap v1.6.1 (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
  

    
    var user = "";
    var count = 0;
    var reference = "";
    var fees = 0;
    var process = 0;
    var total = 0;
    var epaycodata={};
    $(document).ready(function(){




        var handler = ePayco.checkout.configure({
  				key: '<?php echo $epaykey?>',
  				test: <?php echo $epaytest?>
  			})

        epaycodata={
          //Parametros compra (obligatorio)
          name: "Cuota Infinix Hot 2 de 5",
          description: "Pago de Cuotas",
          invoice: "PC0012345-1251",
          currency: "cop",
          amount: "12000",
          tax_base: "0",
          tax: "0",
          country: "co",
          lang: "en",

          //Onpage="false" - Standard="true"
          external: "false",


          //Atributos opcionales
          extra1: "",
          extra2: "",
          extra3: "",
          confirmation: "",
          response: "",

          //Atributos cliente
          name_billing: "",
          address_billing: "",
          type_doc_billing: "cc",
          mobilephone_billing: "",
          number_doc_billing: ""
          }
          



        $('body').on('change','.chk-fees',function(){
            if($(this).is(":checked")){
                addFee($(this).attr('item-value'),"add");
            }else{
                addFee($(this).attr('item-value'),"substract");
            }
            
        })
        $('#sendpayu').click(function(){
            var tmp = [];
            reference = "";
            $('.chk-fees').each(function(idx,val){
                if($(this).is(":checked")){
                    var id = $(this).attr('id');
                    id = id.split('-');
                    id = id[1];
                    tmp.push(id);
                }
            })
            var time = new Date().getTime();
            reference = "PC000"+tmp.join("-")+ "--" +time;
            $("#pamount").val($("#total").attr('total-cost'));
            $("#reference").val(reference);
            $("#response").val('<?php echo base_url("orders/feeresponse/")?>/'+reference);
            $("#confirmation").val('<?php echo base_url("orders/feeconfirmation/")?>/'+reference);
            epaycodata.response = '<?php echo base_url("orders/epayco/")?>/'+reference;
            epaycodata.confirmation = '<?php echo base_url("orders/epaycoc/")?>/'+reference;
            var dataSubmit = {
                reference: reference,
                value: $("#total").attr('total-cost'),
            };
            $.ajax({
                type: 'post',
                data: dataSubmit,
                url: '<?php echo base_url("orders/getSignature/"); ?>',
                dataType: "json",
                success: function (data) {
                    $("#signature").val(data.data);
                    // $("#payu").submit();
                    epaycodata.amount = $("#total").attr('total-cost');
                    epaycodata.invoice = reference;
                    handler.open(epaycodata);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(this).showMessage("Alerta","Error inesperado, por favor recarga la páagina")
                }
            });
        })
        $('.findCustomer').click(function(){
            var dataSubmit = {
                dni: $("#dni").val(),
            }
            $.ajax({
                type: 'post',
                data: dataSubmit,
                url: '<?php echo base_url("orders/getOrdersByDni/"); ?>',
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
    //                    $(this).showMessage("Asignación","Usuario Asignado");
                        $(".name").html("<h5>"+data.data.name+"</h5>");
                        $(".email").html("<h6>"+data.data.email+"</h6>");
                        $("#buyerEmail").val(data.data.email);
                        epaycodata.name_billing = data.data.name;
                        epaycodata.address_billing = data.data.address;
                        epaycodata.mobilephone_billing = data.data.mobile;
                        epaycodata.number_doc_billing = data.data.dni;
                        drawList(data.data.invoices);
                    }else{
                        $(this).showMessage("Error","No se ha realizado la asignación, por favor verifica los datos")
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(this).showMessage("Alerta","Error inesperado, por favor recarga la página")
                }
            });
        });

               
    });
    
                                
    function drawList(list){
        var html = "";
        $.each(list,function(idx,val){
            html += '<div class="plan-selection">';
            html += '   <div class="plan-data">';
            html += '       <input id="question-'+val.feeid+'" name="fees[]" type="checkbox" item-value="'+val.real+'" class="with-font chk-fees" value="sel" />';
            html += '       <label for="question-'+val.feeid+'">'+ val.name +'</label>';
            var cls = '';
            var expiredTxt = '';
            if(val.expired){
                cls = 'text-danger';
                expiredTxt = ' (Vencida)';
            }
            html += '       <p class="plan-text '+cls+'">';
            var date = val.payment_date.date.split(" ");
            html += '            Cuota '+ val.current+' de '+ val.total +'. Pago oportuno '+ date[0] +' '+expiredTxt;
            html += '       </p>';
            html += '       <span class="plan-price">$'+val.real+'</span>';
            html += '   </div>';
            html += '</div>';
        });
        $("#results").html(html);
    }
    function addFee(value,operation){
        if(operation == "add"){
            fees = fees + parseInt(value);
            count = count+1;
        }else{
            fees = fees - parseInt(value);
            count = count -1;
        }
        
        process = Math.ceil((fees * <?php echo AuthConstants::PAYU_PROCCESSPERCENTAGE ?>) 
                            + ((fees * <?php echo AuthConstants::PAYU_PROCCESSPERCENTAGE ?>) * <?php echo AuthConstants::PAYU_PROCCESSIVA?>)
                            + <?php echo AuthConstants::PAYU_PROCCESSBASE ?> 
                            + (<?php echo AuthConstants::PAYU_PROCCESSBASE ?> * <?php echo AuthConstants::PAYU_PROCCESSIVA?>));
        total = parseInt(fees) + parseInt(process);
        $("#fees").html(fees);
        $("#process").html(process);
        $("#total").html(total);
        $("#total").attr('total-cost',total);
        $("input[name='description']").val("Alofijo abono de "+count+" cuota(s)." );
        epaycodata.description = "Alofijo abono de "+count+" cuota(s).";
    }
    
</script>