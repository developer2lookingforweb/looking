<script>
    $("#country").change(function () {
        $.ajax({
            url: "<?php echo base_url(); ?>browser/getStates/",
            type: "post",
            data: {countryId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
            dataType: "json",
            success: function (data) {

                if (data.status != false) {
                    var option = $("<option><?php echo lang('select_value') ?></option>");
                    $("#state").html(option);
                    $.each(data.data, function (idx, option) {
                        var option = $("<option></option>")
                                .attr("value", option.id)
                                .text(option.name);
                        $("#state").append(option);
                    });
                }
            }
        });
    });
    var docType = '<?php echo $docType ?>';
    if (parseInt(docType) > 0) {
        $("#docType").val(parseInt(docType));
    }
    var country = '<?php echo $country ?>';
    if (parseInt(country) > 0) {
        $("#country").val(parseInt(country));
        $("#country").trigger('change');
        var state = '<?php echo $state ?>';
        if (parseInt(state) > 0) {
            setTimeout(function () {
                $("#state").val(parseInt(state));
                $("#state").trigger('change');
            }, 500);
            var city = '<?php echo $city ?>';
            if (parseInt(city) > 0) {
                setTimeout(function () {
                    $("#city").val(parseInt(city));
                    $("#city").trigger('change');
                }, 1000);
            }
        }
    }
    setTimeout(function(){getJob()},500);
    setTimeout(function(){getReferences()},500);
    setTimeout(function(){getAssets()},500);
    
    
    $("#state").change(function () {
        $.ajax({
            url: "<?php echo base_url(); ?>browser/getCities/",
            type: "post",
            data: {stateId: $(this).val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
            dataType: "json",
            success: function (data) {

                if (data.status != false) {
                    var option = $("<option><?php echo lang('select_value') ?></option>");
                    $("#city").html(option);
                    $.each(data.data, function (idx, option) {
                        var option = $("<option></option>")
                                .attr("value", option.id)
                                .text(option.name);
                        $("#city").append(option);
                    });
                }
            }
        });
    });
    $("#addCustomer").click(function () {
        if (validate()) {
            var submitdata = {
                "cid": $("#id").val(),
                "email": $("#email").val(),
                "name": $("#name").val(),
                "last_name": $("#last_name").val(),
                "address": $("#address").val(),
                "phone": $("#phone").val(),
                "mobile": $("#mobile").val(),
                "city": $("#city").val(),
                "dni": $("#dni").val(),
                "docType": $("#docType").val(),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                url: "<?php echo base_url(); ?>orders/persistCustomer/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status === true) {
                        window.location.href = "<?php echo base_url("orders")?>"
                    }
                }
            });
        } else {
            $("#modal-message #modal-title").html("Alerta");
            $("#modal-message #message").html("Error de validacion<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');

        }

    });
    $("#addReference").click(function () {
            var submitdata = {
                "cid": $("#id").val(),
                "name": $("#r_name").val(),
                "mobile": $("#r_phone").val(),
                "job": $("#r_job").val(),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                url: "<?php echo base_url(); ?>orders/persistReference/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status === true) {
                        getReferences();
                    }
                }
            });

    });
    $("#addAsset").click(function () {
            var submitdata = {
                "cid": $("#id").val(),
                "type": $("#p_type").val(),
                "spec": $("#p_spec").val(),
                "value": $("#p_value").val(),
                "reference": $("#p_reference").val(),
                "pledge": $("#p_pledge").val(),
                "balance": $("#p_balance").val(),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                url: "<?php echo base_url(); ?>orders/persistAssets/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status === true) {
                        getAssets();
                    }
                }
            });

    });
    $("#addJob").click(function () {
            var submitdata = {
                "cid": $("#id").val(),
                "age": $("#age").val(),
                "neighborhood": $("#neighborhood").val(),
                "company": $("#company").val(),
                "address": $("#comp_address").val(),
                "phone": $("#comp_phone").val(),
                "earnings": $("#earnings").val(),
                "comp_site": $("#comp_site").val(),
                "comp_age": $("#comp_age").val(),
                "contract_type": $("#contract_type").val(),
                "extra_earnings": $("#extra_earnings").val(),
                "concept": $("#concept").val(),
<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()
            };
            $.ajax({
                url: "<?php echo base_url(); ?>orders/persistJob/",
                type: "post",
                data: submitdata,
                dataType: "json",
                success: function (data) {
                    if (data.status === true) {
                        $("#modal-message #modal-title").html("Alerta");
                        $("#modal-message #message").html("Información extra guardada<br/><br/>");
                        $('#modal-message').foundation('reveal', 'open');

                    }
                }
            });

    });
    function validate() {
        $(".required").each(function () {
            required($(this));
        });
        if ($(".validation-error")[0]) {
            return false;
        } else {
            return true;
        }
    }
    function required(obj) {
        $(obj).removeClass("validation-error")
        if ($(obj).hasClass("required") && ($(obj).val() == "" || $(obj).val() == "-Selecciona-")) {
            $(obj).addClass("validation-error")
//                $(obj).css("background","#ecc");
            return false
        }
    }
    function getReferences (){
        $.ajax({
            url: "<?php echo base_url(); ?>orders/getReferences/",
            type: "post",
            data: {cid: $("#id").val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
            dataType: "json",
            success: function (data) {
                if(data.references.length > 0){
                    var html = "<table width='100%'>";
                        html += "<tr>";
                        html += "<th width='50%'><?php echo lang("name")?></th>";
                        html += "<th width='25%'><?php echo lang("mobile")?></th>";
                        html += "<th width='25%'><?php echo lang("job")?></th>";
                        html += "</tr>";
                    $.each(data.references, function (idx, option) {
                        html += "<tr>";
                        html += "<td>"+option.name+"</td>";
                        html += "<td>"+option.phone+"</td>";
                        html += "<td>"+option.job+"</td>";
                        html += "</tr>";
                    });
                    html += "</table>";
                    $(".list-references").html(html);
                }
            }
        });
    }
    function getAssets (){
        $.ajax({
            url: "<?php echo base_url(); ?>orders/getAssets/",
            type: "post",
            data: {cid: $("#id").val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
            dataType: "json",
            success: function (data) {
                if(data.assets.length > 0){
                    var html = "<table width='100%'>";
                        html += "<tr>";
                        html += "<th width='50%'><?php echo lang("type")?></th>";
                        html += "<th width='25%'><?php echo lang("spec")?></th>";
                        html += "<th width='25%'><?php echo lang("value")?></th>";
                        html += "<th width='25%'><?php echo lang("reference")?></th>";
                        html += "<th width='25%'><?php echo lang("pledge")?></th>";
                        html += "<th width='25%'><?php echo lang("balance")?></th>";
                        html += "</tr>";
                    $.each(data.assets, function (idx, option) {
                        html += "<tr>";
                        html += "<td>"+option.type+"</td>";
                        html += "<td>"+option.spec+"</td>";
                        html += "<td>"+option.value+"</td>";
                        html += "<td>"+option.reference+"</td>";
                        html += "<td>"+option.pledge+"</td>";
                        html += "<td>"+option.balance+"</td>";
                        html += "</tr>";
                    });
                    html += "</table>";
                    $(".list-assets").html(html);
                }
            }
        });
    }
    function getJob (){
        $.ajax({
            url: "<?php echo base_url(); ?>orders/getJob/",
            type: "post",
            data: {cid: $("#id").val(),<?php echo $this->config->item('csrf_token_name') ?>: $("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
            dataType: "json",
            success: function (data) {

                if(data.job !== null){
                    $("#age").val(data.age);
                    $("#neighborhood").val(data.neighborhood);
                    $("#company").val(data.job.company);
                    $("#comp_address").val(data.job.address);
                    $("#comp_phone").val(data.job.phone);
                    $("#earnings").val(data.job.earnings);
                    $("#comp_site").val(data.job.site);
                    $("#comp_age").val(data.job.age);
                    $("#contract_type").val(data.job.contract);
                    $("#extra_earnings").val(data.job.extra_earnings);
                    $("#concept").val(data.job.concept);
                }
            }
        });
    }
</script>
