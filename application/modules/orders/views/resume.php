<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <!--<title>Invoice</title>-->
        <link rel="stylesheet" href="css/bootstrap.css">
        
        <style>

            body {
                color: black;
                font-family: Helvetica;
                font-weight: 300;
                margin: 0;
                padding: 0;
            }
            /*#footer, #header{*/
            #footer{
                background: none;
                text-align: center;
                /*display: none;*/
            }
            #header{
                display: none;
            }
            .header-title{
                color: #FFFFFF;
            }
            .body-content {
                background: none;
            }
            .pdfrow::before, .pdfrow::after {
                content: " ";
                display: table;
            }
            .pdfrow {
                margin: 0 auto;width: 100%;
            }
            .pdfrow::after {
                clear: both;
            }

            label {
                color: #4d4d4d;
                cursor: pointer;
                display: block;
                font-size: 0.875rem;
                font-weight: normal;
                line-height: 1.5;
                margin-bottom: 0;
            }

            .pdfcolumn.large-centered, .pdfcolumns.large-centered {
                float: none;
                margin-left: auto;
                margin-right: auto;
            }/**/
            .invoice{
                font-size: 1.8rem;
                color: #CD6A51;
                font-style: italic;
            }
            .invoice-date {
                font-size: 1.2rem;
                font-style: italic;
                padding-bottom: 5%;
            }
            .seller-info, .buyer-info{
                border-radius: 10px;
                background: #cdcdcd;
                min-height: 150px;
            }
            label.customer{
                font-weight: 400;
                font-size: 1.1rem;
            }
            label.name{
                font-style: italic;
                font-size: 1.3rem;
            }
            .invoice-head{
                padding-bottom: 4px;
            }
            .products{
                max-height: 420px;
                margin: 0 25px;
            }
            .products-head label{
                color: #FFF;
            }
            .products-head{
                font-size: 1.1rem;
                background: #EE4B28 ;
/*                padding: 2px 0;
                margin: 5px 0;*/

            }
            .products-row {
                padding: 2px 0;
                border-top: 1px solid #cfcfcf;
            }
            .detail-row div{
                border-left: 1px solid #EE4B28;
                border-right: 1px solid #EE4B28;
            }
            .total {
                font-weight: 400;
                border-top: 1px dotted #cfcfcf;
                border-bottom: 1px solid #cfcfcf;
                padding: 14px 0;
            }
            .last { position: absolute; bottom: 50px; left: 450px; right: 0px; height: 150px; padding: 5px 20px;}
            .last-img { position: absolute; bottom: 50px; left: 0px; right: 450px; height: 150px; padding: 5px 20px;}
            @page{margin: 0.0in 0.0in 0.0in 0.0in;}
        </style>
    </head>

    <body>

        <div class="container" style="background: #EE4B28 ;">
            <div class="row">
                <div class="col-xs-12">
                    <div class=" text-left" style="width:360px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;padding-top: 1.075em;">
                        <img width="200" src="<?php echo FCPATH; ?>images/LogoNombreLookingBlanco.png"/>
                    </div>
                    <div class="header-title">
                        <h2 style="height: 60px;padding-right: 15px;text-align: right;">Estado de cuenta</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="" style="margin: 0 auto;width: 100%;">
            <div class="large-centered" style="width: 732px;">
                <div class=" invoice-head" style="margin: 0px auto; width: 100%; height: 200px;">
                    <div class=" buyer-info" style="margin-left: 20px;margin-top: 10px;width:400px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;">
                        <label class="customer"><?php echo lang('customer') ?></label>
                        <label class="name"><?php echo $name ?></label>
                        <label><?php echo lang('dni'), ": " ?><?php echo $dni ?></label>
                        <label><?php echo lang('address'), ": " ?><?php echo $address ?></label>
                        <label><?php echo lang('mobile'), ": " ?><?php echo $mobile ?> </label>
        <!--                <label><?php echo $customerCity ?> </label>-->
                        <!--<label><?php echo $customerState ?></label>-->
                        <label><?php echo lang('email'), ": " ?><?php echo $email ?></label>
                        <br/>
                    </div>
                    <div class="text-right" style="width:325px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;height: 200px;">
                        <div class="invoice"  style="text-align: right;">
                            Factura No. <?php echo $invoice; ?>
                        </div>
                        <div class="invoice-date" style="text-align: right;">
                            <?php echo $order_date; ?>
                        </div>
                    </div>


                </div>
            </div>
            <div class="products" style="height:5px; padding:0 -25px;">
                <div class="products products-head" style="margin: 0 0px; height: 25px;padding: 0;width: 100%;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>Fecha</label></div>
                    <div class="" style="width: 40%;float: left;text-align: center"><label>Producto</label></div>
                    <div class="" style="width: 40%;float: left;text-align: center"><label>Valor Total</label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
            </br>
            </div>
            <div class="products" style="height:25px; padding:0 -25px;">
                <div class="products-row" style="margin-top:20px;width: 100%; height:25px;display: block;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label><?php echo $order_date; ?></label></div>
                    <div class="" style="width: 40%;float: left;text-align: center"><label><?php echo $product; ?></label></div>
                    <div class="" style="width: 40%;float: left;text-align: center"><label>$<?php echo number_format($value); ?></label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
            </div>
            <br>
            <div class="products" style="height:5px; margin-top: 55px; padding:0 -25px;">
                <div class="products products-head" style="margin: 0 0px; height: 25px;padding: 0;width: 100%;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 10%;float: left;text-align: center"><label>Cuota</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>Ultimo pago</label></div>
                    <div class="" style="width: 30%;float: left;text-align: center"><label>Pago mínimo</label></div>
                    <div class="" style="width: 30%;float: left;text-align: center"><label>Total a Pagar</label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
                <?php
                ?>
            </br>
            </div>
            <div class="products" style="height:25px; padding:0 -25px;">
                <div class="products-row" style="margin-top:20px;width: 100%; height:25px ;display: block;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 10%;float: left;text-align: center"><label><?php echo $fees ?></label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>$<?php echo number_format($last_payment) ?></label></div>
                    <div class="" style="width: 30%;float: left;text-align: center"><label>$<?php echo number_format($min_payment) ?></label></div>
                    <div class="" style="width: 30%;float: left;text-align: center"><label>$<?php echo number_format($total_due) ?></label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
            </div>
            <br>
            <div class="products" style="height:5px; margin-top: 55px; padding:0 -25px;">
                <div class="products products-head" style="margin: 0 0px; height: 25px;padding: 0;width: 100%;">
                    <div class="" style="width: 100%;float: left;text-align: center"><label>Historial de Pagos</label></div>
                </div>
                <?php
                ?>
            </div>
            <div class="products" style="height:5px; margin-top: 20px; padding:0 -25px;">
                <div class="products products-head" style="margin: 0 0px; height: 25px;padding: 0;width: 100%;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>Fecha Límite</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>Fecha Pago</label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>Pago Mínimo</label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>Pago Realizado</label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
                <?php
                ?>
            </br>
            </div>
            <div class="products" style="height:25px; padding:0 -25px;">
                <?php            foreach ($paid as $key => $row) { ?>
                
                <div class=" detail-row" style="margin-top:18px;width: 100%; height:4px ;display: block;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label><?php echo $row["payment_date"]->format("Y-m-d") ?></label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label><?php echo $row["transaction_date"]->format("Y-m-d") ?></label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>$<?php echo number_format($row["value"]) ?></label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>$<?php echo number_format($row["paid"]) ?></label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
                <?php } ?>
                
                <?php for($i = 0; $i <= 5; $i++){?>
                    <div class=" detail-row" style="margin-top:18px;width: 100%; height:4px ;display: block;">
                        <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                        <div class="" style="width: 20%;float: left;text-align: center"><label>&nbsp;</label></div>
                        <div class="" style="width: 20%;float: left;text-align: center"><label>&nbsp;</label></div>
                        <div class="" style="width: 25%;float: left;text-align: center"><label>&nbsp;</label></div>
                        <div class="" style="width: 25%;float: left;text-align: center"><label>&nbsp;</label></div>
                        <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    </div>
                <?php }?>
                <div class="products products-head" style="margin: 0 0px; height: 25px;padding: 0;width: 100%;">
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 20%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 25%;float: left;text-align: center"><label>&nbsp;</label></div>
                    <div class="" style="width: 5%;float: left;text-align: center"><label>&nbsp;</label></div>
                </div>
            </div>
        </div>
        <div class="last buyer-info" style="width:300px; text-align: right;">
            <label class="customer">Pago Mínimo</label>
            <label class="name">$ <?php echo number_format($min_payment) ?></label>
            <label>Fecha Pago Oportuno</label>
            <label><?php echo $min_payment_date ?></label>
            <br/>
        </div> 
        <div class="last-img text-left" style="">
            <img width="350" src="<?php echo FCPATH; ?>images/footerpagos.png"/>
        </div>
    </body>
</html>