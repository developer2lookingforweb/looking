<header class="header-spacing"></header>
<div class="container-fluid bg-primary col-sm-12 categories-container">
    <div class="btn-group">
        <a class="btn  text-faded go-back" data-toggle="dropdown" href="#">
           Recargas a celulares
        </a>
    </div>
</div>
<br/>
<br/>
<br/>
<div class="container">
    <div class="row">
        <h1 class="text-center">Formulario de asignación temporal de permisos</h1>
    </div>
    <hr>
    <br>
    <br>
    <div class="row">
        <div class="col-lg-6 col-lg-push-3" >
            <div class="col-lg-2" >
                <label>Usuario</label>
            </div>
            <div class="col-lg-10" >
                <div class="input-group">
                     <div class="dropdown">
                        <button id="user-trigger" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Selecciona Usuario
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu user-down">
                            <?php foreach($users as $oper){?>
                            <li id="<?php echo $oper->getId()?>">
                                <a href="#">&nbsp;<?php echo ucfirst($oper->getName())?></a>
                            </li>
                            <?php }?>
                        </ul>
                      </div>
                </div>
                
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th width="50%">Nombre</th>
                                <th>Borrar</th>
                            </tr>
                        </thead>
                        <tbody id="results">
                        </tbody>
                    </table>
                </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>