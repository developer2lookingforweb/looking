<div id="customerModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h5 id="modalTitle">Crear Usuario</h5>
    <p class="lead">
    <div class="large-10 large-centered columns">
            <input name="cid" id="cid"   type="hidden"  value=""  >
            <div class="guests" >
            <div class="row">
                <div class="large-2 columns "><?php echo lang('basic_info')?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="name" name="name" class="required" placeholder="<?php echo lang('name')?>" value=""/>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="last_name" name="last_name" class="required" placeholder="<?php echo lang('last_name')?>" value=""/>
                </div>
                <div class="large-4 columns ">
<!--                    &nbsp;<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email')?>" value=""/>-->
                </div>                           
            </div>                           
            <div class="row">
                <div class="large-2 columns ">&nbsp;</div>
                <div class="large-3 columns ">
                    <select  id="docType" name="docType" class="required" placeholder="<?php echo lang('document_type')?>" >
                        <option value=""><?php echo lang('select_value')?></option>
                        <?php foreach ($docTypes as $key => $dtype) {
                            $selected = "";
                            if(isset($docType)){
                                if($docType== $dtype['id']){
                                    $selected = 'selected="selected"';
                                }
                            }
                            echo "<option value='".$dtype['id']."' $selected>".$dtype['name']."</option>";
                         }
                         ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <input  type="text" id="dni" name="dni" class="required" placeholder="<?php echo lang('dni')?>" value=""/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="email" name="email"  class="required" placeholder="<?php echo lang('email')?>" value=""/>
                </div>                           
            </div>                
            <div class="row">
                <div class="large-2 columns "><?php echo lang('location')?></div>
                <div class="large-3 columns ">
                    <select id="country" name="country" class="required"  placeholder="<?php echo lang('country')?>">
                        <option value=""><?php echo lang('select_value')?></option>
                        <?php foreach ($countries as $key => $country) {
//                            var_dump($value);
                            echo "<option value='".$country['id']."'>".$country['name']."</option>";
                         }
                         ?>
                    </select>
                </div>
                <div class="large-3 columns ">
                    <select id="state" name="state" class="required"  placeholder="<?php echo lang('state')?>">
                        <option value=""><?php echo lang('select_value')?></option>
                    </select>
                </div>
                <div class="large-4 columns ">
                    <select id="city" name="city" class="required"  placeholder="<?php echo lang('city')?>" class="required">
                        <option value=""><?php echo lang('select_value')?></option>
                    </select>
                </div>                
            </div>
            <div class="row">
                <div class="large-2 columns "><?php echo lang('contact')?></div>
                <div class="large-3 columns ">
                    <input  type="text" id="phone" name="phone" class="required" placeholder="<?php echo lang('phone')?>" value=""/>
                </div>  
                <div class="large-3 columns ">
                    <input  type="text" id="mobile" name="mobile" class="required" placeholder="<?php echo lang('mobile')?>" value=""/>
                </div>
                <div class="large-4 columns ">
                    <input  type="text" id="address" name="address" class="required" placeholder="<?php echo lang('address')?>" value=""/>
                </div>
                   
            </div>
            <div class="row">
                <div class="large-3 columns large-push-9 ">
                    <a id="addCustomer" href="#" class="button radius"  ><i class=" icon-user"></i>&nbsp;Crear Usuario </a>
                </div>  
            </div>
        </div>
    </div>
    </p>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<?php 
    $opPay = array();
    $opPay[""] = lang("default_select");
    foreach ($listPaymentMethods as $aPaymentMethod){
        $opPay[$aPaymentMethod->getId()] = $aPaymentMethod->getMethod();
    }
    $opStatus = array();
    $opStatus[""] = lang("default_select");
    foreach ($listOrdersStatus as $aStatus){
        $opStatus[$aStatus->getId()] = $aStatus->getName();
    }
    $opOffers = array();
    $opOffers[""] = lang("default_select");
    foreach ($listOffers as $aOffer){
        $opOffers[$aOffer->getId()] = $aOffer->getName();
    }
    $opFee = array();
    $opFee["30"] = "30%";
    $opFee["40"] = "40%";
    $opFee["50"] = "50%";
    $fields = array();
    $opQuotes = array();
    $opQuotes["1"] = "1";
    $opQuotes["2"] = "2";
    $opQuotes["3"] = "3";
    $opQuotes["4"] = "4";
    $opQuotes["5"] = "5";
    $opQuotes["6"] = "6";
    $opQuotes["7"] = "7";
    $opQuotes["8"] = "8";
    $opQuotes["9"] = "9";
    $opQuotes["10"] = "10";
    $fields["col-1"]["config"]       = array("large"=>"6");
    $fields["col-2"]["config"]       = array("large"=>"6");
    $fields["col-3"]["config"]       = array("large"=>"12");
    $fields["col-2"][lang('invoice')] = form_input(array('name'=>'invoice','id'=>'invoice', 'class'=>'span3 focused','disabled'=>'disabled', 'value'=>$invoice));
    $fields["col-1"][lang('customer')] = form_input(array('name'=>'customer','id'=>'customer', 'class'=>'span3 focused', 'value'=>$customer));
    $fields["col-1"]['no-label'] = '<a href="#" class="button radius right"  data-reveal-id="customerModal"><i class=" icon-user"></i>&nbsp;Crear Usuario </a><label id="labelName"></label><label id="labelEmail"></label><label id="labelMobile"></label><label id="labelAddress"></label>';
    $fields["col-1"]['no-label1'] = '
             <fieldset>
            <legend>Agregar Productos</legend>

            <label>Escribe el nombre del producto o marca
              <input type="text" name="products" id="products">
            </label>
            <label>Detalle del producto
              <input type="text" name="details" id="details" placeholder="IMEI:XXXX,REFERENCIA:XXXX,DETALLE:XXXX">
            </label>
              <input id="productId" name="productId" value="0" type="hidden">
              <input id="productName" name="productName" value="0" type="hidden">
              <input id="productPrice" name="productPrice" value="0" type="hidden">
              <input id="productTax" name="productTax" value="0" type="hidden">
              <input id="productOffer" name="productOffer" value="0" type="hidden">
              <a id="addProduct" href="#" class="button radius right"  ><i class=" icon-plus"></i>&nbsp;Agregar</a>
          </fieldset>
            ';
    $fields["col-3"]['no-label2'] = '
                <table class="large-12">
                    <thead>
                      <tr>
                        <th width="600">Producto</th>
                        <th width="100">Cantidad</th>
                        <th class="right">Iva</th>
                        <th class="right">Precio</th>
                      </tr>
                    </thead>
                    <tbody id="productsList">
                    </tbody>
                  </table>
            ';
    $fields["col-2"][lang('order')] = form_input(array('name'=>'order', 'class'=>'span3 focused','disabled'=>'disabled', 'value'=>$order));
//    $fields[lang('orderDate')] = form_input(array('name'=>'orderDate', 'class'=>'span3 focused', 'value'=>$orderDate));
//    $fields[lang('reference')] = form_input(array('name'=>'reference', 'class'=>'span3 focused', 'value'=>$reference));
//    $fields[lang('responsePol')] = form_input(array('name'=>'responsePol', 'class'=>'span3 focused', 'value'=>$responsePol));
//    $fields["col-2"][lang('status')] = form_dropdown("status", $opStatus, $status, "id='status' class='filters span4'  data-col='7'");
    $fields["col-2"][lang('payment_method')] = form_dropdown("paymentMethod", $opPay, $paymentMethod, "id='paymentMethod' class='filters span4'  data-col='7'");
    $fields["col-2"][lang('no-label3')] = "<div class='columns large-8 large-push-4 financial-zone hide'><label>"
                                            .  lang("initial_fee")."</label>"
                                            .form_input(array('name'=>'initialNum', 'id'=>'initialNum', 'class'=>'span3 focused', 'value'=>$cupon))."<br><label>"
                                            .form_dropdown("initial", $opFee, $paymentMethod, "id='initial' class='filters span4'  data-col='7'")."<br><label>"
                                            .  lang("quotes")."</label>"
                                            .form_dropdown("quotes", $opQuotes, $paymentMethod, "id='quotes' class='filters span4'  data-col='7'")."</div>"
                                            ."<div class='columns large-8  channel-zone hide'><label>"
                                            .  lang("extra_cost")."</label>"
                                            .form_input(array('name'=>'manualCost', 'id'=>'manualCost', 'class'=>'span3 focused', 'value'=>""))."<br><label>"
                                            ."</div>";
//    $fields["col-2"][lang('offer')] = form_dropdown("offer", $opOffers, $offer, "id='offer' class='filters span4'  data-col='7'");
    $fields["col-2"][lang('cupon')] = form_input(array('name'=>'cupon', 'class'=>'span3 focused', 'value'=>$cupon));
    $fields["col-2"]['no-label'] = '<a id="validateCode" href="#" class="button radius right disabled" ><i class=" icon-credit-card"></i>&nbsp;Validar código</a>';
    $fields["col-2"][lang('value')] = form_input(array('name'=>'value', 'id'=>'value', 'class'=>'span3 focused text-right', 'disabled'=>'disabled', 'value'=>0))
                                    ."<div class='columns large-12 financial-cost hide'>"
                                    ."<label class='columns large-6'>".  lang("extra_cost")."</label><label id='extraval'  class='columns large-6 right'></label>"
                                    ."<label class='columns large-6 label-perquote'></label><label id='perquote' class='columns large-6 right'></label></div>"
                                    ."<label class='columns large-6'>".  lang("total")."</label><label id='extratotal' class='columns large-6 right'></label>"
                                    ."</div>"
                                    ."<div class='columns large-12 channel-cost hide'>"
                                    ."<label class='columns large-6'>".  lang("extra_cost")."</label><label id='extraval'  class='columns large-6 right'></label>"
                                    ."<label class='columns large-6'>".  lang("total")."</label><label id='extratotal' class='columns large-6 right'></label>"
                                    ."</div>";
    $fields["col-3"]['no-label'] = '';
    $hidden = array('id' => $id,'customerId'=>0);
    echo print_form('/orders/persist/', $fields, $hidden,'form',false,12);