<div class="large-10 large-centered columns">
    <input name="cid" id="cid"   type="hidden"  value=""  >
    <div class="guests" >
        <div class="row">
            <input  type="hidden" id="order" name="order" value="<?php echo $order ?>"/>
            <div class="large-2 columns "><?php echo lang('total')." ".lang('invoice') ?></div>
            <div class="large-3 columns ">
                <input  type="text" id="invoice-total" name="invoice-total" readonly="readonly" class="required" placeholder="<?php echo $total ?>" value="<?php echo $total ?>"/>
            </div>
            <div class="large-3 columns ">
                <!--<input  type="text" id="last_name" name="last_name" class="required" placeholder="<?php echo lang('last_name') ?>" value="<?php echo $last_name ?>"/>-->
            </div>
            <div class="large-4 columns ">
<!--                    &nbsp;<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email') ?>" value=""/>-->
            </div>                           
        </div>                           
    </div>                           
</div>                           
<table>
  <thead>
    <tr>
        <th width="5%"><?php echo lang("quantity")?></th>
        <th width="40%"><?php echo lang("product")?></th>
        <th width="10%"><?php echo lang("iva")?></th>
        <th width="20%"><?php echo lang("value")?></th>
        <th width="25%"><?php echo lang("detail")?></th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($list as $idx => $product){?>
        <tr>
          <td><input type="text" id="quantity_<?php echo $product["id"] ?>" name="quantity[]" class="number text-right" value="<?php echo $product["quantity"] ?>"/></td>
          <td><input type="hidden" id="id_<?php echo $product["id"] ?>" name="ids[]" value="<?php echo $product["id"] ?>"/><?php echo $product["product"] ?></td>
          <td><input type="text" id="tax_<?php echo $product["id"] ?>" name="taxes[]" class="number text-right" value="<?php echo $product["tax"] ?>"/></td>
          <td><input type="text" id="value_<?php echo $product["id"] ?>" name="values[]" class="number product-val text-right" value="<?php echo $product["value"] ?>"/></td>
          <td><input type="text" id="description_<?php echo $product["id"] ?>" class="" name="descriptions[]" value="<?php echo $product["detail"] ?>"/></td>
        </tr>
      <?php }?>
    
  </tbody>
</table>    

<div class="row">
    <div class="large-3 columns large-push-9 ">
        <a id="updateProducts" href="#" class="button radius"  ><i class=" icon-check-sign"></i>&nbsp;Actualizar Productos </a>
    </div>  
</div>
    
<?php 
   