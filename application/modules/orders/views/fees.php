<div class="large-10 large-centered columns">
    <input name="cid" id="cid"   type="hidden"  value=""  >
    <div class="guests" >
        <div class="row">
            <input  type="hidden" id="order" name="order" value="<?php echo $order ?>"/>
            <div class="large-2 columns "><?php echo lang('total')." ".lang('invoice') ?></div>
            <div class="large-3 columns ">
                <input  type="text" id="invoice-total" name="invoice-total" readonly="readonly" class="required" placeholder="<?php echo $total ?>" value="<?php echo $total ?>"/>
            </div>
            <div class="large-3 columns ">
                <input  type="text" id="payment" name="last_name" class="required" placeholder="valor a registrar" value=""/>
            </div>
            <div class="large-4 columns ">
                <a id="updateProducts" href="#" class="button radius"  ><i class=" icon-check-sign"></i>&nbsp;Actualizar Pagos </a>
<!--                    &nbsp;<input  type="text" id="email" name="email" class="required" placeholder="<?php echo lang('email') ?>" value=""/>-->
            </div>                           
        </div>                           
    </div>                           
</div>                           
<table>
  <thead>
    <tr>
        <th width="25%"><?php echo lang("date")?></th>
        <th width="25%"><?php echo lang("status")?></th>
        <th width="25%"><?php echo lang("value")?></th>
        <th width="25%"><?php echo lang("paid")?></th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($list as $idx => $fee){?>
      <?php $status = ($fee["status"]==4)?"Pagado":"Pendiente";?>
      <?php $disabled = (true)?"disabled='disabled'":"";?>
        <tr>
          <td><input type="hidden" id="id_<?php echo $fee["id"] ?>" name="ids[]" value="<?php echo $fee["id"] ?>"/><?php echo $fee["payment_date"]->format("d-m-y") ?></td>
          <td><input type="text" id="status_<?php echo $fee["id"] ?>" name="status[]" disabled="disabled" class="number text-right" value="<?php echo $status ?>"/></td>
          <td><input type="text" id="values_<?php echo $fee["id"] ?>" name="values[]" disabled="disabled" class="number  text-right" value="<?php echo $fee["value"] ?>"/></td>
          <td><input type="text" id="paids_<?php echo $fee["id"] ?>" <?php echo $disabled?> class="number product-val text-right" name="paids[]" value="<?php echo $fee["paid"] ?>"/></td>
        </tr>
      <?php }?>
    
  </tbody>
</table>    

<div class="row">
    <div class="large-3 columns large-push-9 ">
        
    </div>  
</div>
<div data-alert="" id="alert" class="alert-box" style="display: none;"><div id="message"></div><a href="#" class="close">×</a></div>    
<?php 
   