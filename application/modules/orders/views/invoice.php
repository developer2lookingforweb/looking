<style>
    
      body {
        color: black;
        font-family: Helvetica;
        font-weight: 300;
      }
    /*#footer, #header{*/
     #footer{
        background: none;
        text-align: center;
        /*display: none;*/
     }
     #header{
        display: none;
    }
    .body-content {
        background: none;
    }
    .pdfrow::before, .pdfrow::after {
        content: " ";
        display: table;
      }
      .pdfrow {
        margin: 0 auto;width: 100%;
      }
      .pdfrow::after {
        clear: both;
      }
      
      label {
    color: #4d4d4d;
    cursor: pointer;
    display: block;
    font-size: 0.875rem;
    font-weight: normal;
    line-height: 1.5;
    margin-bottom: 0;
  }
    
.pdfcolumn.large-centered, .pdfcolumns.large-centered {
  float: none;
  margin-left: auto;
  margin-right: auto;
}/**/
    .invoice{
        font-size: 1.8rem;
        color: #CD6A51;
        font-style: italic;
      }
    .invoice-date {
        font-size: 1.2rem;
        font-style: italic;
        padding-bottom: 5%;
      }
    .seller-info, .buyer-info{
        border-radius: 15px;
        background: #ededed;
        min-height: 200px;
    }
    label.customer{
        font-weight: 400;
        font-size: 1.1rem;
    }
    label.name{
        font-style: italic;
        font-size: 1.1rem;
    }
    .invoice-head{
        border-bottom: 3px solid #cfcfcf;
        padding-bottom: 4px;
    }
    .products{
        max-height: 420px;
    }
    .products-head label{
        color: #FFF;
    }
    .products-head div{
        font-size: 1.1rem;
        background: #CD6A51;
        padding: 2px 0;
        margin: 5px 0;
        
    }
    .products-row {
        padding: 2px 0;
        border-top: 1px solid #cfcfcf;
    }
    .total {
        font-weight: 400;
        border-top: 1px dotted #cfcfcf;
        border-bottom: 1px solid #cfcfcf;
        padding: 14px 0;
    }
</style>

<div class="" style="margin: 0 auto;width: 100%;">
    <div class="large-centered" style="width: 732px;">
        <div class=" invoice-head" style="margin: 0px auto; width: 100%; height: 390px;">
            <div class=" text-left" style="width:360px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;height: 220px;">
                <img src="<?php echo FCPATH;?>images/alofijosmall.png"/>
                <label><?php echo $disclamer;?></label>
                <label><?php echo $resolution;?></label>
                <label><?php echo $resolution3;?></label>
                <label><?php echo $resolution2;?></label>
            </div>
            <div class="text-right" style="width:325px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;height: 220px;">
                <img src="<?php echo FCPATH.$qr;?>" style="text-align: right;margin-left: 220px;" />
                <div class="invoice"  style="text-align: right;">
                    Factura No. <?php echo $reference;?>
                </div>
                <div class="invoice-date" style="text-align: right;">
                    <?php echo $referenceDate;?>
                </div>
            </div>
            <div class=" seller-info" style="margin-top: 210px; margin-left: -380px;width: 250px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;">
                <label >&nbsp;</label>
                <label class="name"><?php echo $sellerName ?></label>
                <label><?php echo $sellerNit ?></label>
                <label><?php echo $sellerAddress ?></label>
                <label><?php echo $sellerPhone ?></label>
                <label><?php echo $sellerWeb ?></label>
                <label><?php echo $sellerMail ?></label>
                <br/>
            </div>
            <div class=" buyer-info" style="margin-left: -340px;margin-top: 210px;width:400px;float: left; position: relative;padding-left: 0.9375em;padding-right: 0.9375em;">
                <label class="customer"><?php echo lang('customer') ?></label>
                <label class="name"><?php echo $customerName ?></label>
                <label><?php echo lang('dni'),": " ?><?php echo $customerDni ?></label>
                <label><?php echo lang('address'),": " ?><?php echo $customerAddress." - ".$customerCity ?></label>
                <label><?php echo lang('mobile'),": " ?><?php echo $customerMobile ?> </label>
<!--                <label><?php echo $customerCity ?> </label>-->
                <!--<label><?php echo $customerState ?></label>-->
                <label><?php echo lang('email'),": " ?><?php echo $customerEmail ?></label>
                <br/>
            </div>


        </div>
        <div class="products" style="height:360px">
            <div class="products-head" style="margin: 0 0 0 0px;width: 700px;">
                <div class="" style="width: 61px;float: left;text-align: center"><label>Item</label></div>
                <div class="" style="width:350px;float: left;text-align: center"><label>Producto</label></div>
                <div class="" style="width: 40px;float: left;text-align: center"><label>Iva</label></div>
                <div class="" style="width: 37px;float: left;text-align: center"><label>Cant</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>Costo</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>Total</label></div>
            </div>
            <?php 
            foreach($items as $product){
            ?>
            <br/>
            <div class="products-row" style="margin-top:20px;width: 730px; height:10px ;display: block;">
                <div class="" style="width: 60px;float: left;text-align: center; "><label><?php echo $product['cod']?><?php echo $suffix?></label></div>
                <div class="" style="width:358px;float: left;"><label><?php echo $product['product']?></label></div>
                <div class="" style="width: 40px;float: left;text-align: center"><label><?php echo $product['tax']?>%<?php echo $suffix?></label></div>
                <div class="" style="width: 35px;float: left;text-align: center"><label><?php echo $product['quantity']?><?php echo $suffix?></label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>$<?php echo number_format($product['cost'])?><?php echo $suffix?></label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>$<?php echo number_format($product['total'])?><?php echo $suffix?></label></div>
            </div>
            <br/>
            <?php 
            }
            ?>
            <br/>
            <div class="products-row" style="margin-top:20px;width: 730px; height:10px ;display: block;">
                <div class="" style="width: 60px;float: left;text-align: center; "><label>&nbsp;</label></div>
                <div class="" style="width:358px;float: left;"><label>&nbsp;</label></div>
                <div class="" style="width: 40px;float: left;text-align: center"><label>&nbsp;</label></div>
                <div class="" style="width: 35px;float: left;text-align: center"><label>&nbsp;</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>&nbsp;</label></div>
                <div class="" style="width: 122px;float: left;text-align: center"><label>&nbsp;</label></div>
            </div>
        </div>
        <?php if($status == AuthConstants::IN_CANCELED){?>
        <img src="<?php echo FCPATH;?>images/anulada.png" style="transform: rotate(335deg); top: 560px; left: 200px; position: fixed; z-index: 1000;"/>
        <?php }else if($status == AuthConstants::IN_ERASE){?>
        <img src="<?php echo FCPATH;?>images/borrador.png" style="transform: rotate(335deg); top: 560px; left: 200px; position: fixed; z-index: 1000;"/>
        <?php }?>
        <div class="" style="">
            <div class="total" style="margin: 0 auto;width: 730px;">
                <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                    <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('subtotal') ?></label></div>
                    <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($subtotal)?></label></div>
                </div>
                <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                    <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('iva') ?></label></div>
                    <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($iva)?></label></div>
                </div>
                <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                    <div class="" style="width: 600px; text-align: right;float: left;"><label><?php echo lang('total') ?></label></div>
                    <div class="" style="width: 122px; text-align: right;float: left;"><label>$<?php echo number_format($total)?></label></div>
                </div>
            </div>
            <div class="" style="">
                <div class="" style="margin: 0 auto;width: 730px; height: 40px;display: block;border: 1px solid white;">
                    <div class="" style="width: 730px; text-align: center; font-size: 8pt;"><label>Los equipos facturados se reciben a satisfacción, garantía de 180 días contados a partir de la fecha, no cubre mal uso de software, humedad, golpes,</label></div>
                    <div class="" style="width: 730px; text-align: center; font-size: 8pt;"><label>evidencia de aperturas o mala manipulación, pantalla en blanco o apagada, display ni flex.  No se hace devolución de dinero.</label></div>
                </div>
            </div>
            <div class="" style="">
                <div class="" style="margin: 0 auto;width: 730px; height: 20px;display: block;border: 1px solid white;">
                    <div class="" style="width: 730px; text-align: center; font-size: 9pt;"><label>Consignaciones en el banco AVVillas o grupo AVAL cuenta corriente 052-06689-1 Looking For Web S.A.S. NIT 900.929.208-8</label></div>
                    <div class="" style="width: 730px; text-align: center; font-size: 9pt;"><label>Esta factura de venta se asimila en todos sus efectos legales a un titulo valor según Art. 774 del Código de Comercio y lo estipulado por la ley 1231 de 2008</label></div>
                    <div class="" style="width: 730px; text-align: center; font-size: 9pt;"><label>Favor abstenerse de realizar retención en la fuente de acuerdo a lo estipulado en la Ley 1429 de 2010.</label></div>
                </div>
            </div>
        </div>
    </div>
</div>