<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MY_Controller 
{
        public $sauth_noauth = array("invoice","epayco","epaycoc","assign","psersistAssign","feepayment","feeresponse","feeconfirmation","documents","daily_payments","monthly_resume");
	public function index()
	{
		$actions = array();
		$actions["create_order"] = site_url("orders/form");
		
		$data = array();
		$data["title"] = lang("Orders");
		$this->view('list', $data, $actions);
	}
	
	public function daily_payments(){
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('off')
                ->select( "o.id, c.name, c.last_name, c.email, c.mobile, o.invoice, off.value,off.payment_date" )
                ->join( "off.order","o" )
                ->join( "o.customer","c" )
                ->where("off.status != '".AuthConstants::IN_APPROVED."'")
                ->orwhere("off.status is NULL")
                ->andWhere("off.payment_date = '".date("Y-m-d")."'")
//                ->andWhere("off.payment_date = '2018-05-02'")
                ->getQuery();
            $payments = $query->getResult();
            if(count($payments)>0){
                $html = "<table class='customers' style='font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;		border-collapse: collapse;		width: 100%;'>";
                $html .= "  <tbody>";
                $html .= "  <tr>";
                $html .= "      <th style='border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #EE4B28;color: white;'>Nombre</th>";
                $html .= "      <th style='border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #EE4B28;color: white;'>Celular</th>";
                $html .= "      <th style='border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #EE4B28;color: white;'>Factura</th>";
                $html .= "      <th style='border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #EE4B28;color: white;'>Valor</th>";
                $html .= "  </tr>";
                foreach ($payments as $key => $row) {
                    $this->loadRepository("ProductsOrders");
                    $query2 = $this->ProductsOrders->createQueryBuilder("o")
                            ->select("p.product")
                            ->join("o.order","ord")
                            ->join("o.product","p")
                            ->where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
                            ->andwhere("ord.id = ".$row["id"])
                            ->getQuery();
                    $products = $query2->getResult();
                    $prods = array();
                    foreach ($products as $aProd) {
                        $prods[] = $aProd["product"];
                    }
                    $prods = implode(" + ", $prods);
                    $to = array();
                    $to[]= array('name'=>$row["name"],'mail'=>  $row["email"]);
                    $params = array();
                    $params['*|MC_PREVIEW_TEXT|*'] = "Hoy vence el pago de tu cuota";
                    $params['*|CURRENT_YEAR|*'] = date("Y");
                    $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                    $params['*|NAME|*'] = $row["name"];
                    $params['*|INVOICE|*'] = $row["invoice"];
                    $params['*|PRODUCT|*'] = $prods;
                    $params['*|VALUE|*'] = number_format($row["value"]);
                    $this->load->library('mailgunmailer');
                    $this->mailgunmailer->notifyInvoice($to,"Cuotas alofijo.com","paymentClient",$params,false,false);
                    
                    $html .= "  <tr>";
                    $html .= "      <td style='border: 1px solid #ddd;padding: 8px;'>".$row["name"]." ".$row["last_name"]."</td>";
                    $html .= "      <td style='border: 1px solid #ddd;padding: 8px;'>".$row["mobile"]."</td>";
                    $html .= "      <td style='border: 1px solid #ddd;padding: 8px;'>".$row["invoice"]."</td>";
                    $html .= "      <td style='border: 1px solid #ddd;padding: 8px;'>$ ".number_format($row["value"])."</td>";
                    $html .= "  </tr>";
                    
                }
                $html .= "  </tbody>";
                $html .= "  </table>";
                $to = array();
                $to[]= array('name'=>"Ventas Alofijo",'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
                $params = array();
                $params['*|MC_PREVIEW_TEXT|*'] = "Estos son los pagos para hoy";
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                $params['*|TABLE|*'] = $html;
                $this->load->library('mailgunmailer');
                $this->mailgunmailer->notifyInvoice($to,"Cobros del día","dailyPayment",$params,false,false);
            }
	}
	public function monthly_resume(){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
            $data=array();
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('off')
                ->select( "o.id, c.name, c.last_name, c.email, c.mobile, c.address, c.dni, o.order_date, o.invoice, o.value" )
                ->join( "off.order","o" )
                ->join( "o.customer","c" )
                ->where("off.status != '".AuthConstants::IN_APPROVED."'")
                ->orwhere("off.status is NULL")
                ->groupby("o.invoice")
//                ->andWhere("off.payment_date = '2018-05-02'")
                ->getQuery();
            $invoices = $query->getResult();
            
            foreach($invoices as $key => $row){
//                if($key < 4)continue;
                /** Obtener las cuotas en las que se ha abonado pago aun si no es completo */
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "off.id, o.id ordernum, off.value, off.paid, off.transaction_date, off.payment_date" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
                    ->andwhere("off.paid > 0")
    //                ->andWhere("off.payment_date = '2018-05-02'")
                    ->getQuery();
                $paid = $query->getResult();
                /** Obtener el total que se adeuda*/
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "SUM(off.value) - SUM(off.paid) due" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
                    ->andwhere("(off.status <> ".AuthConstants::IN_APPROVED." OR off.status is NULL)")
    //                ->andWhere("off.payment_date = '2018-05-02'")
                    ->getQuery();
                $totalDue = $query->getResult();
                
                /** Obtener el pago minimo*/
                $dateEnd = new DateTime('last day of this month');
                
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "SUM(off.value) - SUM(off.paid) due" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
                    ->andwhere("(off.status <> ".AuthConstants::IN_APPROVED." OR off.status is NULL)")
                    ->andWhere("off.payment_date < '".$dateEnd->format("Y-m-d")."'")
                    ->getQuery();
                $minPayment = $query->getResult();
                
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "COUNT(o.id) total" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
//                    ->andwhere("(off.status <> ".AuthConstants::IN_APPROVED." OR off.status is NULL)")
                    ->getQuery();
                $totalFees = $query->getResult();
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "MAX(off.id) total" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
                    ->andwhere("off.paid <> 0")
                    ->getQuery();
                $lastPaymentId = $query->getResult();
                $lastPaymentRecord = $this->OrdersFees->find($lastPaymentId[0]['total']);
                $lastPay = $lastPaymentRecord->getPaid();
                if(is_array($lastPaymentRecord->getHistory())){
                    $last = array_pop($lastPaymentRecord->getHistory());
                    $lastPay = $last["amount"];
                }
                $query = $this->OrdersFees->createQueryBuilder('off')
                    ->select( "COUNT(o.id) total" )
                    ->join( "off.order","o" )
                    ->where("o.id = ".$row["id"])
                    ->andwhere("off.status = ".AuthConstants::IN_APPROVED)
                    ->getQuery();
                $paidFees = $query->getResult();
                
                /** Obtener nombre de los productos*/
                $this->loadRepository("ProductsOrders");
                $query2 = $this->ProductsOrders->createQueryBuilder("o")
                        ->select("p.product")
                        ->join("o.order","ord")
                        ->join("o.product","p")
                        ->where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
                        ->andwhere("ord.id = ".$row["id"])
                        ->getQuery();
                $products = $query2->getResult();
                $prods = array();
                foreach ($products as $aProd) {
                    $prods[] = $aProd["product"];
                }
                $prods = implode(" + ", $prods);
                
                
                $data = array();
                $data["name"] = $row['name']." ".$row["last_name"];
                $data["email"] = $row['email'];
                $data["address"] = $row['address'];
                $data["mobile"] = $row['mobile'];
                $data["dni"] = $row['dni'];
                $data["order_date"] = $row['order_date']->format("Y-m-d");
                $data["invoice"] = $row['invoice'];
                $data["value"] = $row['value'];
                $data["paid"] = $paid;
                $data["product"] = $prods;
                $data["total_due"] = $totalDue[0]["due"];
                $data["last_payment"] = $lastPay;
                $data["min_payment"] = $minPayment[0]["due"];
                $data["min_payment_date"] = date("Y-m-").$row['order_date']->format("d");
                $data["fees"] = $paidFees[0]["total"]."/".$totalFees[0]["total"];
                echo $filename = "extracto".$row["id"].date("Ymd").".pdf";
                $this->load->helper('../libraries/pdf');
                $pdf = new pdf();
//                $this->pdf->set_option('enable_html5_parser', TRUE);
                $pdf->set_option('isHtml5ParserEnabled', true);
                $pdf->load_view('resume',$data);
                $pdf->render();
                file_put_contents("invoices/".$filename, $pdf->output());
//                $pdf->stream($filename);
                unset($pdf);
//                if($key == 4)break;
                $to = array();
                $to[]= array('name'=>$row["name"],'mail'=>  $row["email"]);
                $params = array();
                $params['*|MC_PREVIEW_TEXT|*'] = "Hoy vence el pago de tu cuota";
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                $params['*|NAME|*'] = $row["name"];
                $params['*|INVOICE|*'] = $row["invoice"];
                $params['*|PRODUCT|*'] = $prods;
                $params['*|VALUE|*'] = number_format($minPayment[0]["due"]);
//                $this->load->library('mailgunmailer');
//                $this->mailgunmailer->notifyInvoice($to,"Resumen mensual","monthly_resume",$params,false,false,$filename);
            }
	}
	
	
	public function setListParameters()
	{
            $this->load->library('../modules/looking/controllers/looking');
            $this->session->set_userdata(AuthConstants::CATEGORIES,        $this->looking->getCategories(1));
            $this->load->helper('action');

            $model = new Model("Orders", "o", array("id" => "id",'order_reference' => 'orderReference'  , 'invoice' => 'invoice'  ,'order_date' => 'orderDate' ,'value' => 'value' ));
            $model->setNumerics(array("o.id"));

            $status = new Model("OrdersStatus", "s", array( 'name' => 'status' ));
            $status->setRelation("status");
            $status->setNumerics(array("s.id"));

            $payment = new Model("PaymentMethods", "p", array( 'method' => 'method' ));
            $payment->setRelation("payment_method");
            $payment->setNumerics(array("p.id"));

            $client = new Model("Customers", "c", array( 'name' => 'name'  ));
            $client->setRelation("customer");
            $client->setNumerics(array("c.id"));

            $actions = array();
            array_push($actions, new Action("orders", "customer", "customer",true,array("column"=>5,"validation"=>"Borrador","validator"=>"==")));        
            array_push($actions, new Action("orders", "products", "products",true,array("column"=>5,"validation"=>"Borrador","validator"=>"==")));        
            array_push($actions, new Action("orders", "fixing", "fixing",false,array("column"=>6,"validation"=>"De contado/ Pago online","validator"=>"==")));        
    //        array_push($actions, new Action("orders", "delete", "delete", false));        
            array_push($actions, new Action("orders", "invoice", "download", true, array("column"=>2,"validation"=>0,"validator"=>"!=")));        
            array_push($actions, new Action("orders", "validationRequest", "request-validation", true, array("column"=>5,"validation"=>"Borrador","validator"=>"==")));        
            $this->loadRepository("PaymentMethods");
            $financial = $this->PaymentMethods->find(AuthConstants::PM_FINANCE);
            array_push($actions, new Action("orders", "documents", "documents", true, array("column"=>array(5,6),"validation"=>array("Aprobada",$financial->getMethod()),"validator"=>array("==","=="),"operator"=>" && ")));        
            array_push($actions, new Action("orders", "fees", "register_payment", true, array("column"=>array(5,6),"validation"=>array("Aprobada",$financial->getMethod()),"validator"=>array("==","=="),"operator"=>" && ")));        

            $relations = array();
            array_push($relations, $status);
            array_push($relations, $payment);
            array_push($relations, $client);

            $this->model   = $model;
            $this->actions = $actions;
            $this->relations = $relations;
	}
    
    public function fixing (){
        
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        
        $id = $this->input->post("id");
        $this->loadRepository("OrdersStatus");
        $this->loadRepository("Orders");
        $this->loadRepository("ProductsOrders");
        $this->loadRepository("Products");
        
        $order = $this->Orders->find($id);
        $status = $this->OrdersStatus->find(2);
        $products = $this->ProductsOrders->findBy(array("order"=>$id));
        $taxProduct = $this->Products->find(AuthConstants::FN_FINANCIAL_ID);
        
        $order->setStatus($status);
        $this->em->persist($order);
        
        $costs = 0;
        $insert = true;
        foreach ($products as $key => $row) {
            if($row->getProduct()->getId()== AuthConstants::FN_FINANCIAL_ID){
                $insert = false;
            }
            if($row->getProduct()->getCategory()->getId() == 5){
                if($row->getValue() < AuthConstants::FN_TAXLIMIT_TOP){
                    $message = lang("order_fixed");
                    $costs += $row->getValue() - AuthConstants::FN_TAXLIMIT;
                    $row->setValue(AuthConstants::FN_TAXLIMIT);
                    $this->em->persist($row);
                }else{
                    $insert = false;
                }
            }
        }
        
        if($insert){
            $extra = new models\ProductsOrders();
            $extra->setOrder($order);
            $extra->setProduct($taxProduct);
            $extra->setQuantity(1);
            $extra->setTax(0);
            $extra->setValue($costs);

            $this->em->persist($extra);
            
        }
        $this->em->flush();
        
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
    
    public function customer ($identifier = 0){
        $customer = "";
        if ($identifier > 0){
           
            $this->loadRepository("Orders");
            $order = $this->Orders->find($identifier);
//            $order    = (object)$order->toArray();
//                echo "<pre>";
//                var_dump($order);exit;
            $customer = $order->getCustomer();
        }
        $this->loadRepository("CustomersData");
        $customerData = $this->CustomersData->findOneBy(array("customer"=>$customer->getId()));
        if(count($customerData)==0 || is_null($customerData)){
            $cusData = new models\CustomersData();
            $cusData->setCustomer($customer);
            $this->em->persist($cusData); 
            $this->em->flush();
        }
        $actions = array();
        $actions["return_order"] = site_url("orders/index");
        
      
        $this->load->library('../modules/looking/controllers/looking');
        $data['title'] = $order->getOrder();
        $data['countries'] = $this->looking->getCountries();
        $data['docTypes'] = $this->looking->getDocTypes();
        $data['id'] = $customer->getId();
        $data['name'] = $customer->getName();
        $data['last_name'] = $customer->getLastName();
        $data['docType'] = ($customer->getDocType()!=null)?$customer->getDocType()->getId():"";
        $data['city'] = ($customer->getCity()!=null)?$customer->getCity()->getId():"";
        $data['state'] = ($customer->getCity()!=null)?$customer->getCity()->getState()->getId():"";
        $data['country'] = ($customer->getCity()!=null)?$customer->getCity()->getState()->getCountry()->getId():"";
        $data['dni'] = $customer->getDni();
        $data['email'] = $customer->getEmail();
        $data['phone'] = $customer->getPhone();
        $data['mobile'] = $customer->getMobile();
        $data['address'] = $customer->getAddress();
        $data['neighborhood'] = "";
        $data['age'] = "";
        $data['company'] = "";
        $data['comp_address'] = "";
        $data['comp_phone'] = "";
        $data['comp_age'] = "";
        $data['earnings'] = "";
        $data['comp_site'] = "";
        $data['contract_type'] = "";
        $data['extra_earnings'] = "";
        $data['concept'] = "";
//            var_dump($customer->name);exit;
//        var_dump($data);exit;
        $this->view('customer', $data, $actions);
    }
    
    public function products ($identifier = 0){
        $products = "";
        if ($identifier > 0){
            $this->loadRepository("ProductsOrders");
            $products = $this->ProductsOrders->findBy(array("order"=>$identifier));
            $order = $this->ProductsOrders->findOneBy(array("order"=>$identifier));
        }
        $list = array();
        
        foreach($products as $product){
            $list[]= array(
                "id" => $product->getId(),
                "quantity" => $product->getQuantity(),
                "product" => $product->getProduct()->getProduct(),
                "value" => $product->getValue(),
                "tax" => $product->getTax(),
                "detail" => $product->getDetail(),
            );
        }
//        echo "<pre>";
//        var_dump($list);
//        exit;
        $actions = array();
        $actions["return_order"] = site_url("orders/index");
        
      
        $this->load->library('../modules/looking/controllers/looking');
        $data['title'] = $order->getOrder()->getOrder();
        $data['list'] = $list;
        $data['order'] = $order->getOrder()->getId();
        $data['total'] = $order->getOrder()->getValue();
//        var_dump($data);exit;
        $this->view('products', $data, $actions);
        
    }
    public function fees ($identifier = 0){
        $fees = "";
        if ($identifier > 0){
            $this->loadRepository("OrdersFees");
            $fees = $this->OrdersFees->findBy(array("order"=>$identifier));
        }
        $list = array();
        $order = '';
        if(count($fees)>0){
            
            foreach($fees as $aFee){
                $order = $aFee->getOrder();
                $list[]= array(
                    "id" => $aFee->getId(),
                    "value" => $aFee->getValue(),
                    "payment_date" => $aFee->getPaymentDate(),
                    "status" => $aFee->getStatus(),
                    "paid" => $aFee->getPaid(),
                );
            }
    //        echo "<pre>";
    //        var_dump($order);
    //        exit;
            $actions = array();
            $actions["return_order"] = site_url("orders/index");


            $this->load->library('../modules/looking/controllers/looking');
            $data['title'] = $order->getOrder();
            $data['list'] = $list;
            $data['order'] = $order->getId();
            $data['total'] = $order->getValue();
    //        var_dump($data);exit;
            $this->view('fees', $data, $actions);
        }else{
            $this->index();
        }
        
    }
    public function persistAssign(){
        $this->loadRepository("Users");
        if($this->input->post("user")>0){
            $user = $this->Users->find($this->input->post("user"));
            $status = ($user->getAssigned()==1)?0:1;

            $user->setAssigned($status);
            $this->em->persist($user);
            $this->em->flush();
        }
        $listUsers = $this->Users->findBy(array("assigned"=>  AuthConstants::ADMIN_OK));
        $assignedList = array();
        foreach ($listUsers as $user){
            $temp = array();
            $temp["id"] = $user->getId();
            $temp["name"] = $user->getName()." ".$user->getLastName();
            $assignedList[] = $temp;
        }
         echo json_encode(array('status'=>true,'data'=>array(  'users'=>$assignedList)));
    }
    public function assign (){
        
      
        $data['title'] = "Asignar usuarios";
        $this->loadRepository("Users");
        $listUsers = $this->Users->findBy(array("profile"=>4));
//        var_dump($data);exit;
        $data['users'] = $listUsers;
        $this->viewFrontend('assign', $data);
        
    }
    
    public function feePayment()
	{
            $this->load->library('../modules/looking/controllers/looking');
            $data['title'] = "Paga tus cuotas";
            $data['payu'] = $this->looking->getPayUData();
            $data['payutest'] = AuthConstants::PAYU_MODE;
            $data['epaykey'] = AuthConstants::EPAY_KEY;
            //            var_dump($data);exit;
            if($this->config->item("test_enviroment")==true){
                $data['epaytest'] = 'true';
            }else{
                $data['epaytest'] = 'false';
            }
            $this->viewFrontend2('payment', $data);
	}
    
    public function getOrdersByDni()
	{
            $this->loadRepository("Customers");
            $customer = $this->Customers->findOneBy(array("dni"=>$this->input->post("dni")));
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "c.id cus,od.id, o.id feeid, o.payment_date, o.value, o.paid" )
                ->join( "o.order","od" )
                ->join( "od.customer","c" )
                ->where("o.status != '".AuthConstants::IN_APPROVED."'")
                ->orWhere("o.status is NULL")
                ->andWhere("c.dni = '".$this->input->post("dni")."'")
                ->getQuery();
             $invoice = $query->getResult();
//            var_dump($query->getDql());
            $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(c.id) total " )
                ->join( "o.order","od" )
                ->join( "od.customer","c" )
                ->andWhere("c.dni = '".$this->input->post("dni")."'")
                ->getQuery();
             $count = $query->getResult();
             $name = $customer->getName()." ".$customer->getLastName();
             $email = $customer->getEmail();
             $nit = $customer->getDni();
             $phone = $customer->getMobile();
             $address = $customer->getAddress();
             $countPending = count($invoice);
             $count =$count[0]["total"];
             $okFees = $count - $countPending;
             foreach ($invoice as $idx => $val){
                $this->loadRepository("ProductsOrders");
                $query = $this->ProductsOrders->createQueryBuilder('o')
                ->select( "p.product" )
                ->join( "o.product","p" )
                ->where('o.order = '.$val['id'])
                ->andWhere('o.product != '.AuthConstants::FN_FINANCIAL_ID)
                ->getQuery();
                $product = $query->getResult();
                $val["name"] = "";
                $val["real"] = $val['value'] - $val['paid'];
                $val["total"] = $count;
                $okFees += 1;
                $val["current"] = $okFees;
                $currentDate = new DateTime();
                $val["expired"] = ($currentDate > $val["payment_date"]);
                foreach ($product as $dx=>$names){
                    $prefix =($dx>0)?",":"";
                    $val["name"] .= $prefix.$names["product"];
                }
                $invoice[$idx] = $val;
                 
             }
//            var_dump($invoice);exit;
            $data =array();
            $data["name"]=$name;
            $data["email"]=$email;
            $data["address"]=$address;
            $data["dni"]=$nit;
            $data["mobile"]=$phone;
            $data["invoices"]=$invoice;
            $response['status']=true;
            $response['data']=$data;
            echo json_encode($response);
	}
    public function getCustomerByDni()
	{
            $this->loadRepository("Customers");
            $customer = $this->Customers->findOneBy(array("dni"=>$this->input->post("dni")));
            $this->loadRepository("OrdersFees");
            
             $name = $customer->getName();
             $lastname = $customer->getLastName();
             $email = $customer->getEmail();
             $nit = $customer->getDni();
             $phone = $customer->getPhone();
             $mobile = $customer->getMobile();
             $address = $customer->getAddress();
             $docType  = null;
             if($customer->getDocType() != null){
                $docType = $customer->getDocType()->getId();
             }   
             $city = null;
             if($customer->getCity() != null){
                $city = $customer->getCity()->getId();
             }   
             $state = "";
             if($city != null){
                 $state = $customer->getCity()->getState()->getId();
             }
             
//            var_dump($invoice);exit;
            $data =array();
            $data["name"]=$name;
            $data["last_name"]=$lastname;
            $data["email"]=$email;
            $data["address"]=$address;
            $data["dni"]=$nit;
            $data["mobile"]=$mobile;
            $data["phone"]=$phone;
            $data["state"]=$state;
            $data["city"]=$city;
            $data["doctype"]=$docType;
            $response['status']=true;
            $response['data']=$data;
            echo json_encode($response);
	}
    public function getSignature()
	{
            $reference = $this->input->post("reference");
            $amount = $this->input->post("value");
            $this->load->library('../modules/looking/controllers/looking');
            $payu = $this->looking->getPayUData();
            
            $response['data']=md5($payu->key."~".$payu->merchant."~".$reference."~".$amount."~COP");
            echo json_encode($response);
	}
    public function epayco($reference){
        // $data = json_decode(file_get_contents('https://secure.epayco.co/validation/v1/reference/'.$_REQUEST['ref_payco']));
        $data = json_decode(file_get_contents('https://secure.payco.co/restpagos/transaction/response.json?ref_payco='.$_REQUEST['ref_payco']."&public_key=". AuthConstants::EPAY_KEY));
        if($data->success){

            // 1	Aceptada
            // 2	Rechazada
            // 3	Pendiente
            // 4	Fallida
            // 6	Reversada
            // 7	Retenida
            // 8	Iniciada
            // 9	Exprirada
            // 10	Abandonada
            // 11	Cancelada
            // 12	Antifraude
            
            $trstatus = array();
            $trstatus["tx_1"] = 4; //Approved
            $trstatus["tx_2"] = 6; //Declined
            $trstatus["tx_9"] = 5; //Expired
            $trstatus["tx_3"] = 7; //Pending
            $trstatus["tx_4"] = 6; //
            $trstatus["tx_6"] = 6; //
            $trstatus["tx_7"] = 6; //
            $trstatus["tx_8"] = 7; //
            $trstatus["tx_10"] = 6; //
            $trstatus["tx_11"] = 6; //
            $trstatus["tx_12"] = 6; //
            
            $pol = ($data->data->x_cod_response == 1)?1:4;
            
            $_REQUEST['buyerEmail'] = $data->data->x_customer_email;
            $_REQUEST['TX_VALUE'] = $data->data->x_amount;
            $_REQUEST['referenceCode'] = $data->data->x_id_invoice;
            $_REQUEST['reference_pol'] = $data->data->x_transaction_id;
            $_REQUEST['transactionState'] = $trstatus["tx_".$data->data->x_cod_response];
            $_REQUEST['polResponseCode'] = $pol;
            $this->feeResponse($reference);
        }else{
            header("Location: ".base_url());
        }
    }
    public function epaycoc($reference){
        // $data = json_decode(file_get_contents('https://secure.epayco.co/validation/v1/reference/'.$_REQUEST['ref_payco']));
        $p_cust_id_cliente = AuthConstants::EPAY_ID;
        $p_key             = AuthConstants::EPAY_PKEY;
        $x_ref_payco      = $_REQUEST['x_ref_payco'];
        $x_transaction_id = $_REQUEST['x_transaction_id'];
        $x_amount         = $_REQUEST['x_amount'];
        $x_currency_code  = $_REQUEST['x_currency_code'];
        $x_signature      = $_REQUEST['x_signature'];
        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
        $x_response     = $_REQUEST['x_response'];
        $x_motivo       = $_REQUEST['x_response_reason_text'];
        $x_id_invoice   = $_REQUEST['x_id_invoice'];
        $x_autorizacion = $_REQUEST['x_approval_code'];
        //Validamos la firma
        
        if ($x_signature == $signature) {


            // 1	Aceptada
            // 2	Rechazada
            // 3	Pendiente
            // 4	Fallida
            // 6	Reversada
            // 7	Retenida
            // 8	Iniciada
            // 9	Exprirada
            // 10	Abandonada
            // 11	Cancelada
            // 12	Antifraude
            
            $trstatus = array();
            $trstatus["tx_1"] = 4; //Approved
            $trstatus["tx_2"] = 6; //Declined
            $trstatus["tx_9"] = 5; //Expired
            $trstatus["tx_3"] = 7; //Pending
            $trstatus["tx_4"] = 6; //
            $trstatus["tx_6"] = 6; //
            $trstatus["tx_7"] = 6; //
            $trstatus["tx_8"] = 7; //
            $trstatus["tx_10"] = 6; //
            $trstatus["tx_11"] = 6; //
            $trstatus["tx_12"] = 6; //
            
            $pol = ($_REQUEST['x_cod_response'] == 1)?1:4;
            
            $_REQUEST['buyerEmail'] = $_REQUEST['x_customer_email'];
            $_REQUEST['TX_VALUE'] = $_REQUEST['x_amount'];
            $_REQUEST['referenceCode'] = $_REQUEST['x_id_invoice'];
            $_REQUEST['reference_pol'] = $_REQUEST['x_transaction_id'];
            $_REQUEST['transactionState'] = $trstatus["tx_".$_REQUEST['x_cod_response']];
            $_REQUEST['polResponseCode'] = $pol;
            $this->feeResponse($reference);
        } else {
            die("Firma no valida");
        }
    }
    public function feeConfirmation($reference){
        $this->feeResponse($reference,FALSE);
    }
    public function feeResponse($reference,$redirect = true){
        
        $response = $_REQUEST;
        if($redirect==true){
            $buyer = $response['buyerEmail'];
            $transactionValue = $response['TX_VALUE'];
            $referenceCode = $response['referenceCode'];
            $referencePol = $response['reference_pol'];
            $transactionState = $response['transactionState'];
            $polResponseCode = $response['polResponseCode'];
        }  else {
            $buyer = $response['email_buyer'];
            $transactionValue = $response['value'];
            $referenceCode = $response['reference_sale'];
            $referencePol = $response['reference_pol'];
            $transactionState = $response['state_pol'];
            $polResponseCode = $response['response_code_pol'];
        }
        $fees = explode("--", $reference);
        $fees = explode("PC000", $fees[0]);
        $fees = explode("-", $fees[1]);
        $this->loadRepository("OrdersFees");
        $this->loadRepository("Customers");
        $email = $buyer;
        $customer = $this->Customers->findOneBy(array('email'=>$email));
        $data = array();
        $date = array();
        $paymentDetail = array();
        $orderId=0;
         if($transactionState == 4 && $polResponseCode == 1){
             
                foreach($fees as $aFee){
                    $fee = $this->OrdersFees->find($aFee);
                    $fee->setReference($referencePol);
                    $fee->setStatus($transactionState);
                    $fee->setResponse($polResponseCode);
                    $fee->setPaid($fee->getValue());
                    $history = json_encode(array(array('amount'=>$fee->getValue(),'date'=>date("Y-m-d"))));
                    $fee->setHistory($history);
                    $fee->setTransactionDate(new DateTime());
                    $this->em->persist($fee);
                    $this->em->flush();
                    $detail = array();
                    $detail["value"] = $fee->getValue();
                    $this->loadRepository("ProductsOrders");
                    $productNames = array();
                    $products = $this->ProductsOrders->findBy(array("order"=>$fee->getOrder()->getId()));
                    foreach ($products as $key => $eachProduct) {
                        if($eachProduct->getProduct()->getId()!== AuthConstants::FN_FINANCIAL_ID)
                            $productNames[] = $eachProduct->getProduct()->getProduct();
                    }
                    $invoicePayment = $fee->getTransactionDate()->format("d-m-y");
                    $productNames = implode(",", $productNames);
                    $detail["description"] = $productNames." ".  lang("fee")." ".$fee->getPaymentDate()->format("d-m-y");
                    $invoice = str_pad($fee->getOrder()->getInvoice(), 8, "0", STR_PAD_LEFT);
                    $paymentDetail[$invoice][]=$detail;
                    if($orderId == 0 || $orderId !== $fee->getOrder()->getId()){
                        $orderId = $fee->getOrder()->getId();
                        $date[] = $fee->getOrder()->getOrderDate()->format("d-m-y");
                    }
                    $customer = $fee->getOrder()->getCustomer();
                }
                $invoiceData = array_keys($paymentDetail);
                $invoiceData = implode(",", $invoiceData);
                $invoiceDate = implode(",", $date);
                $data['title'] = "Pago recibido";
                $data['reference'] = $referencePol;
                $data['name'] = $customer->getName()." ".$customer->getLastName();
                $data['email'] = $customer->getEmail();
                $data['address'] = $customer->getAddress();
                $data['value'] = $transactionValue;
                $data['detail'] = $paymentDetail;
                $data['invoice'] = $invoiceData;
                $data['invoice_date'] = $invoiceDate;
                $data['invoice_payment'] = $invoicePayment;
        //        var_dump($data);exit;
                $this->load->library('pdf');
                $paper_size = array(0,0,148,210);
                $this->pdf->set_paper("a5",'landscape');
                $this->pdf->load_view('feeInvoicePdf',$data);
                $this->pdf->render();
                if(true){
                    file_put_contents("invoices/".$reference.".pdf", $this->pdf->output());
                    $to[]= array('name'=>"Ventas Alofijo",'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
                    $to[]= array('name'=>$customer->getName(),'mail'=>  $customer->getEmail());
                    $params = array();
                    $params['*|MC_PREVIEW_TEXT|*'] = "Gracias por tu pago, eres muy importante para nosotros";
                    $params['*|CURRENT_YEAR|*'] = date("Y");
                    $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                    $params['*|NAME|*'] = $customer->getName();
                    $val = explode(".", $transactionValue);
                    $params['*|VALUE|*'] = $val[0];
                    $this->load->library('mailgunmailer');
                    $this->mailgunmailer->notifyInvoice($to,"Abono de Cuotas","feePayment",$params,false,false,$reference.".pdf");
                }else{
        //            $this->pdf->stream($reference);
                }
         }
        $this->viewFrontend('feeInvoice', $data);
    }
    
    public function form ($identifier = 0)
    {
        $this->session->set_userdata(AuthConstants::CART, array());
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $invoice = $this->input->post('invoice');
        $order = $this->input->post('order');
        $orderDate = $this->input->post('orderDate');
        $reference = $this->input->post('reference');
        $responsePol = $this->input->post('responsePol');
        $value = $this->input->post('value');
        $status = $this->input->post('status');
        $paymentMethod = $this->input->post('paymentMethod');
        $customer = $this->input->post('customer');
        $offer = $this->input->post('offer');
        $cupon = $this->input->post('cupon');
        
        if ($identifier > 0){
            $output = $this->rest->get('orders/order/', array("id"=>$identifier));
        
            if ($output->status){
                $order    = $output->data;
                $invoice = $order->invoice;
                $order = $order->order;
                $orderDate = $order->orderDate;
                $reference = $order->reference;
                $responsePol = $order->responsePol;
                $value = $order->value;
                $status = $order->status;
                $paymentMethod = $order->paymentMethod;
                $customer = $order->customer;
                $offer = $order->offer;
                $cupon = $order->cupon;
                $id         = $order->id;
            }
        }
        
        $actions = array();
        $actions["return_order"] = site_url("orders/index");
        
        $this->loadRepository("PaymentMethods");
        $listPaymentMethods = $this->PaymentMethods->findBy(array("status"=>1));
        $this->loadRepository("OrdersStatus");
        $ordersStatus = $this->OrdersStatus->findBy(array("status"=>1));
        $this->loadRepository("Offers");
        $offersList = $this->Offers->findBy(array("status"=>1));
        
        $this->loadRepository("Orders");
        $query = $this->Orders->createQueryBuilder('o')
           ->select( "MAX(o.invoice)" )
           ->getQuery();
        $invoice = $query->getResult();
        $invoice = $invoice[0][1] +1;
        $query = $this->Orders->createQueryBuilder('o')
           ->select( "MAX(o.id)" )
           ->getQuery();
        $order = $query->getResult();
        $order = $order[0][1] +1;
        $order = "O".str_pad($order, 8, "0", STR_PAD_LEFT);
        $order = $order.time();
        $data["title"]  = lang("Orders");
        $data['invoice'] = $invoice;
        $data['order'] = $order;
        $data['orderDate'] = $orderDate;
        $data['reference'] = $reference;
        $data['responsePol'] = $responsePol;
        $data['value'] = $value;
        $data['status'] = $status;
        $data['paymentMethod'] = $paymentMethod;
        $data['customer'] = $customer;
        $data['offer'] = $offer;
        $data['cupon'] = $cupon;
        $data["id"] = $id;
        $data["listPaymentMethods"] = $listPaymentMethods;
        $data["listOrdersStatus"] = $ordersStatus;
        $data["listOffers"] = $offersList;
        $this->load->library('../modules/looking/controllers/looking');
        $data['countries'] = $this->looking->getCountries();
        $data['docTypes'] = $this->looking->getDocTypes();
            
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $this->load->library('../modules/looking/controllers/looking');
        $this->loadRepository("Customers");
        $this->loadRepository("OrdersStatus");
        $this->loadRepository("PaymentMethods");
        $paymentMethod = $this->PaymentMethods->find($this->input->post("paymentMethod"));
        $this->loadRepository("Orders");
        $query = $this->Orders->createQueryBuilder('o')
           ->select( "MAX(o.invoice)" )
           ->getQuery();
        $invoice = $query->getResult();
        $invoice = $invoice[0][1] +1;
        $query = $this->Orders->createQueryBuilder('o')
           ->select( "MAX(o.promissory_note)" )
           ->getQuery();
        $promissory = $query->getResult();
        $promissory = $promissory[0][1] +1;
        $status = $this->OrdersStatus->find(AuthConstants::IN_ERASE);
        $customer = $this->Customers->find($this->input->post("customerId"));
        $response = array("status"=>false,"data"=>"");
        $reference = "";
        if($customer){
            $cart = $this->session->userdata(AuthConstants::CART);
            if(!isset($cart)|| $cart == FALSE){
                $cart = array();
            }
            $payable = $unpayable = 0;
            $amount = array();
            $payuamount = 0;
            if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
                foreach ($cart as $index => $item) {
                        $product = $this->em->find('models\Products', $item);
                        $pm = $product->getPaymentMethod()->getId();
                        if(! isset($amount[$pm]))$amount[$pm] = 0;
                        $this->load->helper('offers');
                        $obj = new Offers();
                        $offer = $obj->findOffer($product,1);
                        $offerId = $obj->findOfferId($product,1);
                        $realCost = ($offer>0)?$offer:$product->getCost();
                        if($this->session->userdata("VALID_CODE")  && $offer ==0){
                            $cuponPrice = $obj->validateCode($this->session->userdata("REDEMED_CODE"), $product);
                            $realCost = ($cuponPrice>0)?$cuponPrice:$realCost;
                        }
                        $amount[$pm] += ($realCost * $item["quotes"]);
                        if($pm == AuthConstants::PM_USE_PAYU){
                            $payuamount += ($realCost * $item["quotes"]);
                        }
                }
            }
            foreach ($amount as $method => $value) {
                if($this->input->post("paymentMethod") == AuthConstants::PM_FINANCE || $this->input->post("paymentMethod") == AuthConstants::PM_CHANELS){
                    $value = $value + $this->input->post("extraCost");
                }
                $order = $this->looking->generateOrder($customer, $value, $method);
                if(isset($order)){
                    $reference = "O".str_pad($order->getId(), 8, "0", STR_PAD_LEFT);
                    $reference = $reference.time();
                    if($offerId>0 && $method==1){
                        $offerObj = $this->em->find('models\Offers',$offerId);
                        $order->setOffer($offerObj);
                    }
                    if($this->session->userdata("VALID_CODE") && $method==1){
                        $cupon = $this->em->find('models\Cupons',$this->session->userdata("VALID_CODE"));
                        $order->setCupon($cupon);
                    }
                    $order->setOrder($reference);
                    $order->setInvoice($invoice);
                    $order->setStatus($status);
                    $order->setPaymentMethod($paymentMethod);
                    /**
                     * If payment method is PM_FINANCE, fill fees table
                     * 
                     */
                    if($this->input->post("paymentMethod") == AuthConstants::PM_FINANCE){
                        $order->setPromissoryNote($promissory);
                        $quotes = $this->input->post("quotes");
                        $perquote = $this->input->post("perquote");
                        $initial = $value -($perquote * $quotes);
                        $history = json_encode(array(array('amount'=>$initial,'date'=>date("Y-m-d"))));
                        $date = new DateTime();
                        $fees = new models\OrdersFees();
                        $fees->setOrder($order);
                        $fees->setValue($initial);
                        $fees->setPaid($initial);
                        $fees->setPaymentDate($date);
                        $fees->setTransactionDate($date);
                        $fees->setHistory($history);
                        $fees->setStatus(4);
                        $this->em->persist($fees); 
                        $this->em->flush();
                        $date->add(new DateInterval('P01M'));
                        for($i=1; $i<=$quotes; $i++){
                            $history = json_encode(array());
                            $fees = new models\OrdersFees();
                            $fees->setOrder($order);
                            $fees->setPaid(0);
                            $fees->setValue($perquote);
                            $fees->setPaymentDate($date);
                            $fees->setHistory($history);
                            $this->em->persist($fees); 
                            $this->em->flush();
                            $date->add(new DateInterval('P01M'));
                        }
                    }
                    $this->em->persist($order);
                    $this->em->flush();
                    
                }
                if(! $this->input->post('brand')){
                    $addItems = $this->looking->assocItems($order,$method);
                    if($this->input->post("paymentMethod") == AuthConstants::PM_FINANCE 
                        || $this->input->post("paymentMethod") == AuthConstants::PM_CHANELS){
                        $realCost = $this->input->post("extraCost");
                        $productItem = new models\ProductsOrders();
                        $this->loadRepository("Products");
                        $product = $this->Products->find(AuthConstants::FN_FINANCIAL_ID);
                        $productItem->setOrder($order);
                        $productItem->setProduct($product);
                        $productItem->setQuantity(1);
                        $productItem->setValue($realCost);
                        $productItem->setTax($product->getTax());
                        $this->em->persist($productItem);
                        $this->em->flush();
                    }
                }
            }
            $this->validationRequest($order->getId(),FALSE);
//                    var_dump($payU->key."~".$payU->merchant."~".$reference."~".$this->input->post('total')."~COP");
            $data['amount'] = $amount;
//                $data['amount_manual'] = $unpayable;
            $data['reference'] = $reference;
            $data['address'] = $customer->getAddress();
//            $data['city'] = $customer->getCity()->getName();
            $data['email'] = $customer->getEmail();
//            $data['country'] = $customer->getCity()->getState()->getCountry()->getCode();
            $response['status']=true;
            $response['data']=$data;
        }
        echo json_encode($response);
    }

    public function validationRequest($id,$redirect = true){
        $this->invoice($id,true);
        $to = array();
        $to[]= array('name'=>"Raúl Donado",'mail'=>  AuthConstants::ML_OWNEREMAIL);
        $this->loadRepository("Users");
        $listUsers = $this->Users->findBy(array("assigned"=>  AuthConstants::ADMIN_OK));
        foreach ($listUsers as $user){
            $temp = array();
            $temp["name"] = $user->getName()." ".$user->getLastName();
            $temp["mail"] = $user->getEmail();
            $to[] = $temp;
        }
        $params = array();
        $params['*|MC_PREVIEW_TEXT|*'] = AuthConstants::ML_PENDINGSUBJECT;
        $params['*|CURRENT_YEAR|*'] = date("Y");
        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
        $params['*|LINK|*'] = base_url("looking/validateOrder/".  AuthConstants::IN_HASH."TNT".$id);
        $this->load->library('mailgunmailer');
        $this->mailgunmailer->notifyInvoice($to,"Facturas Alofijo","invoiceValidation",$params,false,false,$id.".pdf");
        if($redirect){
            header('Location: '.  base_url("orders"));
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('orders/order', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("order_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
    
    public function getCustomerInfo(){
        $term = $this->input->get("term");
        $this->loadRepository("Customers");
        $query = $this->Customers->createQueryBuilder('p')
           ->select( "p.id id" )
           ->addSelect( "CONCAT(CONCAT(CONCAT(CONCAT(p.name, ' '), p.last_name),' ('),CONCAT(p.email,')')) value, CONCAT(CONCAT(p.name, ' '), p.last_name) name, p.email email, p.mobile mobile, p.address address" )
           ->where("p.name LIKE '%$term%'")
           ->orWhere("p.last_name LIKE '%$term%'")
           ->orWhere("p.email LIKE '%$term%'")
           ->getQuery();
        $customers = $query->getResult();
        echo json_encode($customers);        
    }
    public function getCupons(){
        $term = $this->input->get("term");
        $this->loadRepository("Cupons");
        $query = $this->Cupons->createQueryBuilder('p')
           ->select( "p.id id" )
           ->addSelect( "p.code value, p.code code, p.discount discount, p.filter filter, p.filter_id filter_id, p.end endDate" )
           ->where("p.code LIKE '%$term%'")
           ->getQuery();
        $cupons = $query->getResult();
        echo json_encode($cupons);        
    }
    
    public function getProducts(){
        $term = $this->input->get("term");
        $this->loadRepository("Products");
        $query = $this->Products->createQueryBuilder('p')
           ->select( "p.id id" )
           ->addSelect( " p.product value, p.product product, p.cost cost, p.tax tax" )
           ->join("p.brand"," b")
           ->where("b.brand LIKE '%$term%'")
           ->orWhere("p.product LIKE '%$term%'")
           ->andWhere("p.status = 1")
           ->getQuery();
        $customers = $query->getResult();
        echo json_encode($customers);        
    }
    public function getReferences(){
        $cid = $this->input->post("cid");
        $this->loadRepository("CustomersData");
        $query = $this->CustomersData->createQueryBuilder('p')
           ->select( "p.personal_references references" )
           ->join("p.customer"," c")
           ->where("c.id = '$cid'")
           ->getQuery();
        $customers = $query->getResult();
        if($customers[0]["references"] !== NULL){
            echo ($customers[0]["references"]);        
        }else{
            echo json_encode(array("references"=>array()));
        }
    }
    public function getAssets(){
        $cid = $this->input->post("cid");
        $this->loadRepository("CustomersData");
        $query = $this->CustomersData->createQueryBuilder('p')
           ->select( "p.assets assets" )
           ->join("p.customer"," c")
           ->where("c.id = '$cid'")
           ->getQuery();
        $customers = $query->getResult();
        if($customers[0]["assets"] !== NULL){
            echo ($customers[0]["assets"]);        
        }else{
            echo json_encode(array("assets"=>array()));
        }
    }
    public function getJob(){
        $cid = $this->input->post("cid");
        $this->loadRepository("CustomersData");
        $query = $this->CustomersData->createQueryBuilder('p')
           ->select( "p.job job, p.age age,p.neighborhood neighborhood" )
           ->join("p.customer"," c")
           ->where("c.id = '$cid'")
           ->getQuery();
        $customers = $query->getResult();
        echo json_encode(array("job"=>json_decode($customers[0]["job"]),"age"=>$customers[0]["age"],"neighborhood"=>$customers[0]["neighborhood"]));        
    }
    public function findOffer(){
        $id = $this->input->post("itemId");
        $this->loadRepository("Products");
        $product = $this->Products->find($id);
        $this->load->helper('offers');
        $off = new Offers();
        $procutOfferPrice = $off->findOffer($product,true);
        $offerId = $off->findOfferId($product,true,"id");
        echo json_encode(array('status'=>true,'data'=>array('offerPrice'=>$procutOfferPrice,'offerId'=>$offerId)));        
    }
    public function addCustomer(){
        $email = $this->input->post('email');
        $this->loadRepository("Customers");
        $customer = $this->Customers->findBy(array('email' => $email));
        if(count($customer)===0){
            $customer = new models\Customers();
            $customer->setEmail($email);
            $customer->setName($this->input->post('name'));
            $customer->setLastName($this->input->post('last_name'));
            $customer->setAddress($this->input->post('address'));
            $city = $this->em->find('models\Cities', $this->input->post('city'));
            if($city !== null)$customer->setCity($city);
            $type = $this->em->find('models\DocumentTypes', $this->input->post('docType'));
            if($type !== null)$customer->setDocType($type);
            $customer->setDni($this->input->post('dni'));
            $customer->setMobile($this->input->post('mobile'));
            $customer->setPhone($this->input->post('phone'));
            $customer->setCreationDate(new DateTime(date("Y-m-d")));
            $customer->setLastAccess(new DateTime(date("Y-m-d H:i:s")));
            $this->em->persist($customer);
            $this->em->flush();
            $this->load->helper('sendy');
            $sendy = new Sendy(AuthConstants::SDY_MANUALINVOICE);
            $sendy->subscribe($this->input->post('name')." ".$this->input->post('last_name'), $this->input->post('email'));
            
            $id = $customer->getId();
            echo json_encode(array('status'=>true,'data'=>array(  'id'=>$id,
                                                    'name'=>$customer->getName()." ".$customer->getLastName(),
                                                    'email'=>$customer->getEmail(),
                                                    'mobile'=>$customer->getMobile(),
                                                    'address'=>$customer->getAddress()
            )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistCustomer(){
        $cid = $this->input->post('cid');
        $this->loadRepository("Customers");
        $customer = $this->Customers->find($cid);

        if(count($customer)!==0){
            $customer->setEmail($this->input->post('email'));
            $customer->setName($this->input->post('name'));
            $customer->setLastName($this->input->post('last_name'));
            $customer->setAddress($this->input->post('address'));
            $city = $this->em->find('models\Cities', $this->input->post('city'));
            if($city !== null)$customer->setCity($city);
            $type = $this->em->find('models\DocumentTypes', $this->input->post('docType'));
            if($type !== null)$customer->setDocType($type);
            $customer->setDni($this->input->post('dni'));
            $customer->setMobile($this->input->post('mobile'));
            $customer->setPhone($this->input->post('phone'));
            $customer->setCreationDate(new DateTime(date("Y-m-d")));
            $customer->setLastAccess(new DateTime(date("Y-m-d H:i:s")));
            $this->em->persist($customer);
            $this->em->flush();
            
            $id = $customer->getId();
            echo json_encode(array('status'=>true,'data'=>array(  'id'=>$id,
                                                    'name'=>$customer->getName()." ".$customer->getLastName(),
                                                    'email'=>$customer->getEmail(),
                                                    'mobile'=>$customer->getMobile(),
                                                    'address'=>$customer->getAddress()
            )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistReference(){
        $cid = $this->input->post('cid');
        $this->loadRepository("CustomersData");
        $customer = $this->CustomersData->findOneBy(array("customer"=>$cid));

        if(count($customer)!==0){
            $newRef = new StdClass();
            if($this->input->post("name") !== '' && $this->input->post("mobile")!==''){
                $newRef->name = $this->input->post("name");
                $newRef->phone = $this->input->post("mobile");
                $newRef->job = $this->input->post("job");

                $references = $customer->getPersonalReferences();
                if(is_null($references)){
                    $references = '{"references":[]}';
                }
                $references = json_decode($references);
                
//                var_dump($references);
                if(!is_null($references->references)){
                    
                    array_push($references->references,$newRef);
                }else{
                    $references->references[] = $newRef;
                }
                $references = json_encode($references);
                $customer->setPersonalReferences($references);
                $this->em->persist($customer);
                $this->em->flush();
                
            }
            
            $id = $customer->getId();
            echo json_encode(array('status'=>true,'data'=>array( )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistAssets(){
        $cid = $this->input->post('cid');
        $this->loadRepository("CustomersData");
        $customer = $this->CustomersData->findOneBy(array("customer"=>$cid));

        if(count($customer)!==0){
            $newAsset = new StdClass();
            if($this->input->post("type") !== '' && $this->input->post("value")!==''){
                $newAsset->type = $this->input->post("type");
                $newAsset->spec = $this->input->post("spec");
                $newAsset->value = $this->input->post("value");
                $newAsset->reference = $this->input->post("reference");
                $newAsset->pledge = $this->input->post("pledge");
                $newAsset->balance = $this->input->post("balance");

                $assets = $customer->getAssets();
                if(is_null($assets)){
                    $assets = '{"assets":[]}';
                }
                $assets = json_decode($assets);
//                var_dump($references);
                if(!is_null($assets->assets)){
                    
                    array_push($assets->assets,$newAsset);
                }else{
                    $assets->assets[] = $newAsset;
                }
                $assets = json_encode($assets);
                $customer->setAssets($assets);
                $this->em->persist($customer);
                $this->em->flush();
                
            }
            
            $id = $customer->getId();
            echo json_encode(array('status'=>true,'data'=>array( )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistJob(){
        $cid = $this->input->post('cid');
        $this->loadRepository("CustomersData");
        $customer = $this->CustomersData->findOneBy(array("customer"=>$cid));

        if(count($customer)!==0){
            $newJobInfo = new StdClass();
            if($this->input->post("company") !== '' && $this->input->post("comp_phone")!==''){
                $newJobInfo->company = $this->input->post("company");
                $newJobInfo->address = $this->input->post("address");
                $newJobInfo->phone = $this->input->post("phone");
                $newJobInfo->earnings = $this->input->post("earnings");
                $newJobInfo->site = $this->input->post("comp_site");
                $newJobInfo->age = $this->input->post("comp_age");
                $newJobInfo->contract = $this->input->post("contract_type");
                $newJobInfo->extra_earnings = $this->input->post("extra_earnings");
                $newJobInfo->concept = $this->input->post("concept");

                $jobInfo = json_encode($newJobInfo);
                $customer->setJob($jobInfo);
                $customer->setAge($this->input->post("age"));
                $customer->setNeighborhood($this->input->post("neighborhood"));
                $this->em->persist($customer);
                $this->em->flush();
                
            }
            
            $id = $customer->getId();
            echo json_encode(array('status'=>true,'data'=>array( )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistProducts(){
        $orderId = $this->input->post("order");
        $totalOrder = $this->input->post("total");
        $products = $this->input->post("products");
        $this->loadRepository("Orders");
        $order = $this->Orders->find($orderId);
        $order->setValue($totalOrder);
        $this->em->persist($order);
        foreach ($products as $key => $item) {
            $this->loadRepository("ProductsOrders");
            $productDetail = $this->ProductsOrders->find($item["id"]);
            $productDetail->setValue($item["value"]);
            $productDetail->setQuantity($item["quantity"]);
            $productDetail->setTax($item["tax"]);
            $productDetail->setDetail($item["description"]);
            $this->em->persist($productDetail);
            
        }
        $this->em->flush();
        if($totalOrder == $order->getValue()){
            echo json_encode(array('status'=>true,'data'=>array(  'id'=>$orderId,
            )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function persistFeePayments(){
        $orderId = $this->input->post("order");
        $totalOrder = $this->input->post("total");
        $payments = $this->input->post("payments");
        $paymentValue = $this->input->post("payments");
        $this->loadRepository("Orders");
        $order = $this->Orders->find($orderId);
        $customer = $order->getCustomer();
        $referencePol = $orderId.rand(800000, 999999);
        $date = array();
        $transactionValue =0;
        $reference = array();
        $this->loadRepository("OrdersFees");
        $fees = $this->OrdersFees->findBy(array("order"=>$orderId));
        foreach ($fees as $key => $eachFee) {
            if($eachFee->getStatus()!== AuthConstants::IN_APPROVED){
                if($payments > 0){
                    $history = json_decode($eachFee->getHistory());
                    $history[] = array('amount'=>$paymentValue,'date'=>date("Y-m-d"));
                    $eachFee->setHistory(json_encode($history));
                    if($payments < $eachFee->getValue()){
                        /**
                         * Si no hay pagos acumulados abona lo pagado que es menor a una cuota y continua
                         * de lo contrario evalua cada caso
                         */
                        if($eachFee->getPaid() == null || $eachFee->getPaid() == 0){
                            $eachFee->setPaid($payments);
                            $payments = 0;
                        }else{
                            /**
                             * si el valor acumulado mas el pago es menor que la cuota asigna el pago y finaliza
                             * de lo contrario abona la cuota y se prepara para abonar a la siguiente
                             */
                            if($payments + $eachFee->getPaid() < $eachFee->getValue()){
                                $eachFee->setPaid($payments + $eachFee->getPaid());
                                $payments = 0;
                            }else{
                                $substract = $eachFee->getValue() - $eachFee->getPaid();
                                $payments -= $substract;
                                $eachFee->setStatus(AuthConstants::IN_APPROVED);
                                $eachFee->setPaid($eachFee->getValue());
                                
                            }
                        }
                    }else{
                        if($eachFee->getPaid() == null || $eachFee->getPaid() == 0){
                           $payments -= $eachFee->getValue();
                            
                        }else{
                           $substract = $eachFee->getValue() - $eachFee->getPaid();
                           $payments -= $substract;
                        }
                        $eachFee->setStatus(AuthConstants::IN_APPROVED);
                        $eachFee->setPaid($eachFee->getValue());
                    }
                    $eachFee->setReference($referencePol);
                    $eachFee->setTransactionDate(new DateTime());
                }
            }
            $this->em->persist($eachFee);
            $this->em->flush();
        }
        
        /**/
        
        $query = $this->OrdersFees->createQueryBuilder('off')
            ->select( "SUM(off.value) - SUM(off.paid) avrg, COUNT(off.id) counter" )
            ->where("off.status != '".AuthConstants::IN_APPROVED."'")
            ->orwhere("off.status is NULL")
            ->andWhere("off.order = '".$orderId."'")
//                ->andWhere("off.payment_date = '2018-05-02'")
            ->getQuery();
        $pending = $query->getResult();
        
        $reference= $referencePol;
        $transactionValue = $payments;
        $detail = array();
        $detail["value"] = $paymentValue;
        $this->loadRepository("ProductsOrders");
        $productNames = array();
        $products = $this->ProductsOrders->findBy(array("order"=>$order->getId()));
        foreach ($products as $key => $eachProduct) {
            if($eachProduct->getProduct()->getId()!== AuthConstants::FN_FINANCIAL_ID)
                $productNames[] = $eachProduct->getProduct()->getProduct();
        }
        $invoicePayment = date("d-m-y");
        $productNames = implode(",", $productNames);
        $detail["description"] = $productNames;
        $invoice = str_pad($order->getInvoice(), 8, "0", STR_PAD_LEFT);
           
        $paymentDetail= array();
        $paymentDetail[$invoice][]=$detail;
//            if($orderId == 0 || $orderId !== $fee->getOrder()->getId()){
//                $orderId = $fee->getOrder()->getId();
//                $date[] = $fee->getOrder()->getOrderDate()->format("d-m-y");
//            }
//            $invoicePayment = $fee->getTransactionDate()->format("d-m-y");
//            $this->em->persist($fee);
            
        $reference = "PC000".$reference;
        $data['title'] = "Pago recibido";
        $data['reference'] = $referencePol;
        $data['name'] = $customer->getName()." ".$customer->getLastName();
        $data['email'] = $customer->getEmail();
        $data['address'] = $customer->getAddress();
        $data['detail'] = $paymentDetail;
        $data['invoice'] = $invoice;
        $data['invoice_date'] = $order->getOrderDate()->format("Y-m-d");
        $data['invoice_payment'] = $invoicePayment;
        $data['manual_payment'] = true;
        $data['pending_amount'] = number_format($pending[0]["avrg"]);
        $data['pending_fees'] = $pending[0]["counter"];
//        var_dump($data);exit;
        $this->load->library('pdf');
        $paper_size = array(0,0,148,210);
        $this->pdf->set_paper("a5",'landscape');
        $this->pdf->load_view('feeInvoicePdf',$data);
        $this->pdf->render();
//        $this->pdf->stream($reference);
        if(true){
            file_put_contents("invoices/".$reference.".pdf", $this->pdf->output());
            $to[]= array('name'=>"Raúl Donado",'mail'=>  AuthConstants::ML_OWNEREMAIL);
            $to[]= array('name'=>$customer->getName(),'mail'=>  $customer->getEmail());
            $params = array();
            $params['*|MC_PREVIEW_TEXT|*'] = "Gracias por tu pago, eres muy importante para nosotros";
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            $params['*|NAME|*'] = $customer->getName();
            $val = explode(".", $transactionValue);
            $params['*|VALUE|*'] = number_format($paymentValue);
            $this->load->library('mailgunmailer');
            $this->mailgunmailer->notifyInvoice($to,"Abono de Cuotas","feePayment",$params,false,false,$reference.".pdf");
        }else{
        $this->pdf->stream($reference);
        }
        
        
        
        /**/
        if($totalOrder == $order->getValue()){
            echo json_encode(array('status'=>true,'data'=>array(  'id'=>$orderId,
            )));
        }else {
            echo json_encode(array('status'=>false,'id'=>0));
        }
    }
    public function documents($oid){
        
        $this->loadRepository("Orders");
        $order = $this->Orders->find($oid);
        $data['title'] = "Pago recibido";
        $data['promissory'] = $order->getPromissoryNote();
        $data['fullName'] = $order->getCustomer()->getName()." ".$order->getCustomer()->getLastName();
        $data['address'] = $order->getCustomer()->getAddress();
        $data['mobile'] = $order->getCustomer()->getMobile();
        $data['email'] = $order->getCustomer()->getEmail();
        $data['dni'] = $order->getCustomer()->getDni();
        $data['city'] = $order->getCustomer()->getCity()->getName();
        $data['type'] = $order->getCustomer()->getDocType()->getDocumentType();
        $this->loadRepository("CustomersData");
        $extraData = $this->CustomersData->findOneBy(array("customer"=>$order->getCustomer()->getId()));
        $data['age'] = $extraData->getAge();
        $data['neighborhood'] = $extraData->getNeighborhood();
        $data['job'] = json_decode($extraData->getJob());
        $data['assets'] = json_decode($extraData->getAssets());
        $data['personalReferences'] = json_decode($extraData->getPersonalReferences());
        $this->loadRepository("ProductsOrders");
        $products = $this->ProductsOrders->findBy(array("order"=>$order->getId()));
        $this->loadRepository("OrdersFees");
        $invoiceFees = $this->OrdersFees->findBy(array("order"=>$order->getId()));
        $data['products'] = $products;
        $data['total'] = $order->getValue();
        $data['fees'] = count($invoiceFees)-1;
        $data['first'] = ($invoiceFees[0]->getValue());
        $data['percuote'] = ($invoiceFees[1]->getValue());
        $data['paymentdate'] = ($invoiceFees[1]->getPaymentDate()->format("d"));
        $this->load->library('pdf');
        $paper_size = array(0,0,148,210);
        $this->pdf->set_paper("a4",'portrait');
        $this->pdf->load_view('documents',$data);
        $this->pdf->render();
        $this->pdf->stream("pagare".$order->getPromissoryNote());
//        $this->viewFrontend("documents", $data);
        
    }
    public function getCart(){
        $code = $this->input->post("code");
        $this->session->set_userdata("VALID_CODE",false);
        $this->session->set_userdata("REDEMED_CODE",false);
        $cart = $this->session->userdata(AuthConstants::CART);
        if(!isset($cart)|| $cart == FALSE){
            $cart = array();
        }
        $description_gifts = "";
        $subtotal = 0;
        $codeRedemed = 0;
        if(is_array($this->session->userdata(AuthConstants::CART)) && count($cart)>0){
            foreach ($cart as $index => $item) {
                    $product = $this->em->find('models\Products', $item['id']);
                    $cart[$index]['name'] = $product->getProduct();
                    $cart[$index]['tax'] = $product->getTax();
                    if($code !== FALSE && $item['offer']==0){
                        $this->load->helper('offers');
                        $obj = new Offers();
                        $newPrice = $obj->validateCode($code,$product);
                        if($newPrice>0){
                            $cart[$index]['cost'] = $newPrice;
                            $subtotal += $newPrice;
                            $codeRedemed = 1;
                            $codeId = $obj->getCodeId($code);
                            $this->session->set_userdata("VALID_CODE",$codeId);
                            $this->session->set_userdata("REDEMED_CODE",$code);
                        }
                    }else{
                        $subtotal += $item['cost'];
                    }
            }
        }
        echo json_encode(array('status'=>true,'total'=>$subtotal,'cart'=>$cart));
    }
    public function qr($reference){
        $qr = $qrFile = "images/invoiceqr/$reference.png";
        $this->load->library('qrcodelib');
        $qr = $this->qrcodelib->standardQr(base_url("looking/invoice/".$reference),  $qrFile);
        echo "<img src='".base_url($qr)."' />";
    }
    public function invoice($reference,$saveFile = false){
        $orderId = ltrim(ltrim($reference, "O"),"0");

        $order = $this->em->find('models\Orders', $orderId);
        $customer = $order->getCustomer();
        $qr = $qrFile = "images/invoiceqr/$reference.png";
        $this->load->library('qrcodelib');
        $qr = $this->qrcodelib->standardQr(base_url("orders/invoice/".$reference),  $qrFile);
        $this->loadRepository("ProductsOrders");
        $products = $this->ProductsOrders->findBy(array('order'=>$order->getId()));
        $items = array();
        $taxCost = 0;
        $totalCost = 0;
        if(count($products)>0){
            foreach ($products as $product) {
                $tax = $product->getTax();
//                    var_dump($product->getTax());exit;
                $taxOperator = "1.".str_pad($tax, 2, "0", STR_PAD_LEFT);
                $suffix ="";
                if($product->getDetail()!== null && $product->getDetail()!== ""){
                    $suffix ="<p>&nbsp;</p>";
                }
                $items[]= array(
                    "cod"=>$product->getProduct()->getId(),
                    "product"=>$product->getProduct()->getBrand()->getBrand()." ".$product->getProduct()->getProduct()."<p>".$product->getDetail()."</p>",
                    "quantity"=>$product->getQuantity(),
                    "tax"=>$tax,
                    "cost"=>$product->getValue()/$taxOperator,
                    "taxCost"=>$product->getValue()-($product->getValue()/$taxOperator),
                    "total"=>($product->getValue() * $product->getQuantity())/$taxOperator,
                );
                $totalCost += ($product->getValue()*$product->getQuantity())/$taxOperator; 
                $taxCost += ($product->getValue()*$product->getQuantity())-(($product->getValue()*$product->getQuantity())/$taxOperator); 
            }
        }else{
            $this->loadRepository("MobileRefills");
            $refill = $this->MobileRefills->findBy(array('order'=>$order->getId()));
            if(count($refill)==1){
                $items[]= array(
                    "cod"=>$refill[0]->getId(),
                    "product"=>"Recargas productos móviles",
                    "quantity"=>1,
                    "cost"=>$refill[0]->getValue(),
                    "total"=>$refill[0]->getValue(),
                );
            }
        }
        $data = array();
        $data['suffix'] = $suffix;
        $data['qr'] = "images/invoiceqr/".$reference.".png";
        $data['items'] = $items;
        $data['subtotal'] = $totalCost;
        $data['iva'] = $taxCost;
        $data['total'] = $order->getValue();
        $data['status'] = $order->getStatus()->getId();
        $data['reference'] = str_pad($order->getInvoice(), 8, "0", STR_PAD_LEFT);
        $data['referenceDate'] = $order->getOrderDate()->format('Y/m/d');
        $data['customerName'] = $customer->getName()." ".$customer->getLastName();
        $data['customerDni'] = $customer->getDni();
        $data['customerAddress'] = $customer->getAddress();
        $data['customerCity'] = $customer->getCity()->getName();
//            $data['customerState'] = $customer->getCity()->getState()->getName();
        $data['customerEmail'] = $customer->getEmail();
        $data['customerMobile'] = $customer->getMobile();
        $data['sellerName'] = AuthConstants::IN_SELLER_NAME;
        $data['sellerAddress'] = AuthConstants::IN_SELLER_ADDRESS;
        $data['sellerPhone'] = AuthConstants::IN_SELLER_PHONE;
        $data['sellerNit'] = AuthConstants::IN_SELLER_NIT;
        $data['sellerWeb'] = AuthConstants::IN_SELLER_WEB;
        $data['sellerMail'] = AuthConstants::ML_INFOEMAIL;
        $data['resolution'] = AuthConstants::IN_SELLER_RES;
        $data['resolution2'] = AuthConstants::IN_SELLER_RES2;
        $data['resolution3'] = AuthConstants::IN_SELLER_RES3;
        $data['disclamer'] = AuthConstants::IN_SELLER_DISC;
        $data['title']= "Factura No ".$reference;
//            $this->view('invoice', $data);
        $this->load->library('pdf');
        $this->pdf->load_view('invoice',$data);
        $this->pdf->render();
        if($saveFile === true){
            file_put_contents("invoices/".$reference.".pdf", $this->pdf->output());
        }else{
            $this->pdf->stream($reference);
        }
    }
}