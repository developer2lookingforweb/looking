<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                category: 'required',
				image: 'required',
				status: 'required',
				home: 'required',
				order: 'required',
				parent: 'required',
            },
            messages: {
                category:'<?=lang('required')?>',
				image:'<?=lang('required')?>',
				status:'<?=lang('required')?>',
				home:'<?=lang('required')?>',
				order:'<?=lang('required')?>',
				parent:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
    });
</script>