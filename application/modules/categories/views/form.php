<?php 
    $opStat = array();
    $opStat["0"] = lang("unpublished");
    $opStat["1"] = lang("published");
    $opYN = array();
    $opYN["0"] = lang("no");
    $opYN["1"] = lang("yes");
    $fields = array();
    $fields[lang('category')] = form_input(array('name'=>'category', 'class'=>'span3 focused', 'value'=>$category));
//	$fields[lang('image')] = form_input(array('name'=>'image', 'class'=>'span3 focused', 'value'=>$image));
    $fields[lang('status')] = form_dropdown("status", $opStat, $status, "id='status' class='filters span4'  data-col='7'");
    $fields[lang('home')] = form_dropdown("home", $opYN, $home, "id='status' class='filters span4'  data-col='7'");
//    $fields[lang('order')] = form_input(array('name'=>'order', 'class'=>'span3 focused', 'value'=>$order));
//	$fields[lang('parent')] = form_input(array('name'=>'parent', 'class'=>'span3 focused', 'value'=>$parent));
    $hidden = array('id' => $id);
    echo print_form('/categories/persist/', $fields, $hidden);