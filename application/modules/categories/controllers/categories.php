<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["create_category"] = site_url("categories/form");
		
		$data = array();
		$data["title"] = lang("Categories");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Categories", "c", array("id" => "id", 'category' => 'category' ,'position' => 'orderes' ,'status' => 'status' ));
        $model->setNumerics(array("c.id"));
        
        $actions = array();
        array_push($actions, new Action("categories", "form", "edit"));        
        array_push($actions, new Action("categories", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $categoryName = $this->input->post('category');
//		$image = $this->input->post('image');
        $status = $this->input->post('status');
        $home = $this->input->post('home');
        $order = $this->input->post('order');
        
        if ($identifier > 0){
            $output = $this->rest->get('categories/category/', array("id"=>$identifier));
            if ($output->status){
                $category    = $output->data;
                $categoryName = $category->category;
//				$image = $category->image;
				$status = $category->status;
				$home = $category->home;
				$order = $category->order;
                $id         = $category->id;
            }
        }
        
        $actions = array();
        $actions["return_category"] = site_url("categories/index");
        
        $data = array();
        $data["title"]  = lang("Categories");
        $data['category'] = $categoryName;
//		$data['image'] = $image;
		$data['status'] = $status;
		$data['home'] = $home;
		$data['order'] = $order;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('category', 'lang:category', 'required');
//		$this->form_validation->set_rules('image', 'lang:image', 'required');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		$this->form_validation->set_rules('home', 'lang:home', 'required');
//		$this->form_validation->set_rules('order', 'lang:order', 'required');
//		$this->form_validation->set_rules('parent', 'lang:parent', 'required');
                
        if ($this->form_validation->run($this)){
            $this->loadRepository("Categories");
            $query = $this->Categories->createQueryBuilder('o')
               ->select( "MAX(o.position)" )
               ->where( "o.status = 1" )
               ->getQuery();
            $order = $query->getResult();
            $order = $order[0][1] +1;
            $_POST["image"] = "noimage.jpg";
            $_POST["order"] = $order;
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('categories/category', $this->input->post()); 
            }else{
                $output = $this->rest->put('categories/category', $this->input->post()); 
            }
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('category_edition') : lang('category_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('categories/category', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("category_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}