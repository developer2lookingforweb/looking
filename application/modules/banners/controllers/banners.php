<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["create_banner"] = site_url("banners/form");
		
		$data = array();
		$data["title"] = lang("Banners");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Banners", "b", array("id" => "id", 'title' => 'title' ,'image' => 'image' ,'description' => 'description' ,'has_button' => 'has_button' ,'published' => 'published' ,'link' => 'link' ,'align' => 'align'));
        $model->setNumerics(array("b.id"));

        $actions = array();
        array_push($actions, new Action("banners", "form", "edit"));        
        array_push($actions, new Action("banners", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $title = $this->input->post('title');
        $image = $this->input->post('image');
        $description = $this->input->post('description');
        $hasButton = $this->input->post('hasButton');
        $published = $this->input->post('published');
        $link = $this->input->post('link');
        $align = $this->input->post('align');
        $creationDate = $this->input->post('creationDate');
        
        if ($identifier > 0){
            $output = $this->rest->get('banners/banner/', array("id"=>$identifier));
        
            if ($output->status){
                $banner    = $output->data;
                $title = $banner->title;
                $image = $banner->image;
                $description = $banner->description;
                $hasButton = $banner->has_button;
                $published = $banner->published;
                $link = $banner->link;
                $align = $banner->align;
//                $creationDate = $banner->creationDate;
                $id         = $banner->id;
            }
        }
        
        $actions = array();
        $actions["return_banner"] = site_url("banners/index");
        
        $data = array();
        $data["title"]  = lang("Banners");
        $data['title'] = $title;
        $data['image'] = $image;
        $data['description'] = $description;
        $data['hasButton'] = $hasButton;
        $data['published'] = $published;
        $data['link'] = $link;
        $data['align'] = $align;
        $data['creationDate'] = $creationDate;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('title', 'lang:title', 'required');
		$this->form_validation->set_rules('image', 'lang:image', 'required');
		$this->form_validation->set_rules('description', 'lang:description', 'required');
		$this->form_validation->set_rules('hasButton', 'lang:hasButton', 'required');
		$this->form_validation->set_rules('published', 'lang:published', 'required');
		$this->form_validation->set_rules('link', 'lang:link', 'required');
//		$this->form_validation->set_rules('creationDate', 'lang:creationDate', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('banners/banner', $this->input->post()); 
            }else{
                $output = $this->rest->put('banners/banner', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('banner_edition') : lang('banner_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('banners/banner', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("banner_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}