<?php 
    $fields = array();
    $fields[lang('title')] = form_input(array('name'=>'title', 'class'=>'span3 focused', 'value'=>$title));
	$fields[lang('image')] = form_input(array('name'=>'image', 'class'=>'span3 focused', 'value'=>$image));
	$fields[lang('description')] = form_textarea(array('name'=>'description', 'class'=>'span3 focused', 'value'=>$description));
	$fields[lang('published')] = form_input(array('name'=>'published', 'class'=>'span3 focused', 'value'=>$published));
	$fields[lang('align')] = form_input(array('name'=>'align', 'class'=>'span3 focused', 'value'=>$align));
	$fields[lang('hasButton')] = form_input(array('name'=>'hasButton', 'class'=>'span3 focused', 'value'=>$hasButton));
	$fields[lang('link')] = form_input(array('name'=>'link', 'class'=>'span3 focused', 'value'=>$link));
//	$fields[lang('creationDate')] = form_input(array('name'=>'creationDate', 'class'=>'span3 focused', 'value'=>$creationDate));
    $hidden = array('id' => $id);
    echo print_form('/banners/persist/', $fields, $hidden);