<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                title: 'required',
				image: 'required',
				description: 'required',
				hasButton: 'required',
				published: 'required',
				link: 'required',
				creationDate: 'required',
            },
            messages: {
                title:'<?=lang('required')?>',
				image:'<?=lang('required')?>',
				description:'<?=lang('required')?>',
				hasButton:'<?=lang('required')?>',
				published:'<?=lang('required')?>',
				link:'<?=lang('required')?>',
				creationDate:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
    });
</script>