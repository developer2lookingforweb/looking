<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller 
{
    public $public = true;
    
    public function setListParameters(){}
    
	public function index()
	{
		$data = array();
        $data["title"] = "Home";
        $csrf = $this->config->config["csrf_token_name"];
        $data['csrf'] = $csrf;
		
//                var_dump($data["topSalesBrand"]);exit;
		$this->viewGlobal('home', $data);
    }
    
    public function notFound()
	{
        $this->load->library('../modules/looking/controllers/looking');

		$data = array();
        $data["title"] = "No encontrada";
        $data['categories'] = $this->looking->getCategories();
        $csrf = $this->config->config["csrf_token_name"];
        $data['csrf'] = $csrf;
		
//                var_dump($data["topSalesBrand"]);exit;
		$this->viewFrontend2('404', $data);
    }
    
    public function remember()
	{
		// $data = array();
        // $data["title"] = "Home";
        // $csrf = $this->config->config["csrf_token_name"];
        // $data['csrf'] = $csrf;
        // $this->viewGlobal('home', $data);
        $this->loadRepository('MultiproposeRegisters');
        $users = $this->MultiproposeRegisters->findAll();
        // var_dump($users);exit;
        $this->load->library('mailgunmailer');
        $params = array();
        $attach = array();
        foreach($users as $user){
            echo "<li>".$user->getEmail();
            $to = array();
            $to[]= array('name'=>  $user->getName(),'mail'=> $user->getEmail());
            $this->mailgunmailer->notify($to,'Tu master class está por iniciar','inglesalofijoRemember',$params,false,false, $attach);
        }
    }
    
    public function persist(){
        
        $register = new models\MultiproposeRegisters();
        $register->setName($this->input->post('name'));
        $register->setLastName($this->input->post('lastName'));
        $register->setEmail($this->input->post('email'));
        $register->setMobile($this->input->post('phone'));
        $register->setDateRequest(new Datetime());
        $this->em->persist($register);
        $this->em->flush();
        echo json_encode(array("status"=>true,"message"=>"Has quedado inscrito!"));
    }
    
    public function poster(){
        $data = array();
        $data["title"] = "Home";
        $csrf = $this->config->config["csrf_token_name"];
        $data['csrf'] = $csrf;

        // $data['x_ref_payco']  = 4561780;
        // $data['x_description']  = "Ingles alofijo";
        // $data['x_transaction_date']  = "2019-03-15 14:17:53";
        // $data['x_transaction_id']  = "47881552677473";
        // $data['x_response']  = "Aceptada";
        // $data['x_response_reason_text']  = "00-Aprobada";
        // $data['x_cod_response']  = "1";
        // $data['x_bank_name']  = "NEQUI";
        // $data['x_id_invoice']  = "PC0002111-1552677449515";
        // $data['x_franchise']  = "PSE";
        // $data['x_amount_ok']  = "136445";
        // $data['x_customer_name']  = "John ";
        // $data['x_customer_lastname']  = "Jimenez";
        // $data['x_customer_country']  = "co";
        // $data['x_customer_email']  = "johnjime@gmail.com";
        // $data['x_customer_phone']  = "";
        // $data['x_customer_address']  = "";

		
		// $this->viewGlobal('poster', $data);
    }
    
    
}
