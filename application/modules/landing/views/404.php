<?php header("HTTP/1.1 404 Not Found"); ?>
<div class="notfound">
			<div class="notfound-404">
				<h1>404</h1>
			</div>
			<h2>Oops! No encontrada</h2>
			<p>La página que está buscando no se encuentra, pudo haber sido cambiada o está temporalmente no disponible. <a href="<?php echo base_url()?>">Volver al inicio</a></p>
			
		</div>