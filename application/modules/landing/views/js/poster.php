
<script>
var data = {};


(function($) {
  $("#exe").click(function(){

    $.ajax({
        // url: "https://i-biling.com/PagosRestWEB/rest/state/response",
        url: "https://i-biling.com/PagosRestWEB/rest/state/test",
        type: "post",            
        dataType: "json",
        data: { 
                
                fWDnxEXMN_csrf_token_name:$("#csrftag").val(),

                x_ref_payco: '<?php echo $x_ref_payco?>',
                x_description: '<?php echo $x_description?>',
                x_transaction_date: '<?php echo $x_transaction_date?>',
                x_transaction_id: '<?php echo $x_transaction_id?>',
                x_response: '<?php echo $x_response?>',
                x_response_reason_text: '<?php echo $x_response_reason_text?>',
                x_cod_response: '<?php echo $x_cod_response?>',
                x_bank_name : '<?php echo $x_bank_name?>',
                x_id_invoice: '<?php echo $x_id_invoice?>',
                x_franchise: '<?php echo $x_franchise?>',
                x_amount_ok: '<?php echo $x_amount_ok?>',
                x_customer_name: '<?php echo $x_customer_name?>',
                x_customer_lastname: '<?php echo $x_customer_lastname?>',
                x_customer_country: '<?php echo $x_customer_country?>',
                x_customer_email: '<?php echo $x_customer_email?>',
                x_customer_phone: '<?php echo $x_customer_phone?>',
                x_customer_address: '<?php echo $x_customer_address?>',


            },
        success: function(data){ 
            if(data.status == true){
                
              $('#exampleModalLabel').html('Registro al evento');
              $('#exampleModal .modal-body').html(data.message);
              $('#exampleModal').modal('show');
                
            }else{
                $('#modal-message').foundation('reveal', 'close');
                $("#modal-message #modal-title").html("Verifica tus datos");
                $("#modal-message #message").html("Ya hay un usuario registrado con el correo<br/><br/>");
                $('#modal-message').foundation('reveal', 'open');
            }

        }
    });
  })

  function validateaRequired(value){
    value.removeClass('text-danger')
    if(value.val() == ""){
          value.addClass('text-danger')
          return false;
        }else{
          return true;
        }
      };
      function validateaEmail(value){
        value.removeClass('text-danger')
        if(!isaEmail(value.val())){
          value.addClass('text-danger')
          return false;
        }else{
            return true;
        }
      }
      function validateaPhone(value){
        value.removeClass('text-danger')
        if(!isaNumber(value.val())){
          console.log("error")
          value.addClass('text-danger')
          return false;
        }else{
            return true;
        }
      
  };
      
  function isaEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
  };
  function isaNumber(number) {
      var regex = /^\s*-?[0-9]{10}\s*$/;
      return regex.test(number);
  };

  $("#back_index").click(function(){
      setTimeout(function(){window.location.href = "<?php echo base_url()?>";},500);
  })

})(jQuery); // End of use strict


</script>