<script type="text/javascript">
    var colors = [
   "#FF0F00"
  ,"#FF6600"
  ,"#FF9E01"
  ,"#FCD202"
  ,"#F8FF01"
  ,"#B0DE09"
  ,"#04D215"
  ,"#0D8ECF"
  ,"#0D52D1"
  ,"#2A0CD0"
  ,"#8A0CCF"
  ,"#CD0D74"
  ,"#FF0F00"
  ,"#FF6600"
  ,"#FF9E01"
  ,"#FCD202"
  ,"#F8FF01"
  ];
    var chartData1 =[];
    var count = 0;
    <?php foreach($chartSalesProduct as $brand){?>
        chartData1.push({"product":"<?php echo $brand["product"]?>","value":<?php echo $brand['total'] ?>,"color":colors[count]});
        count ++;
    <?php }?>
        
    var chartData2 =[];
    var count = 0;
    <?php foreach($chartSalesChannel as $brand){?>
        chartData2.push({"product":"<?php echo $brand["method"]?>","value":<?php echo $brand['total'] ?>,"color":colors[count]});
        count ++;
    <?php }?>
        
    var chart1 = AmCharts.makeChart( "chartdiv1", {
      "type": "pie",
      "theme": "light",
      "dataProvider": chartData1,
      "valueField": "value",
      "titleField": "product",
      "outlineAlpha": 0.4,
      "depth3D": 15,
      "balloonText": "[[product]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "angle": 30,
      "labelText": "[[percents]]%",
      "percentPrecision": 1,
      "export": {
        "enabled": true
      }
    } );
    var chart2 = AmCharts.makeChart( "chartdiv2", {
      "type": "pie",
      "theme": "light",
      "dataProvider": chartData2,
      "valueField": "value",
      "titleField": "product",
      "outlineAlpha": 0.4,
      "depth3D": 15,
      "balloonText": "[[product]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "angle": 30,
      "labelText": "[[percents]]%",
      "percentPrecision": 1,
      "export": {
        "enabled": true
      }
    } );
    $(document).ready(function() {
        var list = $('#list').dataTable( {
            "aoColumns":[{"bVisible": false},null ,null ,null,{ "bSortable": false,"bSearchable": false }],
            "sAjaxSource": "<?php echo site_url('/home/getSalesList'); ?>",
        });

        var id = 0;
        $( "#confirmation_delete" ).dialog({
            autoOpen: false,
            resizable: false,
            height:"auto",
            modal: true,
            buttons: {
                "<?php echo lang("button_delete_ok"); ?>": function() {
                    $.post("<?php echo site_url('/categories/delete'); ?>",
                    {'id':id, '<?=$csrf?>': $('input[name=<?=$csrf?>]').val()},
                    function(data){
                        $("#confirmation_delete").dialog( "close" );
                        if(data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }
                        
                        if (data.warning != "") {
                            $('#alert').addClass("alert-info");
                            $("#message").html(data.warning);
                            $("#alert").show();
                        }
                        
                        if(data.message != ""){
                            list.fnDraw();
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                    },'json'
                );
                },
                "<?php echo lang("button_delete_ko"); ?>": function() {
                    $( this ).dialog( "close" );
                }
            }
        });

        $(document).on("click", ".action_delete", function(){
     	   id = this.id;
           $( "#message_delete" ).html("<?php echo lang("message_delete_category"); ?>");
           $( "#confirmation_delete" ).dialog("open");
        });
    });
</script>
