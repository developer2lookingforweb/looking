
<script>
var data = {};


(function($) {
  $("#register").click(function(){
    if(validateaRequired($("input[name='name']")) && validateaRequired($("input[name='last_name']")) && validateaEmail($("input[name='email']")) && validateaPhone($("input[name='phone']"))){

      $.ajax({
          url: "<?php echo base_url()?>" + "landing/persist/",
          type: "post",            
          dataType: "json",
          data: { email:$("input[name='email']").val(),
                  name:$("input[name='name']").val(),
                  lastName:$("input[name='last_name']").val(),
                  phone:$("input[name='phone']").val(),
                  fWDnxEXMN_csrf_token_name:$("#csrftag").val()
              },
          success: function(data){ 
              if(data.status == true){
                  
                $('#exampleModalLabel').html('Registro al evento');
                $('#exampleModal .modal-body').html(data.message);
                $('#exampleModal').modal('show');
                  
              }else{
                  $('#modal-message').foundation('reveal', 'close');
                  $("#modal-message #modal-title").html("Verifica tus datos");
                  $("#modal-message #message").html("Ya hay un usuario registrado con el correo<br/><br/>");
                  $('#modal-message').foundation('reveal', 'open');
              }
    
          }
      });
    }
  });

  function validateaRequired(value){
    value.removeClass('text-danger')
    if(value.val() == ""){
          value.addClass('text-danger')
          return false;
        }else{
          return true;
        }
      };
      function validateaEmail(value){
        value.removeClass('text-danger')
        if(!isaEmail(value.val())){
          value.addClass('text-danger')
          return false;
        }else{
            return true;
        }
      }
      function validateaPhone(value){
        value.removeClass('text-danger')
        if(!isaNumber(value.val())){
          console.log("error")
          value.addClass('text-danger')
          return false;
        }else{
            return true;
        }
      
  };
      
  function isaEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
  };
  function isaNumber(number) {
      var regex = /^\s*-?[0-9]{10}\s*$/;
      return regex.test(number);
  };

  $("#back_index").click(function(){
      setTimeout(function(){window.location.href = "<?php echo base_url()?>";},500);
  })

})(jQuery); // End of use strict


</script>