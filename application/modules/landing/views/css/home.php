<style>
    #register{
        width: 100%;
    }
    .text-danger{
        border: 1px solid red;
    }
.note
{
    text-align: center;
    height: 80px;
    background: -webkit-linear-gradient(left, #e2362e, #aa362e);
    color: #fff;
    font-weight: bold;
    line-height: 80px;
}
.form-content
{
    padding: 5%;
    border: 1px solid #ced4da;
    margin-bottom: 2%;
}
.form-control{
    border-radius:1.5rem;
}
.btnSubmit
{
    border:none;
    border-radius:1.5rem;
    padding: 1%;
    width: 20%;
    cursor: pointer;
    background: #e2362e;
    color: #fff;
}

</style>