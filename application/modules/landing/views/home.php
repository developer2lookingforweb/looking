
<div class="row">
  <div class="container text-center"><img src="<?php echo base_url("images/alofijosmall.png")?>" alt=""></div>
</div>
<div class="row">
<div class="row">
  <div class="container text-center">
  <div class="col-lg-6 col-lg-push-3">
    <img src="<?php echo base_url("images/english.jpg")?>" alt="" class="responsive" style="width:100%;">
  </div>
</div>
</div>
<div class="row">
<div class="container register-form">
  <div class="col-lg-6 col-lg-push-3">
            <div class="form">
                <div class="note">                    
                    <p>Inglés alofijo te invita a registrate en la primer MasterClass.</p>
                </div>

                <div class="form-content">
                    <p class="h4">Inscríbete y descubre lo sencillo que es aprender inglés</p>
                    <p class="h4">Fecha:  <strong>12 de marzo</strong></p>
                    <p class="h4">Hora:  <strong>12:30PM</strong></p>
                    <p class="h4">Lugar:  <strong>Online</strong></p>
                    <p class="h4 text-justify">La sesion sera desarrollada por <strong>Alfredo Cure</strong>  médico, ingeniero biomédico con un doctorado en fisiología del nervio auditivo, quien romperá los paradigmas de aprendizaje que han obstaculizado tus ganas de aprender inglés.</p>
                    <p class="h4">Agéndate!</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="name" id="name" type="text" class="form-control" placeholder="Nombres *" value=""/>
                                <input type="hidden" value="<?php echo $csrf?>"/>
                            </div>
                            <div class="form-group">
                                <input name="last_name" id="last_name"  type="text" class="form-control" placeholder="Apellidos *" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input name="email" id="email"  type="text" class="form-control" placeholder="Correo *" value=""/>
                            </div>
                            <div class="form-group">
                                <input name="phone" id="phone"  type="text" class="form-control" placeholder="Celular *" value=""/>
                            </div>
                        </div>
                    </div>
                    <button id="register" type="button" class="btnSubmit">Suscribirme</button>
                </div>
            </div>
        </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        
        <button id="back_index" type="button" class="btn btn-primary">Entendido</button>
      </div>
    </div>
  </div>
</div>