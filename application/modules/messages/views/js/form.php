<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                subject: 'required',
				name: 'required',
				message: 'required',
				email: 'required',
				creationDate: 'required',
            },
            messages: {
                subject:'<?=lang('required')?>',
				name:'<?=lang('required')?>',
				message:'<?=lang('required')?>',
				email:'<?=lang('required')?>',
				creationDate:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
    });
</script>