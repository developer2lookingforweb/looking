<!--<div class="form-group col-md-8 center ">
    <h1 class="text-center">Comunícate con nosotros</h1>
    <div class="form-group col-md-2 center ">&nbsp
    </div>
    <div class="form-group col-md-6 center ">
        <div id="map"></div>
         <style>
          #map {
            width: 100%;
            height: 200px;
            background-color: grey;
          }
        </style>
        <script>
          function initMap() {
            var uluru = {lat: 4.703202, lng: -74.043651};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: uluru
            });
            var marker = new google.maps.Marker({
              position: uluru,
              map: map
            });
          }
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcIid-c1snnPHBFS2zBwMN3JrUkm8FVHw&callback=initMap">
        </script>
    </div>
    <div class="form-group col-md-4 center contact-us-data ">
        <h2>Dónde estamos</h2>
        <p>Dirección: Cra 15a # 124 - 75</p>
        <p>Teléfono:  6202066 Ext 113</p>
        <p>Móvil:  317 8045157</p>
        <p>Correo:  sales@lookingforweb.com</p>
    </div>
    <div class="form-group col-md-12 center "></br>
        <h2 class="text-center">Escríbenos</h2>    
        <form class="form-horizontal" role="form" method="post" action="<?=  base_url("messages/persistUser")?>">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><?=  lang("name")?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" placeholder="Nombres y  apellidos" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label"><?=  lang("email")?></label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" placeholder="ejemplo@dominio.com" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="subject" class="col-sm-2 control-label"><?=  lang("subject")?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="message" class="col-sm-2 control-label"><?=  lang("message")?></label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="4" name="message" placeholder="Mensaje"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2 text-center">
                <button id="submit" name="submit" type="submit" value="Send" class="button btn_tipo1 hvr-float-shadow">Enviar</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <! Will be used to display an alert to the user>
            </div>
        </div>
    </form>
    </div>
</div>-->
<section id="contact" class="bg-dark" >
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 hidden-xs hidden-sm hidden-md col-sm-12 col-xs-12 text-center">
                    <div id="map"></div>
                    <style>
                     #map {
                       width: 100%;
                       height: 672px;
                       background-color: grey;
                     }
                   </style>
                   <script>
                     function initMap() {
                       var uluru = {lat: 4.703202, lng: -74.043651};
                       var map = new google.maps.Map(document.getElementById('map'), {
                         zoom: 16,
                         center: uluru,
                         scrollwheel: false
                       });
                       var marker = new google.maps.Marker({
                         position: uluru,
                         map: map
                       });
                     }
                   </script>
                   <script async defer
                   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcIid-c1snnPHBFS2zBwMN3JrUkm8FVHw&callback=initMap&sensor=false">
                   </script>
                </div>
                <div class="col-lg-8 col-sm-12 col-xs-12 text-center">
                    <div class="col-lg-12 col-lg-offset-2 text-center top">
                        <h2 class="section-heading">Comunícate con nosotros</h2>
                        <hr class="primary">
                        <!--<p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>-->
                    </div>
                    <div class="row">
                        <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                            <i class="fa fa-map-marker fa-3x sr-contact"></i>
                            <p>Cra 15a # 124 - 75</p>
                        </div>
                        <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                            <i class="fa fa-phone fa-3x sr-contact"></i>
                            <p>317 8045157</p>
                        </div>
                        <div class="col-lg-4  col-md-4  col-sm-4  text-center">
                            <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                            <p><a href="mailto:sales@lookingforweb.com">sales@lookingforweb.com</a></p>
                        </div>
                    </div>
                    <div class="form-group col-md-12 col-sm-12  center "></br>
                        <h2 class="text-center">Escríbenos</h2>    
                        <form class="form-horizontal" role="form" method="post" action="<?=  base_url("messages/persistUser")?>">
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 control-label"><?=  lang("name")?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombres y  apellidos" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 control-label"><?=  lang("email")?></label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="ejemplo@dominio.com" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="subject" class="col-sm-3 control-label"><?=  lang("subject")?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Asunto" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message" class="col-sm-3 control-label"><?=  lang("message")?></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="4" name="message" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message" class="col-sm-3 control-label"></label>
                            <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
                            <div class="g-recaptcha col-sm-9 "data-sitekey="6LefsZ0UAAAAAMyh8k2faWj1mphciMYxHU1L7vBp"></div><br/><br/>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 col-sm-offset-2 text-center">
                                <button id="submit" name="submit" type="submit" value="Send" class="btn btn-default btn-xl sr-button">Enviar</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>