<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends MY_Controller 
{
    public $sauth_noauth = array("usermessage","persistuser");
	public function index()
	{
		$actions = array();
		$actions["create_message"] = site_url("messages/form");
		
		$data = array();
		$data["title"] = lang("Messages");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Messages", "m", array("id" => "id", 'subject' => 'subject' ,'name' => 'name' ,'message' => 'message' ,'email' => 'email' ,'creationDate' => 'creationDate'));
        $model->setNumerics(array("m.id"));

        $actions = array();
        array_push($actions, new Action("messages", "form", "edit"));        
        array_push($actions, new Action("messages", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $subject = $this->input->post('subject');
		$name = $this->input->post('name');
		$message = $this->input->post('message');
		$email = $this->input->post('email');
		$creationDate = $this->input->post('creationDate');
        
        if ($identifier > 0){
            $output = $this->rest->get('messages/message/', array("id"=>$identifier));
        
            if ($output->status){
                $message    = $output->data;
                $subject = $message->subject;
				$name = $message->name;
				$message = $message->message;
				$email = $message->email;
				$creationDate = $message->creationDate;
                $id         = $message->id;
            }
        }
        
        $actions = array();
        $actions["return_message"] = site_url("messages/index");
        
        $data = array();
        $data["title"]  = lang("Messages");
        $data['subject'] = $subject;
        $this->load->library('../modules/looking/controllers/looking');

        $data['categories'] = $this->looking->getCategories();
		$data['name'] = $name;
		$data['message'] = $message;
		$data['email'] = $email;
		$data['creationDate'] = $creationDate;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    public function userMessage ()
    {
        $actions = array();
        
        $data = array();
        $data["title"]  = lang("Messages");
        $this->viewFrontend2('form', $data, $actions);
    }
    
    public function persistUser ()
    {
        $data = array();
        $message = "";
        $error   = "true";
        $output  = ""; 
        
        $this->form_validation->set_rules('subject', 'lang:subject', 'required');
        $this->form_validation->set_rules('name', 'lang:name', 'required');
        $this->form_validation->set_rules('message', 'lang:message', 'required');
        $this->form_validation->set_rules('email', 'lang:email', 'required');
        
        
        // your secret key
        $secret = "6LefsZ0UAAAAALBGrY-812lGDnsQQiu-nKiUsad-";
        
        // empty response
        $response = null;
        
        // check secret key
        $this->load->helper('recaptcha');
        $reCaptcha = new ReCaptcha($secret);

        // if submitted check response
        if ($_POST["g-recaptcha-response"]) {
            $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }

        if ($response != null && $response->success) {
            if ($this->form_validation->run($this)){
                if ($this->input->post("id") > 0){
                    $output = $this->rest->post('messages/message', $this->input->post()); 
                }else{
                    $output = $this->rest->put('messages/message', $this->input->post()); 
                }
                
                if (empty($output) == false){
                    if ($output->status){
                        $this->load->helper('sendy');
                        $sendy = new Sendy(AuthConstants::SDY_CONTACTLIST);
                        $sendy->subscribe($this->input->post("name"), $this->input->post("email"));
                        $message = ($this->input->post("id") > 0) ? lang('message_edition') : lang('message_creation');
                    }else{
                        $error = (isset($output->error)) ? $output->error : "";
                    }
                }
                $this->load->library('mailgunmailer');
                $to = array();
                $to[]= array('name'=>  AuthConstants::ML_CONTACTSUBJECT,'mail'=>  AuthConstants::ML_LOGISTICSEMAIL);
    
    
                $params = array();
                $params['*|NAME|*'] = $this->input->post("name");
                $params['*|EMAIL|*'] = $this->input->post("email");
                $params['*|SUBJECT|*'] = $this->input->post("subject");
                $params['*|MESSAGE|*'] = $this->input->post("message");
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
    
                $this->mailgunmailer->notify($to,AuthConstants::ML_CONTACTSUBJECT,"contactus",$params);
                if ($this->input->is_ajax_request() == false){
                    redirect(base_url());
                }
            }else{
                $error = validation_errors();
                if ($this->input->is_ajax_request() == false){
                    $this->form();
                }
            }
        } else {
            $data["message"] = "Captcha no valido";
            $data["error"] = "Captcha no valido";
            if ($this->input->is_ajax_request() == false){
                redirect(base_url("messages/userMessage"));
            }else{
                echo json_encode($data);
            }
        }

        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('subject', 'lang:subject', 'required');
		$this->form_validation->set_rules('name', 'lang:name', 'required');
		$this->form_validation->set_rules('message', 'lang:message', 'required');
		$this->form_validation->set_rules('email', 'lang:email', 'required');
		$this->form_validation->set_rules('creationDate', 'lang:creationDate', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('messages/message', $this->input->post()); 
            }else{
                $output = $this->rest->put('messages/message', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('message_edition') : lang('message_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('messages/message', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("message_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}