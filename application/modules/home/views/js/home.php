
<script>
$("[data-clipped-circle-graph]").each(function() {
  var $graph = $(this),
      percent = parseInt($graph.data('percent'), 10),
      deg = 30 + (300*percent)/100;
  if(percent > 50) {
    $graph.addClass('gt-50');
  }
  $graph.find('.clipped-circle-graph-progress-fill').css('transform','rotate('+ deg +'deg)');
  $graph.find('.clipped-circle-graph-percents-number').html(percent+'%');
});

var chartData =[];
<?php foreach($dueChart as $property){?>
    chartData.push({"date":"<?php echo $property["odate"]?>-15","value":<?php echo $property['value'] ?>});
<?php }?>
var colors = [
   "#FF0F00"
  ,"#FF6600"
  ,"#FF9E01"
  ,"#FCD202"
  ,"#F8FF01"
  ,"#B0DE09"
  ,"#04D215"
  ,"#0D8ECF"
  ,"#0D52D1"
  ,"#2A0CD0"
  ,"#8A0CCF"
  ,"#CD0D74"
  ,"#FF0F00"
  ,"#FF6600"
  ,"#FF9E01"
  ,"#FCD202"
  ,"#F8FF01"
  ];
var chartData2 =[];
var count = 0;
<?php foreach($topSalesBrand as $brand){?>
    chartData2.push({"brand":"<?php echo $brand["brand"]?>","value":<?php echo $brand['conteo'] ?>,"color":colors[count]});
    count ++;
<?php }?>
var chartData3 =[];
var count = 0;
<?php foreach($topSales as $brand){?>
    chartData3.push({"product":"<?php echo $brand["product"]?>","value":<?php echo $brand['total'] ?>,"color":colors[count]});
    count ++;
<?php }?>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
  "theme": "light",
  "marginRight": 140,
  "marginLeft": 140,
  "autoMarginOffset": 80,
  "dataDateFormat": "YYYY-MM-DD",
  "valueAxes": [ {
    "id": "v1",
    "axisAlpha": 0,
    "position": "left",
    "ignoreAxisWidth": true
  } ],
  "balloon": {
    "borderThickness": 1,
    "shadowAlpha": 0
  },
  "graphs": [ {
    "id": "g1",
    "balloon": {
      "drop": true,
      "adjustBorderColor": false,
      "color": "#ffffff",
      "type": "smoothedLine"
    },
    "fillAlphas": 0.2,
    "bullet": "round",
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "bulletSize": 3,
    "hideBulletsCount": 50,
    "lineThickness": 2,
    "title": "black line",
    "type": "smoothedLine",
    "useLineColorForBulletBorder": true,
    "valueField": "value",
    "balloonText": "<span style='font-size:12px;'>[[value]]</span>"
  } ],
  "chartCursor": {
    "valueLineEnabled": true,
    "valueLineBalloonEnabled": true,
    "cursorAlpha": 0,
    "zoomable": false,
    "valueZoomable": true,
    "valueLineAlpha": 0.5
  },
  "chartScrollbar": {
        "graph":"g1",
        "gridAlpha":0,
        "color":"#888888",
        "scrollbarHeight":55,
        "backgroundAlpha":0,
        "selectedBackgroundAlpha":0.1,
        "selectedBackgroundColor":"#888888",
        "graphFillAlpha":0,
        "autoGridCount":true,
        "selectedGraphFillAlpha":0,
        "graphLineAlpha":0.2,
        "graphLineColor":"#c2c2c2",
        "selectedGraphLineColor":"#888888",
        "selectedGraphLineAlpha":1

    },
  "categoryField": "date",
  "categoryAxis": {
    "parseDates": true,
    "dashLength": 1,
    "minorGridEnabled": true
  },
  "export": {
    "enabled": true
  },
  "dataProvider": chartData
} );

var chart2 = AmCharts.makeChart("chartdiv2", {
  "theme": "none",
  "type": "gauge",
  "axes": [{
    "topTextFontSize": 10,
    "topTextYOffset": 70,
    "axisColor": "#FE4736",
    "axisThickness": 0,
    "endValue": 100,
    "gridInside": true,
    "inside": false,
    "radius": "70%",
    "valueInterval": 10,
    "tickColor": "#c9c9c9",
    "color": "#c9c9c9",
    "startAngle": -90,
    "endAngle": 90,
    "unit": "%",
    "bandOutlineAlpha": 0,
    "bands": [{
      "color": "#888888",
      "endValue": 100,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [-0.2, 0, 0.2],
      "startValue": 0
    }, {
      "color": "#FE4736",
      "endValue": 0,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [0.3, 0, -0.3],
      "startValue": 0
    }]
  }],
  "arrows": [{
    "borderAlpha": 0.43,
    "color": "#967F7F",
    "innerRadius": "10%",
    "alpha": 0.5,
    "nailRadius": 0,
    "radius": "170%"
  }]
});

var chart3 = AmCharts.makeChart("chartdiv3", {
  "theme": "none",
  "type": "gauge",
  "axes": [{
    "topTextFontSize": 10,
    "topTextYOffset": 70,
    "axisColor": "#FE4736",
    "axisThickness": 0,
    "endValue": 100,
    "gridInside": true,
    "inside": false,
    "radius": "70%",
    "valueInterval": 10,
    "tickColor": "#c9c9c9",
    "color": "#c9c9c9",
    "startAngle": -90,
    "endAngle": 90,
    "unit": "%",
    "bandOutlineAlpha": 0,
    "bands": [{
      "color": "#888888",
      "endValue": 100,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [-0.2, 0, 0.2],
      "startValue": 0
    }, {
      "color": "#FE4736",
      "endValue": 0,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [0.3, 0, -0.3],
      "startValue": 0
    }]
  }],
  "arrows": [{
    "borderAlpha": 0.43,
    "color": "#967F7F",
    "innerRadius": "10%",
    "alpha": 0.5,
    "nailRadius": 0,
    "radius": "170%"
  }]
});
var chart4 = AmCharts.makeChart("chartdiv4", {
  "theme": "none",
  "type": "gauge",
  "axes": [{
    "topTextFontSize": 10,
    "topTextYOffset": 70,
    "axisColor": "#FE4736",
    "axisThickness": 0,
    "endValue": 100,
    "gridInside": true,
    "inside": false,
    "radius": "70%",
    "valueInterval": 10,
    "tickColor": "#c9c9c9",
    "color": "#c9c9c9",
    "startAngle": -90,
    "endAngle": 90,
    "unit": "%",
    "bandOutlineAlpha": 0,
    "bands": [{
      "color": "#888888",
      "endValue": 100,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [-0.2, 0, 0.2],
      "startValue": 0
    }, {
      "color": "#ad9b14",
      "endValue": 0,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [0.3, 0, -0.3],
      "startValue": 0
    }]
  }],
  "arrows": [{
    "borderAlpha": 0.43,
    "color": "#edd41a",
    "innerRadius": "10%",
    "alpha": 0.5,
    "nailRadius": 0,
    "radius": "170%"
  }]
});
var chart5 = AmCharts.makeChart("chartdiv5", {
  "theme": "none",
  "type": "gauge",
  "axes": [{
    "topTextFontSize": 10,
    "topTextYOffset": 70,
    "axisColor": "#FE4736",
    "axisThickness": 0,
    "endValue": 100,
    "gridInside": true,
    "inside": false,
    "radius": "70%",
    "valueInterval": 10,
    "tickColor": "#c9c9c9",
    "color": "#c9c9c9",
    "startAngle": -90,
    "endAngle": 90,
    "unit": "%",
    "bandOutlineAlpha": 0,
    "bands": [{
      "color": "#888888",
      "endValue": 100,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [-0.2, 0, 0.2],
      "startValue": 0
    }, {
      "color": "#ea1919",
      "endValue": 0,
      "innerRadius": "105%",
      "radius": "170%",
      "gradientRatio": [0.3, 0, -0.3],
      "startValue": 0
    }]
  }],
  "arrows": [{
    "borderAlpha": 0.43,
    "color": "#967F7F",
    "innerRadius": "10%",
    "alpha": 0.5,
    "nailRadius": 0,
    "radius": "170%"
  }]
});
var chart6 = AmCharts.makeChart("chartdiv6", {
    "type": "serial",
    "theme": "light",
    "marginRight": 70,
    "dataProvider": chartData2,
    "valueAxes": [{
      "axisAlpha": 0,
      "position": "left",
      "title": "Ventas por marca"
    }],
    "startDuration": 2,
    "graphs": [{
      "balloonText": "<b>[[category]]: [[value]]</b>",
      "fillColorsField": "color",
      "fillAlphas": 0.8,
      "lineAlpha": 0.2,
      "type": "column",
      "topRadius":1,
      "valueField": "value"
    }],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "brand",
    "categoryAxis": {
      "gridPosition": "start",
      "labelRotation": 45
    },
     "depth3D": 40,
          "angle": 30,
    "export": {
      "enabled": true
    }

});

var chart7 = AmCharts.makeChart( "chartdiv7", {
  "type": "pie",
  "theme": "light",
  "dataProvider": chartData3,
  "valueField": "value",
  "titleField": "product",
  "outlineAlpha": 0.4,
  "depth3D": 15,
  "balloonText": "[[product]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 30,
  "labelText": "[[percents]]%",
  "percentPrecision": 1,
  "export": {
    "enabled": true
  }
} );

setTimeout(randomValue, 2000);

// set random value
function randomValue() {
 var value = <?php echo $pastCollected?>;
  chart2.arrows[0].setValue(value);
  chart2.axes[0].setTopText(value + " % <?php echo number_format($pastCollectPaid[0]['total'])?> de <?php echo number_format($pastCollect[0]['total'])?>");
  // adjust darker band to new value
  chart2.axes[0].bands[1].setEndValue(value);
  
  var value = <?php echo $monthCollected?>;
  chart3.arrows[0].setValue(value);
  chart3.axes[0].setTopText(value + " % <?php echo number_format($monthCollectPaid[0]['total'])?> de <?php echo number_format($monthCollect[0]['total'])?>");
  // adjust darker band to new value
  chart3.axes[0].bands[1].setEndValue(value);
  var value = <?php echo $past30Collected?>;
  
  chart4.arrows[0].setValue(value);
  chart4.axes[0].setTopText(value + " % <?php echo number_format($past30CollectPaid[0]['total'])?> de <?php echo number_format($past30Collect[0]['total'])?>");
  // adjust darker band to new value
  chart4.axes[0].bands[1].setEndValue(value);
  
  var value = <?php echo $past90Collected?>;
  chart5.arrows[0].setValue(value);
  chart5.axes[0].setTopText(value + " % <?php echo number_format($past90CollectPaid[0]['total'])?> de <?php echo number_format($past90Collect[0]['total'])?>");
  // adjust darker band to new value
  chart5.axes[0].bands[1].setEndValue(value);
}

</script>