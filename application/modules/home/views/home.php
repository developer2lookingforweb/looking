<script src="<?php echo base_url()?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/gauge.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/themes/dark.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/animate/animate.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/responsive/responsive.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>css/amcharts.export.css" type="text/css" media="all"/>

<style>
#chartdiv, #chartdiv2, #chartdiv3, #chartdiv4, #chartdiv5 , #chartdiv6, #chartdiv7 {
  width: 100%;
  height: 300px;
}	    
.dashboard-nav-card {
  background: #FE4736;
  border-radius: 0;
  color: #fefefe;
  display: block;
  min-height: 100px;
  padding: 2rem;
  position: relative;
  width: 100%;
}

.dashboard-nav-card:hover .dashboard-nav-card-title,
.dashboard-nav-card:hover .value,
.dashboard-nav-card:hover .dashboard-nav-card-icon {
  color: #fefefe;
}

.dashboard-nav-card:hover .dashboard-nav-card-title,
.dashboard-nav-card:hover .value,
.dashboard-nav-card:hover .dashboard-nav-card-icon {
  opacity: 1;
  transition: all 0.2s ease;
}

.dashboard-nav-card-icon {
  font-size: 1.25rem;
  left: 1rem;
  opacity: 0.5;
  position: absolute;
  top: 1rem;
  transition: all 0.2s ease;
  width: auto;
}

.dashboard-nav-card-title {
  bottom: 0;
  position: absolute;
  right: 1rem;
  opacity: 0.5;
  text-align: right;
  transition: all 0.2s ease;
  color: white;
  font-size: 1.2rem;
}
.dashboard-nav-card .value {
	float: right;
	margin-top: -19px;
        opacity: 0.5;
	font-size: 1.2rem;
	margin-right: -16px;
}


</style>
<div class="container">
<h2>Resumen </h2>
    <div class="columns large-3">
        <a class="dashboard-nav-card" href="<?php echo base_url("home/sales")?>">
          <div class="value"><?php echo $sales[0]['total']?></div><i class="dashboard-nav-card-icon fa fa-thumbs-up" aria-hidden="true"></i>
          <h3 class="dashboard-nav-card-title">Ventas Mes</h3>
        </a>
    </div>
    <div class="columns large-3">
        <a class="dashboard-nav-card" href="#">
            <div class="value">$ <?php echo number_format($earnings)?></div><i class="dashboard-nav-card-icon fa fa-money" aria-hidden="true"></i>
          <h3 class="dashboard-nav-card-title">Entradas Mes</h3>
        </a>
    </div>
    <div class="columns large-3">
        <a class="dashboard-nav-card" href="<?php echo base_url("wallet")?>">
          <div class="value">$ <?php echo number_format($notecase[0]['total'])?></div><i class="dashboard-nav-card-icon fa fa-ticket" aria-hidden="true"></i>
          <h3 class="dashboard-nav-card-title">Cartera</h3>
        </a>
    </div>
    <div class="columns large-3">
        <a class="dashboard-nav-card" href="#">
          <div class="value">$ <?php echo number_format($due[0]['total'])?></div><i class="dashboard-nav-card-icon fa fa-warning" aria-hidden="true"></i>
          <h3 class="dashboard-nav-card-title">Mora</h3>
        </a>
    </div>
</div>
<hr>
<div class="container">
<h2>Cartera</h2>
    <div class="row">
        <div class="columns large-6">
            <h5 class="center">Recolección mensual de cuotas</h5>
            <div id="chartdiv3"></div>
        </div>
        <div class="columns large-6">
            <h5 class="center">Recolección mensual de cuotas</h5>
            <table class="large-12">
                <thead>
                  <tr>
                    <th width="25%">Semana</th>
                    <th width="50%">Valor</th>
                    <th width="25%">Cuotas</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach ($monthCollectDetail as $key => $detailRow):?>
                    <tr>
                        <td><?php echo $detailRow['odate']?></td>
                        <td><?php echo "$ ". number_format($detailRow['valortotal'])?></td>
                        <td><?php echo $detailRow['total']?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
              </table>
        </div>
    </div>
    <div class="row">
        <div class="columns large-4">
            <h5 class="center">Recolección de cuotas Totales</h5>
            <div id="chartdiv2"></div>
        </div>
        <div class="columns large-4">
            <h5 class="center">Cuotas con vencimiento > a 30 días</h5>
            <div id="chartdiv4"></div>
        </div>
        <div class="columns large-4">
            <h5 class="center">Cuotas con vencimiento > a 90 días</h5>
            <div id="chartdiv5"></div>
        </div>
    </div>
</div>
<hr>
<div class="container">
<h2>Ventas</h2>
    <div class="row">
        <div class="columns large-12">
            <h5 class="center">Comportamiento de Ventas Mensuales</h5>
            <div id="chartdiv"></div>
        </div>
        <div class="columns large-12">
            <h5 class="center">Ventas por Marca</h5>
            <div id="chartdiv6"></div>
        </div>
    </div>
    <div class="row">
            <h5 class="center">Top Ventas 180 Dias</h5>
        <div class="columns large-6">
            <table class="dashboard-table">
                <colgroup>
                  <col width="40%">
                  <col width="30%">
                  <col width="10%">
                  <col width="20%">
                </colgroup>
                <thead>
                  <tr>
                    <th><a href="#">Equipo </a></th>
                    <th><a href="#">Marca </a></th>
                    <th><a href="#">No Ventas </a></th>
                    <th><a href="#">Ventas en $ </a></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <?php foreach ($topSales as $key => $sale):?>
                <?php 
                    $image = explode("|", $sale['image']);
                    if($key == 4)break;
                ?>
                  <tr>
                    <td>
                        <h6 class="dashboard-table-text"><?php echo $sale["product"]?></h6>
<!--                      <div class="flex-container align-justify align-top">
                        <div class="flex-child-shrink">
                            <img class="dashboard-table-image" width="25%" src="<?php echo base_url("images/products/".$image[0]) ?>">
                        </div>
                        <div class="flex-child-grow">
                          <h6 class="dashboard-table-text"><?php echo $sale["product"]?></h6>
                        </div>
                      </div>-->
                    </td>
                    <td><?php echo $sale["brand"]?></td>
                    <td class="bold"><?php echo $sale["total"]?></td>
                    <td>$<?php echo number_format($sale["tvalor"])?></td>
                  </tr>
                <?php endforeach?>

                </tbody>
              </table>

        </div>
        <div class="columns large-6">
            <div id="chartdiv7"></div>
        </div>
    </div>
</div>
<hr>
<div class="container">
</div>
