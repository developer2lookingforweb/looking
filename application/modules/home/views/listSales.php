<script src="<?php echo base_url()?>js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/gauge.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/themes/dark.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/animate/animate.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/export/export.min.js"></script>
<script src="<?php echo base_url()?>js/amcharts/plugins/responsive/responsive.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>css/amcharts.export.css" type="text/css" media="all"/>

<style>
#chartdiv1, #chartdiv2, #chartdiv3, #chartdiv4, #chartdiv5 , #chartdiv6, #chartdiv7 {
  width: 100%;
  height: 300px;
}	
</style>
<div class="row">
    <div class="columns large-4">
        <h5 class="center">Ventas x Producto</h5>
        <div id="chartdiv1"></div>
    </div>
    <div class="columns large-4">
        <h5 class="center">Ventas x Canal</h5>
        <div id="chartdiv2"></div>
    </div>
</div>
<table id="list" class="display">
    <thead>
        <tr>
            <th><?php echo lang('id') ?></th>
            <th><?=lang('category')?></th>
			<th><?=lang('order')?></th>
			<th><?=lang('status')?></th>
            <th><?php echo lang('actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
        </tr>
    </tbody>
</table>
<?php echo form_open("") . form_close(); ?>