<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller 
{
    public function setListParameters(){}
    
	public function index()
	{
		$data = array();
		$data["title"] = "Home";
		$data["sales"] = $this->getSales();
		$data["earnings"] = $this->getEarnings();
		$data["notecase"] = $this->getNotecase();
		$data["due"] = $this->getDue();
		$data["monthCollected"] = $this->getMonthCollectOk();
		$data["monthCollect"] = $this->getMonthCollect();
		$data["monthCollectDetail"] = $this->getMonthCollectDetailed();
		$data["monthCollectPaid"] = $this->getMonthCollect(true);
		$data["pastCollected"] = $this->getPastCollectOk();
		$data["pastCollect"] = $this->getPastCollect();
		$data["pastCollectPaid"] = $this->getPastCollect(true);
		$data["past30Collected"] = $this->getPastCollectOk(true);
		$data["past30Collect"] = $this->getPastCollect(false,true);
		$data["past30CollectPaid"] = $this->getPastCollect(true,true);
		$data["past90Collected"] = $this->getPastCollectOk(false,true);
		$data["past90Collect"] = $this->getPastCollect(false,false,true);
		$data["past90CollectPaid"] = $this->getPastCollect(true,false,true);
		$data["topSales"] = $this->getTopSales();
		$data["topSalesBrand"] = $this->getTopSalesBrand();
		$data["dueChart"] = $this->getDueChart();
//                var_dump($data["topSalesBrand"]);exit;
		$this->view('home', $data);
	}

        public function sales(){
            $actions = array();

            $data = array();
            $data["title"] = lang("Orders");
            $data["chartSalesProduct"] = $this->getMonthSalesByProduct();
            $data["chartSalesChannel"] = $this->getMonthSalesByChannel();
//                echo  "<pre>"; var_dump($data["chartSalesChannel"]);exit;
            $this->view('listSales', $data, $actions);
        }
        
	public function getListSales(){
        }
        
	public function getMonthSalesByProduct(){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
//            $emConf = $this->em->getConfiguration();
//            $emConf->addCustomDatetimeFunction('WEEK','DoctrineExtensions\Query\Mysql\Week'); 
            $this->loadRepository("ProductsOrders");
            $query = $this->ProductsOrders->createQueryBuilder('po')
            ->select( "p.image,p.product, b.brand, count(o.id)  as total, sum(o.value) as tvalor" )
            ->join("po.order","o")
            ->join("po.product","p")
            ->join("o.status","s")
            ->join("p.brand","b")
            ->Where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
            ->andwhere("s.id = ".AuthConstants::IN_APPROVED)
            ->andwhere("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
            ->groupBy("p.product")
            ->orderBy("total","DESC")
            ->getQuery();
                
             return $count = $query->getResult();
        }
	public function getMonthSalesByChannel(){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
//            $emConf = $this->em->getConfiguration();
//            $emConf->addCustomDatetimeFunction('WEEK','DoctrineExtensions\Query\Mysql\Week'); 
            $this->loadRepository("ProductsOrders");
            $query = $this->ProductsOrders->createQueryBuilder('po')
            ->select( "pm.method, count(o.id)  as total, sum(o.value) as tvalor" )
            ->join("po.order","o")
            ->join("po.product","p")
            ->join("o.payment_method","pm")
            ->join("o.status","s")            
            ->Where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
            ->andwhere("s.id = ".AuthConstants::IN_APPROVED)
            ->andwhere("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
            ->groupBy("pm.method")
            
            ->getQuery();
                
             return $count = $query->getResult();
        }
        
	public function getSales(){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
            $this->loadRepository("Orders");
            $query = $this->Orders->createQueryBuilder('o')
                ->select( "count(o.id) total " )
                ->where("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->andWhere("o.status = ".AuthConstants::IN_APPROVED)
                ->getQuery();
             $count = $query->getResult();
             return ($count);
	}

        public function getPastCollectOk($tirthy = false, $ninety = false){
            $totalOk = $this->getPastCollect(TRUE,  $tirthy,$ninety);
            $total = $this->getPastCollect(false,  $tirthy,$ninety);
//            var_dump($total);
//            var_dump($totalOk);exit;
            return (int)$percent = ceil($totalOk[0]['total'] * 100)/$total[0]['total'];
	}

        public function getPastCollect($paid = false,$tirthy = false, $ninety = false){
            $dateStart = new DateTime();
            $dateStart->format('Y-m-d');
            if($tirthy){
                $dateStart->modify('-30 days');
            }
            if($ninety){
                $dateStart->modify('-90 days');
            }
            $this->loadRepository("OrdersFees");
            $dateLimit = "";
            if($tirthy){
                $limitDate = new DateTime("- 90 days");
                $dateLimit = " AND o.payment_date > '".$limitDate->format('Y-m-d')."'";
            }
            if($paid){
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.id) total " )
                ->Where("o.status = ".AuthConstants::IN_APPROVED)
                ->andwhere("o.payment_date < '".$dateStart->format('Y-m-d')."'".$dateLimit)
                ->getQuery();
                
            }  else {
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.id) total " )
//                ->Where("o.status <> ".AuthConstants::IN_APPROVED)
//                ->orWhere("o.status is null")
                ->Where("o.payment_date < '".$dateStart->format('Y-m-d')."'".$dateLimit)
                ->getQuery();
                
            }
             $count = $query->getResult();
             return ($count);
	}

        public function getMonthCollectOk(){
            $totalOk = $this->getMonthCollect(TRUE);
            $total = $this->getMonthCollect();
//            var_dump($total);
//            var_dump($totalOk);exit;
            return (int)$percent = ceil($totalOk[0]['total'] * 100)/$total[0]['total'];
	}

        public function getMonthCollect($paid = false){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
            $this->loadRepository("OrdersFees");
            if($paid){
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.id) total " )
                ->Where("o.status = ".AuthConstants::IN_APPROVED)
                ->andwhere("o.payment_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->getQuery();
                
            }  else {
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.id) total " )
                // ->Where("o.status <> ".AuthConstants::IN_APPROVED)
                // ->orWhere("o.status is null")
                ->Where("o.payment_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->getQuery();
                
            }
             $count = $query->getResult();
             return ($count);
	}
        public function getMonthCollectDetailed($paid = false){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
            $emConf = $this->em->getConfiguration();
            $emConf->addCustomDatetimeFunction('WEEK','DoctrineExtensions\Query\Mysql\Week'); 
            $this->loadRepository("OrdersFees");
            if($paid){
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.value) total,sum(o.value) valortotal, WEEK(o.payment_date) odate" )
                ->Where("o.status = ".AuthConstants::IN_APPROVED)
                ->andwhere("o.payment_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->groupBy("odate")
                ->getQuery();
                
            }  else {
                $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "count(o.value) total,sum(o.value) valortotal, WEEK(o.payment_date, '%Y-%m') odate" )
                // ->Where("o.status <> ".AuthConstants::IN_APPROVED)
                // ->orWhere("o.status is null")
                ->andwhere("o.payment_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->groupBy("odate")
                ->getQuery();
                
            }
             $count = $query->getResult();
             return ($count);
	}
        public function getTopSales(){
            $dateStart = new DateTime('-180 days');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
//            $emConf = $this->em->getConfiguration();
//            $emConf->addCustomDatetimeFunction('WEEK','DoctrineExtensions\Query\Mysql\Week'); 
            $this->loadRepository("ProductsOrders");
            $query = $this->ProductsOrders->createQueryBuilder('po')
            ->select( "p.image,p.product, b.brand, count(o.id)  as total, sum(o.value) as tvalor" )
            ->join("po.order","o")
            ->join("po.product","p")
            ->join("p.brand","b")
            ->Where("o.status = ".AuthConstants::IN_APPROVED)
            ->Where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
            ->andwhere("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
            ->groupBy("p.product")
            ->orderBy("total","DESC")
            ->getQuery()->setMaxResults(10);
                
             $count = $query->getResult();
             return ($count);
	}
        public function getTopSalesBrand(){
            $dateStart = new DateTime('-180 days');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
//            $emConf = $this->em->getConfiguration();
//            $emConf->addCustomDatetimeFunction('WEEK','DoctrineExtensions\Query\Mysql\Week'); 
            $this->loadRepository("ProductsOrders");
            $query = $this->ProductsOrders->createQueryBuilder('po')
            ->select( "b.brand, count(o.id) as conteo" )
            ->join("po.order","o")
            ->join("po.product","p")
            ->join("p.brand","b")
            ->Where("o.status = ".AuthConstants::IN_APPROVED)
            ->Where("p.id <> ".AuthConstants::FN_FINANCIAL_ID)
            ->andwhere("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
            ->groupBy("p.brand")
            ->orderBy("conteo","DESC")
            ->getQuery();
                
             $count = $query->getResult();
             return ($count);
	}
        public function getEarnings(){
            $dateStart = new DateTime('first day of this month');
            $dateStart->format('Y-m-d');
            $dateEnd = new DateTime('last day of this month');
            $dateEnd->format('Y-m-d');
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "sum(o.paid) total " )
                ->where("o.transaction_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->andWhere("o.status = ".AuthConstants::IN_APPROVED)
                ->getQuery();
             $count = $query->getResult();
            $this->loadRepository("Orders");
            $query = $this->Orders->createQueryBuilder('o')
                ->select( "sum(o.value) total " )
                ->where("o.order_date BETWEEN '".$dateStart->format('Y-m-d')."' and '".$dateEnd->format('Y-m-d')."'")
                ->andWhere("o.status = ".AuthConstants::IN_APPROVED)
                // ->andWhere("o.payment_method = ".AuthConstants::PM_USE_PAYU)
                ->getQuery();
             $count2 = $query->getResult();
             return ($count2[0]['total']);
	}
        public function getNotecase(){
            $dateStart = new DateTime();
            $dateStart->format('Y-m-d');
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "sum(o.value) total " )
                ->where("o.status <> ".AuthConstants::IN_APPROVED)
                ->orWhere("o.status is null")
//                ->andwhere("o.payment_date < '".$dateStart->format('Y-m-d')."'")
                ->getQuery();
             $count = $query->getResult();
             return ($count);
	}
        public function getDue(){
            $dateStart = new DateTime('last month');
            $dateStart->format('Y-m-d');
            $this->loadRepository("OrdersFees");
            $query = $this->OrdersFees->createQueryBuilder('o')
                ->select( "sum(o.value) total " )
                ->where("o.status <> ".AuthConstants::IN_APPROVED)
                ->orWhere("o.status is null")
                ->andwhere("o.payment_date < '".$dateStart->format('Y-m-d')."'")
                ->getQuery();
             $count = $query->getResult();
             return ($count);
	}
        public function getDueChart(){
            $dateStart = new DateTime();
            $dateStart->format('Y-m-d');
            $emConf = $this->em->getConfiguration();
            $emConf->addCustomDatetimeFunction('DATE_FORMAT','DoctrineExtensions\Query\Mysql\DateFormat'); 
            $this->loadRepository("Orders");
            $query = $this->Orders->createQueryBuilder('o')
                ->select( "sum(o.value) value, DATE_FORMAT(o.order_date,'%Y-%m') odate" )
               ->where("o.status = ".AuthConstants::IN_APPROVED)
//                ->orWhere("o.status is null")
                ->andwhere("o.order_date < '".$dateStart->format('Y-m-d')."'")
                ->groupBy("odate")
                ->getQuery();
             $count = $query->getResult();
//             echo "<PRE>"
//;        var_dump($count);
             return ($count);
	}

	public function forbbiden()
	{
		$data = array();
		$data["title"] = "Forbbiden";
		$data["menu"]   = "";
		
		$this->view('forbbiden', $data);
	}
}
