<style>
.parallax {
    /* The image used */
    background-image: url("<?php echo base_url("images/gallery/".$banner)?>");

    /* Set a specific height */
    min-height: 300px;

    /* Create the parallax scrolling effect */
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.section-heading{
    margin-top: 5%;
}
.article-content li {
	list-style-type: decimal!important;
    margin: 15px 0 !important;
}
.article-content p {	
    margin: 15px 0 !important;
}
</style>
<?php if($showBanner ==1){?>
<section class="parallax">
</section>
<?php }?>
<section>
    <div class="container-fluid">
        <div class="container ">
            <div class="row article-content">
                <div class="col-lg-10 col-lg-push-1">
                    <h1 class="section-heading"><?php echo $title?></h1>
                    <!--<p class="lead section-lead"><?php echo $alias?></p>-->
                    <p class="section-paragraph"><?php echo $content?></p>
                </div>
            </div>
        </div>
    </div>
</section>