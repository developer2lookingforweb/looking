<?php 
    $fields = array();
    $fields[lang('title')] = form_input(array('name'=>'title', 'class'=>'span3 focused', 'value'=>$title));
	$fields[lang('banner')] = form_input(array('name'=>'banner', 'class'=>'span3 focused', 'value'=>$banner));
	$fields[lang('showBanner')] = form_input(array('name'=>'showBanner', 'class'=>'span3 focused', 'value'=>$showBanner));
	$fields[lang('content')] = form_input(array('name'=>'content', 'class'=>'span3 focused', 'value'=>$content));
	$fields[lang('published')] = form_input(array('name'=>'published', 'class'=>'span3 focused', 'value'=>$published));
	$fields[lang('alias')] = form_input(array('name'=>'alias', 'class'=>'span3 focused', 'value'=>$alias));
	$fields[lang('createdBy')] = form_input(array('name'=>'createdBy', 'class'=>'span3 focused', 'value'=>$createdBy));
	$fields[lang('creationDate')] = form_input(array('name'=>'creationDate', 'class'=>'span3 focused', 'value'=>$creationDate));
    $hidden = array('id' => $id);
    $fields = array();
    $fields = array();
    $fields["col-1"]["config"]       = array("large"=>"12");
    $fields["col-2"]["config"]       = array("large"=>"6");
    $fields["col-3"]["config"]       = array("large"=>"6");
    $fields["col-4"]["config"]       = array("large"=>"12");
    $fields["col-2"][lang('title')] = form_input(array('name'=>'title', 'class'=>'span3 focused', 'value'=>$title));
    $fields["col-2"][lang('alias')] = form_input(array('name'=>'alias', 'class'=>'span3 focused', 'value'=>$alias));
    $fields["col-2"][lang('showBanner')] = form_input(array('name'=>'showBanner', 'class'=>'span3 focused', 'value'=>$showBanner));
    $fields["col-1"]["no-label"] = Soporte::creaTag("div", 
                                Soporte::creaTag("img", 
                                "", 
                                "id='profileThumb' src='".site_url(AuthConstants::GALLERY_PATH.$banner)."'"),
                            "class='center'");
    $fields["col-3"][lang('banner')] = my_form_upload(array('name'=>'banner','id'=>'banner', 'class'=>'span3 focused', 'value'=>$banner));
    $fields["col-3"][lang('published')] = form_input(array('name'=>'published', 'class'=>'large-6 focused', 'value'=>$published));
    $fields["col-4"]["no-label"] = form_textarea(array('name'=>'content', 'id'=>'tinymce', 'class'=>'span3 focused tinymce','rows'=>50),$content);
    if($id>0)$fields["col-3"][lang('createdBy')] = form_input(array('name'=>'createdBy', 'disabled'=>'disabled', 'class'=>'large-6 focused', 'value'=>$createdBy));
    if($id>0)$fields["col-2"][lang('creationDate')] = form_input(array('name'=>'creationDate', 'disabled'=>'disabled', 'class'=>'large-6 focused', 'value'=>$creationDate));
    $hidden = array('id' => $id);
    echo print_form('/articles/persist/', $fields, $hidden,"form",false,12);