<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends MY_Controller 
{
        public $sauth_noauth = array("display");
	public function index()
	{
		$actions = array();
		$actions["create_article"] = site_url("articles/form");
		
		$data = array();
		$data["title"] = lang("Articles");
		$this->view('list', $data, $actions);
	}
	
	public function display($identifier=0)
	{
            $actions = array();
            $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
            $title = $this->input->post('title');
            $banner = $this->input->post('banner');
            $showBanner = $this->input->post('showBanner');
            $content = $this->input->post('content');
            $published = $this->input->post('published');
            $alias = $this->input->post('alias');
            $createdBy = $this->input->post('createdBy');
            $creationDate = $this->input->post('creationDate');

            if ($identifier > 0) {
                $output = $this->rest->get('articles/article/', array("id" => $identifier));

                if ($output->status) {
                    $article = $output->data;
                    $title = $article->title;
                    $banner = $article->banner;
                    $showBanner = $article->show_banner;
                    $content = $article->content;
                    $published = $article->published;
                    $alias = $article->alias;
                    $createdBy = $article->created_by;
                    $creationDate = $article->creation_date;
                    $id = $article->id;
                }
            }
            $actions = array();
            $actions["return_article"] = site_url("articles/index");

            $data = array();
            $data["title"] = lang("Articles");
            $data['title'] = $title;
            $data['banner'] = $banner;
            $data['showBanner'] = $showBanner;
            $data['content'] = $content;
            $data['published'] = $published;
            $data['alias'] = $alias;
            $data['createdBy'] = $createdBy;
            $data['creationDate'] = $creationDate;
            $data["id"] = $id;
            $this->viewFrontend2('render', $data, $actions);
        }

        public function setListParameters()
	{
            $this->load->library('../modules/looking/controllers/looking');
            $this->session->set_userdata(AuthConstants::CATEGORIES,        $this->looking->getCategories(1));
        $this->load->helper('action');

        $model = new Model("Articles", "a", array("id" => "id", 'title' => 'title' ,'banner' => 'banner' ,'show_banner' => 'showBanner'  ,'published' => 'published' ,'alias' => 'alias' ,'created_by' => 'createdBy'));
        $model->setNumerics(array("a.id"));

        $actions = array();
        array_push($actions, new Action("articles", "form", "edit"));        
        array_push($actions, new Action("articles", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $title = $this->input->post('title');
		$banner = $this->input->post('banner');
		$showBanner = $this->input->post('showBanner');
		$content = $this->input->post('content');
		$published = $this->input->post('published');
		$alias = $this->input->post('alias');
		$createdBy = $this->input->post('createdBy');
		$creationDate = $this->input->post('creationDate');
        
        if ($identifier > 0){
            $output = $this->rest->get('articles/article/', array("id"=>$identifier));
        
            if ($output->status){
                $article    = $output->data;
                $title = $article->title;
                $banner = $article->banner;
                $showBanner = $article->show_banner;
                $content = $article->content;
                $published = $article->published;
                $alias = $article->alias;
                $createdBy = $article->created_by;
                $creationDate = new DateTime($article->creation_date->date);
                $creationDate = $creationDate->format("Y-m-d");
                $id         = $article->id;
            }
        }
        
        $actions = array();
        $actions["return_article"] = site_url("articles/index");
        
        $data = array();
        $data["title"]  = lang("Articles");
        $data['title'] = $title;
		$data['banner'] = $banner;
		$data['showBanner'] = $showBanner;
		$data['content'] = $content;
		$data['published'] = $published;
		$data['alias'] = $alias;
		$data['createdBy'] = $createdBy;
		$data['creationDate'] = $creationDate;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('title', 'lang:title', 'required');
//        $this->form_validation->set_rules('banner', 'lang:banner', 'required');
        $this->form_validation->set_rules('showBanner', 'lang:showBanner', 'required');
        $this->form_validation->set_rules('content', 'lang:content', 'required');
        $this->form_validation->set_rules('published', 'lang:published', 'required');
        $this->form_validation->set_rules('alias', 'lang:alias', 'required');
//        $this->form_validation->set_rules('createdBy', 'lang:createdBy', 'required');
//        $this->form_validation->set_rules('creationDate', 'lang:creationDate', 'required');

        if ($this->form_validation->run($this)){
            $config['upload_path'] = "./".AuthConstants::GALLERY_PATH;
            $config['allowed_types'] = AuthConstants::IMAGES_EXTENSIONS;
            $config['max_size']	= AuthConstants::DOCUMENT_SIZE;
            $posfixMsg="";
            $this->load->library('upload', $config);
            $arrayPost = $this->input->post();
            if($this->upload->do_upload('banner')){
                $uploadData = $this->upload->data();
                $arrayPost["banner"] = $uploadData["file_name"];
            }
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('articles/article', $arrayPost); 
            }else{
                $output = $this->rest->put('articles/article', $arrayPost); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('article_edition') : lang('article_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('articles/article', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("article_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}