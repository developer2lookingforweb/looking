    <!-- BEGIN CONTENT-->
        <div id="body" class="row">
            <br />
            <br />
            <div class="small-1 large-1 columns " >
                &nbsp;
            </div>
            <div class="small-10 large-6 columns login" >
                
                <div class="row">
                    <div class="large-12 columns">
                        <h4>Registro de empresas</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="large-12 columns">
                        <div class="row">
                            <div class="large-12 columns" id="form-login">
                                <?php echo form_open(lang('login').'/'.lang('signin_company'), 'id="login"');?>
                                    <?=  form_hidden("action",$action);?>
                                    <div class="columns large-12">
                                        <span class="span3">Empresa</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'company', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("company")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Contacto</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'contact', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("contact")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Email contacto</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'email', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("email")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Contraseña</span>
                                        <div class="span9">
                                            <?=form_password(array('name'=>'password', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("password")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Repite tu contraseña</span>
                                        <div class="span9">
                                            <?=form_password(array('name'=>'password2', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("password")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Nit</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'nit', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("nit")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-6">
                                        <span class="span3">Teléfono</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'phone', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("phone")));?>
                                        </div>
                                    </div>
                                    <div class="columns large-12">
                                        <span class="span3">Dirección</span>
                                        <div class="span9">
                                            <?=form_input(array('name'=>'address', 'class'=>'span12', 'value'=>'', 'placeholder'=>lang("address")));?>
                                        </div>
                                    </div>
                                    <div class="row-fluid">                                        
                                        <div class="right">
                                            <?=form_button(array('type'=>'submit', 'class'=>'btn-primary span12', 'content'=>lang('register')));?>
                                        </div>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        

                        <div class="row-fluid" id="error-login">
                            <div class="span12"><?=$error?></div>
                        </div>
                        
                    </div>
                    
                </div>
                <br />
                <br />
            </div>
            <div class="small-1 large-1 columns " >
                &nbsp;
            </div>
            <div class="small-10 large-4 columns login" >
                
                
            </div>
            <div class=" large-1 columns " >
                &nbsp;
            </div>
            <div class="span1 hidden-phone"> </div>
        </div>
        <!-- END CONTENT-->

