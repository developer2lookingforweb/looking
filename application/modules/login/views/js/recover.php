<script type="text/javascript">

    $(document).ready(function () {
        <?php if(isset($error)){?>
                $("#dg_error #error-title").html("Datos incorrectos");
                $("#dg_error #error-msg").html("Verifica tu datos de ingreso e intenta nuevamente<br/>");
                $('#dg_error').modal('toggle'); 
        <?php }?>
        $('#numero').focusout(function () {
            validateCell(this.value);
        });
        $('.send-refill').click(function (e) {
            e.preventDefault();

            if (validateEmail($("#email3").val())) {
//            if(validateCell($("#numero").val()) && validateEmail($("#email3").val())){
                $(this).unbind('click').click();
            }
        });
        $('#email').focusout(function () {
            validateEmail($("#email"));
        });
        $('.required').focusout(function () {
            required($(this));
        });
        $('.send').click(function (e) {
            e.preventDefault();

            if (required($("#password"))) {
                $(this).unbind('click').click();
            }
        });
        $('#submit').click(function (e) {
            e.preventDefault();

            if ( required($("#password")) && validateEmail($("#email"))) {
                $(this).unbind('click').click();
            }else{
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Verifica tu datos de ingreso para continuar<br/>");
                $('#dg_error').modal('toggle'); 
                
            }
        });
        $('.tit_loginolvido2').click(function (e) {
            e.preventDefault();

                $("#dg_recover #error-title").html("Recuperar contraseña");
//                $("#dg_recover #error-msg").html("Verifica tu datos de ingreso para continuar<br/>");
                $('#dg_recover').modal('toggle'); 
        });
        $('#submit-recover').click(function (e) {
            e.preventDefault();
            
            if ( validateEmail($("#recover-email"))) {
                $.ajax({
                    url: "<?php echo base_url(); ?>login/recover/",
                    global: false,
                    type: "post",
                    data: {email: $("#recover-email").val()},
                    dataType: "json",
                    success: function (data) {
                        if(data.status == true){
                            $("#recover-email").val("")
                            $('#dg_recover').modal('toggle'); 
                            $("#dg_error #error-title").html("Recuperar contraseña");
                            $("#dg_error #error-msg").html("Consulta tu correo y sigue las instrucciones<br/>");
                            $('#dg_error').modal('toggle'); 
                        }else{
                            $("#dg_error #error-title").html("Recuperar contraseña");
                            $("#dg_error #error-msg").html("El correo ingresado no fue encontrado verifica los datos<br/>");
                            $('#dg_error').modal('toggle'); 
                        }
                    }
                });
            }else{
                $("#dg_error #error-title").html("Datos inválidos");
                $("#dg_error #error-msg").html("Verifica tu datos de ingreso para continuar<br/>");
                $('#dg_error').modal('toggle'); 
                
            }
        });


        function validateTermns(input) {
            if (!input.is(':checked')) {
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("Debes aceptar los términos y condiciones.<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');

            }
            return input.is(':checked');
        }
        function validateCell(value) {
            if (!isCellphone(value)) {
                $("#modal-message #modal-title").html("Datos inválidos");
                $("#modal-message #message").html("No es un número celular válido.<br/><br/>");
                $('#modal-message').removeClass('large').addClass('tiny').css('top', '85px');
                $('#modal-message').foundation('reveal', 'open');
                return false;
            } else {
                return true;
            }
        }
        ;

        function validateEmail(obj) {
            if (!isEmail($(obj).val())) {
                obj.toggleClass("validation-error");
                return false;
            } else {
                $(obj).removeClass("validation-error");
                return true;
            }
        }
        ;
        
        function required(obj){
            if($(obj).hasClass("required") && ($(obj).val()=="" || $(obj).val()=="-Selecciona-")){
                $(obj).addClass("validation-error");
                return false;
            }else{
                $(obj).removeClass("validation-error");
                return true;
            }
        }
        
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        ;

        function isCellphone(number) {
            var regex = /^3[0-9]{9}$/;
            return regex.test(number);
        }
        ;
    });
</script>