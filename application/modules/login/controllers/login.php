<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MY_Controller
{
    public $public = true;
    
    public function setListParameters(){}
    
    public function index ()
    {   
        $this->loadRepository("Users");
        $action     = "";
        $check      = false;
        $email      = "";
        $password   = "";
        $cookie     = $this->config->item("sess_cookie_name");
        
        if (isset($_COOKIE[$cookie."_action"])) {
            $action=$_COOKIE[$cookie."_action"];
        }
        if (isset($_COOKIE[$cookie."_cookuser"]) && isset($_COOKIE[$cookie."_cookpass"])) {
            $email      = $_COOKIE[$cookie."_cookuser"];
            $password   = $_COOKIE[$cookie."_cookpass"];
            $check      = true;
            $user       = $this->Users->getUserByEmailPassword($email, $password);
            $exist      = (empty($user) == false);
            if ($exist == false) {
                $email      = "";
                $password   = "";
                $check      = false;
            }
        }
        
        $data = array();
        $data["title"]      = "LookingFor";
        $data["check"]      = $check;
        $data["email"]      = $email;
        $data["password"]   = $password;
        $data["action"]   = $action;
        $data["error"]      = $this->session->userdata(AuthConstants::ERROR_LOGIN);
        $this->viewLogin('login', $data);
    }
    
    public function signin_form ()
    {   
        $this->loadRepository("Users");
        $action     = "";
        $check      = false;
        $email      = "";
        $password   = "";
        $cookie     = $this->config->item("sess_cookie_name");
        
        if (isset($_COOKIE[$cookie."_action"])) {
            $action=$_COOKIE[$cookie."_action"];
        }
        if (isset($_COOKIE[$cookie."_cookuser"]) && isset($_COOKIE[$cookie."_cookpass"])) {
            $email      = $_COOKIE[$cookie."_cookuser"];
            $password   = $_COOKIE[$cookie."_cookpass"];
            $check      = true;
            $user       = $this->Users->getUserByEmailPassword($email, $password);
            $exist      = (empty($user) == false);
            if ($exist == false) {
                $email      = "";
                $password   = "";
                $check      = false;
            }
        }
        
        $data = array();
        $data["title"]      = "LookingFor";
        $data["check"]      = $check;
        $data["email"]      = $email;
        $data["password"]   = $password;
        $data["action"]   = $action;
        $data["error"]      = $this->session->userdata(AuthConstants::ERROR_LOGIN);
        $this->viewLogin('signin', $data);
    }
    
    public function auth ()
    {
        $this->session->set_userdata(AuthConstants::ERROR_LOGIN, "");
        
        $this->loadRepository("Users");
        
        $this->session->sess_destroy();
        $this->session->sess_create();
        
        $email      = $this->input->post("email");
        $password   = $this->input->post("password");
        $user       = $this->Users->getUserByEmailPassword($email, $password);
        $action     = $this->input->post("action");
        
        if (empty($user) == false) {
            $language = ($user->getLanguage() != "") ? $user->getLanguage() : $this->config->item('language');
            $this->session->set_userdata(AuthConstants::USER_ID,        $user->getId());
            $this->session->set_userdata(AuthConstants::EMAIL,          $user->getEmail());
            $this->session->set_userdata(AuthConstants::NAMES,          $user->getName());
            $this->session->set_userdata(AuthConstants::LAST_NAMES,     $user->getLastName());
            $this->session->set_userdata(AuthConstants::ADMIN,          $user->getAdmin());
            $this->session->set_userdata(AuthConstants::PROFILE,        $user->getProfile()->getId());
            $this->session->set_userdata(AuthConstants::PROFILE_NAME,   $user->getProfile()->getName());
            $this->session->set_userdata(AuthConstants::COUNTRY,        $user->getCity()->getState()->getCountry()->getId());
            $this->session->set_userdata(AuthConstants::LANG,           $language);
            $this->session->set_userdata(AuthConstants::MENU_SELECTED,  4);
            $theme = ($user->getTheme()!=="")?$user->getTheme():'orange';
            $this->session->set_userdata(AuthConstants::THEME,          $theme);
            $this->session->set_userdata(AuthConstants::USER_IMAGE,     $user->getProfileImage());
            $user->setLastAccess(new DateTime('now'));
            $this->em->persist($user);
            $this->em->flush();
            
            $remember = ($this->input->post('remember') == 1);
            $cookie   = $this->config->item("sess_cookie_name");

            if ($remember == false) {
                setcookie($cookie."_cookuser", "", -3600, "/");
                setcookie($cookie."_cookpass", "", -3600, "/");
            }

            if ($remember) {
                setcookie($cookie."_cookuser", $email, time() + 60 * 60 * 24 * 100, "/");
                setcookie($cookie."_cookpass", $password, time() + 60 * 60 * 24 * 100, "/");
            }
            setcookie($cookie."_lang", $language, time() + 60 * 60 * 24 * 100, "/");
            if($action!=""){
                redirect(str_replace("-", "/", $action));
            }  else {                
                redirect("/home/home/");
            }
            
            exit();
        }
        
        $this->session->set_userdata(AuthConstants::ERROR_LOGIN, lang("error_login"));
        redirect("/login/index/");
    }
    
    public function recover ()
    {
        $this->loadRepository("Customers");
        
        $email      = $this->input->post("email");
        $user       = $this->Customers->findOneBy(array("email" => $email));
        $status = false;
        if($user !== null && !empty($user)){
            $status = true;
            $params = array();
            $params['*|NAME|*'] = $user->getName();
            $params['*|URL|*'] = base_url("login/changePasswd")."/".$user->getId()."-".md5($user->getEmail());
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            $this->load->library('mailgunmailer');
            $fullName = $user->getName()." ".$user->getLastName();
            $to=array();
            $to[]= array('name'=>$fullName,'mail'=>$email);

            $this->mailgunmailer->notify($to,  AuthConstants::ML_RECOVERPASSWD,"recover",$params);
        }
        echo json_encode(array('status'=>$status));
    }
    public function changePasswd ($hash)
    {
        $userData = explode("-", $hash);
        $this->loadRepository("Customers");
        $user = $this->Customers->find($userData[0]);
        $data = array();
        $data["title"] = AuthConstants::ML_INFONAME;
        $data["id"] = 0;
        $data["email"] = 0;
        $status = false;
        if($user !== null && !empty($user)){
            if($userData[1]==  md5($user->getEmail())){
                $data["id"] = $user->getId();
                $data["email"] = $user->getEmail();
                $status = true;
            }
        }
        if($status){
            $this->viewFrontend('recover', $data);
        }  else {
            redirect(base_url("looking"));
        }
        
        
        
    }
    public function persistPasswd (){
        $this->loadRepository("Customers");
        $user = $this->Customers->find($this->input->post("id"));
        if($user !== null && !empty($user)){
            
            $user->setPassword(md5($this->input->post("password")));
            $this->em->persist($user);
            $this->em->flush();
            $this->authCustomer();
        }
    }
    
    public function watchdog ($service){
            $params = array();
            $params['*|SERVICE|*'] = $service;
            $params['*|MC_PREVIEW_TEXT|*'] = "";
            $params['*|CURRENT_YEAR|*'] = date("Y");
            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
            $this->load->library('mailgunmailer');
            $to=array();
            $to[]= array('name'=>  "Raúl Donado",'mail'=>  AuthConstants::ML_OWNEREMAIL);
            $to[]= array('name'=>  "John Jiménez",'mail'=>  AuthConstants::ML_SUPPORTEMAIL);

            $this->mailgunmailer->notify($to,"Alerta Watchdog","watchdogReport",$params);
    }
    
    public function authCustomer ($newRegister = false)
    {
        $this->session->set_userdata(AuthConstants::ERROR_LOGIN, "");
        
        $this->loadRepository("Customers");
        $this->loadRepository("Favorites");
        
        $this->session->sess_destroy();
        $this->session->sess_create();
        $email      = $this->input->post("email");
        $password   = $this->input->post("password");
        if(! $this->input->post("password_in_md5"))$password   = md5($password);
//        var_dump($email,$password);exit;
        $user       = $this->Customers->findOneBy(array("email" => $email,"password"=>$password));
        
        
        if (empty($user) == false) {
            $favorites  = $this->Favorites->findBy(array("customer" => $user->getId()));
            $arrayFav = array();
            foreach ($favorites as $fav) {
                $arrayFav[]= $fav->getProduct()->getId();
            }
            $action     = $this->input->post("action");
            $language = $this->config->item('language');
            $this->session->set_userdata(AuthConstants::USER_ID,        $user->getId());
            $this->session->set_userdata(AuthConstants::EMAIL,          $user->getEmail());
            $this->session->set_userdata(AuthConstants::NAMES,          $user->getName());
            $this->session->set_userdata(AuthConstants::LAST_NAMES,     $user->getLastName());
            $this->session->set_userdata(AuthConstants::FB_ID,          $user->getFacebookId());
            $this->session->set_userdata(AuthConstants::CART,     array());
//            $this->session->set_userdata(AuthConstants::ADMIN,          $user->getAdmin());
//            $this->session->set_userdata(AuthConstants::PROFILE,        $user->getProfile()->getId());
//            $this->session->set_userdata(AuthConstants::PROFILE_NAME,   $user->getProfile()->getName());
//            $this->session->set_userdata(AuthConstants::COUNTRY,        $user->getCity()->getState()->getCountry()->getId());
            $this->session->set_userdata(AuthConstants::LANG,           $language);
            $this->session->set_userdata(AuthConstants::MENU_SELECTED,  4);
            $this->session->set_userdata(AuthConstants::THEME,          "default");
            $this->session->set_userdata(AuthConstants::FAVORITES,      $arrayFav);
//            $this->session->set_userdata(AuthConstants::USER_IMAGE,     $user->getProfileImage());
            $user->setLastAccess(new DateTime('now'));
            $this->em->persist($user);
            $this->em->flush();
            
            $remember = ($this->input->post('remember') == 1);
            $cookie   = $this->config->item("sess_cookie_name");

            if ($remember == false) {
                setcookie($cookie."_cookuser", "", -3600, "/");
                setcookie($cookie."_cookpass", "", -3600, "/");
            }

            if ($remember) {
                setcookie($cookie."_cookuser", $email, time() + 60 * 60 * 24 * 100, "/");
                setcookie($cookie."_cookpass", $password, time() + 60 * 60 * 24 * 100, "/");
            }
            setcookie($cookie."_lang", $language, time() + 60 * 60 * 24 * 100, "/");
            
            if (!$this->input->is_ajax_request()) {
                if($newRegister){
                    redirect("/looking/bienvenido/");
                }  else {
                    redirect("/outstanding/");
                }
            }  else {
                echo json_encode(array('status'=>true,'user'=>array('name'=>$user->getName())));
            }
        }  else {
            redirect("/looking/index/?loginError=1");
//            echo json_encode(array('status'=>false,'user'=>array()));
        }
        
        $this->session->set_userdata(AuthConstants::ERROR_LOGIN, lang("error_login"));
//        redirect("/login/index/");
    }
    
    public function signin10 (){
        $this->signin(10);
    }
    public function signin ($cupon = null){
        $this->loadRepository("Customers");
        
        $campaingReference =($this->session->userdata(AuthConstants::REF_CAMPAIGN))?$this->session->userdata(AuthConstants::REF_CAMPAIGN):0;
        $this->session->sess_destroy();
        $this->session->sess_create();
        
        $email          = $this->input->post("email");
        $name           = $this->input->post("name");
        $lastName       = $this->input->post("last_name");
        $mailMarketing  = ($this->input->post("mailMarketing")=="on");
        $address       = $this->input->post("address");
        $mobile       = $this->input->post("mobile");
        $phone       = $this->input->post("phone");
        $passwd         = $this->input->post("password");
        $passwd         = md5($passwd);
        $fbId         = $this->input->post("facebookId");
        $user           = $this->Customers->findOneBy(array("email" => $email));
        $status         = false; 
        $exist         = false;
        $data         = false; 
        $userPass = (empty($user) !== false || $user === null)?'':$user->getPassword();
        if (empty($user) !== false || $user == null) {
            if (preg_match('/^[ÑñáéíóúÁÉÍÓÚa-zA-Z\s]*$/', $name) && preg_match('/^[ÑñáéíóúÁÉÍÓÚa-zA-Z\s]*$/', $lastName)) {
            
                $user = new models\Customers();
                $user->setEmail($email);
                $user->setName($name);
                $user->setLastName($lastName);
                $user->setPassword($passwd);
                $user->setAddress($address);
                $user->setMobile($mobile);
                $user->setMailMarketing($mailMarketing);
                $user->setPhone($phone);
                $user->setFacebookId($fbId);
                $user->setCreationDate(new DateTime(date("Y-m-d")));
                if ($campaingReference>0) {
                    $acampaign = $this->em->find("models\Campaigns",$campaingReference);
                    $user->setCampaign($acampaign);

                }
                $this->em->persist($user);
                $this->em->flush();
                if($user->getId() !== null && $user->getId() > 0){
                    $this->load->helper('sendy');
                    if ($campaingReference==3) {
                        $sendy = new Sendy(AuthConstants::SDY_EDUCATIONLIST);
                    }else{
                        if($cupon != null){
                            $cupon = new models\Cupons();
                            $code = substr(md5(rand(1,999999)),0,7);
                            $cupon->setCode($code);
                            $cupon->setFilter(AuthConstants::CUPON_FILTER_CATS);
                            $cupon->setFilterId(5);
                            $cupon->setCustomer($user);

                            $cupon->setDiscount(AuthConstants::CUPON_REGISTER);
                            $end = new DateTime(date("Y-m-t"));
                            $cupon->setEnd($end);
                            $this->em->persist($cupon);
                            $this->em->flush();
                            $id = $cupon->getId();
                            
                            $sendy = new Sendy(AuthConstants::SDY_SIGNIN10);
                            $this->load->library('mailgunmailer');
                            $to=array();
                            $to[]= array('name'=>$user->getName()." ".$user->getLastName(),'mail'=>$user->getEmail());
                
                            $params = array();
                            $params['*|CUPON|*'] = $cupon->getCode();
                            $params['*|MC_PREVIEW_TEXT|*'] = "Ingresa este código al momento de realizar tu pago";
                            $params['*|CURRENT_YEAR|*'] = date("Y");
                            $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                //            var_dump($params);
                //            var_dump($to);

                            $this->mailgunmailer->notify($to,AuthConstants::ML_WELCOME,"cuponbienvenida",$params);
                        }else{
                            $sendy = new Sendy(AuthConstants::SDY_SINGINLIST);
                        }
                    }
                    $sendy->subscribe($name." ".$lastName, $email);
                    $status = true;
                    $data = $user->getName();
                }
            }
        }else{
            if($userPass == ''){
                $user->setEmail($email);
                $user->setName($name);
                $user->setLastName($lastName);
                $user->setPassword($passwd);
                $user->setAddress($address);
                $user->setMobile($mobile);
                $user->setMailMarketing($mailMarketing);
                $user->setPhone($phone);
                $user->setLastAccess(new DateTime(date("Y-m-d")));
                $this->em->persist($user);
                $this->em->flush();
                if($user->getId() !== null && $user->getId() > 0){
                    $status = true;
                    $data = $user->getName();
                }
            }  else {
                $exist = true;
            } 
        }
        if (!$this->input->is_ajax_request()) {
            if($status){
//                $params = array();
//                $params['*|NAME|*'] = $user->getName();
//                $params['*|CURRENT_YEAR|*'] = date("Y");
//                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
//                $this->load->library('mailgunmailer');
//                $fullName = $user->getName()." ".$user->getLastName();
//                $to=array();
//                $to[]= array('name'=>$fullName,'mail'=>$email);
//
//                $this->mailgunmailer->notify($to,  AuthConstants::ML_WELCOME,"bienvenida",$params);
                $this->authCustomer(true);
            }  else {
                if($exist){
                    redirect("/looking/index/?userExist=1");
                }  else {
                    redirect("/looking/index/?signinError=1");
                }
            }
        }  else {
            echo json_encode(array('status'=>$status,'user'=>array('name'=>$data)));
        }
    }
    public function  signinCampaign (){
        $this->loadRepository("Customers");
        
        $this->session->sess_destroy();
        $this->session->sess_create();
        
        $campaign       = $this->input->post("campaign");
        $email          = $this->input->post("email");
        $name           = $this->input->post("name");
        $lastName       = $this->input->post("last_name");
        $address       = $this->input->post("address");
        $mailMarketing  = ($this->input->post("mailMarketing")=="on");
        $mobile       = $this->input->post("mobile");
        $phone       = $this->input->post("phone");
        $school       = $this->input->post("category");
        $grade       = $this->input->post("grade");
        $passwd         = $this->input->post("password");
        $passwd         = md5($passwd);
        $user           = $this->Customers->findOneBy(array("email" => $email));
        $status         = false; 
        $exist         = false;
        $data         = false; 
        $userPass = (empty($user) !== false || $user === null)?'':$user->getPassword();
        if (empty($user) !== false || $user == null) {
            $user = new models\Customers();
            $user->setEmail($email);
            $user->setName($name);
            $user->setLastName($lastName);
            $user->setPassword($passwd);
            $user->setAddress($address);
            $user->setMailMarketing($mailMarketing);
            $user->setMobile($mobile);
            $user->setPhone($phone);
            $user->setMailMarketing(1);
            $acampaign = $this->em->find("models\Campaigns",$campaign);
            $user->setCampaign($acampaign);
            $user->setCreationDate(new DateTime(date("Y-m-d")));
            $this->em->persist($user);
            $this->em->flush();
            if($user->getId() !== null && $user->getId() > 0){
                $this->load->helper('sendy');
                $sendy = new Sendy(AuthConstants::SDY_SINGINLIST);
                $sendy->subscribe($name." ".$lastName, $email);
                $userData = new models\CustomersData();
                $userData->setCustomer($user);
                $userData->setSchool($school);
                $userData->setGrade($grade);
                $this->em->persist($userData);
                $this->em->flush();
                $status = true;
                $data = $user->getName();
            }
        }else{
            if($userPass == ''){
                $user->setEmail($email);
                $user->setName($name);
                $user->setLastName($lastName);
                $user->setPassword($passwd);
                $user->setMailMarketing($mailMarketing);
                $user->setAddress($address);
                $user->setMobile($mobile);
                $user->setPhone($phone);
                $user->setLastAccess(new DateTime(date("Y-m-d")));
                $this->em->persist($user);
                $this->em->flush();
                if($user->getId() !== null && $user->getId() > 0){
                    $status = true;
                    $data = $user->getName();
                }
            }  else {
                $exist = true;
            } 
        }
        if (!$this->input->is_ajax_request()) {
            if($status){
                $params = array();
                $params['*|NAME|*'] = $user->getName();
                $params['*|CURRENT_YEAR|*'] = date("Y");
                $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
                $this->load->library('mailgunmailer');
                $fullName = $user->getName()." ".$user->getLastName();
                $to=array();
                $to[]= array('name'=>$fullName,'mail'=>$email);

                $this->mailgunmailer->notify($to,  AuthConstants::ML_WELCOME,"bienvenida",$params);
                $this->authCustomer(true);
            }  else {
                if($exist){
                    redirect("/looking/campaign/0/1");
                }  else {
                    redirect("/looking/index/?loginError=1");
                }
            }
        }  else {
            echo json_encode(array('status'=>$status,'user'=>array('name'=>$data)));
        }
    }
    public function webLogout (){
        $this->session->sess_destroy();
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }  else {
            echo json_encode(array('status'=>true));
        }
    }
    
    public function logout ()
    {
        $this->session->sess_destroy();
        if (!$this->input->is_ajax_request()) {
            redirect("/login/index/");
        }  else {
            echo json_encode(array('status'=>true));
        }
    }
    
    public function resetPassword()
    {
        $json = array();
        
        try {
            $this->load->helper('user');
            
            $email = trim($this->input->post("user"));
            
            $json["message"] = "ko";
            
            if (resetPasswordLogic($email)) {
                $json["message"] = "ok";
            }
            
            echo json_encode($json);
        } catch (Exception $exc) {
            $json["message"] = $exc->getMessage();
            echo json_encode($json);
        }
    }
    
    public function setUp()
    {
        $country = new models\Countries();
        $country->setName("Colombia");
        $country->setCode("COL");
        $this->em->persist($country);
        
        $city = new models\Cities();
        $city->setName("Bogota");
        $city->setCountry($country);
        $this->em->persist($city);
        
        $profile = new models\Profiles();
        $profile->setName(AuthConstants::PROFILE_ADMIN);
        $profile->setDescription("This is the super admin.");
        $this->em->persist($profile);
        
        $user = new models\Users();
        $user->setName(AuthConstants::PROFILE_ADMIN);
        $user->setLastName(AuthConstants::PROFILE_ADMIN);
        $user->setEmail("admin@admin.com");
        $user->setPassword(md5("admin"));
        $user->setAdmin(AuthConstants::ADMIN_OK);
        $user->setCity($city);
        $user->setProfile($profile);
        $user->setLanguage($this->config->config["language"]);
        $this->em->persist($user);
        
        $this->createPermissions();
        
        $this->load->helper("setup");
        $classes = getClassesFromFile();
        $this->setFileLanguage($classes);
    }

    public function createPermissions(){
        $this->load->helper("setup");
        $classes = getClassesFromFile();
        createPermissions($classes);
    }

    public function updatePermissions(){
        $this->load->helper("setup");
        $classes = getClassesFromFile();
        updatePermissions($classes);
    }
    
    private function setFileLanguage($classes){
        $DS = DIRECTORY_SEPARATOR;
        $pathLaguage = dirname(__FILE__) . $DS . ".." .$DS . ".." . $DS . ".." .$DS ."language" . $DS;
        $directories = Soporte::leeDirectorio($pathLaguage);
        
        foreach ($directories as  $key => $value){
            $content  = "<?php\n";
            
            foreach ($classes as $aClass => $methods){
                $content .= "\$lang['".$aClass."']='".$aClass."';\n";
                
                foreach ($methods as $aMethod){
                    $content .= "\$lang['".$aClass."_".$aMethod."']='".$aClass."_".$aMethod."';\n";
                }
            }
            
            $pathLang   = $pathLaguage . $key . $DS . "menu_lang.php";
            $file    = fopen($pathLang, "w");
            fwrite($file, $content);
            fclose($file);
        }
    }
    public function signin_company (){
        
        /**
         * Default company parameters
         */
        $_POST['languague'] = "es-co";
        $_POST['idProfile'] = 2;
        $_POST['idCity'] = 110;
        $_POST['name'] = $this->input->post("company");
        $_POST['lastName'] = "";
        
        var_dump($this->input->post());
        
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('company', 'lang:company', 'required');
        $this->form_validation->set_rules('contact', 'lang:contact', 'required');
        $this->form_validation->set_rules('phone', 'lang:phone', 'required');
        $this->form_validation->set_rules('address', 'lang:address', 'required');
        $this->form_validation->set_rules('password', 'lang:password', 'required');
//        $this->form_validation->set_rules('email', 'lang:email', 'required|callback_existUser['.$this->input->post("id").']');
        $this->form_validation->set_message('existUser', lang('user_exist'));
        
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('usersdata/userdata', $this->input->post()); 
            }else{
                $output = $this->rest->put('usersdata/userdata', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $this->auth();
                    $message = ($this->input->post("id") > 0) ? lang('user_edition') : lang('user_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        var_dump($message);
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
        
        
        $params = array();
        $params['*|NAME|*'] = "564654";
        $params['*|ORDER|*'] = "654654";
        $params['*|CURRENT_YEAR|*'] = date("Y");
        $params['*|LIST:COMPANY|*'] = AuthConstants::ML_INFONAME;
        $to = array();
        $to[]= array('name'=>"John",'mail'=>"johnjime@gmail.com");
//        $this->load->library('mailgunmailer');
//        $this->mailgunmailer->notify($to,"Esta es una prueba","pendiente",$params);
    }
}
