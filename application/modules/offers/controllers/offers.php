<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offers extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["create_offer"] = site_url("offers/form");
		
		$data = array();
		$data["title"] = lang("Offers");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Offers", "o", array("id" => "id", 'name' => 'name' ,'description' => 'description' ,'filter' => 'filter' ,'filter_id' => 'filterId' ,'status' => 'status' ,'discount' => 'discount' ,'end' => 'endDate' ,));
        $model->setNumerics(array("o.id"));
        
        $campaign = new Model("Campaigns", "c", array("name"=>"nameC" ));
        $campaign->setRelation("campaign");
        $campaign->setJoinDir("LEFT");
        
        $actions = array();
        array_push($actions, new Action("offers", "form", "edit"));        
        array_push($actions, new Action("offers", "delete", "delete", false));        
        
        $relations = array();
        array_push($relations, $campaign);
        
        $this->model   = $model;
        $this->actions = $actions;
        $this->relations = $relations;
	}
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $name = $this->input->post('name');
		$description = $this->input->post('description');
		$filter = $this->input->post('filter');
		$filterId = $this->input->post('filterId');
		$status = $this->input->post('status');
		$discount = $this->input->post('discount');
		$end = $this->input->post('end');
		$campaign = $this->input->post('campaign');
        
        if ($identifier > 0){
            $output = $this->rest->get('offers/offer/', array("id"=>$identifier));
        
            if ($output->status){
                $offer    = $output->data;
                $name = $offer->name;
				$description = $offer->description;
				$filter = $offer->filter;
				$filterId = $offer->filterId;
				$status = $offer->status;
				$discount = $offer->discount;
				$date = new DateTime ($offer->end->date);
                                $end = $date->format("d/m/Y");
				$campaign = $offer->campaign;
                $id         = $offer->id;
            }
        }
        
        $this->loadRepository("Campaigns");
        $campaigns = $this->Campaigns->findAll();
        $actions = array();
        $actions["return_offer"] = site_url("offers/index");
        
        $data = array();
        $data["title"]  = lang("Offers");
        $data['name'] = $name;
        $data['description'] = $description;
        $data['filter'] = $filter;
        $data['filterId'] = $filterId;
        $data['status'] = $status;
        $data['discount'] = $discount;
        $data['end'] = $end;
        $data['campaign'] = $campaign;
        $data['campaigns'] = $campaigns;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
        $this->form_validation->set_rules('name', 'lang:name', 'required');
		$this->form_validation->set_rules('description', 'lang:description', 'required');
		$this->form_validation->set_rules('filter', 'lang:filter', 'required');
//		$this->form_validation->set_rules('filterId', 'lang:filterId', 'required');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		$this->form_validation->set_rules('discount', 'lang:discount', 'required');
		$this->form_validation->set_rules('end', 'lang:end', 'required');
//		$this->form_validation->set_rules('campaign', 'lang:campaign', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('offers/offer', $this->input->post()); 
            }else{
                $output = $this->rest->put('offers/offer', $this->input->post()); 
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('offer_edition') : lang('offer_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('offers/offer', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("offer_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}