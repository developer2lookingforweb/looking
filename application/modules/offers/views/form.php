<?php 
    $opCat = array();
    $opCat["all"] = lang("all_products");
    $opCat["brand"] = lang("brand");
    $opCat["category"] = lang("category");
    $opStat = array();
    $opStat["0"] = lang("unpublished");
    $opStat["1"] = lang("published");
    $opCamp = array();
    $opCamp[""] = lang("default_select");
    foreach ($campaigns as $key => $aCam) {
        $opCamp[$aCam->getId()] = $aCam->getName();
    }
    $fields = array();
    $fields[lang('name')] = form_input(array('name'=>'name', 'class'=>'span3 focused', 'value'=>$name));
    $fields[lang('description')] = form_input(array('name'=>'description', 'class'=>'span3 focused', 'value'=>$description));
    $fields[lang('filter')] =  form_dropdown("filter", $opCat, "", "id='filter' class='filters span4'  data-col='7'");
    $fields[lang('filterId')] = form_dropdown("filterId", array(), "", "id='filterId' class='filters span4'  data-col='7'");
    $fields[lang('status')] =  form_dropdown("status", $opStat, "", "id='status' class='filters span4'  data-col='7'");
    $fields[lang('discount')] = form_input(array('name'=>'discount', 'class'=>'span3 focused', 'value'=>$discount));
    $fields[lang('end')] = form_input(array('name'=>'end', 'id'=>'end', 'class'=>'span3 focused', 'value'=>$end));
    $fields[lang('campaign')] = form_dropdown("campaign", $opCamp, "", "id='campaign' class='filters span4'  data-col='7'");
    $hidden = array('id' => $id);
    echo print_form('/offers/persist/', $fields, $hidden);