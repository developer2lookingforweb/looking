<script type="text/javascript">
    $(document).ready(function() {
        $("select[name='status']").val(<?=$status?>);
        $("select[name='campaign']").val(<?=$campaign?>);
        $("#form").validate({
            rules: {
                name: 'required',
				description: 'required',
				filter: 'required',
//				filterId: 'required',
				status: 'required',
				discount: 'required',
				end: 'required',
//				campaign: 'required',
            },
            messages: {
                name:'<?=lang('required')?>',
				description:'<?=lang('required')?>',
				filter:'<?=lang('required')?>',
//				filterId:'<?=lang('required')?>',
				status:'<?=lang('required')?>',
				discount:'<?=lang('required')?>',
				end:'<?=lang('required')?>',
//				campaign:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
        
        $( "#end" ).datepicker();
        
        
        $("#filter").change(function(){
            if($(this).val()=="all"){
                $("#filterId").val("");
                $("#filterId").attr("disabled","disabled");
            }else{
                $.ajax({
                    url: "<?php echo base_url();?>cupons/getFilterData/",
                    type: "post",            
                    data: {type:$(this).val(),<?php echo $this->config->item('csrf_token_name') ?>:$("input[name='<?php echo $this->config->item('csrf_token_name') ?>']").val()},
                    dataType: "json",
                    success: function(data){   

                        $("#filterId").removeAttr("disabled")
                        if(data.status != false){
                            var option = $("<option><?php echo lang('select_value') ?></option>");
                            $("#filterId").html(option);
                            $.each(data.data,function(idx, option){
                                var option = $("<option></option>")
                                .attr("value", option.id)
                                .text(option.name);
                                $("#filterId").append(option);
                            });
                        }
                    }
                });
            }
        });
        $("#filter").trigger("change");
    });
</script>