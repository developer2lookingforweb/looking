<?php 
    $opCity = array();
    $opCity[""] = lang("default_select");
    foreach ($cities as $aCity){
        $opCity[$aCity->getId()] = $aCity->getName();
    }

    $opLang = array();
    $opLang[""] = lang("default_select");
    foreach ($languages as $aLanguage){
        $opLang[$aLanguage] = lang($aLanguage);
    }
    
    $fields = array();
    $fields["col-1"]["config"]       = array("large"=>"4");
    $fields["col-2"]["config"]       = array("large"=>"8");
    $fields["col-1"]["no-label"]       = Soporte::creaTag("div", 
                                                            Soporte::creaTag("img", 
                                                            "", 
                                                            "id='profileThumb' src='".site_url(AuthConstants::USERS_PATH.$profileImage)."'"),
                                                        "class='center'");
    $fields["col-2"][lang('city')]       = form_dropdown("idCity", $opCity, $idCity, "class='span10'");
    $fields["col-2"][lang('language')]   = form_dropdown("language", $opLang, $language, "class='span10'");
    $fields["col-2"][lang('name')]       = form_input(array('name'=>'name', 'class'=>'span10 focused', 'value'=>$name));
    $fields["col-2"][lang('last_name')]  = form_input(array('name'=>'lastName', 'class'=>'span10 focused', 'value'=>$last_name));
    $fields["col-2"][lang('email')]      = form_input(array('name'=>'email', 'class'=>'span10 focused', 'value'=>$email));
    //$fields["col-2"][lang('image')]      = "<div class='fileWrapper'>".form_upload(array('name'=>'profileImg','id'=>'profileImg', 'class'=>'span3 focused niceFileInput', 'value'=>"Upload"))."<span class='text'></span></div>";
    $fields["col-2"][lang('image')]      = my_form_upload(array('name'=>'profileImage','id'=>'profileImage', 'class'=>'span3 focused', 'value'=>$profileImage));
    $hidden = array('id' => $id);
    echo print_form('/users/persistMyData/', $fields, $hidden, "form", false, 12); 
