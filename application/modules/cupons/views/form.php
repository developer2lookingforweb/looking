<?php 
    $opCat = array();
    $opCat["all"] = lang("all_products");
    $opCat["brand"] = lang("brand");
    $opCat["category"] = lang("category");
    $fields = array();
//    $fields[lang('code')] = form_input(array('name'=>'code', 'class'=>'span3 focused', 'value'=>$code));
    $fields[lang('quantity')] = form_input(array('name'=>'quantity', 'class'=>'span3 focused', 'value'=>1));
    $fields[lang('filter')] =  form_dropdown("filter", $opCat, "", "id='filter' class='filters span4'  data-col='7'");
    $fields[lang('filterId')] = form_dropdown("filterId", array(), "", "id='filterId' class='filters span4'  data-col='7'");
    $fields[lang('discount')] = form_input(array('name'=>'discount', 'class'=>'span3 focused', 'value'=>$discount));
    $fields[lang('end')] = form_input(array('name'=>'end', 'id'=>'end', 'class'=>'span3 focused', 'value'=>$end));
    $hidden = array('id' => $id);
    echo print_form('/cupons/persist/', $fields, $hidden);