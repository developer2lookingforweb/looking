<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cupons extends MY_Controller 
{
	public function index()
	{
		$actions = array();
		$actions["create_cupon"] = site_url("cupons/form");
		
		$data = array();
		$data["title"] = lang("Cupons");
		$this->view('list', $data, $actions);
	}
	
	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Cupons", "c", array("id" => "id", 'code' => 'code' ,'filter' => 'filter' ,'filter_id' => 'filterId' ,'discount' => 'discount' ,'end' => 'endDate'));
        $model->setNumerics(array("c.id"));

        $actions = array();
//        array_push($actions, new Action("cupons", "form", "edit"));        
        array_push($actions, new Action("cupons", "delete", "delete", false));        
        
        $this->model   = $model;
        $this->actions = $actions;
	}
        
    
    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $code = $this->input->post('code');
		$filter = $this->input->post('filter');
		$filterId = $this->input->post('filterId');
		$discount = $this->input->post('discount');
		$end = $this->input->post('end');
        
        if ($identifier > 0){
            $output = $this->rest->get('cupons/cupon/', array("id"=>$identifier));
        
            if ($output->status){
                $cupon    = $output->data;
                $code = $cupon->code;
                $filter = $cupon->filter;
                $filterId = $cupon->filterId;
                $discount = $cupon->discount;
                $date = new DateTime ($cupon->end->date);
                $end = $date->format("d/m/Y");
                $id         = $cupon->id;
            }
        }
        
        $actions = array();
        $actions["return_cupon"] = site_url("cupons/index");
        
        $data = array();
        $data["title"]  = lang("Cupons");
        $data['code'] = $code;
		$data['filter'] = $filter;
		$data['filterId'] = $filterId;
		$data['discount'] = $discount;
		$data['end'] = $end;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }
    
    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = ""; 
        
//        $this->form_validation->set_rules('code', 'lang:code', 'required');
		$this->form_validation->set_rules('filter', 'lang:filter', 'required');
//		$this->form_validation->set_rules('filterId', 'lang:filterId', 'required');
		$this->form_validation->set_rules('discount', 'lang:discount', 'required');
		$this->form_validation->set_rules('end', 'lang:end', 'required');
                
        if ($this->form_validation->run($this)){
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('cupons/cupon', $this->input->post()); 
            }else{
                if($this->input->post("quantity")>0){
                    for($i=0; $i<$this->input->post("quantity");$i++){
                        $output = $this->rest->put('cupons/cupon', $this->input->post()); 
                    }
                }
            }
            
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('cupon_edition') : lang('cupon_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }
            
            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }
        
        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }
    
    public function getFilterData(){
        $type = $this->input->post('type');
        $results = array();
        if($type=="category"){
            $this->loadRepository("Categories");
            $results = $this->Categories->findBy(array('status'=>  1));
        }
        if($type=="brand"){
            $this->loadRepository("Brands");
            $results = $this->Brands->findBy(array('status'=>  1));
        }
        $return = array();
        foreach ($results as $key => $item) {
            if($type=="category"){
                $return[] = array('id'=>$item->getId(),'name'=>$item->getCategory());
            }
            if($type=="brand"){
                $return[] = array('id'=>$item->getId(),'name'=>$item->getBrand());
            }
        }
        echo json_encode(array('status'=>true,'data'=>$return));
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";
        
        $output = $this->rest->delete('cupons/cupon', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("cupon_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }
        
        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}