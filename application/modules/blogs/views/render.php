<style type="text/css">
.bg-primary:{
    background-color: #ff2f28!important;
}
</style>
<header class="header-spacing"></header>
<div class="container-fluid bg-primary col-sm-12 categories-container">
    <div class="btn-group">
    </div>
</div>
<section id="blog-entry">

    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?= $title ?></h1>
                            <?= $content ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="comments">
                                <div class="fb-comments" data-href="<?= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-width="100%" data-numposts="5"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <form action="http://lookingforweb.com/sendy/subscribe" method="post" class="sign-up">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <p class="lead text-center">¡Saca el máximo provecho de tu dinero!</p>
                            <p class="text-center">Aprende tips y recibe material que te ayudará a tomar decisiones inteligentes.</p>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="list" value="qimA763J24863swVos2Pu3Lg">
                            <button class="btn btn-primary form-control">Suscribirme <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </form>

                <p class="lead">Últimas Entradas</p>

                <ul class="list-group">
                    <?php foreach ($last_blogs as $aBlog){ ?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url($this->router->fetch_module()."/display/".$aBlog->getId()."/".$aBlog->getAlias())?>">
                                <?php echo $aBlog->getTitle()?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

</section>