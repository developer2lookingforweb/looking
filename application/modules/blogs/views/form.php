
<?php 
    $fields = array();
    $fields = array();
    $fields["col-1"]["config"]       = array("large"=>"4");
    $fields["col-2"]["config"]       = array("large"=>"8");
    $fields["col-3"]["config"]       = array("large"=>"12");
    $fields["col-2"][lang('title')] = form_input(array('name'=>'title', 'class'=>'span3 focused', 'value'=>$title));
    $fields["col-2"][lang('alias')] = form_input(array('name'=>'alias', 'class'=>'span3 focused', 'value'=>$alias));
    $fields["col-2"][lang('description')] = form_input(array('name'=>'description', 'class'=>'span3 focused', 'value'=>$description));
    $fields["col-1"]["no-label"] = Soporte::creaTag("div", 
                                Soporte::creaTag("img", 
                                "", 
                                "id='profileThumb' src='".site_url(AuthConstants::GALLERY_PATH.$image)."'"),
                            "class='center'");
    $fields["col-2"][lang('image')] = my_form_upload(array('name'=>'image','id'=>'image', 'class'=>'span3 focused', 'value'=>$image));
    $fields["col-2"][lang('published')] = form_input(array('name'=>'published', 'class'=>'large-6 focused', 'value'=>$published));
    $fields["col-3"]["no-label"] = form_textarea(array('name'=>'content', 'id'=>'tinymce', 'class'=>'span3 focused tinymce','rows'=>50),$content);
    if($id>0)$fields["col-1"][lang('likes')] = form_input(array('name'=>'likes', 'class'=>'large-6 focused', 'value'=>$likes));
    if($id>0)$fields["col-1"][lang('createdBy')] = form_input(array('name'=>'createdBy', 'class'=>'large-6 focused', 'value'=>$createdBy));
    if($id>0)$fields["col-2"][lang('creationDate')] = form_input(array('name'=>'creationDate', 'class'=>'large-6 focused', 'value'=>$creationDate));
    $hidden = array('id' => $id);
    echo print_form('/blogs/persist/', $fields, $hidden,"form",false,12);
    ?>

