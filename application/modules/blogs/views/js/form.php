<script type="text/javascript">
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                title: 'required',
//                image: 'required',
                description: 'required',
                content: 'required',
                published: 'required',
                alias: 'required',
//                likes: 'required',
//                createdBy: 'required',
//                creationDate: 'required',
            },
            messages: {
                title:'<?=lang('required')?>',
//                image:'<?=lang('required')?>',
                description:'<?=lang('required')?>',
                content:'<?=lang('required')?>',
                published:'<?=lang('required')?>',
                alias:'<?=lang('required')?>',
//                likes:'<?=lang('required')?>',
//                createdBy:'<?=lang('required')?>',
//                creationDate:'<?=lang('required')?>',
            },
            submitHandler: function(form) {
                $('#form').ajaxSubmit({success: function(data){
                        if (data.message != ""){
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                        
                        if (data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }      
                    },
                    dataType: 'json'
                    <?php echo ($id == "") ? ",'resetForm': true" : ''; ?>
                });
            }
        });
        $("#image").change(function(){
            var file=($(this).val());
            $(".fileWrapper .text").html(file);                        
            readURL(this,"profileThumb");            
        });
//        $("#tinymce").tinymce({
//          height: 400
//          
//        });
        tinyMCE.baseURL = "<?php echo base_url("js/tinymce/");?>";
        tinymce.init({
            document_base_url: "<?php echo base_url("js/tinymce/");?>",
            selector: 'textarea',
            height: 500,
            theme: 'modern',
            plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern responsivefilemanager'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
            toolbar2: 'print preview media | forecolor backcolor emoticons |',
            image_advtab: true,
            relative_urls: false,
            templates: [
                  { title: 'Test template 1', content: 'Test 1' },
                  { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                  '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                  '//www.tinymce.com/css/codepen.min.css'
            ],
            external_filemanager_path:"<?php echo base_url("imagemanager/")?>/",
            filemanager_title:"Responsive Imagemanager" ,
            external_plugins: { "filemanager" : "plugins/responsivefilemanager/plugin.min.js"}
          });
    });
</script>