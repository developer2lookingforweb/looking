<style type="text/css">
.bg-primary:{
    background-color: #ff2f28!important;
}
#blog-entries .blog-header p{
    color: white;
}
#blog-entries .blog-header {
background: url(../images/blog-header.png) right no-repeat #f05f40;
	background-size: auto 100%;
}
.carousel-inner > .item > a > img, .carousel-inner > .item > img, .img-responsive, .thumbnail a > img, .thumbnail > img {
	display: block;
	max-width: 100%;
	height: auto;
}
</style>
<header class="header-spacing"></header>
<div id="blog-entries">

    <header class="jumbotron hero-spacer blog-header">
        <div class="container text-left">
            <h1>Blog</h1>
            <div class="row">
                <div class="col-md-6">
                    <p>Elegir bien es uno de los secretos de los millonarios para poder amasar una fortuna. Aprende a hacerlo con nuestros tips y saca el mayor provecho de ellos.</p>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php foreach ($blogs as $aBlog){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="<?= base_url($this->router->fetch_module()."/display/".$aBlog->getId()."/".$aBlog->getAlias())?>">
                                            <img src="/images/gallery/<?= $aBlog->getImage() ?>" alt="<?= $aBlog->getTitle() ?>" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="blog-description">
                                            <p class="lead"><a href="<?= base_url($this->router->fetch_module()."/display/".$aBlog->getId()."/".$aBlog->getAlias())?>"><?= $aBlog->getTitle() ?></a></p>
                                            <p class="description-txt"><?= $aBlog->getDescription() ?></p>
                                            <p><a href="<?= base_url($this->router->fetch_module()."/display/".$aBlog->getId()."/".$aBlog->getAlias())?>" class="btn btn-primary form-control">Leer el artículo completo <i class="fa fa-arrow-circle-right"></i></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <form action="http://lookingforweb.com/sendy/subscribe" method="post" class="sign-up">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <p class="lead text-center">¡Saca el máximo provecho de tu dinero!</p>
                            <p class="text-center">Aprende tips y recibe material que te ayudará a tomar decisiones inteligentes.</p>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="list" value="qimA763J24863swVos2Pu3Lg">
                            <button class="btn btn-primary form-control">Suscribirme <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>