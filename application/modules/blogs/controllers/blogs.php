<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogs extends MY_Controller
{
    public $sauth_noauth = array("display","entries");
	public function index()
	{
		$actions = array();
		$actions["create_blog"] = site_url("blogs/form");

		$data = array();
		$data["title"] = lang("Blogs");
		$this->view('list', $data, $actions);
	}

	public function setListParameters()
	{
        $this->load->helper('action');

        $model = new Model("Blogs", "b", array("id" => "id", 'title' => 'title' ,'image' => 'image' ,'description' => 'description' ,'published' => 'published' ,'alias' => 'alias' ,'likes' => 'likes' ,'created_by' => 'createdBy' ,'creation_date' => 'creationDate'));
        $model->setNumerics(array("b.id"));

        $actions = array();
        array_push($actions, new Action("blogs", "form", "edit"));
        array_push($actions, new Action("blogs", "delete", "delete", false));

        $this->model   = $model;
        $this->actions = $actions;
	}
    public function entries()
	{
            $data = array();
            $actions = array();
            $data["title"] = lang("Blogs");
            $this->loadRepository("Blogs");
            $blogs = $this->Blogs->findBy(array("published"=>1));
            $data["blogs"] = $blogs;
            $this->load->library('../modules/looking/controllers/looking');
            $this->session->set_userdata(AuthConstants::CATEGORIES,        $this->looking->getCategories(1));
            $this->viewFrontend2('entries', $data, $actions);

	}
    public function display($identifier=0)
	{
            $actions = array();
            $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
            $title = $this->input->post('title');
            $image = $this->input->post('image');
            $description = $this->input->post('description');
            $content = $this->input->post('content');
            $published = $this->input->post('published');
            $alias = $this->input->post('alias');
            $createdBy = $this->input->post('createdBy');
            $creationDate = $this->input->post('creationDate');
            $this->load->library('../modules/looking/controllers/looking');
            $this->session->set_userdata(AuthConstants::CATEGORIES,        $this->looking->getCategories(1));
            if ($identifier > 0) {
//                $output = $this->rest->get('blogs/blog/', array("id" => $identifier));
                $this->loadRepository("Blogs");
                $output = $this->Blogs->find($identifier);

                if ($output ==! null) {
                    $article = $output;
                    $title = $article->getTitle();
                    $image = $article->getImage();
                    $description = $article->getDescription();
                    $content = $article->getContent();
                    $published = $article->getPublished();
                    $alias = $article->getAlias();
                    $createdBy = $article->getCreatedBy();
                    $creationDate = $article->getCreationDate();
                    $id = $article->getId();
                }
            }
            $actions = array();
            $actions["return_article"] = site_url("articles/index");

            $this->loadRepository("Blogs");
            $blogs = $this->Blogs->findBy(array("published"=>1));
            $lastBlogs = array_slice($blogs, -3, 3, true);

            $data = array();
            $data["title"] = lang("Blogs");
            $data['title'] = $title;
            $data['last_blogs'] = $lastBlogs;
            $data['banner'] = $image;
            $data['showBanner'] = $description;
            $data['content'] = $content;
            $data['published'] = $published;
            $data['alias'] = $alias;
            $data['createdBy'] = $createdBy;
            $data['creationDate'] = $creationDate;
            $data["id"] = $id;
            $this->viewFrontend2('render', $data, $actions);
        }

    public function form ($identifier = 0)
    {
        $id = ($this->input->post("id") > 0) ? $this->input->post("id") : 0;
        $title = $this->input->post('title');
		$image = $this->input->post('image');
		$description = $this->input->post('description');
		$content = $this->input->post('content');
		$published = $this->input->post('published');
		$alias = $this->input->post('alias');
		$likes = $this->input->post('likes');
		$createdBy = $this->input->post('createdBy');
		$creationDate = $this->input->post('creationDate');

        if ($identifier > 0){
            $output = $this->rest->get('blogs/blog/', array("id"=>$identifier));

            if ($output->status){
                $blog    = $output->data;
                $title = $blog->title;
				$image = $blog->image;
				$description = $blog->description;
				$content = $blog->content;
				$published = $blog->published;
				$alias = $blog->alias;
				$likes = $blog->likes;
				$createdBy = $blog->created_by;
				$creationDate = new DateTime($blog->creation_date->date);
				$creationDate = $creationDate->format("Y-m-d");
                $id         = $blog->id;
            }
        }

        $actions = array();
        $actions["return_blog"] = site_url("blogs/index");

        $data = array();
        $data["title"]  = lang("Blogs");
        $data['title'] = $title;
		$data['image'] = $image;
		$data['description'] = $description;
		$data['content'] = $content;
		$data['published'] = $published;
		$data['alias'] = $alias;
		$data['likes'] = $likes;
		$data['createdBy'] = $createdBy;
		$data['creationDate'] = $creationDate;
        $data["id"] = $id;
        $this->view('form', $data, $actions);
    }

    public function persist ()
    {
        $data = array();
        $message = "";
        $error   = "";
        $output  = "";

        $this->form_validation->set_rules('title', 'lang:title', 'required');
//        $this->form_validation->set_rules('image', 'lang:image', 'required');
        $this->form_validation->set_rules('description', 'lang:description', 'required');
        $this->form_validation->set_rules('content', 'lang:content', 'required');
        $this->form_validation->set_rules('published', 'lang:published', 'required');
        $this->form_validation->set_rules('alias', 'lang:alias', 'required');
//        $this->form_validation->set_rules('likes', 'lang:likes', 'required');
//        $this->form_validation->set_rules('createdBy', 'lang:createdBy', 'required');
//        $this->form_validation->set_rules('creationDate', 'lang:creationDate', 'required');

        if ($this->form_validation->run($this)){
            $config['upload_path'] = "./".AuthConstants::GALLERY_PATH;
            $config['allowed_types'] = AuthConstants::IMAGES_EXTENSIONS;
            $config['max_size']	= AuthConstants::DOCUMENT_SIZE;
            $posfixMsg="";
            $this->load->library('upload', $config);
            $arrayPost = $this->input->post();
            if($this->upload->do_upload('image')){
                $uploadData = $this->upload->data();
                $arrayPost["image"] = $uploadData["file_name"];
            }
            if ($this->input->post("id") > 0){
                $output = $this->rest->post('blogs/blog', $arrayPost);
//                var_dump($output);exit;
            }else{
                $arrayPost["createdBy"] = $this->session->userdata(AuthConstants::USER_ID);
                $arrayPost["creationDate"] = date("Y-m-d H:i:s");
                $arrayPost["likes"] = 0;
                $output = $this->rest->put('blogs/blog', $arrayPost);
            }
            if (empty($output) == false){
                if ($output->status){
                    $message = ($this->input->post("id") > 0) ? lang('blog_edition') : lang('blog_creation');
                }else{
                    $error = (isset($output->error)) ? $output->error : "";
                }
            }

            if ($this->input->is_ajax_request() == false){
                $this->index();
            }
        }else{
            $error = validation_errors();
            if ($this->input->is_ajax_request() == false){
                $this->form();
            }
        }

        if ($this->input->is_ajax_request()){
            $data["message"] = $message;
            $data["error"]   = $error;
            echo json_encode($data);
        }
    }

    public function delete()
    {
        $data    = array();
        $message = "";
        $warning = "";
        $error   = "";

        $output = $this->rest->delete('blogs/blog', array("id"=>$this->input->post("id")));

        if ($output->status){
            $message = lang("blog_delete");
        }else{
            $error = (isset($output->error)) ? $output->error : "";
            $warning = (isset($output->warning)) ? lang($output->warning) : "";
        }

        $data["message"] = $message;
        $data["warning"] = $warning;
        $data["error"]   = $error;
        echo json_encode($data);
    }
}