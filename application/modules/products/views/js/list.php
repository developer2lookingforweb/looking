<script type="text/javascript">
    $(document).ready(function() {
        var list = $('#list').dataTable( {
            "aoColumns":[null,null ,null ,{"bVisible": true,"bSearchable":false} ,null,null ,{"bVisible": false} ,{"bVisible": true,"bSearchable":false},{"bVisible": false} ,null ,{ "bSortable": false,"bSearchable": false }],
            "sAjaxSource": "<?php echo site_url('/products/getList'); ?>",
        });

        var id = 0;
        $( "#confirmation_delete" ).dialog({
            autoOpen: false,
            resizable: false,
            height:"auto",
            modal: true,
            buttons: {
                "<?php echo lang("button_delete_ok"); ?>": function() {
                    $.post("<?php echo site_url('/products/delete'); ?>",
                    {'id':id, '<?=$csrf?>': $('input[name=<?=$csrf?>]').val()},
                    function(data){
                        $("#confirmation_delete").dialog( "close" );
                        if(data.error != ""){
                            $('#alert').addClass("alert");
                            $("#message").html(data.error);
                            $("#alert").show();
                        }
                        
                        if (data.warning != "") {
                            $('#alert').addClass("alert-info");
                            $("#message").html(data.warning);
                            $("#alert").show();
                        }
                        
                        if(data.message != ""){
                            list.fnDraw();
                            $('#alert').addClass("success");
                            $("#message").html(data.message);
                            $("#alert").show();
                        }
                    },'json'
                );
                },
                "<?php echo lang("button_delete_ko"); ?>": function() {
                    $( this ).dialog( "close" );
                }
            }
        });

        $(document).on("click", ".action_delete", function(){
     	   id = this.id;
           $( "#message_delete" ).html("<?php echo lang("message_delete_product"); ?>");
           $( "#confirmation_delete" ).dialog("open");
        });
        
        $(document).on("click", ".action_change_active", function(){
     	   id = this.id;
           $.post("<?php echo site_url('/products/publish'); ?>",
                {'id':id, '<?= $csrf ?>': $('input[name=<?= $csrf ?>]').val()},
                function(data){
                    if (data.error != "") {
                        $('#alert').addClass("alert");
                        $("#message").html(data.error);
                        $("#alert").show();
                    }

                    if (data.warning != "") {
                        $('#alert').addClass("alert-info");
                        $("#message").html(data.warning);
                        $("#alert").show();
                    }

                    if (data.message != "") {
                        list.fnDraw();
                        $('#alert').addClass("success");
                        $("#message").html(data.message);
                        $("#alert").show();
                    }
                }, 'json'
            );
        });
        
        $(document).on("click", ".action_change_outstanding", function(){
     	   id = this.id;
           $.post("<?php echo site_url('/products/outstanding'); ?>",
                {'id':id, '<?= $csrf ?>': $('input[name=<?= $csrf ?>]').val()},
                function(data){
                    if (data.error != "") {
                        $('#alert').addClass("alert");
                        $("#message").html(data.error);
                        $("#alert").show();
                    }

                    if (data.warning != "") {
                        $('#alert').addClass("alert-info");
                        $("#message").html(data.warning);
                        $("#alert").show();
                    }

                    if (data.message != "") {
                        list.fnDraw();
                        $('#alert').addClass("success");
                        $("#message").html(data.message);
                        $("#alert").show();
                    }
                }, 'json'
            );
        });
        $(".filters").change(function(){
            var filterCol = $(this).attr('data-col');
            var filterValue = $(this).val();
            list.fnFilter(filterValue, filterCol);
//            list.fnDraw();
        })
    });
</script>
