<?php
    $opCat = array();
    $opCat[""] = lang("default_select");
    foreach ($categories as $aCat){
        $opCat[$aCat->getId()] = $aCat->getCategory();
    }
    $opBrand = array();
    $opBrand[""] = lang("default_select");
    foreach ($brands as $aBrand){
        $opBrand[$aBrand->getId()] = $aBrand->getBrand();
    }
?>
<form>
  
  <div class="row">
    <div class="large-4 columns">
        <label><?php echo lang("category");?>
        <?php echo form_dropdown("category", $opCat, "", "id='category' class='filters span4'  data-col='8'");?>
      </label>
    </div>
    <div class="large-4 columns">
        <label><?php echo lang("brand");?>
        <?php echo form_dropdown("brand", $opBrand, "", "id='brand' class='filters span4'  data-col='6'");?>
      </label>
    </div>
    <div class="large-4 columns">
        <label><?php echo lang("published");?>
            <select id='published' class="filters span4" data-col='2' >
          <option value=""><?php echo lang("default_select");?></option>
          <option value="1">Si</option>
          <option value="0">No</option>
        </select>
      </label>
    </div>
  </div>
  
</form>
<table id="list" class="display">
    <thead>
        <tr>
            <th><?php echo lang('id') ?></th>
            <th><?=lang('product')?></th>
            <!--<th><?=lang('image')?></th>-->
            <th><?=lang('published')?></th>
            <th><?=lang('outstanding')?></th>
            <th><?=lang('cost')?></th>
            <th><?=lang('market_price')?></th>
            <th><?=lang('brand')?></th>
            <th><?=lang('brand')?></th>
            <th><?=lang('category')?></th>
            <th><?=lang('category')?></th>
            <th><?php echo lang('actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
        </tr>
    </tbody>
</table>
<?php echo form_open("") . form_close(); ?>