<?php 
    $fields = array();
    foreach ($categories_configs as $property){
        $rank = "";
        $value = "";
        if(count($properties)>0){
            foreach ($properties as $key => $value) {
                if($property->getName() == $value["property"]){
                    if($value["rank"]!==null){
                        $rank = $value["rank"];
                    }
                    $value = $value["value"];
                    break;
                }  

            }
        }
        $fields[lang(strtolower($property->getName()))] = 
                "<div class='columns large-5'>".
                form_input(array('name'=>'properties[idx_'.($property->getName()).']', 'id'=>$property->getId(), 'class'=>'span3 focused required large-6', 'value'=>$value)).
                "</div>".
                "<div class='columns large-3 large-push-1'><label>".lang("rank")."</label></div>".
                "<div class='columns large-3'>".
                form_input(array('name'=>'rank[idx_'.($property->getName()).']', 'id'=>"rank_".$property->getId(), 'class'=>'span3 focused required  large-6', 'value'=>$rank)).
                "</div>";
    }
    $hidden = array('id' => $id);
//    echo "<pre>";
//    var_dump($properties);exit;
    echo print_form('/products/persistProperties/', $fields, $hidden,"form_properties",false,10);