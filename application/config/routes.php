<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "browser";
$route['default_controller'] = "looking";
$route['empresas'] = "login/login";
$route['contact-us'] = "messages/userMessage";
//$route['navidad'] = "looking/campaign";
$route['sales'] = "looking/campaign/4";
$route['educacion'] = "looking/campaign/3";
$route['registro-empresas'] = "login/signin_form";
$route['404_override'] = 'landing/notFound';
$route['detail/(:num)/(:any)'] = 'looking/detail/$1/$2';
$route['credit/(:num)'] = 'looking/credit/$1';
$route['category/(:num)/(:any)'] = 'looking/category/$1/$2';
$route['compare/(:any)'] = 'looking/compare/$1';
$route['browse'] = 'looking/browse';
$route['brand/(:any)'] = 'looking/brand/$1';
$route['outstanding'] = 'looking/outstanding';
$route['favorites'] = 'looking/favorites';
$route['checkout'] = 'looking/checkout';
$route['bienvenido'] = 'looking/bienvenido';
$route['simulador'] = 'looking/simulator';
$route['asignar'] = 'orders/assign';
$route['recargas'] = 'looking/puntored';
$route['reporte-recargas'] = 'looking/puntoredReport';


/* End of file routes.php */
/* Location: ./application/config/routes.php */