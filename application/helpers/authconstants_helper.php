<?php
class AuthConstants
{
    const ERROR_LOGIN   = "auth_error";
    const USER_ID       = "auth_user_id";
    const EMAIL         = "auth_email";
    const NAMES         = "auth_names";
    const LAST_NAMES    = "auth_last_names";
    const FB_ID         = "auth_fb_id";
    const ADMIN         = "auth_admin";
    const ADMIN_OK      = "1";
    const ADMIN_KO      = "0";
    const YES           = "YES";
    const NO            = "NO";
    const PROFILE       = "auth_profile";
    const PROFILE_NAME  = "auth_profile_name";
    const REF_CAMPAIGN  = "reference_campaign";
    const LANG          = "lang";
    const PROFILE_ADMIN = "Admin";
    const WORKSHOPS     = "user_workshops";
    const COUNTRY       = "user_country";
    const MENU_SELECTED = "menu_selected";
    const THEME = "default";
    const FAVORITES = "favorites";
    const CATEGORIES = "categories";
    const USER_IMAGE = "user_image";
    
    //IDS PROFILES
    const ID_PROFILE_ADMIN      = 1;
    const ID_PROFILE_ATTENDANT  = 2;
    const ID_PROFILE_USER       = 3;
    const ID_PROFILE_WORKSHOP_ADMIN = 4;
    
    //SHIPPING
    const ID_BOGOTA = 110;
    const ID_STANDARD = 1;
    const ID_PREMIUM = 2;
    const ID_ONSITE = 3;
    const SHIPPING_PREMIUM_COST = 23000;
    
    //APPOINTMENTS STATUS
    const STATUS_SCHEDULED  = 'scheduled';
    const STATUS_CANCELED   = 'canceled';
    const STATUS_COMPLETED  = 'completed';
    
    //VEHICLES STATUS
    const VEHICLE_ACTIVE    = 'active';
    const VEHICLE_INACTIVE  = 'inactive';
    const VEHICLE_INPROCESS = 'in_process';
    
    //CART VARS
    const CART_OWNER              = 'cart_owner';
    const CART              = 'cart';
    const COMPARISON        = 'comparison';
    
    //PAYMENTMETHODS
    const PM_USE_PAYU               = 1;
    const PM_FINANCE                = 2;
    const PM_RESERVE                = 3;
    const PM_GENERATE_CODE          = 4;
    const PM_CHANELS                = 5;
    
    //Financial interest
//    const FN_FINANCIAL_ID               = 12229;
    const FN_FINANCIAL_ID               = 12354;
    const FN_INTEREST                   = 3.0;
    const FN_CREDITPERCENT              = 1.04;
    const FN_CREDITPERCENT_INC          = 0.035;
    const FN_AVAILABLE_FINANCIAL        = "5,14,13,12,26";
    const FN_TAXLIMIT        = 649990;
    const FN_TAXLIMIT_TOP        = 1000000;
    
    //General site and desc
    const GN_SITENAME                   = "aLoFijo | Compra online | Colombia";
    const GN_SITENAMESHORT                   = "aLoFijo | ";
    const GN_DESCRIPTION                = "Somos un portal de compras online que permite comparar y recomendar productos de acuerdo a las necesidades o preferencias Entre nuestra oferta, destacan productos relacionados a la tecnología, a la educación, al cuidado personal, belleza y mascotas.";
    // const GN_DESCRIPTION                = "En alofijo.com puedes buscar y comparar equipos. Te recomendamos el ideal para ti y lo compras al mejor precio. ¡Ingresa ahora!";
    const GN_OUTSTANDING    = "Productos Destacados";
    const GN_DESCRIPTION_OUTSTANDING    = "Productos destacados, aquí encontraras las novedades que tenemos para ti.";
    const GN_FAVORITES      = "Favoritos";
    const GN_DESCRIPTION_FAVORITES      = "Éstos son los productos que mas te han llamado la atención y has marcado como favoritos";
    const GN_CART           = "Carrito de compras";
    const GN_DESCRIPTION_CART           = "Zona de pagos, verifica tu órden y completa el pago.";
    
    //PAYU VARS
    const PAYU_RESEND          = 'Verificar pago de la orden ';
    const PAYU_URL          = 'https://gateway.payulatam.com/ppp-web-gateway/';
    const PAYU_URL_S        = 'https://sandbox.gateway.payulatam.com/ppp-web-gateway/';
    const PAYU_MERCHANT        = '563485';
    const PAYU_MERCHANT_S        = '508029';
    const PAYU_ACOUNT        = '566035';
    const PAYU_ACOUNT_S        = '512321';
    const PAYU_KEY        = '213FV389v0d31mACBAo0CuqQjZ';
    const PAYU_KEY_S        = '4Vj8eK4rloUd272L48hsrarnUA';
    const PAYU_RESPONSE        =  'looking/chargeResponse/';
    const PAYU_CONFIRMATION        = 'looking/chargeConfirmation/';
    const PAYU_MODE        = 0; // 1 test 0 production
    const PAYU_PROCCESSMINCOST        = 2900;
    const PAYU_PROCCESSLIMIT        = 83100;
    const PAYU_PROCCESSPERCENTAGE        = 0.03; // over total transaction
    const PAYU_PROCCESSIVA        = 0.19;  //iva over process cost
    const PAYU_PROCCESSBASE        = 900;  //base costo in every transaction
    const PAYU_PROCCESSRETEFTE        = 0.015; //retefte over total transaction
    const PAYU_PROCCESSRETEICA        = 0.00414; //ica over total transaction
    
    //EPAYCO Key
    const EPAY_KEY         = '790abe7bd13cee07797e21a338745d8a'; //user public key to connect
    const EPAY_PKEY         = 'd73c1d7974c3ad3566139bba37e4bac784dc4187'; //user Pkey to connect
    const EPAY_ID         = 26589; //user id
    
    //Ingles Alofijo
    const ENGLISH_ID         = 12628; //product id
    
    //WEBSERVICE DATA
    const WS_URL        = 'http://162.248.54.84:8980/WebServicePuntoRedHTTPAutentication/services/PuntoRedWSService?wsdl';
    const WS_FUNC        = 'recarga';
    const WS_FUNC_2        = 'consulta_paquetigos';
    const WS_FUNC_3        = 'paquetigos';
    const WS_COMERCE        = '293488';
    const WS_SALESPOINT       = '144223';
    const WS_TERMINAL       = '198453';
    const WS_PASSWORD        = '120403';
    const WS_NAMESPACE        = 'http://ws.puntored.brainwinner.com';
    const WS_USER        = 'deronpre';
    const WS_USERPASSWD        = '1a2765212588de4a79718ab973c875d8';
    const WS_WEBUSERPASSWD        = 'Unicentro|Santa Helenita||l00k1ngruler|1512475';
    
    const ML_SUPPORTEMAIL   =   'john.jimenez@lookingforweb.com';
    const ML_OWNEREMAIL   =   'raul.donado@lookingforweb.com';
    const ML_LOGISTICSEMAIL   =   'sales@lookingforweb.com';
    const ML_OPERATIONSEMAIL   =   'ricardo.donado@lookingforweb.com';
    const ML_SUPPORTNAME   =   'Soporte Plataforma';
    const ML_INFOEMAIL   =   'info@lookingforweb.com';
    const ML_INFONAME   =   'alofijo.com';
    const ML_CEONAME   =   'Raúl';
    const ML_EPMNAME   =   'Equipo';
    const ML_COONAME   =   'Ricardo';
    const ML_STOCKSUBJECT   =   'Stock agotado';
    const ML_ERRORSUBJECT   =   'Error en la transacción';
    const ML_SUCCESSSUBJECT   =   'Transacción exitosa';
    const ML_RECOVERPASSWD   =   'Recuperar contraseña';
    const ML_WELCOME   =   'Bienvenido a alofijo.com';
    const ML_RESERVEDSUBJECT   =   'Reserva exitosa';
    const ML_CREDITREQUEST   =   'Solicitud de crédito';
    const ML_CODESUBJECT   =   'Solicitud código de descuento';
    const ML_NEWTRANSACTION   =   'Nueva compra registrada';
    const ML_REJECTEDSUBJECT   =   'Pago rechazado';
    const ML_PENDINGSUBJECT   =   'Transacción pendiente';
    const ML_CONTACTSUBJECT   =   'Contacto Web';
    const ML_FIRSTCHECKOUT   =   ' Porque queremos que tengas el celular que siempre has querido te obsequiamos…';
    const ML_SECONDCHECKOUT   =   ' Por ser uno de nuestros compradores te obsequiamos…';
    
    //INVIOCE
    const SDY_SINGINLIST   =   'g0TgC9mtzqFdY6wZzk7oEQ';
    const SDY_CONTACTLIST   =   'wMbvzrI892m49oeHE1nPJJQg';
    const SDY_NEWSLETTERLIST   =   'qimA763J24863swVos2Pu3Lg';
    const SDY_EDUCATIONLIST   =   'AMMfvBIsq3qaacmRTmp4EA';
    const SDY_SIGNIN10   =   'TZJY5ZIGB2TVOn3U6DZW1A';
    const SDY_MANUALINVOICE   =   'EcudpqXcCGebSQN892Du0IPA';
    const SDY_CREDITREQUEST   =   'ffyosiAtvpuKmDjHphYT763A';
    //INVIOCE
    const IN_SELLER_NAME         = "Looking For Web SAS";
    const IN_SELLER_NIT         = "900.929.208 - 8";
    const IN_SELLER_ADDRESS         = "Cr 15A No 124 - 75";
    const IN_SELLER_PHONE         = "317 8045157";
    const IN_SELLER_WEB         = "www.lookingforweb.com";
    const IN_SELLER_RES         = "Resolución DIAN No. 320001408798  de 2016/06/07";
    const IN_SELLER_RES3         = "Número de formulario 18762014140029 de 2019/04/23";
    const IN_SELLER_RES2         = "Desde 00001 hasta 10000";
    const IN_SELLER_DISC         = "Factura emitida por computador";
    const IN_ERASE         = "2";
    const IN_CANCELED         = "8";
    const IN_APPROVED         = "4";
    const IN_HASH         = "63166C2FB5205DEE74BFEDBB58EDF7BC";
    
    //IMAGE PATHS 
    const USERS_PATH = "images/users/";
    const GALLERY_PATH = "images/gallery/";
    const DEFAULT_USER_IMAGE = "default-avatar.png";
    //DOCUMENT PATH
    const DOCUMENT_PATH = "documents/";
    const DOCUMENT_EXTENSIONS = "xls|xlsx|csv|txt|jpg|png|gif";
    const IMAGES_EXTENSIONS = "jpg|png|gif";
    const DOCUMENT_SIZE = "10000";
    
    //CUPON DETAILLS
    const CUPON_REGISTER = "10";
    const CUPON_FIRST_CHECKOUT = "10";
    const CUPON_SECOND_CHECKOUT = "20";
    
    const CUPON_FILTER_BRAND = "brand";
    const CUPON_FILTER_CATS = "category";
    
    //WATSON
    const WSON_CONVID = "conversationId";
    const WSON_USER = "611626c3-c672-412d-9e5e-09b757c18d1c";
    const WSON_PASSWD = "uuyVX2Ubd18p";
    const WSON_WORKSPACE = "52355a44-5a24-45e2-9162-6b49ea90e2ae";
    const WSON_NEWINTERACTIONKEY = "iniciaNuevoChat";
    
}
?>