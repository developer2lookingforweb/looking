<?php

//namespace helpers;

class Offers
{
    private $ci;
    private $Offers;
    
    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->library('doctrine');
//        $this->Offers = $this->ci->doctrine->em->getRepository('models\\Offers');
    }
    
    public function getOfferData($product,$isModel =false,$return='cost'){
        $dql = "select o.id, o.name, o.description, o.discount, o.filter, o.filter_id from models\\Offers o "
                . "where o.end > CURRENT_DATE() "
                . "and o.status = '1'";
        $querySearch = $this->ci->doctrine->em->createQuery($dql);
        $offer = $querySearch->getResult();
        if (count($offer) > 0) {
            if ($offer[0]['filter'] !== "all" && $offer[0]['filter'] !== null) {
                if ($offer[0]['filter'] == "category") {
                    if($isModel){
                        if ($offer[0]['filter_id'] == $product->getCategory()->getId()) {
                            if($return=="cost"){
                                return round($product->getCost() - (($product->getCost() / 100) * $offer[0]['discount']));
                            }else{
                                return $offer[0]['id'];
                            }
                        }
                    }else{
                        if ($offer[0]['filter_id'] == $product->category) {
                            if($return=="cost"){
                                return round($product->cost - (($product->cost / 100) * $offer[0]['discount']));
                            }else{
                                return $offer[0]['id'];
                            }
                        }
                    }
                }
                if ($offer[0]['filter'] == "brand") {
                    if($isModel){
                        if ($offer[0]['filter_id'] == $product->getBrand()->getId()) {
                            if($return=="cost"){
                                return round($product->getCost() - (($product->getCost() / 100) * $offer[0]['discount']));
                            }else{
                                return $offer[0]['id'];
                            }
                        }
                    }else{
                        if ($offer[0]['filter_id'] == $product->provid) {
                            if($return=="cost"){
                                    return round($product->cost - (($product->cost / 100) * $offer[0]['discount']));
                            }else{
                                return $offer[0]['id'];
                            }
                        }
                    }
                }
            } else {
                if ($offer[0]['filter'] == "all") {
                    if($isModel){
                        if($return=="cost"){
    //                        var_dump($product->getCost());exit;
                            return round($product->getCost() - (($product->getCost() / 100) * $offer[0]['discount']));
                        }else{
                            return $offer[0]['id'];
                        }
                    }else{
                        if($return=="cost"){
                            return round($product->cost - (($product->cost / 100) * $offer[0]['discount']));
                        }else{
                            return $offer[0]['id'];
                        }
                    }
                }
            }
        } else {
            return 0;
        }
//        $favorites = $this->Offers->findBy(array("product"=>$product,'customer'=>$this->ci->session->userdata(AuthConstants::USER_ID)));
//        return (count($favorites)>0);
    }
    public function findOffer($product,$isModel =false){
        return $this->getOfferData($product, $isModel);
    }
    public function findOfferId($product,$isModel =false){
        return $this->getOfferData($product, $isModel,"id");
    }
    public function getCodeId($code){
        $repo = $this->ci->doctrine->em->getRepository('models\\Cupons');
        $cupon = $repo->findOneBy(array("code"=>$code));
        return $cupon->getId();
    }
    public function validateCode($code,$product){
        $dql = "select o.id, o.code, o.discount, o.filter, o.filter_id from models\\Cupons o "
                . "where o.end > CURRENT_DATE() "
                . "and o.code = '$code' ";
        $querySearch = $this->ci->doctrine->em->createQuery($dql);
        $discount = $querySearch->getResult();

        if (count($discount) > 0) {
            $dql = "select o.id,c.id from models\\Orders o "
                    . "JOIN o.cupon c "
                    . "where c.id = '".$discount[0]['id']."' ";
            $querySearch = $this->ci->doctrine->em->createQuery($dql);
            $exist = $querySearch->getResult();
//            var_dump($discount);
//            var_dump($exist);exit;
            if (count($exist) > 0) {
                return -1;
            }else{
                
                if ($discount[0]['filter'] !== "all" && $discount[0]['filter'] !== null) {
                    if ($discount[0]['filter'] == "category") {
                        if ($discount[0]['filter_id'] == $product->getCategory()->getId()) {
                            return round($product->getCost() - (($product->getCost() / 100) * $discount[0]['discount']));
                        }

                    }
                    if ($discount[0]['filter'] == "brand") {
                        if ($discount[0]['filter_id'] == $product->getBrand()->getId()) {
                            return round($product->getCost() - (($product->getCost() / 100) * $discount[0]['discount']));
                        }
                    }
                    if ($discount[0]['filter'] == "product") {
                        if ($discount[0]['filter_id'] == $product->getId()) {
                            return round($product->getCost() - (($product->getCost() / 100) * $discount[0]['discount']));
                        }
                    }
                } else {
                    if ($discount[0]['filter'] == "all") {
                        return round($product->getCost() - (($product->getCost() / 100) * $discount[0]['discount']));
                    }
                }
            }
        } else {
            return 0;
        }
    }
}
?>