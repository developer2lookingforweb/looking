<?php
class Breadcrumb
{
    private $routes;
    

    public function __construct ($routes)
    {
        $this->routes  = $routes;
     
    }

    
    public function addItem ($path,$link)
    {
        $items = array();
        $items['path'] = $path;
        $items['link'] = $link;

        array_unshift($this->routes,$items);
    }

    public function toXHTML ($id)
    {
        $return = "";
        
        $xhtmlSections = Soporte::abreTag("ul", "id='".$id."' class='breadcrumbs'");
       
        $xhtmlSections.= Soporte::abreTag("li");
        $xhtmlSections.= Soporte::creaTag("i","", "class='icon-home'");
        $xhtmlSections.= Soporte::abreTag("a", "href='".base_url()."'");
        $xhtmlSections.= lang("home");
        $xhtmlSections.= Soporte::cierraTag("a");        
//        $xhtmlSections.= Soporte::creaTag("i","", "class='icon-angle-right'");
        $xhtmlSections.= Soporte::cierraTag("li");
        
        $last = count($this->routes);
        $current =0;
        $href = site_url();
        $keys = array_keys($this->routes);
        foreach ($this->routes as $index => $items)
        {   
            $separator = ($index==$keys[0]) ? "" : "/" ;                
            if( is_array($items)){
                $href .= $separator.$items['link'];
                $items = $items['path'];
            }  else {
                $href .= $separator.$items;
            }            
            $current++;
//            echo "<li>".$current."->".$last;
            if($items != "index" && $items != "home" && is_numeric($items)===false){
                $xhtmlSections.= Soporte::abreTag("li");
                $xhtmlSections.= Soporte::creaTag("i","", "class='icon-angle-right'");
                if($current < $last){
                    $xhtmlSections.= Soporte::abreTag("a", "href='$href'");            
                    $xhtmlSections.= $items;
                    $xhtmlSections.= Soporte::cierraTag("a");
                }  else {                                         
                    $xhtmlSections.= Soporte::abreTag("i");            
                    $xhtmlSections.= $items;
                    $xhtmlSections.= Soporte::cierraTag("i");
                }
                $xhtmlSections.= Soporte::cierraTag("li");
            }            
        }


        $xhtmlSections.= Soporte::cierraTag("ul");
        
        $return = $xhtmlSections;

        return $return;
    }
}
?>