<?php

//namespace helpers;

class Audits
{
    private $ci;
    private $Orders;
    
    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->library('doctrine');
        $this->Orders = $this->ci->doctrine->em->getRepository('models\\Orders');
    }
    
    public function addSearchAudit($options,$customer){
        $audit = new models\Audits();
        $audit->setDate(new Datetime());
        $audit->setSearchOptions($options);
        $audit->setSearchUser($customer->getId());
        $order = $this->Orders->findBy(array('customer'=>$customer->getId()));
        if(count($order)>0){
            $audit->setBuyerUser(1);
        }  else {
            $audit->setBuyerUser(0);
        }
        if($this->ci->session->userdata(AuthConstants::NAMES)!== ""){
            $audit->setLoggedUser($this->ci->session->userdata(AuthConstants::USER_ID));
            $audit->setIsLogged(1);
        }  else {
            $audit->setIsLogged(0);
        }
        $this->ci->doctrine->em->persist($audit);
        $this->ci->doctrine->em->flush();
    }
}
?>