<?php
class Sendy
{
    private $list;
    

    public function __construct ($list)
    {
        $this->list  = $list;
     
    }

    
    public function subscribe ($user,$email)
    {
        $data = ["name"=>$user,"email"=>$email,"list"=>  $this->list];
        $curl = curl_init("https://lookingforweb.com/sendy/subscribe");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
    }

}
?>