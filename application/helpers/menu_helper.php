<?php
class Menu
{
    private $sections;
    private $selected;
    private $profile;
    private $image;

    public function __construct ($userdata,$menuSelected,$image)
    {
        $this->sections  = array();
        $this->selected  = $menuSelected;
        $this->profile  = $userdata;
        $this->image    = $image;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function anyadeItem ($section, $title, $url)
    {
        $items = array();

        if (isset ($this->sections[$section]) && is_array($this->sections[$section]))
        {
            $items = $this->sections[$section];
        }

        $items[$title] = $url;
        $this->sections[$section] = $items;
    }

    public function toXHTML ($id,$profileResume=true)
    {
        $return = "";
        
        $xhtmlSections = Soporte::abreTag("ul", "id='".$id."'");
        if($profileResume){
            $xhtmlSections.= Soporte::abreTag("li","class='profile-resume'");
            $xhtmlSections.= Soporte::abreTag("a", "href='".  site_url(array(lang("users"),lang("mydata")))."'");
            $xhtmlSections.= Soporte::abreTag("div","class='row'");            
            $xhtmlSections.= Soporte::abreTag("div","class='medium-5 large-5 columns medium-centered large-centered avatar'");            
            $xhtmlSections.= Soporte::creaTag("img","", "src='".$this->image."'");            
            $xhtmlSections.= Soporte::cierraTag("div");
            $xhtmlSections.= Soporte::creaTag("div",  $this->profile->name." ".$this->profile->lastName, "class='medium-12 large-12 columns center data-hide'");
            $xhtmlSections.= Soporte::creaTag("div",  $this->profile->email, "class='medium-12 large-12 columns center data-hide'");
            $xhtmlSections.= Soporte::creaTag("div",  $this->profile->profile, "class='medium-12 large-12 columns center data-hide'");
            $xhtmlSections.= Soporte::cierraTag("div");
            $xhtmlSections.= Soporte::cierraTag("a");
            $xhtmlSections.= Soporte::cierraTag("li");
        }
        foreach ($this->sections as $titleSeccion => $items)
        {
            $titleSeccion = explode("_", $titleSeccion);
            $menuSelected = ($this->selected == $titleSeccion[1])?"menu-selected":"";
            if (count($items) == 1){
                foreach ($items as $titleItem => $urlSection){
                    $xhtmlSections.= Soporte::abreTag("li");
                    $xhtmlSections.= Soporte::abreTag("a", "href='".$urlSection."' class='menu_".$titleSeccion[1]." ".$menuSelected."'");
                    $xhtmlSections.= Soporte::creaTag("i","", "class='foundicon-checkmark'");
                    $xhtmlSections.= $titleItem;
                    $xhtmlSections.= Soporte::creaTag("i","");
                    $xhtmlSections.= Soporte::creaTag("div","", "class='arrow-selected single'");
                    $xhtmlSections.= Soporte::cierraTag("a");
                    $xhtmlSections.= Soporte::cierraTag("li");
                    break;
                }
                continue;
            }
            $xhtmlSections.= Soporte::abreTag("li");
            $xhtmlSections.= Soporte::abreTag("a", "href='#' class='menu_".$titleSeccion[1]." ".$menuSelected."'");
            $xhtmlSections.= Soporte::creaTag("i","", "class='foundicon-checkmark'");
            $xhtmlSections.= $titleSeccion[0];
            $xhtmlSections.= Soporte::creaTag("i","", "class='arrow-menu icon-angle-left'");
            $xhtmlSections.= Soporte::creaTag("div","", "class='arrow-selected'");
            $xhtmlSections.= Soporte::cierraTag("a");
            
            $xhtmlSections.= Soporte::abreTag("ul", "class='submenu'");
            foreach ($items as $titleItem => $urlSection)
            {
            	$xhtmlSections.= Soporte::abreTag("li");
            	$xhtmlSections.= Soporte::creaTag("a", $titleItem, "href='".$urlSection."' class='menu_".$titleSeccion[1]."'");
            	$xhtmlSections.= Soporte::cierraTag("li");
            }
            $xhtmlSections.= Soporte::cierraTag("ul");
            
            $xhtmlSections.= Soporte::cierraTag("li");
        }

        $xhtmlSections.= Soporte::abreTag("li");
        $xhtmlSections.= Soporte::abreTag("a", "href='".base_url()."login/logout'");
        $xhtmlSections.= Soporte::creaTag("i","", "class='foundicon-checkmark'");
        $xhtmlSections.= Soporte::creaTag("span", lang("logout"));
        $xhtmlSections.= Soporte::cierraTag("a");
        $xhtmlSections.= Soporte::abreTag("ul");
        $xhtmlSections.= Soporte::cierraTag("ul");
        $xhtmlSections.= Soporte::cierraTag("li");

        $xhtmlSections.= Soporte::cierraTag("ul");
        
        $return = Soporte::creaTag("div", $xhtmlSections,"class='span2 sidebar-nav side_nav' id='side_nav'");

        return $return;
    }
}
?>