<?php
function print_form($action, array $fields = array(), array $hiddens = array(), $id = "form", $multipart = false, $columns = 6)
{
    $return = form_open($action, array("name"=>$id, "id"=>$id), $hiddens);
    
    if ($multipart){
       $return = form_open_multipart($action, array("name"=>$id, "id"=>$id), $hiddens);
    }
    
    $cont = 0;
    $return  .= Soporte::abreTag("div", "class='row'");         
    foreach($fields as $column => $field){        
        $innerColumn = 12/count($fields);        
        if(is_array($field)){            
            if(isset($field["config"]["large"])){
                $innerColumn = $field["config"]["large"];       
            }  
            unset($field["config"]);
            $return  .= Soporte::abreTag("div", "class='large-$innerColumn columns'");          
            foreach($field as $label => $aField){
                $return .= Soporte::abreTag("div","class='row collapse'");
                if(strpos($label, 'no-label') !== false){
                    $return .= Soporte::abreTag("div","class='large-12 columns'");
                    $return .= $aField;
                    $return .= Soporte::cierraTag("div");
                }else{
                    $return .= Soporte::abreTag("div","class='large-4 columns'");
                    $return .= Soporte::creaTag("span", $label, "class='label'");
                    $return .= Soporte::cierraTag("div");

                    $return .= Soporte::abreTag("div","class='large-8 columns'");
                    $return .= $aField;
                    $return .= Soporte::cierraTag("div");
                }

                $return .= Soporte::abreTag("div","class='row collapse'");
                $return .= Soporte::creaTag("div", "<br/>","class='large-10 columns'");
                $return .= Soporte::cierraTag("div");
                $cont++;
                if ($cont % 2 == 0){
                    $return .= Soporte::cierraTag("div");
                    $return .= Soporte::abreTag("div", "class='row'");
                }
                $return  .= Soporte::cierraTag("div");
            }
            $return  .= Soporte::cierraTag("div");
        }else{
            $return .= Soporte::abreTag("div","class='row collapse'");

            $return .= Soporte::abreTag("div","class='large-4 columns'");
            $return .= Soporte::creaTag("span", $column, "class='label'");
            $return .= Soporte::cierraTag("div");

            $return .= Soporte::abreTag("div","class='large-8 columns'");
            $return .= $field;
            $return .= Soporte::cierraTag("div");

            $return .= Soporte::cierraTag("div");

            $return .= Soporte::abreTag("div","class='row collapse'");
            $return .= Soporte::creaTag("div", "<br/>","class='large-10 columns'");
            $return .= Soporte::cierraTag("div");
            $cont++;
        }
    }
    $return  .= Soporte::cierraTag("div");
    
    $return .= Soporte::abreTag("div", "class='row collapse'");
    $return .= Soporte::creaTag("div", "&nbsp;", "class='medium-2 large-2 columns'");
    $return .= form_button(array('type'=>'submit', 'class'=>'medium-3 large-3 columns ', 'content'=>lang('send')));
    $return .= Soporte::creaTag("div", "&nbsp;", "class='medium-2 large-2 columns'");
    $return .= form_button(array('type'=>'reset', 'class'=>'medium-3 large-3 columns ', 'content'=>lang('clear')));
    $return .= Soporte::creaTag("div", "&nbsp;", "class='medium-2 large-4 columns hide-for-small'");
    $return .= Soporte::cierraTag("div");
    
    $suffix  = ($id != "form") ? "_".$id : '';
    $visible = (validation_errors() != "") ? '' : 'style="display: none;"';

    $return .= '<div data-alert id="alert'.$suffix.'" class="alert-box" '.$visible.'>';
    $return .= '<div id="message'.$suffix.'">'.validation_errors().'</div>';
    $return .= '<a href="#" class="close">&times;</a>';
    $return .=  '</div>';
    $return .= form_close();
    
    $return = Soporte::creaTag("div", $return,  "class='large-".$columns." columns large-centered'");
    
    return $return;
}

function print_form_columns($action, array $fields = array(), array $hiddens = array(), $id = "form")
{
    $return = form_open($action, array("name"=>$id, "id"=>$id), $hiddens);
    
    $index = 0;
    $return  .= Soporte::abreTag("div", "class='row'");
    foreach($fields as $label => $field){
        $return .= Soporte::abreTag("div","class='large-2 columns'");
        $return .= Soporte::creaTag("span", $label, "class='prefix'");
        $return .= Soporte::cierraTag("div");

        $return .= Soporte::abreTag("div","class='large-4 columns'");
        $return .= $field;
        $return .= Soporte::cierraTag("div");
        
        $index++;
        if ($index % 2 == 0){
            $return .= Soporte::cierraTag("div");
            $return .= Soporte::abreTag("div", "class='row'");
        }
    }
    $return .= Soporte::cierraTag("div");
    
    $return .= Soporte::abreTag("div", "class='row collapse'");
    $return .= Soporte::creaTag("div", "&nbsp;", "class='large-2 columns hide-for-small'");
    $return .= form_button(array('type'=>'submit', 'class'=>'large-3 columns button small', 'content'=>lang('send')));
    $return .= Soporte::creaTag("div", "&nbsp;", "class='large-2 columns hide-for-small'");
    $return .= form_button(array('type'=>'reset', 'class'=>'large-3 columns button small', 'content'=>lang('clear')));
    $return .= Soporte::creaTag("div", "&nbsp;", "class='large-4 columns hide-for-small'");
    $return .= Soporte::cierraTag("div");
    
    $suffix  = ($id != "form") ? "_".$id : '';
    $visible = (validation_errors() != "") ? '' : 'style="display: none;"';

    $return .= '<div data-alert id="alert'.$suffix.'" class="alert-box" '.$visible.'>';
    $return .= '<div id="message'.$suffix.'">'.validation_errors().'</div>';
    $return .= '<a href="#" class="close">&times;</a>';
    $return .=  '</div>';
    $return .= form_close();
    
    $return = Soporte::creaTag("div", $return,  "class='large-12 columns large-centered'");
    
    return $return;
}

function my_form_upload($data = '',$value = '',$extra = ''){
    if(is_array($data)){
        if(!isset($data["id"]))$data["id"]=$data["name"];
        if(!isset($data["class"]))$data["class"]="";
        if(!isset($data["value"]))$data["value"]=$value;
    }
    if ( ! is_array($data))
    {
            $data = array('name' => $data, 'id' => $data);
    }
    
    $return = Soporte::abreTag("div","class='fileWrapper'")
             .form_upload(array('name'=>$data["name"],'id'=>$data["id"], 'class'=>$data["class"]." niceFileInput", 'value'=>$data["value"]),$data["value"],$extra)
             .Soporte::creaTag('span', $data["value"], 'class="text"')
             .Soporte::cierraTag('div');
     return $return;
}