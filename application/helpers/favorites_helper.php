<?php

//namespace helpers;

class Favorites
{
    private $ci;
    private $Favorites;
    
    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->library('doctrine');
        $this->Favorites = $this->ci->doctrine->em->getRepository('models\\Favorites');
    }
    
    public function isFavorite($product){
        $favorites = $this->Favorites->findBy(array("product"=>$product,'customer'=>$this->ci->session->userdata(AuthConstants::USER_ID)));
        return (count($favorites)>0);
    }
}
?>