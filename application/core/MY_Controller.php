<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\Common\Collections\Criteria;

abstract class MY_Controller extends MX_Controller
{
    protected $em;
    protected $model;
    protected $relations = array();
    protected $actions;
    public    $breadcrumb;
    abstract function setListParameters();
    

    public function __construct()
    {
        parent::__construct();        
        $this->em = $this->doctrine->em;
        
        $lang = ($this->session->userdata(AuthConstants::LANG)) ? $this->session->userdata(AuthConstants::LANG) : $this->config->config["language"];
        
        $this->lang->load('general', $lang);
        $this->lang->load('menu', $lang);
        $this->lang->load('controller', $lang);
        $this->setListParameters();
        
        $this->load->library('REST', array(  
            'http_user' => $this->config->config["http_user_api"],
            'http_pass' => $this->config->config["http_pass_api"],
            'http_auth' => $this->config->config["http_auth_api"],
            'server' =>  $this->config->config["server_api"],
        ));
        $this->breadcrumb = new Breadcrumb($this->router->uri->segments);
    }
    
    public function viewLogin($view, $data)
    {
        $model = array();
        $model["header"]    = $this->load->view("admin_header", array("login"=>true), true);
        $model["footer"]    = "";
        $model["login"]     = true;
        $model["body"]      = $this->load->view($view, $data, true);
        $model["JS"]        = $this->getJS($view, $data);
        $model["css"]        = $this->getCss($view, $data);
        $this->load->view("admin_layout", $model);
    }
    public function viewWebLogin($view, $data)
    {
        $model = array();
        $model["header"]    = "";
        $model["footer"]    = $this->load->view("public_web_footer", "", true);
        $model["login"]     = true;
        $model["body"]      = $this->load->view($view, $data, true);
        $model["JS"]        = $this->getJS($view, $data);
        $model["css"]        = $this->getCss($view, $data);
        $this->load->view("public_web_layout", $model);
    }

    public function view($view, $data, array $actions = array())
    {
        $this->load->helper('directory');
        $this->loadRepository("Permissions");
        
        $class      = strtolower($this->router->class);
        $method     = strtolower($this->router->method);
        $url        = $class."/".$method;
        $permission = $this->Permissions->findOneBy(array("url"=>$url));
        
        $section = array();
        $section["content"]    = $this->load->view($view, $data, true);
        $section["title"]      = $data["title"];
        $section["actions"]    = array();
        
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns'>".$this->load->view("admin_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = $this->getMenu();
            $body["actions"]    = $actions;
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns'>".$this->load->view("admin_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = $this->getMenu();
            
            $this->load->helper("permissions");
            foreach ($actions as $label => $route){
//                $info = explode("/", $route);
//                
//                $keys = array_keys($info, "");
//                foreach($keys as $aKey){
//                    array_splice($info, $aKey, 1);
//                }

                $url = str_replace(base_url(), "", $route);
//                foreach($keys as $aKey){
//                    array_splice($info, $aKey, 1);
//                }
                
//                $class = $info[0];
//                $method = (key_exists(1, $info)) ? $info[1] : "index";
                $url = explode("/", $url);
                $class = $url[0];
                $method = $url[1];
//                var_dump($class);exit;
                 if (hasRights($method, $class)){
                    $body["actions"][$label] = $route;
                 }
            }
        }
        

        $breadcrumb = $this->breadcrumb->toXHTML("breadcrumb");
        $body["breadcrumb"]= $breadcrumb;
        
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);

        $model = array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("admin_body", $body, true);
            $model["header"]    = $this->load->view("admin_header", $header, true);
            $model["footer"]    = $this->load->view("admin_footer", null, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("admin_layout", $model);
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("admin_body", $body, true);
            $model["header"]    = $this->load->view("admin_header", $header, true);
            $model["footer"]    = $this->load->view("admin_footer", null, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("admin_layout", $model);
        }
    }
    public function viewPublic($view, $data, array $actions = array())
    {
        $this->load->helper('directory');
        $this->loadRepository("Permissions");
        
        $class      = strtolower($this->router->class);
        $method     = strtolower($this->router->method);
        $url        = $class."/".$method;
        $permission = $this->Permissions->findOneBy(array("url"=>$url));
        
        $section = array();
        $section["content"]    = $this->load->view($view, $data, true);
        $section["title"]      = $data["title"];
        $section["actions"]    = array();
        
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns'>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            $body["actions"]    = $actions;
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns'>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            
            $this->load->helper("permissions");
            foreach ($actions as $label => $route){
                $info = explode("/", $route);
                
                $keys = array_keys($info, "");
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }

                $url = str_replace("/", "", base_url());
                $keys = array_keys($info, $url);
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }
                
                $class = $info[0];
                $method = (key_exists(1, $info)) ? $info[1] : "index";
                
                 if (hasRights($method, $class)){
                    $body["actions"][$label] = $route;
                 }
            }
        }
        

        $breadcrumb = $this->breadcrumb->toXHTML("breadcrumb");
        $body["breadcrumb"]= $breadcrumb;
        
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);

        $model = array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("public_body", $body, true);
            $model["header"]    = $this->load->view("public_header", $header, true);
            $model["footer"]    = $this->load->view("public_footer", null, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("public_layout", $model);
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("public_body", $body, true);
            $model["header"]    = $this->load->view("public_header", $header, true);
            $model["footer"]    = $this->load->view("public_footer", null, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("public_layout", $model);
        }
    }
    public function viewWebPublic($view, $data, array $actions = array())
    {
        $this->load->helper('directory');
        $this->loadRepository("Permissions");
        
        $class      = strtolower($this->router->class);
        $method     = strtolower($this->router->method);
        $url        = $class."/".$method;
        $permission = $this->Permissions->findOneBy(array("url"=>$url));
        
        $section = array();
        $section["content"]    = $this->load->view($view, $data, true);
        $section["title"]      = $data["title"];
        $section["actions"]    = array();
        
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            $body["actions"]    = $actions;
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            
            $this->load->helper("permissions");
            foreach ($actions as $label => $route){
                $info = explode("/", $route);
                
                $keys = array_keys($info, "");
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }

                $url = str_replace("/", "", base_url());
                $keys = array_keys($info, $url);
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }
                
                $class = $info[0];
                $method = (key_exists(1, $info)) ? $info[1] : "index";
                
//                 if (hasRights($method, $class)){
                    $body["actions"][$label] = $route;
//                 }
            }
        }
        

        $breadcrumb = $this->breadcrumb->toXHTML("breadcrumb");
        $body["breadcrumb"]= $breadcrumb;
        
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);

        $model = array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("public_body", $body, true);
            $model["header"]    = $this->load->view("public_header", $header, true);
            $model["footer"]    = $this->load->view("public_footer", null, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("public_layout", $model);
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("public_web_body", $body, true);
            $model["header"]    = $this->load->view("public_web_header", $header, true);
            $model["footer"]    = $this->load->view("public_web_footer", $header, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("public_web_layout", $model);
        }
    }
    public function viewFrontend($view, $data, array $actions = array())
    {
        $this->load->helper('directory');
        $this->loadRepository("Permissions");
        
        $class      = strtolower($this->router->class);
        $method     = strtolower($this->router->method);
        $url        = $class."/".$method;
        $permission = $this->Permissions->findOneBy(array("url"=>$url));
        
        $section = array();
        $section["content"]    = $this->load->view($view, $data, true);
        $section["title"]      = $data["title"];
        $section["actions"]    = array();
        
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
            $body= array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            $body["actions"]    = $actions;
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            
            $this->load->helper("permissions");
            foreach ($actions as $label => $route){
                $info = explode("/", $route);
                
                $keys = array_keys($info, "");
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }

                $url = str_replace("/", "", base_url());
                $keys = array_keys($info, $url);
                foreach($keys as $aKey){
                    array_splice($info, $aKey, 1);
                }
                
                $class = $info[0];
                $method = (key_exists(1, $info)) ? $info[1] : "index";
                
//                 if (hasRights($method, $class)){
                    $body["actions"][$label] = $route;
//                 }
            }
        }
        

        $breadcrumb = $this->breadcrumb->toXHTML("breadcrumb");
        $body["breadcrumb"]= $breadcrumb;
        
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);

        $model = array();
        
            $model["body"]      = $this->load->view("frontend_body", $body, true);
            $model["header"]    = $this->load->view("frontend_header", $header, true);
            $model["footer"]    = $this->load->view("frontend_footer", $header, true);
            $model["JS"]        = $this->getJS($view, $data);
            $model["css"]        = $this->getCss($view, $data);
            $this->load->view("frontend_layout", $model);
    }
    public function viewFrontend2($view, $data, array $actions = array())
    {
        
        $section = array();
        $section["content"]    = $this->load->view($view, $data, true);
        $section["title"]      = $data["title"];
        $section["actions"]    = array();
        
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
            $body= array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            $body["actions"]    = $actions;
        }else if ($profile != AuthConstants::ID_PROFILE_ADMIN){
            $body= array();
            $body["sections"]   = "<div class='large-12 columns '>".$this->load->view("public_section", $section, true)."</div>";
            $body["title_menu"] = $data["title"];
            $body["csrf"]       = $this->config->config["csrf_token_name"];
            $body["menu"]       = array();
            
//           
        }
        

       
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);
        $model = array();
        
        $model["body"]      = $this->load->view("frontend_2_body", $body, true);
        $model["header"]    = $this->load->view("frontend_2_header", $header, true);
        $model["footer"]    = $this->load->view("frontend_2_footer", $header, true);
        $model["JS"]        = $this->getJS($view, $data);
        $model["css"]        = $this->getCss($view, $data);
        
        $this->load->view("frontend_2_layout", $model);
    }
    public function viewGlobal($view, $data)
    {
        $this->load->helper('directory');
       
        
        $model["title"]      = $data["title"];;
        $model["content"]    = $this->load->view($view, $data, true);
        $model["JS"]        = $this->getJS($view, $data);
        $model["css"]        = $this->getCss($view, $data);
        $this->load->view("global_layout", $model);
    }

    public function viewSections($views, array $actions = array(),$tabs = false)
    {
        $this->load->helper('directory');
        $this->loadRepository("Permissions");
        
        $class      = strtolower($this->router->class);
        $method     = strtolower($this->router->method);
        $url        = $class."/".$method;
        $permission = $this->Permissions->findOneBy(array("url"=>$url));
        
        $sections   = "";
        $js         = "";
        $titleMenu  = "";
        
        $cols = 0;
        
        foreach ($views as $view => $dataSection) {
            $span = (isset($dataSection["span"])) ? $dataSection["span"] : 12;
            $cols += $span;
            
            if ($titleMenu == "" && key_exists("title", $dataSection)){
                $titleMenu = $dataSection["title"];
            }
            
            if (isset($dataSection["actions"]) == false){
                $dataSection["actions"] = array();
            }
            
            $dataSection["csrf"]   = $this->config->config["csrf_token_name"];
            
            $section = array();
            $section["content"] = $this->load->view($view, $dataSection, true);
            $section["title"]   = $dataSection["title"];
            $section["span"]    = $span;
            $section["box"]     = true;
            if($tabs){
                $tabData["groupTabs"][] = $section["title"];
                $tabData["groupSections"][] = $section;                
                $js         .= $this->getJS($view, $dataSection);
            }else{                
                $sections   .= $this->load->view("admin_section", $section, true);
                $js         .= $this->getJS($view, $dataSection);
            }
            if ($cols % 12 == 0){
                $sections .= "</div>";
                $sections .= "<div class='row'>";
            }
            
        }
        if($tabs){            
            $sections   = $this->load->view("admin_tabs", $tabData, true);
        }
        $profile = $this->session->userdata(AuthConstants::PROFILE);
        
        $body= array();
        $body["sections"]   = $sections;
        $body["title"]      = $titleMenu;
        $body["csrf"]       = $this->config->config["csrf_token_name"];
        $body["actions"]    = $actions;
        
        $body["menu"]   = $this->getMenu();
        
        $breadcrumb = $this->breadcrumb->toXHTML("breadcrumb");
        $body["breadcrumb"]= $breadcrumb;
        
        $header = array();
        $header["full_name"] = $this->session->userdata(AuthConstants::NAMES) ." ". $this->session->userdata(AuthConstants::LAST_NAMES);

        $model = array();
        
        if ($profile  == AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("admin_body", $body, true);
            $model["header"]    = $this->load->view("admin_header", $header, true);
            $model["footer"]    = $this->load->view("admin_footer", null, true);
            $model["JS"]        = $js;
            $model["css"]        = $this->getCss($view, $dataSection);
            $this->load->view("admin_layout", $model);
        }else if ($profile  != AuthConstants::ID_PROFILE_ADMIN){
            $model["body"]      = $this->load->view("admin_body", $body, true);
            $model["header"]    = $this->load->view("admin_header", $header, true);
            $model["footer"]    = $this->load->view("admin_footer", null, true);
            $model["JS"]        = $js;
            $model["css"]        = $this->getCss($view, $dataSection);
            $this->load->view("admin_layout", $model);
        }
    }

    public function getMenu($id = "menu")
    {
        $menuSession = false;
        $return = "";
                
        $this->loadRepository("Permissions");
        $this->loadRepository("Sections");
        
        $userData['profile']    = $this->session->userdata(AuthConstants::PROFILE_NAME);
        $userData['email']      = $this->session->userdata(AuthConstants::EMAIL);
        $userData['name']       = $this->session->userdata(AuthConstants::NAMES);
        $userData['lastName']   = $this->session->userdata(AuthConstants::LAST_NAMES);
        
        $menuSelected = $this->session->userdata(AuthConstants::MENU_SELECTED);
        $profileImage= site_url(AuthConstants::USERS_PATH."thumb_".$this->session->userdata(AuthConstants::USER_IMAGE));
        $menu = new Menu((object) $userData,$menuSelected,$profileImage);
        $admin = $this->session->userdata(AuthConstants::ADMIN);
        $sections = $this->Sections->findBy(array(), array('position' => 'ASC'));
        
        foreach ($sections as $aSection) {
            if ($admin == AuthConstants::ADMIN_KO){
                $idProfile = $this->session->userdata(AuthConstants::PROFILE);
                $profile = $this->em->find('models\Profiles', $idProfile);
                $allPermissions = $profile->getPermissions();
                $allPermissions->setInitialized(true);

                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq("in_menu", AuthConstants::YES))
                    ->andWhere(Criteria::expr()->eq("section", $aSection))
                    ->orderBy(array("position" => "ASC"));

                $permissions = $allPermissions->matching($criteria);
            }
            
            if ($admin == AuthConstants::ADMIN_OK){
                $permissions   = $this->Permissions->findBy(array("in_menu"=>AuthConstants::YES, "section"=>$aSection->getId()), array('position' => 'ASC'));
            }
            foreach ($permissions as $aPermission) {
                $preUrl = explode("/", $aPermission->getUrl());
                $postUrl = array();
                foreach ($preUrl as $value) {
                    if($value !="index")
                        $postUrl[] = ($value);
                }
                $menu->anyadeItem(lang($aSection->getLabel())."_".$aSection->getId(), lang($aPermission->getLabel()), site_url($postUrl));
            }
        }
        
        $return = $menu->toXHTML($id);
        
        return $return;
    }
    
    public function loadRepository($repository){
        if (isset($this->$repository) == false){
            $this->$repository = $this->em->getRepository('models\\'.$repository);
        }
    }
    public function parseString($string){
        $restrict= array ("(",")","+","?","!","*"," ","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $convert=  array ("","","","","","","_","a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $text = strtolower(str_replace($restrict, $convert ,$string));
        return $text; 
    }
    public function getList()
    {
        $this->load->helper('permissions');
        $getData = $this->input->get();

        $arrayData  = $this->datatable->getData($getData, $this->model, $this->relations);
        $registers  = array();

        foreach ($arrayData['aaData'] as $register) {

            $actions = "";
            
            foreach ($this->actions as $aAction){
                if (hasRights($aAction->getMethod(), $aAction->getClass())){
                    $url = $aAction->getUrl();
                    $urlStr = "#";
                    
//                        $urlStr = site_url($aAction->getClass() . "/" . $aAction->getMethod()). "/" . $register[0];
                        if($aAction->getMethod()==lang("edit"))$urlStr = site_url( $aAction->getClass()."/". $register[0]."/".$this->parseString($register[1]));
                        else {
                            $dependency = $aAction->getDependency();
                            if(isset($dependency['column'])){

                                if(is_array($dependency['validator'])){
                                    $e = array();
                                    foreach($dependency['validator'] as $idx => $validation){
                                        $e[] = "('".$register[$dependency['column'][$idx]]."'".$dependency['validator'][$idx]."'".$dependency['validation'][$idx]."')";
                                    }
                                    $e = implode($dependency['operator'], $e);
                                    $e = "if(".$e."){ return true;}else{return false;}";
                                    $d= eval(" $e;");
                                    if($d == true){
                                        $urlStr = site_url($aAction->getClass() . "/" . $aAction->getMethod()). "/" . $register[0]."/".$this->parseString($register[1]);
                                    }else{
                                        $urlStr = "";
                                    }
                                }else{
                                    $e = "if('".$register[$dependency['column']]."'".$dependency['validator']."'".$dependency['validation']."'){ return true;}else{return false;}";
                                    $d= eval(" $e;");
                                    if($d == true){
                                        $urlStr = site_url($aAction->getClass() . "/" . $aAction->getMethod()). "/" . $register[0]."/".$this->parseString($register[1]);
                                    }else{
                                        $urlStr = "";
                                    }
                                }

//                                }
//                                if($dependency['validator']=="!="){
//                                    if($register[$dependency['column']] !== $dependency['validation']){
//                                        $urlStr = site_url($aAction->getClass() . "/" . $aAction->getMethod()). "/" . $register[0]."/".$this->parseString($register[1]);
//                                    }else{
//                                        $urlStr = "";
//                                    }
//                                }
                            }else{
                                $urlStr = site_url($aAction->getClass() . "/" . $aAction->getMethod()). "/" . $register[0]."/".$this->parseString($register[1]);
                            }
                        }
                    
                    $trans      = (Soporte::cadenaVacia(lang($aAction->getLabel())) == false) ? lang($aAction->getLabel()) : lang(ucfirst($aAction->getLabel()));
                    $label      = "title='" . $trans . "'";
                    $class      = "class='action_list action_" . $aAction->getLabel() . "'";
                    $id         = "id='".$register[0]."'";
                    $parameters = $label . " " . $class . " " . $id;
                    if($urlStr !== ""){
                        if ($url){
                            $actions   .= Soporte::creaEnlaceTexto("", $urlStr, $parameters);
                        }else{
                            $actions   .= Soporte::creaEnlaceTexto("", "#", $parameters);
                        }
                    }
                }
            }
            
            $cont = 0;
            foreach ($register as $aRegister){
                if ($aRegister instanceof DateTime){
                    $register[$cont] = $aRegister->format("Y-m-d");
                }
                $cont++;
            }
            
            $register[]  = $actions;
            $registers[] = $register;
        }

        $arrayData['aaData'] = $registers;
        echo json_encode($arrayData);
    }
    
    protected function getJS ($view, $data)
    {
        $return = "";
        
        $DS     = DIRECTORY_SEPARATOR;
        $module = $this->router->module;
        $pathJS = APPPATH .$DS. "modules" .$DS. $module .$DS. "views" .  $DS . "js" . $DS . $view . ".php";
        
        if (file_exists($pathJS)){
            $return = $this->load->view("js/".$view, $data, true);
        }
        
        return $return;
    }
    
    protected function getCss ($view, $data)
    {
        $return = "";
        
        $DS     = DIRECTORY_SEPARATOR;
        $module = $this->router->module;
        $pathJS = APPPATH .$DS. "modules" .$DS. $module .$DS. "views" .  $DS . "css" . $DS . $view . ".php";
        
        if (file_exists($pathJS)){
            $return = $this->load->view("css/".$view, $data, true);
        }
        
        return $return;
    }
    
    /**
     * Upload a file 
     */
    public function uploadFile($file, $config, $oldFile = "", $required = true)
    {
        $status = false;
        $error = "";
        $data = "";
        
        $this->load->library('upload');
        
        $existFile = (empty($_FILES[$file]['name']) == false);
        $edition = ($oldFile != "");
        
        if ($existFile){
            $this->upload->initialize($config);
            if ($this->upload->do_upload($file)) {
                $status = true;
                $error = "";
                $data = $this->upload->data();

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }
            } else {
                $status = false;
                $error = $this->upload->display_errors();
                $data = "";
            }
        }else{
            $data = "";
            $error = "";
            
            if ($edition || $required == false){
                $status = true;
            }else{
                $status = false;
                $error = lang($file."_required");
            }
        }
        
        $return = array();
        $return["status"] = $status;
        $return["error"] = $error;
        $return["data"] = $data;
        return $return;
    }
}
