<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>alofijo.com te presenta Intelliging</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="css/new-age.css" rel="stylesheet">
    <!-- Google Analytics Code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80994196-1', 'auto');
        ga('send', 'pageview');

        </script>
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/logo.png" class="img-fluid" alt="alofijo.com"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#download">Lo quiero</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#features">Características</a>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>

    <section class="download bg-primary text-center" id="download">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mx-auto section-heading">
            <h2 class="section-heading">Descubre lo sencillo que es!</h2>
            <p>Por sólo <strike>$1380 mil / año</strike>, precio de lanzamiento <strong>$690 mil / año</strong> y logra tus objetivos hasta en 3 meses! (Cupos Limitados)</p>
            <!-- <p>Aprovecha nuestra precio especial de bienvenida con el 50% de descuento!</p> -->
            <div class="badges row">
              <div class="col-md-5 relative align-self-center">
            
				<form action="#" class="bg-white rounded pb_form_v1" _lpchecked="1">
				  <h2 class="mb-4 mt-0 text-center">Inicia ahora</h2>
				  <div class="form-group">
					<input type="text" name="name" class="form-control pb_height-50 reverse" placeholder="Nombre" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
				  </div>
				  <div class="form-group">
					<input type="text" name="last_name" class="form-control pb_height-50 reverse" placeholder="Apellidos">
				  </div>
				  <div class="form-group">
					<input type="text"  name="email" class="form-control pb_height-50 reverse" placeholder="Correo">
				  </div>
				  <div class="form-group">
            <select  name="product" class="form-control pb_height-50 reverse" placeholder="Producto">
              <option value="12628">Standard</option>
              <option value="12643">Executive</option>
            </select>
				  </div>
				  
				  <div class="form-group">
					<a id="register" class="btn btn-outline btn-xl js-scroll-trigger">Quiero aprender!</a>
				  </div>
				</form>

        </div>
        <div class="col-lg-7 my-auto">
            <div class="">
				<iframe id="video-frame"  src="https://www.youtube.com/embed/JVeu7Z85Aig?autoplay=1&loop=1&rel=0">
				</iframe>
              
            </div>
          </div>
			  
            </div>
          </div>
        </div>
      </div>
    </section>

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-5 my-auto">
            <div class="header-content mx-auto">
              <h1 class="mb-5">Te presentamos la forma natural de aprender inglés alofijo, basado en técnicas de neurolinguística!</h1>
              <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">Inicia ahora!</a>
            </div>
          </div>
          <div class="col-md-7 align-self-center">
				<img src="img/grupo-joven.jpg" class="img-fluid" alt="">
			  </div>
        </div>
      </div>
    </header>

    <section class="features" id="features">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Características</h2>
          <p class="text-muted">Así de fácil es aprender!</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-4 my-auto">
            <div class="device-container">
              <div class="device-mockup iphone6_plus portrait white">
                <div class="device">
                  <div class="screen">
                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                    <img src="img/demo-screen-1.jpg" class="img-fluid" alt="">
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-organization text-primary"></i>
                    <h3>Lógica Diagramática</h3>
                    <p class="text-muted">Formas, colores, acoples y pictogramas que se graban en tu memoria!</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-paper-plane text-primary"></i>
                    <h3>Vocabulario Acelerado</h3>
                    <p class="text-muted">Aprende más de 120 palabras nuevas por hora gracias al método de neuroasociación!</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-bell text-primary"></i>
                    <h3>Sensibilidad Auditiva</h3>
                    <p class="text-muted">Logra conciencia fonética haciendo visibles los sonidos del inglés!</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-trophy text-primary"></i>
                    <h3>Agilidad Verbal</h3>
                    <p class="text-muted">Aprende mejor y más rápido escuchando tu propia voz!</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="cta">
      <div class="cta-content">
      <div class="container">
	<div class="row">
		<div class="hidden-xs hidden-sm col-md-2 col-lg-2">&nbsp;
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-pull-2 col-lg-4 col-lg-pull-2">
          <ul class="list-group">
            <li class="list-group-item text-center"><h3>Standard</h3></li>
            <li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Ingreso ilimitado</li>
            <li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Repetir sesiones</li>
            <li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Repetir videos</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Tutorías profesionales</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> <i class="icon-close"></i></li>
				<li class="list-group-item text-center"><h4 class="h2">$690.000 <small>/año</small></h4></li>
				<li class="list-group-item text-center"><a href="#download" class="btn btn-outline btn-xl js-scroll-trigger  b-standard">Inicia ahora!</a></li>
			</ul>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-md-pull-2 col-lg-4 col-lg-pull-2">
			<ul class="list-group">
				<li class="list-group-item text-center "><h3>Executive</h3></li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Ingreso ilimitado</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Repetir sesiones</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Repetir videos</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> Tutorías profesionales</li>
				<li class="list-group-item text-center"><span class="glyphicon glyphicon-ok text-success" aria-hidden="true"></span> 4 Tutorías precenciales</li>
				<li class="list-group-item text-center"><h4 class="h2">$1440,000 <small>/año</small></h4></li>
				<li class="list-group-item text-center"><a href="#download" class="btn btn-outline btn-xl js-scroll-trigger b-executive">Inicia ahora!</a></li>
			</ul>
		</div>
    <div class="hidden-xs hidden-sm col-md-2 col-lg-2">&nbsp;
        </div>
		
	</div>
</div>
      </div>
      <div class="overlay"></div>
    </section>
	<!--
    <section class="contact bg-primary" id="contact">
      <div class="container">
        <h2>We
          <i class="fas fa-heart"></i>
          new friends!</h2>
        <ul class="list-inline list-social">
          <li class="list-inline-item social-twitter">
            <a href="#">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item social-facebook">
            <a href="#">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="list-inline-item social-google-plus">
            <a href="#">
              <i class="fab fa-google-plus-g"></i>
            </a>
          </li>
        </ul>
      </div>
    </section> -->

    <footer>
      <div class="container">
        <p>&copy; alofijo.com 2018. Todos los derechos reservados.</p>
        <!--<ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Privacy</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Terms</a>
          </li>
          <li class="list-inline-item">
            <a href="#">FAQ</a>
          </li>
        </ul>-->
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
	<script type="text/javascript" src="https://checkout.epayco.co/checkout.js">   </script>
    <script src="js/new-age.js"></script>
	<script>
		  
			$( document ).ready(function() {
				
			});
	</script>
  </body>

</html>
