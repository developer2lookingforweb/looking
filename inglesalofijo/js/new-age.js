var data = {};


(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  var handler = ePayco.checkout.configure({
    key: '790abe7bd13cee07797e21a338745d8a',
    test: false
  })

  data = {
      //Parametros compra (obligatorio)
      name: "Inglés alofijo",
      description: "Inglés alofijo",
      invoice: "O00007730032168451",
      currency: "cop",
      amount: "550000",
      tax_base: "0",
      tax: "0",
      country: "co",
      lang: "en",

      //Onpage="false" - Standard="true"
      external: "false",


      //Atributos opcionales
      extra1: "extra1",
      extra2: "extra2",
      extra3: "extra3",
      confirmation: "http://secure2.payco.co/prueba_curl.php",
      response: "http://secure2.payco.co/prueba_curl.php",

      //Atributos cliente
      name_billing: "",
      address_billing: "",
      type_doc_billing: "cc",
      mobilephone_billing: "",
      number_doc_billing: ""
  }

 $(".b-executive").click(function(){
    $("select[name='product']").val(12643)
 });
 $(".b-standard").click(function(){
    $("select[name='product']").val(12628)
 });

  $("#register").click(function(){
    
    if(validate()){
      
      var submitdata = {
        "name": $("input[name='name']").val(),
                "last_name": $("input[name='last_name']").val(),
                "email": $("input[name='email']").val(),
                "product": $("select[name='product']").val()
            };
            $.ajax({
              url: "processPayment.php",
              type: "post",
              dataType: "json",
              data: submitdata,
              success: function (data) {
                if (data.status == true) {
                    data.name = "Inglés alofijo";
                    data.description = "Inglés alofijo";
                    data.amount = data.data.amount[1];
                    data.invoice = data.data.reference;
                    data.response = "http://localhost/looking/looking/epayco/" + data.data.reference;
                    data.confirmation = "http://localhost/looking/looking/epaycoc/" + data.data.reference;
                    handler.open(data)
                    $("#buyerEmail").val(data.data.email);
                    $("#reference").val(data.data.reference);
                    $("#pamount").val(data.data.amount[1]);
                    $("#signature").val(data.data.signature);
                    $("#response").val(data.data.response);
                    $("#confirmation").val(data.data.confirmation);
                    // $("#payu").submit();
                } else {
                }
            }
        });
    }
})

function validate(){
    $( ":input" ).removeClass("text-danger");
    var name      = $("input[name='name']").val();
    var last_name = $("input[name='last_name']").val();
    // var nit       = $("input[name='nit']").val();
    var email     = $("input[name='email']").val();
    var nameRequired = (name !== "");
    var lastNameRequired = (last_name !== "");
    // var nitRequired = (nit !== "");
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var num = /^\d{1,10}$/;
      re.test(String(email).toLowerCase());
    if(nameRequired && lastNameRequired && re.test(String(email).toLowerCase())){
        return true
    }
    if(! nameRequired){
        $("input[name='name']").addClass("text-danger");
        $("input[name='name']").focus();
    }
    if(! lastNameRequired){
        $("input[name='last_name']").addClass("text-danger");
        $("input[name='last_name']").focus();
    }
    // if(! nitRequired || ! num.test(String(nit))){
    //     $("input[name='nit']").addClass("text-danger");
    //     $("input[name='nit']").focus();
    // }
    if(! re.test(String(email).toLowerCase())){
        $("input[name='email']").addClass("text-danger");
        $("input[name='email']").focus();
    }
    return false;
}


})(jQuery); // End of use strict

