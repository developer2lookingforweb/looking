<div class="container-fluid"  style="" >
    <div class="row" >
        <div class="col-md-12">
            <h1 class="tit_19" ><br/>EDITA TU PERFIL <br/><p class="tit_20" >PUEDES ACTUALIZAR TU INFORMACION DE MANERA RAPIDA </p> </h1>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-4" >

        </div> 
        <div class="col-md-4" style='padding-right:0px!important;' >
            <div class="team-member modern" >
                <form class="form-horizontal" role="form" action="#" method="post" >
                    <a href="#"> 
                        <div class="member-photo imgevent" >
                            <img class='img-responsive center-block' style="width: 100px;" src="<?php echo base_url(); ?>/images/avatar.png" alt="" />
                            <div class="member-name color01">
                                <span id="update-profile">Actualizar</span> 
                            </div>
                        </div>    
                    </a>
                    <input type="hidden" id="cid" name="cid" value="0"/>
                    <div class="member-info">
                        <div >
                            <h2 class="tit_tarjeta_a" >Nombre <br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="text" id="name" name="name" class="form-control input_lin required"  placeholder="John">
                            </div> 							
                            <div class="col-md-6">
                                <input type="text" id="last_name" name="last_name" class="form-control input_lin required"  placeholder="Doe">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Email<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="email" name="email" class="form-control input_lin required"  placeholder="usuario@dominio.com">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Tipo de documento <br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select class="form-control tit_tarjeta_a required" id="docType" name="docType">
                                </select> 
                            </div> 								
                        </div> 
                        <div >
                            <div >
                                <h2 class="tit_tarjeta_a" >No Documento<br /><br /></h2>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" id="dni" name="dni" class="form-control input_lin required"  placeholder="">
                                </div> 							
                            </div> 
                            <h2 class="tit_tarjeta_a" >Departamento<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="hidden" id="country" name="country" value="0"/>
                                <select class="form-control tit_tarjeta_a" id="state" name="state">
                                </select> 
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Ciudad<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select class="form-control tit_tarjeta_a" id="city" name="city">
                                </select> 
                            </div> 							
                        </div> 
                    </div>
                    <div class="member-socail ">
                        <div >
                            <h2 class="tit_tarjeta_b" >Datos de contacto <br /></h2>
                        </div>
                        <div >
                            <h2 class="tit_tarjeta_a" >Telefono fijo <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="phone" name="phone" class="form-control input_lin required"  placeholder="xxx-xx-xx">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Telefono movil <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="mobile" name="mobile" class="form-control input_lin required"  placeholder="3xx-xxxxxxx">
                            </div> 							
                        </div> 	
                        <div >
                            <h2 class="tit_tarjeta_a" >Direccion <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="address" name="address" class="form-control input_lin required"  placeholder="Dirección">
                            </div> 							
                        </div> 
                    </div>

                </form>  	 		
            </div>
        </div> 
        <div class="col-md-4" >
        </div> 

    </div>  
</div>	

