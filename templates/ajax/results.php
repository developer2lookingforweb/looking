
<div id="dg_comparison" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" style="z-index: 100001!important;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <button type="button" class="close btn_cerrar_color01" style="float: right;position: relative;z-index: 10;" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    <div class="team-member modern">
                        <div  class="member-info-det">
                            <div id="error-title" class="col-md-12 tit_tarjeta_f2" style="text-align:center;">

                            </div>
                        </div> 
                        <div id="error-msg" class="col-md-12 tit_tarjeta_f1" style="text-align:center;">

                        </div>

                    </div> 

                </div>
            </div>

        </div>
    </div>
</div>
<div class="" style="margin-top:0px;height:100%;width:100%;"  >
    <div class="row" >
        <div class="col-md-2"> </div>
        <div class="col-md-8">
            <h1 class="tit_19" id="main-title"><br/> <br/><p class="tit_20" >  </p> </h1>
        </div>
        <div class="col-md-2 hidden-xs hidden-sm hidden-md" style='height: 100px; background-color: #626B7E; padding-right: 0px!important; ' > </div>
    </div> 
    <div class="row">
        <div class="col-md-3" style='padding-right: 0px!important; ' >

            <div class="team-member modern filter-block" >
                <div class="member-info">
                    <div >
                        <h2 class="tit_tarjeta_a" id="filter-title" >Ajusta tu búsqueda <i class="fa fa-caret-down fa-2x hidden-md hidden-lg" aria-hidden="true" style="float: right;"></i><br /><br /></h2>
                    </div>
                </div> 
                <div id="questions" class="member-socail ">
                    <br />
                    <a href="#"  style='padding: 4px 4px;display:inline-block;margin: 5 20px;border-left: 1px #EEE;' class="tit_tarjeta_c" >EQUIPOS	</a> 
                    <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                    <br /><br /><br />
                </div>
            </div> 

        </div> 
        <div class="col-md-7" id="grid-results" >






        </div> 
        <div class="col-md-2 col-xs-12" id="div_wrapper_comp" style='position: relative; padding-left: 0;' > 
            <div class="div_comparar" id="div_comparation">
                <div>
                <h2 class="tit_comparar_a" >¡COMPARA OPCIONES!<br /><br /> <p class="tit_comparar_b" > Haz click en éste ícono en tu tarjeta preferida para añadirla o removerla de los espacios de comparación, podrás evaluar tus opciones más fácil </p>

                    <div class="row" style="margin-right: 0px; " >
                        <div class="col-md-12">
                            <img src="*|comparison-icon-src|*" alt="">
                        </div> 
                    </div>  
                    <div class="row" style="margin-right: 0px; " >
                        <div class="col-md-12">
                            <h2 class="tit_comparar_c" >SELECCIONA Y COMPARA<br />
                            <br /> 
                            <!--<p class="tit_comparar_d" > Haz click en el círculo inferior de la tarjeta para añadirlo o quitarlo</p>-->

                        </div> 
                    </div> 
                    <div class="row" id="wrapper_prods" >
                        <div id='div_1' data-pos='1' data-ocupado='0' data-id='0' class="col-md-4 col-sm-4 col-xs-4 caja_compara">
                            <a href="#" class="thumbnail caja_pos">
                                1
                                <img class="comparison-img img-responsive" src="" >
                            </a>
                        </div>
                        <div id='div_2' data-pos='2' data-ocupado='0' data-id='0' class="col-md-4 col-sm-4 col-xs-4  caja_compara">
                            <a href="#" class="thumbnail caja_pos">
                                2
                                <img class="comparison-img img-responsive" src="" >
                            </a>
                        </div>
                        <div id='div_3' onclick="" data-pos='3' data-ocupado='1' data-id='' class="col-md-4 col-sm-4 col-xs-4  caja_compara">
                            <a href="#" class="thumbnail caja_pos">
                                3
                                <img class="comparison-img img-responsive" src="" >
                            </a>
                        </div>
                    </div>
                    <div class="row" style="margin-right: 0px; " >
                        <div class="col-md-12">
                            <a id='btn_comparar' href="#" class="button btn_tipo1 hvr-float-shadow" style="margin-top: 15px; padding-top: 15px; " >Comparar</a>
                        </div> 
                    </div>  
                </div>
            </div>
        </div>


    </div>
</div>	