<div class="container-fluid"  style="" >
    <div class="row" >
        <div class="col-md-12">
            <h1 class="tit_19" ><br/>ESTAMOS A UN PASO DE TERMINAR <br/>
                <p class="tit_20" >LLENA LOS SIGUIENTES DATOS Y REALIZA EL PAGO  </p> 
            </h1>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-4" style='padding-right:0px!important;' >
            <div class="team-member modern" >
                <form class="form-horizontal" role="form" action="#" method="post" >
                    <input type="hidden" id="cid" name="cid" value="0"/>
                    <div class="member-info">
                        <div >
                            <h2 class="tit_tarjeta_a" >Nombre <br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="text" id="name" name="name" class="form-control input_lin required"  placeholder="John">
                            </div> 							
                            <div class="col-md-6">
                                <input type="text" id="last_name" name="last_name" class="form-control input_lin required"  placeholder="Doe">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Email<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="email" name="email" class="form-control input_lin required"  placeholder="usuario@dominio.com">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Tipo identificación <br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select class="form-control tit_tarjeta_a required" id="docType" name="docType">
                                </select> 
                            </div> 								
                        </div> 
                        <div >
                        <div >
                            <h2 class="tit_tarjeta_a" >No Documento<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="dni" name="dni" class="form-control input_lin required"  placeholder="xxxxxx">
                            </div> 							
                        </div> 
                            <h2 class="tit_tarjeta_a" >Departamento<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="hidden" id="country" name="country" value="0"/>
                                <select class="form-control tit_tarjeta_a required" id="state" name="state">
                                </select> 
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Ciudad<br /><br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <select class="form-control tit_tarjeta_a required" id="city" name="city">
                                </select> 
                            </div> 							
                        </div> 
                    </div>
                    <div class="member-socail ">
                        <div >
                            <h2 class="tit_tarjeta_b" >Datos de contacto <br /></h2>
                        </div>
                        <div >
                            <h2 class="tit_tarjeta_a" >Telefono fijo <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="phone" name="phone" class="form-control input_lin required"  placeholder="xxx-xx-xx">
                            </div> 							
                        </div> 
                        <div >
                            <h2 class="tit_tarjeta_a" >Telefono movil <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="mobile" name="mobile" class="form-control input_lin required"  placeholder="3xx-xxxxxxx">
                            </div> 							
                        </div> 	
                        <div >
                            <h2 class="tit_tarjeta_a" >Direccion <br /></h2>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" id="address" name="address" class="form-control input_lin required"  placeholder="Dirección">
                            </div> 							
                        </div> 
                    </div>

                </form>  	 		
            </div>
        </div> 
        <div class="col-md-7"  >
            <p class="tit_24"><br/>ESTE ES EL RESUMEN DE TU COMPRA<br/></p> 
            <div class="team-member modern">
                <div class="member-info-det">
                    <div >
                        <div class="col-md-9 tit_tarjeta_f11"  > Planes o Productos añadidos </div> 
                        <div class="col-md-3 tit_tarjeta_f11">Valor a pagar </div> 
                        <br /><br />
                    </div> 
                </div> 
                <div id="cart-products"> 
                </div> 
                
                <div class="member-info-det"  >
                    <br /><br />
                    <div class="col-md-9 tit_tarjeta_f11"  >Total a pagar  </div> 
                    <div id="total-value" class="col-md-3 tit_tarjeta_f11"  ></div>
                    <br /><br />					 
                </div> 
                <div class="member-info-det">
                    <div class="col-md-12 text-center">
                        <form id="payu" method="post" action="*|urlPayU|*">
                          <input name="merchantId"    type="hidden"  value="*|merchant|*"   >
                          <input name="accountId"     type="hidden"  value="*|account|*" >
                          <input name="description"   type="hidden"  value="*|description|*"  >
                          <input id="address" name="shippingAddress" type="hidden"  value="" >
                          <input id="city" name="shippingCity" type="hidden"  value="" >
                          <input id="country" name="shippingCountry" type="hidden"  value="" >
                          <input id="reference" name="referenceCode" type="hidden"  value="*|reference|*" >
                          <input id="pamount" name="amount"        type="hidden"  value="*|amount|*"   >
                          <input name="tax"           type="hidden"  value="0"  >
                          <input name="taxReturnBase" type="hidden"  value="0" >
                          <input name="currency"      type="hidden"  value="*|currency|*" >
                          <input id="signature" name="signature"     type="hidden"  value=""  >
                          <input name="test"          type="hidden"  value="*|test|*" >
                          <input name="buyerEmail"    type="hidden"  value="*|buyerEmail|*" >
                          <input id="response" name="responseUrl"    type="hidden"  value="*|response|*" >
                          <input id="confirmation" name="confirmationUrl"    type="hidden"  value="*|confirmation|*" >
                            <a  id="payment"  href="#"  class="button btn_tipo1 hvr-float-shadow " style="margin-top: 10px; padding-top: 10px;margin-bottom: 10px; padding-bottom: 10px;" >Realizar Pago</a>
                        </form>
                    </div> 
                </div>
            </div> 
        </div>
        <div class="col-md-1" >
        </div> 

    </div>  
</div>	

