<?php
/**
  * Clase que lee la configuraci�n del archivo xml para setearlo donde sea necesario
  * @author Administrador
  * 28/09/2011
  */
class constants {
    
    private $_wsdlUrl = '';
    private $_wsdlFunc = '';
    private $_dbHost    = '';
    private $_dbPort    = '';
    private $_dbUser    = '';
    private $_dbPasswd  = '';
    private $_dbSchema  = '';
    private $_comercio   = '';
    private $_puntoVenta  = '';
    private $_clave  = '';
    private $_terminal  = '';
    private $_namespace  = '';
    private $_prUser  = '';
    private $_prPasswd  = '';
    
    function __construct() {
        $this->_dbHost          = 'localhost';
        $this->_dbPort          = '3306';
        $this->_dbUser          = 'root';
        $this->_dbPasswd        = '';
        $this->_dbSchema        = 'lookingsite';
//        $this->_wsdlUrl        = 'http://162.248.54.84:8980/WebServicePuntoRedHTTPAutentication/services/PuntoRedWSService?wsdl';
//        $this->_wsdlUrl        = 'http://10.125.15.4:8980/WebServicePuntoRedHTTPAutentication/services/PuntoRedWSService?wsdl';
        $this->_wsdlUrl        = 'http://200.31.19.79:8980/WebServicePuntoRedHTTPAutentication/services/PuntoRedWSService?wsdl';
        $this->_wsdlFunc        = 'recarga';
//        $this->_comercio        = '293488';
        $this->_comercio        = '298904';
//        $this->_puntoVenta        = '144223';
        $this->_puntoVenta        = '147236';
//        $this->_terminal       = '198453';
        $this->_terminal       = '204966';
//        $this->_clave        = '120403';
        $this->_clave        = '200608';
        $this->_namespace        = 'http://ws.puntored.brainwinner.com';
//        $this->_prUser        = 'deronpre';
//        $this->_prPasswd        = '1a2765212588de4a79718ab973c875d8';
        $this->_prUser        = 'WS_LOOKINGFOR';
        $this->_prPasswd        = 'a3c7e8cd3f6a053f99c635dba68d9f79';
    }
    public function getConfig($param){
        $param = "_".$param;
        if(isset($this->$param)){
            return $this->$param;
        }else{
            return "Config param not found";
        }
    }
}
?>