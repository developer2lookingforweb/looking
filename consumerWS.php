<?php
ini_set("display_errors", "on");
date_default_timezone_set('America/Bogota');
include("nusoap.php"); 
include_once("configuracion.php"); 

class ConsumerWS{
    private $_wsdl;
    private $_namespace;
    private $_function;
    private $_comercio;
    private $_puntoVenta;
    private $_terminal;
    private $_clave;
    private $_params = array();
    
    public function __construct() {
        $config = new constants();
        $this->_wsdl = new nusoap_client($config->getConfig("wsdlUrl"));
        $this->_function = $config->getConfig("wsdlFunc");
        $this->_namespace    = $config->getConfig("namespace");
        $this->_wsdl->setCredentials($config->getConfig("prUser"), $config->getConfig("prPasswd"));
        $this->initParams();
    }
    
    private function initParams(){
        
        $config = new constants();
        $this->setParam('comercioId', $config->getConfig("comercio"));
        $this->setParam('puntodeventaId', $config->getConfig("puntoVenta"));
        $this->setParam('terminalId', $config->getConfig("terminal"));
        $this->setParam('clave', $config->getConfig("clave"));
    }
    private function initPaquetigoParams($idPaquetigo, $celular){
        
        $config = new constants();
        $this->_params = array();
        $this->setParam('clave', $config->getConfig("clave"));
        $this->setParam('codigoPaquetigo', $idPaquetigo);
        $this->setParam('comercioId', $config->getConfig("comercio"));
        $this->setParam('numeroCelular', $celular);
        $this->setParam('puntodeventaId', $config->getConfig("puntoVenta"));
        $this->setParam('terminalId', $config->getConfig("terminal"));
    }
    private function initPaquetigoQueryParams($idPaquetigo){
        
        $config = new constants();
        $this->_params = array();
        $this->setParam('codigoPaquetigo', $idPaquetigo);
        $this->setParam('comercioId', $config->getConfig("comercio"));
    }
    private function setParam($key,$value){
        $this->_params[$key] = $value;
    }
    public function setNumber($number){
        $this->setParam("numero", $number);
    }
    public function setValue($value){
        $this->setParam("valor", $value);
    }
    public function setTrace($id){
        $this->setParam("trace", $id);
    }
    
    public function setOwner($name){
        switch ($name){
            case 'movistar':
                $this->setParam("codigoOperador", '668');                
                break;
            case 'claro':
                $this->setParam("codigoOperador", '266');                
                break;
            case 'tigo':
                $this->setParam("codigoOperador", '652');
                break;
            case 'exito':
                $this->setParam("codigoOperador", '660');
                break;
            case 'avantel':
                $this->setParam("codigoOperador", '350');
                break;
            case 'cellvoz':
                $this->setParam("codigoOperador", '300');
                break;
            case 'uffmovil':
                $this->setParam("codigoOperador", '304');
                break;
            case 'une':
                $this->setParam("codigoOperador", '500');
                break;
            case 'directv':
                $this->setParam("codigoOperador", '400');
                break;
            case 'etb':
                $this->setParam("codigoOperador", '903');
                break;
            case 'virgin':
                $this->setParam("codigoOperador", '906');
                break;
        }
    }
    
    public function sendPaquetigo($idPaquetigo,$celular){
        try{
                $this->initPaquetigoParams($idPaquetigo, $celular);
                $result = $this->_wsdl->callRequest('paquetigo', $this->_params , $this->_namespace, '',true);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
        }
    }

    public function findPaquetigo($idPaquetigo){
        try{
                $this->initPaquetigoQueryParams($idPaquetigo);
                $result = $this->_wsdl->call('consulta_paquetigo', $this->_params , $this->_namespace, '',true);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
        }
    }


    public function send(){
        if(count($this->_params)== 8){
            try{
                $result = $this->_wsdl->call($this->_function,  $this->_params, $this->_namespace, '',true);
                return $result;
            } catch (Exception $ex) {
                var_dump($ex);
            }
        }  else {
            echo "Datos incompletos";
        }
    }
    
}