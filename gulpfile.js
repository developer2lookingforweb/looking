var gulp = require('gulp');

var autoprefix = require('gulp-autoprefixer');
var minifyCSS  = require('gulp-minify-css');
var concat     = require('gulp-concat');
var sass       = require('gulp-sass');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');

gulp.task('sass', function(){
    gulp.src('./css/styles.scss')
        .pipe(sass())
        .pipe(autoprefix())
        .pipe(minifyCSS())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./css/assets/css/'));
});


gulp.task('css', function() {  
  gulp.src('./assets/css/main.css')
    .pipe(minifyCSS())
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('./assets/css/'));
  gulp.src('./css/normalize.css')
    .pipe(minifyCSS())
    .pipe(rename('normalize.min.css'))
    .pipe(gulp.dest('./css/'));
});

gulp.task('js', function() 
{
  gulp.src('./assets/js/*.js')
    .pipe(concat('zipped.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./assets/'))
});

gulp.task('watch', function(){
    gulp.watch('./css/styles.scss', ['sass'])
    gulp.watch('./assets/js/*.js', ['js'])
    gulp.watch('./assets/css/main.css', ['css'])
});

// gulp.task('styles', function(){
//     gulp.src([
//         './css/styles.css'
//     ])
//         .pipe(concat('styles.css'))
//         .pipe(autoprefix('last 2 versions'))
//         .pipe(minifyCSS())
//         .pipe(gulp.dest('./'));
// });

// gulp.task('scripts', function(){
//     gulp.src(['./bower_components/jquery/dist/jquery.min.js', './bower_components/jquery-ui/jquery-ui.js', './js/modernizr.custom.17475.js', './js/jquery.min.js', './bs3/js/bootstrap.min.js', './js/bootstrap-datepicker.js', './js/jquery.flexslider-min.js', './js/script.js', './js/jquery.minimalect.min.js', './js/styleswitcher.js', './js/rs-plugin/js/jquery.plugins.min.js', './js/rs-plugin/js/jquery.revolution.min.js', './js/jquery.bxslider.min.js', './bower_components/jquery-validation/dist/jquery.validate.js', './js/script.js'])
//         .pipe(concat('scripts.js'))
//         .pipe(uglify())
//         .pipe(gulp.dest('./'));
// });

// gulp.task('custom-scripts', function(){
//     gulp.src(['./js/custom.js'])
//         .pipe(uglify())
//         .pipe(gulp.dest('./'));
// });

gulp.task('default', ['watch', 'sass', 'css', 'js']);

// Set the banner content
var banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2017-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n',
    ' */\n',
    ''
].join('');

// Compile LESS files from /less into /css
gulp.task('less', function() {
    return gulp.src('less/creative.less')
        .pipe(less())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Minify compiled CSS
gulp.task('minify-css', ['less'], function() {
    return gulp.src('css/creative.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Minify JS
gulp.task('minify-js', function() {
    return gulp.src('js/creative.js')
        .pipe(uglify())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Copy vendor libraries from /node_modules into /vendor
gulp.task('copy', function() {
    gulp.src(['node_modules/bootstrap/dist/**/*', '!**/npm.js', '!**/bootstrap-theme.*', '!**/*.map'])
        .pipe(gulp.dest('vendor/bootstrap'))

    gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest('vendor/jquery'))

    gulp.src(['node_modules/magnific-popup/dist/*'])
        .pipe(gulp.dest('vendor/magnific-popup'))

    gulp.src(['node_modules/scrollreveal/dist/*.js'])
        .pipe(gulp.dest('vendor/scrollreveal'))

    gulp.src([
            'node_modules/font-awesome/**',
            '!node_modules/font-awesome/**/*.map',
            '!node_modules/font-awesome/.npmignore',
            '!node_modules/font-awesome/*.txt',
            '!node_modules/font-awesome/*.md',
            '!node_modules/font-awesome/*.json'
        ])
        .pipe(gulp.dest('vendor/font-awesome'))
})

gulp.task('watchless', function(){
    gulp.watch('less/creative.less', ['less','minify-css'])
    gulp.watch('js/creative.js', ['minify-js'])
    gulp.watch('./css/styles.scss', ['sass'])
});

// Run everything
gulp.task('default', ['watchless','less', 'sass', 'minify-css', 'minify-js', 'copy']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})


// Dev task with browserSync
gulp.task('dev', ['browserSync', 'less', 'minify-css', 'minify-js'], function() {
    gulp.watch('less/*.less', ['less']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});
