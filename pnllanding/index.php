<?php
    session_start();
    const PAYU_RESEND          = 'Verificar pago de la orden ';
    const PAYU_URL          = 'https://gateway.payulatam.com/ppp-web-gateway/';
    const PAYU_URL_S        = 'https://sandbox.gateway.payulatam.com/ppp-web-gateway/';
    const PAYU_MERCHANT        = '563485';
    const PAYU_MERCHANT_S        = '508029';
    const PAYU_ACOUNT        = '566035';
    const PAYU_ACOUNT_S        = '512321';
    const PAYU_KEY        = '213FV389v0d31mACBAo0CuqQjZ';
    const PAYU_KEY_S        = '4Vj8eK4rloUd272L48hsrarnUA';
    const PAYU_RESPONSE        =  'looking/chargeResponse/';
    const PAYU_CONFIRMATION        = 'looking/chargeConfirmation/';
    const PAYU_MODE        = 0; // 1 test 0 production
    const PAYU_PROCCESSMINCOST        = 2900;
    const PAYU_PROCCESSLIMIT        = 83100;
    const PAYU_PROCCESSPERCENTAGE        = 0.0349; // over total transaction
    const PAYU_PROCCESSIVA        = 0.16;  //iva over process cost
    const PAYU_PROCCESSRETEFTE        = 0.015; //retefte over total transaction
    const PAYU_PROCCESSRETEICA        = 0.00414; //ica over total transaction
    
    
    const PAYU_DESCRIPTION        = "Taller de Ho'oponopono Bogotá 5% off"; //ica over total transaction
    const PAYU_REFERENCE        = "O00000321321321"; //ica over total transaction
    const PAYU_AMOUNT        = 541500; //ica over total transaction
    
    
    const TEST_ENVIROMENT        = false; //set test enviroment
    
    function getPayUData(){
            $payU = new stdClass();
            if(TEST_ENVIROMENT==true){
                $payU->test = 1;
                $payU->url = PAYU_URL_S;
                $payU->merchant = PAYU_MERCHANT_S;
                $payU->account = PAYU_ACOUNT_S;
                $payU->key = PAYU_KEY_S;
            }else{
                $payU->test = 0;
                $payU->url = PAYU_URL;
                $payU->merchant = PAYU_MERCHANT;
                $payU->account = PAYU_ACOUNT;
                $payU->key = PAYU_KEY;
                
            }
            $payU->response = PAYU_RESPONSE;
            $payU->confirmation = PAYU_CONFIRMATION;
            $payU->description = PAYU_DESCRIPTION;
            return $payU;
    }
    $payu = getPayUData();
    $signature = md5($payu->key."~".$payu->merchant."~".PAYU_REFERENCE."~".PAYU_AMOUNT."~COP");
    $byuerEmail = "john.jimenez@lookingforweb.com";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alofijo te invita</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/coming-soon.min.css" rel="stylesheet">

  </head>

  <body>

    <div class="overlay"></div>

    <div class="masthead">
      <div class="masthead-bg"></div>
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto">
            <div class="masthead-content text-white py-5 py-md-0">
              <!--<p class="mb-5">todo lo que aparece en nuestra vida es un pensamiento, una memoria, un programa funcionando y se presenta para darnos una oportunidad de soltar, de limpiar, de borrar</p>-->
              <h1 class="mb-3">Inscribete ahora y obtén el 5% de descuento</h1>
              <p class="mb-5">Te esperamos el proximo 24-25 de febrero, Maria José Cabanillas, psicóloga, formadora y escritora, pionera en difundir la técnica de Ho´oponopono en España compartirá con todos sus experiencias</p>
              <form id="form" method="post" action=""  data-toggle="validator">
              <div class="input-group input-group-newsletter">
                <input name="name" type="text" class="form-control" placeholder="Ingresa nombre..." aria-label="Search for..." required>
                <input name="last_name" type="text" class="form-control" placeholder="Ingresa apellido..." aria-label="Search for..." required>
              </div>
              <div class="input-group input-group-newsletter">
                  <input name="nit" type="text" class="form-control" placeholder="Ingresa identificación..." aria-label="Search for..." required>
              </div>
              <div class="input-group input-group-newsletter">
                  <input name="email" type="email" class="form-control" placeholder="Ingresa email..." aria-label="Search for..." required>
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Inscribete!</button>
                </span>
              </div>
                  
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <form id="payu" method="post" action="<?php echo $payu->url?>">
         <input name="merchantId"    type="hidden"  value=<?php echo $payu->merchant?>   >
         <input name="accountId"     type="hidden"  value="<?php echo $payu->account?>" >
         <input name="description"   type="hidden"  value="<?php echo $payu->description?>"  >
         <input id="address" name="shippingAddress" type="hidden"  value="" >
         <input id="city" name="shippingCity" type="hidden"  value="" >
         <input id="country" name="shippingCountry" type="hidden"  value="" >
         <input id="reference" name="referenceCode" type="hidden"  value="<?php echo PAYU_REFERENCE?>" >
         <input id="pamount" name="amount"        type="hidden"  value="<?php echo PAYU_AMOUNT?>"   >
         <input name="tax"           type="hidden"  value="0"  >
         <input name="taxReturnBase" type="hidden"  value="0" >
         <input name="currency"      type="hidden"  value="COP" >
         <input id="signature" name="signature"     type="hidden"  value="<?php echo $signature?>"  >
         <input name="test"          type="hidden"  value="<?php echo $payu->test?>" >
         <input id="buyerEmail" name="buyerEmail"    type="hidden"  value="<?php echo $byuerEmail?>" >
         <input id="response" name="responseUrl"    type="hidden"  value="<?php echo $payu->response?>" >
         <input id="confirmation" name="confirmationUrl"    type="hidden"  value="<?php echo $payu->confirmation?>" >
         <!--<input name="Submit"   class="button"     type="submit"  value="Realizar pago" >-->
       </form>
    <div class="social-icons">
      <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
          <a href="#">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="#">
            <i class="fa fa-facebook"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="#">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
      </ul>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/vide/jquery.vide.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/coming-soon.min.js"></script>

  </body>

</html>
