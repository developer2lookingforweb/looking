(function($) {
  "use strict"; // Start of use strict

  // Vide - Video Background Settings
  $('body').vide({
    mp4: "mp4/mariajose.mp4",
    ogv: "mp4/mariajose.ogv",
    webm: "mp4/mariajose.webm",
    poster: "img/image.png"
  }, {
    posterType: 'png'
  });
  $(".btn-secondary").click(function(){
      if(validate()){
          
            var submitdata = {
                  "name": $("input[name='name']").val(),
                  "last_name": $("input[name='last_name']").val(),
                  "email": $("input[name='email']").val()
              };
            $.ajax({
              url: "processPayment.php",
              type: "post",
              dataType: "json",
              data: submitdata,
              success: function (data) {
                  if (data.status == true) {
                      $("#buyerEmail").val(data.data.email);
                      $("#reference").val(data.data.reference);
                      $("#pamount").val(data.data.amount[1]);
                      $("#signature").val(data.data.signature);
                      $("#response").val(data.data.response);
                      $("#confirmation").val(data.data.confirmation);
                      $("#payu").submit();
                  } else {
                  }
              }
          });
      }
  })
  
  function validate(){
      $( ":input" ).removeClass("error");
      var name      = $("input[name='name']").val();
      var last_name = $("input[name='last_name']").val();
      var nit       = $("input[name='nit']").val();
      var email     = $("input[name='email']").val();
      var nameRequired = (name !== "");
      var lastNameRequired = (last_name !== "");
      var nitRequired = (nit !== "");
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var num = /^\d{1,10}$/;
        re.test(String(email).toLowerCase());
      if(nameRequired && lastNameRequired && nitRequired && num.test(String(nit)) && re.test(String(email).toLowerCase())){
          return true
      }
      if(! nameRequired){
          $("input[name='name']").addClass("error");
          $("input[name='name']").focus();
      }
      if(! lastNameRequired){
          $("input[name='last_name']").addClass("error");
          $("input[name='last_name']").focus();
      }
      if(! nitRequired || ! num.test(String(nit))){
          $("input[name='nit']").addClass("error");
          $("input[name='nit']").focus();
      }
      if(! re.test(String(email).toLowerCase())){
          $("input[name='email']").addClass("error");
          $("input[name='email']").focus();
      }
      return false;
  }
})(jQuery); // End of use strict
