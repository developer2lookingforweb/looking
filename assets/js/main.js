$(document).ready(function(){
        $("#opc_buscar_off, #opc_buscar_off_m").keypress(function(e){
//            e.preventDefault();
            var key = e.which;
            if (key == 13)  // the enter key code
            {
//                if(jQuery(this).attr("id") == "opc_buscar-m"){
//                    jQuery("#menu").removeClass("visible");
//                }
                window.location.href="/looking/principal/browse/"+$(this).val();

            }
        });
        $("#menu li a").click(function(event){
        if(!$(this).hasClass('menu-focus')){
            $("#menu li a.menu-focus").parent().find(".submenu").toggle("lineal");
            $("#menu li a.menu-focus").find(".arrow-menu").toggleClass("icon-angle-left");
            $("#menu li a.menu-focus").find(".arrow-menu").toggleClass("icon-angle-down");
            $("#menu li a.menu-focus").toggleClass("menu-focus");
        }
        event.preventDefault();
        var obj = $(this);
        var menu = null;
        if($(this).attr("class")!="" && $(this).attr("class")!=undefined){
            menu = $(this).attr("class");
            if(menu.indexOf("_")>0){
                menu = menu.split("_");
                
                menu = menu[1].split(" ");
                $.ajax({
                    global: false,
                    type: "POST",
                    url: base_url + "sections/setMenu/"+menu[0],
                    success: function(){

                        var href = $(obj).attr("href");

                        if(href != "" && href !=undefined && href !="#")window.location = href;
                    }
                });
            }else{
                window.location = $(this).attr("href");
            }
        }else{
                window.location = $(this).attr("href");
            }
        $(this).parent().find(".submenu").toggle("lineal");
        $(this).toggleClass("menu-focus");
        $(this).find(".arrow-menu").toggleClass("icon-angle-left");
        $(this).find(".arrow-menu").toggleClass("icon-angle-down");        
        resizeHeight();
    });
});
(function($) {

	skel.breakpoints({
		xlarge:	'(max-width: 1680px)',
		large:	'(max-width: 1280px)',
		medium:	'(max-width: 980px)',
		small:	'(max-width: 736px)',
		xsmall:	'(max-width: 480px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body'),
			$header = $('#header');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 100);
			});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		// Scrolly.
			$('.scrolly').scrolly({
				offset: function() {
					return $header.height();
				}
			});

		// Menu.
			$('#menu')
				.append('<a href="#menu" class="close"></a>')
				.appendTo($body)
				.panel({
					delay: 500,
					hideOnClick: true,
					hideOnSwipe: true,
					resetScroll: true,
					resetForms: true,
					side: 'right'
				});

	});

})(jQuery);