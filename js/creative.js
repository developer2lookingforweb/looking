(function ($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function () {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 10
        }
    })

    // Initialize and Configure Scroll Reveal Animation
    window.sr = ScrollReveal();
    sr.reveal('.sr-icons', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 200);
    sr.reveal('.sr-button', {
        duration: 1000,
        delay: 200
    });
    sr.reveal('.sr-contact', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 300);

    // Initialize and Configure Magnific Popup Lightbox Plugin
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });


    // Click tracking for marketing in G Analytics


//    $('form[action*="//lookingforweb.com/login/signin"]').submit(function () {
//        ga("send", "event", 'form', 'submit', 'email signup');
//    });
//    $('.ga-facebook-signup').click(function () {
//        ga("send", "event", 'link', 'click', 'facebook signup');
//    });
//    $('.ga-cta-signup').click(function () {
//        ga("send", "event", 'link', 'click', 'signup cta');
//    });
//    $('a[href*="/blogs/entries"]').click(function () {
//        ga("send", "event", 'link', 'click', 'blog');
//    });
//    $('form[action*="//lookingforweb.com/sendy/subscribe"]').submit(function () {
//        ga("send", "event", 'form', 'submit', 'newsletter');
//    });
//    $('form[action*="//lookingforweb.com/messages/persistUser"]').submit(function () {
//        ga("send", "event", 'form', 'submit', 'contact us');
//    });



    // Browse a product by keyword

    $(".opc_buscar_off_b").click(function (e) {
        var action = $(".search-form").attr("action");
//        $(".search-form").attr("action", "/" + $("#opc_buscar_off").val());
        var val = $(this).parent().parent().find(".search-box").val()
        $(".search-form").attr("action", action + "/" + val);
        $(".search-form").submit();
    });
    
    
    $(document).on("keypress", ".search-box", function(e) {
        if (e.which == 13) {
            var action = $(".search-form").attr("action");
//        $(".search-form").attr("action", "/" + $("#opc_buscar_off").val());
            var val = $(this).val()
            $(".search-form").attr("action", action + "/" + val);
            $(".search-form").submit();
            return false; // prevent the button click from happening
            //do some stuff
        }
   });
    // Browse a product by keyword

    $("#opc_buscar_off").keydown(function(){
            
    });
    
    // trigger signup form
    $(".btn-start,.signup").click(function (e) {

        $.ajax({
            type: 'post',
            url: '/looking/looking/start',
            //            dataType: "json",
            success: function (data) {
                $(this).showMessage("", data);

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });
    $(".login").click(function (e) {

        $.ajax({
            type: 'post',
            url: '/looking/looking/login',
            //            dataType: "json",
            success: function (data) {
                $(this).showMessage("", data);

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });
    $('body').on("click",".remember-password",function (e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/looking/looking/recoverPasswd/',
            //            dataType: "json",
            success: function (data) {
                $(this).showMessage("Recuperar contraseña",data);

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    });
    $('body').on("click","#submit-recover",function (e) {
        e.preventDefault();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if( ! regex.test($("#recover-email").val())){
            $.ajax({
                url: "login/recover/",
                global: false,
                type: "post",
                data: {email: $("#email").val()},
                dataType: "json",
                success: function (data){
                    if(data.status === true){
                        $("#recover-email").val("");
                        $(this).showMessage("Recuperar contraseña","Consulta tu correo y sigue las instrucciones<br/>");
                    }else{
                        $("#dg_error #error-title").html("Recuperar contraseña");
                        $(this).showMessage("Recuperar contraseña","El correo ingresado no fue encontrado verifica los datos<br/>");
                    }
                }
            });
        }else{
            $(this).showMessage("Datos inválidos","Verifica tu datos de ingreso para continuar<br/>");

        }
    });

    $(".go-back").click(function (e) {
        window.history.back();
    });
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    $('body').on('submit','form#registerForm',function (event) {
        var form = $(this)
        var valid = true;
        var message = '';
        $("form#registerForm :input").each(function(){
            $(this).removeClass('validation-error');
            if($(this).hasClass("required")){
                if($(this).val()==""){
                    valid = false;
                    $("#"+$(this).attr("id")).addClass('validation-error');
                    if($(this).attr('error-msg')){
                        message = $(this).attr('error-msg');
                    }else{
                        message = 'Hay campos requeridos sin llenar';
                    }
                    return
                }
            }
            if($(this).hasClass("alpha")){
                var textdata = /^[ÑñáéíóúÁÉÍÓÚa-zA-Z\s]*$/
                if( ! textdata.test($(this).val())){
                    valid = false;
                    $("#"+$(this).attr("id")).addClass('validation-error');
                    if($(this).attr('error-msg')){
                        message = $(this).attr('error-msg');
                    }else{
                        message = 'El campo no puede contener números, verifica la información';
                    }
                    return
                }
            }
            if($(this).hasClass("email")){
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if( ! regex.test($(this).val())){
                    valid = false;
                    $("#"+$(this).attr("id")).addClass('validation-error');
                    if($(this).attr('error-msg')){
                        message = $(this).attr('error-msg');
                    }else{
                        message = 'No es un correo válido';
                    }
                    return
                }
            }

        });
        if(valid == false){
//            $(this).showMessage("Datos Inválidos",message);
        event.preventDefault();

        }else{
//            form.submit();
//            return true;
        }
    });
})(jQuery); // End of use strict

/**
 * Message alert
 */
(function ($) {
    $.fn.showMessage = function (title, msg) {
        $("#small-dialog").removeClass("large-popup");
        $("#small-dialog").removeClass("medium-popup");
        $("#small-dialog h2").html(title);
        $("#small-dialog p").html(msg);
        setTimeout(function () {
            $("#modal-trigger").trigger('click')
        }, 500);
        return this;
    };
})(jQuery);
/**
 * Validator
 */
(function ($) {
    $.fn.extend({
        validateForm: function(){
            
        },
        required: function(){
            this.each(function(){
                alert("prueba")
            })
        }
    })
})(jQuery);

/**
 * Chat js
 */
//(function () {
//    $(function () {
//        
//        
//        sendMessage('Hello Philip! :)','looking');
//        setTimeout(function () {
//            return sendMessage('Hi Sandy! How are you?','guest');
//        }, 1000);
//        setTimeout(function () {
//           return $(this).sendMessage('I\'m fine, thank you!','guest');
//       }, 2000);
//    });
//}.call(this));


(function ($) {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    var getMessageText, message_side, sendMessage;
    message_side = 'right';
    $.fn.getMessageText = function () {
        var $message_input;
        $message_input = $('.message_input');
        return $message_input.val();
    };
    $.fn.sendMessage = function (text, author) {
        var $messages, message;
        if (text.trim() === '') {
            return;
        }
        $('.message_input').val('');
        $messages = $('.messages');
        message_side = message_side === 'left' ? 'right' : 'left';
        message_side = author === 'looking' ? 'right' : 'left';
        message = new Message({
            text: text,
            message_side: message_side
        });
        message.draw();
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
    };
    $('.send_message').click(function (e) {
        interaction($(this).getMessageText());
        return $(this).sendMessage($(this).getMessageText(),'guest');
    });
    $('.message_input').keyup(function (e) {
        if (e.which === 13) {
            interaction($(this).getMessageText());
            return $(this).sendMessage($(this).getMessageText());
        }
    });
    $('.top_menu').click(function (e) {
        var height = (!$(".messages").hasClass("fade"))?"179px":"450px";
        $(".messages").toggleClass("fade");
        $(".bottom_wrapper").toggleClass("fade");
        $(".chat_window").css("height",height);
    });
    function interaction(msg){
        $.ajax({
            type: 'post',
            url: 'watson',
            data: {msg: msg},
            dataType: "json",
            success: function (data) {
                if (data.status == true) {
                    $(this).sendMessage(data.message,'looking');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#contenido_").html("<small>" + xhr.responseText + "</small>");
            }
        });
    }
//           $(this).sendMessage('Hello Philip! :)','looking');
//        setTimeout(function () {
//            return $(this).sendMessage('Hi Sandy! How are you?','guest');
//        }, 1000);
//        setTimeout(function () {
//           return $(this).sendMessage('I\'m fine, thank you!','looking');
//       }, 2000);
    setTimeout(function(){interaction("")},1000);
})(jQuery);