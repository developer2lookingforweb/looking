//recuperamos la query string
var qs = document.getElementById("identificador_js").src.match(/\w+=\w+/g);
//array para almacenar las variables
var _GET= {};
 
var t,i = qs.length;
while (i--) {
 
     //t[0] nombre del parametro y t[1] su valor
     t = qs[i].split("=");
 
     //opción 1: a modo de PHP
     _GET[t[0]] = t[1];
 
    //opción2: variables con el mismo nombre usando eval
    eval('var '+t[0]+'="'+t[1]+'";');
}
 
//mostrará el nombre 2 veces
//alert(_GET['base_url'] + " " + base_url);

base_url = base_url.replace(/47slash/g,"/");
base_url = base_url.replace(/250dot/g,".");
base_url = base_url.replace(/58twopoints/g,":");
$(document).ready(function(){
    $("body").addClass("body-content");    
    changeBehaviorDropdown();
    
    $("#menu .menu-selected").parent().find(".submenu").toggle("lineal");
    $("#menu .menu-selected").toggleClass("menu-focus");
    $("#menu .menu-selected").find(".arrow-menu").toggleClass("icon-angle-left");
    $("#menu .menu-selected").find(".arrow-menu").toggleClass("icon-angle-down");
    
    setTimeout(resizeHeight(),5000);
    
    $("#menu li a").click(function(event){
        if(!$(this).hasClass('menu-focus')){
            $("#menu li a.menu-focus").parent().find(".submenu").toggle("lineal");
            $("#menu li a.menu-focus").find(".arrow-menu").toggleClass("icon-angle-left");
            $("#menu li a.menu-focus").find(".arrow-menu").toggleClass("icon-angle-down");
            $("#menu li a.menu-focus").toggleClass("menu-focus");
        }
        event.preventDefault();
        var obj = $(this);
        var menu = null;
        if($(this).attr("class")!="" && $(this).attr("class")!=undefined){
            menu = $(this).attr("class");
            if(menu.indexOf("_")>0){
                menu = menu.split("_");
                
                menu = menu[1].split(" ");
                $.ajax({
                    global: false,
                    type: "POST",
                    url: base_url + "sections/setMenu/"+menu[0],
                    success: function(){

                        var href = $(obj).attr("href");

                        if(href != "" && href !=undefined && href !="#")window.location = href;
                    }
                });
            }else{
                window.location = $(this).attr("href");
            }
        }else{
                window.location = $(this).attr("href");
            }
        $(this).parent().find(".submenu").toggle("lineal");
        $(this).toggleClass("menu-focus");
        $(this).find(".arrow-menu").toggleClass("icon-angle-left");
        $(this).find(".arrow-menu").toggleClass("icon-angle-down");        
        resizeHeight();
    });
    
    $(".content-user-menu .color").click(function(){
       $("#css-swithcer").attr("href",base_url + "/css/"+$(this).attr("color")+".css") ;
        $.ajax({
            global: false,
            type: "POST",
            url: base_url + "/users/setTheme/"+$(this).attr("color"),
            success: function(){
            }
        });
    });
    $("#drop1-trigger").click(function(){
       cartDropdown($(this)); 
    });
//    $("#globalSearch").focusin(function(){
//        console.log('in')
//       $("#search-img").attr("src",base_url+"images/search-icon2.png"); 
//    });
//    $("#globalSearch").focusout(function(){
//        console.log('out')
//       $("#search-img").attr("src",base_url+"images/search-icon.png"); 
//    });
//    
    $("#login-button").click(function(){
        if(validateaEmail($("input[name='email-login']").val()) && validateaRequired($("input[name='password-login']").val())){
           $.ajax({
                url: base_url + "login/authCustomer/",
                type: "post",            
                dataType: "json",
                data: { email:$("input[name='email-login']").val(),
                        password:$("input[name='password-login']").val(),
                        remember:$("input[name='remember']").val()
                    },
                success: function(data){ 
                    if(data.status == true){
                        $("#drop4-trigger #username").html(data.user.name);
                        $("#drop4-trigger").removeClass("hide");
                        $("#drop3-trigger").addClass("hide");
                        $("#drop2-trigger").addClass("hide");
                        $("#drop3-trigger").trigger('click');
                        $('.email').each(function(idx,one){
                            var email = $("input[name='email-login']").val();
                            console.log(email)
                            $(one).val(email);
                        });
                    }else{
                        $("#modal-message #message").html("No se han encontrado el usuario<br/><br/>");
                        $('#modal-message').foundation('reveal', 'open');
                    }

                }
            }); 
        }
    });
    
    $("#logout-button").click(function(){
        $.ajax({
             url: base_url + "login/logout/",
             type: "post",            
             dataType: "json",
             data: {},
             success: function(data){ 
                 if(data.status == true){
                     window.location = base_url;
                 }
             }
         }); 
    });
    
//    $("input[name='reg-repassword']").focusout(function(){
//        validatePasswords();
//    });
    function validatePasswords(){
        var passwd  = $("input[name='reg-password']").val()
        var passwd2 = $("input[name='reg-repassword']").val()
        if(passwd !== passwd2){
            $("#modal-message #message").html("La contraseña no coincide, intentalo de nuevo.<br/><br/>");
            $('#modal-message').foundation('reveal', 'open');
            return false;
        }else{
            return true;
        }
    }
    $(".required").focusout(function(){
        $(this).removeClass("validation-error");
        if($(this).val() == ""){
            $(this).addClass("validation-error");
        }
    })
    $("#register-button").click(function(){
        if(validateaEmail($("input[name='reg-email']").val()) 
                && validateaRequired($("input[name='reg-name']").val())
                && validateaRequired($("input[name='reg-last_name']").val())
                && validateaRequired($("input[name='reg-password']").val())
                && validateaRequired($("input[name='reg-repassword']").val())
                && validatePasswords()){
           $.ajax({
                url: base_url + "login/signin/",
                type: "post",            
                dataType: "json",
                data: { email:$("input[name='reg-email']").val(),
                        name:$("input[name='reg-name']").val(),
                        lastName:$("input[name='reg-last_name']").val(),
                        address:$("input[name='reg-address']").val(),
                        phone:$("input[name='reg-phone']").val(),
                        mobile:$("input[name='reg-mobile']").val(),
                        password:$("input[name='reg-password']").val(),
                        fWDnxEXMN_csrf_token_name:$("#csrftag").val()
                    },
                success: function(data){ 
                    if(data.status == true){
                        $("#drop4-trigger #username").html(data.user.name);
                        $("#drop4-trigger").removeClass("hide");
                        $("#drop3-trigger").addClass("hide");
                        $("#drop2-trigger").addClass("hide");
                        $("#drop2-trigger").trigger('click');
                        $('#modal-message').foundation('reveal', 'close');
                        $("#modal-message #modal-title").html("Bienvenido a LookingForWeb");
                        $("#modal-message #message").html("Tu registro a finalizado con éxito.<br/><br/>");
                        $('#modal-message').foundation('reveal', 'open');
                        $('.email').each(function(idx,one){
                            $(one).val($("input[name='reg-email']").val());
                        });
                    }else{
                        $('#modal-message').foundation('reveal', 'close');
                        $("#modal-message #modal-title").html("Verifica tus datos");
                        $("#modal-message #message").html("Ya hay un usuario registrado con el correo<br/><br/>");
                        $('#modal-message').foundation('reveal', 'open');
                    }

                }
            }); 
        }
    })
    
    function validateaRequired(value){
         if(value == ""){
             $('#modal-message').foundation('reveal', 'close');
             $("#modal-message #modal-title").html("Datos inválidos");
             $("#modal-message #message").html("Verifica los datos requeridos para continuar<br/><br/>");
             $('#modal-message').foundation('reveal', 'open');
             return false;
         }else{
             return true;
         }
     };
    function validateaEmail(value){
         if(!isaEmail(value)){
             $('#modal-message').foundation('reveal', 'close');
             $("#modal-message #modal-title").html("Datos inválidos");
             $("#modal-message #message").html("Verifica tu dirección de correo para continuar<br/><br/>");
             $('#modal-message').foundation('reveal', 'open');
             return false;
         }else{
             return true;
         }
     };
       
        function isaEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        };
        
});

function cartDropdown(obj){
    $('#drop1 .resume-cart').html('<img src="'+base_url+'images/loading.png"/>');
    $.ajax({
        url: base_url + "browser/cartInfo/",
        type: "post",            
        data: {},
        dataType: "json",
        success: function(data){   
            if(data.status == true){
                var html ='';
                $("#cartCount").html(data.data.cart.length);
                $.each(data.data.cart, function( key, value ) {
                    html += '<div class="products row">';
                    html += '   <div class="columns large-9 small-12 justify">';
                    html += '       '+value.name;
                    html += '   </div>';
//                    html += '   <div class="columns large-3">';
//                    html += '       '+value.cost;
//                    html += '   </div>';
                    html += '   <div class="columns large-3 small-12 text-right">';
                    html += '      $ '+value.totalCost;
                    html += '   </div>';
                    html += '</div>';
                });
                if(html == ''){
                    html += '<div class="products row">';
                    html += '   <div class="columns large-9 small-12 justify">';
                    html += '       No hay productos';
                    html += '   </div>';
                    html += '</div>';
                }
                html += '<div class="products row">';
                html += '   <div class="columns large-6 right resume-total">';
                html += ' $ '+data.data.total.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                html += '   </div>';
                html += '   <div class="columns large-6 text-left resume-total">';
                html += '   Total';
                html += '   </div>';
                html += '</div>';
//                console.log(data.data.total.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'))
                $('#drop1 .resume-cart').html(html);
            }
        }
    });
}

/*
 * Active dropdwon when the 'link' has children
 * Hide the dropdown when click in a other element
 * */
function changeBehaviorDropdown(){
    $("[data-dropdown]").children().data("dropdown-child", true);                
    
    $(document).on("mousehover", "*", function(e){
        var isDropdown = $(e.target).data('dropdown');
        var isChildDropdown = $(e.target).data('dropdown-child');
        var isContent = $(e.target).hasClass("f-dropdown");
        var isIntoContent = false;
        
        $(e.target).parents().each(function(){
            if ($(this).hasClass("f-dropdown")){
                isIntoContent = true;
            }
        });
        
        if (!isDropdown && !isChildDropdown  && !isContent && !isIntoContent){
            $("[data-dropdown]").each(function(){
                Foundation.libs.dropdown.hide($(this));
            });
        }
    });
} 

//function for preview image from input file
  function readURL(input,destinationId) {      
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+destinationId).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    
}


function resizeHeight(){
//    $("#container-menu").css("height",'');
//    var heightMenu = $("#container-menu").css("height").replace('px','');
//    var heightContent = $("#content").css("height").replace('px','');  
////    alert(heightMenu)
////    alert(heightContent)
//    if(parseInt(heightMenu) < parseInt(heightContent)){
//        $("#container-menu").css("height",heightContent);        
//    }else{
//        $("#content").css("height",heightMenu);                
//    }
}