$(document).ready(function(){
    
    mediaQueries($(window).width());
    $(window).resize(function() {
        mediaQueries($(window).width());
        
    });
    function mediaQueries(windowsize){      
        //for small
        if (windowsize < 641) {
            if( ! $(".tabs").hasClass("vertical")){
                $(".tabs").addClass("vertical");
            }
            if (windowsize < 341) {
                if($(".content:first-child").hasClass("columns")){
                    $(".content:first-child").removeClass("columns")
                }
            }
        }
        //for medium
        if (windowsize >= 641) {        
            if($(".tabs").hasClass("vertical")){
                $(".tabs").removeClass("vertical");
            }
        }
        //for large
        if (windowsize >= 1025) {

        }
    }
    
})
